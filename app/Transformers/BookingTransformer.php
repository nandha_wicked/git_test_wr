<?php 
namespace App\Transformers;

/**
 *
 */
class BookingTransformer extends Transformer{

    public function transform($booking){
        $fromDate = strtotime($booking['from_datetime']);
        $toDate = strtotime($booking['to_datetime']);
        $datediff = $toDate - $fromDate;

        return [
          'rent_summary_id' => $booking['id'],
          'rent_number_of_days' => floor($datediff/(60*60*24)) . ' days',
          'rent_amount' => $booking['total_paid']
        ];
    }
}