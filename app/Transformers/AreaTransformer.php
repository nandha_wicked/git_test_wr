<?php 
  namespace App\Transformers;

/**
 *
 */
  class AreaTransformer extends Transformer{
    public function transform($area){
        return [
          'id' => $area['id'],
          'city_id' => $area['city_id'],
          'area' => $area['area'],
          'address' => $area['address'],
          'latitude' => $area['latitude'],
          'longitude' => $area['longitude'],
          'status' => $area['status']
        ];
    }
  }