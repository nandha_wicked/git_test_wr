<?php 
    namespace App\Transformers;

  /**
   *
   */
  class ImageTransformer extends Transformer{
    public function transform($image){
      return [
        'id' => $image['id'],
        'full' => empty($image['full']) ? "" : env('HTTPSORHTTP').":".$image['full'],
        'small' => empty($image['small']) ? "" : env('HTTPSORHTTP').":".$image['small'],
        'medium' => empty($image['medium']) ? "" : env('HTTPSORHTTP').":".$image['medium'],
        'large' => empty($image['large']) ? "" : env('HTTPSORHTTP').":".$image['large']
      ];
    }

  }
