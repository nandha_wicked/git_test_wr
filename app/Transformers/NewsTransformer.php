<?php namespace App\Transformers;

/**
 *
 */
class NewsTransformer extends Transformer{

      public function transform($news){
          return [
            'id' => $news['id'],
            'news' => $news['news'],
            'status' => $news['status'],
          ];
      }
}