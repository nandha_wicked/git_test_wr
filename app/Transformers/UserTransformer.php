<?php namespace App\Transformers;

/**
 *
 */
class UserTransformer extends Transformer{

      public function transform($user){
          return [
            'first_ame' => $user['first_name'],
            'last_ame' => $user['last_name'],
            'email' => $user['email'],
            'mobile' => $user['mobile_num'],
            'dob' => $user['dob']
          ];
      }
}