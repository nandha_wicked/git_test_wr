<?php 
namespace App\Transformers;

/**
 *
 */
class CityTransformer extends Transformer{

      public function transform($city){
          return [
            'id' => $city['id'],
            'city_name' => $city['city'],
            'status' => $city['status']
          ];
      }
}