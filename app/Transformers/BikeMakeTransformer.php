<?php 
  namespace App\Transformers;

  /**
   *
   */
  class BikeMakeTransformer extends Transformer{
    public function transform($bm){
      return [
        'id' => $bm['id'],
        'name' => $bm['bike_make'],
        'status' => $bm['status'],
        'logo_id' => $bm['logo_id'],
        'logo' => $bm['logo']
      ];
    }

  }
