<?php namespace App\Transformers;

/**
 *
 */
class BikeTransformer extends Transformer{

      public function transform($bike){
          return [
            'id' => $bike['id'],
            'name' => $bike['name'],
          ];
      }
}