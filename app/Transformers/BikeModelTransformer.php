<?php 
namespace App\Transformers;

/**
 *
 */
class BikeModelTransformer extends Transformer{

      public function transform($bm){
          return [
            'id' => $bm['id'],
            'name' => $bm['bike_model'],
            'description' => $bm['description'],
            'make_id' => $bm['bike_make_id'],
            'image_id' => $bm['thumbnail_img_id'],
            'image' => $bm['image']
          ];
      }
}