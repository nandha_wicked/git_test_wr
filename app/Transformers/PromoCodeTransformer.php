<?php 
namespace App\Transformers;

class PromoCodeTransformer extends Transformer{
  public function transform($promoCode){
    return [
      "id" => $promoCode['id'],
      "code" => $promoCode['code'],
      "title" => $promoCode['title'],
      "description" => $promoCode['description'],
      "valid_from" => $promoCode['start_set'],
      "valid_to" => $promoCode['end_set'],
      "value_type" => $promoCode['value_type'],
      "value" => $promoCode['value'],
      "total_count" => $promoCode['total_count'],
      "available_count" => $promoCode['available_count'],
      "new_user" => $promoCode['new_user'],
      "min_order" => $promoCode['min_order'],
      "weekday" => $promoCode['weekday'],
      "weekend" => $promoCode['weekend'],
      "dates" => $promoCode['dates'],
      "status" => $promoCode['status']
    ];
  }
}