<?php namespace App\Transformers;

/**
 *
 */
class ReviewsTransformer extends Transformer{

      public function transform($reviews){
          return [
            'id' => $reviews['id'],
            'review' => $reviews['review'],
            'reviewed_on' => $reviews['reviewed_on'],  
            'name' => $reviews['name'],
            'status' => $reviews['status'],
            'image_id' => $reviews['image_id'],
            'image' => $reviews['image']
          ];
      }
}