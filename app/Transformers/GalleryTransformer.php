<?php 
namespace App\Transformers;

/**
 *
 */
class GalleryTransformer extends Transformer{
  public function transform($gallery){
    return [
      'id' => $gallery['id'],
      'name' => $gallery['name'],
      'description' => $gallery['description'],
      'images' => $gallery['images']
      ];
  }

}