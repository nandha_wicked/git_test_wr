<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException as TokenMismatchException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof TokenMismatchException) {
            $message = "Session expired, go back and try again!";
            return response()->view('front/error', ['message' => $message]);
        }
        //JWT EXCEPTIONS
        if ($e instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            return response()->json([
                ["result" => 
                    [
                        "error" => [
                            "message" => "token_expired",
                            "status_code" => $e->getStatusCode()
                        ]
                    ]
                ]
            ], $e->getStatusCode());
        } else if ($e instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return response()->json([
                 ["result" => 
                    [
                        "error" => [
                            "message" => "token_invalid",
                            "status_code" => $e->getStatusCode()
                        ]
                    ]
                ]
            ], $e->getStatusCode());
        }

        //API EXCEPTIONS
        if ($request->is('api/*') && $e instanceof MethodNotAllowedHttpException) {
            return response()->json([
                 ["result" => 
                    [
                        "error" => [
                            "message" => "Method Not Allowed",
                            "status_code" => 405
                        ]
                    ]
                ]
            ], 405);
        }

        if($request->is('api/*') && ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException) ){
            return response()->json([
                 ["result" => 
                    [
                        "error" => [
                            "message" => "Not Found",
                            "status_code" => 404
                        ]
                    ]
                ]
            ], 404);
        }

        if($request->is('api/*')){
            return response()->json([
                 ["result" => 
                    [
                        "error" => [
                            "message" => "Server Error!",
                            "status_code" => 500
                        ]
                    ]
                ]
            ], 500);
        }

        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        return parent::render($request, $e);
    }
}
