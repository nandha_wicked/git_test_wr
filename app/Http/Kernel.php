<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\AuthCheck::class,
        'auth.erpapp' => \App\Http\Middleware\AuthenticateErpApi::class,
        'auth.admin' => \App\Http\Middleware\Authenticate::class,
        'auth.vendor' => \App\Http\Middleware\AuthenticateVendor::class,
        'auth.partner' => \App\Http\Middleware\AuthenticatePartner::class,
        'auth.erp' => \App\Http\Middleware\AuthenticateErp::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'api.log' => \App\Http\Middleware\LogRequests::class,
        'permission'=> \App\Http\Middleware\Permission::class
    ];
}
