<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BikeList extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required|integer',
            'pick_up_date' => 'date',
            'drop_date' => 'date',
            'make_id' => 'integer'
        ];
    }
}
