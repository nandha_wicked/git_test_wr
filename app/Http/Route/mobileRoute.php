<?php
/*
|--------------------------------------------------------------------------
| Mobile Application Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'api'], function(){
	Route::get('users', 'AppController@getAll');
	Route::get('users/{id}', 'AppController@getById');

	Route::get('cities/{id}/areas', 'AreaController@getAllAreas');
	Route::get('news', 'NewsController@getAllNews');
	Route::get('makes/by-city', 'BikeMakeController@getMakesByCity');
	Route::get('models/{id}/', 'BikeModelController@getModelDetails');

	
	Route::get('gallery/by-model', 'GalleryController@getGalleryImages');
	Route::post('gallery/create','GalleryController@store');
});
