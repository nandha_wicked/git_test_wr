<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\User;
use App\Models\Parameters;


class AdminController extends AppController
{
    public function index(){
    	if(Auth::check())
    		return redirect('admin/dashboard');
    	else
    		return redirect('admin/login');
    }

    public function login(){
    	return view('admin.login');
    }

    public function dashboard(){
    	return view('admin.dashboard');
    }
    
    public function indexAppRequest(Request $request)
    {
        return view('admin.appRequest.index');
    }
    
    public function searchAppRequest(Request $request)
    {
        $appRequestObj = DB::table('appRequest')->where('request', 'like', '%'.$request->searchStr.'%')->get();
        
        $appRequests=[];
        foreach($appRequestObj as $appRequest)
        {
                        
            array_push($appRequests,['enquiry'=>"",'string'=>$appRequest->request]);
            
        }
        
        return view('admin.appRequest.index',compact('appRequests'));
    }
    
    
    public function searchUser(Request $request)
    {
        $searchStr = $request->searchStr;
        $userResult = User::where('email',$request->searchStr)->first();
        return view('admin.addUserToErp.index',compact('searchStr','userResult'));
    }
    
    public function indexAppRequestLog(Request $request)
    {
        $appRequests = DB::table('payment_success_calls')->orderBy('created_at','desc')->get();
        return view('admin.appRequestLog.index',compact('appRequests'));
    }
    
    public function searchAppLogRequest(Request $request)
    {
        $appRequests = DB::table('payment_success_calls')->where('request', 'like', '%'.$request->searchStr.'%')->get();
        
        return view('admin.appRequestLog.index',compact('appRequests'));
    }
    
    
    
    
    
}
