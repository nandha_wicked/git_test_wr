<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\BikationVendor;
use Hash;
use Session;
use DB;
use Mail;
use App\Models\Bikation_Address;
use App\Models\Bikation_Package;
use DateTime;
use Response;

class BikationvendorController extends AppController
{
    public function index(){
      if(Session::has('email') && Session::has('First_Name')){
        return redirect('bikation-vendor/dashboard');
      }else{
        return redirect('bikation-vendor');
    }
  }
    public function dashboard(){
    if(Session::has('email') && Session::has('First_Name') ){
      return view('bikationvendor.front.dashboard');
      }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
    }
    public function getlogin(){

       if(Session::has('email') && Session::has('First_Name')){
        return redirect('bikation-vendor/dashboard');
      }else{
        return view('bikationvendor.front.login');
    }
     
    }
    public function postlogin(Request $request){
      $email=$request->email;
      $password=md5($request->password);
      $data=  DB::select("select * from bikationvendor_users where password='$password' AND email='$email'");
      if(!empty($data)){
        if ($data['0']->status == 1) {
          Session::put('email',$data['0']->email);
          Session::put('Vendor_ID',$data['0']->id);
          Session::put('First_Name',$data['0']->first_name);
          // Authentication passed...
          return redirect('bikation-vendor/dashboard');
        }
        return redirect('bikation-vendor')->withErrors("Sorry, you are not Active User Please contact Admin.");
      }
      return redirect('bikation-vendor')->withErrors("Sorry, Invalid Credentials");
    }
    public function getregister(){
      if(Session::has('email') && Session::has('First_Name')){
        return redirect('bikation-vendor/dashboard');
      }else{
       return view('bikationvendor.front.register');
    }
      
    }
    public function postregister(Request $request){
      $rules = array('first_name' => 'required',
                    'email' => 'required|unique:bikationvendor_users',
                    'mobile_num' => 'required',
                    'password' => 'required',
                    'Area' => 'required',
                    'City' => 'required',
                    'State' => 'required',
                    'Country' => 'required',
                    'ZipCode' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-vendor')->withErrors($validator->errors());
      }else{
        $status=0;
        $password=md5($request->password);
        $user_email=$request->email;  
      
        $inseart= BikationVendor::create(['first_name' => $request->first_name, 'last_name' => $request->last_name,'email' => $request->email,'mobile_num' => $request->mobile_num,'password' => $password,'remember_token' => $request->remember_token,'status'=>$status]);
        $Vendor_ID= $inseart->id;
        $CreatedBy=$Vendor_ID;
        $UpdatedBy=$Vendor_ID;
        $Created_On = new DateTime();
        $CreatedOn= $Created_On->format('Y-m-d H:i:s');
        $Updated_On = new DateTime();
        $UpdatedOn= $Updated_On->format('Y-m-d H:i:s');
        $address=Bikation_Address::create(['Vendor_ID' => $Vendor_ID,'Created_By' => $CreatedBy,'Updated_By' => $UpdatedBy,'Created_On' => $CreatedOn,'Updated_On'=>$UpdatedOn,'Door_No' => $request->Door_No,'Street_1' => $request->Street_1, 'Street_2' => $request->Street_2,'Area' => $request->Area,'City' => $request->City,'State'=>$request->State,'Country' => $request->Country,'ZipCode' => $request->ZipCode,'Name' => $request->Name,'LandLine_Number'=>$request->LandLine_Number,'Phone_Number' => $request->Phone_Number,'Alternate_Number' => $request->Alternate_Number,'Website'=>$request->Website,'about_me'=>$request->about_me]);
        $Address_ID= $address->id;
        BikationVendor::where('id', $Vendor_ID)->update(['Address_ID'=>$Address_ID]);
		 $email = $request->email;
		 $data = ['fname'=>$request->first_name, 'lname'=>$request->last_name, 'email'=>$request->email, 'mob'=>$request->mobile_num, 'Vendor_ID'=>$Vendor_ID];
		 Mail::send('emails.bikation_vendor_registration', $data, function ($message) use ($email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($email)->subject('WickedRide Bikation Vendor');

                });
				$adm_email = env('BIKATION_ADMIN_EMAIL');
		Mail::send('emails.bikation_admin_vendor_registration', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('WickedRide Bikation Vendor');

                });	
				
        return redirect('bikation-vendor/register')->with('message', 'Thanks For Register!!. Please wait for admin approval email.')->with('message-type', 'success');
      }
    }
    public function doLogout(){
    Session::flush(); // log the user out of our application
    return redirect('bikation-vendor'); // redirect the user to the homepage
    }
   public function get_packages(Request $request)
   {
	   $rules = array();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
	     $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name','Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Status', 1)->where('Trip_ID', $request->Trip_ID)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
		}
		 return Response::json(array(
			 'success' => $status,
            'bikation_packageList' => $bikation_packageList,
            'errors' => $validator->getMessageBag()->toArray()
            
        ));
		 //return $bikation_packageList;
   }
}
