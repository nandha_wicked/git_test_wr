<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Auth;
use DB;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\User;
use App\Models\Parameters;
use Curl;
use Carbon\Carbon;
use Mail;


use App\Models\RandMService;
use App\Models\RandMServiceAlert;
use App\Models\RandMServiceDealer;
use App\Models\RandMServiceRecord;
use App\Models\RandMServiceSchedule;
use App\Models\RandMInventory;
use App\Models\RandMInventoryRegister;
 
class RandMController extends AppController
{
    
    public function index()
    {
    	if(Auth::check())
        {
    	   return redirect('randm/dashboard');
        }
        else
    		return redirect('randm/login');
    }

    public function login()
    {
    	return view('randm.login');
    }

    public function dashboard()
    {
    	return view('randm.dashboard');
    }
    
    
    public function indexRandMInventory(Request $request)
    {
        $pageTitle = "RandM Inventory";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Name"],
                                            ["width"=>140,"title"=>"Code"],
                                            ["width"=>250,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMInventoryObjects = RandMInventory::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMInventoryObjects as $randMInventoryObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMInventoryObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryObject->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryObject->code,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($randMInventoryObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMInventory_btn",
                                                            "id"=>"edit_randMInventory_id",
                                                            "data_id"=>$randMInventoryObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMInventory_btn",
                                                            "id"=>"delete_randMInventory_id",
                                                            "data_id"=>$randMInventoryObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMInventory_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Inventory";
        $addObject['add_url'] = "/randm/randm-inventory/add";
        $addObject['add_modal_form_items'] = [];
        
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_add"],
                ["type"=>"input","label"=>"Code","input_type"=>"text","name"=>"code_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMInventory_modal";
        $editObject['edit_btn_class'] = "edit_randMInventory_btn";
        $editObject['modal_title'] = "Edit RandMInventory";
        $editObject['edit_url'] = "/randm/randm-inventory/edit";
        $editObject['ajax_url'] = "/randm/randm-inventory/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_edit"],
                ["type"=>"input","label"=>"Code","input_type"=>"text","name"=>"code_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMInventory_modal";
        $deleteObject['delete_btn_class'] = "delete_randMInventory_btn";
        $deleteObject['modal_title'] = "Delete RandM Inventory";
        $deleteObject['delete_url'] = "/randm/randm-inventory/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Inventory";
        
        $layoutOverride = "randm_layout";
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride', 'layoutOverride')); 
        
    }
    
    public function addRandMInventory(Request $request)
    {
        $rules = array(
            'name_add' => 'required',
            'status_add' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        if($request->code_add == "")
            $code = $request->name_add;
        else
            $code = $request->code_add;

        
        
        RandMInventory::create([
            'created_by'=>Auth::user()->id,
            'name'=>$request->name_add,
            'code'=>$code,
            'description'=>$request->description_add,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsRandMInventory(Request $request)
    {
        $randMInventory = RandMInventory::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"name_edit","value"=>$randMInventory->name],
                ["type"=>"input","name"=>"code_edit","value"=>$randMInventory->code],
                ["type"=>"textarea","name"=>"description_edit","value"=>$randMInventory->description],
                ["type"=>"checkbox","status"=>$randMInventory->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]

        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMInventory($id, Request $request)
    {
        $rules = array(
            'name_edit' => 'required',
            'status_edit' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        if($request->code_edit == "")
            $code = $request->name_edit;
        else
            $code = $request->code_edit;

        
        
        
        RandMInventory::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'name'=>$request->name_edit,
            'code'=>$code,
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteRandMInventory($id, Request $request)
    {
        RandMInventory::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    
    
    public function indexRandMInventoryRegister(Request $request)
    {
        $pageTitle = "RandM Inventory Register";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Inventory"],
                                            ["width"=>140,"title"=>"Code"],
                                            ["width"=>140,"title"=>"Dealer"],
                                            ["width"=>80,"title"=>"Qty"],
                                            ["width"=>80,"title"=>"Cost"],
                                            ["width"=>80,"title"=>"Nature"],
                                            ["width"=>120,"title"=>"Reference"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMInventoryRegisterObjects = RandMInventoryRegister::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMInventoryRegisterObjects as $randMInventoryRegisterObject)
        {
            $inventory = $randMInventoryRegisterObject->getInventory();
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMInventoryRegisterObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$inventory->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$inventory->code,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryRegisterObject->getDealerName(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryRegisterObject->qty,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryRegisterObject->cost,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($randMInventoryRegisterObject->nature=="A")?"Added":"Consumed","pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMInventoryRegisterObject->reference,"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMInventoryRegister_btn",
                                                            "id"=>"edit_randMInventoryRegister_id",
                                                            "data_id"=>$randMInventoryRegisterObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMInventoryRegister_btn",
                                                            "id"=>"delete_randMInventoryRegister_id",
                                                            "data_id"=>$randMInventoryRegisterObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMInventoryRegister_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Inventory Register";
        $addObject['add_url'] = "/randm/randm-inventory-register/add";
        $addObject['add_modal_form_items'] = [];
        
        
        $inventoryList = [];
        $inventoryObjs = RandMInventory::where('deleted',0)->where('status',1)->get();
        
        foreach($inventoryObjs as $inventoryObj)
        {
            array_push($inventoryList,["value"=>$inventoryObj->id,"text"=>$inventoryObj->code]);
        }
        
        $dealerList = [];
        $dealerObjs = RandMServiceDealer::where('deleted',0)->where('status',1)->get();
        
        foreach($dealerObjs as $dealerObj)
        {
            array_push($dealerList,["value"=>$dealerObj->id,"text"=>$dealerObj->name]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"combobox","label"=>"Inventory", "name"=>"inventory_add", "id"=>"inventory_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$inventoryList],["type"=>"combobox","label"=>"Dealer", "name"=>"dealer_add", "id"=>"dealer_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Dealer --", "select_items"=>$dealerList],
                ["type"=>"input","label"=>"Qty","input_type"=>"text","name"=>"qty_add"],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_add"],
                ["type"=>"input","label"=>"Reference","input_type"=>"text","name"=>"reference_add"]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMInventoryRegister_modal";
        $editObject['edit_btn_class'] = "edit_randMInventoryRegister_btn";
        $editObject['modal_title'] = "Edit RandMInventoryRegister";
        $editObject['edit_url'] = "/randm/randm-inventory-register/edit";
        $editObject['ajax_url'] = "/randm/randm-inventory-register/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"select","label"=>"Inventory", "name"=>"inventory_edit", "id"=>"inventory_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$inventoryList],["type"=>"select","label"=>"Dealer", "name"=>"dealer_edit", "id"=>"dealer_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Dealer --", "select_items"=>$dealerList],
                ["type"=>"input","label"=>"Qty","input_type"=>"text","name"=>"qty_edit"],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_edit"],
                ["type"=>"input","label"=>"Reference","input_type"=>"text","name"=>"reference_edit"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMInventoryRegister_modal";
        $deleteObject['delete_btn_class'] = "delete_randMInventoryRegister_btn";
        $deleteObject['modal_title'] = "Delete RandM Inventory Register";
        $deleteObject['delete_url'] = "/randm/randm-inventory-register/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Inventory Register";
        
        $jqueryOverride = "<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>";
        
        $layoutOverride = "randm_layout";
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride','jqueryOverride')); 
        
    }
    
    public function addRandMInventoryRegister(Request $request)
    {
        $rules = array(
            'inventory_add' => 'required',
            'dealer_add' => 'required',
            'qty_add' => 'required',
            'cost_add' => 'required',
            'reference_add' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }        
        
        RandMInventoryRegister::create([
            'created_by'=>Auth::user()->id,
            'inventory'=>$request->inventory_add,
            'dealer'=>$request->dealer_add, 
            'qty'=>$request->qty_add, 
            'cost'=>$request->cost_add, 
            'nature'=>"A", 
            'reference'=>$request->reference_add,
        ]);
        
        $inventory = RandMInventory::where('id',$request->inventory_add)->first();
        
        RandMInventory::where('id',$request->inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added + $request->qty_add),"available"=>($inventory->available + $request->qty_add)]);
        
        return redirect()->back();
    }
    
    public function detailsRandMInventoryRegister(Request $request)
    {
        $randMInventoryRegister = RandMInventoryRegister::where('id',$request->id)->first();
        $data = [
                ["type"=>"select","name"=>"inventory_edit","value"=>$randMInventoryRegister->country_of_origin],
                ["type"=>"input","name"=>"qty_edit","value"=>$randMInventoryRegister->qty],
                ["type"=>"input","name"=>"cost_edit","value"=>$randMInventoryRegister->cost],
                ["type"=>"input","name"=>"reference_edit","value"=>$randMInventoryRegister->reference]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMInventoryRegister($id, Request $request)
    {
        $rules = array(
            'inventory_edit' => 'required',
            'dealer_edit' => 'required',
            'qty_edit' => 'required',
            'cost_edit' => 'required',
            'reference_edit' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $inventoryRegister = RandMInventoryRegister::where('id',$id)->first();
        
        RandMInventoryRegister::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'inventory'=>$request->inventory_edit,
            'dealer'=>$request->dealer_edit, 
            'qty'=>$request->qty_edit, 
            'cost'=>$request->cost_edit, 
            'reference'=>$request->reference_edit,
        ]);
        
        $inventory = RandMInventory::where('id',$request->inventory_add)->first();
        
        RandMInventory::where('id',$request->inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added + ($request->qty_add - $inventoryRegister->qty)),"available"=>($inventory->available + ($request->qty_add - $inventoryRegister->qty))]);
        
        return redirect()->back();
    }
    
    public function deleteRandMInventoryRegister($id, Request $request)
    {
        $inventory = RandMInventory::where('id',$request->inventory_add)->first();
        
        RandMInventory::where('id',$request->inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added - $inventoryRegister->qty),"available"=>($inventory->available - $inventoryRegister->qty)]);
        
        RandMInventoryRegister::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    
    
    public function indexRandMServiceDealer(Request $request)
    {
        $pageTitle = "RandM Service Dealer";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Name"],
                                            ["width"=>140,"title"=>"City"],
                                            ["width"=>200,"title"=>"Address"],
                                            ["width"=>140,"title"=>"Contact Person"],
                                            ["width"=>160,"title"=>"Email"],
                                            ["width"=>140,"title"=>"Phone"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMServiceDealerObjects = RandMServiceDealer::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMServiceDealerObjects as $randMServiceDealerObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->city,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->address,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->contact_person_name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->contact_person_email,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceDealerObject->contact_person_phone,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($randMServiceDealerObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMServiceDealer_btn",
                                                            "id"=>"edit_randMServiceDealer_id",
                                                            "data_id"=>$randMServiceDealerObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMServiceDealer_btn",
                                                            "id"=>"delete_randMServiceDealer_id",
                                                            "data_id"=>$randMServiceDealerObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMServiceDealer_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Service Dealer";
        $addObject['add_url'] = "/randm/randm-service-dealer/add";
        $addObject['add_modal_form_items'] = [];
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_add"],
                ["type"=>"input","label"=>"City","input_type"=>"text","name"=>"city_add"],
                ["type"=>"input","label"=>"State","input_type"=>"text","name"=>"state_add"],
                ["type"=>"input","label"=>"Country","input_type"=>"text","name"=>"country_add"],
                ["type"=>"textarea","label"=>"Address","input_type"=>"text","name"=>"address_add"],
                ["type"=>"input","label"=>"Postal Code","input_type"=>"text","name"=>"postal_code_add"],
                ["type"=>"input","label"=>"GST","input_type"=>"text","name"=>"gst_add"],
                ["type"=>"input","label"=>"Contact Person","input_type"=>"text","name"=>"contact_person_name_add"],
                ["type"=>"input","label"=>"Phone Number","input_type"=>"text","name"=>"contact_person_phone_add"],
                ["type"=>"input","label"=>"Email","input_type"=>"text","name"=>"contact_person_email_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMServiceDealer_modal";
        $editObject['edit_btn_class'] = "edit_randMServiceDealer_btn";
        $editObject['modal_title'] = "Edit RandMServiceDealer";
        $editObject['edit_url'] = "/randm/randm-service-dealer/edit";
        $editObject['ajax_url'] = "/randm/randm-service-dealer/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_edit"],
                ["type"=>"input","label"=>"City","input_type"=>"text","name"=>"city_edit"],
                ["type"=>"input","label"=>"State","input_type"=>"text","name"=>"state_edit"],
                ["type"=>"input","label"=>"Country","input_type"=>"text","name"=>"country_edit"],
                ["type"=>"textarea","label"=>"Address","input_type"=>"text","name"=>"address_edit"],
                ["type"=>"input","label"=>"Postal Code","input_type"=>"text","name"=>"postal_code_edit"],
                ["type"=>"input","label"=>"GST","input_type"=>"text","name"=>"gst_edit"],
                ["type"=>"input","label"=>"Contact Person","input_type"=>"text","name"=>"contact_person_name_edit"],
                ["type"=>"input","label"=>"Phone Number","input_type"=>"text","name"=>"contact_person_phone_edit"],
                ["type"=>"input","label"=>"Email","input_type"=>"text","name"=>"contact_person_email_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMServiceDealer_modal";
        $deleteObject['delete_btn_class'] = "delete_randMServiceDealer_btn";
        $deleteObject['modal_title'] = "Delete RandM Service Dealer";
        $deleteObject['delete_url'] = "/randm/randm-service-dealer/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Service Dealer";
        
        $layoutOverride = "randm_layout";
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride')); 
        
    }
    
    public function addRandMServiceDealer(Request $request)
    {
        $rules = array(
            'name_add' => 'required',
            'status_add' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }        
        
        RandMServiceDealer::create([
            'name'=>$request->name_add, 
            'city'=>$request->city_add, 
            'state'=>$request->state_add, 
            'country'=>$request->country_add, 
            'address'=>$request->address_add, 
            'postal_code'=>$request->postal_code_add, 
            'gst'=>$request->gst_add, 
            'contact_person_name'=>$request->contact_person_name_add, 
            'contact_person_phone'=>$request->contact_person_phone_add, 
            'contact_person_email'=>$request->contact_person_email_add,
            'status'=>$request->status_add,
            'created_by'=>Auth::user()->id
        ]);
        
        
        return redirect()->back();
    }
    
    public function detailsRandMServiceDealer(Request $request)
    {
        $randMServiceDealer = RandMServiceDealer::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"name_edit","value"=>$randMServiceDealer->name],
                ["type"=>"input","name"=>"city_edit","value"=>$randMServiceDealer->city],
                ["type"=>"input","name"=>"state_edit","value"=>$randMServiceDealer->state],
                ["type"=>"input","name"=>"country_edit","value"=>$randMServiceDealer->country],
                ["type"=>"textarea","name"=>"address_edit","value"=>$randMServiceDealer->address],
                ["type"=>"input","name"=>"postal_code_edit","value"=>$randMServiceDealer->postal_code],
                ["type"=>"input","name"=>"gst_edit","value"=>$randMServiceDealer->gst],
                ["type"=>"input","name"=>"contact_person_name_edit","value"=>$randMServiceDealer->contact_person_name],
                ["type"=>"input","name"=>"contact_person_phone_edit","value"=>$randMServiceDealer->contact_person_phone],
                ["type"=>"input","name"=>"contact_person_email_edit","value"=>$randMServiceDealer->contact_person_email],
                ["type"=>"checkbox","status"=>$randMServiceDealer->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMServiceDealer($id, Request $request)
    {
        $rules = array(
            'name_edit' => 'required',
            'status_edit' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        
        RandMServiceDealer::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'name'=>$request->name_edit, 
            'city'=>$request->city_edit, 
            'state'=>$request->state_edit, 
            'country'=>$request->country_edit, 
            'address'=>$request->address_edit, 
            'postal_code'=>$request->postal_code_edit, 
            'gst'=>$request->gst_edit, 
            'contact_person_name'=>$request->contact_person_name_edit, 
            'contact_person_phone'=>$request->contact_person_phone_edit, 
            'contact_person_email'=>$request->contact_person_email_edit,
            'status'=>$request->status_edit,
        ]);
        
        
        return redirect()->back();
    }
    
    public function deleteRandMServiceDealer($id, Request $request)
    {
        RandMServiceDealer::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    
    
    public function indexRandMService(Request $request)
    {
        $pageTitle = "RandM Service";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Name"],
                                            ["width"=>140,"title"=>"Code"],
                                            ["width"=>250,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMServiceObjects = RandMService::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMServiceObjects as $randMServiceObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMServiceObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceObject->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceObject->code,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($randMServiceObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMService_btn",
                                                            "id"=>"edit_randMService_id",
                                                            "data_id"=>$randMServiceObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMService_btn",
                                                            "id"=>"delete_randMService_id",
                                                            "data_id"=>$randMServiceObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMService_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Service";
        $addObject['add_url'] = "/randm/randm-service/add";
        $addObject['add_modal_form_items'] = [];
        
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_add"],
                ["type"=>"input","label"=>"Code","input_type"=>"text","name"=>"code_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMService_modal";
        $editObject['edit_btn_class'] = "edit_randMService_btn";
        $editObject['modal_title'] = "Edit RandMService";
        $editObject['edit_url'] = "/randm/randm-service/edit";
        $editObject['ajax_url'] = "/randm/randm-service/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Name","input_type"=>"text","name"=>"name_edit"],
                ["type"=>"input","label"=>"Code","input_type"=>"text","name"=>"code_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMService_modal";
        $deleteObject['delete_btn_class'] = "delete_randMService_btn";
        $deleteObject['modal_title'] = "Delete RandM Service";
        $deleteObject['delete_url'] = "/randm/randm-service/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Service";
        
        $layoutOverride = "randm_layout";
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride')); 
        
    }
    
    public function addRandMService(Request $request)
    {
        $rules = array(
            'name_add' => 'required',
            'status_add' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        if($request->code_add == "")
            $code = $request->name_add;
        else
            $code = $request->code_add;

        
        
        RandMService::create([
            'created_by'=>Auth::user()->id,
            'name'=>$request->name_add,
            'code'=>$code,
            'description'=>$request->description_add,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsRandMService(Request $request)
    {
        $randMService = RandMService::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"name_edit","value"=>$randMService->name],
                ["type"=>"input","name"=>"code_edit","value"=>$randMService->code],
                ["type"=>"textarea","name"=>"description_edit","value"=>$randMService->description],
                ["type"=>"checkbox","status"=>$randMService->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]

        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMService($id, Request $request)
    {
        $rules = array(
            'name_edit' => 'required',
            'status_edit' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        if($request->code_edit == "")
            $code = $request->name_edit;
        else
            $code = $request->code_edit;

        
        
        
        RandMService::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'name'=>$request->name_edit,
            'code'=>$code,
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteRandMService($id, Request $request)
    {
        RandMService::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    
    
    

public function indexRandMServiceSchedule(Request $request)
    {
        $pageTitle = "RandM Service Schedule";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Service"],
                                            ["width"=>140,"title"=>"Code"],
                                            ["width"=>160,"title"=>"Model"],
                                            ["width"=>80,"title"=>"Duration"],
                                            ["width"=>100,"title"=>"Soft Threshold"],
                                            ["width"=>100,"title"=>"Hard Threshold"],
                                            ["width"=>80,"title"=>"Cost"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMServiceScheduleObjects = RandMServiceSchedule::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMServiceScheduleObjects as $randMServiceScheduleObject)
        {
            $service = $randMServiceScheduleObject->getService();
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$service->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$service->code,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->getModelName(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->duration,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->odo_soft_threshold,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->odo_hard_threshold,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceScheduleObject->cost,"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMServiceSchedule_btn",
                                                            "id"=>"edit_randMServiceSchedule_id",
                                                            "data_id"=>$randMServiceScheduleObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMServiceSchedule_btn",
                                                            "id"=>"delete_randMServiceSchedule_id",
                                                            "data_id"=>$randMServiceScheduleObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMServiceSchedule_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Service Schedule";
        $addObject['add_url'] = "/randm/randm-service-schedule/add";
        $addObject['add_modal_form_items'] = [];
        
        
        $serviceList = [];
        $serviceObjs = RandMService::where('deleted',0)->where('status',1)->get();
        
        foreach($serviceObjs as $serviceObj)
        {
            array_push($serviceList,["value"=>$serviceObj->id,"text"=>$serviceObj->code]);
        }
    
        $modelList = [];
        $modelObjects = BikeModel::where('status',1)->get();
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->bike_model]);
        }
    
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"combobox","label"=>"Service", "name"=>"service_add", "id"=>"service_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$serviceList],
                ["type"=>"combobox","label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"input","label"=>"Duration","input_type"=>"text","name"=>"duration_add"],
                ["type"=>"input","label"=>"Soft Threshold","input_type"=>"text","name"=>"odo_soft_threshold_add"],
                ["type"=>"input","label"=>"Hard Threshold","input_type"=>"text","name"=>"odo_hard_threshold_add"],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_add"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMServiceSchedule_modal";
        $editObject['edit_btn_class'] = "edit_randMServiceSchedule_btn";
        $editObject['modal_title'] = "Edit RandMServiceSchedule";
        $editObject['edit_url'] = "/randm/randm-service-schedule/edit";
        $editObject['ajax_url'] = "/randm/randm-service-schedule/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"select","label"=>"Service", "name"=>"service_edit", "id"=>"service_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$serviceList],
                ["type"=>"select","label"=>"Model", "name"=>"model_edit", "id"=>"model_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"input","label"=>"Duration","input_type"=>"text","name"=>"duration_edit"],
                ["type"=>"input","label"=>"Soft Threshold","input_type"=>"text","name"=>"odo_soft_threshold_edit"],
                ["type"=>"input","label"=>"Hard Threshold","input_type"=>"text","name"=>"odo_hard_threshold_edit"],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_edit"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMServiceSchedule_modal";
        $deleteObject['delete_btn_class'] = "delete_randMServiceSchedule_btn";
        $deleteObject['modal_title'] = "Delete RandM Service Schedule";
        $deleteObject['delete_url'] = "/randm/randm-service-schedule/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Service Schedule";
        
        $jqueryOverride = "<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>";
        $layoutOverride = "randm_layout";
    
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride','jqueryOverride')); 
        
    }
    
    public function addRandMServiceSchedule(Request $request)
    {
        $rules = array(
            'service_add' => 'required',
            'model_add' => 'required',
            'duration_add' => 'required',
            'odo_soft_threshold_add' => 'required',
            'odo_hard_threshold_add' => 'required',
            'cost_add' => 'required',
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }        
        
        RandMServiceSchedule::create([
            'created_by'=>Auth::user()->id,
            'service'=>$request->service_add, 
            'model'=>$request->model_add, 
            'odo_soft_threshold'=>$request->odo_soft_threshold_add, 
            'odo_hard_threshold'=>$request->odo_hard_threshold_add, 
            'duration'=>$request->duration_add, 
            'cost'=>$request->cost_add, 
        ]);
        
        
        return redirect()->back();
    }
    
    public function detailsRandMServiceSchedule(Request $request)
    {
        $randMServiceSchedule = RandMServiceSchedule::where('id',$request->id)->first();
        $data = [
                ["type"=>"select","name"=>"service_edit","value"=>$randMServiceSchedule->service],
                ["type"=>"select","name"=>"model_edit","value"=>$randMServiceSchedule->model],
                ["type"=>"input","name"=>"duration_edit","value"=>$randMServiceSchedule->duration],
                ["type"=>"input","name"=>"odo_soft_threshold_edit","value"=>$randMServiceSchedule->odo_soft_threshold],
                ["type"=>"input","name"=>"odo_hard_threshold_edit","value"=>$randMServiceSchedule->odo_hard_threshold],
                ["type"=>"input","name"=>"cost_edit","value"=>$randMServiceSchedule->cost]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMServiceSchedule($id, Request $request)
    {
        $rules = array(
            'service_edit' => 'required',
            'model_edit' => 'required',
            'duration_edit' => 'required',
            'odo_soft_threshold_edit' => 'required',
            'odo_hard_threshold_edit' => 'required',
            'cost_edit' => 'required',
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $serviceRegister = RandMServiceSchedule::where('id',$id)->first();
        
        RandMServiceSchedule::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'service'=>$request->service_edit, 
            'model'=>$request->model_edit, 
            'odo_soft_threshold'=>$request->odo_soft_threshold_edit, 
            'odo_hard_threshold'=>$request->odo_hard_threshold_edit, 
            'duration'=>$request->duration_edit, 
            'cost'=>$request->cost_edit, 
        ]);
        
        return redirect()->back();
    }
    
    public function deleteRandMServiceSchedule($id, Request $request)
    {
        RandMServiceSchedule::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    
    
    
    
    public function indexRandMServiceRecord(Request $request)
    {
        $pageTitle = "RandM Service Record";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Service"],
                                            ["width"=>140,"title"=>"Code"],
                                            ["width"=>140,"title"=>"Dealer"],
                                            ["width"=>80,"title"=>"Cost"],
                                            ["width"=>120,"title"=>"Reference"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $randMServiceRecordObjects = RandMServiceRecord::where('deleted',0)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($randMServiceRecordObjects as $randMServiceRecordObject)
        {
            $service= $randMServiceRecordObject->getService();
            $body = [
                        ["type"=>"text","value"=>["text"=>$randMServiceRecordObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$service->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$service->code,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceRecordObject->getDealerName(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceRecordObject->cost,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$randMServiceRecordObject->reference,"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_randMServiceRecord_btn",
                                                            "id"=>"edit_randMServiceRecord_id",
                                                            "data_id"=>$randMServiceRecordObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_randMServiceRecord_btn",
                                                            "id"=>"delete_randMServiceRecord_id",
                                                            "data_id"=>$randMServiceRecordObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_randMServiceRecord_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add RandM Service Record";
        $addObject['add_url'] = "/randm/randm-service-record/add";
        $addObject['add_modal_form_items'] = [];
        
        
        $inventoryList = [];
        $inventoryObjs = RandMInventory::where('deleted',0)->where('status',1)->get();
        
        foreach($inventoryObjs as $inventoryObj)
        {
            array_push($inventoryList,["value"=>$inventoryObj->id,"text"=>$inventoryObj->code]);
        }
        
        $serviceList = [];
        $serviceObjs = RandMService::where('deleted',0)->where('status',1)->get();
        
        foreach($serviceObjs as $serviceObj)
        {
            array_push($serviceList,["value"=>$serviceObj->id,"text"=>$serviceObj->code]);
        }
        
        $dealerList = [];
        $dealerObjs = RandMServiceDealer::where('deleted',0)->where('status',1)->get();
        
        foreach($dealerObjs as $dealerObj)
        {
            array_push($dealerList,["value"=>$dealerObj->id,"text"=>$dealerObj->name]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"combobox","label"=>"Service", "name"=>"service_add", "id"=>"service_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$serviceList],
                ["type"=>"combobox","label"=>"Dealer", "name"=>"dealer_add", "id"=>"dealer_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Dealer --", "select_items"=>$dealerList],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_add"],
                ["type"=>"input","label"=>"Reference","input_type"=>"text","name"=>"reference_add"],
                ["type"=>"fieldset","fieldSetId"=>"inventoryAddFieldset","fieldSetButtonText"=>"Inventory","fieldSetElements"=>[["type"=>"select","label"=>"Inventory","name"=>"inventory_add","select_items"=>$inventoryList],["type"=>"input","label"=>"Qty","name"=>"inventory_qty_add"]]]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_randMServiceRecord_modal";
        $editObject['edit_btn_class'] = "edit_randMServiceRecord_btn";
        $editObject['modal_title'] = "Edit RandMServiceRecord";
        $editObject['edit_url'] = "/randm/randm-service-record/edit";
        $editObject['ajax_url'] = "/randm/randm-service-record/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"select","label"=>"Service", "name"=>"inventory_edit", "id"=>"inventory_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$inventoryList],["type"=>"select","label"=>"Dealer", "name"=>"dealer_edit", "id"=>"dealer_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Dealer --", "select_items"=>$dealerList],
                ["type"=>"input","label"=>"Qty","input_type"=>"text","name"=>"qty_edit"],
                ["type"=>"input","label"=>"Cost","input_type"=>"text","name"=>"cost_edit"],
                ["type"=>"input","label"=>"Reference","input_type"=>"text","name"=>"reference_edit"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_randMServiceRecord_modal";
        $deleteObject['delete_btn_class'] = "delete_randMServiceRecord_btn";
        $deleteObject['modal_title'] = "Delete RandM Service Record";
        $deleteObject['delete_url'] = "/randm/randm-service-record/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this RandM Service Record";
        
        $jqueryOverride = "<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>";
        
        $layoutOverride = "randm_layout";
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject', 'layoutOverride','jqueryOverride')); 
        
    }
    
    public function addRandMServiceRecord(Request $request)
    {
        $rules = array(
            'service_add' => 'required',
            'dealer_add' => 'required',
            'cost_add' => 'required',
            'reference_add' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }        
        
        $serviceRecord = RandMServiceRecord::create([
            'created_by'=>Auth::user()->id,
            'service'=>$request->service_add,
            'service_dealer'=>$request->dealer_add, 
            'cost'=>$request->cost_add, 
            'reference'=>$request->reference_add,
        ]);
        
        foreach($request->inventory_add as $index=>$inventory_add)
        {
            RandMInventoryRegister::create([
                'created_by'=>Auth::user()->id,
                'inventory'=>$inventory_add,
                'qty'=>$request->inventory_qty_add[$index], 
                'nature'=>"D", 
                'reference'=>$serviceRecord->id,
            ]);
        
            $inventory = RandMInventory::where('id',$inventory_add)->first();

            RandMInventory::where('id',$inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added - $request->inventory_qty_add[$index]),"available"=>($inventory->available - $request->inventory_qty_add[$index])]);
        }
        return redirect()->back();
    }
    
    public function detailsRandMServiceRecord(Request $request)
    {
        $randMServiceRecord = RandMServiceRecord::where('id',$request->id)->first();
        $data = [
                ["type"=>"select","name"=>"inventory_edit","value"=>$randMServiceRecord->country_of_origin],
                ["type"=>"input","name"=>"qty_edit","value"=>$randMServiceRecord->qty],
                ["type"=>"input","name"=>"cost_edit","value"=>$randMServiceRecord->cost],
                ["type"=>"input","name"=>"reference_edit","value"=>$randMServiceRecord->reference]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editRandMServiceRecord($id, Request $request)
    {
        $rules = array(
            'inventory_edit' => 'required',
            'dealer_edit' => 'required',
            'qty_edit' => 'required',
            'cost_edit' => 'required',
            'reference_edit' => 'required'
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $inventoryRegister = RandMServiceRecord::where('id',$id)->first();
        
        RandMServiceRecord::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'inventory'=>$request->inventory_edit,
            'dealer'=>$request->dealer_edit, 
            'qty'=>$request->inventory_qty_add[$index], 
            'cost'=>$request->cost_edit, 
            'reference'=>$request->reference_edit,
        ]);
        
        $inventory = RandMInventory::where('id',$request->inventory_add)->first();
        
        RandMInventory::where('id',$request->inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added + ($request->qty_add - $inventoryRegister->qty)),"available"=>($inventory->available + ($request->qty_add - $inventoryRegister->qty))]);
        
        return redirect()->back();
    }
    
    public function deleteRandMServiceRecord($id, Request $request)
    {
        $inventory = RandMInventory::where('id',$request->inventory_add)->first();
        
        RandMInventory::where('id',$request->inventory_add)->update(["total_qty_added"=>($inventory->total_qty_added - $inventoryRegister->qty),"available"=>($inventory->available - $inventoryRegister->qty)]);
        
        RandMServiceRecord::where('id',$id)->update(["deleted"=>1]);
        return redirect()->back();
    }
    

    
    
    
}
