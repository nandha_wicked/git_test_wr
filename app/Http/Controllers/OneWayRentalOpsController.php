<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Filesystem\Filesystem;
use Auth;
use DB;
use App\Models\OneWayRentalAdditionalTxn;
use App\Models\OneWayRentalBookingNotes;
use App\Models\OneWayRentalBike;
use App\Models\OneWayRentalBooking;
use App\Models\OneWayRentalLocation;
use App\Models\OneWayRentalRoute;
use App\Models\OneWayRentalMovementLog;
use App\Models\OneWayRentalMovementOrder;
use App\Models\OneWayRentalPrice;
use App\Models\OneWayRentalFinancialInfo;
use App\Models\OneWayRentalPricingInfo;
use App\Models\BikeModel;
use App\Models\User;
use App\Models\Wallet;
use App\Models\UserDocImage;
use App\Models\Parameters;
use App\Http\Controllers\UserController;
use Curl;
use App\Jobs\RazorPayOneWay;
use Carbon\Carbon;



class OneWayRentalOpsController extends AppController
{ 
    
    public function opsIndex(){
    	if(Auth::check())
        {
    	   return redirect('a2b/dashboard');
        }
        else
    		return redirect('a2b/login');
    }
    
    public function opsLogin(){
    	return view('a2b.login');
    }
    
    
    public function opsDashboard(){
    	return view('a2b.dashboard');
    }
    
    public function getDeliveriesForOps(Request $request)
    {
        
        if(isset($request->locationId))
        {
            $selected = $request->locationId;
        }
        else
            $selected = 0; 
        
        $activeLocations = OneWayRentalLocation::where('status',1)->get();
        $activeLocationsArray = $activeLocations->keyBy('id')->toArray();
        if(array_key_exists($selected,$activeLocationsArray))
        {
            $tableTitle = "One Way rentals for ".$activeLocationsArray[$selected]['name'];
        }
        else
        {
            $tableTitle = "";
        }

        if(isset($request->booking_id))
        {
            $bookingsByLocation = OneWayRentalBooking::where('reference_id',$request->booking_id)->get();
            $tableTitle = "Search results for ".$request->booking_id;
        }
        else
        {
            if($selected == 0)
            {
                $bookingsByLocation = OneWayRentalBooking::where('status','Booking Made')->get();
                $tableTitle = "One Way rentals for all locations";
            }
            else
            {
                $bookingsByLocation = OneWayRentalBooking::where('status','Booking Made')->where('from_location',$selected)->get();
            }
            
        }
        $documentsByUsersObj = UserDocImage::whereIn('user_id',$bookingsByLocation->pluck('user_id'))->get();
        
        $documentsByUsers = $documentsByUsersObj->groupBy('user_id');

        $documentsByUsers->toArray();

        $users = User::whereIn('id',$bookingsByLocation->pluck('user_id'))->get()->keyBy('id');

        foreach($bookingsByLocation as $booking)
        {

            if(isset($documentsByUsers[$booking->user_id]))
            {

                $booking['user_document'] = $documentsByUsers[$booking->user_id]; 
            }
            else
            {
                $booking['user_document'] = [];
            }

            if($users[$booking->user_id]->work_email == "")
            {
                $booking['work_email_verified'] = "No";
            }
            else
            {
                $booking['work_email_verified'] = "Yes";
            }

            $booking['user_doc_notes'] = $users[$booking->user_id]->user_doc_notes;
            
            $booking['user_amount_pending'] = $users[$booking->user_id]->getA2BBalance();

        }
        
        $booking_or_movement = "booking";
        
        return view('a2b.deliveries.index',compact('bookingsByLocation','activeLocations','selected','tableTitle','booking_or_movement'));
        
    }
    
    public function getMovementDeliveriesForOps(Request $request)
    {
        if(isset($request->locationId))
        {
            $selected = $request->locationId;
        }
        else
            $selected = 0; 
        
        $activeLocations = OneWayRentalLocation::where('status',1)->get();
        $activeLocationsArray = $activeLocations->keyBy('id')->toArray();
        if(array_key_exists($selected,$activeLocationsArray))
        {
            $tableTitle = "Movements for ".$activeLocationsArray[$selected]['name'];
        }
        else
        {
            $tableTitle = "";
        }

        if(isset($request->booking_id))
        {
            $bookingsByLocation = OneWayRentalMovementOrder::where('id',$request->booking_id)->get();
            $tableTitle = "Search results for ".$request->booking_id;
        }
        else
        {
            if($selected == 0)
            {
                $bookingsByLocation = OneWayRentalMovementOrder::where('status','Movement Order Created')->get();
                $tableTitle = "One Way rentals for all locations";
            }
            else
            {
                $bookingsByLocation = OneWayRentalMovementOrder::where('status','Movement Order Created')->where('from_location',$selected)->get();
            }
            
        }
        
        foreach($bookingsByLocation as $booking)
        {
            $booking['user_amount_pending'] = 0;
        }
        
        $booking_or_movement = "movement";
        
        return view('a2b.deliveries.index',compact('bookingsByLocation','activeLocations','selected','tableTitle','booking_or_movement'));
    }
    
    
    public function deliverBike(Request $request)
    {
        $opsapp=$request->opsapp;
        if($request->booking_or_movement == "booking")
            $booking = OneWayRentalBooking::where('id',$request->id)->first();
        else
            $booking = OneWayRentalMovementOrder::where('id',$request->id)->first();
        
        
        if(!$booking)
        {
            if(isset($opsapp))
            {
                return $this->respondNotFound();
            }
            
            return redirect()->back()->withErrors(['Booking not found.']);
        }
        else
        {
            
            if($booking->status == "Booking Made" || $booking->status == "Movement Order Created")
            {
                if($request->booking_or_movement == "movement")
                {
                    $imagePrefix = "movement ";
                    $userId = 1;
                }
                else
                {
                    $imagePrefix = "";
                    $userId = $booking->user_id;
                }
                
                if(isset($request->image))
                {
                    foreach($request->image as $key=>$index)
                    {

                        $docImageDestinationPath = 'img/bike/a2bbooking/';

                        $createImage = Self::base64_with_header_to_image($request['imageBase64'.$index],$docImageDestinationPath);


                        if($createImage['success'])
                        {
                            DB::table('a2b_booking_images')->insert([
                                'user_id' =>$userId, 
                                'booking_id' => $booking->id,
                                'image_url' => $createImage['image'],
                                'type'=>$imagePrefix."bike",
                                'created_by' => Auth::user()->id
                            ]);
                        }
                    }
                }
                if(isset($request->fuelimage))
                {

                    foreach($request->fuelimage as $key=>$index)
                    {

                        $docImageDestinationPath = 'img/fuel/a2bbooking/';

                        $createImage = Self::base64_with_header_to_image($request['fuelimageBase64'.$index],$docImageDestinationPath);


                        if($createImage['success'])
                        {
                            DB::table('a2b_booking_images')->insert([
                                'user_id' =>$userId, 
                                'booking_id' => $booking->id,
                                'image_url' => $createImage['image'],
                                'type'=>$imagePrefix."fuel",
                                'created_by' => Auth::user()->id
                            ]);
                        }
                    }
                }
                
                
                
                if($request->booking_or_movement == "booking")
                {
                        
                    OneWayRentalBooking::where('id',$request->id)->update([
                        'updated_by'=>Auth::user()->id,
                        'status'=>"Bike delivered",
                        'delivered_by'=>Auth::user()->id,
                        'delivered_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                        'start_odo'=>$request->start_odo,
                        'delivery_ending_odo'=>$request->delivery_ending_odo
                    ]);
                    
                    $pendingBalance = $booking->user->getA2BBalance();
                    
                    if($pendingBalance > 0)
                    {
                        OneWayRentalFinancialInfo::create([
                            'booking_id'=>$booking->id,
                            'user_id'=>$booking->user_id,
                            'debit_or_credit'=>"C",
                            'new_or_edit'=>"E",
                            'amount'=>$pendingBalance,
                            'payment_method'=>$request->payment_method,
                            'payment_id'=>$request->payment_id,
                            'payment_nature'=>1,
                            'status'=>"Paid",
                            'created_by'=>Auth::user()->id
                        ]);
                    }
                    
                }
                else
                {
                    if($booking->priority == 0)
                    {
                        $bike = OneWayRentalBike::where('id',$booking->bike_id)->first();
    
                        OneWayRentalBike::where('id',$bike->id)->update([
                            'current_location'=>0, 
                            'current_location_latitude'=>0, 
                            'current_location_longitude'=>0, 
                            'updated_by'=>Auth::user()->id,
                            'current_status'=>"Bike blocked for movement"
                        ]);
                    }
                    
                    $booking = OneWayRentalMovementOrder::where('id',$request->id)->update([
                        'updated_by'=>Auth::user()->id,
                        'status'=>"Bike on the move",
                        'delivered_by'=>Auth::user()->id,
                        'delivered_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                        'start_odo'=>$request->start_odo
                    ]);
                }
                
                if(isset($opsapp))
                {
                    return $this->respondWithSuccess('Booking successfully delivered');

                }    
    
                return redirect()->back()->withErrors(['Booking successfully delivered']);
            }
            elseif($booking->status == "Bike delivered" )
            {
                if(isset($opsapp))
                {
                 return $this->respondWithError('Booking cannot be delivered.');
                }

                return redirect()->back()->withErrors(['Booking cannot be delivered.']);
            }
            else
            {
                if(isset($opsapp))
                {
                 return $this->respondWithError('Booking cannot be delivered.');
                }

                return redirect()->back()->withErrors(['Booking cannot be delivered.']);
            }
        }
    }
    
    
    public function getreturnsForOps(Request $request)
    {
        
        if(isset($request->locationId))
        {
            $selected = $request->locationId;
        }
        else
            $selected = 0; 
        
        $activeLocations = OneWayRentalLocation::where('status',1)->get();
        $activeLocationsArray = $activeLocations->keyBy('id')->toArray();
        if(array_key_exists($selected,$activeLocationsArray))
        {
            $tableTitle = "One Way rentals for ".$activeLocationsArray[$selected]['name'];
        }
        else
        {
            $tableTitle = "";
        }

        if(isset($request->booking_id))
        {
            $bookingsByLocation = OneWayRentalBooking::where('reference_id',$request->booking_id)->get();
            $tableTitle = "Search results for ".$request->booking_id;
        }
        else
        {
            if($selected == 0)
            {
                $bookingsByLocation = OneWayRentalBooking::where('status','Bike delivered')->get();
                $tableTitle = "One Way rentals for all locations";
            }
            else
            {
                $bookingsByLocation = OneWayRentalBooking::where('status','Bike delivered')->where('to_location',$selected)->get();
            }
            
        }
        
        foreach($bookingsByLocation as $booking)
        {
            $bikeImages = DB::table('a2b_booking_images')->where('type','bike')->where('booking_id',$booking->id)->get();
            $fuelImages = DB::table('a2b_booking_images')->where('type','fuel')->where('booking_id',$booking->id)->get();
            
            $bikeImagesUrls = [];
            $fuelImagesUrls = [];
            
            foreach($bikeImages as $image)
            {
                array_push($bikeImagesUrls,$image->image_url);
            }
            foreach($fuelImages as $image)
            {
                array_push($fuelImagesUrls,$image->image_url);
            }
            
            $booking['bike_images'] = implode('@',$bikeImagesUrls);
            $booking['fuel_images'] = implode('@',$fuelImagesUrls);
            
        }
        
        $booking_or_movement = "booking";
        
        return view('a2b.returns.index',compact('bookingsByLocation','activeLocations','selected','tableTitle','booking_or_movement'));
        
    }
    
    public function getMovementReturnsForOps(Request $request)
    {
        if(isset($request->locationId))
        {
            $selected = $request->locationId;
        }
        else
            $selected = 0; 
        
        $activeLocations = OneWayRentalLocation::where('status',1)->get();
        $activeLocationsArray = $activeLocations->keyBy('id')->toArray();
        if(array_key_exists($selected,$activeLocationsArray))
        {
            $tableTitle = "One Way rentals for ".$activeLocationsArray[$selected]['name'];
        }
        else
        {
            $tableTitle = "";
        }

        if(isset($request->booking_id))
        {
            $bookingsByLocation = OneWayRentalMovementOrder::where('reference_id',$request->booking_id)->get();
            $tableTitle = "Search results for ".$request->booking_id;
        }
        else
        {
            if($selected == 0)
            {
                $bookingsByLocation = OneWayRentalMovementOrder::where('status','Bike on the move')->get();
                $tableTitle = "One Way rentals for all locations";
            }
            else
            {
                $bookingsByLocation = OneWayRentalMovementOrder::where('status','Bike on the move')->where('to_location',$selected)->get();
            }
        }
        
        foreach($bookingsByLocation as $booking)
        {
            $bikeImages = DB::table('a2b_booking_images')->where('type','movement bike')->where('booking_id',$booking->id)->get();
            $fuelImages = DB::table('a2b_booking_images')->where('type','movement fuel')->where('booking_id',$booking->id)->get();
            
            $bikeImagesUrls = [];
            $fuelImagesUrls = [];
            
            foreach($bikeImages as $image)
            {
                array_push($bikeImagesUrls,$image->image_url);
            }
            foreach($fuelImages as $image)
            {
                array_push($fuelImagesUrls,$image->image_url);
            }
            
            $booking['bike_images'] = implode('@',$bikeImagesUrls);
            $booking['fuel_images'] = implode('@',$fuelImagesUrls);
            
        }
        
        $booking_or_movement = "movement";
        
        return view('a2b.returns.index',compact('bookingsByLocation','activeLocations','selected','tableTitle','booking_or_movement'));
        
        
    }
    
    
    public function returnBike(Request $request)
    {

        $opsapp=$request->opsapp;
        if($request->booking_or_movement == "booking")
            $booking = OneWayRentalBooking::where('id',$request->id)->first();
        else
            $booking = OneWayRentalMovementOrder::where('id',$request->id)->first();
        
        
        if(!$booking)
        {
            if(isset($opsapp))
            {
                return $this->respondNotFound();
            }
            return redirect()->back()->withErrors(['Booking not found.']);
        }
        else
        {
            if($booking->start_odo > $request->end_odo) {
                if(isset($opsapp))
                {
                 return $this->respondWithError('Start Odo cannot be greater than End Odo.');
                }
                return redirect()->back()->withErrors(['Start Odo cannot be greater than End Odo.']);
            }
        
            else
            {
                if($booking->status == "Bike delivered" || $booking->status == "Bike on the move")
                {
                    
                    if($request->booking_or_movement == "booking")
                    {
                        OneWayRentalBooking::where('id',$request->id)->update([
                            'updated_by'=>Auth::user()->id,
                            'status'=>"Bike returned",
                            'returned_by'=>Auth::user()->id,
                            'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                            'end_odo'=>$request->end_odo,
                            'return_ending_odo'=>$request->return_ending_odo
                        ]);
                        
                        $movementReason = "Booking";
                        $bookingReferenceId = $booking->reference_id;
                        
                        Self::cashbackForOffPeakHours($booking);
                        
                    }
                    else
                    {
                        OneWayRentalMovementOrder::where('id',$request->id)->update([
                            'updated_by'=>Auth::user()->id,
                            'status'=>"Bike returned",
                            'returned_by'=>Auth::user()->id,
                            'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                            'end_odo'=>$request->end_odo
                        ]);
                        
                        $movementReason = "Movement";
                        $bookingReferenceId = $booking->id;
                    }
                    
                    OneWayRentalMovementLog::create([
                        'bike_id'=>$booking->bike_id, 
                        'model_id'=>$booking->model_id, 
                        'from_location'=>$booking->from_location, 
                        'to_location'=>$booking->to_location, 
                        'reason'=>$movementReason, 
                        'ref_id'=>$bookingReferenceId,
                        'start_datetime'=>$booking->delivered_at,
                        'end_datetime'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                    ]);

                    OneWayRentalBike::where('id',$booking->bike_id)->update([
                        'current_location'=>$booking->to_location,
                        'current_location_latitude'=>$booking->to_location_latitude,
                        'current_location_longitude'=>$booking->to_location_longitude,
                        'current_status'=>"Available for booking"
                    ]);

                    if($request->booking_or_movement == "booking")
                    {
                        $fromDateTimeCarbon = Carbon::parse($booking->delivered_at);
                        $todateTimeCarbon = Carbon::now('Asia/Kolkata');
                        $diffInMinutes = $todateTimeCarbon->diffInMinutes($fromDateTimeCarbon);
                        $diffInKM = $request->end_odo - $booking->start_odo;

                        $priceObj = OneWayRentalPrice::where('model_id',$booking->model_id)->first();
                        $finalPrice = ($priceObj->price_per_km*$diffInKM)+($priceObj->price_per_min*$diffInMinutes);

                        $oldPricingInfo = $booking->pricingInfo()->where('deleted',0)->first();

                        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];

                        $finalTaxablePrice = ($finalPrice - $oldPricingInfo->promotional_balance_used);
                        $finalTax = round($finalTaxablePrice*($taxDetails->amount/100),2);

                        $finalPriceWithTax = $finalPrice + $finalTax;

                        $extraAmount = $finalPriceWithTax - $oldPricingInfo->price_after_tax;

                        if($extraAmount != 0 && $request->booking_or_movement == "booking")
                        {

                            OneWayRentalPricingInfo::where('booking_id',$booking->id)->where('deleted',0)->update([
                                'deleted'=>1,
                                'deleted_by'=>Auth::user()->id,
                                'deleted_at'=>Carbon::now()->toDateTimeString(),
                            ]);

                            OneWayRentalPricingInfo::create([
                                'booking_id'=>$booking->id,
                                'pricing_id'=>$priceObj->id,
                                'distance'=>$diffInKM,
                                'duration'=>$diffInMinutes,
                                'full_price'=>$finalPrice,
                                'promo_discount'=> 0,
                                'price_after_discount'=>$finalPrice,
                                'promo_code'=>"",
                                'tax_id'=>2,
                                'promotional_balance_used'=>$oldPricingInfo->promotional_balance_used,
                                'taxable_price'=>$finalTaxablePrice,
                                'tax_amount'=>$finalTax,
                                'price_after_tax'=>$finalPriceWithTax
                            ]);
                            
                            OneWayRentalFinancialInfo::create([
                                'booking_id'=>$booking->id,
                                'user_id'=>$booking->user_id,
                                'debit_or_credit'=>"C",
                                'new_or_edit'=>"E",
                                'amount'=>$oldPricingInfo->price_after_discount,
                                'payment_method'=>0,
                                'payment_id'=>"na",
                                'payment_nature'=>7,
                                'status'=>"na",
                                'created_by'=>Auth::user()->id
                            ]);
                            
                            OneWayRentalFinancialInfo::create([
                                'booking_id'=>$booking->id,
                                'user_id'=>$booking->user_id,
                                'debit_or_credit'=>"C",
                                'new_or_edit'=>"E",
                                'amount'=>$oldPricingInfo->tax_amount,
                                'payment_method'=>0,
                                'payment_id'=>"na",
                                'payment_nature'=>8,
                                'status'=>"na",
                                'created_by'=>Auth::user()->id
                            ]);
                            
                            OneWayRentalFinancialInfo::create([
                                'booking_id'=>$booking->id,
                                'user_id'=>$booking->user_id,
                                'debit_or_credit'=>"D",
                                'new_or_edit'=>"E",
                                'amount'=>$finalPrice,
                                'payment_method'=>0,
                                'payment_id'=>"na",
                                'payment_nature'=>5,
                                'status'=>"na",
                                'created_by'=>Auth::user()->id
                            ]);
                            
                            OneWayRentalFinancialInfo::create([
                                'booking_id'=>$booking->id,
                                'user_id'=>$booking->user_id,
                                'debit_or_credit'=>"D",
                                'new_or_edit'=>"E",
                                'amount'=>$finalTax,
                                'payment_method'=>0,
                                'payment_id'=>"na",
                                'payment_nature'=>6,
                                'status'=>"na",
                                'created_by'=>Auth::user()->id
                            ]);

                            if($extraAmount > 0)
                            {
                                $debitOrCredit = "D";
                            }
                            else
                            {
                                $debitOrCredit = "C";
                            }

                            if($extraAmount > 0)
                            {

                                OneWayRentalFinancialInfo::create([
                                    'booking_id'=>$booking->id,
                                    'user_id'=>$booking->user_id,
                                    'debit_or_credit'=>"C",
                                    'new_or_edit'=>"E",
                                    'amount'=>abs($extraAmount),
                                    'payment_method'=>1,
                                    'payment_id'=>"na",
                                    'payment_nature'=>38,
                                    'status'=>"na",
                                    'created_by'=>Auth::user()->id
                                ]);


                                if(isset($opsapp))
                                {
                                 return $this->respondWithSuccess("Booking returned with money","This reservation has an extra charge. ".$diffInKM." KM and ".$diffInMinutes." Minutes done. Extra charge of ₹".$extraAmount." to be collected.");
                                }

                                return redirect()->back()->withErrors(["Booking returned with money","This reservation has an extra charge. ".$diffInKM." KM and ".$diffInMinutes." Minutes done. Extra charge of ₹".$extraAmount." to be collected."]);
                            }
                            else
                            {
                                $dispatcher = app('Dingo\Api\Dispatcher');
                                $addWalletBalance = $dispatcher->with([
                                    'user_email_add' => $booking->user->email,
                                    'amount_add' => abs($extraAmount),
                                    'booking_id_add'=>$booking->id,
                                    'nature_add' => 'Refunded for extra money in one way rental',
                                    'notes_add' => "",
                                    'onewayrental'=>"yes"
                                ])->get('wallet/add_amount_int_new');
                                
                                OneWayRentalFinancialInfo::create([
                                    'booking_id'=>$booking->id,
                                    'user_id'=>$booking->user_id,
                                    'debit_or_credit'=>"D",
                                    'new_or_edit'=>"E",
                                    'amount'=>abs($extraAmount),
                                    'payment_method'=>13,
                                    'payment_id'=>$addWalletBalance['result']['data']['wallet_txn_id'],
                                    'payment_nature'=>38,
                                    'status'=>"na",
                                    'created_by'=>Auth::user()->id
                                ]);
                            }

                        }
                    }
                 
                     if(isset($opsapp))
                    {
                     return $this->respondWithSuccess('Booking successfully returned.');
                    }
                    return redirect()->back()->withErrors(['Booking successfully returned','Booking successfully returned']);
                }
                elseif($booking->status == "Bike returned" )
                {
                    if(isset($opsapp))
                    {
                     return $this->respondWithError('Booking cannot be returned.');
                    }

                    return redirect()->back()->withErrors(['Booking cannot be returned.','Booking cannot be returned.']);
                }
                else
                {
                    if(isset($opsapp))
                    {
                     return $this->respondWithError('Booking cannot be returned.');
                    }
                    return redirect()->back()->withErrors(['Booking cannot be returned.','Booking cannot be returned.']);
                }
            }
        }
    }
    
    
    public function movementDeliverBike(Request $request)
    {
        $movementOrder = OneWayRentalMovementOrder::where('id',$request->id)->first();
        if(!$movementOrder)
            return redirect()->back()->withErrors(['Booking not found.']);
        else
        {
            if($movementOrder->status == "Booking Made")
            {
                $movementOrder = OneWayRentalMovementOrder::where('id',$request->id)->update([
                    'updated_by'=>Auth::user()->id,
                    'status'=>"Bike delivered",
                    'delivered_by'=>Auth::user()->id,
                    'delivered_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                ]);
                return redirect()->back()->withErrors(['Booking successfully delivered']);
            }
            elseif($movementOrder->status == "Bike delivered" )
            {
                return redirect()->back()->withErrors(['Booking cannot be delivered.']);
            }
            else
            {
                return redirect()->back()->withErrors(['Booking cannot be delivered.']);
            }
        }
    }
    
    
    
    
    
    
    public function movementReturnBike(Request $request)
    {
        $movementOrder = OneWayRentalMovementOrder::where('id',$request->id)->first();
        if(!$movementOrder)
            return redirect()->back()->withErrors(['Booking not found.']);
        else
        {
            if($movementOrder->status == "Bike delivered")
            {
                OneWayRentalMovementOrder::where('id',$request->id)->update([
                    'updated_by'=>Auth::user()->id,
                    'status'=>"Bike returned",
                    'returned_by'=>Auth::user()->id,
                    'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                ]);
                OneWayRentalMovementLog::create([
                    'bike_id'=>$movementOrder->bike_id, 
                    'model_id'=>$movementOrder->model_id, 
                    'from_location'=>$movementOrder->from_location, 
                    'to_location'=>$movementOrder->to_location, 
                    'reason'=>"Movement", 
                    'ref_id'=>$movementOrder->reference_id,
                    'start_datetime'=>$movementOrder->delivered_at,
                    'end_datetime'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
                ]);
                
                OneWayRentalBike::where('id',$movementOrder->bike_id)->update([
                    'current_location'=>$movementOrder->to_location,
                    'current_location_latitude'=>$movementOrder->to_location_latitude,
                    'current_location_longitude'=>$movementOrder->to_location_longitude,
                    'current_status'=>"Available for booking"
                ]);
                
                return redirect()->back()->withErrors(['Booking successfully returned']);
            }
            elseif($movementOrder->status == "Bike returned" )
            {
                return redirect()->back()->withErrors(['Booking cannot be returned.']);
            }
            else
            {
                return redirect()->back()->withErrors(['Booking cannot be returned.']);
            }
        }
    }
    
    function base64_with_header_to_image($base64_string,$docImageDestinationPath) {
        
        $str1 = explode(",",$base64_string);
        if(!isset($str1[1]))
        {
            return ["success"=>false, "image"=>""];
        }
        
        $str2 = explode(";",$str1[0]);
        $str3 = explode("/",$str2[0]);
        $output_file = base64_decode($str1[1]);
         
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
        $docImageHashName = $str.'.'.$str3[1];
        
        $s3 = \Storage::disk('s3');
        $filePath = $docImageDestinationPath.$docImageHashName;
        $s3->put($filePath, $output_file, 'public');
        
        
        if($s3)
            $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
        else
            $image = "";
        
        return ["success"=>true, "image"=>$image];  
    }    
    
    
    
    public function reportIndex(Request $request)
    {
        if(!isset($request->start_date))
        {
            $reportData = ['bikes'=>[]];
            return view('a2b.a2bReport.index',compact('reportData'));
        }
        $bookings = OneWayRentalBooking::where('created_at','>',$request->start_date." 00:00:00")->where('created_at','<',$request->end_date." 23:59:59")->where('status',"Bike returned")->get();
        $bookingsByBikes = $bookings->groupBy('bike_id');
        
        $reportData = [];
        $reportData['bikes'] = [];
        
        foreach($bookingsByBikes as $bikeId=>$bookings)
        {
            $revenue = 0;
            $kmDone = 0;
            $hours = 0;
            
            foreach($bookings as $booking)
            {
                $revenue += $booking->pricingInfo()->where('deleted',0)->first()->price_after_tax;
                $kmDone += ($booking->end_odo - $booking->start_odo);
                $hours += Carbon::parse($booking->returned_at)->diffInMinutes(Carbon::parse($booking->delivered_at));
            }
            $count = count($bookings);
            
            $bike = $bookings->first()->bike;
            
            if(!$bike)
            {
                $number = "Bike Deleted";
                $area = "Bike Deleted";
            }
            else
            {
                $number = $bookings->first()->bike->bike_number;
                $area = $bookings->first()->bike->getCurrentLocation();
            }
            
            
            $data = [
                "number" =>$number,
                "bookings_count"=>$count,
                "total_revenue"=>$revenue,
                "km_done"=>$kmDone,
                "hours_rented"=>round($hours/60,1),
                "area"=>$area
            ];
            
            array_push($reportData['bikes'],$data);
        }
        
        return view('a2b.a2bReport.index',compact('reportData'));
        
        
        
        
    }
    
    
    public function assignSupervisor(Request $request)
    {
        
        $user =UserController::getUserByToken();

        $idSplit = explode('@',$request->id);
        $type=$idSplit[0];
        $id=$idSplit[1];
        
        if($type=="booking")
        {
            $bookingOrder = OneWayRentalBooking::where('id',$id)->first();
           // echo $bookingOrder;
            if(!$bookingOrder)
                return $this->respondNotFound();
            else
            {
                if($bookingOrder->status == "Booking Made")
                {
                    $bookingOrder = OneWayRentalBooking::where('id',$id)->update([
                        'delivery_assigned_to'=>$user->id,
                        'delivery_assigned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                    ]);
                return $this->respondWithSuccess('Updated');
                }
                elseif($bookingOrder->status == "Bike delivered" )
                {
                    $bookingOrder = OneWayRentalBooking::where('id',$id)->update([
                        'return_assigned_to'=>$user->id,
                        'return_assigned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                    ]);
                return $this->respondWithSuccess('Updated');            }
                else
                {
                    return redirect()->back()->withErrors(['Booking cannot be delivered.']);
                }
            }
        }
        else
        {

            $movementOrder = OneWayRentalMovementOrder::where('id',$id)->first();
           // echo $bookingOrder;
            if(!$movementOrder)
                return $this->respondNotFound();
            else
            {
                if($movementOrder->status == "Movement Order Created")
                {
                    $movementOrder = OneWayRentalMovementOrder::where('id',$id)->update([
                        'delivery_assigned_to'=>$user->id,
                        'delivery_assigned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                    ]);
                return $this->respondWithSuccess('Updated');
                }
                else
                {
                    $movementOrder = OneWayRentalMovementOrder::where('id',$id)->update([
                        'return_assigned_to'=>$user->id,
                        'return_assigned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
                    ]);
                return $this->respondWithSuccess('Updated');            }
                
            }

        }




    }


    public function cancelOneWayRentalBooking(Request $request)
    {
         $user =UserController::getUserByToken();

         $idSplit = explode('@',$request->id);
        $type=$idSplit[0];
        $id=$idSplit[1];

        if($type=="booking")
        {
        $booking = OneWayRentalBooking::where('id',$id)->first();
        if(!$booking)
                return $this->respondNotFound();

        
        OneWayRentalBooking::where('id',$id)->update([
            'updated_by'=>$user->id,
            'status'=>"Booking Cancelled",
            'customer_instructions'=>"Cancel Reason : ".$request->reason,
            'returned_by'=>$user->id,
            'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
        ]);
        OneWayRentalBike::where('id',$booking->bike_id)->update([
            'current_location'=>$booking->from_location,
            'current_location_latitude'=>$booking->from_location_latitude,
            'current_location_longitude'=>$booking->from_location_longitude,
            'current_status'=>"Available for booking"
        ]);
        
        
        $pricingInfo = OneWayRentalPricingInfo::where('booking_id',$booking->id)->where('deleted',0)->first();
        
        if(!(!$pricingInfo))
        {
            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"C",
                'new_or_edit'=>"D",
                'amount'=>$pricingInfo->price_after_discount,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>10,
                'status'=>"na",
                'created_by'=>$user->id
            ]);

            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"C",
                'new_or_edit'=>"D",
                'amount'=>$pricingInfo->tax_amount,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>11,
                'status'=>"na",
                'created_by'=>$user->id
            ]);
        }        
        
                return $this->respondWithSuccess('Cancelled');   
         }
         else
         {
            return $this->respondNotFound();
         }     
    }




    public function getA2BHomeScreenList(Request $request)
    {
        if(isset($request->area_id))
        {
            $selectedAreaIds = array($request->area_id);
        }
        else
        {
            $selectedAreaIds = OneWayRentalLocation::get()->pluck('id')->all();
        }
        
        $screenList = [];
        
        $bookings = OneWayRentalBooking::whereIn('from_location',$selectedAreaIds)->whereIn('status',array("Booking Made","Bike delivered"))->orderBy('created_at','desc')->with(['fromArea','toArea','user'])->get();
        //print_r($bookings);
        //exit;
        foreach($bookings as $booking)
        {
            $screenListEl = [];
            $screenListEl['id'] = "booking@".$booking->id;
            $screenListEl['reference_id'] = $booking->reference_id;
            $screenListEl['created_at'] = $booking->created_at;
            $screenListEl['booking_or_movement'] = "booking";
            $screenListEl['customer_name'] = $booking->user->first_name." ".$booking->user->last_name;
            $screenListEl['user_image']="";
            $screenListEl['customer_phone'] = $booking->user->mobile_num;
            
            $screenListEl['user_location_delivery'] = false;
            $screenListEl['user_location_return'] = false;


            
            if($booking->status == "Booking Made")
            {
                $screenListEl['delivery_or_return'] = "delivery";
                
                if($booking->delivery_assigned_to=='0')
                {
                    $screenListEl['assigned_status']=false;
                }
                else
                {
                    $screenListEl['assigned_status']=true;
                    $screenListEl['assigned_to']=User::find($booking->delivery_assigned_to)->name();
                    $screenListEl['assigned_person_image']="";
                }
                if(($booking->from_location_longitude != $booking->fromArea->longitude)&&($booking->from_location_latitude != $booking->fromArea->latitude))
                {
                    $screenListEl['user_location_delivery'] = true;
                }
                $screenListEl['location_id'] = $booking->fromArea->id;
                $screenListEl['location_name'] = $booking->fromArea->name;
            }
            else
            {
                $screenListEl['delivery_or_return'] = "return";

                if($booking->return_assigned_to=='0')
                {
                    $screenListEl['assigned_status']=false;
                }
                else
                {
                    $screenListEl['assigned_status']=true;
                    $screenListEl['assigned_to']=User::find($booking->return_assigned_to)->name();
                    $screenListEl['assigned_person_image']="";

                }


                if(($booking->to_location_longitude != $booking->toArea->longitude)&&($booking->to_location_latitude != $booking->toArea->latitude))
                {
                    $screenListEl['user_location_delivery'] = true;
                }
                $screenListEl['location_id'] = $booking->toArea->id;
                
                $screenListEl['location_name'] = $booking->toArea->name;

               // array_push($screenListEl, Self::getBookingDetails($booking->id));

            }
            $screenListEl['details']=Self::getBookingDetails($booking->id);
            
            array_push($screenList,$screenListEl);
        }
        
        $movements = OneWayRentalMovementOrder::whereIn('from_location',$selectedAreaIds)->whereIn('status',array("Movement Order Created","Bike on the move"))->orderBy('created_at','desc')->with(['fromArea','toArea'])->get();
        
        foreach($movements as $movement)
        {
            $screenListEl = [];
            $screenListEl['id'] = "movement@".$movement->id;
            $screenListEl['reference_id'] = $movement->id;
            $screenListEl['created_at'] = $movement->created_at;
            $screenListEl['booking_or_movement'] = "movement";
            $screenListEl['customer_name'] = "";
            $screenListEl['customer_phone'] = "";
            
            $screenListEl['user_location_delivery'] = false;
            $screenListEl['user_location_return'] = false;
            
            if($movement->status == "Movement Order Created")
            {

                if($movement->delivery_assigned_to=='0')
                {
                    $screenListEl['assigned_status']=false;
                }
                else
                {
                    $screenListEl['assigned_status']=true;
                    $screenListEl['assigned_to']=User::find($movement->delivery_assigned_to)->name();
                    $screenListEl['assigned_person_image']="http://www.metrobikes.in/user/images/user-icon-helmet.png";

                }

                $screenListEl['delivery_or_return'] = "delivery";
                $screenListEl['location_id'] = $movement->fromArea->id;
                $screenListEl['location_name'] = $movement->fromArea->name;
            }
            else
            {
                if($movement->return_assigned_to=='0')
                {
                    $screenListEl['assigned_status']=false;
                }
                else
                {
                    $screenListEl['assigned_status']=true;
                    $screenListEl['assigned_to']=User::find($movement->return_assigned_to)->name();
                    $screenListEl['assigned_person_image']="http://www.metrobikes.in/user/images/user-icon-helmet.png";

                }

                $screenListEl['delivery_or_return'] = "return";
                $screenListEl['location_id'] = $movement->toArea->id;
                $screenListEl['location_name'] = $movement->toArea->name;
            }
            $screenListEl['details']=Self::getMovementDetails($movement->id);
            array_push($screenList,$screenListEl);
        }
        
        usort($screenList, array($this, 'my_sort_function'));
        
        return $this->respondWithSuccess($screenList); 
    }
    
    function my_sort_function($a, $b)
    {
        return $a['created_at'] < $b['created_at'];
    }
    
    public function getA2Bdetail(Request $request)
    {
        $rules = array(
            'id' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        
        $idSplit = explode('@',$request->id);
        if($idSplit[0] == "booking")
        {
            $details = Self::getBookingDetails($idSplit[1]);
        }
        elseif($idSplit[0] == "movement")
        {
            $details = Self::getMovementDetails($idSplit[1]);
        }
        else
        {
            return redirect()->back()->withErrors(["Details for ID not found"]);
        }
        
        
        return $this->respondWithSuccess($details);
    }
    
    function getBookingDetails($id)
    {
        $details = [];
        
        $booking = OneWayRentalBooking::find($id);
        if(!$booking)
        {
            return redirect()->back()->withErrors(["Details for ID not found"]);
        }
        $details['customer_name'] = $booking->user->first_name." ".$booking->user->last_name;
        $details['customer_phone'] = $booking->user->mobile_num;

        $details['bike_number'] = $booking->bike->bike_number;
        
        $details['status'] = $booking->status;
        
        $details['from_location_latitude'] = $booking->from_location_latitude;
        $details['from_location_longitude'] = $booking->from_location_longitude;
        
        $details['bike_location_latitude'] = $booking->fromArea->latitude;
        $details['bike_location_longitude'] = $booking->fromArea->longitude;
        
        $details['to_location_latitude'] = $booking->to_location_latitude;
        $details['to_location_longitude'] = $booking->to_location_longitude;
        
        $details['nearest_store_to_dropoff_location_latitude'] = $booking->toArea->latitude;
        $details['nearest_store_to_dropoff_location_longitude'] = $booking->toArea->longitude;
        
        
        if($booking->delivered_by != 0)
        {
            $details['delivered'] = true;
            $details['delivered_by'] = $booking->deliveredBy->email;
            $details['start_odo'] = $booking->start_odo;
            $details['start_fuel'] = $booking->deliveryInfo?$booking->deliveryInfo->starting_fuel:"";
            $details['helmets_given'] = $booking->deliveryInfo?$booking->deliveryInfo->helmets_given:"";
        }
        else
        {
            $details['delivered'] = false;
            $details['delivered_by'] = "";
            $details['start_odo'] = 0;
            $details['start_fuel'] = "";
            $details['helmets_given'] = 0;
        }
        
        $details['delivery_images'] = $booking->bookingImages()->where('type','bike_delivery')->get(['id','image_url']);
        
        if($booking->returned_by != 0)
        {
            $details['returned'] = true;
            $details['returned_by'] = $booking->returnedBy->email;
            $details['end_odo'] = $booking->end_odo;
            $details['end_fuel'] = $booking->returnInfo?$booking->returnInfo->ending_fuel:"";
            $details['helmets_renturned'] = $booking->returnInfo?$booking->returnInfo->helmets_returned:"";
        }
        else
        {
            $details['returned'] = false;
            $details['returned_by'] = "";
            $details['end_odo'] = 0;
            $details['end_fuel'] = "";
            $details['helmets_renturned'] = 0;
        }
        
        $details['return_images'] = $booking->bookingImages()->where('type','bike_return')->get(['id','image_url']);
        
        $details['user_documents'] = $booking->user->getDocuments();
        
        $details['estimated_distance'] = $booking->estimated_distance;
        
        $details['estimated_duration'] = $booking->estimated_duration;
        
        $a2bBalance = $booking->user->getA2BBalance();
        
        if($a2bBalance > env('MINIMUMCASHTOCOLLECT'))
        {
            $details['excess_amount_to_be_collected'] = $a2bBalance;
        }
        else
        {
            $details['excess_amount_to_be_collected'] = 0;
        }
        
        return $details;
        
    }
    
    
    function getMovementDetails($id)
    {
        $details = [];
        
        $movement = OneWayRentalMovementOrder::find($id);
        if(!$movement)
        {
            return redirect()->back()->withErrors(["Details for ID not found"]);
        }
        
        $details['bike_number'] = $movement->bike->bike_number;
        
        $details['status'] = $movement->status;
        
        $details['from_location_latitude'] = $movement->from_location_latitude;
        $details['from_location_longitude'] = $movement->from_location_longitude;
        
        $details['to_location_latitude'] = $movement->to_location_latitude;
        $details['to_location_longitude'] = $movement->to_location_longitude;
        
            
        if($movement->delivered_by != 0)
        {
            $details['delivered'] = true;
            $details['delivered_by'] = $movement->deliveredBy->email;
            $details['start_odo'] = $movement->start_odo;
            
        }
        else
        {
            $details['delivered'] = false;
            $details['delivered_by'] = "";
            $details['start_odo'] = 0;
        }
        
        
        if($movement->returned_by != 0)
        {
            $details['returned'] = true;
            $details['returned_by'] = $movement->returnedBy->email;
            $details['end_odo'] = $movement->end_odo;
        }
        else
        {
            $details['returned'] = false;
            $details['returned_by'] = "";
            $details['end_odo'] = 0;
        }
        
        return $details;
        
    }
    
    public static function cashbackForOffPeakHours($booking)
    {
        $startTimeCarbon = Carbon::parse($booking->delivered_at);
                        
        if(11<=$startTimeCarbon->hour && $startTimeCarbon->hour<17)
        {
            $now = Carbon::now();
            if(11<=$now->hour && $now->hour<17)
            {
                $diff = $now->diffInMinutes($startTimeCarbon);
                $cashback = $diff*0.5;
                $dispatcher = app('Dingo\Api\Dispatcher');
                $addWalletBalance = $dispatcher->with([
                    'user_email_add' => $booking->user->email,
                    'amount_add' => $cashback,
                    'booking_id_add'=>$booking->id,
                    'nature_add' => 'Cashback for one way rental booking',
                    'notes_add' => "",
                    'onewayrental'=>"yes"
                ])->get('wallet/add_amount_int_new');
            }
        }
        
    }
    
    
    
     
}
