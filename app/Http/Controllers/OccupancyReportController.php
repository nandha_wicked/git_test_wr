<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Availability;
use App\Models\City;
use App\Models\OccupancyReport;
use App\Models\Parameters;
use App\Models\AnalyticsData;
use DB;
use App\Transformers\BookingTransformer;
use Carbon\Carbon;



class OccupancyReportController extends Controller
{
    public function update(){
        
        
        
        $report_from_date_time_str = Parameters::where('parameter_name','report_from_date_time')->first();
        $report_to_date_time_str = Parameters::where('parameter_name','report_to_date_time')->first();
        $requiredArea = Parameters::where('parameter_name','required_area')->first();
        $requiredModel = Parameters::where('parameter_name','required_model')->first();
        $report_from_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $report_from_date_time_str->parameter_value);
        $report_to_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $report_to_date_time_str->parameter_value);
        
        $areas = Area::where('status',1)->get();
        foreach ($areas as $area)
        {
                        
            if($area->area==$requiredArea->parameter_value || $requiredArea->parameter_value == 'all')
            {
            $modelIds = Bike::where('area_id', $area->id)->where('status', 1)->groupBy('model_id')->get()->lists('model_id');

            $models=BikeModel::whereIn('id',$modelIds)->get();
            
            foreach ($models as $model)
            {
                if($model->bike_model==$requiredModel->parameter_value || $requiredModel->parameter_value == 'all')
                {
                
                    $bikes = Bike::where('area_id', $area->id)->where('model_id',$model->id)->get()->lists('id');

                    $new_report_from_date_time= clone $report_from_date_time;


                    while($new_report_from_date_time<=$report_to_date_time)
                    {
                        $new_report_to_date_time = $new_report_from_date_time->addHour(); 
                        $total = Booking::where('end_datetime', '>', $new_report_from_date_time)
          ->where('start_datetime', '<', $new_report_to_date_time)->whereIn('bike_id',$bikes)->count();
                        $service = Booking::where('end_datetime', '>', $new_report_from_date_time)
          ->where('start_datetime', '<', $new_report_to_date_time)->where('user_id',2)->whereIn('bike_id',$bikes)->count();
                        $marketing = Booking::where('end_datetime', '>', $new_report_from_date_time)
          ->where('start_datetime', '<', $new_report_to_date_time)->where('user_id',3)->whereIn('bike_id',$bikes)->count();
                        $movement = Booking::where('end_datetime', '>', $new_report_from_date_time)
          ->where('start_datetime', '<', $new_report_to_date_time)->where('user_id',5)->whereIn('bike_id',$bikes)->count();
                        $admin = Booking::where('end_datetime', '>', $new_report_from_date_time)
          ->where('start_datetime', '<', $new_report_to_date_time)->where('user_id',1)->whereIn('bike_id',$bikes)->count();

                        $reservations = $total - $service - $marketing - $movement -$admin;

                        $occupancy_report_add = OccupancyReport::create(['area_name'=>$area->area, 'model_name' => $model->bike_model, 'reservations'=>$reservations, 'service'=>$service, 'marketing'=>$marketing, 'movement'=>$movement, 'admin'=>$admin,'date_time'=>$new_report_from_date_time]);
                        
                        
                        $reservation_hours = $reservations;
                        $service_hours = $service;
                        $marketing_hours = $marketing;
                        $movement_hours = $movement;
                        $admin_hours = $admin;
                        if($new_report_to_date_time->isWeekend())
                        {
                            $reservation_hours_weekday = 0;
                            $service_hours_weekday = 0;
                            $marketing_hours_weekday = 0;
                            $movement_hours_weekday = 0;
                            $admin_hours_weekday = 0;

                            $reservation_hours_weekend = $reservations;
                            $service_hours_weekend = $service;
                            $marketing_hours_weekend = $marketing;
                            $movement_hours_weekend = $movement;
                            $admin_hours_weekend = $admin;
                        }
                        else
                        {
                            $reservation_hours_weekday = $reservations;
                            $service_hours_weekday = $service;
                            $marketing_hours_weekday = $marketing;
                            $movement_hours_weekday = $movement;
                            $admin_hours_weekday = $admin;

                            $reservation_hours_weekend = 0;
                            $service_hours_weekend = 0;
                            $marketing_hours_weekend = 0;
                            $movement_hours_weekend = 0;
                            $admin_hours_weekend = 0;    
                        }

                        

                        
                        $existing_analytics_data = AnalyticsData::where('area_name',$area->area)->where('model_name',$model->bike_model)->first();
                        
                        if(isset($existing_analytics_data['area_name']))
                        {
                            $reservation_hours = $existing_analytics_data->reservation_hours + $reservation_hours;
                            $service_hours = $existing_analytics_data->service_hours + $service_hours;
                            $marketing_hours = $existing_analytics_data->marketing_hours + $marketing_hours;
                            $movement_hours = $existing_analytics_data->movement_hours + $movement_hours;
                            $admin_hours = $existing_analytics_data->admin_hours + $admin_hours;
                            $reservation_hours_weekday = $existing_analytics_data->reservation_hours_weekday + $reservation_hours_weekday;
                            $service_hours_weekday = $existing_analytics_data->service_hours_weekday + $service_hours_weekday;
                            $marketing_hours_weekday = $existing_analytics_data->marketing_hours_weekday + $marketing_hours_weekday;
                            $movement_hours_weekday = $existing_analytics_data->movement_hours_weekday + $movement_hours_weekday;
                            $admin_hours_weekday = $existing_analytics_data->admin_hours_weekday + $admin_hours_weekday;
                            $reservation_hours_weekend = $existing_analytics_data->reservation_hours_weekend + $reservation_hours_weekend;
                            $service_hours_weekend = $existing_analytics_data->service_hours_weekend + $service_hours_weekend;
                            $marketing_hours_weekend = $existing_analytics_data->marketing_hours_weekend + $marketing_hours_weekend;
                            $movement_hours_weekend = $existing_analytics_data->movement_hours_weekend + $movement_hours_weekend;
                            $admin_hours_weekend = $existing_analytics_data->admin_hours_weekend + $admin_hours_weekend;

                            
                            AnalyticsData::where('area_name',$area->area)->where('model_name',$model->bike_model)->update(['reservation_hours'=>$reservation_hours,'service_hours'=>$service_hours,'marketing_hours'=>$marketing_hours,'movement_hours'=>$movement_hours,'admin_hours'=>$admin_hours,'reservation_hours_weekday'=>$reservation_hours_weekday,'service_hours_weekday'=>$service_hours_weekday,'marketing_hours_weekday'=>$marketing_hours_weekday,'movement_hours_weekday'=>$movement_hours_weekday,'admin_hours_weekday'=>$admin_hours_weekday,'reservation_hours_weekend'=>$reservation_hours_weekend,'service_hours_weekend'=>$service_hours_weekend,'marketing_hours_weekend'=>$marketing_hours_weekend,'movement_hours_weekend'=>$movement_hours_weekend,'admin_hours_weekend'=>$admin_hours_weekend]);
                            
                            
                            
                        }
                        
                        else
                        {
                            AnalyticsData::create(['area_name'=>$area->area,'model_name'=>$model->bike_model,'reservation_hours'=>$reservation_hours,'service_hours'=>$service_hours,'marketing_hours'=>$marketing_hours,'movement_hours'=>$movement_hours,'admin_hours'=>$admin_hours,'reservation_hours_weekday'=>$reservation_hours_weekday,'service_hours_weekday'=>$service_hours_weekday,'marketing_hours_weekday'=>$marketing_hours_weekday,'movement_hours_weekday'=>$movement_hours_weekday,'admin_hours_weekday'=>$admin_hours_weekday,'reservation_hours_weekend'=>$reservation_hours_weekend,'service_hours_weekend'=>$service_hours_weekend,'marketing_hours_weekend'=>$marketing_hours_weekend,'movement_hours_weekend'=>$movement_hours_weekend,'admin_hours_weekend'=>$admin_hours_weekend]);
                            
                        }

                        $new_report_from_date_time = $new_report_to_date_time;

                    }
                }
                
            }
            }
            
        }
        
        return "Done";

        
    	
    }

}
