<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reviews;
use App\Models\Image;
use App\Transformers\ReviewsTransformer;

class ReviewsController extends AppController{

  protected $reviewsTransformer;

  function __construct(ReviewsTransformer $reviewsTransformer){
    $this->reviewsTransformer = $reviewsTransformer;
  }

  public function index(){
    $reviewsList = Reviews::all();
    return view('admin.reviews.index',compact('reviewsList'));
  }

  public function add(Request $request){
    $rules = array(
      'reviews' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/reviews')->withErrors($validator->errors());        
    }
    $imageImg = $request->file('imageUpload');
        if($imageImg){
            $imageExt = $imageImg->guessClientExtension();

            $imageImgHashName = time().'.'.$imageExt;
            $imageImgDestinationPath = 'img/bikes_upload/reviewimage/';
            $imageImg->move($imageImgDestinationPath, $imageImgHashName);

            $imageObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$imageImgDestinationPath.$imageImgHashName]);

            $imageId = $imageObj['id'];
        }
        Reviews::create(['review' => $request->reviews,'reviewed_on' => $request->reviewedon,'name' => $request->name, 'status' => $request->publishStatus, 'image_id' => $imageId]);
      
      return redirect('/admin/reviews');
      
  }


  public function edit($reviews_id,Request $request){
    $rules = array(
      'reviews' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/reviews')->withErrors($validator->errors());        
    }
          /* Logo Upload */
        $reviews = Reviews::find($reviews_id);
        $lImgId = $reviews->image_id;
        $imageImg = $request->file('imageUpload');
        if($imageImg){
            $imageExt = $imageImg->guessClientExtension();

            $imageImgHashName = time().'.'.$imageExt;
            $imageImgDestinationPath = 'img/bikes_upload/reviewimage/';
            $imageImg->move($imageImgDestinationPath, $imageImgHashName);

            $imageObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$imageImgDestinationPath.$imageImgHashName]);

            $imageId = $imageObj['id'];
        }
        else{
            if(isset($lImgId))
                $imageId = $lImgId;
            else
                $imageId = null;
        }
        Reviews::where('id',$reviews_id)->update(['name' => $request->name,'review' => $request->reviews,'reviewed_on' => $request->reviewedon, 'image_id' => $imageId, 'status' => $request->publishStatus]);
      
      
        return redirect('/admin/reviews'); 
  }


  public function delete($reviews_id){
    Reviews::destroy($reviews_id);
    return redirect('/admin/reviews');
  }

  public function getAllReviews(){
    $reviews = Reviews::where('status', 1)->get();
    foreach ($reviews as $review) {
          $image = Image::find($review['image_id']);
          $image = $image->transform();
          $review['image_id'] = $image['id'];
          $review['image'] = $image;
        }
    $data = $this->ReviewsTransformer->transformCollection($reviews->toArray());
    return $this->respondWithSuccess($data);
  }
}
