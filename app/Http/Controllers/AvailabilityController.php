<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Availability;
use App\Models\WorkingSlot;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\Area;
use App\Http\Controllers\WorkingSlotController;
use Carbon\Carbon;

use DB;

class AvailabilityController extends AppController{

  use Helpers;

  public function generateFromAvailabilityCalendar(Request $request){
    $rules = array(
      'model_id' => 'required',
      'area_id' => 'required',
      'month' => 'required',
      'year' => 'required'
    );

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }

    $dispatcher = app('Dingo\Api\Dispatcher');

    $modelId = $request->model_id;
    $cityId = $request->city_id;
    $areaId = $request->area_id;
    $month = $request->month;
    $year = $request->year;

    $currentDate = Carbon::today();
    $currentMonth = $currentDate->month;
    $currentYear = $currentDate->year;

    $calendar = array();

    if($month == $currentMonth && $year == $currentYear) {
      $fromDate = $currentDate;
      $lastDate = $fromDate->copy()->endOfMonth()->day;
      $endDate = Carbon::createFromDate($currentYear, $currentMonth, $lastDate);
    }else{
      $fromDate = Carbon::createFromDate($year, $month, 1);
      $lastDate = $fromDate->copy()->endOfMonth()->day;
      $endDate = Carbon::createFromDate($year, $month, $lastDate);
    }
    //ALL BIKES
    $bikeIds = Bike::where('model_id', $modelId)->where('area_id', $areaId)->where('status', 1)->get()->lists('id');
    if (empty($bikeIds->toArray())) {
      return $this->respondNotFound('No bikes available in this area.');
    }

    $availability = Availability::whereIn('bike_id', $bikeIds)
      ->whereBetween('date',[$fromDate->toDateString(), $endDate->toDateString()])->get();
 
    //MONTH MATRIX
    while ($fromDate <= $endDate) {
      $slots = $dispatcher->with(['date' => $fromDate->toDateString(),'city_id'=>$cityId])->get('from-working-hours-web');
      if(!empty($slots))
      {
          foreach ($slots as $slot) {
            $slot['status'] = true;
          }
          $data = [ "date" => $fromDate->toDateString(), "slots" => $slots, "status" => "A", "updated" => false];
          array_push($calendar, $data);
      }
      else {
          
          $data = [ "date" => $fromDate->toDateString(), "slots" => $slots, "status" => "A", "updated" => false];
          array_push($calendar, $data);
      }
      $fromDate->addDay();
    }

    //BOOKED BIKE IDS
    $bookedBikeIds = $availability->lists('bike_id');
    $bookedBikeIds = array_unique($bookedBikeIds->toArray());
    sort($bookedBikeIds);

    //CHECK IF A BIKE IS AVAILABLE THROUGHOUT A MONTH 
    $availableBikes = array_diff($bikeIds->toArray(), $bookedBikeIds);
    if (!empty($availableBikes)) {
      return $calendar;
    }

    //DAILY CHECK
    foreach ($calendar as &$cal) {
      $cur_date = $cal['date'];
      
      $bookedBikeIdsForDate = $availability->where('date', $cur_date)->lists('bike_id');
      $bikeAvailable = array_diff($bookedBikeIds, $bookedBikeIdsForDate->toArray());
      if (!empty($bikeAvailable)) {
        continue;
      }

      foreach ($bookedBikeIds as $bookedBikeId) {
        $slotIdsFinal = [];
        $slotsForDate = $availability->where('date', $cur_date)->where('bike_id', $bookedBikeId)->lists('slots');
       
        if($slotsForDate->isEmpty()){
          continue 2;
        }
        $slotIdsArr = $slotsForDate->toArray();
        foreach ($slotIdsArr as $slotIds) {
          $slotIdsFinal[] = explode(',', $slotIds);
        }

        if(count($slotIdsFinal) > 1){
          $slotIdsFinal = call_user_func_array('array_intersect', $slotIdsFinal);
        }
        else
          $slotIdsFinal = $slotIdsFinal[0];

        $working_slots = $dispatcher->with(['date' => $cur_date,'city_id'=>$cityId])->get('from-working-hours-web');
        $cur_slots_count = self::countAvailableSlots($cal['slots']);

        //if(count($working_slots) ==  $cur_slots_count || $cur_slots_count < count($slotIdsFinal)){
          foreach ($cal['slots'] as $cSlot) {
            if($cal['updated'] == false)
              $cSlot['status'] = false;
            if (in_array($cSlot['id'], $slotIdsFinal)) {
              $cSlot['status'] = true;
            }
          }
          $cal['updated'] = true;
        //}
      }
      
    } // DAILY CHECK ENDS
    foreach ($calendar as &$cal) {
        
      $date = Carbon::parse($cal['date']);
      if(Carbon::today() == $date){
        foreach ($cal['slots'] as &$slot) {
            if($slot){
              $date = Carbon::parse("today " . $slot['start_time']);
              if($date < Carbon::now())
                $slot['status'] = false;
            }
        }
      }
      $working_slotsDate = $dispatcher->with(['date' => $date,'city_id'=>$cityId])->get('from-working-hours-web');
      $count = self::countAvailableSlots($cal['slots']);
      
      if ($count == 0) {
        $cal['status'] = 'N';
      }elseif ($count < count($working_slotsDate)) {
        $cal['status'] = 'P';
      }else{
        $cal['status'] = 'A';
      }

    }
    return $calendar; 
  }
    
 public function generateFromAvailabilityCalendarWeb(Request $request){
    $rules = array(
      'model_id' => 'required',
      'area_id' => 'required',
      'month' => 'required',
      'year' => 'required'
    );

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }

    $dispatcher = app('Dingo\Api\Dispatcher');

    $modelId = $request->model_id;
    $cityId = $request->city_id;
    $areaId = $request->area_id;
    $month = $request->month;
    $year = $request->year;

    $currentDate = Carbon::today();
    $currentMonth = $currentDate->month;
    $currentYear = $currentDate->year;

    $calendar = array();

    if($month == $currentMonth && $year == $currentYear) {
      $fromDate = $currentDate;
      $lastDate = $fromDate->copy()->endOfMonth()->day;
      $endDate = Carbon::createFromDate($currentYear, $currentMonth, $lastDate);
    }else{
      $fromDate = Carbon::createFromDate($year, $month, 1);
      $lastDate = $fromDate->copy()->endOfMonth()->day;
      $endDate = Carbon::createFromDate($year, $month, $lastDate);
    }
    //ALL BIKES
    $bikeIds = Bike::where('model_id', $modelId)->where('area_id', $areaId)->where('status', 1)->get()->lists('id');
    if (empty($bikeIds->toArray())) {
      return $this->respondNotFound('No bikes available in this area.');
    }

    $availability = Availability::whereIn('bike_id', $bikeIds)
      ->whereBetween('date',[$fromDate->toDateString(), $endDate->toDateString()])->get();
 
    //MONTH MATRIX
    while ($fromDate <= $endDate) {
      $slots = $dispatcher->with(['date' => $fromDate->toDateString(),'city_id'=>$cityId])->get('from-working-hours-web');
      if(!empty($slots))
      {
          foreach ($slots as $slot) {
            $slot['status'] = true;
          }
          $data = [ "date" => $fromDate->toDateString(), "slots" => $slots, "status" => "A", "updated" => false];
          array_push($calendar, $data);
      }
      else {
          
          $data = [ "date" => $fromDate->toDateString(), "slots" => $slots, "status" => "A", "updated" => false];
          array_push($calendar, $data);
      }
      $fromDate->addDay();
    }

    //BOOKED BIKE IDS
    $bookedBikeIds = $availability->lists('bike_id');
    $bookedBikeIds = array_unique($bookedBikeIds->toArray());
    sort($bookedBikeIds);

    //CHECK IF A BIKE IS AVAILABLE THROUGHOUT A MONTH 
    $availableBikes = array_diff($bikeIds->toArray(), $bookedBikeIds);
    if (!empty($availableBikes)) {
      return $calendar;
    }
     

    //DAILY CHECK
    foreach ($calendar as &$cal) {
      $cur_date = $cal['date'];
      
      $bookedBikeIdsForDate = $availability->where('date', $cur_date)->lists('bike_id');
      $bikeAvailable = array_diff($bookedBikeIds, $bookedBikeIdsForDate->toArray());
      if (!empty($bikeAvailable)) {
        continue;
      }

      foreach ($bookedBikeIds as $bookedBikeId) {
        $slotIdsFinal = [];
        $slotsForDate = $availability->where('date', $cur_date)->where('bike_id', $bookedBikeId)->lists('slots');
       
        if($slotsForDate->isEmpty()){
          continue 2;
        }
        $slotIdsArr = $slotsForDate->toArray();
        foreach ($slotIdsArr as $slotIds) {
          $slotIdsFinal[] = explode(',', $slotIds);
        }

        if(count($slotIdsFinal) > 1){
          $slotIdsFinal = call_user_func_array('array_intersect', $slotIdsFinal);
        }
        else
          $slotIdsFinal = $slotIdsFinal[0];

        $working_slots = $dispatcher->with(['date' => $cur_date,'city_id'=>$cityId])->get('from-working-hours-web');
        $cur_slots_count = self::countAvailableSlots($cal['slots']);

        //if(count($working_slots) ==  $cur_slots_count || $cur_slots_count < count($slotIdsFinal)){
          
          
          foreach ($cal['slots'] as $cSlot) {
              
            if($cal['updated'] == false)
              $cSlot['status'] = false;
            if (in_array($cSlot['id'], $slotIdsFinal)) {
              $cSlot['status'] = true;
            }
          }
          $cal['updated'] = true;
        //}
      }
      
    } // DAILY CHECK ENDS
     
    foreach ($calendar as &$cal) {
      $date = Carbon::parse($cal['date']);
      if(isset($cal['slots']['slots']))
        {

        }
        else{          
          if(Carbon::today() == $date){  
            foreach ($cal['slots'] as &$slot) {
              $date = Carbon::parse("today " . $slot['start_time']);
              if($date < Carbon::now())
                $slot['status'] = false;
            }
          }
        }
      $working_slotsDate = $dispatcher->with(['date' => $date,'city_id'=>$cityId])->get('from-working-hours-web');
      $count = self::countAvailableSlots($cal['slots']);
      if(!empty($working_slotsDate))
      $working_slotsDate_count = count($working_slotsDate);
      else
      $working_slotsDate_count = 0; 
   
        
      if ($count == 0 && $working_slotsDate_count!=0) {
        $cal['status'] = 'N';
      }elseif($count == 0 && $working_slotsDate_count==0) {
        $cal['status'] = 'G';
      }
        elseif ($count < count($working_slotsDate)) {
        $cal['status'] = 'P';
      }else{
        $cal['status'] = 'A';
      }

    }
    return $calendar; 
  }

  public static function countAvailableSlots($slots){
    $count = 0;
      
    if(isset($slots['slots']))
    {
        
    }
    else{      
        foreach ($slots as $cSlot) {
          if ($cSlot['status'] == true) {
            $count = $count + 1;
          }
        }
    }
    return $count;
  }

  public function getAvailableBike(Request $request){
     
      
    $rules = array(
      'model_id' => 'required',
      'start_date' => 'required',
      'end_date' => 'required',
      'start_time' => 'required',
      'end_time' => 'required' 
    );
      
    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }
    $dispatcher = app('Dingo\Api\Dispatcher');
      

    $startDate = Carbon::parse($request->start_date);
    $endDate = Carbon::parse($request->end_date);
    $modelId = $request->model_id;
    $cityId = $request->city_id;
    $areaId = $request->area_id;
    $startTime = $request->start_time;
    $endTime = $request->end_time;

    $st = $startDate->toDateString();
    $en = $endDate->toDateString();
    
    if($request->area_id)
      $allBikeIds = Bike::where('model_id', $modelId)->where('area_id', $areaId)->where('status', 1)->get()->lists('id');
    else{
      $areaIds = Area::where('city_id', $cityId)->where('status', 1)->get()->lists('id');
      $allBikeIds = Bike::where('model_id', $modelId)->whereIn('area_id', $areaIds)->where('status', 1)->get()->lists('id');
    }

    if (empty($allBikeIds->toArray())) {
      return $this->response->array(['Bikes not found' => "none"]);
    }
    
    $availability =  Availability::whereIn('bike_id', $allBikeIds)->whereBetween('date',[$st, $en])->get();
      
    
    $availabilityCalendar= [];
    foreach ($availability as $booking) {
      if(array_key_exists($booking['bike_id'], $availabilityCalendar) && array_key_exists($booking['date'], $availabilityCalendar[$booking['bike_id']]))
        $availabilityCalendar[$booking['bike_id']][$booking['date']] = array_intersect($availabilityCalendar[$booking['bike_id']][$booking['date']], (empty($booking['slots']))? []: explode(',', $booking['slots']));
      else{
        $slots = (empty($booking['slots']))? []: explode(',', $booking['slots']);
        $availabilityCalendar[$booking['bike_id']][$booking['date']] = $slots;
      }
    }

    foreach ($availabilityCalendar as $bike_id => $dateVsSlots) {
      foreach ($dateVsSlots as $date => $slots) {
        if($date == $st && $date == $en){ // START DATE = END DATE
          $allSlotIds = AvailabilityController::getSlotsInBetween($st, $startTime, $endTime);
          $workingSlots = $dispatcher->with(['date' => $st])->get('working-hours');
         
          $slots = Slot::fixEndSlots($slots);
          $diff = array_diff($allSlotIds, $slots);
          if(!empty($diff)) //one or more slot(s) not found
            continue 2; // move on to next bike
          return $this->response->array(['bike_id' => $bike_id]);
        }else{ //START DATE != END DATE
          if($date == $st){
            $stDateSlots = $dispatcher->with(['date' => $st])->get('working-hours');
            $end5 = $stDateSlots->toArray();
            $end6 = end($end5);
            $startDateSlotIds = AvailabilityController::getSlotsInBetween($st, $startTime, $end6['start_time']);
            $diff = array_diff($startDateSlotIds, $slots);
            if(!empty($diff)) //one or more slot(s) not found
              continue 2; // move on to next bike
          }elseif ($date == $en) {
            $enDateSlots = $dispatcher->with(['date' => $en])->get('working-hours');
            $reset5 = $enDateSlots->toArray();
            $reset6 = reset($reset5);
            $stTime = $reset6['start_time'];
            $endDateSlotIds = AvailabilityController::getSlotsInBetween($en, $stTime, $endTime);
            $slots = Slot::fixEndSlots($slots);
            
            //get time in hh:mm format
            $parts = explode(':', $stTime);
            array_pop($parts);
            $firstSlotStTime = implode(':', $parts);
            //add the first slot, if the request end-time is first slot
            if($firstSlotStTime == $endTime){
                $reset3 = $enDateSlots->toArray();
                $reset4 = reset($reset3);
                $id_to_add = strval($reset4['id']);
                if(! in_array($id_to_add, $slots))
                    array_push($slots, $id_to_add);
 
            }
              
            $diff = array_diff($endDateSlotIds, $slots);
            if(!empty($diff)) //one or more slot(s) not found
              continue 2; // move on to next bike
          }else{
            continue 2; // move on to next bike
          }
        }
      }
      return $this->response->array(['bike_id' => $bike_id]);
    }
    $bookedBikeIds = $availability->lists('bike_id');
    $bookedBikeIds = array_unique($bookedBikeIds->toArray());
      
    $availableBikes = array_diff($allBikeIds->toArray(), $bookedBikeIds);
      
      
    if(empty($availableBikes))
      return $this->response->array(["Bikes not found"]);
    else
      return $this->response->array(['bike_id' => reset($availableBikes)]);
  }
  public function getAvailableBikeMobile(Request $request){
    $rules = array(
      'model_id' => 'required',
      'city_id' => 'required',
      'start_date' => 'required',
      'end_date' => 'required',
      'start_time' => 'required',
      'end_time' => 'required' 
    );
      

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }
    $dispatcher = app('Dingo\Api\Dispatcher');

    $startDate = Carbon::parse($request->start_date);
    $endDate = Carbon::parse($request->end_date);
    $modelId = $request->model_id;
    $cityId = $request->city_id;
    $areaId = $request->area_id;
    $startTime = $request->start_time;
    $endTime = $request->end_time;

    $st = $startDate->toDateString();
    $en = $endDate->toDateString();
    
    if($request->area_id)
      $allBikeIds = Bike::where('model_id', $modelId)->where('area_id', $areaId)->where('status', 1)->get()->lists('id');
    else{
      $areaIds = Area::where('city_id', $cityId)->where('status', 1)->get()->lists('id');
      $allBikeIds = Bike::where('model_id', $modelId)->whereIn('area_id', $areaIds)->where('status', 1)->get()->lists('id');
    }

    if (empty($allBikeIds->toArray())) {
      return $this->response->array(['Bikes not found' => "none"]);
    }
    
    $availability =  Availability::whereIn('bike_id', $allBikeIds)->whereBetween('date',[$st, $en])->get();
    
    $availabilityCalendar= [];
    foreach ($availability as $booking) {
      if(array_key_exists($booking['bike_id'], $availabilityCalendar) && array_key_exists($booking['date'], $availabilityCalendar[$booking['bike_id']]))
      {
        $exploded1 = explode(',', $booking['slots']);
        $availabilityCalendar[$booking['bike_id']][$booking['date']] = array_intersect($availabilityCalendar[$booking['bike_id']][$booking['date']], (empty($booking['slots']))? []: $exploded1);
      }
      else{
          $exploded2 = explode(',', $booking['slots']);
        $slots = (empty($booking['slots']))? []: $exploded2;
        $availabilityCalendar[$booking['bike_id']][$booking['date']] = $slots;
      }
    }

    foreach ($availabilityCalendar as $bike_id => $dateVsSlots) {
      foreach ($dateVsSlots as $date => $slots) {
        if($date == $st && $date == $en){ // START DATE = END DATE
          $allSlotIds = AvailabilityController::getSlotsInBetween($st, $startTime, $endTime);
          $workingSlots = $dispatcher->with(['date' => $st])->get('working-hours');
          
          $slots = Slot::fixEndSlots($slots);
          $diff = array_diff($allSlotIds, $slots);
          if(!empty($diff)) //one or more slot(s) not found
            continue 2; // move on to next bike
          return $this->response->array(['bike_id' => $bike_id]);
        }else{ //START DATE != END DATE
          if($date == $st){
            $stDateSlots = $dispatcher->with(['date' => $st])->get('working-hours');
            $end1 = $stDateSlots->toArray();
            $end2 = end($end1);
            $startDateSlotIds = AvailabilityController::getSlotsInBetween($st, $startTime, $end2['start_time']);
            $diff = array_diff($startDateSlotIds, $slots);
            if(!empty($diff)) //one or more slot(s) not found
              continue 2; // move on to next bike
          }elseif ($date == $en) {
            $enDateSlots = $dispatcher->with(['date' => $en])->get('working-hours');
            $reset1 = $enDateSlots->toArray();
            $reset2 = reset($reset1);
            $stTime = $reset2['start_time'];
            $endDateSlotIds = AvailabilityController::getSlotsInBetween($en, $stTime, $endTime);
            $slots = Slot::fixEndSlots($slots);
              //get time in hh:mm format
            $parts = explode(':', $stTime);
            array_pop($parts);
            $firstSlotStTime = implode(':', $parts);
            //add the first slot, if the request end-time is first slot
            if($firstSlotStTime == $endTime){
                $reset7 = $enDateSlots->toArray();
                $reset8 = reset($reset7);
                $id_to_add = strval($reset8['id']);
                if(! in_array($id_to_add, $slots))
                    array_push($slots, $id_to_add);
 
            }
              
            $diff = array_diff($endDateSlotIds, $slots);
            if(!empty($diff)) //one or more slot(s) not found
              continue 2; // move on to next bike
          }else{
            continue 2; // move on to next bike
          }
        }
      }
      return $this->response->array(['bike_id' => $bike_id]);
    }
    $bookedBikeIds = $availability->lists('bike_id');
    $bookedBikeIds = array_unique($bookedBikeIds->toArray());
    $availableBikes = array_diff($allBikeIds->toArray(), $bookedBikeIds);
    if(empty($availableBikes))
      return $this->response->array(['bike_id' => "none"]);
    else
      return $this->response->array(['bike_id' => reset($availableBikes)]);
  }

  public function generateToAvailabilityCalendar(Request $request){
    $rules = array(
      'model_id' => 'required',
      'area_id' => 'required',
      'start_date' => 'required',
      'start_time' => 'required'
    );

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }
    
    $cityId = $request->city_id;

    $dispatcher = app('Dingo\Api\Dispatcher');
    $bikeIds = Bike::where('area_id', $request->area_id)->where('model_id', $request->model_id)->where('status', 1)->get()->lists('id');
    if(empty($bikeIds->toArray()))
      return $this->respondWithError('No bikes available');

    $fromDateTime = $request->start_date.' '.$request->start_time.':00';
    $fromDateTimeObj = Carbon::parse($fromDateTime);
    
    $notBikeIds = Booking::where('start_datetime', '<', $fromDateTimeObj->toDateTimeString())
      ->where('end_datetime', '>', $fromDateTimeObj->toDateTimeString())->get()->lists('bike_id');
    $notBikeIds = array_unique($notBikeIds->toArray());
    $filteredBikeIds = array_diff($bikeIds->toArray(), $notBikeIds);

    if (empty($filteredBikeIds))
      return $this->respondWithError('No bikes available');

    // GET FUTURE BOOKINGS OF A MODEL, CHECK STARTING FROM GIVEN DATE
    $bookings = Booking::whereIn('bike_id', $bikeIds)
      ->where('end_datetime', '>=', $fromDateTimeObj->toDateTimeString())
      ->where('start_datetime', '>=', $fromDateTimeObj->toDateTimeString())
      ->get();

    $bookedBikeIds = $bookings->lists('bike_id');
    $bookedBikeIds = array_unique($bookedBikeIds->toArray());
    
    //GET ALL AVAILABLE BIKE IDS
    $availableBikeIds = array_diff($filteredBikeIds, $bookedBikeIds);
    sort($availableBikeIds); 

    // CHECK IF BIKE IS AVAILABLE
    if (!empty($availableBikeIds)){
      $date = Carbon::now()->addYear();
      $dateSlots = $dispatcher->with(['date' => $date->toDateString(),'city_id'=>$cityId])->get('to-working-hours-web');
      $dateSlots = $dateSlots->toArray();
      $endSlot = end($dateSlots);
      $data = ['bike_id' => reset($availableBikeIds), 'end_date' => $date->toDateString(), "last_slot" => $endSlot];
      return $this->response->array($data);
    }
    
    // ASSUMED NO BIKE IS AVAILABLE
    // CREATE A MATRIX, BIKEIDs vs BOOKING DATES  
    $matrix = [];
    $bookedBikeIds = array_diff($bookedBikeIds, $notBikeIds);
    
    foreach ($bookedBikeIds as $bookedBikeId) {
      $filteredBooking = $bookings->where('bike_id', $bookedBikeId)->toArray();
      // SORT THE BOOKINGS IN ASC ORDER OF BOOKING(START_DATETIME), FIRST FUTURE BOOKING OF A BIKE
      usort($filteredBooking, function($a, $b){
        return $a['start_datetime'] > $b['start_datetime'];
      });
      // ADD BOOKING START_DATETIME AGAINST EACH BIKE_ID
      $matrix[$bookedBikeId] = reset($filteredBooking)['start_datetime'];
    }
    
    // SORT THE MATRIX, RESULT: MOST AVAILABLE BIKE 
    // SORT THE MATRIX IN DESC ORDER OF MATRIX's DATES, LAST FUTURE BOOKING OF ANY BOOKED BIKE
    uasort($matrix, function($a, $b){
      return $a < $b;
    });

    $finalDate = reset($matrix);
    $finalDateObj = Carbon::parse($finalDate);
    $finalBikeId = key($matrix);
    $slot = Slot::where('start_time', $finalDateObj->toTimeString())->first();

    $data = ['bike_id' => $finalBikeId, 'end_date' => $finalDateObj->toDateString(), "last_slot" => $slot];
    return $this->response->array($data);
  } 

  /* Helper methods */

  public static function getSlotsInBetween($date, $start_time, $end_time){
    $dispatcher = app('Dingo\Api\Dispatcher');
    $dateSlots = $dispatcher->with(['date' => $date])->get('working-hours');
    $startSlot = Slot::where('start_time', $start_time)->first();
    $endSlot = Slot::where('start_time', $end_time)->first();
    $slots = [];
    $flag = false;
    foreach ($dateSlots as $ds) {
      if($ds['id'] == $startSlot['id'] || $flag){
        $flag = true;
        if($ds['id'] == $endSlot['id'])
          $flag = false;
        $slots[] = $ds['id'];
      }
    }
    return $slots;
  }

  public static function maxValueInArray($array, $keyToSearch){
    $currentMax = NULL;
    $currentBikeId = NULL;
    $slots = NULL;
    foreach($array as $arr){
      foreach($arr as $key => $value){
        if ($key == $keyToSearch && ($value >= $currentMax)){
          $currentMax = $value;
          $currentBikeId = $arr['bike_id'];
          $slots = end($arr['available_slots']);
          if (empty($slots)) {
            $dispatcher = app('Dingo\Api\Dispatcher');             
            $dateSlots = $dispatcher->with(['date' => $arr['end_date']])->get('working-hours');
            $dateSlotsArr = $dateSlots->toArray();
            $slots = end($dateSlotsArr);
            //$slots = $endSlot['start_time'];
          }
        }
      }
    }
    $data = [
        'bike_id' => $currentBikeId, 
        'end_date' => $currentMax,
        'last_slot' => $slots
      ];
    return $data;
  }

  public function getAllavailableBikes(Request $request){
    $rules = array(
      'model_id' => 'required',
      'city_id' => 'required',
      'start_date' => 'required',
      'end_date' => 'required',
      'start_time' => 'required',
      'end_time' => 'required' 
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }
      $dispatcher = app('Dingo\Api\Dispatcher');

      $startDate = Carbon::parse($request->start_date);
      $endDate = Carbon::parse($request->end_date);
      $modelId = $request->model_id;
      $cityId = $request->city_id;
      $areaId = $request->area_id;
      $startTime = $request->start_time;
      $endTime = $request->end_time;

      $dateSlots = $dispatcher->with(['date' => $endDate])->get('working-hours');
      $areaIds = Area::where('city_id', $cityId)->where('status', 1)->get()->lists('id');
      
      if($request->area_id)
        $bikeIds = Bike::where('model_id', $modelId)->where('area_id', $areaId)->where('status', 1)->get()->lists('id');
      else
        $bikeIds = Bike::where('model_id', $modelId)->whereIn('area_id', $areaIds)->where('status', 1)->get()->lists('id');

      if (empty($bikeIds)) {
        return false;
      }

      $st = $startDate->toDateString();
      $en = $endDate->toDateString();

      if($st != $en){
        $end3 = $dateSlots->toArray();
        $end4 = end($end3);
        $startDateSlotsIds = AvailabilityController::getSlotsInBetween($st, $startTime, $end4['start_time']);
        $endDateSlotsIds = AvailabilityController::getSlotsInBetween($en, current( ($dateSlots->toArray()) )['start_time'], $endTime);
      }else{
        $allSlotsIds = AvailabilityController::getSlotsInBetween($st, $startTime, $endTime);
      }


      $availability =  Availability::whereIn('bike_id', $bikeIds)
                                  ->whereBetween('date',[$st, $en])->get();
 
      $bookedArr = array();
      $availableBikeIds = array();
      foreach ($bikeIds as $bikeId) {

          foreach ($availability as $booked) {
            if($bikeId == $booked['bike_id']){
              $slotIdsArr = explode(',', $booked['slots']);
              $bookedArr[$booked['date']] = $slotIdsArr;
            }
          }

          foreach ($bookedArr as $date => $slots) {
            //if start_date = end_date -> check $allslots has $slots
            //if date = start_date -> startslots has slots
            //if date = end_date -> endslots has slots
            //else continue 2
          

            if(empty($slots))
              continue 1;
            
            if ($st == $en) {
              $reqdSlots = array_diff($allSlotsIds, $slots);
              if (!empty($reqdSlots)) {
                continue 1;
              }
            }

            if ($date == $st) {
              $reqdSlots = array_diff($startDateSlotsIds, $slots);
              if (!empty($reqdSlots)) {
                continue 1;
              } 
            }

            if($date == $en){
              $reqdSlots = array_diff($endDateSlotsIds, $slots);
              if (!empty($reqdSlots)) {
                continue 1;
              }
            }
            // if($startDate != $endDate && $date >= $startDate && $date <= $endDate){
            //   continue 2; 
            // }
          }
          array_push($availableBikeIds, $bikeId);
          
      } // main foreach loop
    return $availableBikeIds;
  }

  public static function generateAvailabiltityTable(){
    ini_set('max_execution_time', 600);
    $bookings = Booking::all();
    foreach ($bookings as $booking) {
      $startSlotId = Carbon::parse($booking['start_datetime']);
      
      $booking['start_slot_id'] = 11;
      $booking['end_slot_id'] = 21;
      $booking->save();

      $startSlot = Slot::where('id', $booking['start_slot_id'])->first();
      $endSlot = Slot::where('id', $booking['end_slot_id'])->first();

      $startDate = new Carbon($booking['start_datetime']);
      $st = new Carbon($booking['start_datetime']);
      $endDate = new Carbon($booking['end_datetime']);
      $ed = new Carbon($booking['end_datetime']);

      $availableSlots = 0;
      while ($startDate <= $endDate) {
        $availableSlots = null;
        
        $bike = Bike::where('id',$booking['bike_id'])->first();
        if($bike){
          $model = $bike->bikeModel;
          $area = $bike->area;
          $make = $bike->make();
          $city = $bike->city();
        }

        $availability = Availability::create(['city_id' => $city['id'], 'area_id' => $area['id'],
                              'make_id' => $make['id'], 'model_id' => $model['id'],
                              'bike_id' => $bike['id'], 'date' => $startDate->toDateString(),
                              'slots' => $availableSlots, 'booking_id' => $booking['id']
                        ]);

        $startDate->addDay();
      }
      
    }

  }


}
