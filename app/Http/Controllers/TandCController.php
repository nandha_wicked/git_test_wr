<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Area;
use App\City;

class TandCController extends Controller
{
    
    public function index(){

    	return view('front/termsandconditions');
    }
    public function indexDelhi(){

    	return view('front/termsandconditionsDelhi');
    }
    public function indexNZ(){

    	return view('front/termsandconditionsNZ');
    }
}
