<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Bike;
use App\Models\Price;
use App\Models\Booking;
use App\Models\City;
use App\Models\Image;
use App\Models\Vendor;
use App\Models\User;
use App\Models\BikeDocument;
use App\Models\Parameters;

use App\Http\Requests\BikeList;
use App\Http\Requests\BikeDetailsRequest;
use App\Transformers\BikeModelTransformer;
use App\Transformers\BookingTransformer;
use App\Transformers\AreaTransformer;
use App\Http\Controllers\BikeAvailabilityController;


use DB;
use Auth;
use Carbon\Carbon;



class BikeController extends AppController{

    protected $bikeModelTransformer;
    protected $bookingTransformer;
    protected $areaTransformer;

    function __construct(BikeModelTransformer $bikeModelTransformer, BookingTransformer $bookingTransformer, AreaTransformer $areaTransformer){
      $this->bikeModelTransformer = $bikeModelTransformer;
      $this->bookingTransformer = $bookingTransformer;
      $this->areaTransformer = $areaTransformer;
    }

    public function index()
    {
        $bikeModelList = BikeModel::all();
        $areaList = Area::where('status',1)->get();
        $bikes = Bike::orderBy('status','desc')->orderBy('area_id','desc')->orderBy('id','desc')->with(['area.city','bikeModel.make','owner'])->get();
        foreach ($bikes as $bike) {
            $model = $bike->bikeModel;
            $area = $bike->area;
            $city = $area->city;
            $make = $model->make;
            $bike['make_name'] = $make['bike_make'];
            $bike['model_name'] = $model['bike_model'];
            $bike['area_name'] = $area['area'];
            $bike['city_name'] = $city->city;

            if($bike->reg_number!="")
            {
                $bikeNumberArray = explode('-',$bike->reg_number);
                $bike['stateCode'] = $bikeNumberArray[0];
                $bike['areaCode'] = $bikeNumberArray[1];
                $bike['seriesCode'] = $bikeNumberArray[2];
                $bike['number'] = $bikeNumberArray[3];
            }
            else
            {
                $bike['stateCode'] = "0";
                $bike['areaCode'] = "";
                $bike['seriesCode'] = "";
                $bike['number'] = "";
            }


        }
        $priceList = Price::all();
        return view('admin.bike.index',compact('bikeModelList','areaList','bikes','priceList'));
    }   

    public function add(Request $request)
    {
        $rules = array(
            'bikeStatus' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/admin/bike')->withErrors($validator->errors());
        }
        else
        {
            $seriesCode = strtoupper($request->seriesCode);
            $areaCode = str_replace("-","",$request->areaCode);
            $number = str_replace("-","",$request->number);
            $bikeNumber = $request->stateCode."-".$areaCode."-".$seriesCode."-".$number;
            $existing = Bike::where('reg_number',$bikeNumber)->first();

            if(!(!$existing))
            {
                return redirect()->back()->withErrors(['A bike with the same number already exists.']);
            }

            $dateOfAddition = Carbon::parse($request->date_of_addition);

            $vendor = User::where('email',$request->vendor_email)->first();

            if(!$vendor)
            {
                return redirect()->back()->withErrors(['No aggregator exists with the entered email.']);
            }

            $bike = Bike::create([
                'model_id' => $request->modelName, 
                'area_id' => $request->areaName, 
                'status' => $request->bikeStatus,
                'reg_number'=>$bikeNumber,
                'created_by'=>Auth::user()->id,
                'date_of_addition'=>$dateOfAddition->toDateTimeString(),
                'vendor_id'=>$vendor->id
            ]);

            if($request->bikeStatus == 1)
                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"add",0,0);

            return redirect('/admin/bike');
        }   
    }

    public function edit($bike_id,Request $request)
    {
        $rules = array(
            'bikeStatus' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/admin/bike')->withErrors($validator->errors());
        }

        $bike = Bike::where('id',$bike_id)->first();

        $seriesCode = strtoupper($request->seriesCode);
        $areaCode = str_replace("-","",$request->areaCode);
        $number = str_replace("-","",$request->number);
        $bikeNumber = $request->stateCode."-".$areaCode."-".$seriesCode."-".$number;
        $existing = Bike::where('reg_number',$bikeNumber)->where('id','<>',$bike->id)->first();

        if(!(!$existing))
        {
            return redirect()->back()->withErrors(['A bike with the same number already exists.']);
        }
        /*
        if($bike->status!=$request->bikeStatus)
        {
            if($request->bikeStatus==1)
            {
                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"add",0,0);
            }
            else
            {
                $today = Carbon::today();
                $today->addDays(env('FUTUREDAYSNO'));
                $today->addDay();
                $now =Carbon::now();
                $now->addHour();
                $now->minute = 0;
                $now->second = 0;

                $dispatcher = app('Dingo\Api\Dispatcher');
                $bikeAvailability = $dispatcher->with(['model_id' => $bike->model_id, 'area_id' => $bike->area_id, 'start_date' => $now->format('Y-m-d'), 'end_date' => $today->format('Y-m-d'), 'start_time' => $now->format('H:i'), 'end_time' => $today->format('H:i')])->get('check-availability');

                if($bikeAvailability['bike_id']=="none")
                    return redirect('/admin/bike')->withErrors(["not available"=>"There are bookings for this bike, and it cannot be inactivated before they are deleted."]);

                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"delete",0,0);
            }
        }
        */

        Bike::where('id',$bike_id)->update([
            'area_id' => $request->areaName, 
            'status' => $request->bikeStatus,
            'reg_number'=>$bikeNumber
        ]); 

        return redirect('/admin/bike');
    } 

    public function indexVendor()
    {
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        if(!$vendor) 
        {
            return "No vendor found";
        }
        else 
        {
            $bikeModelList = BikeModel::all();
            $areaList = Area::where('vendor_id',$vendor['id'])->get();
            $bikes = Bike::where('vendor_id',$vendor['id'])->get();
            foreach ($bikes as $bike) 
            {
                $model = $bike->bikeModel;
                $area = $bike->area;
                $city = $bike->city();
                $make = $model->make;
                $price = Price::where('model_id', $model['id'])->where('area_id', $area['id'])->first();
                $bike['make_name'] = $make['bike_make'];
                $bike['model_name'] = $model['bike_model'];
                $bike['area_name'] = $area['area'];
                $bike['city_name'] = $city['city'];
                $bike['price_per_hour'] = $price['price_per_hour'];
                $bike['minimum_hours'] = $price['minimum_hours'];
            }

            $priceList = Price::all();
            return view('vendor.bike.index',compact('bikeModelList','areaList','bikes','priceList'));
        }
    }   

    public function addVendor(Request $request)
    {

        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        $rules = array(
            'bikeStatus' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/vendor/bike')->withErrors($validator->errors());
        }
        else
        {

            $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');

            if (in_array($request->areaName, $areaList->toArray())) 
            {
                $bike = Bike::create(['model_id' => $request->modelName, 'area_id' => $request->areaName, 'status' => $request->bikeStatus, 'vendor_id'=> $vendor['id']]);
            }
            BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"add",0,0);
            return redirect('/vendor/bike');
        }   
    }

    public function editVendor($bike_id,Request $request){
        $rules = array(
            'bikeStatus' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/vendor/bike')->withErrors($validator->errors());
        }
        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');

        if (in_array($request->areaName, $areaList->toArray())) {
            $bike = Bike::where('id',$bike_id)->first();

            Bike::where('id',$bike_id)
            ->update(['area_id' => $request->areaName, 'status' => $request->bikeStatus]);
        }

        if($bike->status!=$request->bikeStatus)
        {
            if($request->bikeStatus==1)
            {
                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"add",0,0);
            }
            else
            {
                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"delete",0,0);

            }
        }
        return redirect('/vendor/bike'); 
    }

    
    public function indexBike(Request $request)
    {
        $pageTitle = "Bike";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>90,"title"=>"Make"],
                                            ["width"=>100,"title"=>"Model"],
                                            ["width"=>100,"title"=>"City"],
                                            ["width"=>120,"title"=>"Area"],
                                            ["width"=>120,"title"=>"Number"],
                                            ["width"=>140,"title"=>"Owner"],
                                            ["width"=>120,"title"=>"Date of Addition"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>140,"title"=>"Rc Card"],
                                            ["width"=>140,"title"=>"Insurance"],
                                            ["width"=>140,"title"=>"Permit"],
                                            ["width"=>140,"title"=>"Emission"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $bikeObjects = Bike::orderBy('status','desc')->orderBy('area_id','desc')->orderBy('id','desc')->with(['area.city','bikeModel.make','owner','documents'])->get();
        foreach ($bikeObjects as $bike) {
            $model = $bike->bikeModel;
            $area = $bike->area;
            $city = $area->city;
            $make = $model->make;
            $bike['make_name'] = $make['bike_make'];
            $bike['model_name'] = $model['bike_model'];
            $bike['area_name'] = $area['area'];
            $bike['city_name'] = $city->city;

            if($bike->reg_number!="")
            {
                $bikeNumberArray = explode('-',$bike->reg_number);
                $bike['stateCode'] = $bikeNumberArray[0];
                $bike['areaCode'] = $bikeNumberArray[1];
                $bike['seriesCode'] = $bikeNumberArray[2];
                $bike['number'] = $bikeNumberArray[3];
            }
            else
            {
                $bike['stateCode'] = "0";
                $bike['areaCode'] = "";
                $bike['seriesCode'] = "";
                $bike['number'] = "";
            }
            
            $rcCard = "";
            $insurance = "";
            $permit = "";
            $emission = "";

            foreach($bike->documents as $doc)
            {
                if($doc->deleted == 0)
                {
                    if($doc->doc_type == "RC Card")
                    {
                        $rcCard =  $doc->url;
                    }
                    elseif($doc->doc_type == "Insurance")
                    {
                        $insurance = $doc->url;
                    }
                    elseif($doc->doc_type == "Permit")
                    {
                        $permit = $doc->url;
                    }
                    elseif($doc->doc_type == "Emission")
                    {
                        $emission = $doc->url;
                    }
                }
            }
            
            $bike["RC Card"] = $rcCard;
            $bike["Insurance"] = $insurance;
            $bike["Permit"] = $permit;
            $bike["Emission"] = $emission;
        }
        
        $objectTableArray['body'] = [];
        
        foreach($bikeObjects as $bikeObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$bikeObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject['make_name'],"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject['model_name'],"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject['city_name'],"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject['area_name'],"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->reg_number,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->owner->email,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->readableDateTime($bikeObject->date_of_addition),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($bikeObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_bike_btn",
                                                            "id"=>"edit_bike_id",
                                                            "data_id"=>$bikeObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"image","value"=>["src"=>$bikeObject["RC Card"],"width"=>100]],
                        ["type"=>"image","value"=>["src"=>$bikeObject["Insurance"],"width"=>100]],
                        ["type"=>"image","value"=>["src"=>$bikeObject["Permit"],"width"=>100]],
                        ["type"=>"image","value"=>["src"=>$bikeObject["Emission"],"width"=>100]]

                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_bike_modal";
        $addObject['add_button_text'] = "Add Bike";
        $addObject['modal_title'] = "Add Bike";
        $addObject['add_url'] = "/admin/bike/add";
        $addObject['add_modal_form_items'] = [];
        
        $bikeModelList = BikeModel::all();
        $areas = Area::where('status',1)->get();
        
        $modelList = [];
        foreach($bikeModelList as $bikeModelListEl)
        {
            array_push($modelList,["value"=>$bikeModelListEl->id,"text"=>$bikeModelListEl->bike_model]);
        }
        
        $areaList = [];
        foreach($areas as $area)
        {
            array_push($areaList,["value"=>$area->id,"text"=>$area->area]);
        }
        
        
        $stateCodeList = [];
        $stateCodes = explode(",",Parameters::getParameter('bike_number_series_list'));
        foreach($stateCodes as $stateCode)
        {
            array_push($stateCodeList,["value"=>$stateCode,"text"=>$stateCode]);            
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"select","label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"select","label"=>"Area", "name"=>"area_add", "id"=>"area_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Area --", "select_items"=>$areaList],
                ["type"=>"select","label"=>"State Code", "name"=>"state_code_add", "id"=>"state_code_id_add", "default_value"=>"not_set", "default_text"=>"-- Select State Code --", "select_items"=>$stateCodeList],
                ["type"=>"input", "input_type"=>"text","label"=>"Area Code","name"=>"area_code_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Series Letters","name"=>"series_code_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Number","name"=>"number_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Owner Email","name"=>"owner_email_add"],
                ["type"=>"date","label"=>"Date of addition","name"=>"date_of_addition_add","id"=>"date_of_addition_id_add"],
                ["type"=>"image","label"=>"RC Card","name"=>"rc_card_add"],
                ["type"=>"image","label"=>"Insurance","name"=>"insurance_add"],
                ["type"=>"image","label"=>"Permit","name"=>"permit_add"],
                ["type"=>"image","label"=>"Emission","name"=>"emission_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_bike_modal";
        $editObject['edit_btn_class'] = "edit_bike_btn";
        $editObject['modal_title'] = "Edit Bike";
        $editObject['edit_url'] = "/admin/bike/edit";
        $editObject['ajax_url'] = "/admin/bike/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"select","label"=>"Area", "name"=>"area_edit", "id"=>"area_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Area --", "select_items"=>$areaList],
                ["type"=>"select","label"=>"State Code", "name"=>"state_code_edit", "id"=>"state_code_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select State Code --", "select_items"=>$stateCodeList],
                ["type"=>"input", "input_type"=>"text","label"=>"Area Code","name"=>"area_code_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Series Letters","name"=>"series_code_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Number","name"=>"number_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Owner Email","name"=>"owner_email_edit"],
                ["type"=>"input","label"=>"Date of addition","input_type"=>"date","name"=>"date_of_editition_edit","id"=>"date_of_editition_id_edit"],
                ["type"=>"image","label"=>"RC Card","name"=>"rc_card_edit","old_image_id"=>"old_rc_card_id_edit"],
                ["type"=>"image","label"=>"Insurance","name"=>"insurance_edit","old_image_id"=>"old_insurance_id_edit"],
                ["type"=>"image","label"=>"Permit","name"=>"permit_edit","old_image_id"=>"old_permit_id_edit"],
                ["type"=>"image","label"=>"Emission","name"=>"emission_edit","old_image_id"=>"old_emission_id_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
                
            ];
        
        
        $deleteObject = [];
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addBike(Request $request)
    {
        $rules = array(
            'model_add' => 'required',
            'area_add' => 'required',
            'state_code_add' => 'required',
            'area_code_add' => 'required|numeric',
            'series_code_add' => 'required|alpha',
            'number_add' => 'required|numeric',
            'status_add' => 'required',
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $seriesCode = strtoupper($request->series_code_add);
        $areaCode = str_replace("-","",$request->area_code_add);
        $number = str_replace("-","",$request->number_add);
        $bikeNumber = $request->state_code_add."-".$areaCode."-".$seriesCode."-".$number;
        $existing = Bike::where('reg_number',$bikeNumber)->first();

        if(!(!$existing))
        {
            return redirect()->back()->withErrors(['A bike with the same number already exists.']);
        }

        $dateOfAddition = Carbon::parse($request->date_of_addition_add);

        $vendor = User::where('email',$request->owner_email_add)->first();

        if(!$vendor)
        {
            return redirect()->back()->withErrors(['No aggregator exists with the entered email.']);
        }
        
        $bike = Bike::create([
            'model_id' => $request->model_add, 
            'area_id' => $request->area_add, 
            'status' => $request->status_add,
            'reg_number'=>$bikeNumber,
            'created_by'=>Auth::user()->id,
            'date_of_addition'=>$dateOfAddition->toDateTimeString(),
            'vendor_id'=>$vendor->id
        ]);
        
        $docImageDestinationPath = "bike/documents/";
        
        $rcImg = $request->file('rc_card_add');
        
        if($rcImg)
        {
            $createImage = Self::saveImageToFile($rcImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "RC Card",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                    'status' => 1
                ]);
            }
        }
        
        $insuranceImg = $request->file('insurance_add');
        
        if($insuranceImg)
        {
            $createImage = Self::saveImageToFile($insuranceImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Insurance",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        
        $permitImg = $request->file('permit_add');
        
        if($permitImg)
        {
            $createImage = Self::saveImageToFile($permitImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Permit",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        
        $emissionImg = $request->file('emission_add');
        
        if($emissionImg)
        {
            $createImage = Self::saveImageToFile($emissionImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Emission",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        

        if($request->status_add == 1)
            BikeAvailabilityController::addOrDeleteBike($request->area_add,$request->model_add,"add",0,0);

        
        return redirect()->back();
    }
    
    public function detailsBike(Request $request)
    {
        $bike = Bike::where('id',$request->id)->first();
        
        $bikeNumberArray = explode('-',$bike->reg_number);
        
        $rcCard = "";
        $insurance = "";
        $permit = "";
        $emission = "";
        
        foreach($bike->documents as $doc)
        {
            if($doc->deleted == 0)
            {
                if($doc->doc_type == "RC Card")
                {
                    $rcCard = "<img src=\"".$doc->url."\" width=\"90px\">";
                }
                elseif($doc->doc_type == "Insurance")
                {
                    $insurance = "<img src=\"".$doc->url."\" width=\"90px\">";
                }
                elseif($doc->doc_type == "Permit")
                {
                    $permit = "<img src=\"".$doc->url."\" width=\"90px\">";
                }
                elseif($doc->doc_type == "Emission")
                {
                    $emission = "<img src=\"".$doc->url."\" width=\"90px\">";
                }
            }
        }
        
        $data = [
            ["type"=>"select","name"=>"area_edit","value"=>$bike->area_id],
            ["type"=>"select","name"=>"state_code_edit","value"=>$bikeNumberArray[0]],
            ["type"=>"input","name"=>"area_code_edit","value"=>$bikeNumberArray[1]],
            ["type"=>"input","name"=>"series_code_edit","value"=>$bikeNumberArray[2]],
            ["type"=>"input","name"=>"number_edit","value"=>$bikeNumberArray[3]],
            ["type"=>"input","name"=>"owner_email_edit","value"=>$bike->owner->email],
            ["type"=>"date","name"=>"date_of_editition_edit","value"=>$bike->date_of_addition],
            ["type"=>"image","imgId"=>"old_rc_card_id_edit","html"=>$rcCard],
            ["type"=>"image","imgId"=>"old_insurance_id_edit","html"=>$insurance],
            ["type"=>"image","imgId"=>"old_permit_id_edit","html"=>$permit],
            ["type"=>"image","imgId"=>"old_emission_id_edit","html"=>$emission],
            ["type"=>"checkbox","status"=>$bike->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editBike($id, Request $request)
    {
        $rules = array(
            'area_edit' => 'required',
            'state_code_edit' => 'required',
            'area_code_edit' => 'required|numeric',
            'series_code_edit' => 'required|alpha',
            'number_edit' => 'required|numeric',
            'status_edit' => 'required',
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        

        $seriesCode = strtoupper($request->series_code_edit);
        $areaCode = str_replace("-","",$request->area_code_edit);
        $number = str_replace("-","",$request->number_edit);
        $bikeNumber = $request->state_code_edit."-".$areaCode."-".$seriesCode."-".$number;
        $existing = Bike::where('reg_number',$bikeNumber)->first();

        if((!(!$existing))&&($existing->id != $id))
        {
            return redirect()->back()->withErrors(['A bike with the same number already exists.']);
        }
        
        $bike = Bike::where('id',$id)->first();
        
        /*
        if($bike->status!=$request->bikeStatus)
        {
            if($request->bikeStatus==1)
            {
                BikeAvailabilityController::addOrDeleteBike($request->areaName,$request->modelName,"add",0,0);
            }
            else
            {
                $today = Carbon::today();
                $today->addDays(env('FUTUREDAYSNO'));
                $today->addDay();
                $now =Carbon::now();
                $now->addHour();
                $now->minute = 0;
                $now->second = 0;

                $dispatcher = app('Dingo\Api\Dispatcher');
                $bikeAvailability = $dispatcher->with([
                    'model_id' => $bike->model_id, 
                    'area_id' => $bike->area_id, 
                    'start_date' => $now->format('Y-m-d'), 
                    'end_date' => $today->format('Y-m-d'), 
                    'start_time' => $now->format('H:i'), 
                    'end_time' => $today->format('H:i')
                ])->get('check-availability');

                if($bikeAvailability['bike_id']=="none")
                    return redirect('/admin/bike')->withErrors(["not available"=>"There are bookings for this bike, and it cannot be inactivated before they are deleted."]);

                BikeAvailabilityController::addOrDeleteBike($request->area_edit,$request->model_edit,"delete",0,0);
            }
        }
        */

        Bike::where('id',$bike->id)->update([
            'area_id' => $request->area_edit, 
            'status' => $request->status_edit,
            'reg_number'=>$bikeNumber
        ]); 
        
        
        $docImageDestinationPath = "bike/documents/";
        
        $rcImg = $request->file('rc_card_edit');
        
        if($rcImg)
        {
            $createImage = Self::saveImageToFile($rcImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                $existingImg = BikeDocument::where('bike_id',$bike->id)->where('doc_type',"RC Card")->orderBy('id','desc')->first();
                if(!(!$existingImg))
                {
                    $existingImg->update([
                        'deleted'=>1,
                        'deleted_at'=>Carbon::now()->toDateTimeString(),
                        'deleted_by'=>Auth::user()->id
                    ]);
                }
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "RC Card",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                    'status' => 1
                ]);
            }
        }
        
        $insuranceImg = $request->file('insurance_edit');
        
        if($insuranceImg)
        {
            $createImage = Self::saveImageToFile($insuranceImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Insurance",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        
        $permitImg = $request->file('permit_edit');
        
        if($permitImg)
        {
            $createImage = Self::saveImageToFile($permitImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Permit",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        
        $emissionImg = $request->file('emission_edit');
        
        if($emissionImg)
        {
            $createImage = Self::saveImageToFile($emissionImg,$docImageDestinationPath);
            if($createImage['success'])
            {
                BikeDocument::create([
                    'bike_id'=>$bike->id, 
                    'doc_type' => "Emission",
                    'url' => $createImage['image'],
                    'created_by'=>Auth::user()->id,
                ]);
            }
        }
        

        return redirect()->back()->withErrors(['Bike edited successfully.']);
    }
    
    public function deleteBike($id, Request $request)
    {
        $bike = Bike::where('id',$id)->first();
        
        DB::table('on_demand_a2b_hub_spoke')->where('hub_area',$bike->id)->update([
            'deleted'=>1,
            'deleted_at'=>Carbon::now()->toDateTimeString(),
            'deleted_by'=>Auth::user()->id
        ]);
        
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($bike),"table_name"=>"on_demand_a2b_bike","deleted_by"=>Auth::user()->id]);
        Bike::where('id',$id)->delete();
        
        return redirect()->back();
    }
    
    function saveImageToFile($imgFile,$docImageDestinationPath) {
         
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
        
        $imgExt = $imgFile->guessClientExtension();
        
        $docImageHashName = $str.'.'.$imgExt;
                
        $s3 = \Storage::disk('s3');
        $filePath = $docImageDestinationPath.$docImageHashName;
        $s3->put($filePath,file_get_contents($imgFile), 'public');
        
        
        if($s3)
            $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
        else
            $image = "";
        
        return ["success"=>true, "image"=>$image];
    }






}
