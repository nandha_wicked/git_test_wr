<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use Auth;
use App\Models\GCM;
use App\Models\User;
use Session;
use Config;

class GCMController extends AppController
{   
    public function index(){

        return view('admin.gcm.index');
    }
    
    public function push(Request $request){
        
        $ids = [];
        
        if($request->user_type == 0)
        {
            
            $unregisteredUsers = GCM::all();
            foreach($unregisteredUsers as $unregisteredUser)
            {
                array_push($ids,$unregisteredUser->gcm_token);
            }
            
            User::chunk(200, function ($users) use(&$ids){
                foreach($users as $user)
                {
                    if($user->gcm_token)
                    {
                        array_push($ids,$user->gcm_token);
                    }
                    
                }
            });
        }
        elseif($request->user_type == 1)
        {
            
            User::chunk(200, function ($users) use(&$ids){
                foreach($users as $user)
                {
                    if($user->gcm_token)
                    {
                        array_push($ids,$user->gcm_token);
                    }
                    
                }
            });
        }
        elseif($request->user_type == 2)
        {
            
            $unregisteredUsers = GCM::all();
            foreach($unregisteredUsers as $unregisteredUser)
            {
                array_push($ids,$unregisteredUser->gcm_token);
            }
            
        }
        elseif($request->user_type == 3)
        {
            $user = User::where('email','varunagni@gmail.com')->first();
            array_push($ids,$user->gcm_token);
        }
        
        $data = ["data"=>[$request->param1=>$request->value1,
                 $request->param2=>$request->value2,
                 $request->param3=>$request->value3,
                 $request->param4=>$request->value4,
                 $request->param5=>$request->value5,
                 $request->param6=>$request->value6,
                 $request->param7=>$request->value7
                ]];
        
        
        $smallerIdArrays = array_chunk($ids, 900);
        
        foreach($smallerIdArrays as $smallerIdArray)
        {
            $resultJson = GCM::sendPushNotification($data,$smallerIdArray);
            $result = json_decode($resultJson);
            foreach($result->results as $key=>$resultForId)
            {
                foreach($resultForId as $keyId => $response)
                {
                    if($keyId == "error")
                    {
                        if($response == "NotRegistered")
                        {
                            GCM::where('gcm_token',$smallerIdArray[$key])->delete();
                        }

                    }

                }

            }
        }
        
        
        

        return view('admin.gcm.index');
    }
    
    
    
    
    
    

}
