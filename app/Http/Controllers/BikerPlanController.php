<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BikerPlan;
use Session;
use App\Models\User;
use Auth;
use App\Models\BikerPlanSubscription;
use Dingo\Api\Routing\Helpers;




class BikerPlanController extends Controller
{
  use Helpers;

    public function index()
    {
        $bikerPlans = BikerPlan::where('status',1)->get();
        return view('front/bikerPlanList',compact('bikerPlans'));
    }


public function create(Request $request){
 

    $user = UserController::getLoggedInUser();
    
    $status = "COMPLETE";
    
    

    

    
    $bikerPlanSubscription = BikerPlanSubscription::create([ 'user_id' => $user['id'], 'biker_plan_id' => $request->biker_plan_id, 'total_price' => $request->total_price, 'pg_status' => $request->pg_status, 'pg_txn_id' => $request->pg_txn_id, 'bank_txn_id' => $request->bank_txn_id, 'order_id' => $request->order_id, 'pg_mode' => $request->pg_mode, 'pg_txn_amount' => $request->pg_txn_amount, 'raw_data' => $request->raw_data,'status' => $status]);

 
    

    $data = ['id' => $bikerPlanSubscription['id']];
    return $this->response->array($data);
  }
 
    public function detail($name)
    {
       
        $bikerPlan = BikerPlan::where('name',$name)->first();
        $otherPlans = BikerPlan::where('name','!=',$name)->take(4)->get();
        
        Session::put('planCost', $bikerPlan->cost);
        Session::put('planName', $bikerPlan->name);
        Session::put('planId', $bikerPlan->id);
        Session::put('planImage', $bikerPlan->image);
        Session::put('planLongDescription', $bikerPlan->longDescription);
        
        


        return view('front/bikerPlan',compact('bikerPlan','otherPlans'));
        
        
        
    }
    
    public function checkUserLoggedin()
    {
        

        if(Auth::check()){
            //return view('/front/booking/bookingConfirm');
            return redirect('/confirm-plan');
        }
        else
            return redirect('/plan-login');
    }
    
    public function planConfirm()
    {
     
        $planCost = Session::get('planCost');
        $planName = Session::get('planName');
        $planId = Session::get('planId');
        $planImage = Session::get('planImage');
        $planLongDescription = Session::get('planLongDescription');

        if (Auth::check() && !empty($planCost) && !empty($planName) && !empty($planId) && !empty($planImage) && !empty($planLongDescription))
             return view('front/bikerPlanConfirm');
        else
            return redirect('/biker-plan');       
                
    }
    
    public function planLogin()
    {
        return view('front/planLogin');
        //echo session('bkmodelId');
    }

   

    
}
