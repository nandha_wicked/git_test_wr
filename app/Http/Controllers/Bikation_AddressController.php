<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use Session;
use DateTime;
use App\Models\Bikation_Address;


class Bikation_AddressController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){
    if(Session::has('email') && Session::has('First_Name') ){
      $bikation_venor_user_id=Session::get('Vendor_ID');
      $bikation_address = Bikation_Address::where('Vendor_ID',$bikation_venor_user_id)->first();
      return view('bikationvendor.front.add_index',compact('bikation_address'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addAddress(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array(
         'Area' => 'required',
          'City' => 'required',
          'State' => 'required',
          'Country' => 'required',
          'ZipCode' => 'required',
          'Phone_Number' => 'required',
          'Website' => 'required'
          );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-address')->withErrors($validator->errors());
      }
      else{
        if(!empty($request->Vendor_ID)){
        $Created_On = new DateTime();
        $CreatedOn= $Created_On->format('Y-m-d H:i:s');
        $Created_By= Session::get('Vendor_ID');

        $Updated_On = new DateTime();
        $UpdatedOn= $Updated_On->format('Y-m-d H:i:s');
        $Updated_By= Session::get('Vendor_ID');
        Bikation_Address::where('Vendor_ID', $request->Vendor_ID)->update(['Valid_From' => $request->Valid_From,'Valid_To' => $request->Valid_To, 'Created_By' => $Created_By,'Updated_By' => $Updated_By,'Updated_On' => $UpdatedOn,'Door_No' => $request->Door_No,'Street_1' => $request->Street_1, 'Street_2' => $request->Street_2,'Area' => $request->Area,'City' => $request->City,'State'=>$request->State,'Country' => $request->Country,'ZipCode' => $request->ZipCode,'Name' => $request->Name,'LandLine_Number'=>$request->LandLine_Number,'Phone_Number' => $request->Phone_Number,'Alternate_Number' => $request->Alternate_Number,'Email' => $request->Email,'Website'=>$request->Website,'about_me'=>$request->about_me]);
        return redirect('bikation-address');
      }else{
        Bikation_Address::create(['Valid_From' => $request->Valid_From,'Valid_To' => $request->Valid_To, 'Created_By' => $Created_By,'Updated_By' => $Updated_By,'Created_On'=>$CreatedOn,'Door_No' => $request->Door_No,'Street_1' => $request->Street_1, 'Street_2' => $request->Street_2,'Area' => $request->Area,'City' => $request->City,'State'=>$request->State,'Country' => $request->Country,'ZipCode' => $request->ZipCode,'Name' => $request->Name,'LandLine_Number'=>$request->LandLine_Number,'Phone_Number' => $request->Phone_Number,'Alternate_Number' => $request->Alternate_Number,'Email' => $request->Email,'Website'=>$request->Website,'about_me'=>$request->about_me]);
        return redirect('bikation-address');
      }
    }
  }else{
    return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
  }
}
   
  
    
}
