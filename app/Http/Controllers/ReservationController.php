<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use App\Models\User;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\City;
use App\Models\Price;
use App\Models\Image;
use App\Models\Wallet;
use App\Models\Parameters;

use App\Models\ReservationInfo;
use App\Models\BookingHeader;
use App\Models\DeliveryInfo;
use App\Models\FinancialInfo;
use App\Models\NotesInfo;
use App\Models\PricingInfo;
use App\Models\ReturnInfo;
use App\Models\AccessoriesList;

use DB;
use Mail;
use Carbon\Carbon;
use Carbon\CarbonInterval;

use Auth;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\BikeAvailabilityController;
use App\Jobs\ModelHourArea;

use Excel;
use Zipper;
use Curl;



class ReservationController extends AppController
{
    use Helpers;
    
    public function createBookingApi(Request $request)
    {
        $rules = array(
            'user_id' => 'exists:users,id',
            'reservation_details' => 'required',
            'creation_mode' => 'required',
            'financial_info' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        
        if(isset($request->user_id))
        {
            $user = User::where('id',$request->user_id)->first();
        }
        else
        {
            $user = UserController::getUserByToken();
        }
        
        $creation_mode = $request->creation_mode;
        $promo_code = $request->promo_code;
        
        $reservation_details = $request->reservation_details;
        $financial_infos = $request->financial_info;
        
        $notes = $request->notes;
        
        $data = Self::createBooking($user,$creation_mode,$promo_code,$reservation_details,$financial_infos,$notes,0,'api');
        
        if(array_key_exists('bookingId',$data))
            return $this->respondWithSuccess($data);
        else
            return $this->respondWithValidationErrorStatusCode($data);   
    }
    
    
    public static function createBooking($user,$creation_mode,$promo_code,$reservation_details,$financial_infos,$notes,$agreedPrice,$booking_platform)
    {
                
        if(in_array($creation_mode,explode(',',env('DIRECTCREATIONMODES'))))
        {
            $createdByUser = $user;
            $notesNature = "User creation notes";
        }
        else
        {
            $createdByUser = Auth::user();
            if($creation_mode == "S")
                $notesNature = "Store creation notes";
            else
                $notesNature = "Customer Care creation notes";
        }
        
        $bookingHeader = BookingHeader::create([
            "user_id"=>$user->id
        ]);
        
        $totalBookingPrice = 0;
        $promoWalletAmountRedeemed = 0;
        $nonpromoWalletAmountRedeemed = 0;
        
        $pricingObjectArray = [];
        
        
        $failedReservations = [];
        $successfulReservations = [];
        $reservationFailed = false;
        
        $reservationId = 0;
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        foreach($reservation_details as $reservation_detail)
        {
            $rules = array(
                'bike_or_accessory' => 'required',
                'model_id' => 'required_if:bike_or_accessory,B|exists:bike_models,id',
                'area_id' => 'required|exists:areas,id',
                'accessory_model_id' => 'required_if:bike_or_accessory,A|exists:accessories_model,id',
                'accessory_size_id' => 'required_if:bike_or_accessory,A|exists:accessories_size,id',
                'invoice_amount' => 'required_if:bike_or_accessory,A',
                'start_datetime' => 'required|date',
                'end_datetime' => 'required|date|after:start_datetime',
            );

            $validator = Validator::make($reservation_detail,$rules);

            if($validator->fails()){
                return $validator->errors();        
            }
            
            
            if($reservation_detail['bike_or_accessory'] == "B")
            {
                $dispatcher = app('Dingo\Api\Dispatcher');
                $priceDetails = $dispatcher->with([
                    'model_id' => $reservation_detail['model_id'], 
                    'area_id' => $reservation_detail['area_id'], 
                    'start_datetime' => $reservation_detail['start_datetime'], 
                    'end_datetime' => $reservation_detail['end_datetime'], 
                    'coupon'=>$promo_code
                ])->get('bookings/total-price');
                
                $bikeAvailability = $dispatcher->with([
                    'model_id' => $reservation_detail['model_id'], 
                    'area_id' => $reservation_detail['area_id'], 
                    'start_datetime' => $reservation_detail['start_datetime'], 
                    'end_datetime' => $reservation_detail['end_datetime']
                ])->get('check-availability');
                
                $startDateTimeCarbon = Carbon::parse($reservation_detail['start_datetime']);
                $endDateTimeCarbon = Carbon::parse($reservation_detail['end_datetime']);
                
                $weekEndHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
                   return $date->isWeekend();
                }, $endDateTimeCarbon);

                $weekDayHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
                   return $date->isWeekday();
                }, $endDateTimeCarbon);
                
                $totalHours = $weekEndHours+$weekDayHours;
                
                if($reservation_detail['fuel_included'] == true)
                {
                    $fuel_included = true;
                    $KMlimit = 0;
                }
                else
                {
                    $number_of_hours = $priceDetails['no_of_hours'];
                    $minimum_hours = $priceDetails['minimum_hours'];
                    $max_hours = max($number_of_hours,$minimum_hours);
                    
                    $fuel_included = false;
                    $KMlimit = floor($max_hours*env('KMLIMIT'));
                }
                
                if($bikeAvailability['bike_id'] != "none")
                {
                                
                    $reservationInfo = ReservationInfo::create([
                        'booking_id'=>$bookingHeader->id,
                        'bike_or_accessory'=>"B",
                        'model_id'=>$reservation_detail['model_id'], 
                        'area_id'=>$reservation_detail['area_id'], 
                        'start_datetime'=>$reservation_detail['start_datetime'], 
                        'end_datetime'=>$reservation_detail['end_datetime'],
                        'user_id'=>$user->id,
                        'creation_mode'=>$creation_mode,
                        'current_state'=>"Reservation Created",
                        'created_by'=>$createdByUser->id,
                        'creation_reason'=>1,
                        'KMlimit'=>$KMlimit,
                        'fuel_included'=>$fuel_included,
                        'weekday_hours'=>$weekDayHours,
                        'weekend_hours'=>$weekEndHours,
                        'total_hours'=>$totalHours,
                        'booking_platform'=>$booking_platform,
                        'res_lead_time'=>$startDateTimeCarbon->diffInHours(Carbon::now())
                    ]);

                    $reservationId = $reservationInfo->id;
                    
                    if($creation_mode == "S"||$creation_mode == "C")
                    {
                        $totalAmountPaid = $agreedPrice;
                        
                        $totalBookingPrice = round($totalAmountPaid/(1+($taxDetails->amount/100)),2);
                        
                        $totalTaxAmount = $totalAmountPaid - $totalBookingPrice;
                                                
                        $promoDiscount = $priceDetails['total_price'] - $totalBookingPrice;
                        $priceAfterDiscount = $totalBookingPrice;
                        
                        PricingInfo::create([
                            'bike_or_accessory'=>"B",
                            'reservation_id'=>$reservationInfo->id,
                            'pricing_id'=>$priceDetails['pricing_id'],
                            'full_price'=>$priceDetails['total_price'],
                            'promo_discount'=> $promoDiscount,
                            'price_after_discount'=>$priceAfterDiscount,
                            'promo_code'=>$promo_code,
                            'tax_id'=>1,
                            'taxable_price'=>$totalBookingPrice,
                            'tax_amount'=>$totalTaxAmount,
                            'price_after_tax'=>$totalAmountPaid
                        ]);
                        
                    }
                    else
                    {
                        $pricingInfo = PricingInfo::create([
                            'bike_or_accessory'=>"B",
                            'reservation_id'=>$reservationInfo->id,
                            'pricing_id'=>$priceDetails['pricing_id'],
                            'full_price'=>$priceDetails['total_price'],
                            'promo_discount'=>$priceDetails['total_price'] - $priceDetails['final_price'],
                            'price_after_discount'=>$priceDetails['final_price'],
                            'promo_code'=>$promo_code
                        ]);
                        array_push($pricingObjectArray,$pricingInfo);

                        $totalBookingPrice += $priceDetails['final_price'];
                    }                    
                    
                    array_push($successfulReservations,["id"=>$reservationInfo->id,"details"=>$reservation_detail]);
                }
                else
                {
                    array_push($failedReservations,$reservation_detail);
                    $reservationFailed = true;
                }
            }
            else
            {
                $accessory = AccessoriesList::where('id',$reservation_detail['accessory_model_id'])->first();
                $startDateTimeCarbon = Carbon::parse($reservation_detail['start_datetime']);
                $endDateTimeCarbon = Carbon::parse($reservation_detail['end_datetime']);
                $price = $accessory->price*($endDateTimeCarbon->diffInHours($startDateTimeCarbon));
                
                $reservationInfo = ReservationInfo::create([
                    'booking_id'=>$bookingHeader->id,
                    'reference_reservation'=>$reservationId,
                    'bike_or_accessory'=>"A",
                    'accessory_model_id'=>$reservation_detail['accessory_model_id'], 
                    'accessory_size_id'=>1, 
                    'area_id'=>$reservation_detail['area_id'], 
                    'start_datetime'=>$reservation_detail['start_datetime'], 
                    'end_datetime'=>$reservation_detail['end_datetime'],
                    'user_id'=>$user->id,
                    'creation_mode'=>$creation_mode,
                    'current_state'=>"Reservation Created",
                    'creation_reason'=>1,
                    'created_by'=>$createdByUser->id
                ]);


                $pricingInfo = PricingInfo::create([
                    'bike_or_accessory'=>"B",
                    'reservation_id'=>$reservationInfo->id,
                    'pricing_id'=>0,
                    'full_price'=>$price,
                    'promo_discount'=>$price - $reservation_detail['invoice_amount'],
                    'price_after_discount'=>$reservation_detail['invoice_amount'],
                    'promo_code'=>""
                ]);

                array_push($pricingObjectArray,$pricingInfo);

                $totalBookingPrice += $reservation_detail['invoice_amount'];
                
                array_push($successfulReservations,["id"=>$reservationInfo->id,"details"=>$reservation_detail]);
                
            }
            
            $area = Area::where('id',$reservation_detail['area_id'])->first();
            $referenceId = Self::getBookingReferenceId($area, $bookingHeader->id);
                    
        }
        
        BookingHeader::where('id',$bookingHeader->id)->update([
            "ref_id"=>$referenceId
        ]);
        
        $promoWalletAmountRedeemed = 0;
        
        foreach($financial_infos as $financial_info)
        {
            $promoWalletAmountRedeemed += Self::parseFinancialInfo($financial_info,$totalBookingPrice,$promo_code,$user,$bookingHeader->id,$createdByUser,"N",1,0,0,0);
        }
        
        if(!($creation_mode == "S"||$creation_mode == "C"))
        {

            $totalTaxableBookingPrice = $totalBookingPrice - $promoWalletAmountRedeemed;

            $totalTaxAmount = $totalTaxableBookingPrice*($taxDetails->amount/100);
            
            foreach($pricingObjectArray as $pricingInfo)
            {
                if($totalBookingPrice == 0)
                    $taxable_price = 0;
                else
                    $taxable_price = round($totalTaxableBookingPrice/$totalBookingPrice*$pricingInfo->price_after_discount,2);

                $tax_amount = round($taxable_price*$taxDetails->amount/100,2);
                $price_after_tax = $pricingInfo->price_after_discount+$tax_amount;

                PricingInfo::where('id',$pricingInfo->id)->update([
                    'tax_id'=>1,
                    'taxable_price'=>$taxable_price,
                    'tax_amount'=>$tax_amount,
                    'price_after_tax'=>$price_after_tax
                ]);
            }
        }
        
        FinancialInfo::create([
            'booking_id'=>$bookingHeader->id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$totalBookingPrice,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>2,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        FinancialInfo::create([
            'booking_id'=>$bookingHeader->id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$totalTaxAmount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>3,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        if($notes != "")
        {
            NotesInfo::create([
                'booking_id'=>$bookingHeader->id,
                'nature'=>$notesNature,
                'text'=>$notes,
                'created_by'=>$createdByUser->id
            ]);
        }
        
        if($reservationFailed)
        {
            $data = [
                "bookingId"=>$bookingHeader->id,
                "reservation_failure"=>true,
                "successfulReservations"=>$successfulReservations,
                "failed_reservations"=>$failedReservations
            ];
        }
        else
        {
            $data = [
                "bookingId"=>$bookingHeader->id,
                "reservation_failure"=>false,
                "successfulReservations"=>$successfulReservations,
                "failed_reservations"=>$failedReservations
            ];
        }
            
        return $data;
    }
    
    
    public static function editReservation($user,$creation_mode,$reservation_id,$edited_start_datetime,$edited_end_datetime,$edited_model_id,$edited_area_id,$creation_reason,$edited_invoice_amount,$promo_code,$financial_infos,$notes,$agreedPrice)
    {
                
        if(in_array($creation_mode,explode(',',env('DIRECTCREATIONMODES'))))
        {
            $createdByUser = $user;
            $notesNature = "User edit notes";
        }
        else
        {
            $createdByUser = Auth::user();
            if($creation_mode == "S")
                $notesNature = "Store edit notes";
            else
                $notesNature = "Customer Care edit notes";
        }

        $existingReservation = ReservationInfo::where('id',$reservation_id)->first();
        
        $oldPricing = PricingInfo::where('reservation_id',$existingReservation->id)->first();
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];    
        
        $startDateTimeCarbon = Carbon::parse($edited_start_datetime);
        $endDateTimeCarbon = Carbon::parse($edited_end_datetime);

        $weekEndHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
            return $date->isWeekend();
        }, $endDateTimeCarbon);

        $weekDayHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
            return $date->isWeekday();
        }, $endDateTimeCarbon);

        $totalHours = $weekEndHours+$weekDayHours;
        
        if($existingReservation['bike_or_accessory'] == "B")
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $priceDetails = $dispatcher->with([
                'model_id' => $edited_model_id,
                'area_id' => $edited_area_id, 
                'start_datetime' => $edited_start_datetime, 
                'end_datetime' => $edited_end_datetime, 
                'coupon'=>$promo_code
            ])->get('bookings/total-price');
            
            BikeAvailabilityController::addOrDeleteReservation($existingReservation->id,"delete");
            
            $bikeAvailability = $dispatcher->with([
                'model_id' => $edited_model_id,
                'area_id' => $edited_area_id, 
                'start_datetime' => $edited_start_datetime, 
                'end_datetime' => $edited_end_datetime, 
            ])->get('check-availability');
            
            if($bikeAvailability["bike_id"] == "none")
            {
                BikeAvailabilityController::addOrDeleteReservation($existingReservation->id,"add");
                $data = [
                    "message"=>"The requested model is available in the area for the dates.",
                ];
                
                return ["status"=>"error","data"=>$data];
            }
            
            $edited_fuel_included = $existingReservation->fuel_included;

            if($edited_fuel_included == true)
            {
                $fuel_included = true;
                $KMlimit = 0;
            }
            else
            {
                $number_of_hours = $priceDetails['no_of_hours'];
                $minimum_hours = $priceDetails['minimum_hours'];
                $max_hours = max($number_of_hours,$minimum_hours);

                $fuel_included = false;
                $KMlimit = floor($max_hours*env('KMLIMIT'));
            }


            $reservationInfo = ReservationInfo::create([
                'booking_id'=>$existingReservation->booking_id,
                'reference_reservation'=>$existingReservation->id,
                'bike_or_accessory'=>"B",
                'model_id'=>$edited_model_id, 
                'area_id'=>$edited_area_id, 
                'start_datetime'=>$edited_start_datetime, 
                'end_datetime'=>$edited_end_datetime,
                'user_id'=>$user->id,
                'creation_mode'=>$creation_mode,
                'creation_reason'=>(isset($creation_reason)?$creation_reason:0),
                'current_state'=>"updated reservation",
                'created_by'=>$createdByUser->id,
                'KMlimit'=>$KMlimit,
                'fuel_included'=>$fuel_included,
                'weekday_hours'=>$weekDayHours,
                'weekend_hours'=>$weekEndHours,
                'total_hours'=>$totalHours,
                'res_lead_time'=>$startDateTimeCarbon->diffInHours(Carbon::now())
            ]);
            
            ReservationInfo::where('id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString(),
                'current_state'=>"edited"
            ]);
            
            
            $priceId = $oldPricing->pricing_id;
            $fullPrice = $oldPricing->full_price;
            $promoDiscount = $oldPricing->promo_discount;
            $priceAfterDiscount = $oldPricing->price_after_discount;
                
                
            $totalAmountPaid = $agreedPrice;

            $totalBookingPrice = round($totalAmountPaid/(1+($taxDetails->amount/100)),2);

            $totalTaxAmount = $totalAmountPaid - $totalBookingPrice;

            $promoDiscount = $priceDetails['total_price'] - $totalBookingPrice;
            $priceAfterDiscount = $totalBookingPrice;

            PricingInfo::create([
                'bike_or_accessory'=>"B",
                'reservation_id'=>$reservationInfo->id,
                'pricing_id'=>$priceDetails['pricing_id'],
                'full_price'=>$priceDetails['total_price'],
                'promo_discount'=> $promoDiscount,
                'price_after_discount'=>$priceAfterDiscount,
                'promo_code'=>$promo_code,
                'tax_id'=>1,
                'taxable_price'=>$totalBookingPrice,
                'tax_amount'=>$totalTaxAmount,
                'price_after_tax'=>$totalAmountPaid
            ]);
            
            PricingInfo::where('reservation_id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString()
            ]);
            

            //$totalBookingPrice = $priceDetails['final_price'];
        }
        else
        {
            
            $accessory = AccessoriesList::where('id',$edited_accessory_model_id)->first();
            $startDateTimeCarbon = Carbon::parse($edited_start_datetime);
            $endDateTimeCarbon = Carbon::parse($edited_end_datetime);
            $price = $accessory->price*($endDateTimeCarbon->diffInHours($startDateTimeCarbon));

            $reservationInfo = ReservationInfo::create([
                'booking_id'=>$existingReservation->booking_id,
                'reference_reservation'=>$existingReservation->reference_reservation,
                'bike_or_accessory'=>"A",
                'accessory_model_id'=>$edited_accessory_model_id, 
                'accessory_size_id'=>1, 
                'area_id'=>$edited_area_id, 
                'start_datetime'=>$edited_start_datetime, 
                'end_datetime'=>$edited_end_datetime,
                'user_id'=>$user->id,
                'creation_mode'=>$creation_mode,
                'creation_reason'=>(isset($creation_reason)?$creation_reason:0),
                'current_state'=>"Reservation Created",
                'created_by'=>$createdByUser->id,
                'KMlimit'=>0,
                'fuel_included'=>0,
                'weekday_hours'=>$weekDayHours,
                'weekend_hours'=>$weekEndHours,
                'total_hours'=>$totalHours,
                'res_lead_time'=>$startDateTimeCarbon->diffInHours(Carbon::now())
            ]);
            
            ReservationInfo::where('id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString(),
                'current_state'=>"edited"
            ]);


            $pricingInfo = PricingInfo::create([
                'bike_or_accessory'=>"B",
                'reservation_id'=>$reservationInfo->id,
                'pricing_id'=>0,
                'full_price'=>$price,
                'promo_discount'=>$price - $edited_invoice_amount,
                'price_after_discount'=>$edited_invoice_amount,
                'promo_code'=>""
            ]);
            
            PricingInfo::where('reservation_id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString()
            ]);

            $totalBookingPrice = $edited_invoice_amount;

        }
        
        
        if($totalBookingPrice > $existingReservation->price_after_discount)
        {
            $extraAmount = $totalBookingPrice - $existingReservation->price_after_discount;
        }
        else
        {
            $extraAmount = 0;
        }
        
        foreach($financial_infos as $financial_info)
        {
            Self::parseFinancialInfo($financial_info,$extraAmount,$promo_code,$user,$existingReservation->booking_id,$createdByUser,"E",4,$reservationInfo->id,$existingReservation->id,$reservationInfo->id);
        }
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"E",
            'reservation_id'=>$reservationInfo->id,
            'old_reservation'=>$existingReservation->id,
            'edited_reservation'=>$reservationInfo->id,
            'amount'=>$totalBookingPrice,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>5,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"E",
            'reservation_id'=>$reservationInfo->id,
            'old_reservation'=>$existingReservation->id,
            'edited_reservation'=>$reservationInfo->id,
            'amount'=>$totalTaxAmount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>6,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"C",
            'new_or_edit'=>"D",
            'reservation_id'=>$existingReservation->id,
            'amount'=>$oldPricing->price_after_discount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>7,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"C",
            'new_or_edit'=>"D",
            'reservation_id'=>$existingReservation->id,
            'amount'=>$oldPricing->tax_amount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>8,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        if($notes != "")
        {
            NotesInfo::create([
                'booking_id'=>$existingReservation->booking_id,
                'nature'=>$notesNature,
                'text'=>$notes,
                'created_by'=>$createdByUser->id
            ]);
        }
        
        DeliveryInfo::where('id',$existingReservation->id)->update([
            "reservation_id"=>$reservationInfo->id
        ]);
        
        ReturnInfo::where('id',$existingReservation->id)->update([
            "reservation_id"=>$reservationInfo->id
        ]);
        
        ReservationInfo::where('reference_reservation',$existingReservation->id)->update([
            "reference_reservation"=>$reservationInfo->id
        ]);
        
        
        $data = [
            "new_reservation_id"=>$reservationInfo->id,
            "message"=>"Reservation edited successfully"
        ];
        
        return ["status"=>"success","data"=>$data];
        
    }
    
    
    public static function deleteReservation($user_id,$creation_mode,$reservation_id,$refund_financial_info,$notes)
    {
       
        if(isset($user_id))
        {
            $user = User::where('id',$user_id)->first();
        }
        else
        {
            $user = UserController::getUserByToken();
        }
                
        if(in_array($creation_mode,explode(',',env('DIRECTCREATIONMODES'))))
        {
            $createdByUser = $user;
            $notesNature = "User deletion notes";
        }
        else
        {
            $createdByUser = Auth::user();
            if($creation_mode == "S")
                $notesNature = "Store deletion notes";
            else
                $notesNature = "Customer Care deletion notes";
        }

        $existingReservation = ReservationInfo::where('id',$reservation_id)->where('deleted',0)->first();
        
        if(!$existingReservation)
        {
            return ["status"=>"error","data"=>"Booking already deleted"];
        }
        
        $oldPricing = PricingInfo::where('reservation_id',$existingReservation->id)->first();
            

        $user = User::where('id',$existingReservation->user_id)->first();
        
        if($existingReservation['bike_or_accessory'] == "B")
        {
            
            ReservationInfo::where('id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString(),
                'current_state'=>"deleted"
            ]);
            
            PricingInfo::where('reservation_id',$existingReservation->id)->update([
                'deleted'=>1,
                'deleted_by'=>$createdByUser->id,
                'deleted_at'=>Carbon::now()->toDateTimeString()
            ]);

        }
        
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"C",
            'new_or_edit'=>"D",
            'reservation_id'=>$existingReservation->id,
            'amount'=>$oldPricing->price_after_discount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>10,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        FinancialInfo::create([
            'booking_id'=>$existingReservation->booking_id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"C",
            'new_or_edit'=>"D",
            'reservation_id'=>$existingReservation->id,
            'amount'=>$oldPricing->tax_amount,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>11,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        if(!isset($refund_financial_info))
        {
            $refund_financial_info = [];
        }
        else
        {
            $refund_financial_info = $refund_financial_info;
        }
        
        foreach($refund_financial_info as $financial_info)
        {
            FinancialInfo::create([
                'booking_id'=>$existingReservation->booking_id,
                'user_id'=>$user->id,
                'debit_or_credit'=>"D",
                'new_or_edit'=>"D",
                'reservation_id'=>$existingReservation->id,
                'amount'=>$financial_info->amount,
                'payment_method'=>$financial_info->payment_method,
                'payment_id'=>$financial_info->payment_id,
                'payment_nature'=>9,
                'status'=>"na",
                'created_by'=>$createdByUser->id
            ]);
        }
        
        
        if($notes != "")
        {
            NotesInfo::create([
                'booking_id'=>$existingReservation->booking_id,
                'nature'=>$notesNature,
                'text'=>$notes,
                'created_by'=>$createdByUser->id
            ]);
        }
        
        
        $data = [
            "message"=>"Reservation deleted successfully"
        ];
        
        return ["status"=>"success","data"=>$data];
        
    }
    
    
    
    public static function parseFinancialInfo($financial_info,$totalBookingPrice,$promo_code,$user,$bookingHeaderId,$createdByUser,$newOrEdit,$payment_nature,$reservationId,$oldResevationId,$editedReservationId)
    {
        $promoWalletAmountRedeemed =0;
        
        if(in_array($financial_info['payment_method'],explode(',',env('OFFLINEPAYMENTS'))))
        {
            $status = "Paid";
        }
        else
        {
            $status = $financial_info['payment_status'];   
        }

        if($financial_info['payment_method']==2)
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $walletDetails = $dispatcher->with([
                'category'=>"AtoA",
                'device'=>"MOBILE",
                'amount_to_apply'=>$financial_info['amount'],
                'price'=>$totalBookingPrice,
                'coupon'=>$promo_code,
                'user_id'=>$user->id
            ])->get('wallet/function_apply_amount');


            $amountFinancialInfo = $walletDetails['applied_amount'];
            $promoWalletAmountRedeemed = $walletDetails['promotional_credit_used'];
            $nonpromoWalletAmountRedeemed = $walletDetails['non_promotional_credit_used'];


            $walletTxnId = WalletController::createWalletBalanceEntry($user->id,$amountFinancialInfo,"Redeemed for a boooking",$bookingHeaderId,$createdByUser->id);

            if($promoWalletAmountRedeemed>0)
            {
                $financialInfo = FinancialInfo::create([
                    'booking_id'=>$bookingHeaderId,
                    'user_id'=>$user->id,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>$newOrEdit,
                    'amount'=>$promoWalletAmountRedeemed,
                    'payment_method'=>12,
                    'payment_id'=>$walletTxnId,
                    'payment_nature'=>$payment_nature,
                    'status'=>$status,
                    'created_by'=>$createdByUser->id
                ]);
            }

            if($nonpromoWalletAmountRedeemed>0)
            {
                $financialInfo = FinancialInfo::create([
                    'booking_id'=>$bookingHeaderId,
                    'user_id'=>$user->id,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>$newOrEdit,
                    'amount'=>$nonpromoWalletAmountRedeemed,
                    'payment_method'=>13,
                    'payment_id'=>$walletTxnId,
                    'payment_nature'=>$payment_nature,
                    'status'=>$status,
                    'created_by'=>$createdByUser->id
                ]);
            }

        }
        else
        {
            $amountFinancialInfo = $financial_info['amount'];
            $financialInfo = FinancialInfo::create([
                'booking_id'=>$bookingHeaderId,
                'user_id'=>$user->id,
                'debit_or_credit'=>"C",
                'new_or_edit'=>$newOrEdit,
                'amount'=>$amountFinancialInfo,
                'payment_method'=>$financial_info['payment_method'],
                'payment_id'=>$financial_info['payment_id'],
                'payment_nature'=>$payment_nature,
                'status'=>$status,
                'created_by'=>$createdByUser->id
            ]);
        }
        
        return $promoWalletAmountRedeemed;
        
    }
    
    public static function getResevationsForErp($deliverOrReturn,$areaIds,$pending,$todayOrTom)
    {
        if($todayOrTom == "today")
        {
            $start = Carbon::today();
            $end = Carbon::today()->addDay();
        }
        else
        {
            $today = Carbon::today();
            $start = Carbon::today()->addDay();
            $end = $today->addDay(2);
        }
        
        if($deliverOrReturn == "D")
        {
            if($pending == "off")
            {
                $reservationInfos = ReservationInfo::orderBy('start_datetime', 'asc')->where('start_datetime', '>=', $start->toDateTimeString())->where('start_datetime', '<=', $end->toDateTimeString())->whereIn('area_id',$areaIds)->where('deleted',0)->where('bike_or_accessory',"B")->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->get();
            }
            else
            {
                $reservationInfos = ReservationInfo::orderBy('start_datetime', 'desc')->has('deliveryInfo', '<', 1)->whereIn('area_id',$areaIds)->where('deleted',0)->where('bike_or_accessory',"B")->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->get();
            }
        }
        else
        {
            if($pending == "off")
            {
                $reservationInfos = ReservationInfo::orderBy('end_datetime', 'asc')->where('end_datetime', '>=', $start->toDateTimeString())->where('end_datetime', '<=', $end->toDateTimeString())->whereIn('area_id',$areaIds)->where('deleted',0)->where('bike_or_accessory',"B")->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->get();
            }
            else
            {
                $reservationInfos = ReservationInfo::orderBy('end_datetime', 'desc')->has('returnInfo', '<', 1)->whereIn('area_id',$areaIds)->where('deleted',0)->where('bike_or_accessory',"B")->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->get();
            }
        }
        
        $dataArray = [];
        
        foreach($reservationInfos as $reservationInfo)
        {
            if($deliverOrReturn == "D")
            {
                if(count($reservationInfo->deliveryInfo) == 1)
                {
                    $status = "delivered";
                }
                else
                {
                    $status = "pending";
                }
            }
            else
            {
                if(count($reservationInfo->returnInfo) == 1)
                {
                    $status = "returned";
                }
                else
                {
                    $status = "pending";
                }
            }
            
            
            $data = [
                "id"=>$reservationInfo->id,
                "reference_id"=>$reservationInfo->bookingHeader->ref_id,
                "customer_first_name"=>$reservationInfo->user->first_name,
                "model_name"=>$reservationInfo->model->bike_model,
                "model_id"=>$reservationInfo->model_id,
                "start_datetime"=>$reservationInfo->readableStartDateTime(),
                "end_datetime"=>$reservationInfo->readableEndDateTime(),
                "duration"=>$reservationInfo->getDuration(),
                "status"=>$status
            ];
            
            array_push($dataArray,$data);
        }
        
        return $dataArray;
    }
    
    public static function getBookingReferenceId($area, $bookingId)
    {
        $code = env('BOOKINGPREFIX').$area->code;
        $bookingCode = sprintf("%'.06d", $bookingId);
        return $code.$bookingCode;
    }
    
    
    
}
