<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use App\Models\User;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Availability;
use App\Models\Available;
use App\Models\Capacity;
use App\Models\Service;
use App\Models\Occupancy;
use App\Models\ServiceBlock;
use App\Models\City;
use App\Models\Helmet;
use App\Models\Jacket;
use App\Models\Price;
use App\Models\Image;
use App\Models\Wallet;
use App\Models\UserDocImage;
use App\Models\Parameters;
use App\Models\PaymentMethods;

use DB;
use Mail;
use App\Transformers\BookingTransformer;
use Carbon\Carbon;
use Auth;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\BikeAvailabilityController;
use App\Jobs\ModelHourArea;

use Excel;
use Zipper;
use Curl;




use App\Models\Vendor;
use App\Models\Partner;

class BookingController extends AppController
{
  use Helpers;

  function __construct(BookingTransformer $bookingTransformer){
    $this->bookingTransformer = $bookingTransformer;
  }
    
  public function index(){
    $models = BikeModel::where('status', 1)->get();
    $areas = Area::where('status', 1)->get();
    $today = Carbon::now();
    //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
    $bookings = Booking::orderBy('id', 'desc')->where('booking_status',1)->take(100)->with(['reservationInfo','reservationInfo.bookingHeader','reservationInfo.bookingHeader.financialInfos'])->get();
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);
      // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
      // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
    }
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    
    $paymentMethods = PaymentMethods::where('display',1)->get();
      
    $editReasons = DB::table('creation_reason')->where('display_edit',1)->get();
   
    return view('admin.booking.index',compact('userList','models', 'areas', 'bookings','paymentMethods','editReasons'));
  }
  public function search(Request $request){
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::where('status', 1)->get();
    $areas = Area::where('status', 1)->get();
    $today = Carbon::now();
    $bookings = Booking::search($request->id)->get();
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);
      // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
      // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
    }
    $userList = User::orderBy('id', 'asc')->take(20)->get();
   
    $paymentMethods = PaymentMethods::where('display',1)->get();
      
    $editReasons = DB::table('creation_reason')->where('display_edit',1)->get();
   
    return view('admin.booking.index',compact('userList','models', 'areas', 'bookings','paymentMethods','editReasons'));
  }
    
 public function indexdelete(){
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::where('status', 1)->get();
    $areas = Area::where('status', 1)->get();
    $today = Carbon::now();
    //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
    $bookings = Booking::orderBy('created_at', 'desc')->where('booking_status',1)->get();
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);
      // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
      // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
    }
   
    return view('admin.deletebooking.index',compact('userList','models', 'areas', 'bookings'));
  }
    
    
  public function indexParse(){
    
    
    $bookings = Booking::orderBy('id', 'desc')->take(5000)->get();
    foreach ($bookings as $booking) {
        

      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);


      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
        
        
    }
   
    return view('admin.bookingParse.index',compact('bookings'));
  }
    
    
  public function add(Request $request){
    $rules = array(
      'model' => 'required',
      'area' => 'required',
      'startDate' => 'required',
      'startTime' => 'required',
      'endDate' => 'required',
      'endTime' => 'required',
        'amount' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);
    
    if($request->source == "ERP")
    {
        $source = "erp";
        $leadSource = "In-Store";
    }
    else
    {
        $source = "admin";
        $leadSource = "Inside-Sales";
    }
    
    if($validator->fails()){
      return redirect('/'.$source.'/booking')->withErrors($validator->errors());        
    }
      if($request->customer == env('RESERVATIONSID'))
      {
          $rules = array(
          'customer_email' => 'required',
          'customer_mobile' => 'required',
          'customer_fname' => 'required',
          'customer_lname' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/'.$source.'/booking')->withErrors($validator->errors());        
        }
      }
    
      
    $actual_user = Auth::user();
      
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $startTime = $request->startTime;
    $endTime = $request->endTime;
    
    $startDateCarbon = Carbon::parse($startDate);
    $endDateCarbon = Carbon::parse($endDate);
    $no_of_days = $startDateCarbon->diffInDays($endDateCarbon);
    if($no_of_days>90){  
        return redirect('/'.$source.'/booking')->withErrors(['block'=>['Block too long.']]);
    }
    if((Carbon::parse($startDate." ".$startTime)->diffInHours(Carbon::parse($endDate." ".$endTime)))<1)
    {
        return redirect('/'.$source.'/booking')->withErrors(['block'=>['End Date Time has to be greater than Start Date Time.']]);
    }
    $userId = $request->customer;
    $modelId = $request->model;
    $areaId = $request->area;
    $area = Area::where('id', $areaId)->where('status', 1)->first();
    $amount = $request->amount;
    $city = City::where('id', $area['city_id'])->where('status', 1)->first();
    $raw_data = '';
    $dispatcher = app('Dingo\Api\Dispatcher');
    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,
                                        'start_time' => $startTime, 'end_time' => $endTime,
                                        'model_id' => $modelId, 'area_id' => $areaId, 'city_id' => $city['id']
                                      ])->get('bookings/price-and-check-availability');
      
    $number_of_hours = $availability['number_of_hours'];
    $minimum_hours = $availability['minimum_hours'];

    $max_hours = max($number_of_hours,$minimum_hours);
      
    $model = BikeModel::where('id',$modelId)->first();

    $modelImage = Image::where('id', $model['thumbnail_img_id'])->first();
    $modelName = $model['bike_model'];
    
    $modelThumbnail = $modelImage['full'];
    $extrakmprice = Price::getExtraKMPriceformodel($modelId);
    $freekm = $max_hours*env('KMLIMIT');

    $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";

    if($request->delivery_area == "same")
    {
        $delivered_at_area = $areaId;
    }
    else
    {
        $delivered_at_area = $request->delivery_area;
    }
      
    if(in_array($areaId,explode(",",env('AREAINCLUDEDFUEL'))) && ($max_hours<24))
    {
        $fuelIncluded = 1;
    }
    else
    {
        $fuelIncluded = 0;
    }
    if($availability['bike_id'] != "none"){
        $status = 'COMPLETE';
        $booking = $dispatcher->with([
            'start_date' => $startDate, 
            'end_date' => $endDate, 
            'start_time' => $startTime, 
            'end_time' => $endTime, 
            'user_id' => $userId,
            'actual_user_id'=> $actual_user->id, 
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'status' => $status,
            'total_price' => $request->amount, 
            'note' => $request->customer_note,
            'raw_data' => $raw_data,
            'delivered_at_area'=>$delivered_at_area,
            'fuel_included'=>$fuelIncluded
        ])->get('bookingsweb');


        $bookingId = BookingController::getBookingReferenceId($area, $booking['id']);
        
        $non_revenue_ids = explode(',',env('NONREVENUEIDS'));
        
        if(in_array($userId,$non_revenue_ids))
        {
            Booking::where('id', $booking['id'])->update(['reference_id' => $bookingId]);
            
        }
        else
        {
            Booking::where('id', $booking['id'])->update(['reference_id' => $bookingId,'assisted'=>1,'KMlimit'=>$freekm]);
            
            Booking::where('id',$booking['id'])->update(['payment_method'=>$request->payment_method,'pg_txn_id'=>$request->payment_id,'bank_txn_id'=>$request->payment_id,'order_id'=>$request->payment_id,'lead_source'=>$leadSource]);
        }
        $booking = Booking::where('id',$booking['id'])->first();
        
        
        
        
        if($request->customer == env('RESERVATIONSID'))
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $createUser = $dispatcher->with(['first_name' => $request->customer_fname, 'last_name' => $request->customer_lname,'mobile' => $request->customer_mobile, 'email' => $request->customer_email,'reservationId'=>$bookingId])->post('signupFromAdmin');
            
            
            $sdCarbonObj = Carbon::parse($booking->start_datetime);
            $edCarbonObj = Carbon::parse($booking->end_datetime);
            $hours = $edCarbonObj->diffInHours($sdCarbonObj);

            $ss = $hours*60*60;
            $s = $ss%60;
            $m = floor(($ss%3600)/60);
            $h = floor(($ss%86400)/3600);
            $d = floor(($ss%2592000)/86400);
            $M = floor($ss/2592000);

            $duration = $d. " days, .".$h." hours";
            $unit = "Days";
            
            $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
            
            $manualFile = str_replace(env('HOME_URL'),"",$model->manual_url);
            $manualFile = "/var/www/html/public".$manualFile;

                        
            $data = [
                        'fromDate' => $startDate, 
                        'toDate' => $endDate, 
                        'fromTime' => $startTime, 
                        'toTime' => $endTime,  
                        'readable_start_datetime'=>Self::readableDateTime($startDate." ".$startTime),
                        'readable_end_datetime'=>Self::readableDateTime($endDate." ".$endTime),
                        'areaName' => $area->area, 
                        'amount' => $amount,
                        'price_after_discount'=>round($amount/(1+($taxDetails->amount/100)),2),
                        'tax'=>round(($amount - ($amount/(1+($taxDetails->amount/100)))),2),
                        'price_with_tax'=>$amount,
                        'duration' => $duration, 
                        'areaAddress' => $area->address, 
                        'userFname' => $request->customer_fname, 
                        'modelName' => $modelName, 
                        'unit' => $unit, 
                        'modelThumbnail' => $modelThumbnail, 
                        'userEmail' => $request->customer_email, 
                        'userMob' => $request->customer_mobile, 
                        'note' => '', 
                        'bookingId' => $bookingId, 
                        'kmLimitMessage'=>$kmLimitMessage, 
                        'areaMapLink'=>$area->maps_link,
                        'tax_name' => $taxDetails->tax_name,
                        'tax_amount' => $taxDetails->amount,
                        'manualPresent'=>($model->manual_url==""?false:true),
                        'manualFile'=>$manualFile
                    ];

                        Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
                            $message->to($data['userEmail']);
                            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                            $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
                            if($data['manualPresent'])
                                $message->attach($data['manualFile']);
                        });
            
            $nowCarbon = Carbon::now();
            $res_lead_time = $sdCarbonObj->diffInHours($nowCarbon);
            
            if($res_lead_time<(env('DOCUMENTSMSLEADTIME')+1))
            {
                $dispatcher = app('Dingo\Api\Dispatcher');

                $sendDocsSMS = $dispatcher->with([
                    'booking_id'=>$booking->id
                ])->get('send-document-sms');

            }
        }

        return redirect('/'.$source.'/booking')->withErrors(['Booking has been made successfully Reference ID: '.$bookingId]);
    }
    else
        return redirect('/'.$source.'/booking')->withErrors(['Looks like the bikes are not available.']);
  }

  public function delete($booking_id, Request $request){
     
    $bookingParse=Booking::where('id',$booking_id)->first();
    
    $bookingParse['reason_for_delete'] = $request->reason;
    $bookingParse['deleted_by'] = Auth::user()->id;
    $bookingParse['deleted_at'] = date('Y-m-d H:i:s',strtotime("now"));
    
    $deleted_booking = DB::table('deleted_bookings')->where('id',$booking_id)->first();
    if(!$deleted_booking)
    {
        DB::table('deleted_bookings')->insert($bookingParse->toArray());  
        Booking::where('id',$booking_id)->update(['booking_status'=>0]);
      
    
        BikeAvailabilityController::addOrDeleteBooking($booking_id,"delete");

        Booking::destroy($booking_id);
        $availability = Availability::where('booking_id', '=', $booking_id)->delete();
    }
    
      
    return redirect('/admin/booking');
  }
    
  public function unblock(Request $request){
      
     
    $bookingParse=Booking::where('id',$request->booking_id)->first();
    
    $bookingParse['reason_for_delete'] = "service_block";
    $bookingParse['deleted_by'] = Auth::user()->id;
    $bookingParse['deleted_at'] = date('Y-m-d H:i:s',strtotime("now"));
           
    
    $deleted_booking = DB::table('deleted_bookings')->where('id',$request->booking_id)->first();
    if(!$deleted_booking)
    {
        DB::table('deleted_bookings')->insert($bookingParse->toArray());  
        Booking::where('id',$request->booking_id)->update(['booking_status'=>0]);
      
    
        BikeAvailabilityController::addOrDeleteBooking($request->booking_id,"delete");

        Booking::destroy($request->booking_id);
        $availability = Availability::where('booking_id', '=', $request->booking_id)->delete();
    }
      
    $data = ['id' => $request->booking_id];
      
    $start_datetime = new \DateTime($request->start_datetime);
    $end_datetime = new \DateTime();
    $serviceBlock_end_datetime = new \DateTime($request->end_datetime);

    if($start_datetime<$end_datetime && $end_datetime<$serviceBlock_end_datetime)
    {

        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $area = Area::where('id',$areaId)->first();
        $cityId =$area->city_id;

        $startDate = $start_datetime->format('Y-m-d');
        $startTime = $start_datetime->format('H:i:s');

        $endDate = $end_datetime->format('Y-m-d');
        $endTime = $end_datetime->format('H:00:00');
        $note = $request->note;

        $dispatcher = app('Dingo\Api\Dispatcher');
        
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId,
            'area_id' => $areaId, 
            'city_id' => $cityId,
            'start_datetime' => $startDate." ".$startTime, 
            'end_datetime' => $endDate." ".$endTime, 
            'coupon' => ''
        ])->get('bookings/price-and-check-availability');

        $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];
        
        
        if($bikeAvailabilityWithPrice['bike_id'] != "none"){


            $booking = $dispatcher->with([
                'start_date' => $startDate, 
                'end_date' => $endDate, 
                'start_time' => $startTime, 
                'end_time' => $endTime, 
                'user_id' => 2, 
                'actual_user_id'=>$request->actual_user_id, 
                'model_id' => $modelId, 
                'area_id' => $areaId, 
                'status' => 'COMPLETE',
                'total_price' => 0,
                'pg_status' => '', 
                'pg_txn_id' => '', 
                'bank_txn_id' => '', 
                'order_id' => '', 
                'pg_mode' => '', 
                'note' => $note, 
                'pg_txn_amount' => 0, 
                'raw_data' => '', 
                'coupon'=>'',
                'payment_method'=>'',
                'kmlimit'=>''
            ])->get('bookingsweb');
            
            
            ServiceBlock::where('id',$request->id)->update(['booking_id'=>$booking['id']]);

            //Emails to Ops team & Blocking after booking is unblocked for the Service Block

            $serviceBlock=ServiceBlock::where('id',$request->id)->first();

            $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail(),'unblocked_by'=>$serviceBlock->getUnblockedByEmail()];

            if(env('SERVER_STATUS')=='live')
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('operations@wickedride.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject($data['unblocked_by'].' unblocked  '.$data['bike_number']);
                });


                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('blocking@wickedride.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject($data['unblocked_by'].' unblocked  '.$data['bike_number']);
                });

            }
            else
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('varunagni@gmail.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject($data['unblocked_by'].' unblocked  '.$data['bike_number']);
                });
            }
        
        }
    }
    return $this->response->array($data);
  }

  public function checkAdminBikeAvailability(Request $request)
  {
    $fromDate = $request->fromDate;
    $toDate = $request->toDate;

    $fmdtArr = explode(" ", $fromDate);
    $todtArr = explode(" ", $toDate);

    $fm = $fmdtArr[0];
    $to = $todtArr[0];

    $fts = strtotime($fm);
    $tts = strtotime($to);

    $from_date = date('Y-m-d', $fts);
    $to_date = date('Y-m-d', $tts);

    $bike = new Bike();
    $availability = $bike->checkBikeAvailability($from_date, $to_date, $request->modelId, $request->areaId);

    if($availability)
        return 1;
    else
        return 0;
  }

 public function create(Request $request){
    $rules = array(
      'start_date' => 'required',
      'end_date' => 'required',
      'start_time' => 'required',
      'end_time' => 'required',
        'model_id' => 'required',
        'area_id' => 'required'
    );
    $validator = Validator::make($request->all(),$rules);
    if($validator->fails()){
      return $this->respondWithValidationError($validator->errors());
    }
        
        
    $user = User::where('id', $request->user_id)->first();
 
    $userActual = User::where('id', $request->actual_user_id)->first();
    if(!$userActual)
    {
        $userActual = User::where('id', env('RESERVATIONSID'))->first();
    }
    
    $startDateTimeCarbon = Carbon::parse($request->start_date.' '.$request->start_time);
    $endDateTimeCarbon = Carbon::parse($request->end_date.' '.$request->end_time);
    
    $startDateTime = Carbon::parse($request->start_date.' '.$request->start_time)->toDateTimeString();
    $endDateTime = Carbon::parse($request->end_date.' '.$request->end_time)->toDateTimeString();
     
     
    $instruction = "Carry your DL. Carry your Helmet.";
    $status = "COMPLETE";
        
    $dispatcher = app('Dingo\Api\Dispatcher');
            
    $areaById = Area::where('id', $request->area_id)->where('status', 1)->first();
    $cityById = City::where('id', $areaById['city_id'])->where('status', 1)->first();

    $availability = $dispatcher->with(['start_date' => $request->start_date, 
                                       'end_date' => $request->end_date,
                                       'start_time' => $request->start_time, 
                                       'end_time' => $request->end_time,
                                       'model_id' => $request->model_id, 
                                       'area_id' => $request->area_id
                                    ])->get('bookings/price-and-check-availability');

    $bike_id = $availability['bike_id'];
            
       
     
 
    $dispatcher = app('Dingo\Api\Dispatcher');
    $dispatcherData = ['start_date' => $request->start_date, 'end_date' => $request->end_date, 'start_time' => $request->start_time, 'end_time' => $request->end_time, 'model_id' => $request->model_id,'area_id' => $request->area_id];
    $bikeAvailability = $dispatcher->with($dispatcherData)->get('is_available');
     
    
    
    if (!$bikeAvailability['available'])
      return $this->response->array(['error' => 'Bike is not available.']);
    if (isset($bikeAvailability['bike_id']))
      $bike_id = $bikeAvailability['bike_id'];
    else
      return $this->response->array(['error' => 'Bike no longer available.']);
    
    $model = BikeModel::where('id',$request->model_id)->first();
    $area = Area::where('id',$request->area_id)->first();
    
     
    $intKMlimit = (int)$request->kmlimit;
     
    $nowCarbon = Carbon::now();
    $res_lead_time = $startDateTimeCarbon->diffInHours($nowCarbon);
    
    $hours = Self::weekDayAndWeekEndHours($startDateTimeCarbon,$endDateTimeCarbon);
    $weekDayHours = $hours['weekDayHours'];
    $weekEndHours = $hours['weekEndHours'];
    
    if($request->delivered_at_area)
    {
        $delivered_at_area = $request->delivered_at_area;
    }
    else
    {
        $delivered_at_area = $request->area_id;
    }
    
    if(in_array($area->id,explode(",",env('AREAINCLUDEDFUEL'))) && ($endDateTimeCarbon->diffInHours($startDateTimeCarbon)<24))
    {
        $fuelIncluded = 1;
    }
    else
    {
        $fuelIncluded = 0;
    }
    
    $booking = Booking::create([
        'start_datetime' => $startDateTime, 
        'end_datetime' => $endDateTime,
        'user_id' => $user['id'], 
        'bike_id' => $bike_id, 
        'total_price' => $request->total_price, 
        'pg_status' => $request->pg_status, 
        'pg_txn_id' => $request->pg_txn_id, 
        'bank_txn_id' => $request->bank_txn_id, 
        'order_id' => $request->order_id, 
        'pg_mode' => $request->pg_mode, 
        'pg_txn_amount' => $request->pg_txn_amount, 
        'raw_data' => $request->raw_data,
        'status' => $status, 
        'note' => $request->note, 
        'promocode' => $request->coupon,
        'created_at_ts'=>$nowCarbon->timestamp,
        'createdBy'=>$userActual->id,
        'payment_method'=>$request->payment_method,
        'active'=>1,
        'weekday_hours'=>$weekDayHours,
        'weekend_hours'=>$weekEndHours,
        'total_hours'=>($weekDayHours+$weekEndHours),
        'KMlimit'=>$intKMlimit,
        'area_id'=>$request->area_id,
        'model_id'=>$request->model_id,
        'res_lead_time'=>$res_lead_time,
        'delivered_at_area'=>$delivered_at_area,
        'wallet_amount_applied'=>$request->applied_wallet_amount!=null?$request->applied_wallet_amount:0,
        'fuel_included'=>$fuelIncluded
    ]);
     
    
     
    $bookingId = BookingController::getBookingReferenceId($area, $booking['id']);
     
    if(isset($request->applied_wallet_amount))
    {
        if($request->applied_wallet_amount>0)
        {
            
            $response = WalletController::createWalletBalanceEntry($user['id'],$request->applied_wallet_amount,"Redeemed for a booking",$booking['id'],$userActual->id);
            
            
        }
    }
     
    $fullPriceAvailability = $dispatcher->with(['start_date' => $request->start_date, 'end_date' => $request->end_date,'start_time' => $request->start_time, 'end_time' => $request->end_time,'model_id' => $request->model_id, 'area_id' => $request->area_id])->get('bookings/price-and-check-availability');

    $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
     
    Booking::where('id', $booking['id'])->update(['reference_id' => $bookingId,'full_price'=>round($fullPriceAvailability['total_price']*round((1+($taxDetails->amount/100)),2))]);
    
    BikeAvailabilityController::addOrDeleteBooking($booking['id'],"add");
    
    if($res_lead_time<(env('DOCUMENTSMSLEADTIME')+1)&&$user['id']>25)
    {
        $dispatcher = app('Dingo\Api\Dispatcher');

        $sendDocsSMS = $dispatcher->with([
            'booking_id'=>$booking['id']
        ])->get('send-document-sms');
        
    }
     
    $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('area',$request->area_id)->get();
    if(count($kiranaAreaRelations) != 0)
    {
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');
        
        $currentBooking = Booking::where('id', $booking['id'])->first();
        
        $smsMessage = "Booking ID ".$bookingId." created for your store. The user is ".$user->first_name." ".$user->last_name." Ph - ".$user->mobile_num." The booking is for ".$currentBooking->getBikeModelNameThis()." at ".$currentBooking->getAreaNameThis()." from ".$currentBooking->getReadableDateTime($currentBooking->start_datetime)." to ".$currentBooking->getReadableDateTime($currentBooking->end_datetime);
        
        foreach($kiranaAreaRelations as $kiranaAreaRelation)
        {
            $kiranaUser = User::where('id',$kiranaAreaRelation->user)->first();
            if(!$kiranaUser)
            {
            }
            else
            {
                $phone_number = str_replace("+","",$kiranaUser->mobile_num);
                $phone_number = str_replace(" ","",$phone_number);
                $phone_number = str_replace("-","",$phone_number);
                $phone_number = str_replace("(","",$phone_number);
                $phone_number = str_replace(")","",$phone_number);
                    
                if(env('TESTSMS')!=true)
                {
                    $response = Curl::to($smsGatewayUrl)->withData(array(
                        'authkey' => $smsGatewayAPIKey, 
                        'mobiles'=>$phone_number, 
                        'message'=>$smsMessage, 
                        'sender'=>$smsSenderId, 
                        'route'=>4, 
                        'country'=>91, 
                        'response'=>'json'
                    ))->get();
                }
            }
        }
    }
    

    $data = ['id' => $booking['id'], 'reference_id' => $bookingId, 'instructions' => $instruction];
    return $this->response->array($data);
  }

    public function editBooking($id, Request $request)
    {
        $rules = array(
            'model' => 'required',
            'area' => 'required',
            'startDate' => 'required',
            'startTime' => 'required',
            'endDate' => 'required',
            'endTime' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());        
        }
        
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $startTime = $request->startTime;
        $endTime = $request->endTime;

        
        if((Carbon::parse($startDate." ".$startTime)->diffInHours(Carbon::parse($endDate." ".$endTime)))<1)
        {
            return redirect()->back()->withErrors(['block'=>['End Date Time has to be greater than Start Date Time.']]);
        }
        
        
        $booking = Booking::where('id',$id)->first();
            if(!$booking)
                return redirect()->back()->withErrors(['The booking to be edited not found. Please refresh the screen.']);
            
        BikeAvailabilityController::addOrDeleteBooking($booking->id,"delete");
        
        $modelId = $request->model;
        $areaId = $request->area;    
        
        $startDateTimeCarbon = Carbon::parse($startDate." ".$startTime);
        $endDateTimeCarbon = Carbon::parse($endDate." ".$endTime);
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'start_date' => $startDate, 
            'end_date' => $endDate,
            'start_time' => $startTime, 
            'end_time' => $endTime, 
        ])->get('bookings/price-and-check-availability');

        if($bikeAvailabilityWithPrice['bike_id'] != "none")
        {
            if($request->delivery_area  != "same")
            {
                $delivered_at_area = $request->delivery_area;
            }
            else
            {
                $delivered_at_area = $request->area;
            }
            
            $number_of_hours = $bikeAvailabilityWithPrice['number_of_hours'];
            $minimum_hours = $bikeAvailabilityWithPrice['minimum_hours'];
            $max_hours = max($number_of_hours,$minimum_hours);
            $freekm = $max_hours*env('KMLIMIT');
            
            $hours = Self::weekDayAndWeekEndHours($startDateTimeCarbon,$endDateTimeCarbon);
            $weekDayHours = $hours['weekDayHours'];
            $weekEndHours = $hours['weekEndHours'];
            
            DB::table('edited_bookings')->insert([$booking->toArray()]);
            
            Booking::where('id',$id)->update([
                'start_datetime'=>$startDateTimeCarbon->toDateTimeString(),
                'end_datetime'=>$endDateTimeCarbon->toDateTimeString(),
                'model_id'=>$modelId,
                'area_id'=>$areaId,
                'model_id'=>$modelId,
                'delivered_at_area'=>$delivered_at_area,
                'note'=>($booking->note."  Edit Notes - ".$request->edit_note),
                'total_price'=>($booking->total_price+$request->amount),
                'payment_method'=>($booking->payment_method.",".$request->payment_method),
                'pg_txn_id'=>($booking->pg_txn_id.",".$request->payment_id),
                'order_id'=>($booking->order_id.",".$request->payment_id),
                'KMlimit'=>$freekm,
                'weekday_hours'=>$weekDayHours,
                'weekend_hours'=>$weekEndHours,
                'total_hours'=>($weekDayHours+$weekEndHours),
                'updated_by' => Auth::user()->email
            ]);
            
            BikeAvailabilityController::addOrDeleteBooking($booking->id,"add");
            
            $area = Area::where('id',$delivered_at_area)->first();
            
            if($request->notify_user == "yes")
            {
                $booking = Booking::where('id',$id)->first();
                
                $data = [
                    'fromDate' => $startDate, 
                    'toDate' => $endDate, 
                    'fromTime' => $startTime, 
                    'toTime' => $endTime, 
                    'areaName' => $area->area, 
                    'amount' => $request->amount, 
                    'areaAddress' => $area->address, 
                    'userFullName' => $booking->getUserFullNameThis(), 
                    'modelName' => $booking->getBikeMakeName($booking->model_id)." - ".$booking->getBikeModelNameThis(), 
                    'bookingId' => $booking->reference_id,
                    'userEmail' => $booking->getUserEmailThis()
                    
                ];

                Mail::send('emails.user_booking_edit_mail',$data,function($message) use ($data) {
                  $message->to($data['userEmail']);
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject('Change in '.env('SITENAMECAP').' Booking - '.$data['bookingId']);
                });
            }
            return redirect()->back()->withErrors(['The booking has been edited successfully!!']);
        }
        else
        {
            BikeAvailabilityController::addOrDeleteBooking($booking->id,"add");
            return redirect()->back()->withErrors(['The request bike is not available in the area. Please change the dates and try again.']);
        }

    }
  

  public static function getBookingReferenceId($area, $bookingId){
    $code = env('BOOKINGPREFIX').$area->code;
    $bookingCode = sprintf("%'.06d", $bookingId);
    return $code.$bookingCode;
  }
    
  public function indexPartner(){
    $user = Auth::user();
    $partner = Partner::where('user_id',$user->id)->first();
    $model_ids = explode(',',$partner->model_id);
    $area_ids = explode(',',$partner->area_id);
      

    $today = Carbon::now();
    $twomonthsdate = Carbon::now()->subMonths(2);
    if($partner->date_added>$twomonthsdate)
     {
      $twomonthsdate=$partner->date_added;
     }
           
    $bookings = Booking::whereIn('area_id',$area_ids)->whereIn('model_id',$model_ids)->where('start_datetime','>',$twomonthsdate)->orderBy('created_at', 'desc')->take(5000)->get();
      
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);

      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
    }
   
    return view('partner.booking.index',compact('bookings'));
  }  
    
    
  public function indexVendor(){
    $user = Auth::user();
    $userList = User::where('id',$user->id)->get();
    $vendor = Vendor::where('user_id',$user->id)->first();
    $bikeListArray = Bike::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
    $areas = Area::where('vendor_id',$vendor['id'])->where('status', 1)->get();
    $modelIDs = Bike::where('vendor_id', $vendor['id'])->where('status', 1)->groupBy('model_id')->get()->lists('model_id');
    $models = BikeModel::whereIn('id',$modelIDs)->get();

    $today = Carbon::now();
    //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
    $bookings = Booking::whereIn('bike_id',$bikeListArray)->orderBy('created_at', 'desc')->where('booking_status',1)->take(5000)->get();
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking['start_datetime']);
      $endDateObj = Carbon::parse($booking['end_datetime']);
      // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
      // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

      $booking['start_date'] = $startDateObj->toDateString();
      $booking['end_date'] = $endDateObj->toDateString();
      $booking['start_time'] = $startDateObj->toTimeString();
      $booking['end_time'] = $endDateObj->toTimeString();
    }
   
    return view('vendor.booking.index',compact('userList','models', 'areas', 'bookings'));
  }
    
    
    
  public function addVendor(Request $request){
    $rules = array(
      'model' => 'required',
      'area' => 'required',
      'startDate' => 'required',
      'startTime' => 'required',
      'endDate' => 'required',
      'endTime' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/vendor/booking')->withErrors($validator->errors());        
    }
    
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $startTime = $request->startTime;
    $endTime = $request->endTime;
      
       $actual_user = Auth::user();

    $userId = $request->customer;
    $modelId = $request->model;
    $areaId = $request->area;
    $area = Area::where('id', $areaId)->where('status', 1)->first();
    $amount = $request->amount;
    $city = City::where('id', $area['city_id'])->where('status', 1)->first();
    $raw_data = '';
    $dispatcher = app('Dingo\Api\Dispatcher');
    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,
                                        'start_time' => $startTime, 'end_time' => $endTime,
                                        'model_id' => $modelId, 'area_id' => $areaId, 'city_id' => $city['id']
                                      ])->get('bookings/price-and-check-availability');

    if($availability['bike_id'] != "none"){
      $status = 'COMPLETE';
      $booking = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate, 
                            'start_time' => $startTime, 'end_time' => $endTime, 
                            'user_id' => $userId, 'actual_user_id'=>$actual_user->id,'model_id' => $modelId,'area_id'=>$areaId, 'status' => $status,
                            'total_price' => $request->amount, 'note' => $request->note,
                            'raw_data' => $raw_data])
                            ->get('bookingsweb');

      $bookingId = BookingController::getBookingReferenceId($area, $booking['id']);
      Booking::where('id', $booking['id'])->update(['reference_id' => $bookingId]);
      return redirect('/vendor/booking')->with(['msg', 'Booking has been made successfully']);
    }
    else
        return redirect('/vendor/booking')->withErrors(['Looks like the bikes are not available.']);
  }

  public function deleteVendor($booking_id){
     
    $bookingParse=Booking::where('id',$booking_id)->first();
    
        
    $bookingParse['reason_for_delete'] = "vendor_delete";
    $bookingParse['deleted_by'] = Auth::user()->id;
    $bookingParse['deleted_at'] = date('Y-m-d H:i:s',strtotime("now"));
      
    
    $deleted_booking = DB::table('deleted_bookings')->where('id',$booking_id)->first();
    if(!$deleted_booking)
    {
        DB::table('deleted_bookings')->insert($bookingParse->toArray());  
        Booking::where('id',$booking_id)->update(['booking_status'=>0]);
      
    
        BikeAvailabilityController::addOrDeleteBooking($booking_id,"delete");

        Booking::destroy($booking_id);
        $availability = Availability::where('booking_id', '=', $booking_id)->delete();
    }
      
    return redirect('/vendor/booking');
  }

    
 public function adminEmail(){
    
        $bookings = [];

        $datetime = Carbon::now();

        $bookingsArr = Booking::orderBy('id', 'desc')->where('user_id',2)->where('end_datetime','>',$datetime)->get();

        foreach ($bookingsArr as $booking) {
          $area = Area::where('id',$booking->area_id)->first();
          $model = BikeModel::where('id',$booking->model_id)->first();
          $user = User::find($booking['user_id']);
          $data = [
            'id' => $booking['id'],
            'note' => $booking['note'],
            'start_datetime' => $booking['start_datetime'],
            'end_datetime' => $booking['end_datetime'],
            'areaName' => $booking->getAreaName($booking->area_id),
            'make-model' => $booking->getBikeMakeName($booking->model_id)." - ".$booking->getBikeModelName($booking->model_id),
            'block-email' =>$booking->getCreatedByEmail($booking->createdBy)

          ];
          array_push($bookings, $data);
        }

        $mailData = ['bookings' => $bookings];

        Mail::send('emails.admin_service_block_mail',$mailData,function($message) {
                //var_dump($data);
                  $message->to(env('OPERATIONS_EMAIL'));
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject(env('SITENAMECAP').' Block Mail');
              });

        return "done";

 }
 public function indexErpToday(Request $request){
     
    if(isset($request->areaId))
    {
        $selected = $request->areaId;
    }
    else
        $selected =1; 
          
     
    $userList = User::orderBy('id', 'asc')->take(20)->get();
     
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
    
    $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('user',Auth::user()->id)->get();
    if(count($kiranaAreaRelations) == 0)
    {
        $activeAreas = Area::where('status', 1)->get();
        $allowAllAreas = true;
    }
    else
    {
        $kiranaAreas = collect($kiranaAreaRelations)->pluck('area');
        $activeAreas = Area::where('status', 1)->whereIn('id',$kiranaAreas)->get();
        $allowAllAreas = false;
        if(!in_array($selected,$kiranaAreas->all()))
            $selected = $kiranaAreas[0];
    }
     
    
     
     
    $bookingsByArea = Self::getTodayDeliveries($selected,$request->pending);
    
     
    $users = User::whereIn('id',$bookingsByArea->pluck('user_id'))->get()->keyBy('id');
     
    $helmetList = Helmet::all();
    $jacketList = Jacket::all();
     
    $documentsByUsersObj = UserDocImage::whereIn('user_id',$bookingsByArea->pluck('user_id'))->get();
        
    $documentsByUsers = $documentsByUsersObj->groupBy('user_id');
    
    $documentsByUsers->toArray();
     
     
     
    foreach($bookingsByArea as $booking)
    {

        if(isset($documentsByUsers[$booking->user_id]))
        {
            foreach($documentsByUsers[$booking->user_id] as $docObj)
            {
                if(substr($docObj->doc,0,4) != "http")
                {
                    $docObj->doc = env('HTTPSORHTTP').":".$docObj->doc;
                }
            }

            $booking['user_document'] = $documentsByUsers[$booking->user_id]; 
        }
        else
        {
            $booking['user_document'] = [];
        }
        
        if($users[$booking->user_id]->work_email == "")
        {
            $booking['work_email_verified'] = "No";
        }
        else
        {
            $booking['work_email_verified'] = "Yes";
        }
        
        $booking['user_doc_notes'] = $users[$booking->user_id]->user_doc_notes;
        
    }
         
   
    return view('erp.todaybooking.index',compact('bookingsByArea','activeAreas','selected','models','helmetList','jacketList','allowAllAreas'));
  }
    
 public function indexErpTomorrow(Request $request){
     
    if(isset($request->areaId))
        $selected = $request->areaId;
    else
        $selected =1; 
    
     
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
     
    $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('user',Auth::user()->id)->get();
    if(count($kiranaAreaRelations) == 0)
    {
        $activeAreas = Area::where('status', 1)->get();
        $allowAllAreas = true;
    }
    else
    {
        $kiranaAreas = collect($kiranaAreaRelations)->pluck('area');
        $activeAreas = Area::where('status', 1)->whereIn('id',$kiranaAreas)->get();
        $allowAllAreas = false;
        if(!in_array($selected,$kiranaAreas->all()))
            $selected = $kiranaAreas[0];
    }
    
    $bookingsByArea = Self::getTomorrowDeliveries($selected);    
   
    return view('erp.tomorrowbooking.index',compact('bookingsByArea','activeAreas','selected','models','allowAllAreas'));
  }
    
 public function indexErpTodayDrop(Request $request){
     
    if(isset($request->areaId))
        $selected = $request->areaId;
    else
        $selected =1; 
     
    
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
    
    $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('user',Auth::user()->id)->get();
    if(count($kiranaAreaRelations) == 0)
    {
        $activeAreas = Area::where('status', 1)->get();
        $allowAllAreas = true;
    }
    else
    {
        $kiranaAreas = collect($kiranaAreaRelations)->pluck('area');
        $activeAreas = Area::where('status', 1)->whereIn('id',$kiranaAreas)->get();
        $allowAllAreas = false;
        if(!in_array($selected,$kiranaAreas->all()))
            $selected = $kiranaAreas[0];
    }
     
    $bookings = Self::getTodayReturns($selected,$request->pending);
    
    
    return view('erp.todaybookingdrop.index',compact('bookings','activeAreas','selected','allowAllAreas'));
     
  }
 public function indexErpTomorrowDrop(Request $request){
     
    if(isset($request->areaId))
        $selected = $request->areaId;
    else
        $selected = 1; 
     
    $bookingsByArea = Self::getTomorrowReturns($selected); 
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
     
    $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('user',Auth::user()->id)->get();
    if(count($kiranaAreaRelations) == 0)
    {
        $activeAreas = Area::where('status', 1)->get();
        $allowAllAreas = true;
    }
    else
    {
        $kiranaAreas = collect($kiranaAreaRelations)->pluck('area');
        $activeAreas = Area::where('status', 1)->whereIn('id',$kiranaAreas)->get();
        $allowAllAreas = false;
        if(!in_array($selected,$kiranaAreas->all()))
            $selected = $kiranaAreas[0];
    }
    
         
   
    return view('erp.tomorrowbookingdrop.index',compact('bookingsByArea','activeAreas','selected','allowAllAreas'));
  }
    
    
    
 public static function getTodayDeliveries($selected,$pending)
    {
        $today = Carbon::today();
        $tomorrow = Carbon::today()->addDay();
        if($selected==0)
        {
            $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
            if($pending=="on")
            {
                $bookingsByArea = Booking::orderBy('start_datetime', 'asc')->where(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereIn('area_id',$userAreas)->whereNotIn('user_id',array(2,5,7));
                        })->orWhere(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('is_delivered',0)->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereIn('area_id',$userAreas)->whereNotIn('user_id',array(2,5,7));
                        })->take(1000)->get();
            }
            else
            {
                $today = Carbon::today();
                $tomorrow = Carbon::today()->addDay();
                $bookingsByArea = Booking::orderBy('start_datetime', 'asc')->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereIn('area_id',$userAreas)->whereNotIn('user_id',array(2,5,7))->take(1000)->get();
            }
        }
        else
        {
            if($pending=="on")
            {
                $bookingsByArea = Booking::orderBy('start_datetime', 'asc')->where(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('delivered_at_area',$selected)->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->orWhere(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('delivered_at_area',$selected)->where('is_delivered',0)->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->take(1000)->get();
            }
            else
            {
                $today = Carbon::today();
                $tomorrow = Carbon::today()->addDay();
                $bookingsByArea = Booking::orderBy('start_datetime', 'asc')->where('delivered_at_area',$selected)->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->take(1000)->get();

            }
        }

        foreach ($bookingsByArea as $booking) {
          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);
          // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
          // $endTime = Slot::where('id', $booking['end_slot_id'])->first();
          $booking['start_date'] = $startDateObj->toDateString();
          $booking['end_date'] = $endDateObj->toDateString();
          $booking['start_time'] = $startDateObj->toTimeString();
          $booking['end_time'] = $endDateObj->toTimeString();
        }
     
        return $bookingsByArea;
    }
    
    
    
 public static function getTomorrowDeliveries($selected)
    {
        $today = Carbon::today();
        $tomorrow = Carbon::today()->addDay();
        $dayAfter = $today->addDay(2);
     
        if($selected==0)
        {
            $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
            $bookings = Booking::orderBy('start_datetime', 'asc')->where('user_id','<>',2)->where('user_id','<>',5)->where('start_datetime', '>=', $tomorrow->toDateTimeString())->where('start_datetime', '<=', $dayAfter->toDateTimeString())->take(200)->get();
            
        }
        else
        {
            
            $bookings = Booking::orderBy('start_datetime', 'asc')->where('delivered_at_area',$selected)->where('user_id','<>',2)->where('user_id','<>',5)->where('start_datetime', '>=', $tomorrow->toDateTimeString())->where('start_datetime', '<=', $dayAfter->toDateTimeString())->take(200)->get();
            
        }
        
        foreach ($bookings as $booking) 
        {
              $startDateObj = Carbon::parse($booking['start_datetime']);
              $endDateObj = Carbon::parse($booking['end_datetime']);
              // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
              // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

              $booking['start_date'] = $startDateObj->toDateString();
              $booking['end_date'] = $endDateObj->toDateString();
              $booking['start_time'] = $startDateObj->toTimeString();
              $booking['end_time'] = $endDateObj->toTimeString();
        }
        
        return $bookings;
    }
    
    
    
 public static function getTodayReturns($selected,$pending)
    {
        $today = Carbon::today();
        $tomorrow = Carbon::today()->addDay();
        if($selected==0)
        {
            $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
            if($pending=="on")
            {
                $bookings = Booking::orderBy('end_datetime', 'asc')->where(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();

                            $query->where('end_datetime', '>=', $today->toDateTimeString())->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->orWhere(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('is_returned',0)->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->take(1000)->get();
            }
            else
            {
                $today = Carbon::today();
                $tomorrow = Carbon::today()->addDay();

                $bookings = Booking::where('end_datetime', '>=', $today->toDateTimeString())->orderBy('end_datetime', 'asc')->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->take(1000)->get();

            }
        }
        else
        {
            if($pending=="on")
            {
                $bookings = Booking::orderBy('end_datetime', 'asc')->where(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();

                            $query->where('delivered_at_area',$selected)->where('end_datetime', '>=', $today->toDateTimeString())->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->orWhere(function ($query) use($selected){
                            $today = Carbon::today();
                            $tomorrow = Carbon::today()->addDay();
                            $query->where('delivered_at_area',$selected)->where('is_returned',0)->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7));
                        })->take(1000)->get();
            }
            else
            {
                $today = Carbon::today();
                $tomorrow = Carbon::today()->addDay();

                $bookings = Booking::where('delivered_at_area',$selected)->orderBy('end_datetime', 'asc')->where('end_datetime', '>=', $today->toDateTimeString())->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->take(1000)->get();

            }
        }
     

        foreach ($bookings as $booking) {
            $startDateObj = Carbon::parse($booking['start_datetime']);
            $endDateObj = Carbon::parse($booking['end_datetime']);
            // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
            // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

            $booking['start_date'] = $startDateObj->toDateString();
            $booking['end_date'] = $endDateObj->toDateString();
            $booking['start_time'] = $startDateObj->toTimeString();
            $booking['end_time'] = $endDateObj->toTimeString();

            $extrakmprice = Price::getExtraKMPriceformodel($booking->model_id);
            $booking['extra_km_price'] = $extrakmprice;
            $booking['price_per_hour'] = Price::where('model_id',$booking->model_id)->where('area_id',$booking->area_id)->pluck('price_per_hour');



            $booking['accessories_label'] = $booking->getAccessoryLabel();


        }
     
        return $bookings;
    }
    
    
    
 public static function getTomorrowReturns($selected)
    {
        $today = Carbon::today();
        $tomorrow = Carbon::today()->addDay();
        $dayAfter = $today->addDay(2);
        
        if($selected==0)
        {
            $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
            $bookings = Booking::orderBy('end_datetime', 'asc')->where('user_id','<>',2)->where('user_id','<>',5)->where('end_datetime', '>=', $tomorrow->toDateTimeString())->whereIn('area_id',$userAreas)->where('end_datetime', '<=', $dayAfter->toDateTimeString())->take(200)->get();
        }
        else
        {
            $bookings = Booking::orderBy('end_datetime', 'asc')->where('delivered_at_area',$selected)->where('user_id','<>',2)->where('user_id','<>',5)->where('end_datetime', '>=', $tomorrow->toDateTimeString())->where('end_datetime', '<=', $dayAfter->toDateTimeString())->take(200)->get();
        
        }
        
        foreach ($bookings as $booking) {
          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);

          $booking['start_date'] = $startDateObj->toDateString();
          $booking['end_date'] = $endDateObj->toDateString();
          $booking['start_time'] = $startDateObj->toTimeString();
          $booking['end_time'] = $endDateObj->toTimeString();
        }

        
        return $bookings;
    }
    
    
    

    
 public function indexErpServiceBlock(Request $request){
     
    if(isset($request->areaId))
        $selected = $request->areaId;
    else
        $selected = 1; 
     
     
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
    $activeAreas = Area::where('status', 1)->get();
    $today = Carbon::now();
     
    if($selected == 0)
    {
        $bookingsByArea = Booking::orderBy('end_datetime', 'asc')->where('user_id',2)->where('end_datetime', '>=', $today->toDateTimeString())->take(300)->get();
    }
    else
    {
        $bookingsByArea = Booking::orderBy('end_datetime', 'asc')->where('user_id',2)->where('area_id',$selected)->where('end_datetime', '>=', $today->toDateTimeString())->take(300)->get();

    }
    
    foreach ($bookingsByArea as $booking) {
        $serviceBlock = ServiceBlock::where('booking_id',$booking->id)->first();
        if(!$serviceBlock)
        {
            $booking['bike_number'] = "Manual Block";
            $booking['created_by_email'] = $booking->getCreatedByEmail($booking->createdBy);
        }
        else
        {
            $booking['bike_number'] = $serviceBlock->getBikeNumber();
            $booking['created_by_email'] = $serviceBlock->getCreatedByEmail();
        }
    }
   
    
         
   
    return view('erp.serviceBlock.index',compact('bookingsByArea','activeAreas','selected'));
  }

 public function indexErpMovement(Request $request){
     
    if(isset($request->areaId))
        $selected = $request->areaId;
    else
        $selected = 1; 
     
     
    $userList = User::orderBy('id', 'asc')->take(20)->get();
    $models = BikeModel::orderBy('bike_model','desc')->where('status', 1)->get();
    $activeAreas = Area::where('status', 1)->get();
    
   
    $bookingsByArea = Self::getMovement($selected);   
   
    return view('erp.movementBlock.index',compact('bookingsByArea','activeAreas','selected'));
  }
    
    
    
 public static function getMovement($selected)
    {
        
     
        $today = Carbon::today();
        if($selected == 0)
        {
            $bookings = Booking::orderBy('end_datetime', 'asc')->where('user_id',env('MOVEMENTID'))->where('end_datetime', '>=', $today->toDateTimeString())->take(300)->get();
            
        }
        else
        {
            $bookings = Booking::orderBy('end_datetime', 'asc')->where('area_id',$selected)->where('user_id',env('MOVEMENTID'))->where('end_datetime', '>=', $today->toDateTimeString())->take(300)->get();
            
        }
        
        foreach ($bookings as $booking) {
          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);
          // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
          // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

          $booking['start_date'] = $startDateObj->toDateString();
          $booking['end_date'] = $endDateObj->toDateString();
          $booking['start_time'] = $startDateObj->toTimeString();
          $booking['end_time'] = $endDateObj->toTimeString();
        }

        
        return $bookings;
    }
    
    
    public function create_zip(Request $request)
    {
        $k = $request->k;
        $files = glob(public_path('excel/'.$k.'/*'));
        Zipper::make('excel/'.$k.'/booking.zip')->add($files);
        return "done";
    }
    
    
    public function getBookingFile(Request $request)
    {
        return Response::download(public_path('excel/'.$request->name.'/booking.zip'));

    }
    
    public function createFiles(Request $request)
    {
    
        ini_set('max_execution_time', 0);  
        $k = rand (100000,1000000);
        $i=0;
        Booking::chunk(1000, function ($bookings) use(&$i,$k){
            Excel::create('Bookings Part'.$i, function($excel) use($bookings,&$i,$k) {
                $excel->sheet('bookings', function($sheet) use($bookings) {
                    foreach($bookings as $booking)
                    {
                        $booking['area'] = $booking->getAreaNameThis();
                        $booking['model'] = $booking->getBikeModelNameThis();
                        $user = $booking->getUserThis();
                        $booking['customer email'] = $user->email;
                        $booking['customer Phone'] = $user->mobile_num;
                        $booking['customer First Name'] = $user->first_name;
                        $booking['customer Last Name'] = $user->last_name;
                    }
                    
                    $sheet->fromModel($bookings);
                });
                $i++;
            })->store('xls','excel/'.$k);
           
        });
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $dispatcher->with(['k' => $k])->get('create_zip');
        
        $button = "<form class=\"form-inline\" method=\"get\" action=\"getBookingFile\"><input type=\"hidden\" name=\"name\" value=\"".$k."\"><button type=\"submit\" class=\"btn btn-primary\"> Download</button></form>";
        
        return $button;
        
        
    }
    
    
    public function createBookingAndSendEmails(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required' 
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        $model_id = $request->model_id;
        $area_id = $request->area_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $user_id = $request->user_id;
        $actual_user_id = $request->actual_user_id;
        $pg_txn_id = $request->pg_txn_id;
        $bank_txn_id = $request->bank_txn_id;
        $order_id = $request->order_id;
        $pg_mode = $request->pg_mode;
        $note = $request->note;
        $pg_txn_amount = $request->pg_txn_amount;
        $coupon = $request->coupon;
        $payment_method = $request->payment_method;
        $actual_user_id = $request->actual_user_id;
        $user_id = $request->user_id;
        
        
                    
        
        $dispatcher = app('Dingo\Api\Dispatcher');

        $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $model_id, 'area_id' => $area_id, 'start_date' => $start_date, 'end_date' => $end_date, 'start_time' => $start_time, 'end_time' => $end_time, 'coupon' => $coupon])->get('bookings/price-and-check-availability');
        
        $totalPrice = $bikeAvailabilityWithPrice['total_price'];
        $bikeId = $bikeAvailabilityWithPrice['bike_id'];
        
        
        $number_of_hours = $bikeAvailabilityWithPrice['number_of_hours'];
        $minimum_hours = $bikeAvailabilityWithPrice['minimum_hours'];

        $max_hours = max($number_of_hours,$minimum_hours);

        $model = BikeModel::where('id',$model_id)->first();

        $modelImage = Image::where('id', $model['thumbnail_img_id'])->first();
        $modelName = $model['bike_model'];

        $modelThumbnail = $modelImage['full'];
        $extrakmprice = Price::getExtraKMPriceformodel($model_id);
        $freekm = $max_hours*env('KMLIMIT');

        $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";
        
       
        $dispatcher = app('Dingo\Api\Dispatcher');

        $booking = $dispatcher->with([
            'start_date' => $start_date, 
            'end_date' => $end_date,
            'start_time' => $start_time, 
            'end_time' => $end_time, 
            'user_id' =>$user_id, 
            'actual_user_id'=>$user_id, 
            'model_id' => $model_id, 
            'area_id' => $area_id,
            'status' => 'COMPLETE',
            'total_price' => $totalPrice,
            'pg_status' => 'success',
            'pg_txn_id' => $pg_txn_id, 
            'bank_txn_id' => $bank_txn_id, 
            'order_id' => $order_id, 
            'pg_mode' => $pg_mode, 
            'note' => $note, 
            'pg_txn_amount' => $pg_txn_amount, 
            'coupon'=>$coupon, 
            'payment_method'=>$payment_method, 
            'kmlimit'=>$freekm
        ])->get('bookingswebBglojd76');
        
        if(isset($booking['error'])){
        $message = $booking['error'];
        return ['error'=>'Error in Booking.'];
        }
    
        $refId = $booking['reference_id'];
        
        $area = Area::where('id',$area_id)->first();
        $areaName = $area->area;
        
        
        if($user_id >25)
        {
        
            if($user_id > 25 && $actual_user_id!=$user_id)
            {
                $dispatcher = app('Dingo\Api\Dispatcher');
                $createUser = $dispatcher->with(['first_name' => $request->customer_fname, 'last_name' => $request->customer_lname,'mobile' => $request->customer_mobile, 'email' => $request->customer_email,'reservationId'=>$bookingId])->post('signupFromAdmin');
            }

            $data = [
                'fromDate' => $start_date, 
                'toDate' => $end_date, 
                'fromTime' => $start_time, 
                'toTime' => $end_time, 
                'areaName' => $areaName, 
                'amount' => $amount, 
                'duration' => $duration, 
                'areaAddress' => $areaAddress, 
                'userFname' => $userFname, 
                'modelName' => $modelName, 
                'unit' => $unit, 
                'modelThumbnail' => $modelThumbnail, 
                'userEmail' => $userEmail, 
                'userMob' => $userMob, 
                'note' => $note, 
                'bookingId' => $refId, 
                'kmLimitMessage'=>$kmLimitMessage
            ];

            Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
                $message->to($data['userEmail']);
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
                if($data['manualPresent'])
                        $message->attach($data['manualFile']);
            });

            Mail::send('emails.admin_booking_mail',$data,function($message) use ($data) {
            //var_dump($data);
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject(env('SITENAMECAP').' '.$data['userFname'].' Booking - '.$data['bookingId']);
            });

        }

        // PROMOCODE CHANGE AVAILABLE COUNT
        $promocode = PromoCode::where('code', $coupon)->first();
        if ($promocode) {
        $promocode['available_count'] = $promocode['available_count'] - 1;
        $promocode->save();
        }
        
    }
    
    public static function weekDayAndWeekEndHours($startDateTimeCarbon,$endDateTimeCarbon)
    {
        $totalHours = $startDateTimeCarbon->diffInHours($endDateTimeCarbon); 
     
        $weekEndHours = 0;

        if($startDateTimeCarbon->toDateString() == $endDateTimeCarbon->toDateString())
        {
            if($startDateTimeCarbon->isWeekend())
            {
                $weekEndHours = $startDateTimeCarbon->diffInHours($endDateTimeCarbon);
            }
        }
        else
        {
            for($date = $startDateTimeCarbon->copy(); $date->lte($endDateTimeCarbon); $date->addDay()) 
            {
                if($date->toDateString() == $startDateTimeCarbon->toDateString())
                {
                    if($date->isWeekend())
                    {
                        $weekEndHours += (24 - $startDateTimeCarbon->hour);
                    }
                }
                elseif($date->toDateString() == $endDateTimeCarbon->toDateString())
                {
                    if($date->isWeekend())
                    {
                        $weekEndHours += $endDateTimeCarbon->hour;
                    }
                }
                else
                {
                    if($date->isWeekend())
                    {
                        $weekEndHours += 24;
                    }
                }
            }

        }

        $weekDayHours = $totalHours - $weekEndHours;
        
        return ['weekEndHours'=>$weekEndHours,'weekDayHours'=>$weekDayHours];
    }
    
    public static function getBookingsInBetweenDates($startDateTime,$endDateTime,$area_id,$model_id)
    {
        if($area_id == 0)
        {
            $matchedBookings = Booking::where(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<=', $startDateTime)->where('end_datetime','>=',$endDateTime)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '>=', $startDateTime)->where('end_datetime','<=',$endDateTime)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<', $startDateTime)->where('end_datetime', '>', $startDateTime)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<', $endDateTime)->where('end_datetime','>',$endDateTime)->where('model_id',$model_id);
                    })->orderBy('start_datetime','asc')->take(1000)->get();
        }
        else
        {
            $matchedBookings = Booking::where(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<=', $startDateTime)->where('end_datetime','>=',$endDateTime)->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '>=', $startDateTime)->where('end_datetime','<=',$endDateTime)->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<', $startDateTime)->where('end_datetime', '>', $startDateTime)->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($startDateTime,$endDateTime,$area_id,$model_id) {
                        $query->where('start_datetime', '<', $endDateTime)->where('end_datetime','>',$endDateTime)->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orderBy('start_datetime','asc')->take(1000)->get();
        }
        
        
        
        return $matchedBookings;
        
    }
    
    public static function getCollidingBookingsForDates($startDateTimeCarbon,$endDateTimeCarbon,$area_id,$model_id)
    {
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        $availableObjs = Available::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        $availableStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {            
            $availableStr.=$availableObj->hour_string.",";             
        }
        
        
        $datesArray = AMHAvailabilityController::returnDatesWithZeroAvailability($startDateTimeCarbon,$endDateTimeCarbon,$availableStr);
        
        $resultArray = [];
        
        
        foreach($datesArray as $datesArrayEl)
        {
            $exactMatchBookings = Booking::where('start_datetime',$datesArrayEl['start_datetime'])->where('end_datetime',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id)->get();

            //$matchedBookings = Booking::where('start_datetime','<=',$datesArrayEl['start_datetime'])->where('end_datetime','>=',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id)->get();
            
            $matchedBookings = Booking::where(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('start_datetime', '<', $datesArrayEl['start_datetime'])->where('end_datetime','>',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('end_datetime', '>', $datesArrayEl['start_datetime'])->where('end_datetime','<',$datesArrayEl['end_datetime'])->where('start_datetime', '<', $datesArrayEl['start_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('start_datetime', '>', $datesArrayEl['start_datetime'])->where('start_datetime','<',$datesArrayEl['end_datetime'])->where('end_datetime','>',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('start_datetime', '<>', $datesArrayEl['start_datetime'])->where('end_datetime',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('start_datetime', $datesArrayEl['start_datetime'])->where('end_datetime','<>',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orWhere(function ($query) use($datesArrayEl,$area_id,$model_id) {
                        $query->where('start_datetime', '>', $datesArrayEl['start_datetime'])->where('end_datetime','<',$datesArrayEl['end_datetime'])->where('area_id',$area_id)->where('model_id',$model_id);
                    })->orderBy('start_datetime','asc')->take(1000)->get();
            
            $dates = Carbon::parse($datesArrayEl['start_datetime'])->format('j M-y ga')." - ".Carbon::parse($datesArrayEl['end_datetime'])->format('j M-y ga');
            
            array_push($resultArray,['dates'=>$dates, 'exactMatchBookings'=>$exactMatchBookings, 'matchedBookings'=>$matchedBookings]);
        }
        
        
        return $resultArray;

        
    }
    
    
    public function sendBookingSMSToCustomers()
    {
        if(env('SERVER_STATUS')!="test")
        {
            $now = Carbon::now();
            $now->minute = 0;
            $now->second = 0;
            $nowPlusHour = $now->copy()->addHours(env('BOOKINGSMSLEADTIME'));


            $bookings = Booking::where('start_datetime',$nowPlusHour->format('Y-m-d H:i:s'))->where('user_id',">",25)->get();

            foreach($bookings as $booking)
            {
                $smsGatewayUrl = env('SMSGATEWAYURL');
                $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
                $smsSenderId = env('SMSSENDERID');

                $user = User::where('id',$booking->user_id)->first();
                $area = Area::where('id',$booking->delivered_at_area)->first();
                $model = BikeModel::where('id',$booking->model_id)->first();
                
                $phone_number = str_replace("+","",$user->mobile_num);
                $phone_number = str_replace(" ","",$phone_number);
                $phone_number = str_replace("-","",$phone_number);
                $phone_number = str_replace("(","",$phone_number);
                $phone_number = str_replace(")","",$phone_number);

                $smsMessage = "Booking ID - ".$booking->reference_id." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is coming up in ".env('BOOKINGSMSLEADTIME').(env('BOOKINGSMSLEADTIME')==1?" hour":" hours").". Please pick it up at ".$area->address." The google maps link - ".$area->maps_link;

                if(env('TESTSMS')!=true)
                {
                    $response = Curl::to($smsGatewayUrl)->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))->get();
                }
                

            }
            
            $nowPlusDocsHour = $now->copy()->addHours(env('DOCUMENTSMSLEADTIME'));


            $bookings = Booking::where('start_datetime',$nowPlusDocsHour->format('Y-m-d H:i:s'))->where('user_id',">",25)->get();

            foreach($bookings as $booking)
            {
                $dispatcher = app('Dingo\Api\Dispatcher');

                $sendDocsSMS = $dispatcher->with([
                    'booking_id'=>$booking->id
                ])->get('send-document-sms');
            }
            
            return "done";
        }

    }
    
    public function sendDocumentSMSToCustomers(Request $request)
    {
        

        $booking = Booking::where('id',$request->booking_id)->first();

        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');

        $user = User::where('id',$booking->user_id)->first();
        $area = Area::where('id',$booking->delivered_at_area)->first();
        $model = BikeModel::where('id',$booking->model_id)->first();

        $phone_number = str_replace("+","",$user->mobile_num);
        $phone_number = str_replace(" ","",$phone_number);
        $phone_number = str_replace("-","",$phone_number);
        $phone_number = str_replace("(","",$phone_number);
        $phone_number = str_replace(")","",$phone_number);
        

        $smsMessage = "Your Booking - ".$booking->reference_id." is due soon. The following are the documents that are needed for renting the bike:   Valid Driving License to ride a motor-cycle with gear ".((env("SITENAMESMALLNOCOM")=="metrobikes")?"(without gear for scooters)":"")." in India. Any two of the following:   i. Test email to reservations@".env('SITENAMESMALL')." from your work email ID    ii. Passport. Original to be brought, copy will be retained for record    iii. Swipe your credit card on our EDC machine for Rs.1. The credit card should be in your name.";

        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
                ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, 'route'=>4, 'country'=>91, 'response'=>'json'))
                ->get();
        }
        
        $surveyUrl = env('KYCSURVEYLINK')."?usp=pp_url&entry.1121588755=".$user->email."&entry.1758923294=".$booking->reference_id;
        
        $linkResponse = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>$surveyUrl,'key'=>env('GOOGLESHORTURLAPI')]))->post();
        $surveyLink = json_decode($linkResponse)->id;

        $data = [
            'reference_id'=>$booking->reference_id,
            'docleadtime'=>env('DOCUMENTSMSLEADTIME'),
            'leadtimelabel'=>(env('DOCUMENTSMSLEADTIME')==1?" hour":" hours"),
            'userEmail'=>$user->email,
            'fname'=>$user->first_name,
            'surveyLink'=>$surveyLink
        ];

        Mail::send('emails.user_docs_mail',$data,function($message) use ($data) {
          $message->to($data['userEmail']);
          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
          $message->subject("KYC required for ".env('SITENAMECAP').' Booking - '.$data['reference_id']);
        });

        return "done";
        

    }

    public function tpBooking(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date_time' => 'required',
            'end_date_time' => 'required',
            'add_or_edit' => 'required',
            'reference_id' => 'required' 
        );
    

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());
        }
        
        $thirdParty = DB::table('third_party_api')->where('api_key',$request->api_key)->where('status',1)->first();
        
        if(!$thirdParty)
            return $this->respondWith400StatusCode("API Key could not be authenticated.");
        
        if((!in_array($request->area_id,explode(',',$thirdParty->area_ids)))&&(!($thirdParty->area_ids=="all areas")))
            return $this->respondWith400StatusCode("Booking attempted for unauthorized area.");
        
        if((!in_array($request->model_id,explode(',',$thirdParty->model_ids)))&&(!($thirdParty->model_ids=="all models")))
            return $this->respondWith400StatusCode("Booking attempted for unauthorized model.");
        
        if($request->add_or_edit == "add")
        {
            $model_id = $request->model_id;
            $area_id = $request->area_id;
            $startDateTimeCarbon = Carbon::parse($request->start_date_time);
            $start_date = $startDateTimeCarbon->toDateString();
            $start_time = $startDateTimeCarbon->toTimeString();
            $endDateTimeCarbon = Carbon::parse($request->end_date_time);
            $end_date = $endDateTimeCarbon->toDateString();
            $end_time = $endDateTimeCarbon->toTimeString();
            
            $dispatcher = app('Dingo\Api\Dispatcher');

            $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $model_id, 'area_id' => $area_id, 'start_datetime' => $startDateTimeCarbon->toDateTimeString(), 'end_datetime' => $endDateTimeCarbon->toDateTimeString(), 'coupon' => ""])->get('bookings/price-and-check-availability');
            
            if($bikeAvailabilityWithPrice['bike_id'] != "none")
            {
                $booking = $dispatcher->with([
                    'start_date' => $start_date, 
                    'end_date' => $end_date, 
                    'start_time' => $start_time, 
                    'end_time' => $end_time,
                    'user_id' => env('OWNERBLOCKID'),
                    'actual_user_id'=> env('OWNERBLOCKID'), 
                    'model_id' => $model_id, 
                    'area_id' => $area_id, 
                    'status' => "COMPLETE",
                    'total_price' => isset($request->amount)?$request->amount:0, 
                    'note' => isset($request->customer_instructions)?$request->customer_instructions:"",
                    'raw_data' => "",
                    'delivered_at_area'=>$area_id
                ])->get('bookingsweb');
                
                Booking::where('id',$booking['id'])->update(["reference_id"=>$request->reference_id]);
                
                $data = [
                    "reference_id"=>$request->reference_id,
                    "status"=>true,
                    "message"=>"booking created successfully"
                ];
                
                return $this->respondWithSuccess($data);
            }
            else
            {
                $data = [
                    "reference_id"=>"",
                    "status"=>false,
                    "message"=>"Booking could not be created. Bike not available"
                ];
                
                return $this->respondWithSuccess($data);
            }
            
        }
    }
    
    public static function readableDateTime($datetime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($datetime)->format('jS M-y ga');
        }
        return Carbon::parse($datetime)->format('jS M ga');
    }
    
    
} /*END OF CONTROLLER CLASS*/
