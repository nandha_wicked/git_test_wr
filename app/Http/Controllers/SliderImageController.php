<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SliderImage;

class SliderImageController extends AppController{



    public function index(){
        $sliderImageList = SliderImage::all();
        return view('admin.sliderImage.index',compact('sliderImageList'));
    }

    public function add(Request $request){
        $rules = array(
        'url' => 'required',
        'publishStatus' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails())
        {
            return redirect('/admin/sliderImage')->withErrors($validator->errors());        
        }
        else
        {
            $coverImg = $request->file('coverImgUpload');
            if($coverImg)
            {
                $coverImgExt = $coverImg->guessClientExtension();
                $coverImgFn = $coverImg->getClientOriginalName();

                $coverImgHashName = time().'.'.$coverImgExt;
                $coverImgDestinationPath = 'img/slider/';
                $coverImg->move($coverImgDestinationPath, $coverImgHashName);

                $coverImgUrl =  env('HTTPSORHTTP').'://'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName;

            }
            else
            {
                $coverImgUrl =''; 
            }
            
           
            $coverMobileImg = $request->file('coverMobileImgUpload');


            if($coverMobileImg)
            {
                $coverMobileImgExt = $coverMobileImg->guessClientExtension();
                $coverMobileImgFn = $coverMobileImg->getClientOriginalName();

                $coverMobileImgHashName = "mobile".time().'.'.$coverMobileImgExt;
                $coverMobileImgDestinationPath = 'img/slider/';
                $coverMobileImg->move($coverMobileImgDestinationPath, $coverMobileImgHashName);

                $coverMobileImgUrl =  env('HTTPSORHTTP')."://".env('IMAGEURL').'/'.$coverMobileImgDestinationPath.$coverMobileImgHashName;

            }
            else
            {
                $coverMobileImgUrl =''; 
            }

            SliderImage::create(['url' => $request->url, 'image'=>$coverImgUrl,'mobile_image'=>$coverMobileImgUrl,'description'=>$request->description, 'status' => $request->publishStatus,'button_description'=>$request->button_description]);

            return redirect('/admin/sliderImage');
        }   
    }


    public function edit($sliderImage_id,Request $request)
    {
        $rules = array(
          'url' => 'required',
          'publishStatus' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails())
        {
            return redirect('/admin/sliderImage')->withErrors($validator->errors());        
        }

        $coverImg = $request->file('coverImgUpload');

        if($coverImg)
        {
            $coverImgExt = $coverImg->guessClientExtension();
            $coverImgFn = $coverImg->getClientOriginalName();

            $coverImgHashName = time().'.'.$coverImgExt;
            $coverImgDestinationPath = 'img/email-promo/';
            $coverImg->move($coverImgDestinationPath, $coverImgHashName);

            $coverImgUrl =  env('HTTPSORHTTP')."://".env('IMAGEURL').'/'.$coverImgDestinationPath.$coverImgHashName;

        }
        else
        {
            $sliderImage = SliderImage::where('id',$sliderImage_id)->first();
            $coverImgUrl = $sliderImage->image; 
        }
        
        $coverMobileImg = $request->file('coverMobileImgUpload');


        if($coverMobileImg)
        {
            $coverMobileImgExt = $coverMobileImg->guessClientExtension();
            $coverMobileImgFn = $coverMobileImg->getClientOriginalName();

            $coverMobileImgHashName = "mobile".time().'.'.$coverMobileImgExt;
            $coverMobileImgDestinationPath = 'img/email-promo/';
            $coverMobileImg->move($coverMobileImgDestinationPath, $coverMobileImgHashName);

            $coverMobileImgUrl =  env('HTTPSORHTTP')."://".env('IMAGEURL').'/'.$coverMobileImgDestinationPath.$coverMobileImgHashName;

        }
        else
        {
            $sliderImage = SliderImage::where('id',$sliderImage_id)->first();
            $coverMobileImgUrl = $sliderImage->mobile_image; 
        }

        SliderImage::where('id',$sliderImage_id)->where('status', 1)->update(['url' => $request->url, 'image'=>$coverImgUrl,'mobile_image'=>$coverMobileImgUrl,'description'=>$request->description, 'status' => $request->publishStatus,'button_description'=>$request->button_description]);

        return redirect('/admin/sliderImage'); 
    }


    public function delete($sliderImage_id)
    {
        SliderImage::destroy($sliderImage_id);
        return redirect('/admin/sliderImage');
    }
    
    
    public function indexImage(){
        return view('admin.imageUpload.index');
    }
    
    public function addImage(Request $request){
        $rules = array(
        'coverImgUpload' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails())
        {
            return redirect('/admin/image')->withErrors($validator->errors());        
        }
        else
        {
            $coverImg = $request->file('coverImgUpload');
            if($coverImg)
            {
                $coverImgExt = $coverImg->guessClientExtension();
                $coverImgFn = $coverImg->getClientOriginalName();
                
                $coverImgFn = str_replace(' ','-',$coverImgFn);

                $coverImgDestinationPath = 'img/';
                $coverImg->move($coverImgDestinationPath,$coverImgFn);


            }
            

            return redirect('/admin/image')->withErrors(["url"=>"/img/".$coverImgFn]);
        }   
    }



}
