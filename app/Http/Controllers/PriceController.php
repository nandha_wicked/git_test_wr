<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VendorController;
use Illuminate\Support\Facades\Validator;
use App\Models\Price;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Bike;
use App\Models\Slot;
use App\Models\Vendor;
use App\Models\Parameters;
use App\Models\Wallet;
use App\Models\GlobalConfig;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Dingo\Api\Routing\Helpers;
use Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use DB;

class PriceController extends AppController
{
    use Helpers;
    public function index(){

        $priceList = Price::orderBy('id','desc')->where('status',1)->get();
        $modelList = BikeModel::where('status',1)->get();
        $areaList = Area::where('status',1)->get();
        foreach ($priceList as $price) {
          $model = $price->model;
          $area = $price->area;
          $price->model_name = $model['bike_model'];
          $price->area_name = $area['area'];
        }
        return view('admin.price.index', compact('priceList', 'modelList', 'areaList'));
    }

    public function add(Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'minimum_hours' => 'required',
        'hour_weekday' => 'required',
        'hour_weekend' => 'required',
        'day_weekday' => 'required',
        'day_weekend' => 'required',
        'five_weekday' => 'required',
        'week' => 'required',
        'month' => 'required' 
        );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/price')->withErrors($validator->errors());        
      }
      $admin = UserController::getLoggedInUser(); 
      Price::create(['model_id' => $request->modelId, 
                     'area_id' => $request->areaId,  
                     'minimum_hours' => $request->minimum_hours,
                    'hour_weekday' => $request->hour_weekday,
                    'hour_weekend' => $request->hour_weekend,
                    'day_weekday' => $request->day_weekday,
                    'day_weekend' => $request->day_weekend,
                    'five_weekday' => $request->five_weekday,
                    'week' => $request->week,
                    'month' => $request->month,
                    'per_hour_fuel_included_weekday' => $request->perhourfuel,
                    'per_km_fuel_included' => $request->perkmfuel,
                    'updated_by' => $admin->id
                    ]);
      return redirect('/admin/price');            
    }

    public function edit($price_id,Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'minimum_hours' => 'required',
        'hour_weekday' => 'required',
        'hour_weekend' => 'required',
        'day_weekday' => 'required',
        'day_weekend' => 'required',
        'five_weekday' => 'required',
        'week' => 'required',
        'month' => 'required',
        'priceStatus' => 'required'
        
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/price')->withErrors($validator->errors());        
      }

      $admin = UserController::getLoggedInUser(); 
      $price = Price::where('id',$price_id)->first();
      //store edited data
      DB::table('edited_prices')->insert($price->toArray());
      Price::where('id',$price_id)->update(['model_id' => $request->modelId, 
                                            'area_id' => $request->areaId,
                                            'minimum_hours' => $request->minimum_hours,
                                            'hour_weekday' => $request->hour_weekday,
                                            'hour_weekend' => $request->hour_weekend,
                                            'day_weekday' => $request->day_weekday,
                                            'day_weekend' => $request->day_weekend,
                                            'five_weekday' => $request->five_weekday,
                                            'week' => $request->week,
                                            'month' => $request->month,
                                            'per_hour_fuel_included_weekday' => $request->perhourfuel,
                                            'per_km_fuel_included' => $request->perkmfuel,
                                            'status'=>$request->priceStatus,
                                            'updated_by' => $admin->id
                                           ]);
      return redirect('/admin/price');    
    }   
    


    public function getTotalPrice(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required_without_all:start_datetime,end_datetime',
            'start_time' => 'required_without_all:start_datetime,end_datetime',
            'end_date' => 'required_without_all:start_datetime,end_datetime',
            'end_time' => 'required_without_all:start_datetime,end_datetime',
            'start_datetime' =>'required_without_all:start_date,start_time,end_date,end_time',
            'end_datetime' => 'required_without_all:start_date,start_time,end_date,end_time'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        
        $modelId = $request->model_id;
        $areaId = $request->area_id;

        $areaisvendor = Area::where('id',$areaId)->first();
        $areaisvendorBool = $areaisvendor->vendor_id;

        $priceObj = Price::where('model_id', $modelId)->where('area_id', $areaId)->orderBy('id','desc')->where('status',1)->first();
        if(!$priceObj)
        {
            $priceObj = Price::where('model_id', $modelId)->where('area_id',0)->orderBy('id','desc')->where('status',1)->first();
            if(!$priceObj)
            {
                return ['total_price' => null, 'effective_price' => null, 'no_of_hours' => null];
            }
        }
        
        $price = 0;
        
        $weekDayHours = 0;
        $weekEndHours = 0;
        $totalHours = 0;
        
        if(isset($request->start_date))
        {
            
            $startDateTimeStr = $request->start_date.' '.$request->start_time;
            $endDateTimeStr = $request->end_date.' '.$request->end_time;
        
        }
        else
        {
            $startDateTimeStr = $request->start_datetime;
            $endDateTimeStr = $request->end_datetime;
            
        }
        
        $startDateTimeCarbon = Carbon::parse($startDateTimeStr);
        $endDateTimeCarbon = Carbon::parse($endDateTimeStr);


        $holidayInDatesBool = $startDateTimeCarbon->diffFiltered(CarbonInterval::days(), function(Carbon $date) {
            $result = false;
            $holidayDates = DB::table('holiday')->get();
            foreach($holidayDates as $holidayDate)
            {
                if($date->toDateString() == $holidayDate->date)
                    $result = true;
            }
            return $result;
        }, $endDateTimeCarbon);



        $weekEndHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
           return $date->isWeekend();
        }, $endDateTimeCarbon);

        $weekDayHours = $startDateTimeCarbon->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
           return $date->isWeekday();
        }, $endDateTimeCarbon);


        $no_of_days = $startDateTimeCarbon->diffInDays($endDateTimeCarbon);

        if($no_of_days == 0)
            $no_of_days = 1;

        
        
        $totalHours = $weekEndHours + $weekDayHours;
                
                
        $totalHoursTrue = $weekEndHours + $weekDayHours;
        
       

        $minimum_weekday_hours = $priceObj->minimum_hours;

        $minimum_weekend_hours = Parameters::where('parameter_name','minimum_weekend_hours')->pluck('parameter_value');

        $minimum_weekend_hours = (int)$minimum_weekend_hours;


        if($totalHours<$minimum_weekday_hours&&$weekEndHours==0)
        {
            $weekDayHours = $minimum_weekday_hours;
        }
        if($totalHours<$minimum_weekend_hours&&$weekEndHours>0)
        {
            $weekEndHours = 24;
            $weekDayHours = 0;
        }

        $totalHours = $weekEndHours + $weekDayHours;
           
        if($holidayInDatesBool)
        {
            $totalHours = max(24,$totalHours);
        }

        $hours_for_weekday_daily_cieling = ceil($priceObj->day_weekday/$priceObj->hour_weekday);
        $hours_for_weekend_daily_cieling = ceil($priceObj->day_weekend/$priceObj->hour_weekend);


        if($totalHours>=720)
        {
            $price = $totalHours*($priceObj->month/720);
        }
        elseif($totalHours>=168)
        {
            $price = $totalHours*($priceObj->week/168);
        }
        elseif($totalHours>=120)
        {
            if($weekDayHours>=96+$hours_for_weekday_daily_cieling)
            {
                //1 5-weekday price applies for the 5 weekdays
                if($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->week;
                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->five_weekday + $priceObj->day_weekend + (($weekEndHours-24)*$priceObj->day_weekend/24);
                }
                elseif($weekEndHours>=$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->five_weekday + $priceObj->day_weekend;
                }
                else
                {
                    $price = $priceObj->five_weekday + ($weekEndHours*$priceObj->hour_weekend);  
                }
            }
            else
            {
                if($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = 2*$priceObj->day_weekend + (floor($weekDayHours/24)*$priceObj->day_weekday);
                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->day_weekend + (($weekEndHours-24)*$priceObj->day_weekend/24) + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                elseif($weekEndHours>=$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->day_weekend + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                else
                {
                    $price = ($weekEndHours*$priceObj->hour_weekend) + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }  
                }

            }
        }
        elseif($weekDayHours>=96+$hours_for_weekday_daily_cieling)
        {
            if($weekEndHours==0)
                $price = $priceObj->five_weekday;
            else
            {
                $price = $priceObj->five_weekday;
                if(fmod($weekEndHours,24)>=$hours_for_weekend_daily_cieling)
                {
                    $price += $priceObj->day_weekend;
                }
                else
                {
                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;
                }   
            }
        }
        else
        {
            if($weekDayHours == 0)
            {
                $price = floor($weekEndHours/24)*$priceObj->day_weekend;

                if(fmod($weekEndHours,24)>=$hours_for_weekend_daily_cieling)
                {
                    $price += $priceObj->day_weekend;
                }
                else
                {
                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;
                }                
            }
            elseif($weekEndHours == 0)
            {

                $price = floor($weekDayHours/24)*$priceObj->day_weekday;

                if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                {
                    $price += $priceObj->day_weekday;
                }
                else
                {
                    $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;

                }
            }
            else
            {
                if($weekEndHours>=48)
                {
                    $price = 2*$priceObj->day_weekend;

                    $price += floor($weekDayHours/24)*$priceObj->day_weekday;

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }

                }
                elseif($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = 2*$priceObj->day_weekend;
                    $weekDayHoursAfter2Weekends = $weekDayHours - (24-fmod($weekEndHours,24));
                    if($weekDayHoursAfter2Weekends<0)
                        $weekDayHoursAfter2Weekends = 0;

                    $price += (floor($weekDayHoursAfter2Weekends/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHoursAfter2Weekends,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHoursAfter2Weekends,24)*$priceObj->hour_weekday;
                    }

                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->day_weekend;

                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;

                    $price += (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }

                }
                else
                {
                    $price = $priceObj->day_weekend;
                    $weekDayHoursAfter2Weekends = $weekDayHours - (24-$weekEndHours);
                    if($weekDayHoursAfter2Weekends<0)
                        $weekDayHoursAfter2Weekends = 0;

                    $price += (floor($weekDayHoursAfter2Weekends/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHoursAfter2Weekends,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHoursAfter2Weekends,24)*$priceObj->hour_weekday;
                    }
                }
            }
        }

        //Holiday Price Hike

        if($holidayInDatesBool)
        {
            $price = $price*env('HOLIDAYSURGE');
        }

        //Add of surge price
        $surgeprice = GlobalConfig::where('parameter','surgeprice')->where('active_status',1)->pluck('value');
        if($surgeprice<0.1||!is_numeric($surgeprice)) {
            $surgeprice=1;
        }
        $price = $price*$surgeprice;
        

        // Need $totalHours and $price
        /* DISCOUNT PRICE */
        $price = Price::getFinalPriceAfterDiscount($price, $totalHours, $areaisvendorBool);
        
        if(in_array($areaId,explode(",",env('AREAINCLUDEDFUEL'))) && ($totalHours<24))
        {
            $price = $priceObj->per_hour_fuel_included_weekday*$totalHours;
            if($holidayInDatesBool)
            {
                $price = $price*env('HOLIDAYSURGE');
            }
        }
        
        if($totalHoursTrue==0)
        {
            $effectivePrice = 0;   
        }
        else
        {
            $effectivePrice = round($price/$totalHoursTrue);   
            
        }
        if($weekEndHours==0)
        {
            $pricePerDay = $priceObj->day_weekday;
        }
        else
        {
            $pricePerDay = $priceObj->day_weekend;
        }
        
        


        $totalPrice = $price;
        $finalPrice = $price;

        $coupon = '';
        $couponStatus = false;
        $couponMessage = 'No coupon code applied';
        $discount = 0;
        Session::put('bkPromoCode', null);
        $device = $request->device;
        if (empty($device))
            $device = 'MOBILE';
        
        
        
        $full_slab_price = max($totalHours,$totalHoursTrue)*$priceObj['hour_weekend'];
        

        
        if($request->source!="apply_promocode")
        {
            if ($request->coupon) 
            {

                $couponData = $dispatcher->with([
                    'code' => $request->coupon, 
                    'model_id' => $modelId, 
                    'area_id' => $areaId, 
                    'start_datetime' => $startDateTimeStr, 
                    'end_datetime' => $endDateTimeStr, 
                    'price' => $price, 
                    'device' => $device,
                    'full_slab_price'=>$full_slab_price
                ])->get('promocodes/apply');


                if ($couponData['status']) {
                  Session::put('bkPromoCode', $request->coupon);
                  $coupon = $request->coupon;
                  $couponStatus = $couponData['status'];
                  $couponMessage = $couponData['message'];
                  $discount = $couponData['discount'];
                  $finalPrice = $couponData['final_price'];
                } else {
                  $coupon = $request->coupon;
                  $couponStatus = $couponData['status'];
                  $couponMessage = $couponData['message'];
                }
            }
            
        }
        
        
        

        $accessory = [];
        $accessoryPrice = 0;

        if($request->accessory_ids)
        {
            $accessory = self::getAccessoriesPrice($request->accessory_ids);
            $accessoryPrice = 500;
            $totalPrice = $price + $accessoryPrice;
            $effectivePrice = round($totalPrice/$totalHoursTrue);
            $pricePerDay = round($totalPrice / $no_of_days);
        }
        
         
        if($totalHoursTrue < $minimum_weekend_hours){

            if($weekEndHours>0 && $weekEndHours<$minimum_weekend_hours)
            {
                $minimum_billing_message = "Minimum billing of ".$minimum_weekend_hours." is applicable.";
                $minimum_hours = $minimum_weekend_hours;

            }
            else if($totalHoursTrue < $priceObj['minimum_hours'])
            {
                $minimum_billing_message = "The price shown is for ".$priceObj['minimum_hours']." hours due to minimum billing policy.";
                $minimum_hours = $priceObj['minimum_hours'];

            }
            else
            {
                $minimum_billing_message = "";
                $minimum_hours = $priceObj['minimum_hours'];
            }
        }
        else
        {
            $minimum_billing_message="";
            $minimum_hours = $priceObj['minimum_hours'];
        }
        
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        
        $model = BikeModel::where('id',$modelId)->first(); 
        if(!$model)
            $minAge = "I am over ".env('MINAGE');
        else
            $minAge = "I am over ".$model->age_limit;

        if((in_array($request->area_id,explode(",",env('AREAINCLUDEDFUEL')))) && ($totalHours < env('FUELINCLUDEDMAXHOURS')) && env('FUELINCPAYLATER'))
        {
            $paylater = true;
        }
        else
        {
            $paylater = false;
        }
        
        $tandcText = "I agree to <a href=\"".env('HOME_URL')."/termsandconditions\">Terms and Conditions</a>";
        
        $conditions = array(
                    
                [
                    'id'=>2,
                    'text'=>$minAge,
                    'required'=>true
                ],
                [
                    'id'=>3,
                    'text'=>"I have a valid driving license and required documents",
                    'required'=>true
                ],
                [
                    'id'=>4,
                    'text'=>$tandcText,
                    'required'=>true
                ]
        );

        return [
            'pricing_id'=>$priceObj->id,
            'accessories' => $accessory,
            'promocode' => [
                            'code' => $coupon,
                            'status' => $couponStatus,
                            'message' => $couponMessage
                          ],
            'bike_rental_total' => round($price),
            'accessories_rental_total' => round($accessoryPrice),
            'total_price' => round($totalPrice), 
            'final_price' => round($finalPrice),
            'tax_string' => $taxDetails->string,
            'tax_name' => $taxDetails->tax_name,
            'tax_amount' => $taxDetails->amount,
            'price_with_tax' =>  round($finalPrice*(1+($taxDetails->amount/100))),
            'discount' => round($discount),
            'effective_price' => round($effectivePrice), 
            'no_of_hours' => round($totalHours), 
            "price_per_day" => round($pricePerDay), 
            "no_of_days" => round($no_of_days),
            "minimum_billing_message" => $minimum_billing_message,
            "minimum_hours" => $minimum_hours,
            "price_per_hour" => $priceObj['hour_weekend'],
            "hour_weekday" => $priceObj['hour_weekday'],
            'full_slab_price'=>$full_slab_price,
            'weekday_price'=>$priceObj['day_weekday'],
            'weekend_price'=>$priceObj['day_weekend'],
            'conditions'=>$conditions,
            'pay_later'=>$paylater
        ];
        
    }

    public static function getTotalHours($date, $sd, $ed, $startTime, $endTime, $firstHour, $lastHour){
      if($date == $sd){
        $noOfHours = PriceController::getNoOfHours($startTime, $lastHour);
      }
      if($date == $ed){
        $noOfHours = PriceController::getNoOfHours($firstHour, $endTime);
      }
      if($date != $sd && $date != $ed){
        $noOfHours = PriceController::getNoOfHours($firstHour, $lastHour);
      }
      return $noOfHours;
    }

    public static function getNoOfHours($startTime, $endTime){
      $start = explode(':', $startTime);
      $end = explode(':', $endTime);
      $hours = $end[0] - $start[0];       

      return $hours;
    }

    public function getPriceAndcheckAvailability(Request $request){
        
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required_without_all:start_datetime,end_datetime',
            'start_time' => 'required_without_all:start_datetime,end_datetime',
            'end_date' => 'required_without_all:start_datetime,end_datetime',
            'end_time' => 'required_without_all:start_datetime,end_datetime',
            'start_datetime' =>'required_without_all:start_date,start_time,end_date,end_time',
            'end_datetime' => 'required_without_all:start_date,start_time,end_date,end_time'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        
        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $cityId = $request->city_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $coupon = $request->coupon;
        $device = $request->device;
        
        if(!$cityId)
        {
            $area = Area::where('id',$areaId)->first();
            $cityId = $area->city_id;
        }
    
        if(isset($request->start_date))
        {
            
            $startDateTimeStr = $request->start_date.' '.$request->start_time;
            $endDateTimeStr = $request->end_date.' '.$request->end_time;
            
        }
        else
        {
            $startDateTimeStr = $request->start_datetime;
            $endDateTimeStr = $request->end_datetime;
            
        }
        
        $startDateTimeCarbon = Carbon::parse($startDateTimeStr);
        $endDateTimeCarbon = Carbon::parse($endDateTimeStr);
     
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $totalPrice = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'city_id' => $cityId, 
            'start_datetime' => $startDateTimeStr, 
            'end_datetime' => $endDateTimeStr,
            'coupon' => $coupon,
            'device'=>$device
        ])->get('bookings/total-price');
        

        $bikeAvailability = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'city_id' => $cityId,
            'start_datetime' => $startDateTimeStr, 
            'end_datetime' => $endDateTimeStr
        ])->get('check-availability');
     
        
        
        $areaIds = Area::where('city_id', $cityId)->get()->lists('id');
        
        if(!$areaId)
        {
            $price = Price::whereIn('area_id',$areaIds)->where('model_id',$modelId)->where('status',1)->first();
        }
        else
        {
            $price = Price::where('area_id',$areaId)->where('model_id',$modelId)->where('status',1)->orderBy('id','desc')->first();
        }
        
        if(!$price)
        {
            $price = Price::where('model_id',$modelId)->where('area_id',0)->where('status',1)->orderBy('id','desc')->first();
        }
        
        if(!$price)
        {
            return [
                'total_price' => 0, 
                'effective_price' => 0, 
                'total_price_with_tax' => 0, 
                'bike_availability' => false, 
                'bike_id'=>"none",
                'number_of_hours' => 0, 
                'minimum_hours'=>0, 
                'minimum_billing_message'=>"", 
                'area_warning'=>"", 
                'store_open'=>""
            ];
        }

        if($totalPrice['no_of_hours'] < 24){
           
            if($totalPrice['total_price']/$price->hour_weekday >=23)
            {
                $minimum_billing_message = "The price shown is for 24 hours due to minimum billing policy.";
                $minimum_hours = 24;

            }
            else if($totalPrice['no_of_hours'] < $totalPrice['minimum_hours'])
            {
                $minimum_billing_message = "The price shown is for ".$totalPrice['minimum_hours']." hours due to minimum billing policy.";
                $minimum_hours = $totalPrice['minimum_hours'];

            }
            else
            {
                $minimum_billing_message = "";
                $minimum_hours = $totalPrice['minimum_hours'];
            }
        }
        else
        {
            $minimum_billing_message="";
            $minimum_hours = $totalPrice['minimum_hours'];
        }
        
         $dispatcher = app('Dingo\Api\Dispatcher');
         $result = $dispatcher->with(['area_id' => $areaId, 'start_datetime' => $startDateTimeStr, 'end_datetime' => $endDateTimeStr])->get('get-store-open-status');
         $areaOpenStatus = $result['result']['data'];
        
        if(in_array($areaId,$areaOpenStatus['start_available_areas']) && in_array($areaId,$areaOpenStatus['end_available_areas']))
        {
            $availableArea['area_warning'] = "";
            $availableArea['store_open'] = "true";
        }
        else
        {
            $warningArray = [];

            if(array_key_exists($areaId,$areaOpenStatus['start_not_available_areas']))
            {
                $startHour = $areaOpenStatus['start_not_available_areas'][$areaId];
                if($startHour==0)
                {

                    array_push($warningArray,"Store closed on start date");
                }
                else
                {
                    $startHourWarningHour = $startHour==0?"12 am":($startHour==12?'12 pm':($startHour>12?($startHour-12).' pm':$startHour.' am'));
                    array_push($warningArray,"Opens at ".$startHourWarningHour);
                }


            }
            if(array_key_exists($areaId,$areaOpenStatus['end_not_available_areas']))
            {
                $endHour = $areaOpenStatus['end_not_available_areas'][$areaId];
                if($endHour==0)
                {

                    array_push($warningArray,"Store closed on end date");
                }
                else
                {
                    $endHourWarningHour = $endHour==0?"12 am":($endHour==12?'12 pm':($endHour>12?($endHour-12).' pm':$endHour.' am'));

                    array_push($warningArray,"Closes at ".$endHourWarningHour);
                }
            }

            $warningStr = implode(' and ',$warningArray);

            $availableArea['area_warning'] = "(".$warningStr.")";
            $availableArea['store_open'] = "false";
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        if (array_key_exists('bike_id', $bikeAvailability) ) 
        {
            
            if($bikeAvailability['bike_id'] == "none")
                $bikeAvailabilityBool = false;
            else 
                $bikeAvailabilityBool = true;
            
            return [
                'total_price' => $totalPrice['final_price'], 
                'effective_price' => $totalPrice['effective_price'], 
                'total_price_with_tax' => round(($totalPrice['final_price']*(1+($taxDetails->amount/100))),2), 
                'bike_availability' => $bikeAvailabilityBool, 
                'bike_id' => $bikeAvailability['bike_id'],
                'number_of_hours' => $totalPrice['no_of_hours'], 
                'minimum_hours'=>$minimum_hours, 
                'minimum_billing_message'=>$minimum_billing_message, 
                'area_warning'=>$availableArea['area_warning'], 
                'store_open'=>$availableArea['store_open']
            ];
        }
        else
        {
            return [
                'total_price' => $totalPrice['total_price'], 
                'effective_price' => $totalPrice['effective_price'], 
                'total_price_with_tax' => $totalPrice['total_price']*round((1+($taxDetails->amount/100)),2), 
                'bike_availability' => false, 
                'bike_id'=>"none",
                'number_of_hours' => $totalPrice['no_of_hours'], 
                'minimum_hours'=>$minimum_hours, 
                'minimum_billing_message'=>$minimum_billing_message, 
                'area_warning'=>$availableArea['area_warning'], 
                'store_open'=>$availableArea['store_open']
            ];
        }
    }
 
    public function isBikeAvailable(Request $request){
      $dispatcher = app('Dingo\Api\Dispatcher');
     
      $dispatcherData = ['start_date' => $request->start_date, 'end_date' => $request->end_date, 'start_time' => $request->start_time, 'end_time' => $request->end_time, 'model_id' => $request->model_id, 'area_id' =>  $request->area_id];
      $bikeAvailability = $dispatcher->with($dispatcherData)->get('check-availability');
      if (!$bikeAvailability['bike_id'])
        return $this->response->array(['available' => false, 'error' => 'Bike is not available.']);
      else
        return $this->response->array(['available' => true, 'bike_id' => $bikeAvailability['bike_id']]);
    }

    public static function getAccessoriesPrice($ids)
    {
      $accessoryIds = explode(',', $ids);
      $dispatcher = app('Dingo\Api\Dispatcher');
      $totalPrice = 0;
      foreach ($accessoryIds as $id) {
        $accessory = $dispatcher->get('accessories/{'.$id.'}');
        $totalPrice = $totalPrice + $accessory['amount'];
      }

      return [
        [
          "id" => "2",
          "total_price" => 100,
        ],
        [
          "id" => "3",
          "total_price" => 100,
        ]
      ];
    }
    
    
    public function indexVendor(){

        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();
        $areaListArray = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        $priceList = Price::whereIn('area_id',$areaListArray)->get();
        $modelList = BikeModel::where('status',1)->get();
        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get();
        foreach ($priceList as $price) {
          $model = $price->model;
          $area = $price->area;
          $price->model_name = $model['bike_model'];
          $price->area_name = $area['area'];
        }
        return view('vendor.price.index', compact('priceList', 'modelList', 'areaList'));
    }

    public function addVendor(Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'price_per_hour' => 'required',
        'minimum_hours' => 'required',
        
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/vendor/price')->withErrors($validator->errors());        
      }
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        if (in_array($request->areaId, $areaList->toArray())) {
      
            Price::create(['model_id' => $request->modelId, 'area_id' => $request->areaId, 'price_per_hour' => $request->price_per_hour, 'minimum_hours' => $request->minimum_hours]);
            return redirect('/vendor/price'); 
        }
        else {
            return redirect('/vendor/price'); 

        }
    }

    public function editVendor($price_id,Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'price_per_hour' => 'required',
        'minimum_hours' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/vendor/price')->withErrors($validator->errors());        
      }
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        if (in_array($request->areaId, $areaList->toArray())) {
            
            Price::where('id',$price_id)->update(['model_id' => $request->modelId, 'area_id' => $request->areaId, 'price_per_hour' => $request->price_per_hour, 'minimum_hours' => $request->minimum_hours]);
            
            return redirect('/vendor/price');   
        }
        else {
            return redirect('/vendor/price'); 

        }
    }   

}
