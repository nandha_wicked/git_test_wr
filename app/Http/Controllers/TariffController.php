<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Area;
use App\Models\City;
use App\Models\BikeModel;
use App\Models\Bike;
use App\Models\Price;
use Session;



class TariffController extends AppController
{
    
    public function index(){
        
        $cityId = Session::get('current_cityId');
       
    if(!$cityId)
        $cityId = "1";
        
        $areas = Area::where('city_id',$cityId)->where('status',1)->get()->lists('id');
        $modelIDs = Bike::whereIn('area_id',$areas)->where('status',1)->get()->lists('model_id');
        $modelIDs=$modelIDs->toArray();
        $modelIDs = array_unique($modelIDs);
        
        $bikeModels=BikeModel::whereIn('id',$modelIDs)->get();
        
        foreach ($bikeModels as $bikeModel)
        {
          
            $price = Price::where('model_id',$bikeModel->id)->whereIn('area_id',$areas)->orderBy('id','desc')->where('status',1)->first();
            if(!$price)
            {
                $price = Price::where('model_id',$bikeModel->id)->where('area_id',0)->where('status',1)->orderBy('id','desc')->first();
            }
            
            
            $bikeModel['price_per_hour'] = $price->price_per_hour;
            $bikeModel['price_per_day'] = $price->price_per_hour*24;
            
            $bikeModel['hour_weekday'] = $price['hour_weekday'];
            $bikeModel['hour_weekend'] = $price['hour_weekend'];
            $bikeModel['day_weekday'] = $price['day_weekday'];
            $bikeModel['day_weekend'] = $price['day_weekend'];
            $bikeModel['five_weekday'] = $price['five_weekday'];
            $bikeModel['week'] = $price['week'];
            $bikeModel['month'] = $price['month'];
            
            $bikeModel['minimum_hours'] = $price->minimum_hours;
            $bikeModel['minimum_price'] = $price->minimum_hours*$price->hour_weekday;
            
            $bikeModel['bike_make_name'] = $bikeModel->getBikeMakeName($bikeModel->bike_make_id);
            
            
            
        }

    	return view('front/tariff',compact('bikeModels'));
    }
}
