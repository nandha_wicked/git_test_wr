<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeMake;
use App\Models\Bike;
use App\Models\Price;
use App\Models\Packages;
use App\Models\BikeModelImage;
use App\Models\Image;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\City;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\PackagesUser;


class PackagesdashboardController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){

    $packagesMakeList = Packages::where('status', 1)->get();
    $packagesModelList = Packages::where('status', 1)->get();
    foreach ($packagesModelList as $model) {
      $coverImg = Image::where('id', $model->cover_img_id)->first();
      
      $model['cover_img'] = ($coverImg) ? $coverImg['full'] : '';
     
    }
    return view('front.packages.index',compact('packagesMakeList','packagesModelList'));
  }

  public function detail($url){

    $url_id = DB::table ('bikation_packages')->select('id')->where('url', $url)->first();
    $id=$url_id->id;
    $packageDetail = Packages::where('id', $id)->first();

    $reviews = PackagesReview::where('product_id', $id)->orderBy('created_at','desc')->paginate(3);
    $coverImg = Image::where('id', $packageDetail->cover_img_id)->first();
    $packageDetail['cover_img'] = ($coverImg) ? $coverImg['full'] : '';

    $total_rating = DB::table ('bikation_packages_reviews')
                ->select (DB::raw ('AVG(rating) as average_score'))
                ->where('product_id', $id)->first();
    $total_rows = DB::table ('bikation_packages_reviews')
                ->select ('id', DB::raw ('AVG(rating) as average_score'))
                ->where('product_id', $id)->count();


                 $packagesreviewdate = PackagesReview::where('product_id', $id)->get();
               
    return view('front.packages.detail',compact('packageDetail','reviews','total_rating','total_rows'));
  }

public function addreviews(Request $request){

    $rules = array(
      'comment'=>'required|min:10',
      'rating'=>'required|integer|between:1,5'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('packages/'.$request->url)->withErrors($validator->errors());
    }
    $user_id=2;
    
    PackagesReview::create(['product_id' => $request->product_id,'product_name' => $request->product_name, 'user_id' => $user_id,'rating' => $request->rating,'comment' => $request->comment,'url'=>$request->url]);
   
    return redirect('packages/'.$request->url);

  
  }
  public function addpackage(Request $request){

    $rules = array(
      'firstname'=>'required',
      'lastname'=>'required',
       'email'=>'required',
      'mobileNo'=>'required',
      'no_seats'=>'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('packages/'.$request->url)->withErrors($validator->errors());
    }
    $user_id=2;
    $packages_seats= DB::table('bikation_packages')->select('available_seat')->where('id',$request->package_id)->first();
    $available = $packages_seats->available_seat - $request->no_seats;
    Packages::where('id',$request->package_id)->update(['available_seat' => $available]);
    
    PackagesUser::create(['user_id' => $user_id,'package_id' => $request->package_id, 'package_name' => $request->package_name,'no_seats' => $request->no_seats,'firstname' => $request->firstname, 'lastname' => $request->lastname,'gender' => $request->gender,'email' => $request->email,'mobileNo' => $request->mobileNo,'url'=>$request->url ]);
    
    return redirect('packages/'.$request->url);

  
  }
  
    
}
