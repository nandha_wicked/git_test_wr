<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\BikeAvailabilityController;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Partner;
use App\Models\Bike;
use App\Models\Area;
use App\Models\Booking;
use App\Models\BikeModel;
use App\Models\Availability;
use App\Models\Slot;
use App\Models\Available;
use App\Models\Service;
use App\Models\Occupancy;
use App\Models\Capacity;
use Carbon\Carbon;
use DB;
use Auth;



class BikeFilterController extends AppController
{
    
    public function index(){
      $area = new Area();
      $activeAreas = $area->getAllActiveAreas();
      
      return view('admin.bikeFilter.index',compact('activeAreas'));
    }



    public function getBookings(Request $request){
        $rules = array(
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
        }

        $startDateTime = $request->start_date.' '.$request->start_time.':00';
        $endDateTime = $request->end_date.' '.$request->end_time.':00';

        if(isset($request->area_id))
        {
            $bookings = Booking::where('end_datetime', '>', $startDateTime)->where('start_datetime', '<', $endDateTime)->where('area_id',$request->area_id)->get();
        }
        else
        {
            $bookings = Booking::where('end_datetime', '>', $startDateTime)->where('start_datetime', '<', $endDateTime)->get();
        }

        

        foreach ($bookings as $booking) {
            $user = User::where('id', $booking['user_id'])->first();
            $model = BikeModel::where('id', $booking['model_id'])->first();            
            $area = Area::where('id', $booking['area_id'])->first();  
            $booking['custName'] = $user['first_name'];
            $booking['email'] = $user['email'];
            $booking['mobile'] = $user['mobile_num'];
            $booking['area'] = $area['area'];
            $booking['model'] = $model['bike_model'];
        }
        return $bookings;
    }
    
    
    
    public function indexVendor(){
        $user = Auth::user();
        $userList = User::where('id',$user->id)->get();
        $vendor = Vendor::where('user_id',$user->id)->first();
        $areas = Area::where('vendor_id',$vendor['id'])->where('status', 1)->get();

      
        return view('vendor.bikeFilter.index',compact('areas'));
    }

    public function getBikeCountVendor(Request $request){
      $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'area_id' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
      }

      $dispatcher = app('Dingo\Api\Dispatcher');
      $bookings = $dispatcher->with(['start_date' => $request->start_date, 'end_date' => $request->end_date, 
                              'start_time' => $request->start_time, 'end_time' => $request->end_time])
                              ->get('filter/bookings-vendor');

      $bookedBikeId = [];
      foreach ($bookings as $booking) {
        array_push($bookedBikeId, $booking['bike_id']);
      }
      $bookedBikeId = array_unique($bookedBikeId);
      
      $modelIds = Bike::where('area_id', $request->area_id)->where('status', 1)->groupBy('model_id')->get()->lists('model_id');
      $bikeIdsByArea = Bike::where('area_id', $request->area_id)->where('status', 1)->get()->lists('id');
      $modelData = [];

      foreach ($modelIds  as $modelId) {
        $notAvailBikeIds = [];
        $bikeIds = [];
        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $bikes = $model->bikes;
        foreach ($bikes as $bike) {
          if (in_array($bike['id'], $bikeIdsByArea->toArray()) == true) {
            array_push($bikeIds, $bike['id']);
            if (in_array($bike['id'], $bookedBikeId) == true) {
              array_push($notAvailBikeIds, $bike['id']);
            }
          }
        }
        
        $availCount = count($bikeIds) - count($notAvailBikeIds);
        $counts = ['model_id' => $model['id'], 'model_name' => $model['bike_model'], 'total_bikes' => count($bikeIds), 'available_count' => $availCount, 'not_available_count' => count($notAvailBikeIds)];
        array_push($modelData, $counts);
      }
      return $modelData;
    }

    public function getBookingsVendor(Request $request){
      $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
      }

      $startDateTime = $request->start_date.' '.$request->start_time.':00';
      $endDateTime = $request->end_date.' '.$request->end_time.':00';

      
        $user = Auth::user();
        $userList = User::where('id',$user->id)->get();
        $vendor = Vendor::where('user_id',$user->id)->first();
        $bikeListArray = Bike::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');

      $bookings = Booking::where('end_datetime', '>', $startDateTime)
      ->where('start_datetime', '<', $endDateTime)->whereIn('bike_id',$bikeListArray)
      ->get();

      foreach ($bookings as $booking) {
        $user = User::where('id', $booking['user_id'])->first();
        $model = BikeModel::where('id', $booking['model_id'])->first();            
        $area = Area::where('id', $booking['area_id'])->first();
        $booking['custName'] = $user['first_name'];
        $booking['email'] = $user['email'];
        $booking['mobile'] = $user['mobile_num'];
        $booking['area'] = $area['area'];
        $booking['model'] = $model['bike_model'];
      }
      return $bookings;
    }
    
    
    
    
    
    
    
     public function indexPartner(){
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        
        $area_ids = explode(',',$partner->area_id);
        $areas = Area::whereIn('id',$area_ids)->where('status', 1)->get();

      
        return view('partner.bikeFilter.index',compact('areas'));
    }

   
    public function getBookingsPartner(Request $request){
      $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
      }

      $startDateTime = $request->start_date.' '.$request->start_time.':00';
      $endDateTime = $request->end_date.' '.$request->end_time.':00';

      
     
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $modelIds = explode(',',$partner->model_id);
        $area_ids = explode(',',$partner->area_id);

        if($startDateTime >= $partner->date_added){


          $bookings = Booking::where('end_datetime', '>', $startDateTime)
          ->where('start_datetime', '<', $endDateTime)->whereIn('area_id',$area_ids)->whereIn('model_id',$modelIds)
          ->get();

          foreach ($bookings as $booking) {
            $user = User::where('id', $booking['user_id'])->first();
            
            $booking['custName'] = $user['first_name'];
            $booking['area'] = $booking->getCityNameThis();
            $booking['model'] = $booking->getBikeModelNameThis();
          }
          return $bookings;
        }
        else
        {
            return null;
        }
    }
    
    
    
    public function indexErp(){
      $area = new Area();
      $activeAreas = $area->getAllActiveAreas();
      
      return view('erp.bikeFilter.index',compact('activeAreas'));
    }
    
    //AMH Bike Filter
    
    public function indexAMH(){
      $area = new Area();
      $activeAreas = $area->getAllActiveAreas();
      
      return view('erp.bikeFilterAMH.index',compact('activeAreas'));
    }
    
    
    
    public function hourByHourIndex(){
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        $date_hour_read_format =[];
        $model_data_array = [];
        $start_date = "";
        $end_date = "";
        $start_time = "";
        $end_time = "";
        $area_id= null;
        $width = 0;
        $aorc="withoutService";




        return view('admin.hourByHourAMH.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width','aorc'));
    }

 
    
    public function hourByHourAvailableIndex(){
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        $date_hour_read_format =[];
        $model_data_array = [];
        $start_date = "";
        $end_date = "";
        $start_time = "";
        $end_time = "";
        $area_id= null;
        $width = 0;
        




        return view('admin.hourByHourAMHAvailable.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width'));
    }
    
    
    public function hourByHourCapacityIndex(){
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        $date_hour_read_format =[];
        $model_data_array = [];
        $start_date = "";
        $end_date = "";
        $start_time = "";
        $end_time = "";
        $area_id= null;
        $width = 0;
        




        return view('admin.hourByHourAMHCapacity.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width'));
    }
    
    
    
    
    public function hourByHourFilterCapacityAMH(Request $request){
          $rules = array(
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'area_id' => 'required'
          );

          $validator = Validator::make($request->all(),$rules);

          if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());        
          }
        
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        $date_hour_read_format=[];
        $startIndex = $startDateTimeCarbon->copy()->hour;
        $endIndex = $startIndex;
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $hour_read_format = [];
            for($dateHour = $date->copy();$dateHour->lt($date->copy()->addDay());$dateHour->addHour())
            {
                if($dateHour->gte($startDateTimeCarbon)&&$dateHour->lt($endDateTimeCarbon))
                {
                    array_push($hour_read_format,$dateHour->hour);
                    $endIndex++;
                }
            }
            
            $dates[] = $date->format('Y-m-d');
            
            $date_hour_read_format[]=['date'=>$date->format('d-M'),'hours'=>$hour_read_format,'count'=>count($hour_read_format)];
        }
        
        $endIndex = $endIndex-$startIndex;
        
        
        
        
        
        $capacityObjs = Capacity::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $capacityObjByModel = [];
        
        
        foreach($capacityObjs as $capacityObj)
        {
            if(!array_key_exists($capacityObj->model_id,$capacityObjByModel))
            {
                $capacityObjByModel[$capacityObj->model_id] = "";
            }
            $capacityObjByModel[$capacityObj->model_id].=$capacityObj->hour_string.",";             
            
        }
        
        $model_data_array = [];
        $count2 = 0;
        
        foreach($capacityObjByModel as $modelId => $dateStr)
        {

            
            $total_capacity_array = [];
            
            $total_capacity_array = explode(',',$capacityObjByModel[$modelId]);
            array_splice($total_capacity_array,0,$startIndex);
            array_splice($total_capacity_array,$endIndex);
            
            if(array_sum($total_capacity_array)>0)
            {
                
                $aorc_count_color_array=[];
                $occupancy_count_color_array=[];
                $count2 = count($total_capacity_array);
                
                for($j=0;$j<$count2;$j++)
                {
                    
                    $aorc_count_color_array_element = ['count'=>$total_capacity_array[$j],'color'=>""];

                    array_push($aorc_count_color_array,$aorc_count_color_array_element);
                }
                
                
                
                $model = BikeModel::where('id',$modelId)->first();
                
                $counts = ['model_name' => $model->bike_model, 'aorc' =>$aorc_count_color_array,'percentage'=>"NA"];
                array_push($model_data_array, $counts);
            }
            
            
            
        }
        $width = $count2*45;
        $width+=310;
        $aorc = $request->aorc;       
            
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $area_id=  $request->area_id;
        
        return view('admin.hourByHourAMHCapacity.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width','aorc'));
    }
    
    public function hourByHourFilterAvailable(Request $request){
          $rules = array(
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'area_id' => 'required'
          );

          $validator = Validator::make($request->all(),$rules);

          if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());        
          }
        
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        $date_hour_read_format=[];
        $startIndex = $startDateTimeCarbon->copy()->hour;
        $endIndex = $startIndex;
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $hour_read_format = [];
            for($dateHour = $date->copy();$dateHour->lt($date->copy()->addDay());$dateHour->addHour())
            {
                if($dateHour->gte($startDateTimeCarbon)&&$dateHour->lt($endDateTimeCarbon))
                {
                    array_push($hour_read_format,$dateHour->hour);
                    $endIndex++;
                }
            }
            
            $dates[] = $date->format('Y-m-d');
            
            $date_hour_read_format[]=['date'=>$date->format('d-M'),'hours'=>$hour_read_format,'count'=>count($hour_read_format)];
        }
        
        $endIndex = $endIndex-$startIndex;
        
                
        $capacityObjs = Capacity::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $capacityObjByModel = [];
        
        
        foreach($capacityObjs as $capacityObj)
        {
            if(!array_key_exists($capacityObj->model_id,$capacityObjByModel))
            {
                $capacityObjByModel[$capacityObj->model_id] = "";
            }
            $capacityObjByModel[$capacityObj->model_id].=$capacityObj->hour_string.",";             
            
        }
        
        
        $availableObjs = Available::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $availableObjByModel = [];
        
        
        foreach($availableObjs as $availableObj)
        {
            if(!array_key_exists($availableObj->model_id,$availableObjByModel))
            {
                $availableObjByModel[$availableObj->model_id] = "";
            }
            $availableObjByModel[$availableObj->model_id].=$availableObj->hour_string.",";             
            
        }
        
        $model_data_array = [];
        
        foreach($capacityObjByModel as $modelId => $dateStr)
        {
            $total_capacity_array = [];
            
            $total_capacity_array = explode(',',$capacityObjByModel[$modelId]);
            array_splice($total_capacity_array,0,$startIndex);
            array_splice($total_capacity_array,$endIndex);
            
            if(array_sum($total_capacity_array)>0)
            {
                                
                $total_available_array = [];
            
                $total_available_array = explode(',',$availableObjByModel[$modelId]);
                array_splice($total_available_array,0,$startIndex);
                array_splice($total_available_array,$endIndex);
                
                
                $aorc_count_color_array=[];
                $occupancy_count_color_array=[];
                $count2 = count($total_available_array);
                $totalAvailable = 0;
                
                for($j=0;$j<$count2;$j++)
                {
                    if($total_available_array[$j] == 0)
                        $color = "red";
                    else
                    {
                        $color = "green";
                        $totalAvailable++;
                    }
                    
                    $aorc_count_color_array_element = ['count'=>$total_available_array[$j],'color'=>$color];

                    array_push($aorc_count_color_array,$aorc_count_color_array_element);
                }
                
                

                $percentage = round($totalAvailable/$count2*100);
                
                $model = BikeModel::where('id',$modelId)->first();
                
                $counts = ['model_name' => $model->bike_model, 'aorc' =>$aorc_count_color_array,'percentage'=>$percentage];
                array_push($model_data_array, $counts);
            }
            
            
            
        }
        $width = $count2*45;
        $width+=310;
        $aorc = $request->aorc;       
            
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $area_id=  $request->area_id;
        
        return view('admin.hourByHourAMHAvailable.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width','aorc'));
    }
    
    
    
    
    public function hourByHourFilterOccupancyAMH(Request $request){
          $rules = array(
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'area_id' => 'required'
          );

          $validator = Validator::make($request->all(),$rules);

          if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());        
          }
        
        $area = new Area();
        $activeAreas = $area->getAllActiveAreas();
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        $date_hour_read_format=[];
        $startIndex = $startDateTimeCarbon->copy()->hour;
        $endIndex = $startIndex;
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $hour_read_format = [];
            for($dateHour = $date->copy();$dateHour->lt($date->copy()->addDay());$dateHour->addHour())
            {
                if($dateHour->gte($startDateTimeCarbon)&&$dateHour->lt($endDateTimeCarbon))
                {
                    array_push($hour_read_format,$dateHour->hour);
                    $endIndex++;
                }
            }
            
            $dates[] = $date->format('Y-m-d');
            
            $date_hour_read_format[]=['date'=>$date->format('d-M'),'hours'=>$hour_read_format,'count'=>count($hour_read_format)];
        }
        
        $endIndex = $endIndex-$startIndex;
        
                
        $capacityObjs = Capacity::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $capacityObjByModel = [];
        
        
        foreach($capacityObjs as $capacityObj)
        {
            if(!array_key_exists($capacityObj->model_id,$capacityObjByModel))
            {
                $capacityObjByModel[$capacityObj->model_id] = "";
            }
            $capacityObjByModel[$capacityObj->model_id].=$capacityObj->hour_string.",";             
            
        }
        
        $availableObjs = Available::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $availableObjByModel = [];
        
        
        foreach($availableObjs as $availableObj)
        {
            if(!array_key_exists($availableObj->model_id,$availableObjByModel))
            {
                $availableObjByModel[$availableObj->model_id] = "";
            }
            $availableObjByModel[$availableObj->model_id].=$availableObj->hour_string.",";             
            
        }
        
        $serviceObjs = Service::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $serviceObjByModel = [];
        
        
        foreach($serviceObjs as $serviceObj)
        {
            if(!array_key_exists($serviceObj->model_id,$serviceObjByModel))
            {
                $serviceObjByModel[$serviceObj->model_id] = "";
            }
            $serviceObjByModel[$serviceObj->model_id].=$serviceObj->hour_string.",";             
            
        }
        
        
        $model_data_array = [];
        $count2 = 0;
        
        foreach($capacityObjByModel as $modelId => $dateStr)
        {
            
            $total_capacity_array = [];
            
            $total_capacity_array = explode(',',$capacityObjByModel[$modelId]);
            array_splice($total_capacity_array,0,$startIndex);
            array_splice($total_capacity_array,$endIndex);
            
            if(array_sum($total_capacity_array)>0)
            {
            
                $total_available_array = [];
            
                $total_available_array = explode(',',$availableObjByModel[$modelId]);
                array_splice($total_available_array,0,$startIndex);
                array_splice($total_available_array,$endIndex);
                
                
                $total_service_array = [];
                
                $total_service_array = explode(',',$serviceObjByModel[$modelId]);
                array_splice($total_service_array,0,$startIndex);
                array_splice($total_service_array,$endIndex);
                
                
                $aorc_count_color_array=[];
                $occupancy_count_color_array=[];
                $count2 = count($total_available_array);
                
                $totalOccupancy = 0;
                $totalCapacity = 0;
                $trueOccupancy = 0;
                $trueCapacity = 0;
                
                for($j=0;$j<$count2;$j++)
                {
                    if($request->aorc=="withoutService")
                    {
                        if($total_capacity_array[$j] != 0)
                        {
                            $per = ($total_capacity_array[$j]-$total_available_array[$j]-$total_service_array[$j])/$total_capacity_array[$j];
                        }
                        else
                        {
                            $per = 1;
                        }
                        if($per==1)
                            $color = "green";
                        else if($per>=0.75)
                            $color = "yellowgreen";
                        else if($per>=0.5)
                            $color = "yellow";
                        else if($per>=0.25)
                            $color = "orange";
                        else if($per>0)
                            $color = "orangered";
                        else if($per==0)
                            $color = "red";     
                    }
                    else
                    {
                        
                        if(($total_capacity_array[$j]-$total_service_array[$j]) != 0)
                            $per = ($total_capacity_array[$j]-$total_available_array[$j]-$total_service_array[$j])/($total_capacity_array[$j]-$total_service_array[$j]);
                        else
                            $per = 1;

                        $trueOccupancy+=($total_capacity_array[$j]-$total_available_array[$j]-$total_service_array[$j]);
                        $trueCapacity+=($total_capacity_array[$j]-$total_service_array[$j]);


                        if($per==1)
                        $color = "green";
                        else if($per>=0.75)
                            $color = "yellowgreen";
                        else if($per>=0.5)
                            $color = "yellow";
                        else if($per>=0.25)
                            $color = "orange";
                        else if($per>0)
                            $color = "orangered";
                        else if($per==0)
                            $color = "red"; 
                        

                    }
                    
                    $aorc_count_color_array_element = ['count'=>$total_available_array[$j],'color'=>$color];

                    $totalOccupancy+=($total_capacity_array[$j]-$total_available_array[$j]-$total_service_array[$j]);
                    $totalCapacity+=$total_capacity_array[$j];


                    array_push($aorc_count_color_array,$aorc_count_color_array_element);
                }

                if($totalCapacity==0)
                    $percentage ="NA";
                else
                {
                    if($request->aorc=="withoutService")
                    {
                        $percentage = round($totalOccupancy/$totalCapacity*100);
                    }
                    else
                    {
                        if($trueCapacity==0)
                        {
                            $percentage =100;   
                        }
                        else
                        {
                            $percentage = round($trueOccupancy/$trueCapacity*100);
                        }
                    }

                }
                
                
                
                $model = BikeModel::where('id',$modelId)->first();
                
                $counts = ['model_name' => $model->bike_model, 'aorc' =>$aorc_count_color_array,'percentage'=>$percentage];
                array_push($model_data_array, $counts);
            }
            
            
            
        }
        $width = $count2*45;
        $width+=310;
        $aorc = $request->aorc;       
            
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $area_id=  $request->area_id;
        
        return view('admin.hourByHourAMH.index',compact('activeAreas','date_hour_read_format','model_data_array','start_date','end_date','start_time','end_time','area_id','width','aorc'));
    }
    
                
                
    
    /*END OF CONTROLLER CLASS*/

    
    
}
