<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Bike;
use App\Models\City;
use App\Models\Area;
use App\Models\News;
use App\Models\Price;
use App\Models\ContactEnquiry;
use App\Models\EmailSubscribe;
use App\Models\User;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Http\Controllers\PriceController;
use Auth;
use Crypt;
use Response;
use Session;
use Mail;
use Hash;
use DB;
use Config;
use Input;
use Carbon\Carbon;

class FrontViewController extends AppController {

    
    
    public function getModelsAMH(Request $request){
        
        $daysOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

        Session::put('bkPromoCode', null);
        $cityId = Session::get('current_cityId');

        $priorityModelId = Session::get('booking_model_priority');
        
        $startTimeArr = explode(':',$request->start_time);
        if(count($startTimeArr) == 1)
        {
            $startTime = str_pad($request->start_time,2,"0",STR_PAD_LEFT).":00";
        }
        else
        {
            $startTime = $request->start_time;
        }
        
        $endTimeArr = explode(':',$request->end_time);
        if(count($endTimeArr) == 1)
        {
            $endTime = str_pad($request->end_time,2,"0",STR_PAD_LEFT).":00";
        }
        else
        {
            $endTime = $request->end_time;
        }
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$startTime);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$endTime);


        $startDate = $startDateTimeCarbon->format('d M Y');
        $endDate = $endDateTimeCarbon->format('d M Y');
        $startTime = $startDateTimeCarbon->format('H:i');
        $endTime = $endDateTimeCarbon->format('H:i');

        $sd = $startDateTimeCarbon->format('Y-m-d');
        $ed = $endDateTimeCarbon->format('Y-m-d');

        $month = $startDateTimeCarbon->month;
        $year = $startDateTimeCarbon->year;

        if($startDateTimeCarbon > $endDateTimeCarbon)
          return 'Sorry! Something went wrong.';


        if(!$cityId)
            $cityId = "1";

        $modelsData = AMHAvailabilityController::checkAvailabilityByCityForFrontPage($startDateTimeCarbon,$endDateTimeCarbon,$cityId);
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $result = $dispatcher->with([
            'start_datetime' => $request->start_date." ".$request->start_time,
            'end_datetime' => $request->end_date." ".$request->end_time
        ])->get('get-store-open-status');
        $areaOpenStatus = $result['result']['data'];
        

        $areas = Area::where('city_id', $cityId)->where('status',1)->get();
        $areaIds = $areas->lists('id')->toArray();

        
        $bikeModels = BikeModel::where('status',1)->get();


        $areasByKey = $areas->keyBy('id');
        $modelsByKey = $bikeModels->keyBy('id');

        $models = [];

        foreach($modelsData as $modelId => $modelData)
        {
            $model = [];
            
            if(array_key_exists('available areas',$modelData)||array_key_exists('not available areas',$modelData))
            {
                $model['id'] = $modelId;
                $model['name'] = $modelsByKey[$modelId]->bike_model;
                $model['make_id'] = $modelsByKey[$modelId]->bike_make_id;
                $model['image_id'] = $modelsByKey[$modelId]->thumbnail_img_id;
                $model['image'] = $modelsByKey[$modelId]->getThumbnailImg();
                $model['year'] = $startDateTimeCarbon->year;
                $model['month'] = $startDateTimeCarbon->month;
                $model['cityId'] = $cityId;
                $model['startDate'] = $startDate;
                $model['endDate'] = $endDate;
                $model['startTime'] = $startTime;
                $model['endTime'] = $endTime;


                $areasWithModel = [];
                $availableAreas = [];
                if(array_key_exists('available areas',$modelData))
                {
                    $availableAreaIds = explode(',',$modelData['available areas']);
                    $areasWithModel = array_merge($areasWithModel,$availableAreaIds);

                    foreach($availableAreaIds as $availableAreaId)
                    {
                        $availableArea = clone $areasByKey[$availableAreaId];
                        
                        //check for open hours against start and end time
                        
                        if(in_array($availableAreaId,$areaOpenStatus['start_available_areas']) && in_array($availableAreaId,$areaOpenStatus['end_available_areas']))
                        {
                            $availableArea['area_warning'] = "";
                            $availableArea['store_open'] = "true";
                        }
                        else
                        {
                            $warningArray = [];
                            
                            if(array_key_exists($availableAreaId,$areaOpenStatus['start_not_available_areas']))
                            {
                                $startHour = $areaOpenStatus['start_not_available_areas'][$availableAreaId];
                                if($startHour==0)
                                {
                                    
                                    array_push($warningArray,"Store closed on ".$daysOfWeek[$areaOpenStatus['start_day']]);
                                }
                                else
                                {
                                    $startHourWarningHour = $startHour==0?"12 am":($startHour==12?'12 pm':($startHour>12?($startHour-12).' pm':$startHour.' am'));
                                    array_push($warningArray,"Opens at ".$startHourWarningHour);
                                }
                                
                                
                            }
                            if(array_key_exists($availableAreaId,$areaOpenStatus['end_not_available_areas']))
                            {
                                $endHour = $areaOpenStatus['end_not_available_areas'][$availableAreaId];
                                if($endHour==0)
                                {
                                    
                                    array_push($warningArray,"Store closed on ".$daysOfWeek[$areaOpenStatus['end_day']]);
                                }
                                else
                                {
                                    $endHourWarningHour = $endHour==0?"12 am":($endHour==12?'12 pm':($endHour>12?($endHour-12).' pm':$endHour.' am'));

                                    array_push($warningArray,"Closes at ".$endHourWarningHour);
                                }
                            }
                            
                            $warningStr = implode(' and ',$warningArray);
                            
                            $availableArea['area_warning'] = "(".$warningStr.")";
                            $availableArea['store_open'] = "false";
                        }
                        
                        $availableArea['bikeAvailabilityStatus'] = "true";

                        $dispatcher = app('Dingo\Api\Dispatcher');
                        $price = $dispatcher->with(['model_id' => $modelId,'area_id' => $availableAreaId,'start_date' => $sd, 'end_date' => $ed,'start_time' => $startTime, 'end_time' => $endTime])->get('bookings/total-price');
                        

                        if(array_key_exists("hour_weekday",$price))
                        {
                            $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                            $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';

                            $availableArea['price'] = [$line2,$line1];
                            $availableArea['price_details'] = $price;
                            array_push($availableAreas,$availableArea);
                        }
                        
                    }

                }
                $notAvailableAreas = [];
                if(array_key_exists('not available areas',$modelData))
                {
                    $notAvailableAreaIds = explode(',',$modelData['not available areas']);
                    $areasWithModel = array_merge($areasWithModel,$notAvailableAreaIds);
                    foreach($notAvailableAreaIds as $notAvailableAreaId)
                    {
                        $notAvailableArea = clone $areasByKey[$notAvailableAreaId];
                        $notAvailableArea['bikeAvailabilityStatus'] = "false";
                        
                        $notAvailableArea['area_warning'] = "";
                        $notAvailableArea['store_open'] = "true";

                        $dispatcher = app('Dingo\Api\Dispatcher');
                        $price = $dispatcher->with(['model_id' => $modelId,'area_id' => $notAvailableAreaId,'start_date' => $sd, 'end_date' => $ed,'start_time' => $startTime, 'end_time' => $endTime])->get('bookings/total-price');


                        
                        $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                        $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';

                        $notAvailableArea['price'] = [$line2,$line1];
                        $notAvailableArea['price_details'] = $price;

                        array_push($notAvailableAreas,$notAvailableArea);

                    }            
                }

                $model['available_locations'] = array_merge($availableAreas,$notAvailableAreas);



                $areasWithoutModel = [];        
                $areaIdsWithoutModel = array_diff($areaIds,$areasWithModel);
                foreach($areaIdsWithoutModel as $areaIdWithoutModel)
                {
                    $areaWithoutModel = clone $areasByKey[$areaIdWithoutModel];

                    $areaWithoutModel['price'] = ["Minimum billing of 10 hours on weekdays and 24 hours on weekends","Rs /hour and /day"];

                    array_push($areasWithoutModel, $areaWithoutModel);
                }

                $model['not_available_locations'] = $areasWithoutModel;


                if(count($availableAreas)>0)
                {
                    $model['bikeAvailability'] = true;
                }
                else
                {
                    $model['bikeAvailability'] = false;
                }


                $model['minimum_billing_message'] = $model['available_locations'][0]['price_details']['minimum_billing_message'];
                $model['minimum_hours'] = $model['available_locations'][0]['price_details']['minimum_hours'];
                $model['totalPrice'] = $model['available_locations'][0]['price_details']['total_price'];
                $model['effectivePrice'] = $model['available_locations'][0]['price_details']['effective_price'];
                $model['price_per_hour'] = $model['available_locations'][0]['price_details']['price_per_hour'];
                $model['number_of_hours'] = $model['available_locations'][0]['price_details']['no_of_hours'];

                $model['full_slab_price'] = $model['price_per_hour']*$model['number_of_hours'];
                
                $model['priority'] = $modelsByKey[$modelId]->priority;

                if($model['id']==$priorityModelId)
                    $model['priority'] = -1;
                array_push($models,$model);
            }

        }


        if(count($models)>0)
        {
            $sort = array();
            foreach($models as $k=>$v) {
                $sort['id'][$k] = $v['id'];
                $sort['priority'][$k] = $v['priority'];
                $sort['bikeAvailability'][$k] = $v['bikeAvailability'];
            }
            # sort by event_type desc and then title asc
            array_multisort($sort['bikeAvailability'], SORT_DESC, $sort['priority'], SORT_ASC, $sort['id'], SORT_ASC,  $models);
        }


        $json = json_encode($models);
        $json = addslashes($json);
        
        $areajson = json_encode($areas);
        $areajson = addslashes($areajson);

        $uniqueMakeIds = array_unique(array_map(function($elem){return $elem['make_id'];}, $models));
        $bikeMake = BikeMake::where('status',1)->whereIn('id',$uniqueMakeIds)->get();



        return view('front/booking/chooseBikeScreen',compact('models', 'json','areajson', 'bikeMake', 'sd', 'ed', 'startTime', 'endTime', 'month', 'year', 'cityId'));

    }
    
    
    public function getModelsAMHForApp($id,Request $request){
        $daysOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        
        $cityId = $id;
        $areaId = $request->area_id;
        $makeId = $request->make_ids;
        $makeIds = explode(',', $makeId);
        $modelId = $request->model_id;
        
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        
        
        /* Optional parameter checks */
        if ($startDate != "" && $endDate != "" && $startTime != "" && $endTime != "") {
            $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
            $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
            
            $dateSet = true;

        }
        elseif(isset($request->start_datetime))
        {
            $startDateTimeCarbon = Carbon::parse($request->start_datetime);
            $endDateTimeCarbon = Carbon::parse($request->end_datetime);
            
            $dateSet = true;
        }
        else
        {
            $startDateTimeCarbon = Carbon::now();
            $startDateTimeCarbon->minute = 0;
            $startDateTimeCarbon->second = 0;
            $startDateTimeCarbon->addHour(1);
            $endDateTimeCarbon = $startDateTimeCarbon->copy()->addHour(1);
            $dateSet = false;
            
            if(!(isset($request->area_id)||isset($request->make_ids)||isset($request->model_id)))
            {
                $responseObj = DB::table('model_amh_response')->where('city_id',$cityId)->first();
                if(!(!$responseObj))
                {
                    
                    $response = json_decode($responseObj->model_response);
                    return $this->respondWithSuccess($response); 
                }
            }
        }


        

       
        $startTime = $startDateTimeCarbon->format('H:i');
        $endTime = $endDateTimeCarbon->format('H:i');

        $sd = $startDateTimeCarbon->format('Y-m-d');
        $ed = $endDateTimeCarbon->format('Y-m-d');

        $month = $startDateTimeCarbon->month;
        $year = $startDateTimeCarbon->year;

        if($startDateTimeCarbon > $endDateTimeCarbon)
          return 'Sorry! Something went wrong.';



        $modelsData = AMHAvailabilityController::checkAvailabilityByCityForFrontPage($startDateTimeCarbon,$endDateTimeCarbon,$cityId);
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $result = $dispatcher->with([
            'start_datetime' => $request->start_date." ".$request->start_time,
            'end_datetime' => $request->end_date." ".$request->end_time
        ])->get('get-store-open-status');
        $areaOpenStatus = $result['result']['data'];

        $areas = Area::where('city_id', $cityId)->where('status',1)->get();
        $areaIds = $areas->lists('id')->toArray();

        $bikeModels = BikeModel::where('status',1)->get();


        $areasByKey = $areas->keyBy('id');
        $modelsByKey = $bikeModels->keyBy('id');

        $models = [];

        foreach($modelsData as $modelId => $modelData)
        {
            if(isset($request->model_id))
            {
                if($modelId!=$request->model_id)
                    continue;
            }
            $model = [];
            if(array_key_exists('available areas',$modelData)||array_key_exists('not available areas',$modelData))
            {
                $model['id'] = $modelId;
                
                $model['name'] = $modelsByKey[$modelId]->bike_model;
                $model['make_id'] = $modelsByKey[$modelId]->bike_make_id;
                $model['image_id'] = $modelsByKey[$modelId]->thumbnail_img_id;
                $model['image'] = $modelsByKey[$modelId]->getThumbnailImg();
                $model['image']['full']="http:".$model['image']['full'];
                $model['year'] = $startDateTimeCarbon->year;
                $model['month'] = $startDateTimeCarbon->month;
                $model['cityId'] = $cityId;

                $areasWithModel = [];
                $availableAreas = [];
                if(array_key_exists('available areas',$modelData))
                {
                    
                    $availableAreaIds = explode(',',$modelData['available areas']);
                    $areasWithModel = array_merge($areasWithModel,$availableAreaIds);
                    

                    foreach($availableAreaIds as $availableAreaId)
                    {
                        $availableArea = clone $areasByKey[$availableAreaId];
                        
                        //check for open hours against start and end time
                        if($dateSet)
                        {
                            if(in_array($availableAreaId,$areaOpenStatus['start_available_areas']) && in_array($availableAreaId,$areaOpenStatus['end_available_areas']))
                            {
                                $availableArea['area_warning'] = "";
                                $availableArea['store_open'] = "true";
                            }
                            else
                            {
                                $warningArray = [];

                                if(array_key_exists($availableAreaId,$areaOpenStatus['start_not_available_areas']))
                                {
                                    $startHour = $areaOpenStatus['start_not_available_areas'][$availableAreaId];
                                    if($startHour==0)
                                    {

                                        array_push($warningArray,"Store closed on ".$daysOfWeek[$areaOpenStatus['start_day']]);
                                    }
                                    else
                                    {
                                        $startHourWarningHour = $startHour==0?"12 am":($startHour==12?'12 pm':($startHour>12?($startHour-12).' pm':$startHour.' am'));
                                        array_push($warningArray,"Opens at ".$startHourWarningHour);
                                    }


                                }
                                if(array_key_exists($availableAreaId,$areaOpenStatus['end_not_available_areas']))
                                {
                                    $endHour = $areaOpenStatus['end_not_available_areas'][$availableAreaId];
                                    if($endHour==0)
                                    {

                                        array_push($warningArray,"Store closed on ".$daysOfWeek[$areaOpenStatus['end_day']]);
                                    }
                                    else
                                    {
                                        $endHourWarningHour = $endHour==0?"12 am":($endHour==12?'12 pm':($endHour>12?($endHour-12).' pm':$endHour.' am'));

                                        array_push($warningArray,"Closes at ".$endHourWarningHour);
                                    }
                                }

                                $warningStr = implode(' and ',$warningArray);

                                $availableArea['area'] .= " (".$warningStr.")"; 
                                $availableArea['area_warning'] = $warningStr;
                                $availableArea['store_open'] = "false";
                            }
                        }
                        else
                        {
                            $availableArea['area_warning'] = "";
                            $availableArea['store_open'] = "true";
                        }
                        
                        $availableArea['bikeAvailabilityStatus'] = ['bike_id'=>1];

                        $dispatcher = app('Dingo\Api\Dispatcher');
                        $price = $dispatcher->with([
                            'model_id' => $modelId,
                            'area_id' => $availableAreaId,
                            'start_date' => $sd, 
                            'end_date' => $ed,
                            'start_time' => $startTime, 
                            'end_time' => $endTime
                        ])->get('bookings/total-price');
                        
                        if($price['total_price'] != null)
                        {
                            if((in_array($availableAreaId,explode(",",env('AREAINCLUDEDFUEL'))))&&($endDateTimeCarbon->diffInHours($startDateTimeCarbon) < env('FUELINCLUDEDMAXHOURS')))
                            {
                                $priceObj = Price::where('model_id',$modelId)->where('area_id',$availableAreaId)->orderBy('id','desc')->first();
                                if(!$priceObj)
                                {
                                    $priceObj = Price::where('model_id',$modelId)->where('area_id',0)->orderBy('id','desc')->first();
                                }


                                $line2 = 'Rs. '.$priceObj->per_hour_fuel_included_weekday.'/hour. All rentals less than '.env('FUELINCLUDEDMAXHOURS').' hours include fuel and costs Rs. '.$priceObj->per_km_fuel_included.' per KM';
                                $line1 = 'Minimum billing of 24 hours on weekends';
                            }
                            else
                            {
                                $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                                $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';
                            }

                            $availableArea['price'] = [$line2,$line1];
                            $availableArea['price_details'] = $price;
                            array_push($availableAreas,$availableArea);
                        }
                        
                    }

                }
                
                
                
                $notAvailableAreas = [];
                if(array_key_exists('not available areas',$modelData))
                {
                    
                    $notAvailableAreaIds = explode(',',$modelData['not available areas']);
                    $areasWithModel = array_merge($areasWithModel,$notAvailableAreaIds);
                                        
                    foreach($notAvailableAreaIds as $notAvailableAreaId)
                    {
                        $notAvailableArea = clone $areasByKey[$notAvailableAreaId];
                        $notAvailableArea['bikeAvailabilityStatus'] = ['bike_id'=>'none'];
                        
                        $notAvailableArea['area_warning'] = "";
                        $notAvailableArea['store_open'] = "true";

                        $dispatcher = app('Dingo\Api\Dispatcher'); 
                        $price = $dispatcher->with(['model_id' => $modelId,'area_id' => $notAvailableAreaId,'start_date' => $sd, 'end_date' => $ed,'start_time' => $startTime, 'end_time' => $endTime])->get('bookings/total-price');


                        
                        if((in_array($notAvailableAreaId,explode(",",env('AREAINCLUDEDFUEL'))))&&($endDateTimeCarbon->diffInHours($startDateTimeCarbon) < env('FUELINCLUDEDMAXHOURS')))
                        {
                            $priceObj = Price::where('model_id',$modelId)->where('area_id',$notAvailableAreaId)->orderBy('id','desc')->first();
                            if(!$priceObj)
                            {
                                $priceObj = Price::where('model_id',$modelId)->where('area_id',0)->orderBy('id','desc')->first();
                            }

                            
                            $line2 = 'Rs. '.$priceObj->per_hour_fuel_included_weekday.'/hour. All rentals less than '.env('FUELINCLUDEDMAXHOURS').' hours include fuel and costs Rs. '.$priceObj->per_km_fuel_included.' per KM.';
                            $line1 = 'Minimum billing of 24 hours on weekends';
                        }
                        else
                        {
                            $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                            $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';
                        }
                        
                        
                        $notAvailableArea['price'] = [$line2,$line1];
                        $notAvailableArea['price_details'] = $price;
                        

                        array_push($notAvailableAreas,$notAvailableArea);

                    }            
                }
                

                if(count($availableAreas)>0)
                {
                    $model['bikeAvailability'] = true;
                }
                else
                {
                    $model['bikeAvailability'] = false;
                }
                
                
                
                
                
                $model['available_locations'] = array_merge($availableAreas,$notAvailableAreas);

                if(count($model['available_locations'])>0)
                {
                    $sort = array();
                    foreach($model['available_locations'] as $k=>$v) {
                        $sort['price'][$k] = $v['price_details']['price_per_day'];                              
                    }

                    if($sort['price'] != null)
                        array_multisort($sort['price'], SORT_ASC, $model['available_locations']);
                }

                $areasWithoutModel = [];        
                $areaIdsWithoutModel = array_diff($areaIds,$areasWithModel);
                foreach($areaIdsWithoutModel as $areaIdWithoutModel)
                {
                    $areaWithoutModel = clone $areasByKey[$areaIdWithoutModel];

                    $areaWithoutModel['price'] = ["Minimum billing of 10 hours on weekdays and 24 hours on weekends","Rs /hour and /day"];

                    array_push($areasWithoutModel, $areaWithoutModel);
                }

                $model['not_available_locations'] = $areasWithoutModel;
                
                if(count($model['available_locations'])>0)
                {
                    $model['minimum_billing_message'] = $model['available_locations'][0]['price_details']['minimum_billing_message'];
                    $model['minimum_hours'] = $model['available_locations'][0]['price_details']['minimum_hours'];
                    $model['totalPrice'] = $model['available_locations'][0]['price_details']['total_price'];
                    $model['effectivePrice'] = $model['available_locations'][0]['price_details']['effective_price'];
                    $model['number_of_hours'] = $model['available_locations'][0]['price_details']['no_of_hours'];
                    $model['price_per_hour'] = $model['available_locations'][0]['price_details']['price_per_hour'];
                }
                else
                {
                    $model['minimum_billing_message'] = "";
                    $model['minimum_hours'] = 0;
                    $model['totalPrice'] = 0;
                    $model['effectivePrice'] = 0;
                    $model['number_of_hours'] = 0;
                    $model['price_per_hour'] = 0;
                }
                
                $model['full_slab_price'] = $model['price_per_hour']*$model['number_of_hours'];


                $model['priority'] = $modelsByKey[$modelId]->priority;
                
                
                array_push($models,$model);
                
                
            }

        }


        if(count($models)>0)
        {
            $sort = array();
            foreach($models as $k=>$v) {
                $sort['id'][$k] = $v['id'];
                $sort['priority'][$k] = $v['priority'];
                $sort['bikeAvailability'][$k] = $v['bikeAvailability'];
            }
            # sort by event_type desc and then title asc
            array_multisort($sort['bikeAvailability'], SORT_DESC, $sort['priority'], SORT_ASC, $sort['id'], SORT_ASC,  $models);
        }

        if(!$dateSet)
        {
            foreach($models as $model)
            {
                foreach($model['available_locations'] as $availableArea)
                {
                    $availableArea['bikeAvailabilityStatus'] = ['bike_id'=>"all"];
                }
            }
        }
        
        
        
        if(isset($areaId)||isset($makeId))
        {
        
            $firstSet = [];
            $secondSet = [];

            foreach($models as $model)
            {
                
                $modelInArea = 0;

                foreach($model['available_locations'] as $key=>$availableArea)
                {
                    if(isset($areaId))
                    {
                        if($availableArea['id']==$areaId)
                        {
                            $modelInArea = 1;
                            if(count($model['available_locations'])>1)
                            {
                                unset($model['available_locations'][$key]);
                                array_unshift($model['available_locations'],$availableArea);
                            }
                            array_push($firstSet,$model);
                        }
                    }
                }
                if($modelInArea == 0)
                {
                    array_push($secondSet,$model);
                }

                


            }


            if(isset($makeId))
            {
                foreach($firstSet as $key=>$model)
                {
                    if(in_array($model['make_id'],$makeIds))
                    {
                        if(count($firstSet)>1)
                        {
                            unset($firstSet[$key]);
                            array_unshift($firstSet,$model);
                        }                       
                    }

                }
                foreach($secondSet as $key=>$model)
                {
                    if(in_array($model['make_id'],$makeIds))
                    {
                       if(count($secondSet)>1)
                        {
                            unset($secondSet[$key]);
                            array_unshift($secondSet,$model);
                        }                        
                    }
                }

            }

            $models = array_merge($firstSet,$secondSet);
        }
        
        if(isset($request->model_id))
        {
            if(count($models)>0)
            {
                $model = $models[0];
                $bikeModel = BikeModel::where('id',$model['id'])->first();
                $model['description'] = $bikeModel->description;
                $model['logo_id'] = $bikeModel->getModelDetailsById()['logo_id'];
                $model['logo'] =  $bikeModel->getModelDetailsById()['logo'];
                $model['not_available_locations']=array();
                
            }
            else
            {
                $this->respondWithSuccess(array());   
            }
            
            return $this->respondWithSuccess($model);
        }

        return $this->respondWithSuccess($models);

    }
    
    public static function storeHourlyResponses()
    {
        $cities = City::where('status',1)->get();


        foreach($cities as $city)
        {
            $models = Self::getModelsAMHFunctionForWithoutDates($city->id);
            
            DB::table('model_amh_response')->where('city_id',$city->id)->update([
                'model_response'=>json_encode($models)
            ]);
        }
        return "done";
    }
    
    
    
    public static function getModelsAMHFunctionForWithoutDates($cityId){

        $daysOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];        
        
        $startDateTimeCarbon = Carbon::now();
        $startDateTimeCarbon->minute = 0;
        $startDateTimeCarbon->second = 0;
        $startDateTimeCarbon->addHour(1);
        $endDateTimeCarbon = $startDateTimeCarbon->copy()->addHour(1);
        
       
        $startTime = $startDateTimeCarbon->format('H:i');
        $endTime = $endDateTimeCarbon->format('H:i');

        $sd = $startDateTimeCarbon->format('Y-m-d');
        $ed = $endDateTimeCarbon->format('Y-m-d');

        $month = $startDateTimeCarbon->month;
        $year = $startDateTimeCarbon->year;

        if($startDateTimeCarbon > $endDateTimeCarbon)
          return 'Sorry! Something went wrong.';



        $modelsData = AMHAvailabilityController::checkAvailabilityByCityForFrontPage($startDateTimeCarbon,$endDateTimeCarbon,$cityId);
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $result = $dispatcher->with([
            'start_datetime' => $sd." ".$startTime,
            'end_datetime' => $ed." ".$endTime
        ])->get('get-store-open-status');
        $areaOpenStatus = $result['result']['data'];

        $areas = Area::where('city_id', $cityId)->where('status',1)->get();
        $areaIds = $areas->lists('id')->toArray();

        $bikeModels = BikeModel::where('status',1)->get();


        $areasByKey = $areas->keyBy('id');
        $modelsByKey = $bikeModels->keyBy('id');

        $models = [];

        foreach($modelsData as $modelId => $modelData)
        {
            if(isset($request->model_id))
            {
                if($modelId!=$request->model_id)
                    continue;
            }
            $model = [];
            if(array_key_exists('available areas',$modelData)||array_key_exists('not available areas',$modelData))
            {
                $model['id'] = $modelId;
                
                $model['name'] = $modelsByKey[$modelId]->bike_model;
                $model['make_id'] = $modelsByKey[$modelId]->bike_make_id;
                $model['image_id'] = $modelsByKey[$modelId]->thumbnail_img_id;
                $model['image'] = $modelsByKey[$modelId]->getThumbnailImg();
                $model['image']['full']="http:".$model['image']['full'];
                $model['year'] = $startDateTimeCarbon->year;
                $model['month'] = $startDateTimeCarbon->month;
                $model['cityId'] = $cityId;

                $areasWithModel = [];
                $availableAreas = [];
                if(array_key_exists('available areas',$modelData))
                {
                    
                    $availableAreaIds = explode(',',$modelData['available areas']);
                    $areasWithModel = array_merge($areasWithModel,$availableAreaIds);
                    

                    foreach($availableAreaIds as $availableAreaId)
                    {
                        $availableArea = clone $areasByKey[$availableAreaId];
                        
                        //check for open hours against start and end time
                        
                        $availableArea['area_warning'] = "";
                        $availableArea['store_open'] = "true";

                        
                        $availableArea['bikeAvailabilityStatus'] = ['bike_id'=>1];

                        $dispatcher = app('Dingo\Api\Dispatcher');
                        $price = $dispatcher->with([
                            'model_id' => $modelId,
                            'area_id' => $availableAreaId,
                            'start_date' => $sd, 
                            'end_date' => $ed,
                            'start_time' => $startTime, 
                            'end_time' => $endTime
                        ])->get('bookings/total-price');
                        
                        if($price['total_price'] != null)
                        {
                            if((in_array($availableAreaId,explode(",",env('AREAINCLUDEDFUEL'))))&&($endDateTimeCarbon->diffInHours($startDateTimeCarbon) < env('FUELINCLUDEDMAXHOURS')))
                            {
                                $priceObj = Price::where('model_id',$modelId)->where('area_id',$availableAreaId)->orderBy('id','desc')->first();
                                if(!$priceObj)
                                {
                                    $priceObj = Price::where('model_id',$modelId)->where('area_id',0)->orderBy('id','desc')->first();
                                }


                                $line2 = 'Rs. '.$priceObj->per_hour_fuel_included_weekday.'/hour. All rentals less than '.env('FUELINCLUDEDMAXHOURS').' hours include fuel and costs Rs. '.$priceObj->per_km_fuel_included.' per KM';
                                $line1 = 'Minimum billing of 24 hours on weekends';
                            }
                            else
                            {
                                $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                                $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';
                            }

                            $availableArea['price'] = [$line2,$line1];
                            $availableArea['price_details'] = $price;
                            array_push($availableAreas,$availableArea);
                        }
                        
                    }

                }
                
                
                
                $notAvailableAreas = [];
                if(array_key_exists('not available areas',$modelData))
                {
                    
                    $notAvailableAreaIds = explode(',',$modelData['not available areas']);
                    $areasWithModel = array_merge($areasWithModel,$notAvailableAreaIds);
                                        
                    foreach($notAvailableAreaIds as $notAvailableAreaId)
                    {
                        $notAvailableArea = clone $areasByKey[$notAvailableAreaId];
                        $notAvailableArea['bikeAvailabilityStatus'] = ['bike_id'=>'none'];
                        
                        $notAvailableArea['area_warning'] = "";
                        $notAvailableArea['store_open'] = "true";

                        $dispatcher = app('Dingo\Api\Dispatcher'); 
                        $price = $dispatcher->with(['model_id' => $modelId,'area_id' => $notAvailableAreaId,'start_date' => $sd, 'end_date' => $ed,'start_time' => $startTime, 'end_time' => $endTime])->get('bookings/total-price');


                        
                        if((in_array($notAvailableAreaId,explode(",",env('AREAINCLUDEDFUEL'))))&&($endDateTimeCarbon->diffInHours($startDateTimeCarbon) < env('FUELINCLUDEDMAXHOURS')))
                        {
                            $priceObj = Price::where('model_id',$modelId)->where('area_id',$notAvailableAreaId)->orderBy('id','desc')->first();
                            if(!$priceObj)
                            {
                                $priceObj = Price::where('model_id',$modelId)->where('area_id',0)->orderBy('id','desc')->first();
                            }

                            
                            $line2 = 'Rs. '.$priceObj->per_hour_fuel_included_weekday.'/hour. All rentals less than '.env('FUELINCLUDEDMAXHOURS').' hours include fuel and costs Rs. '.$priceObj->per_km_fuel_included.' per KM.';
                            $line1 = 'Minimum billing of 24 hours on weekends';
                        }
                        else
                        {
                            $line1 = 'Rs '.$price['hour_weekday'].'/hour and '.$price['weekday_price'].'/day';
                            $line2 = 'Minimum billing of '.$price['minimum_hours'].' hours on weekdays and 24 hours on weekends';
                        }
                        
                        
                        $notAvailableArea['price'] = [$line2,$line1];
                        $notAvailableArea['price_details'] = $price;
                        

                        array_push($notAvailableAreas,$notAvailableArea);

                    }            
                }
                

                if(count($availableAreas)>0)
                {
                    $model['bikeAvailability'] = true;
                }
                else
                {
                    $model['bikeAvailability'] = false;
                }
                
                
                
                
                
                $model['available_locations'] = array_merge($availableAreas,$notAvailableAreas);

                $sort = array();
                foreach($model['available_locations'] as $k=>$v) {
                    $sort['price'][$k] = $v['price_details']['price_per_day'];                              
                }
                
                if(array_key_exists("price",$sort))
                    array_multisort($sort['price'], SORT_ASC, $model['available_locations']);
   

                $areasWithoutModel = [];        
                $areaIdsWithoutModel = array_diff($areaIds,$areasWithModel);
                foreach($areaIdsWithoutModel as $areaIdWithoutModel)
                {
                    $areaWithoutModel = clone $areasByKey[$areaIdWithoutModel];

                    $areaWithoutModel['price'] = ["Minimum billing of 10 hours on weekdays and 24 hours on weekends","Rs /hour and /day"];

                    array_push($areasWithoutModel, $areaWithoutModel);
                }

                $model['not_available_locations'] = $areasWithoutModel;

                if(count($model['available_locations'])>0)
                {
                    $model['minimum_billing_message'] = $model['available_locations'][0]['price_details']['minimum_billing_message'];
                    $model['minimum_hours'] = $model['available_locations'][0]['price_details']['minimum_hours'];
                    $model['totalPrice'] = $model['available_locations'][0]['price_details']['total_price'];
                    $model['effectivePrice'] = $model['available_locations'][0]['price_details']['effective_price'];
                    $model['number_of_hours'] = $model['available_locations'][0]['price_details']['no_of_hours'];
                    $model['price_per_hour'] = $model['available_locations'][0]['price_details']['price_per_hour'];
                }
                else
                {
                    $model['minimum_billing_message'] = "";
                    $model['minimum_hours'] = 0;
                    $model['totalPrice'] = 0;
                    $model['effectivePrice'] = 0;
                    $model['number_of_hours'] = 0;
                    $model['price_per_hour'] = 0;
                }
                
                $model['full_slab_price'] = $model['price_per_hour']*$model['number_of_hours'];


                $model['priority'] = $modelsByKey[$modelId]->priority;
                
                
                array_push($models,$model);
                
                
            }

        }


        if(count($models)>0)
        {
            $sort = array();
            foreach($models as $k=>$v) {
                $sort['id'][$k] = $v['id'];
                $sort['priority'][$k] = $v['priority'];
                $sort['bikeAvailability'][$k] = $v['bikeAvailability'];
            }
            # sort by event_type desc and then title asc
            array_multisort($sort['bikeAvailability'], SORT_DESC, $sort['priority'], SORT_ASC, $sort['id'], SORT_ASC,  $models);
        }

        foreach($models as $model)
        {
            foreach($model['available_locations'] as $availableArea)
            {
                $availableArea['bikeAvailabilityStatus'] = ['bike_id'=>"all"];
            }
        }
        

        return $models;

    }
    
    
}
