<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\ApiCategory;
use App\Models\ApiCall;
use App\Models\ApiLanguage;
use App\Models\ApiArgument;
use App\Models\ApiTable;
use App\Models\ApiTableCell;
use App\Models\ApiMessage;
use App\Models\ApiResponse;
use App\Models\ApiCodeSample;
use App\Models\User;

class ApiDocController extends AppController
{
    public function indexApiDoc(){
        
        $languages = ApiLanguage::all();
        $data_languages_string = "";
        
        foreach($languages as $language)
        {
            $data_languages_string .= "&quot;".$language->language."&quot;,";
        }
        $data_languages_string = substr($data_languages_string,0,-1);
        
        $apiCategories=[];
        
        $categories = ApiCategory::get()->keyBy('id');
        
        $calls = ApiCall::where('status',1)->orderBy('priority','asc')->get();
        
        $callsByCategory = clone $calls;
        if(!(!$callsByCategory))
            $callsByCategory = $callsByCategory->groupBy('category');
        
        $tables = ApiTable::get();
        if(!(!$tables))
            $tablesByCall = $tables->groupBy('call_id');
        
        $tableCells = ApiTableCell::orderBy('priority','asc')->get();
        if(!(!$tableCells))
            $tableCellsByTable = $tableCells->groupBy('table_id');
        
        $tablesByCallArray = $tablesByCall->toArray();
        $tableCellsByTableArray = $tableCellsByTable->toArray();
        
        $arguments = ApiArgument::orderBy('priority','asc')->get();
        if(!(!$arguments))
            $argumentsByCall = $arguments->groupBy('call_id');
        $argumentsByCallArray = $argumentsByCall->toArray();
        
        $responses = ApiResponse::get();
        if(!(!$responses))
            $responsesByCall = $responses->groupBy('call_id');
        $responsesByCallArray = $responsesByCall->toArray();

        $messages = ApiMessage::get();
        if(!(!$messages))
            $messagesByCall = $messages->groupBy('call_id');
        $messagesByCallArray = $messagesByCall->toArray();

        
        foreach($callsByCategory as $category => $callsByCategoryEl)
        {
            $apiCategory["category"] = $categories[$category]->category;
            $apiCategory['category_id'] = str_replace(" ","_",$categories[$category]->category);
            $apiCategory["apis"] = [];
            
            foreach($callsByCategoryEl as $apiCall)
            {
                
                $apiCallDetails = Self::prepareCall($apiCall,$argumentsByCallArray,$messagesByCallArray,$responsesByCallArray,$tablesByCallArray,$tableCellsByTableArray,$argumentsByCall,$messagesByCall,$responsesByCall,$tablesByCall,$tableCellsByTable);
                
                array_push($apiCategory["apis"],$apiCallDetails);
                
            }
            
            array_push($apiCategories,$apiCategory);

        }
                
        return view('apiDocs.index',compact('data_languages_string','languages','apiCategories')); 
    	
    }
    
    
    
    public function indexApiCall()
    {
        $apiList = ApiCall::where('status',1)->orderBy('priority','asc')->get();
        $apiListByCategory = $apiList->groupBy('category');
        $apiListByCategoryArray = $apiListByCategory->toArray();
        $apiCategories = ApiCategory::get();
        foreach($apiCategories as $apiCategory)
        {
            if(array_key_exists($apiCategory->id,$apiListByCategoryArray))
            {
                $apiCategory['apis'] = $apiListByCategory[$apiCategory->id];
            }
            else
            {
                $apiCategory['apis'] = [];
            }
             
        }
        return view('admin.indexApi.index',compact('apiList','apiCategories')); 
    }
    
    
    
    public function indexApiCategory(Request $request)
    {
        $pageTitle = "API Category";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Category"],
                                            ["width"=>250,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $apiCategoryObjects = ApiCategory::all();
        
        $objectTableArray['body'] = [];
        
        foreach($apiCategoryObjects as $apiCategoryObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$apiCategoryObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$apiCategoryObject->category,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$apiCategoryObject->description,"pre"=>"true"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_apiCategory_btn",
                                                            "id"=>"edit_apiCategory_id",
                                                            "data_id"=>$apiCategoryObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_apiCategory_btn",
                                                            "id"=>"delete_apiCategory_id",
                                                            "data_id"=>$apiCategoryObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_apiCategory_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add API Category";
        $addObject['add_url'] = "/admin/api-category/add";
        $addObject['add_modal_form_items'] = [];
        
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Category Title","input_type"=>"text","name"=>"category_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_apiCategory_modal";
        $editObject['edit_btn_class'] = "edit_apiCategory_btn";
        $editObject['modal_title'] = "Edit ApiCategory";
        $editObject['edit_url'] = "/admin/api-category/edit";
        $editObject['ajax_url'] = "/admin/api-category/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Category Title","input_type"=>"text","name"=>"category_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_apiCategory_modal";
        $deleteObject['delete_btn_class'] = "delete_apiCategory_btn";
        $deleteObject['modal_title'] = "Delete API Category";
        $deleteObject['delete_url'] = "/admin/api-category/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this API Category";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addApiCategory(Request $request)
    {
        
        
        
        ApiCategory::create([
            'created_by'=>Auth::user()->id,
            'category'=>$request->category_add,
            'description'=>$request->description_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsApiCategory(Request $request)
    {
        $apiCategory = ApiCategory::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"category_edit","value"=>$apiCategory->category],
                ["type"=>"textarea","name"=>"description_edit","value"=>$apiCategory->description]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editApiCategory($id, Request $request)
    {
        
        ApiCategory::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'category'=>$request->category_edit,
            'description'=>$request->description_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteApiCategory($id, Request $request)
    {
        $apiCategory = ApiCategory::where('id',$id)->first();
        
        
        
        ApiCategory::where('id',$id)->delete();
        
        return redirect()->back();
    }
    
    
    
    
    public function indexApiLanguage(Request $request)
    {
        $pageTitle = "API Language";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>140,"title"=>"Language"],
                                            ["width"=>250,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $apiLanguageObjects = ApiLanguage::all();
        
        $objectTableArray['body'] = [];
        
        foreach($apiLanguageObjects as $apiLanguageObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$apiLanguageObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$apiLanguageObject->language,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$apiLanguageObject->description,"pre"=>"true"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_apiLanguage_btn",
                                                            "id"=>"edit_apiLanguage_id",
                                                            "data_id"=>$apiLanguageObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_apiLanguage_btn",
                                                            "id"=>"delete_apiLanguage_id",
                                                            "data_id"=>$apiLanguageObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_apiLanguage_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add API Language";
        $addObject['add_url'] = "/admin/api-language/add";
        $addObject['add_modal_form_items'] = [];
        
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Language Title","input_type"=>"text","name"=>"language_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_apiLanguage_modal";
        $editObject['edit_btn_class'] = "edit_apiLanguage_btn";
        $editObject['modal_title'] = "Edit ApiLanguage";
        $editObject['edit_url'] = "/admin/api-language/edit";
        $editObject['ajax_url'] = "/admin/api-language/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Language Title","input_type"=>"text","name"=>"language_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_apiLanguage_modal";
        $deleteObject['delete_btn_class'] = "delete_apiLanguage_btn";
        $deleteObject['modal_title'] = "Delete API Language";
        $deleteObject['delete_url'] = "/admin/api-language/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this API Language";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addApiLanguage(Request $request)
    {
        
        
        
        ApiLanguage::create([
            'created_by'=>Auth::user()->id,
            'language'=>$request->language_add,
            'description'=>$request->description_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsApiLanguage(Request $request)
    {
        $apiLanguage = ApiLanguage::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"language_edit","value"=>$apiLanguage->language],
                ["type"=>"textarea","name"=>"description_edit","value"=>$apiLanguage->description]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editApiLanguage($id, Request $request)
    {
        
        ApiLanguage::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'language'=>$request->language_edit,
            'description'=>$request->description_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteApiLanguage($id, Request $request)
    {
        $apiLanguage = ApiLanguage::where('id',$id)->first();
        
        
        
        ApiLanguage::where('id',$id)->delete();
        
        return redirect()->back();
    }
    
    public function addApiCallIndex(Request $request)
    {
        
        $categories = ApiCategory::where('status',1)->get();
        
        return view('admin.addApi.index',compact('categories'));
    }
    
    public function addApiCall(Request $request)
    {
        
        $createdByUser = UserController::getLoggedInUser();
        
        $callId = DB::table('api_call')->insertGetId([
            'title'=>$request->call_name,
            'description'=>$request->call_description,
            'url'=>$request->call_url,
            'method'=>$request->call_type,
            'created_by'=>$createdByUser->id,
            'category'=>$request->call_category
        ]);
        
        
        Self::addCallDetails($request, $callId, $createdByUser);
        
        return redirect('/admin/api-call')->withErrors(["success"=>"API Call has been added successfully"]);
    }
    
    
    public function prepareCall($apiCall,$argumentsByCallArray,$messagesByCallArray,$responsesByCallArray,$tablesByCallArray,$tableCellsByTableArray,$argumentsByCall,$messagesByCall,$responsesByCall,$tablesByCall,$tableCellsByTable)
    {
        $apiCallDetails['call_title'] = $apiCall->title;
        $apiCallDetails['call_id'] = str_replace(" ","_",$apiCall->title);
        $apiCallDetails['call_method'] = $apiCall->method;
        $apiCallDetails['call_category'] = $apiCall->category;
        $apiCallDetails['call_url'] = $apiCall->url;
        $apiCallDetails['call_description'] = $apiCall->description;
        $apiCallDetails['call_description'] = $apiCall->description;

        

        if(array_key_exists($apiCall->id,$argumentsByCallArray))
        {

            $argumentsById = $argumentsByCall[$apiCall->id]->keyBy('id');
                        
            $argumentsByParentID = $argumentsByCall[$apiCall->id]->groupBy('parent_argument');
            
            $argumentsInOrderByKey = [];
            
            $argumentsByIdInOrder = collect([]);
            
            $argumentsMissedOut = [];
            
            
            foreach($argumentsByParentID as $parentId => $argumentArray)
            {
                if($argumentArray[0]->id == $parentId)
                {
                    $argumentsInOrderByKey[$parentId] = $parentId;
                }
                
                $argumentArray = $argumentArray->reverse();
                foreach($argumentArray as $argument)
                {
                    if($argument->id != $parentId)
                    {
                        $result = Self::array_insert_after($parentId, $argumentsInOrderByKey, $argument->id, $argument->id);
                        if(!$result)
                        {
                            $argumentsMissedOut[$argument->id] = $parentId;
                        }
                        else
                        {
                            $argumentsInOrderByKey = $result;
                        }
                    }
                }
                
            }
            
            while(count($argumentsMissedOut) > 0)
            {
                foreach($argumentsMissedOut as $id=>$parentId)
                {
                    $result = Self::array_insert_after($parentId, $argumentsInOrderByKey, $id, $id);
                    if(!$result)
                    {
                        $argumentsMissedOut[$argument->id] = $parentId;
                    }
                    else
                    {
                        unset($argumentsMissedOut[$id]);
                        $argumentsInOrderByKey = $result;
                    }
                }
            }
            
            foreach($argumentsInOrderByKey as $id => $id2)
            {
                $argumentsByIdInOrder = $argumentsByIdInOrder->push($argumentsById[$id]);
            }
            
            $argumentsByIdInOrder = $argumentsByIdInOrder->keyBy('id');
            
            
            
            $apiCallDetails['arguments'] = [];


            foreach($argumentsByIdInOrder as $id=>$argument)
            {
                if($argument->parent_argument == $argument->id)
                    $argumentEl['child_argument'] = false;
                else
                    $argumentEl['child_argument'] = true;

                $offset = 0;
                $parent_argument = $argument->parent_argument;
                $argument_id = $argument->id;


                while($parent_argument != $argument_id)
                {
                    $argument_id = $argumentsByIdInOrder[$parent_argument]->id;
                    $parent_argument = $argumentsByIdInOrder[$parent_argument]->parent_argument;

                    $offset += 20;
                }


                $argumentEl['child_argument_offset'] = $offset;

                $argumentEl['argument'] = $argument->argument;
                $argumentEl['required_or_optional'] = $argument->required_or_optional;
                $argumentEl['type'] = $argument->type;
                $argumentEl['description'] = $argument->description;
                $argumentEl['parent_argument'] = $argument->parent_argument;
                $argumentEl['id'] = $argument->id;

                array_push($apiCallDetails['arguments'],$argumentEl);

            }

        }
        else
        {

            $apiCallDetails['arguments'] = [];
        }


        if(array_key_exists($apiCall->id,$messagesByCallArray))
        {
            $apiCallDetails['messages'] = [];

            foreach($messagesByCallArray[$apiCall->id] as $message)
            {
                array_push($apiCallDetails['messages'],$message);
            }
        }
        else
        {
            $apiCallDetails['messages'] = [];
        }

        if(array_key_exists($apiCall->id,$responsesByCallArray))
        {
            $apiCallDetails['responses'] = [];

            foreach($responsesByCall[$apiCall->id] as $response)
            {
                array_push($apiCallDetails['responses'],$response);
            }
        }
        else
        {
            $apiCallDetails['responses'] = [];
        }

        

        if(array_key_exists($apiCall->id,$tablesByCallArray))
        {
            $apiCallDetails['tables'] = [];

            foreach($tablesByCall[$apiCall->id] as $table)
            {
                $table['title_id'] = str_replace(" ","_",$table->title);

                if(array_key_exists($table->id,$tableCellsByTableArray))
                {
                    $tableCells = [];
                    foreach($tableCellsByTable[$table->id] as $tableCell)
                    {
                        array_push($tableCells,$tableCell);
                    }

                }
                else
                {
                    $tableCells = [];
                }

                $table['table_cells'] = $tableCells;

                array_push($apiCallDetails['tables'],$table);
            }
        }
        else
        {
            $apiCallDetails['tables'] = [];
        }
        
        
        return $apiCallDetails;
    }
    
    public function deleteApiCall($id, Request $request)
    {
        $tableIds = ApiTable::where('call_id',$id)->get()->pluck('id');
        ApiArgument::where('call_id',$id)->delete();
        ApiTable::where('call_id',$id)->delete();
        ApiTableCell::whereIn('table_id',$tableIds)->delete();
        ApiMessage::where('call_id',$id)->delete();
        ApiResponse::where('call_id',$id)->delete();
        ApiCodeSample::where('call_id',$id)->delete();
        ApiCall::where('id',$id)->delete();
        
        return redirect('/admin/api-call')->withErrors(["success"=>"API Call has been deleted successfully"]);
        
    }
    
    function array_insert_after($key, array &$array, $new_key, $new_value) {
      if (array_key_exists($key, $array)) {
        $new = array();
        foreach ($array as $k => $value) {
          $new[$k] = $value;
          if ($k === $key) {
            $new[$new_key] = $new_value;
          }
        }
        return $new;
      }
      return FALSE;
    }
    
    function array_insert_after_value($value, array &$array, $new_key, $new_value) {
      if (array_key_exists($key, $array)) {
        $new = array();
        foreach ($array as $k => $value) {
          $new[$k] = $value;
          if ($k === $key) {
            $new[$new_key] = $new_value;
          }
        }
        return $new;
      }
      return FALSE;
    }
    
    
    public function editApiCallIndex($id,Request $request)
    {
        $apiCategories=[];
        $categories = ApiCategory::get();
        
        $calls = ApiCall::where('status',1)->where('id',$id)->get();
        
        $callsByCategory = clone $calls;
        if(!(!$callsByCategory))
            $callsByCategory = $callsByCategory->groupBy('category');
        
        $tables = ApiTable::where('call_id',$id)->get();
        if(!(!$tables))
            $tablesByCall = $tables->groupBy('call_id');
        
        $tableCells = ApiTableCell::orderBy('priority','asc')->get();
        if(!(!$tableCells))
            $tableCellsByTable = $tableCells->groupBy('table_id');
        
        $tablesByCallArray = $tablesByCall->toArray();
        $tableCellsByTableArray = $tableCellsByTable->toArray();
        
        $arguments = ApiArgument::where('call_id',$id)->orderBy('priority','asc')->get();
        if(!(!$arguments))
            $argumentsByCall = $arguments->groupBy('call_id');
        $argumentsByCallArray = $argumentsByCall->toArray();
        
        $responses = ApiResponse::where('call_id',$id)->get();
        if(!(!$responses))
            $responsesByCall = $responses->groupBy('call_id');
        $responsesByCallArray = $responsesByCall->toArray();

        $messages = ApiMessage::where('call_id',$id)->get();
        if(!(!$messages))
            $messagesByCall = $messages->groupBy('call_id');
        $messagesByCallArray = $messagesByCall->toArray();

        
        $apiCall = ApiCall::where('id',$id)->first();
        
        $apiCallDetails = Self::prepareCall($apiCall,$argumentsByCallArray,$messagesByCallArray,$responsesByCallArray,$tablesByCallArray,$tableCellsByTableArray,$argumentsByCall,$messagesByCall,$responsesByCall,$tablesByCall,$tableCellsByTable);
        
        $apiCallDetails['category']=$apiCall->category;
        $apiCallDetails['id'] = $apiCall->id;
        
        $id = $apiCall->id;
        
        $json = json_encode($apiCallDetails);
        $json = addslashes($json);
        
        return view('admin.editApi.index',compact('categories','apiCallDetails','json','id')); 
    }
    
    public static function addCallDetails(Request $request, $callId, $createdByUser)
    {
        $parentArguments = [];
        
        $numberOfArguments = count($request->argument_name);
        
        if($numberOfArguments)
        {
            foreach($request->argument_name as $key=>$argument)
            {
                if($key == $request->parent_argument[$key])
                {
                    $argumentObjId = DB::table('api_argument')->insertGetId([
                        'call_id'=>$callId,
                        'parent_argument'=>0,
                        'argument'=>$request->argument_name[$key],
                        'required_or_optional'=>$request->argument_required_or_optional[$key],
                        'description'=>$request->argument_description[$key],
                        'type'=>$request->argument_type[$key],
                        'priority'=>($key+1),
                        'created_by'=>$createdByUser->id
                    ]);

                    ApiArgument::where('id',$argumentObjId)->update(['parent_argument'=>$argumentObjId]);
                    $parentArguments[$key] = $argumentObjId;
                    

                }            
            }

            while(count($parentArguments)<$numberOfArguments)
            {
                foreach($request->argument_name as $key=>$argument)
                {
                    if(($key != $request->parent_argument[$key]) && (array_key_exists($request->parent_argument[$key],$parentArguments)))
                    {
                        $argumentObj = DB::table('api_argument')->insertGetId([
                            'call_id'=>$callId,
                            'parent_argument'=>$parentArguments[$request->parent_argument[$key]],
                            'argument'=>$request->argument_name[$key],
                            'required_or_optional'=>$request->argument_required_or_optional[$key],
                            'description'=>$request->argument_description[$key],
                            'type'=>$request->argument_type[$key],
                            'priority'=>($key+1),
                            'created_by'=>$createdByUser->id
                        ]);

                        $parentArguments[$key] = $argumentObj;

                    }            
                }
            }
            
        }
        
        if(count($request->message_type)>0)
        {
            foreach($request->message_type as $index=>$type)
            {
                ApiMessage::create([
                    'call_id'=>$callId,
                    'type'=>$request->message_type[$index],
                    'message'=>$request->message[$index],
                    'created_by'=>$createdByUser->id
                ]);
            }
        }
        
        if(count($request->table_title)>0)
        {
            foreach($request->table_left_title as $index=>$leftTitle)
            {
                $apiTable = ApiTable::create([
                    'call_id'=>$callId,
                    'title'=>$request->table_title[$index],
                    'description'=>$request->table_description[$index],
                    'left_title'=>$request->table_left_title[$index],
                    'right_title'=>$request->table_right_title[$index],
                    'created_by'=>$createdByUser->id                    
                ]);
                
                foreach($request->table_left_cell[$index] as $key=>$leftCell)
                {
                    ApiTableCell::create([
                        'table_id' => $apiTable->id,
                        'title'=> $request->table_left_cell[$index][$key],
                        'description'=> $request->table_right_cell[$index][$key],
                        'created_by'=>$createdByUser->id                   
                    ]);
                }
            }
        }
        
        if(count($request->response_intro)>0)
        {
            foreach($request->response_intro as $index=>$response_intro)
            {                
                ApiResponse::create([
                    'call_id'=>$callId,
                    'description'=>$request->response_intro[$index],
                    'response'=>$request->response[$index],
                    'created_by'=>$createdByUser->id
                ]);
            }
        }
        
        return "done";
    }
    
    
    public function editApiCall($id, Request $request)
    {
        $createdByUser = UserController::getLoggedInUser();
        
        $tableIds = ApiTable::where('call_id',$id)->get()->pluck('id');
        ApiArgument::where('call_id',$id)->delete();
        ApiTable::where('call_id',$id)->delete();
        ApiTableCell::whereIn('table_id',$tableIds)->delete();
        ApiMessage::where('call_id',$id)->delete();
        ApiResponse::where('call_id',$id)->delete();
        ApiCodeSample::where('call_id',$id)->delete();
        
        ApiCall::where('id',$id)->update([
                'title'=>$request->call_name,
                'description'=>$request->call_description,
                'url'=>$request->call_url,
                'method'=>$request->call_type,
                'created_by'=>$createdByUser->id,
                'category'=>$request->call_category
        ]);
        
        
        Self::addCallDetails($request, $id, $createdByUser);
        
        return redirect('/admin/api-call')->withErrors(["success"=>"API Call has been edited successfully"]);
    }
    
    
    
    public function rearrangeApiCall(Request $request)
    {
        foreach($request->api_category as $category => $apiOrderArray)
        {
            $i=1;
            foreach($apiOrderArray as $api)
            {
                ApiCall::where('id',$api)->update(['priority'=>$i]);
                $i++;
            }
        }
        
        return redirect()->back();
    }
    
    public function rearrangeApiElements(Request $request)
    {
        $i=1;
    
        if(isset($request->argument))   
        {
            foreach($request->argument as $argumentId)
            {            
                ApiArgument::where('id',$argumentId)->update(['priority'=>(array_search($argumentId,$request->api_arg)+1)]);
                if(isset($request->argumentChild[$argumentId]))
                {
                    $reverseChildArray = array_reverse($request->argumentChild[$argumentId],true);
                    foreach($reverseChildArray as $argumentChildId)
                    {
                        ApiArgument::where('id',$argumentChildId)->update(['priority'=>(array_search($argumentChildId,$request->api_arg)+1)]);

                        if(isset($request->argument2Child[$argumentChildId]))
                        {
                            $reverseChild2Array = array_reverse($request->argument2Child[$argumentChildId]);
                            foreach($reverseChild2Array as $argumentChild2Id)
                            {
                                ApiArgument::where('id',$argumentChild2Id)->update(['priority'=>(array_search($argumentChild2Id,$request->api_arg)+1)]);
                            }                        
                        }
                    }
                }
            }
        }
        
        if(isset($request->cell))
        {
            foreach($request->cell as $tabelId => $cellArray)
            {
                $i = 1;
                foreach($cellArray as $cellId)
                {
                    ApiTableCell::where('id',$cellId)->update(['priority'=>$i]);
                    $i++;
                }
            }
        }
        
        return redirect('/admin/api-call');
    }
    
    
    
    
    
    
    
}
