<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use App\Models\User;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Availability;
use App\Models\ServiceBlock;
use App\Models\City;
use App\Models\Helmet;
use App\Models\Jacket;
use App\Models\AggregatedBikes;
use App\Models\Aggregator;
use App\Models\OwnerBlocks;
use App\Models\BikeNumbers;
use DB;
use Mail;
use App\Transformers\BookingTransformer;
use Carbon\Carbon;
use Auth;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\BikeAvailabilityController;
use App\Jobs\ModelHourArea;

class AggregationController extends AppController
{
    public function aggregatorIndex(){

        $aggregatorList = Aggregator::all();
        return view('admin.addAggregator.index',compact('aggregatorList'));

    }

    public function addAggregator(Request $request){

        $id = $request->id;
        $name = $request->name;
        $phone_number = $request->phone_number;
        $bank_account_number = $request->bank_account_number;
        $ifsc_code = $request->ifsc_code;
        $bank_account_notes = $request->bank_account_notes;
        $address = $request->address;
        $pan = $request->pan;
        $email = $request->email;

        $user = User::where('email',$email)->first();

        if(!$user)
        {
            return redirect('/admin/aggregators')->withErrors(['user'=>['Create a user with the email before adding here.']]);

        }
        else
        {
            Aggregator::create(['id' => $id,'name' => $name,'phone_number' => $phone_number,'bank_account_number' => $bank_account_number,'ifsc_code' => $ifsc_code,'bank_account_notes' => $bank_account_notes,'address' => $address,'pan' => $pan,'user_id' => $user->id]);

            return redirect('admin/aggregators');

        }

    }


    public function deleteAggregator($id,Request $request){

        $aggregator = Aggregator::where('id',$id)->first();

        if(!$aggregator)
        {
            return redirect('/admin/aggregators')->withErrors(['user'=>['The Aggregator is already deleted.']]);

        }
        else
        {
            Aggregator::where('id',$id)->delete();
            AggregatedBikes::where('aggregator_id',$id)->delete();

            return redirect('admin/aggregators');

        }

    }

    public function editAggregator($id,Request $request){

        $id = $request->id;
        $name = $request->name;
        $phone_number = $request->phone_number;
        $bank_account_number = $request->bank_account_number;
        $ifsc_code = $request->ifsc_code;
        $bank_account_notes = $request->bank_account_notes;
        $address = $request->address;
        $pan = $request->pan;
        $email = $request->email;

        $user = User::where('email',$email)->first();

        if(!$user)
        {
            return redirect('/admin/aggregators')->withErrors(['user'=>['Create a user with the email before adding here.']]);

        }
        else
        {
            Aggregator::where('id',$id)->update(['id' => $id,'name' => $name,'phone_number' => $phone_number,'bank_account_number' => $bank_account_number,'ifsc_code' => $ifsc_code,'bank_account_notes' => $bank_account_notes,'address' => $address,'pan' => $pan,'user_id' => $user->id]);

            return redirect('admin/aggregators');

        }


    }



    public function aggregatedBikeIndex(){

        $aggregatedBikeList = AggregatedBikes::all();
        $aggregatorList = Aggregator::all();
        $bikeNumberList = BikeNumbers::orderBy('id','desc')->get();
        $modelList = BikeModel::all();
        $cityList = City::all();
        return view('admin.addAggregatedBike.index',compact('aggregatedBikeList','aggregatorList','bikeNumberList','cityList','modelList'));

    }

    public function addAggregatedBike(Request $request){

        $rules = array(
          'model_id' => 'required',
          'bike_number_id' => 'required',
          'date_of_addition' => 'required',
          'aggregator_id' => 'required',
          'commission_perc' =>'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/aggregatedBikes')->withErrors($validator->errors());        
        }

        $model_id = $request->model_id;
        $bike_number_id = $request->bike_number_id;
        $date_of_addition = $request->date_of_addition;
        $aggregator_id = $request->aggregator_id;
        $citySelected =[];


        $cities = City::all();
        foreach($cities as $city)
        {
            if($request['ch'.$city->id])
            {
                array_push($citySelected,$city->id);
            }
        }
        AggregatedBikes::create(['model_id'=>$model_id,'bike_number_id'=>$bike_number_id,'date_of_addition'=>$date_of_addition." 00:00:00",'aggregator_id'=>$aggregator_id,'cities'=>implode(',',$citySelected),'commission_perc'=>$request->commission_perc]);

        return redirect('admin/aggregatedBikes');
    }


    public function deleteAggregatedBike($id,Request $request){

        $aggregatedBike = AggregatedBikes::where('id',$id)->first();

        if(!$aggregatedBike)
        {
            return redirect('/admin/aggregators')->withErrors(['user'=>['The Bike is already deleted.']]);
        }
        else
        {
            AggregatedBikes::where('id',$id)->delete();
            return redirect('admin/aggregatedBikes');
        }

    }


    public function indexOwnerblock(){
        $aggregatorList = Aggregator::all();
        $ownerBlockList = OwnerBlocks::all();
        return view('admin.ownerBlock.index',compact('aggregatorList','ownerBlockList'));
      }

    public function addOwnerBlock(Request $request){
        $rules = array(
          'aggregator_id' => 'required',
          'booking_id' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/ownerBlock')->withErrors($validator->errors());        
        }
        
        $booking = Booking::where('id',$request->booking_id)->first();
        
        if(!$booking)
        {
            return redirect('/admin/ownerBlock')->withErrors(['user'=>['There is no booking for the ID.']]);
        }
        else
        {
            if($booking->user_id!=15)
            {
                return redirect('/admin/ownerBlock')->withErrors(['user'=>['Booking is not an Owner Block.']]);
            }
            else
            {
                if(OwnerBlocks::where('booking_id',$booking->id)->count()>0)
                {
                    return redirect('/admin/ownerBlock')->withErrors(['user'=>['Owner Block already exists for the Booking']]);
                }
                else
                {
                    OwnerBlocks::create(['aggregator_id'=>$request->aggregator_id,'model_id'=>$booking->model_id,'start_datetime'=>$booking->start_datetime,'end_datetime'=>$booking->end_datetime,'booking_id'=>$booking->id]);
                }
                
            }
        }
        
        return redirect('/admin/ownerBlock');


      }

    public function deleteOwnerBlock($id, Request $request){
        
        $ownerBlock = OwnerBlocks::where('id',$id)->first();

        if(!$ownerBlock)
        {
            return redirect('/admin/ownerBlock')->withErrors(['user'=>['The block is already deleted.']]);
        }
        else
        {
            OwnerBlocks::where('id',$id)->delete();
            return redirect('admin/ownerBlock');
        }

      }
    
    public function calculatePayoutIndex(Request $request){
        return view('admin.aggregationCalcIndex.index');
    }
    
    public function calculatePayout(Request $request){
         $rules = array(
          'month' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/calculatePayoutIndex')->withErrors($validator->errors());        
        }
        
        $month = $request->month;
        
        $aggregators = Aggregator::all();
        $aggregatedBikes = AggregatedBikes::all();
        
        $firstDate = new Carbon('first day of '.$month);
        $lastDate = (new Carbon('last day of '.$month))->addDay();
        
        $bookings = Booking::whereNotIn('user_id',array(1,2,3,5,6,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25))->where('end_datetime','>',$firstDate)->where('end_datetime','<',$lastDate)->orderBy('end_datetime','asc')->get();
        
        $aggregatorsIndex = [];
        $aggregatedBikesIndex =[];
        $bookingsIndex = [];
        
        foreach($aggregators as $aggregator)
        {
            array_push($aggregatorsIndex,$aggregator->id);
            $aggregator['booking_bucket'] = [];
            $aggregator['aggregatedBike_bucket'] = [];
        }
        foreach($aggregatedBikes as $aggregatedBike)
        {
            array_push($aggregatedBikesIndex,$aggregatedBike->id);
            
            
            $areaArray =[];
            $cities = explode(',',$aggregatedBike->cities);
            $areas = Area::whereIn('city_id',$cities)->get();
            foreach($areas as $area)
            {
                array_push($areaArray,$area->id);
            }
            
            $aggregatedBike->areas = $areaArray;
            
            
            $aggregatedBike['booking_bucket'] = [];
            
            
        }
        foreach($bookings as $booking)
        {
            array_push($bookingsIndex,$booking->id);
        }
        
            
        
        
        foreach($bookings as $booking)
        {
            $booking['aggregator_bucket'] = [];
            
            foreach($aggregatedBikes as $aggregatedBike)
            {
                
                
                if($booking->model_id == $aggregatedBike->model_id)
                {
                    if(in_array($booking->area_id,$aggregatedBike->areas))
                    {
                        
                        if($booking->start_datetime >= $aggregatedBike->date_of_addition)
                        {
                            $ownerBlockCount = OwnerBlocks::where('model_id',$aggregatedBike->model_id)->where('start_datetime', '<', $booking->end_datetime)->where('end_datetime', '>', $booking->start_datetime)->where('aggregator_id',$aggregatedBike->$aggregatedBike)->count();
                            
                            
                            if($ownerBlockCount > 0)
                            {
                                $agg_bikes_from_agg = AggregatedBikes::where('aggregator_id',$aggregatedBike->$aggregatedBike)->where('date_of_addition','<',$booking->start_datetime)->where('model_id',$booking->model_id)->orderBy('commission_perc','asc')->get();
                                
                                $i = 1;
                                
                                foreach($agg_bikes_from_agg as $agg_bikes_from_agg_el)
                                {
                                    if($ownerBlockCount<$i)
                                    {
                                        if($agg_bikes_from_agg_el->id == $aggregatedBike->id)
                                        {
                                            $booking_agg_buc = $booking['aggregator_bucket'];
                                            array_push($booking_agg_buc,$aggregatedBike->id);
                                            $booking['aggregator_bucket'] = $booking_agg_buc;
                                            
                                            $agg_book_buc = $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['booking_bucket'];
                                            array_push($agg_book_buc,$booking);
                                            $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['booking_bucket'] = $agg_book_buc;
                                            
                                            $agg_bike_book_buc = $aggregatedBike['booking_bucket'];
                                            array_push($agg_bike_book_buc,$booking->id);
                                            $aggregatedBike['booking_bucket'] = $agg_bike_book_buc;
                                            
                                        }
                                    }
                                    $i++;
                                }
                                                                    
                            }
                            else
                            { 
                                $booking_agg_buc = $booking['aggregator_bucket'];
                                array_push($booking_agg_buc,$aggregatedBike->id);
                                $booking['aggregator_bucket'] = $booking_agg_buc;
                                
                                $agg_book_buc = $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['booking_bucket'];
                                array_push($agg_book_buc,$booking);
                                $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['booking_bucket'] = $agg_book_buc;
                                
                                $agg_bike_book_buc = $aggregatedBike['booking_bucket'];
                                array_push($agg_bike_book_buc,$booking->id);
                                $aggregatedBike['booking_bucket'] = $agg_bike_book_buc;
                                
                                
                            }
                            
                            
                        }
                        
                    }
                                        
                }
            
            }
        
        }
        
        
        foreach($aggregatedBikes as $aggregatedBike)
        {
            $aggregatedBike['totalCollection'] = 0;
            $booking_ids_for_agg_bike = $aggregatedBike['booking_bucket'];
            
            foreach($booking_ids_for_agg_bike as $booking_id_for_agg_bike)
            {
                $booking = $bookings[array_search($booking_id_for_agg_bike, $bookingsIndex)];
                
                $share = $booking->total_price/count($booking['aggregator_bucket']);
                
                $aggregatedBike['totalCollection'] += $share;
                
            }
            
            $agg_book_buc = $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['aggregatedBike_bucket'];
            array_push($agg_book_buc,$aggregatedBike);
            $aggregators[array_search($aggregatedBike->aggregator_id, $aggregatorsIndex)]['aggregatedBike_bucket'] = $agg_book_buc;
            
        }
        
        
        return view('admin.aggregationCalc.index',compact('aggregators'));
        
    
    }

}
