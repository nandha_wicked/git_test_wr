<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\GlobalConfig;

class GlobalConfigController extends AppController
{
    public function indexGlobalconfig(){
        $configdatas = GlobalConfig::where('active_status',1)->get();                
        return view('admin.globalconfig.index',compact('configdatas')); 
    	
    }
    
    public function editGlobalconfig($id, Request $request)
    {   
        GlobalConfig::where('parameter',$request->parameter)->where('active_status',1)->update(['updated_by'=>Auth::user()->id,'active_status'=>0]);
        GlobalConfig::create([
            'created_by'=>Auth::user()->id,
            'value'=>$request->value,
            'parameter'=>$request->parameter,
            'active_status'=>1
        ]);
        return redirect()->back()->withErrors(["success"=>"successfully changed"]);
    }
    
    public function editApiCategory($id, Request $request)
    {
        
        ApiCategory::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'category'=>$request->category_edit,
            'description'=>$request->description_edit
        ]);
        
        return redirect()->back();
    }
    
    public function addApiCall(Request $request)
    {
        
        $createdByUser = UserController::getLoggedInUser();
        
        $callId = DB::table('api_call')->insertGetId([
            'title'=>$request->call_name,
            'description'=>$request->call_description,
            'url'=>$request->call_url,
            'method'=>$request->call_type,
            'created_by'=>$createdByUser->id,
            'category'=>$request->call_category
        ]);
        
        
        Self::addCallDetails($request, $callId, $createdByUser);
        
        return redirect('/admin/api-call')->withErrors(["success"=>"API Call has been added successfully"]);
    }
     
    
}
