<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Enquiry;
use App\Models\User;
use App\Models\Booking;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Sendmail;
use App\Models\Bikation_Bookings;
use App\Models\OneWayRentalBooking;
use App\Models\UserDocImage;
use Mail;
use Carbon\Carbon;
use Session;
use Response;
use Auth;
use Hash;
use Image;
use DB;

class EdituserController extends AppController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
	 if (Auth::check())  
	 {
        $id=Auth::user()->id;
        $user = User::find($id);
        $first_name =$user['first_name'];
        $last_name=$user['last_name'];
        $image=$user['avatar_thumb'];
		$blur_image=$user['blur_image'];
        $location=$user['location'];
        $mobile_num=$user['mobile_num'];
        $email=$user['email'];
        $user_document_status=$user['user_document_status'];
		$work_email=$user['work_email'];
		
		 $bikation_user_bookingsList = DB::table('bikation_bookings')
            ->leftJoin('bikation_trip', 'bikation_trip.Trip_ID', '=', 'bikation_bookings.Trip_ID')
			->leftJoin('bikation_trip_photo', 'bikation_trip_photo.Trip_ID', '=', 'bikation_bookings.Trip_ID')
			->leftJoin('users', 'users.id', '=', 'bikation_bookings.Customer_ID')
			->where('bikation_bookings.Payment_status', '=', 1)
			->where('bikation_bookings.Customer_ID', '=', $id)
			->where('bikation_bookings.book_status', '=', 'Booking')
			//->Orwhere('bikation_bookings.book_status', '=', 'Cancel')
			->groupby('Booking_ID')
            ->get();
			//echo $id; exit;
			
			
			
			//print_R($bikation_user_bookingsList);
			
		 $bikation_user_bookings_cancel_List = DB::table('bikation_bookings')
            ->leftJoin('bikation_trip', 'bikation_trip.Trip_ID', '=', 'bikation_bookings.Trip_ID')
			->leftJoin('users', 'users.id', '=', 'bikation_bookings.Customer_ID')
            ->where('bikation_bookings.Customer_ID', '=', $id)
			->where('bikation_bookings.Payment_status', '=', 1)
            ->where('bikation_bookings.book_status', '=', 'Booking')
			->where('bikation_trip.Trip_Start_Date', '>', date('Y-m-d H:i:s'))
			 ->get();
			/*echo '<pre>';
			print_R($bikation_user_bookings_cancel_List); 
			exit;*/
		$bikation_user_bookings_complete_List = DB::table('bikation_bookings')
            ->leftJoin('bikation_trip', 'bikation_trip.Trip_ID', '=', 'bikation_bookings.Trip_ID')
			->leftJoin('users', 'users.id', '=', 'bikation_bookings.Customer_ID')
            ->where('bikation_bookings.Customer_ID', '=', $id)
			->where('bikation_bookings.Payment_status', '=', 1)
            ->where('bikation_bookings.book_status', '=', 'Booking')
			->where('bikation_trip.Trip_End_Date', '<=', date('Y-m-d'))
			 ->get();
			 
		$bikation_user_bookings_current_List = DB::table('bikation_bookings')
            ->leftJoin('bikation_trip', 'bikation_trip.Trip_ID', '=', 'bikation_bookings.Trip_ID')
			->leftJoin('users', 'users.id', '=', 'bikation_bookings.Customer_ID')
            ->where('bikation_bookings.Customer_ID', '=', $id)
			->where('bikation_bookings.Payment_status', '=', 1)
            ->where('bikation_bookings.book_status', '=', 'Booking')
			->where('bikation_trip.Trip_Start_Date', '<=', date('Y-m-d H:i:s'))
			->where('bikation_trip.Trip_End_Date', '>=', date('Y-m-d  H:i:s'))
			 ->get();	 
			
		$bikation_user_document_List = DB::table('user_doc_image')
			->select('user_doc_image.id','user_doc_image.user_id','user_doc_image.doc','user_doc_image.doc_type','user_doc_image.doc_reason','user_doc_image.status')
            ->leftJoin('users', 'users.id', '=', 'user_doc_image.user_id')
            ->where('user_doc_image.user_id', '=', $id)
             ->get();
			 
			 
		/* Bike Booking*/	 
		
    $models = BikeModel::where('status', 1)->get();
    $areas = Area::where('status', 1)->get();
    $today = Carbon::now();
    //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
    $bookings = DB::table('bookings')->where('user_id', '=', $id)->take(5000)->get();
	$bookings_cancel_List = DB::table('bookings')->where('user_id', '=', $id)->where('status', '=','COMPLETE')->where('start_datetime', '>=', date('Y-m-d'))->get();
	$bookings_complete_List = DB::table('bookings')->where('user_id', '=', $id)->where('status', '=', 'COMPLETE')->where('end_datetime', '<=', date('Y-m-d'))->get();
	$bookings_current_List = DB::table('bookings')->where('user_id', '=', $id)->where('status', '=', 'COMPLETE')->where('start_datetime', '<=', date('Y-m-d'))->where('end_datetime', '>=', date('Y-m-d'))->get();
	//$bikation_user_bookingsList['booking'] = 'bikation_book';
    foreach ($bookings as $booking) {
      $startDateObj = Carbon::parse($booking->start_datetime);
      $endDateObj = Carbon::parse($booking->end_datetime);
      $booking->booking = 'bike_book';
      $booking->start_date = $startDateObj->toDateString();
      $booking->end_date = $endDateObj->toDateString();
      $booking->start_time = $startDateObj->toTimeString();
      $booking->end_time = $endDateObj->toTimeString();
    }
	 foreach ($bikation_user_bookingsList as $bikation_user_booking) {
		    $bikation_user_booking->booking = 'bikation_book';
	 }
	//echo '<pre>';
	
	$bikation_book1 = array();
			
			foreach ($bikation_user_bookingsList as $key => $value) {
				$bikation_book1[$value->Trip_Start_Date.$value->Booking_ID] = $bikation_user_bookingsList[$key];
			}
	$bikation_book2 = array();
			
			foreach ($bookings as $key => $value) {
				$bikation_book2[$value->start_datetime.$value->id] = $bookings[$key];
			}	

	
	//$bikation_user_bookingsList = array_merge($bikation_user_bookingsList,$bookings);
	$bikation_user_bookingsList = array_merge($bikation_book1,$bikation_book2);
	$bikation_user_bookings_cancel_List = array_merge($bikation_user_bookings_cancel_List,$bookings_cancel_List);
	$bikation_user_bookings_complete_List = array_merge($bikation_user_bookings_complete_List,$bookings_complete_List);
	$bikation_user_bookings_current_List = array_merge($bikation_user_bookings_current_List,$bookings_current_List);
	//echo '<pre>';
	krsort($bikation_user_bookingsList);
	//print_r($bikation_user_bookingsList);
	
	//exit;
        return view('front.edituser',['first_name' => $first_name,'last_name' => $last_name,'small_image' => $image,'blur_image' => $blur_image,'location' => $location,'mobile_num' => $mobile_num,'email' => $email,'user_document_status' => $user_document_status], compact('bikation_user_bookingsList','bikation_user_bookings_cancel_List','bikation_user_bookings_complete_List','bikation_user_document_List','bikation_user_bookings_current_List'));
		
		 }else{
		 return redirect('login');
		}
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $task = User::find($id);
        return Response::json($task);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $user = User::findOrFail($id);
   
            $input = $request->all();
            $first_name =$request->first_name;
            $last_name=$request->last_name;
            $email=$request->email;
            $mobile_num=$request->mobile_num;
            $dob=$request->dob;
            $password=$request->password;
            $gender=$request->gender;
            $location=$request->location;
			$work_email=$request->work_email;
			 
            $save['first_name']=$first_name;
            $save['last_name']=$last_name;
            $save['email']=$email;
            $save['mobile_num']=$mobile_num;
			$save['work_email']=$work_email;
            $save['dob']= date('Y-m-d', strtotime($dob));
            if(!empty($password)){
                $save['password']=Hash::make($password);
            }
            
            $save['gender']=$gender;
            $save['location']=$location;
           
            $user->fill($save)->save();
                        
            return Response::json($user);
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendmail(Request $request)
    {

         $this->validate($request, [
               'email_send' => 'required|email'
                                
            ]);
            
            
            $input = $request->all();
            $email=$input['email_send'];

            $old_email = Sendmail::where('email_send', $email)->first();

            
           if(empty($old_email)){
            $user = Sendmail::create($request->all());
           }
            
            $data = array('name' => "Friend");
                Mail::send('emails.welcome_sendmail', $data, function ($message) use ($email){

                    $message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($email)->subject('Wickedride Friend Request');

                });
               
             $response = array( 'msg' => 'Setting created successfully');
 
        return Response::json( $response );
    }

    public function upload(Request $request) {

        if($request->file('user_avtar')) {
            $input=$request->all();

            $token=$input['_token'];

            $base64size = strlen($_POST['base64_avtar']);
            $f = base64_decode($_POST['base64_avtar']);
            $name =   'user_'.time() .".png";
            $tmpFilePath_optimize =  'img/user/optimize/';
            $image = file_put_contents($tmpFilePath_optimize .$name, $f);

            $tmpFilePath_blur_img =  'img/user/blur_img/';
            $img = Image::make($tmpFilePath_optimize .$name);
            $img->brightness(-40);
            $img->blur(100);
            $img->fit(1200,600, function ($constraint) {
                $constraint->aspectRatio();
            })->save($tmpFilePath_blur_img.'/'.$name);
            
            $save['blur_image'] = $tmpFilePath_blur_img.$name;

            $tmpFilePath_thumbh = 'img/user/thumbh/';
            $img = Image::make($tmpFilePath_optimize .$name);
            $img->fit(216,216, function ($constraint) {
            $constraint->aspectRatio();
            })->save($tmpFilePath_thumbh.'/'.$name);
            $image_thumbh = '//'.$_SERVER['HTTP_HOST'].'/'.$tmpFilePath_thumbh.$name;	  


            $id=Auth::user()->id;
            $user = User::find($id);
            if(isset($tmpFilePath_optimize_path)){
            $save['avatar_original']=$tmpFilePath_optimize_path;
            }else{
            $save['avatar_original']=$image;
            }

            $save['avatar_thumb']=$image_thumbh;
            $user->fill($save)->save();

            return response()->json($save);

        }
    }

	
	 public function upload_document(Request $request) {
     
            $output_file = base64_decode($request->base64);
            $name =   $request->user_doc.'-'.time() .".png";
            
            $docImageDestinationPath = 'img/user/document/';                    
            $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
            $docImageHashName = $str.'.png';

            $s3 = \Storage::disk('s3');
            $filePath = $docImageDestinationPath.$docImageHashName;
            $s3->put($filePath, $output_file, 'public');

            if($s3)
                $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
            else
                $image = "";
            
            $input=$request->all();

            $token=$input['_token'];


            $id=Auth::user()->id;
            $user = User::find($id);

            $inseart= UserDocImage::create([
                'user_id' =>$id, 
                'doc_type' => $request->user_doc,
                'doc' => $image,
                'doc_reason' => '',
                'status' => 0
            ]);

            $data = [
                'doc_type'=>$request->user_doc, 
                'user'=>$user->first_name .' '.$user->last_name, 
                'email'=>$user->email, 
                'mobile'=>$user->mobile_num
            ];

            $adm_email = env('ADMIN_EMAIL');
            Mail::send('emails.user_document_mail', $data, function ($message) use ($adm_email){
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->to($adm_email)->subject('User Document Upload');
            });

            return redirect('edituser/?doc');
    }
    

	
	 public function upload_document_app(Request $request) {

     
        $profileImgArray = $request->document_array;
        $dataArray = [];
        $i=1;
        $user = UserController::getUserByToken();
        if(isset($request->opsapp))
        {
            $idSplit = explode('@',$request->booking_id);
            $requestedId=$idSplit[1];
            $booking = OneWayRentalBooking::where('id',$requestedId)->first();
            $user = User::where('id',$booking->user_id)->first();
        }
        //$user = User::where('id',1)->first();
        $id = $user->id;
         
        while($i<6)
        {
            $profileImg = $request['image'.$i];
            if(isset($profileImg)) {
                $profileExt = $profileImg->getClientOriginalExtension();
                $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
                $profileImgHashName = $str.'.'.$profileExt;
                $profileImgDestinationPath = 'img/user/document/';
                $filePath = $profileImgDestinationPath.$profileImgHashName;

                $s3 = \Storage::disk('s3');
                $s3->put($filePath, file_get_contents($profileImg), 'public');

                if($s3)
                    $image = env('AWSS3URL').$filePath;
                else
                    $image = "";
            

                $inseart= UserDocImage::create(['user_id' =>$id, 'doc_type' => $request->doc_type,'doc' => $image,'doc_reason' => '','status' => 0]);

                $data = [
                    'message' => 'Image Stored',
                    'doc_id' => $inseart->id,
                    'url' => $image
                ];

                array_push($dataArray,$data);
                
            }
            $i++;
        }
         
        $data = ['doc_type'=>$request->doc_type, 'user'=>$user->first_name .' '.$user->last_name, 'email'=>$user->email, 'mobile'=>$user->mobile_num];

        $adm_email = env('ADMIN_EMAIL');
        Mail::send('emails.user_document_mail', $data, function ($message) use ($adm_email){
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->to($adm_email)->subject('User Document Upload');
        });
         
        return $this->respondWithSuccess($dataArray);
    }
    
    public function delete_document_app(Request $request)
	{
        $user = UserController::getUserByToken();
        $doc = UserDocImage::where('id', '=', $request->doc_id)->first();
        if(!$user)
        {
            $data = [
                    'message' => 'User Not LoggedIn'
                ];
            return $this->respondWithSuccess($data);
        }
        elseif(!$doc)
        {
            $data = [
                    'message' => 'Image Not Found'
                ];
            return $this->respondWithSuccess($data);
        }
        else
        {
            
            if($user->id == $doc->user_id)
            {
                UserDocImage::where('id', '=', $request->doc_id)->delete();
                $data = [
                        'message' => 'Image Deleted'
                    ];
                return $this->respondWithSuccess($data);
            }
            else
            {
                $data = [
                        'message' => 'User Not Verified'
                    ];
                return $this->respondWithSuccess($data);
            }
        }
		
	}
    
    public function get_user_document_status(Request $request)
	{
        $user = UserController::getUserByToken();
        $docs = UserDocImage::where('user_id',$user->id)->get();
        $dlImageResponseArray = [];
        $idImageResponseArray = [];
        $dlStatus = "pending";
        $idStatus = "pending";
        
        foreach($docs as $doc)
        {
            if(substr($doc->doc,0,4)== "http")
            {
                $url = $doc->doc;
            }
            else
            {
                $url = "http:".$doc->doc;   
            }
            
            if($doc->doc_type=="DL")
            {
                if($doc->status == 2)
                {
                
                    $docEle = ["status"=>"rejected","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($dlImageResponseArray,$docEle);
                    $dlStatus = "rejected";
                    
                }
                elseif($doc->status == 1)
                {
                    $docEle = ["status"=>"approved","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($dlImageResponseArray,$docEle);
                    if($dlStatus != "rejected"||$dlStatus != "pending")
                    {
                        $dlStatus = "approved";
                    }
                }
                else
                {
                    $docEle = ["status"=>"pending","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($dlImageResponseArray,$docEle);
                    if($dlStatus != "rejected")
                    {
                        $dlStatus = "pending";
                    }
                }
                    
            }
            else
            {
                if($doc->status == 2)
                {
                    
                    $docEle = ["status"=>"rejected","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($idImageResponseArray,$docEle);
                    $idStatus = "rejected";
                    
                }
                elseif($doc->status == 0)
                {
                    
                    $docEle = ["status"=>"pending","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($idImageResponseArray,$docEle);
                    if($idStatus != "rejected")
                    {
                        $idStatus = "pending";
                    }
                    
                }
                else
                {
                    $docEle = ["status"=>"approved","doc_id"=>$doc->id,"url"=>$url,"notes"=>$doc->doc_reason];
                    array_push($idImageResponseArray,$docEle);
                    if($idStatus != "rejected" || $idStatus != "pending")
                    {
                        $idStatus = "approved";
                    }
                }
                
            }
        
        }
        if($dlStatus== "pending")
        {
            if(count($dlImageResponseArray)==1)
                $imageStr = count($dlImageResponseArray)." image";
            else
                $imageStr = count($dlImageResponseArray)." images";
            if(count($dlImageResponseArray)>0)
                $dlStatus = $imageStr." uploaded. Approval Pending.";
            else
                $dlStatus = "0 images uploaded";
        }
        if($idStatus== "pending")
        {
            if(count($idImageResponseArray)==1)
                $imageStr = count($idImageResponseArray)." image";
            else
                $imageStr = count($idImageResponseArray)." images";
            if(count($idImageResponseArray)>0)
                $idStatus = $imageStr." uploaded. Approval Pending.";
            else
                $idStatus = "0 images uploaded"; 
        }
        
        $responseArray = ['dl'=>['status'=>$dlStatus,'images'=>$dlImageResponseArray],'id'=>['status'=>$idStatus,'images'=>$idImageResponseArray]];
        
        return $this->respondWithSuccess($responseArray);
		
	}
    
    
	
	public function cancel_booking_request(Request $request) {
		
			$bikation_booking = DB::table('bikation_bookings')->select('Trip_ID','Customer_ID','Total_Cost')->where('Booking_ID',$request->Booking_ID)->first();
			
            $bikation_trip_booking = DB::table('bikation_trip')->select('Trip_Name','Trip_Start_Date','Trip_End_Date','Start_Location')->where('Trip_ID',$bikation_booking->Trip_ID)->first();
            $bikation_booking_user= DB::table('users')->select('email','first_name','last_name','mobile_num')->where('id', $bikation_booking->Customer_ID)->first();
            $bikation_img_booking = DB::table('bikation_trip_photo')->select('Media_URL_details')->where('Trip_ID',$bikation_booking->Trip_ID)->first();

           /* Email Sending */
            $data = ['Booking_ID'=>$request->Booking_ID, 'fname'=>$bikation_booking_user->first_name .' '.$bikation_booking_user->last_name, 'email'=>$bikation_booking_user->email, 'mob'=>$bikation_booking_user->mobile_num, 'trip_name'=>$bikation_trip_booking->Trip_Name, 'start_date'=>$bikation_trip_booking->Trip_Start_Date, 'end_date'=>$bikation_trip_booking->Trip_End_Date, 'meeting_loc'=>$bikation_trip_booking->Start_Location, 'price'=>$bikation_booking->Total_Cost,'image'=>'http:'.$bikation_img_booking->Media_URL_details];

            $adm_email = env('BIKATION_ADMIN_EMAIL');
            Mail::send('emails.bikation_user_booking_cancel', $data, function ($message) use ($adm_email){
                $message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                $message->to($adm_email)->subject('Booking Cancel Request');
            });

            return response()->json('Cancel_req');
	}
    
    function base64_with_header_to_image($base64_string,$docImageDestinationPath) {
        
        $str1 = explode(",",$base64_string);
        if(!isset($str1[1]))
        {
            return ["success"=>false, "image"=>""];
        }
        
        $str2 = explode(";",$str1[0]);
        $str3 = explode("/",$str2[0]);
        $output_file = base64_decode($str1[1]);
         
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
        $docImageHashName = $str.'.'.$str3[1];
        
        $s3 = \Storage::disk('s3');
        $filePath = $docImageDestinationPath.$docImageHashName;
        $s3->put($filePath, $output_file, 'public');
        
        
        if($s3)
            $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
        else
            $image = "";
        
        return ["success"=>true, "image"=>$image]; 
    }
    
    
}
