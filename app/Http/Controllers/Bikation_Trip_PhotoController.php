<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Trip_Photo;
use Session;
use Image;
use File;


class Bikation_Trip_PhotoController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
    if(Session::has('email') && Session::has('First_Name')){
    
     $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Vendor_ID',Session::get('Vendor_ID'))->get();
     $selected="";
      if (isset($request->btn_submit)) {
         $bikation_trip_photoList = Bikation_Trip_Photo::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_trip_photoList = Bikation_Trip_Photo::where('Vendor_ID',Session::get('Vendor_ID'))->get();
        $selected="";
      }   
    
     $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
      //$bikation_trip_photoList = Bikation_Trip_Photo::all();
      foreach ($bikation_trip_photoList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      return view('bikationvendor.front.trip_photo_index',compact('bikation_trip_photoList','bikation_tripList','bikation_AlltripList','selected'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addTrip_Photo(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array(
            /* 'name' => 'required',
              'Description' => 'required',
              'No_of_Tickets' => 'required',
              'Tickes_Remaining' => 'required',
              'Status' => 'required'*/
            );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-trip-photo')->withErrors($validator->errors());
      }
      else{

        if($request->Media_Select == "Video"){

           $path_Media_URL=$request->Media_URL;
           $path_1=$request->Media_URL;
           $path_2=$request->Media_URL;
           $path_3=$request->Media_URL;
           $path_details=$request->Media_URL;
        }else{

          $coverImg = $request->file('Media_URL');
              if($coverImg){
                    $coverImgExt = $coverImg->guessClientExtension();
                    $img = Image::make($coverImg->getRealPath());
					
					$img1 = Image::make($coverImg->getRealPath());
					$img2 = Image::make($coverImg->getRealPath());
					$background = Image::canvas(630,481,'#ffffff');
					$img3 = Image::make($coverImg->getRealPath())->resize(630,481, function ($c) {
							$c->aspectRatio();
							$c->upsize();
						});
					

                    $coverImgHashName = time().'.'.$coverImgExt;
                    $coverImgDestinationPath_1 = 'img/trip_image/Media_URL_trip1/';
                    $coverImgDestinationPath_2 = 'img/trip_image/Media_URL_trip2/';
                    $coverImgDestinationPath_3 = 'img/trip_image/Media_URL_trip3/';
                    $coverImgDestinationPath_details = 'img/trip_image/Media_URL_details/';
                    $coverImgDestinationPath_Media_URL = 'img/trip_image/Media_URL/';
                    $coverImg->move($coverImgDestinationPath_Media_URL, $coverImgHashName);
                      $img->fit(435,350, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_1.'/'.$coverImgHashName);
                     $img1->fit(390,350,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_2.'/'.$coverImgHashName);
                      $img2->fit(630,350,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_3.'/'.$coverImgHashName);
					$background->insert($img3, 'center');
                   /*    $img3->fit(630,481,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_details.'/'.$coverImgHashName);*/
					$background->save($coverImgDestinationPath_details.'/'.$coverImgHashName);
                  $path_Media_URL = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_Media_URL.$coverImgHashName; 
                  $path_1 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_1.$coverImgHashName;
                  $path_2 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_2.$coverImgHashName;
                  $path_3 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_3.$coverImgHashName;
                  $path_details = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_details.$coverImgHashName;
              }

        }
        $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Trip_Photo::create(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'Media_Select' => $request->Media_Select, 'Media_Size' => $request->Media_Size, 'Media_URL' => $path_Media_URL,'Media_URL_trip1' => $path_1,'Media_URL_trip2' => $path_2,'Media_URL_trip3' => $path_3,'Media_URL_details' => $path_details,'created_on' => date('Y-m-d', strtotime($request->created_on)),'updated_on' => date('Y-m-d', strtotime($request->updated_on)),'Created_By'=>$request->Created_By,'Updated_by'=>$request->Updated_by,'Status'=>$request->Status]);
        return redirect('bikation-trip-photo');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($Trip_PhotoId, Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array(
            /* 'name' => 'required',
              'Description' => 'required',
              'No_of_Tickets' => 'required',
              'Tickes_Remaining' => 'required',
              'Status' => 'required'*/
            );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-trip-photo')->withErrors($validator->errors());
      }
      else{
       
         if($request->Media_Select == "Video"){
            if(empty($request->Media_URL)){
           $old_image = DB::table('bikation_trip_photo')->select('Media_URL','Media_URL_trip1','Media_URL_trip2','Media_URL_trip3','Media_URL_details')->where('id',$Trip_PhotoId)->first();
           $path_Media_URL=$old_image->Media_URL;
           $path_1=$old_image->Media_URL_trip1;
           $path_2=$old_image->Media_URL_trip2;
           $path_3=$old_image->Media_URL_trip3;
           $path_details=$old_image->Media_URL_details;
         }
        }else{

          $coverImg = $request->file('Media_URL');
              if($coverImg){

                 $old_image = DB::table('bikation_trip_photo')->select('Media_URL','Media_URL_trip1','Media_URL_trip2','Media_URL_trip3','Media_URL_details')->where('id', '=', $Trip_PhotoId)->first();
                  $path_Media_URL=$old_image->Media_URL;
                  $array_Media_URL= explode("/",$path_Media_URL);
                  $delete_path_Media_URL=$array_Media_URL[3].'/'.$array_Media_URL[4].'/'.$array_Media_URL[5].'/'.$array_Media_URL[6];


                   $path_1=$old_image->Media_URL_trip1;
                   $array_path_1= explode("/",$path_1);
                    $delete_path_1=$array_path_1[3].'/'.$array_path_1[4].'/'.$array_path_1[5].'/'.$array_path_1[6];

                   $path_2=$old_image->Media_URL_trip2;
                    $array_path_2= explode("/",$path_2);
                    $delete_path_2=$array_path_2[3].'/'.$array_path_2[4].'/'.$array_path_2[5].'/'.$array_path_2[6];

                   $path_3=$old_image->Media_URL_trip3;
                    $array_path_3= explode("/",$path_3);
                    $delete_path_3=$array_path_3[3].'/'.$array_path_3[4].'/'.$array_path_3[5].'/'.$array_path_3[6];

                   $path_details=$old_image->Media_URL_details;
                    $array_path_details= explode("/",$path_details);
                    $delete_path_details=$array_path_details[3].'/'.$array_path_details[4].'/'.$array_path_details[5].'/'.$array_path_details[6];

                   File::delete($delete_path_Media_URL,$delete_path_1,$delete_path_2,$delete_path_3,$delete_path_details);
                    $coverImgExt = $coverImg->guessClientExtension();
                    $img = Image::make($coverImg->getRealPath());
					$img1 = Image::make($coverImg->getRealPath());
					$img2 = Image::make($coverImg->getRealPath());
					$background = Image::canvas(630,481,'#ffffff');
					$img3 = Image::make($coverImg->getRealPath())->resize(630,481, function ($c) {
							$c->aspectRatio();
							$c->upsize();
						});
                    $coverImgHashName = time().'.'.$coverImgExt;
                    $coverImgDestinationPath_1 = 'img/trip_image/Media_URL_trip1/';
                    $coverImgDestinationPath_2 = 'img/trip_image/Media_URL_trip2/';
                    $coverImgDestinationPath_3 = 'img/trip_image/Media_URL_trip3/';
                    $coverImgDestinationPath_details = 'img/trip_image/Media_URL_details/';
                    $coverImgDestinationPath_Media_URL = 'img/trip_image/Media_URL/';
                    $coverImg->move($coverImgDestinationPath_Media_URL, $coverImgHashName);
                    $img->resize(435,350, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_1.'/'.$coverImgHashName);
                     $img1->resize(390,350,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_2.'/'.$coverImgHashName);
                      $img2->resize(630,350,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_3.'/'.$coverImgHashName);
                      $background->insert($img3, 'center');
                   /*    $img3->fit(630,481,function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($coverImgDestinationPath_details.'/'.$coverImgHashName);*/
					$background->save($coverImgDestinationPath_details.'/'.$coverImgHashName);

                  $path_Media_URL = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_Media_URL.$coverImgHashName;  
                  $path_1 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_1.$coverImgHashName;
                  $path_2 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_2.$coverImgHashName;
                  $path_3 = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_3.$coverImgHashName;
                  $path_details = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath_details.$coverImgHashName;
              }else{
                $old_image = DB::table('bikation_trip_photo')->select('Media_URL','Media_URL_trip1','Media_URL_trip2','Media_URL_trip3','Media_URL_details')->where('id',$Trip_PhotoId)->first();
                  $path_Media_URL=$old_image->Media_URL;
                   $path_1=$old_image->Media_URL_trip1;
                   $path_2=$old_image->Media_URL_trip2;
                   $path_3=$old_image->Media_URL_trip3;
                   $path_details=$old_image->Media_URL_details;
              }

        }
       $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Trip_Photo::where('id', $Trip_PhotoId)
        ->update(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'Media_Select' => $request->Media_Select, 'Media_Size' => $request->Media_Size, 'Media_URL' => $path_Media_URL,'Media_URL_trip1' => $path_1,'Media_URL_trip2' => $path_2,'Media_URL_trip3' => $path_3,'Media_URL_details' => $path_details,'created_on' => date('Y-m-d', strtotime($request->created_on)),'updated_on' => date('Y-m-d', strtotime($request->updated_on)),'Created_By'=>$request->Created_By,'Updated_by'=>$request->Updated_by,'Status'=>$request->Status]);
        return redirect('bikation-trip-photo');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function delete($Trip_PhotoId){
    if(Session::has('email') && Session::has('First_Name') ){
     
      $old_image = DB::table('bikation_trip_photo')->select('Media_URL','Media_URL_trip1','Media_URL_trip2','Media_URL_trip3','Media_URL_details')->where('id', '=', $Trip_PhotoId)->first();
                  $path_Media_URL=$old_image->Media_URL;
                  $array_Media_URL= explode("/",$path_Media_URL);
                  $delete_path_Media_URL=$array_Media_URL[3].'/'.$array_Media_URL[4].'/'.$array_Media_URL[5].'/'.$array_Media_URL[6];

                   $path_1=$old_image->Media_URL_trip1;
                   $array_path_1= explode("/",$path_1);
                    $delete_path_1=$array_path_1[3].'/'.$array_path_1[4].'/'.$array_path_1[5].'/'.$array_path_1[6];

                   $path_2=$old_image->Media_URL_trip2;
                    $array_path_2= explode("/",$path_2);
                    $delete_path_2=$array_path_2[3].'/'.$array_path_2[4].'/'.$array_path_2[5].'/'.$array_path_2[6];

                   $path_3=$old_image->Media_URL_trip3;
                    $array_path_3= explode("/",$path_3);
                    $delete_path_3=$array_path_3[3].'/'.$array_path_3[4].'/'.$array_path_3[5].'/'.$array_path_3[6];

                   $path_details=$old_image->Media_URL_details;
                    $array_path_details= explode("/",$path_details);
                    $delete_path_details=$array_path_details[3].'/'.$array_path_details[4].'/'.$array_path_details[5].'/'.$array_path_details[6];

                   File::delete($delete_path_Media_URL,$delete_path_1,$delete_path_2,$delete_path_3,$delete_path_details);
       $packages = Bikation_Trip_Photo::where('id', '=', $Trip_PhotoId)->delete();
      return redirect('bikation-trip-photo');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
}
