<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Bike;
use App\Models\City;
use App\Models\Area;
use App\Models\News;
use App\Models\Price;
use App\Models\Image;
use App\Models\ContactEnquiry;
use App\Models\EmailSubscribe;
use App\Models\PaytmOrderDetails;
use App\Models\User;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\Bikation_Reviews;
use App\Models\BikationVendor;
use App\Models\Bikation_Trip;
use App\Models\Bikation_Package;
use App\Models\Bikation_Address;
use App\Models\Bikation_Bookings;
use App\Models\Bikation_Itinerary;
use App\Models\SliderImage;
use App\Models\AccessoriesModel;
use App\Models\AccessoriesSize;
use App\Models\Parameters;
use App\Models\Faq;
use Auth;
use Crypt;
use Response;
use Session;
use Mail;
use Hash;
use DB;
use Config;
use Input;
use Carbon\Carbon;
require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");


class FrontPagesController extends AppController
{

	public function home(Request $request)
    {    
        
        
        $packagesMakeList = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_reviews', 'bikation_reviews.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Trip_url','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikation_reviews.Overall','bikationvendor_users.first_name','bikationvendor_users.last_name')
            ->where('bikation_trip.Status', '=', 1)
            ->where('bikation_trip.Trip_Start_Date', '>=', date("Y-m-d"))
			->where('bikation_trip.Approve_By_Admin', '=', 1)
            ->groupby('Trip_ID')
            ->get();

			 $trip_banner = DB::table ('bikation_banner')->select('banner_title','image')->where('status', 1)->first();   
			 
						
        foreach ($packagesMakeList as $model) {
            $Total_reviews = DB::table('bikation_reviews')->select('id','Trip_ID')->where('Trip_ID',$model->Trip_ID)->where('Status', '=', 1)->get();
            $model->Total_reviews= count($Total_reviews);
        }
        
        
        
        $sliderImages = SliderImage::where('status',1)->get();
        
        $faqs = Faq::get()->take(5);
        
        
    	return view('front/home',compact('packagesMakeList','sliderImages','faqs'));
    }
    
    public function appRedirect(Request $request)
    {    
        // $city_id = Session::has('current_cityId') ? Session::get('current_cityId') : 1;
        // $bikeMakeObjects = BikeMake::getUniqueBikeMakes($city_id);

    	return view('front/appHome');
    }
    
     public function appRedirectToWeb(Request $request)
    {  
         Session::put('redirectedfromapp', 'yes');
         return redirect('/');
    }
    public function aboutUs(Request $request)
    {    	
    	return view('front/aboutUs');
    }
     public function wpp(Request $request)
    {    	
    	return view('front/wpp');
    }
       public function wppMore(Request $request)
    {    	
    	return view('front/wpp-more');
    }
     
    public function offers(Request $request)
    {    	
    	return view('front/offers');
    }

    public function contactUs(Request $request)
    {
    	return view('front/contactUs');
    }

    public function faq(Request $request)
    {
        $faqs = Faq::get();
    	return view('front/faq',compact('faqs'));
    }

    public function reviews(Request $request)
    {
    	return view('front/reviews');
    }

    public function login(Request $request)
    {
        return view('front/login');
    }
    
    public function privacy(Request $request)
    {
        return view('front/privacy');
    }

    public function bikes($bike_name, Request $request)
    {    	
        $bikeModel = BikeModel::where('bike_model',$bike_name)->where('status', 1)->first();
        if(!$bikeModel)
        {
            return redirect('/');
        }
        else
        {
            return view('front/bike',compact('bikeModel'));
        }
     
    }


    public function bookingSelectDate(Request $request)
    {        
        // If in url model id is being sent then put it in session 
        // so that we can show that model on top in model book listing section
        
        $city_id = Session::get('current_cityId');
      
        if(!$city_id)
            $city_id = "1";
        $city = City::where('id',$city_id)->first();
        $model_id = $request->model_id;
        $model = BikeModel::where('id',$model_id)->first();
        
        if(isset($model_id))
        {
            Session::put('booking_model_priority', $model_id);
            
            $modelObj = (object) [
                'model_name' => $model->bike_model,
                'make_name' => $model->getBikeMakeName($model->bike_make_id),
                'city_name' => $city->city,
              ];
            
                
        }
        else
        {
           $modelObj = (object) [
                'model_name' => "none",
                'make_name' => "",
                'city_name' => "",
              ]; 
        }
        return view('front/booking/selectBookingDate',compact('modelObj','city_id'));
    }
    
    public function bookingSelectDateByName($make_name,$model_name)
    {        
        // If in url model id is being sent then put it in session 
        // so that we can show that model on top in model book listing section
        $city_id = Session::get('current_cityId');
      
        if(!$city_id)
            $city_id = "1";
        $city = City::where('id',$city_id)->first();
        $model = BikeModel::where('bike_model',$model_name)->where('status', 1)->first();
        
        
        if(!$model)
        {
            $model = BikeModel::where('status',1)->first();
            $model_id = $model->id;
        }
        else
            $model_id = $model->id;
        
        if(isset($model_id))
        {
            Session::put('booking_model_priority', $model_id);
            
            $modelObj = (object) [
                'model_name' => $model->bike_model,
                'make_name' => $model->getBikeMakeName($model->bike_make_id),
                'city_name' => $city->city,
              ];
            
                
        }

        return view('front/booking/selectBookingDate',compact('modelObj','city_id'));
    }

    public function bookingChooseBike(Request $request)
    {
        $fmdt = $request->from;
        $todt = $request->to;

        $fmdtArr = explode(" ", $fmdt);
        $todtArr = explode(" ", $todt);

        $fromDate = $fmdtArr[0];
        $toDate = $todtArr[0];

        $user_id = Session::get('user_id');

        $fm=date('Y-m-d', strtotime($fromDate));
        $to=date('Y-m-d', strtotime($toDate));
        
      
        $city_id = Session::get('current_cityId');
      
        if(!$city_id)
            $city_id = "1";

             
        $get_all_models_with_detail = \DB::table('bikes')
            ->select('bikes.*','bike_models.*')
            ->join('areas', 'areas.id', '=', 'bikes.area_id')
            ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->where('areas.city_id', $city_id)
            ->where('bike_models.status', 1)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            ->orderBy('priority', 'desc')
            ->groupBy('bikes.model_id')         
            ->get();
        
        usort($get_all_models_with_detail, 'my_sort_function');
        

        function my_sort_function($a, $b)
        {
            return $a->priority < $b->priority;
        }

        // For each model add list of areas that model is available in.
        foreach($get_all_models_with_detail as $model){               
            $model->areas = FrontPagesController::get_areas_by_model_id($model->model_id,$city_id, $fm, $to);
            $model->pid = $this->getPriceId($model->model_id, $model->area_id);       
            $model->totalPrice = $this->getPriceByDate($fromDate,$toDate,$model->pid); //FrontPagesController::get_areas_by_model_id($model->model_id,$city_id);       
            $model->weekDayPrice = $this->getWeekDayPrice($model->pid);
            $model->fromDate = $fromDate;
            $model->toDate = $toDate;
            $model->userId = $user_id;
            $model->makeName = $this->getBikeMakeName($model->bike_make_id);
            $model->areaName = $this->getAreaName($model->area_id);
            $model->perDayPrice = $this->perDayPrice($fromDate, $toDate, $model->totalPrice);
            $model->allAreasBooked = $this->checkAllAreasBooked($model->model_id, $city_id, $fm, $to);           
            //$model->bookedAid = $av;
        } 

        $get_all_models_with_detail;


        /*
        **
        ** If booking_model_priority is passed in session then show that model first in result.
        **
        */        
        $model_id = Session::get('booking_model_priority');
        
        if(isset($model_id))
        {
            foreach ($get_all_models_with_detail as $key => $value) 
            {
                if($value->model_id == $model_id)
                {
                    $swap_key = $key;

                    $new_value = $get_all_models_with_detail[$swap_key];
                    unset($get_all_models_with_detail[$swap_key]);
                    array_unshift($get_all_models_with_detail, $new_value);
                    break;
                }
            }
        }


        //return $d1;
        /*return Response::json(array(
                'details' => $get_all_models_with_detail,
                'd1' => $d1
            ));*/

        
        /*echo "<pre>";
            print_r($get_all_models_with_detail);
        echo "</pre>";
        die();*/

        return view('front/booking/bookingChooseBike',compact('get_all_models_with_detail'));
    }
    
    public function bookingShowAccessories(Request $request)
    {
        $modelId = $request->modelId;
        $cityId = $request->cityId;
        $areaName = $request->areaName;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $totalPrice = $request->totalPrice;
        $full_slab_price = $request->full_slab_price;
        $number_of_hours = $request->number_of_hours;
        $minimum_hours = $request->minimum_hours;
        
        $dispatcher = app('Dingo\Api\Dispatcher');

        $accessories = $dispatcher->with(['area_id' => $areaName, 'start_date' => $start_date, 'end_date' => $end_date, 'start_time' => $start_time, 'end_time' => $end_time])->get('check-accessories-availability-for-area-with-dates');
        
        
        
        $modelObjs = AccessoriesModel::get()->keyBy('id');
        $sizeObjs = AccessoriesSize::get()->keyBy('id');
        
        $modeljson = json_encode($modelObjs);
        $modeljson = addslashes($modeljson);
        
        $sizejson = json_encode($sizeObjs);
        $sizejson = addslashes($sizejson);
        
        return view('front/booking/chooseAccessoriesScreen',compact('modelId','cityId','areaName','start_date','end_date','start_time','end_time','totalPrice','full_slab_price','number_of_hours','minimum_hours','accessories','accessoriesJSON','sizejson','modeljson'));
        
        
    }


    public function bookingCheckUserLoggedin(Request $request)
    {
        $modelId = $request->modelId;
        $cityId = $request->cityId;
        $areaId = $request->areaName;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $totalPrice = $request->totalPrice;
        $full_slab_price = $request->full_slab_price;
        Session::put('noTaxAmt', $totalPrice);
        Session::put('fullSlabAmt', $full_slab_price);
        
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        Session::put('taxAmt', floor($totalPrice * round(($taxDetails->amount/100),2) * 100)/100);

        //TAX
        $totalPrice = $totalPrice + round(($totalPrice * ($taxDetails->amount/100)),2);
        
        
        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $area = Area::where('id', $areaId)->where('status', 1)->first();
        
        $areaIds = Area::where('city_id', $cityId)->get()->lists('id');
        
        $price = Price::where('model_id',$modelId)->where('area_id',$areaId)->where('status',1)->orderBy('id','desc')->first();
        if(!$price)
        {
            $price = Price::where('model_id',$modelId)->where('area_id',0)->where('status',1)->orderBy('id','desc')->first();
        }
        

        $image = Image::where('id', $model['thumbnail_img_id'])->first();
        $number_of_hours = $request->number_of_hours;
        $minimum_hours = $request->minimum_hours;
        
        $max_hours = max($number_of_hours,$minimum_hours);
        
        $extrakmprice = Price::getExtraKMPriceformodel($modelId);

        $freekm = $max_hours*env('KMLIMIT');
        
        $startDateTime = Carbon::parse($startDate.' '.$startTime);
        $endDateTime = Carbon::parse($endDate.' '.$endTime);
        
        if(in_array($areaId,explode(",",env('AREAINCLUDEDFUEL'))) && ($endDateTime->diffInHours($startDateTime) < 24))
        {
            $message = 'All rentals less than 24 hours include fuel and costs Rs. '.$price->per_km_fuel_included.' per KM';
        }
        else
        {
            $message = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";
        }
            
        
        Session::put('booking_model_priority', $modelId);
        Session::put('bkmodelId', $modelId);
        Session::put('bkcityId', $cityId);
        Session::put('bkareaId', $areaId);
        Session::put('bktotalPrice', $totalPrice);
        Session::put('bkmodelName', $model['bike_model']);
        Session::put('bkmodelImg', $image['full']);
        Session::put('bkareaName', $area['area']);
        Session::put('bkareaAddress', $area['address']);
        Session::put('bkStartDate', $startDateTime->toDateString());
        Session::put('bkEndDate', $endDateTime->toDateString());
        Session::put('bkStartTime', $startDateTime->toTimeString());
        Session::put('bkEndTime', $endDateTime->toTimeString());
        Session::put('number_of_hours', $number_of_hours);
        Session::put('minimum_hours', $minimum_hours);
        Session::put('message', $message);
        Session::put('freekm',$freekm);
        Session::put('bkFuelncluded',1);

        if(Auth::check()){
            
            //return view('/front/booking/bookingConfirm');
            return redirect('/booking/confirm');
        }
        else
            return redirect('/booking/login');
    }

    public function bookingLogin()
    {
        return view('front/booking/bookingLogin');
        //echo session('bkmodelId');
    }

    public function bookingConfirm()
    {
        $startDate = Session::get('bkStartDate');
        $endDate = Session::get('bkEndDate');
        $startTime = Session::get('bkStartTime');
        $endTime = Session::get('bkEndTime');
        $modelId = Session::get('bkmodelId');
        $areaId = Session::get('bkareaId');
        $cityId = Session::get('bkcityId');
        
        DB::table('test_log')->insert(['string'=>$startDate." - ".$endDate." - ".$startTime." - ".$endTime." - ".$modelId." - ".$areaId." - ".$cityId]);
        
        if(empty($startDate)||empty($modelId)||empty($areaId))
            return redirect('/booking/select-date');
        
        $model = BikeModel::where('id',$modelId)->first();
        $minAge = $model->age_limit;
        $security_deposit = $model->security_deposit;
        $area = Area::where('id',$areaId)->first();
        
        $startDateTime = Carbon::parse($startDate.' '.$startTime);
        $endDateTime = Carbon::parse($endDate.' '.$endTime);
        $dateStr = $startDateTime->format('D j My ha').' to '.$endDateTime->format('D j My ha');
        $enquiry = $model->bike_model." - ".$area->area."  From ".$dateStr;

        if (Auth::check() && !empty($startDate) && !empty($endDate) && !empty($startTime) && !empty($endTime) && !empty($modelId) && !empty($areaId) && !empty($cityId))
        {
            $userName = Auth::user()->first_name." ".Auth::user()->last_name;
            $userEmail = Auth::user()->email;
            $userMob = Auth::user()->mobile_num;
            
            $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
            
            return view('front/booking/bookingConfirm',compact('userName', 'userEmail', 'userMob', 'security_deposit', 'enquiry', 'taxDetails','minAge'));
        }
        else
            return redirect('/booking/select-date');
    }
    
    public function redirectForPaytm(Request $request)
    {
        $startDate = Session::get('bkStartDate');
        $endDate = Session::get('bkEndDate');
        $startTime = Session::get('bkStartTime');
        $endTime = Session::get('bkEndTime');
        $modelId = Session::get('bkmodelId');
        $areaId = Session::get('bkareaId');
        $startDateTime = Carbon::parse($startDate.' '.$startTime);
        $endDateTime = Carbon::parse($endDate.' '.$endTime);
        $user = Auth::user();
        
        $paytmOrderDetails = PaytmOrderDetails::create([
            'area_id'=>$areaId,
            'model_id'=>$modelId,
            'start_datetime'=>$startDateTime->toDateTimeString(),
            'end_datetime'=>$endDateTime->toDateTimeString(),
            'promocode'=>$request->promocode,
            'user_id'=>$user->id,
            'note'=>$request->note
        ]);
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'start_date' => $startDate, 
            'end_date' => $endDate, 
            'start_time' => $startTime, 
            'end_time' => $endTime, 
            'coupon' => $request->promocode,
            'device'=>'WEB'
        ])->get('bookings/price-and-check-availability');
        
        if($bikeAvailabilityWithPrice['bike_availability'])
        {
            $TXN_AMOUNT = $bikeAvailabilityWithPrice['total_price_with_tax'];
        }
        else
        {
            return redirect('/booking/select-date');
        }
       
        $checkSum = "";
        $payTMParams = array();

        $ORDER_ID = $paytmOrderDetails->id;
        $CUST_ID = $user->id;
        $INDUSTRY_TYPE_ID = env('PAYTM_INDUSTRY');
        $CHANNEL_ID = "WEB";
        

        // Create an array having all required parameters for creating checksum.
        $payTMParams["MID"] = env('PAYTM_MERCHANT_MID');
        $payTMParams["ORDER_ID"] = $ORDER_ID;
        $payTMParams["CUST_ID"] = $CUST_ID;
        $payTMParams["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
        $payTMParams["CHANNEL_ID"] = $CHANNEL_ID;
        $payTMParams["TXN_AMOUNT"] = $TXN_AMOUNT;
        $payTMParams["WEBSITE"] = env('PAYTM_MERCHANT_WEBSITE');
        $payTMParams["MSISDN"] = $user->mobile_num; //Mobile number of customer
        $payTMParams["EMAIL"] = $user->email; //Email ID of customer
        $payTMParams["VERIFIED_BY"] = "EMAIL"; //
        $payTMParams["IS_USER_VERIFIED"] = "YES"; //
        $payTMParams["CALLBACK_URL"] = env('PAYTMCALLBACKURL')."?_token=".Session::token();
        
        $checkSum = getChecksumFromArray($payTMParams,env('PAYTM_MERCHANT_KEY'));
        
        return view('front/booking/payTMRedirectionPage',compact('payTMParams', 'checkSum'));


        
    }
    
    public function bookingConfirmZero(Request $request)
    {
        $startDate = Session::get('bkStartDate');
        $endDate = Session::get('bkEndDate');
        $startTime = Session::get('bkStartTime');
        $endTime = Session::get('bkEndTime');
        $modelId = Session::get('bkmodelId');
        $areaId = Session::get('bkareaId');
        $cityId = Session::get('bkcityId');
        $model = BikeModel::where('id',$modelId)->first();
        $minAge = $model->age_limit;
        $security_deposit = $model->security_deposit;
        $area = Area::where('id',$areaId)->first();
        $coupon = $request->coupon;
        
        $startDateTime = Carbon::parse($startDate.' '.$startTime);
        $endDateTime = Carbon::parse($endDate.' '.$endTime);
        $dateStr = $startDateTime->format('D j My ha').' to '.$endDateTime->format('D j My ha');
        $enquiry = $model->bike_model." - ".$area->area."  From ".$dateStr;

        if (Auth::check() && !empty($startDate) && !empty($endDate) && !empty($startTime) && !empty($endTime) && !empty($modelId) && !empty($areaId) && !empty($cityId))
        {
            $userName = Auth::user()->first_name." ".Auth::user()->last_name;
            $userEmail = Auth::user()->email;
            $userMob = Auth::user()->mobile_num;
            
            return view('front/booking/bookingConfirmZero',compact('userName','userEmail','userMob','security_deposit','enquiry','coupon','minAge'));
        }
        else
            return redirect('/booking/select-date');
    }



    public function booking(Request $request)
    {    
       
    	return view('front/booking',compact('get_all_models_with_detail','all_areas','prc'));
    }

    public function listMyBike(Request $request)
    {    	
    	return view('front/listMyBike');
    }

    public function getSelectedCity(Request $request){

        $city_id=$request->select_activeCity;
        /*if(isset($cityId))
            $city_id = $cityId;
        else
            $city_id = "1";    */
        /*if($cityId)  */   
            Session::put('current_cityId', $city_id); // Storing city-id in session variable which can be accessable in all pages.
        /*else*/
        
        return redirect('/');
    }
    
    public function getSelectedCityBangalore(){
        
  
            Session::put('current_cityId', 1);

        return redirect('/');
    }
    
     public function getSelectedCityMysore(){
        

        
            Session::put('current_cityId', 4); 

        return redirect('/');
    }
    
         public function getSelectedCityJaipur(){
        

       
            Session::put('current_cityId', 2); 

        return redirect('/');
    }
         public function getSelectedCityUdaipur(){
        
       
            Session::put('current_cityId', 3); 

        return redirect('/');
    }
         public function getSelectedCityBhuj(){
        
       
            Session::put('current_cityId', 5); 

        return redirect('/');
    }
         public function getSelectedCityAhmedabad(){
        

             
            Session::put('current_cityId', 6); 

        return redirect('/');
    }
         public function getSelectedCityBelagavi(){
        

       
            Session::put('current_cityId', 8); 

        return redirect('/');
    }
         public function getSelectedCityHampi(){
        

       
            Session::put('current_cityId', 7); 

        return redirect('/');
    }
         public function getSelectedCityJaisalmer(){
        
       
            Session::put('current_cityId', 9); 

        return redirect('/');
    }
         public function getSelectedCityDelhi(){
        

       
            Session::put('current_cityId', 10); 

        return redirect('/');
    }
    



    public function addContactEnquiry(Request $request){       

        $uri=$request->path();
        //Validating Captchas
        $requests = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.env('GOOGLE_RECAPTCHA_SECRET').'&response='.$request['g-recaptcha-response']);
        $response = json_decode($requests);
        if(!$response->success)
        {
            $message = 'Please, try again. You must complete the Captcha!'; 
            if($uri == "contact-enquiry/add") {
                return redirect('/contact-us')->with('contactSuccessMsg',$message);
            } else if($uri == "faq-home-enquiry/add")
            return redirect('/')->with('faqHomeSuccessMsg',$message);  
            else
             return redirect('/faqs')->with('faqSuccessMsg',$message);
     }

        $contactEnquiryCount = Session::get('contactEnquiryCount');
        $lastEnquiryStr = Session::get('lastEnquiry');
            
        if(!$contactEnquiryCount)
            $contactEnquiryCount = 0;

        if(!$lastEnquiryStr)
            $lastEnquiryStr = Carbon::parse('last year');
        
        $lastEnquiry = Carbon::parse($lastEnquiryStr);
        
        if($uri == "contact-enquiry/add"){

            $rules = array(
                'firstName' => 'required',
                'email' => 'required|email',
                'mobileNumber' => 'required|digits:10',
                'comment' => 'required'
            );
            
            
            
            if(!(($lastEnquiry->diffInMinutes(Carbon::now()) < 240) && $contactEnquiryCount > 2))
            {
                
                $fname = $request->firstName;
                $lname = $request->lastName;
                $email = $request->email;
                $mob = $request->mobileNumber;
                $comment = $request->comment;
                $data = ['fname'=>$fname, 'lname'=>$lname, 'email'=>$email, 'mob'=>$mob, 'comment'=>$comment];

                $validator = Validator::make($request->all(),$rules);

                if($validator->fails())
                    return $this->respondWithValidationError($validator->errors());        

                ContactEnquiry::create(['first_name' => $request->firstName, 'last_name' => $request->lastName, 'email' => $request->email,
                                        'mobile_num' => $request->mobileNumber, 'enquiry' => $request->comment]);

                Mail::send('emails.user_contact_mail',$data,function($message){
                    $message->to(Input::get('email'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' ContactEnquiry');
                });

                Mail::send('emails.admin_contact_mail',$data,function($message){
                    $message->to('customer-support@wickedride.com');
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' ContactEnquiry');
                });
                $contactEnquiryCount++;
                Session::put('contactEnquiryCount',$contactEnquiryCount);
                Session::put('lastEnquiry',Carbon::now()->toDateTimeString());

                return redirect('/contact-us')->with('contactSuccessMsg','Thank You for contacting.....');
                
            }
        }
        else{

            $rules = array(
                'firstName' => 'required',
                'lastName' => 'required',
                'email' => 'required|email',
                'mobileNumber' => 'required|numeric',
                'enquiry' => 'required'
            );

            $fname = $request->firstName;
            $lname = $request->lastName;
            $email = $request->email;
            $mob = $request->mobileNumber;
            $enquiry = $request->enquiry;
            $data = ['fname'=>$fname, 'lname'=>$lname, 'email'=>$email, 'mob'=>$mob, 'enquiry'=>$enquiry];

            $validator = Validator::make($request->all(),$rules);

            $validator->after(function($validator){
                if( strlen(Input::get('mobileNumber')) != 10 ){
                    $validator->errors()->add('Error', 'Mobile number must be 10 digit!');
                }
            });

            if($validator->fails()){
                if($uri == "faq-home-enquiry/add"){
                    return redirect('/')
                    ->withErrors($validator,'faqhome')
                    ->withInput();  
                }
                else{
                    return redirect('/faqs')
                    ->withErrors($validator,'faq')
                    ->withInput();  
                }
            }
            else{
                
                if(!(($lastEnquiry->diffInMinutes(Carbon::now()) < 240) && $contactEnquiryCount > 2))
                {
                   
                    ContactEnquiry::create(['first_name' => $request->firstName, 'last_name' => $request->lastName, 'email' => $request->email,
                                            'mobile_num' => $request->mobileNumber, 'enquiry' => $request->enquiry]);

                    Mail::send('emails.user_faq_mail',$data,function($message){
                        $message->to(Input::get('email'));
                        $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                        $message->subject(env('SITENAMECAP').' Faqs');
                    });

                    Mail::send('emails.admin_faq_mail',$data,function($message){
                        $message->to('customer-support@wickedride.com');
                        $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                        $message->subject(env('SITENAMECAP').' Faqs');
                    });
                    $contactEnquiryCount++;
                    Session::put('contactEnquiryCount',$contactEnquiryCount);
                    Session::put('lastEnquiry',Carbon::now()->toDateTimeString());

                    if($uri == "faq-home-enquiry/add")
                        return redirect('/')->with('faqHomeSuccessMsg','Your enquiry sent successfully.....');  
                    else
                        return redirect('/faqs')->with('faqSuccessMsg','Your enquiry sent successfully.....'); 
                    
                }
            }

        }   
    }



    public function addEmailSubscribers(Request $request){

        $rules = array(
                'email' => 'required|email',
            );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            $message = "Please enter a valid email id.";
            return view('front/error', compact('message'));    
        }
        else{

            $email = $request->email;

            EmailSubscribe::create(['email' => $email]);

            $data=array('email'=> $email);
            Mail::send('emails.user_subscriber_mail',$data,function($message){
                $message->to(Input::get('email'));
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->subject(env('SITENAMECAP').' Email Subscription');
            });

            Mail::send('emails.admin_subscriber_mail',$data,function($message){
                $message->to(env('ADMIN_EMAIL'));
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->subject(env('SITENAMECAP').' Email Subscription');
            });


            return redirect('/')->with('subscribeSuccessMsg','Thank You for subscribing.....');           
        }
    }


    public function doLogin(Request $request){

        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        $bookValue = $request->booking;
        $planValue = $request->plan;
        // run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) { 
            $status = false;
        } else {
            // create our user data for the authentication
            $userdata = array(
                'email'     => $request->email,
                'password'  => $request->password
            );          

            // attempt to do the login
            if (Auth::attempt($userdata)) 
            {               

                $status = true;
                //echo 'SUCCESS!';
            } else {        
                // validation not successful, send back to form 
                $status = false;
                $validator->errors()->add('Error', 'Invalid username or password!');
            }
        }

        return Response::json(array(
            'success' => $status,
            'booking' => $bookValue,
            'plan' => $planValue,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }


    public function doSignup(Request $request){

        $rules = array(
            'firstName' => 'required',
            'lastName' => 'required',
            //'email'    => 'required|email',
            'email'    => 'required|unique:users,email', // make sure the email is an actual email
            'mobileNumber' => 'required|numeric',
            'password' => 'required|alphaNum|min:5', // password can only be alphanumeric and has to be greater than 3 characters
            'confirmPassword' => 'required|alphaNum|min:5'
        );

        $validator = Validator::make($request->all(), $rules);
        $password1 = $request->password;
        $password2 = $request->confirmPassword;
        $fname = $request->firstName;
        $lname = $request->lastName;
        $email = $request->email;
        $mob=$request->mobileNumber;
        $bookValue = $request->booking;
        $planValue = $request->plan;


        if ($validator->fails()) { 
            $status = false;
        } 
        else {
            if($password1 != $password2){ 
                $status = false;
                $validator->errors()->add('Error', 'Confirm Password Mismatch!');
            }
            else if( strlen($mob) != 10 ){
                $status = false;
                $validator->errors()->add('Error', 'Mobile number must be 10 digit!');
            }
            else{
                $status = true;
                
                $length = 5;
                $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
        
                $fname = ucwords(strtolower($request->firstName));
                $lname = ucwords(strtolower($request->lastName));
                
                $user = User::create(['first_name' => $fname, 'last_name' => $lname, 'email' => $request->email,'mobile_num' => $request->mobileNumber, 'password' => Hash::make($request->password),'referral_code'=>$referral_code]);
                
                
                

                $data = ['fname'=>$fname, 'lname'=>$lname, 'email'=>$email, 'mob'=>$mob, 'password'=>$password1];
                Mail::send('emails.user_signup_mail',$data,function($message){
                    $message->to(Input::get('email'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' User SignUp');
                });

                Mail::send('emails.admin_signup_mail',$data,function($message){
                    $message->to(env('ADMIN_EMAIL'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' User SignUp');
                });


                $userdata = array(
                    'email'     => $request->email,
                    'password'  => $request->password
                );          

                // attempt to do the login
                Auth::attempt($userdata);        
            }  
        }

        return Response::json(array(
            'success' => $status,
            'booking' => $bookValue,
            'plan' => $planValue,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }


    public function doLogout(Request $request){
        Auth::logout();
        return redirect('/'); 
    }



    public function getPriceByDate($fromDate, $toDate, $priceId){

        
        $fromTs = strtotime($fromDate);
        $toTs = strtotime($toDate);
        $diffTs = $toTs - $fromTs;
        $no_of_days = floor($diffTs/(60*60*24));
        $price=0;

        $bike = new Bike();
      
        if($no_of_days == "1" || $no_of_days == "2")
        {
            $price = 0;

            if(date('N', strtotime($fromDate)) == 7 && date('N', strtotime($toDate)) == 1)
               $price += $bike->getWeekDayPrice($priceId);
            else if(date('N', strtotime($fromDate)) == 7 && $no_of_days == 2)
               $price += $bike->getWeekDayPrice($priceId);    
            else if(date('N', strtotime($fromDate)) >= 5)
               $price += $bike->getWeekEndPrice($priceId);
            else
               $price += $bike->getWeekDayPrice($priceId);

            if($no_of_days == 2)
                if(date('N', strtotime($toDate)) >= 6)
                   $price += $bike->getWeekEndPrice($priceId);
                else
                   $price += $bike->getWeekDayPrice($priceId);

            /*if($no_of_days == "1"){
                if(date('N', $fromTs) >= 6)
                    $price += $bike->getWeekEndPrice($priceId);
                else
                    $price += $bike->getWeekDayPrice($priceId);
            }
            else {
                if(date('N', $fromTs) < 6  &&  date('N', $toTs) < 6)
                    $price += $bike->getWeekDayPrice($priceId);
                else if(date('N', $fromTs) >= 6  &&  date('N', $toTs) >= 6)
                    $price += $bike->getWeekEndPrice($priceId);
                else               
            }  */  

            return $price;
        
           }else if((3 <= $no_of_days) && ($no_of_days <= 6)){
                return $price = $no_of_days * $bike->get_3_6_DaysPrice($priceId);
               
           }else if((7 <= $no_of_days) && ($no_of_days <= 14)){
                return $price = $no_of_days * $bike->get_7_14_DaysPrice($priceId);

           }else if((15 <= $no_of_days) && ($no_of_days <= 30)){
                return $price = $no_of_days * $bike->get_15_30_DaysPrice($priceId);
           }else{
                return $price = $no_of_days * $bike->get_30_plus_DaysPrice($priceId);
           }
        
    }



    public function get_areas_by_model_id($model_id,$city_id,$from_date,$to_date)
    {
        $areas =  \DB::table('areas')
            ->select('bikes.*','areas.*')
            ->join('bikes', 'areas.id', '=', 'bikes.area_id')
            //->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->where('areas.city_id', $city_id)
            ->where('bikes.model_id', $model_id)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            //->where('bikes.model_id', "1")
            ->groupBy('areas.id')         
            ->get();


            $bike = new Bike();
        foreach($areas as $area){
            //$area->bikeBooked = $bike->checkBikeAvailability($from_date, $to_date, $model_id, $area->area_id);
            //$area->bikeCount = $bike->getBikeCount($model_id, $area->area_id);
            $area->bikeAvailability = $bike->checkBikeAvailability($from_date, $to_date, $model_id, $area->area_id);
            $area->totalPrice = $this->getPriceByDate($from_date, $to_date, $area->price_id);
            $area->perDayPrice = $this->perDayPrice($from_date, $to_date, $area->totalPrice);
        }    

        return $areas;
            /*$bike = new Bike();
            $b = $bike->checkBikeAvailability($from_date,$to_date,$model_id,1);
            if($b)
                echo "yes";
            else
                echo "no";*/
/*$bike = new Bike();                
foreach ($a as $c) {
    //echo $c->area_id;
    $c->areaStatus = $bike->checkBikeAvailability($from_date,$to_date,$model_id,$c->area_id);*/
/*}
        echo "<pre>"; 
        print_r($c);
        echo "</pre>";*/
    }

    public function getWeekDayPrice($price_id){

        $price = Price::where('id', $price_id)->first();
        return $price->week_day_price;   
    }

    public function getPriceId($model_id, $area_id){
        $price = Price::where('model_id', $model_id)
                ->where('area_id',$area_id)
                ->first();
        
        if(!$price)
        {
            $price = Price::where('model_id',$model_id)->orderBy('id','asc')->first();
        }

        return $price->id;        
    }

    public function getBikeMakeName($make_id){
        $bikemake = BikeMake::where('id', $make_id)->where('status', 1)->first();
        return $bikemake->bike_make;
    }

    public function getAreaName($area_id){
        $area = Area::where('id', $area_id)->where('status', 1)->first();
        return $area->area;
    }

   /* public function getBookedAreaId($fromDate, $toDate, $model_id, $city_id){
        return DB::table('bikes')
        ->join('bookings', 'bikes.id', '=', 'bookings.bike_id')
        ->join('areas', 'bikes.area_id', '=', 'areas.id')
        ->select('bikes.area_id')
        ->where('bookings.from_datetime', $fromDate)
        ->where('bookings.from_datetime', $toDate)
        ->where('bikes.model_id', $model_id)
        ->where('areas.city_id', $city_id)
        ->get();
    }*/

    public function perDayPrice($from_date, $to_date, $total_price){

        $from_ts = strtotime($from_date);
        $to_ts = strtotime($to_date);
        $diff_ts = $to_ts - $from_ts;
        $no_of_days = floor($diff_ts/(60*60*24));

        return $total_price/$no_of_days;
    }

    public function checkAllAreasBooked($model_id,$city_id,$from_date,$to_date){

        $areas = $this->get_areas_by_model_id($model_id,$city_id,$from_date,$to_date);

        foreach ($areas as $area) {
            
            if($area->bikeAvailability == true)
                return false;
        }

        return true;
    }

    public function getDate(Request $request){

        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $user_id = Session::get('user_id');

        $fm=date('Y-m-d', strtotime($fromDate));
        $to=date('Y-m-d', strtotime($toDate));
        

       

        $city_id = Session::get('current_cityId');
       /* if($city_id)
            $city_id = $city_id;
        else
            $city_id = "1";*/
        if(!$city_id)
            $city_id = "1";

       // $all_areas = Area::where('city_id',$city_id)->get();
        
        $get_all_models_with_detail = \DB::table('bikes')
            ->select('bikes.*','bike_models.*')
            ->join('areas', 'areas.id', '=', 'bikes.area_id')
            ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->where('areas.city_id', $city_id)
            ->where('bike_models.status', 1)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            ->groupBy('bikes.model_id')         
            ->get();

        // For each model add list of areas that model is available in.
        foreach($get_all_models_with_detail as $model){               
            $model->areas = FrontPagesController::get_areas_by_model_id($model->model_id,$city_id, $fm, $to);
            $model->pid = $this->getPriceId($model->model_id, $model->area_id);       
            $model->totalPrice = $this->getPriceByDate($fromDate,$toDate,$model->pid); //FrontPagesController::get_areas_by_model_id($model->model_id,$city_id);       
            $model->weekDayPrice = $this->getWeekDayPrice($model->pid);
            $model->fromDate = $fromDate;
            $model->toDate = $toDate;
            $model->userId = $user_id;
            $model->makeName = $this->getBikeMakeName($model->bike_make_id);
            $model->areaName = $this->getAreaName($model->area_id);
            $model->perDayPrice = $this->perDayPrice($fromDate, $toDate, $model->totalPrice);
            $model->allAreasBooked = $this->checkAllAreasBooked($model->model_id, $city_id, $fm, $to);
           
            //$model->bookedAid = $av;
        } 

        return $get_all_models_with_detail;
        //return $d1;
        /*return Response::json(array(
                'details' => $get_all_models_with_detail,
                'd1' => $d1
            ));*/
    }

    public function getPid(Request $request){
        $model_id = $request->model_id;
        $area_id = $request->area_id;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $pid = $this->getPriceId($model_id, $area_id);

        //$weekDayPrice = $this->getWeekDayPrice($pid);
        $totalPrice = $this->getPriceByDate($fromDate,$toDate,$pid);
        $areaName = $this->getAreaName($area_id);
        $perDayPrice = $this->perDayPrice($fromDate, $toDate, $totalPrice);
        
        return Response::json(array(
                //'weekDayPrice' => $weekDayPrice,
                'perDayPrice' => $perDayPrice,
                'totalPrice' => $totalPrice,
                'areaName' => $areaName
            ));
    }
    
    public function salesReport(Request $request)
    {
        if(isset($request->start_date)&&isset($request->end_date))
        {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $state = $request->state;
            
            $dispatcher = app('Dingo\Api\Dispatcher');
            $reportData = $dispatcher->with(['data_start_date' => $request->start_date,'data_end_date' => $request->end_date,'state'=>$state])->get('getReportDataJson');
            
            
        }
        else
        {
            $start_date="";
            $end_date = "";
            $reportData = "{}";
        }
        return view('admin.reportView.index',compact('reportData','start_date','end_date'));
    }
    
    public function salesReportChart(Request $request)
    {
        if(isset($request->start_date)&&isset($request->end_date))
        {
            if($request->end_date!="")
            {
                $start_date = $request->start_date;
                $end_date = $request->end_date;

                $dispatcher = app('Dingo\Api\Dispatcher');
                $reportData = $dispatcher->with(['data_start_date' => $request->start_date,'data_end_date' => $request->end_date])->get('getReportChart');

                $mainSeriesCount = $reportData['mainSeriesCount'];
                $drillDownSeriesCount = $reportData['drillDownSeriesCount'];
                
                $mainSeriesAmount = $reportData['mainSeriesAmount'];
                $drillDownSeriesAmount = $reportData['drillDownSeriesAmount'];
            }
            else
            {
                $start_date = "";
                $end_date = "";
                $mainSeriesCount = "";
                $drillDownSeriesCount = "";
                
                $mainSeriesAmount = "";
                $drillDownSeriesAmount = "";
                
            }
            
     
        }
        else
        {
            $start_date = "";
            $end_date = "";
            $mainSeriesCount = "";
            $drillDownSeriesCount = "";
            
            $mainSeriesAmount = "";
            $drillDownSeriesAmount = "";
        }
         
        return view('admin.salesReportChart.index',compact('mainSeriesCount','drillDownSeriesCount','start_date','end_date','mainSeriesAmount','drillDownSeriesAmount'));
    }
}
