<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Bookings;
use Session;
use App\Models\Bikation_Address;
use App\Models\Bikation_Trip;
use App\Models\Bikation_Package;
use App\Models\User;
use Auth;
use Mail;

class Bikation_Bookings_AdminController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
   /* if(Session::has('email') && Session::has('First_Name') ){*/
	
	  $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->get();
	   $selected="";
      if (isset($request->btn_submit)) {
         $bikation_bookingsList = Bikation_Bookings::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_bookingsList = Bikation_Bookings::orderBy('Booking_ID','desc')->limit(100)->get();
        $selected="";
      }
	  
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Approve_By_Admin', 1)->get();
      $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name')->where('Status', 1)->get();
      foreach ($bikation_bookingsList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name','Trip_Start_Date','Trip_End_Date','Start_Location','End_Location','Total_Distance','Total_riding_hours','No_of_stops')->where('Trip_ID',$model->Trip_ID)->first();
        $model['Trip_Name'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
		$model['Trip_Start_Date'] = ($Trip_Name) ? $Trip_Name->Trip_Start_Date : '';
		$model['Trip_End_Date'] = ($Trip_Name) ? $Trip_Name->Trip_End_Date : '';
		$model['Start_Location'] = ($Trip_Name) ? $Trip_Name->Start_Location : '';
		$model['End_Location'] = ($Trip_Name) ? $Trip_Name->End_Location : '';
		$model['Total_Distance'] = ($Trip_Name) ? $Trip_Name->Total_Distance : '';
		$model['Total_riding_hours'] = ($Trip_Name) ? $Trip_Name->Total_riding_hours : '';
		$model['No_of_stops'] = ($Trip_Name) ? $Trip_Name->No_of_stops : '';
      }
      foreach ($bikation_bookingsList as $model) {
        $Package_Name= DB::table('bikation_package')->select('name')->where('Package_ID',$model->Package_ID)->first();
        $model['PackageName'] = ($Package_Name) ? $Package_Name->name : '';
      }
	   foreach ($bikation_bookingsList as $model) {
		   	  // if($model->Booked_Type	 != 'Bikation_Vender'){
		
				$Customer_Name= DB::table('users')->select('first_name','last_name','email','mobile_num','location')->where('id',$model->Customer_ID)->first();
				$model['first_name'] = ($Customer_Name) ? $Customer_Name->first_name : '';
				$model['last_name'] = ($Customer_Name) ? $Customer_Name->last_name : '';
				$model['email'] = ($Customer_Name) ? $Customer_Name->email : '';
				$model['mobile_num'] = ($Customer_Name) ? $Customer_Name->mobile_num : '';
				$model['location'] = ($Customer_Name) ? $Customer_Name->location : '';
			  /* }
			   else{
				$Customer_Name= DB::table('bikationvendor_users')->select('first_name','last_name','email','mobile_num')->where('id',$model->Customer_ID)->first();
				$model['first_name'] = ($Customer_Name) ? $Customer_Name->first_name : '';
				$model['last_name'] = ($Customer_Name) ? $Customer_Name->last_name : '';
				$model['email'] = ($Customer_Name) ? $Customer_Name->email : '';
				$model['mobile_num'] = ($Customer_Name) ? $Customer_Name->mobile_num : '';
				$model['location'] =  '';
			 
			   }*/
		}
			 
	  //echo '<pre>';
	  //print_r($bikation_bookingsList);exit;
      return view('bikationvendor.admin.booking',compact('bikation_bookingsList','bikation_packageList','bikation_tripList','bikation_AlltripList','selected'));
    /*}
    else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
  public function addBookings(Request $request){
	  
   if(Auth::check()) {
	   
    $rules = array('Trip_ID' => 'required',
                    'Package_ID' => 'required',
                    'No_of_Tickets_Cost_1px' => 'required',
                    'No_of_Tickets_Cost_2px' => 'required',
                    'Package_Cost' => 'required',
                    'Total_Cost' => 'required'                   // 'Vehicle' => 'required',
                  //  'Addon_cost' => 'required',
                    //'Status' => 'required'
                  );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('admin/bikation-bookings')->withErrors($validator->errors());
      }
      else{
		  
		  $email=$request->Cust_email;
		  $email_check=  DB::select("select * from users where email='$email'");
          $length = 5;
            $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
		  
		  if(count($email_check) == 0)
		  {
			  $inseart= User::create(['first_name' => $request->Cust_Name, 'email' => $request->Cust_email,'mobile_num' => $request->Cust_mob, 'referral_code'=>$referral_code]);
			 $Customer_ID= $inseart->id;
		  }
		  else
		  {
			  $Customer_ID = $email_check['0']->id;
		  }
	    
		  
        $Booked_On = new DateTime();
        $BookedOn= $Booked_On->format('Y-m-d H:i:s');
        $Cancelled_On = new DateTime();
        $CancelledOn= $Cancelled_On->format('Y-m-d H:i:s');
		//$Vendor_ID= Session::get('Vendor_ID');
        $Vendor_ID= 0;
        $packages_seats= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();

        if($packages_seats->Tickes_Remaining_Cost_1px < $request->No_of_Tickets_Cost_1px){
           return redirect('admin/bikation-bookings')->withErrors('Sorry only  '.$packages_seats->Tickes_Remaining_Cost_1px.' tickets available for Tickes Remaining Cost 1px');
        }
        if($packages_seats->Tickes_Remaining_Cost_2px < $request->No_of_Tickets_Cost_2px){
           return redirect('admin/bikation-bookings')->withErrors('Sorry only  '.$packages_seats->Tickes_Remaining_Cost_2px.' tickets available for Tickes Remaining Cost 2px');
        }
        $available_Tickes_Remaining_Cost_1px = $packages_seats->Tickes_Remaining_Cost_1px - $request->No_of_Tickets_Cost_1px;
        $available_Tickes_Remaining_Cost_2px = $packages_seats->Tickes_Remaining_Cost_2px - $request->No_of_Tickets_Cost_2px;

       Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $available_Tickes_Remaining_Cost_1px,'Tickes_Remaining_Cost_2px'=>$available_Tickes_Remaining_Cost_2px]);
        
        $booking_insert = Bikation_Bookings::create(['Trip_ID' => $request->Trip_ID,'Customer_ID' => $Customer_ID,'Vendor_ID' => $Vendor_ID, 'Package_ID' => $request->Package_ID,'Booked_On' => $BookedOn,'Booked_By' => $request->Booked_By,'No_of_People'=>$request->No_of_People,'No_of_Tickets_Cost_1px'=>$request->No_of_Tickets_Cost_1px,'No_of_Tickets_Cost_2px'=>$request->No_of_Tickets_Cost_2px,'Package_Cost' => $request->Package_Cost, 'Addon_cost' => $request->Addon_cost,'Total_Cost' => $request->Total_Cost,'Cancelled_On' => $CancelledOn,'Cancelled_By'=>$request->Cancelled_By,'Vehicle' => $request->Vehicle,'Additional_Remarks' => $request->Additional_Remarks,'Payment_status' => 1,'book_status' => 'Booking','Booked_Type'=>$request->Booked_Type]);
		
		$bikation_trip_booking = DB::table('bikation_trip')->select('Trip_Name','Trip_Start_Date','Trip_End_Date','Start_Location','Trip_Description')->where('Trip_ID',$request->Trip_ID)->first();
		$bikation_booking_user= DB::table('users')->select('email','first_name','last_name','mobile_num')->where('id', $Customer_ID)->first();
		$bikation_img_booking = DB::table('bikation_trip_photo')->select('Media_URL_details')->where('Trip_ID',$request->Trip_ID)->first();
		/* Email Sending */
		$data = ['Booking_ID'=>$booking_insert->id, 'fname'=>$bikation_booking_user->first_name .' '.$bikation_booking_user->last_name, 'email'=>$bikation_booking_user->email, 'mob'=>$bikation_booking_user->mobile_num, 'trip_name'=>$bikation_trip_booking->Trip_Name, 'start_date'=>$bikation_trip_booking->Trip_Start_Date, 'end_date'=>$bikation_trip_booking->Trip_End_Date, 'meeting_loc'=>$bikation_trip_booking->Start_Location, 'cust_inst'=>$bikation_trip_booking->Trip_Description, 'price'=>$request->Total_Cost,'image'=>'http:'.$bikation_img_booking->Media_URL_details];
		
		$email = $bikation_booking_user->email;

			Mail::send('emails.bikation_user_booking', $data, function ($message) use ($email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($email)->subject('Booking Details');
                });
					$adm_email = env('BIKATION_ADMIN_EMAIL');
				Mail::send('emails.bikation_user_booking', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('Booking Details');
                });
				
        return redirect('admin/bikation-bookings');
      }
    }
    else{
	  return redirect('admin/login')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($Booking_ID,Request $request){
  /*if(Session::has('email') && Session::has('First_Name') ){*/
   $rules = array('Trip_ID' => 'required',
                  'Package_ID' => 'required',
                    'No_of_Tickets_Cost_1px' => 'required',
                    'No_of_Tickets_Cost_2px' => 'required',
                    'Package_Cost' => 'required',
                    'Total_Cost' => 'required',
                    'Vehicle' => 'required',
                    'Addon_cost' => 'required',
                    'Status' => 'required'
                  );
  $validator = Validator::make($request->all(),$rules);
  if($validator->fails()){
    return redirect('admin/bikation-bookings')->withErrors($validator->errors());
  }
  else{
        $Booked_On = new DateTime();
        $BookedOn= $Booked_On->format('Y-m-d H:i:s');
        $Cancelled_On = new DateTime();
        $CancelledOn= $Cancelled_On->format('Y-m-d H:i:s');
        $Vendor_ID= Session::get('Vendor_ID');
        $old_No_of_People= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();
        
        $old_No_of_People_bookings= DB::table('bikation_bookings')->select('No_of_Tickets_Cost_1px','No_of_Tickets_Cost_2px')->where('Trip_ID',$request->Trip_ID)->where('Booking_ID',$Booking_ID)->first();


       if($old_No_of_People_bookings->No_of_Tickets_Cost_1px > $request->No_of_Tickets_Cost_1px)
    {
      $diffrence = $old_No_of_People_bookings->No_of_Tickets_Cost_1px - $request->No_of_Tickets_Cost_1px;
      $new_update_No_of_People_cost_1px =   $old_No_of_People->Tickes_Remaining_Cost_1px + $diffrence;
    }
    else{ 
      $diffrence = $request->No_of_Tickets_Cost_1px - $old_No_of_People_bookings->No_of_Tickets_Cost_1px;
      $new_update_No_of_People_cost_1px =   $old_No_of_People->Tickes_Remaining_Cost_1px - $diffrence;
      
    }
     if($old_No_of_People_bookings->No_of_Tickets_Cost_2px > $request->No_of_Tickets_Cost_2px)
    {
      $diffrence = $old_No_of_People_bookings->No_of_Tickets_Cost_2px - $request->No_of_Tickets_Cost_2px;
      $new_update_No_of_People_cost_2px =   $old_No_of_People->Tickes_Remaining_Cost_2px + $diffrence;
    }
    else{ 
      $diffrence = $request->No_of_Tickets_Cost_2px - $old_No_of_People_bookings->No_of_Tickets_Cost_2px;
      $new_update_No_of_People_cost_2px =   $old_No_of_People->Tickes_Remaining_Cost_2px - $diffrence;
      
    }
    
  if($new_update_No_of_People_cost_1px < 0)
  {
   return redirect('admin/bikation-bookings')->withErrors('Sorry only  '.$old_No_of_People->Tickes_Remaining_Cost_1px.' tickets available for Cost 1px');
  }

  if($new_update_No_of_People_cost_2px < 0)
  {
   return redirect('admin/bikation-bookings')->withErrors('Sorry only  '.$old_No_of_People->Tickes_Remaining_Cost_2px.' tickets available for Cost 2px');
  }

     Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $new_update_No_of_People_cost_1px,'Tickes_Remaining_Cost_2px'=>$new_update_No_of_People_cost_2px]);
    Bikation_Bookings::where('Booking_ID', $Booking_ID)
    ->update(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'Customer_ID' => $request->Customer_ID, 'Package_ID' => $request->Package_ID,'Booked_On' => $BookedOn,'Booked_By' => $request->Booked_By,'No_of_People'=>$request->No_of_People,'No_of_Tickets_Cost_1px'=>$request->No_of_Tickets_Cost_1px,'No_of_Tickets_Cost_2px'=>$request->No_of_Tickets_Cost_2px,'Status' => $request->Status,'Package_Cost' => $request->Package_Cost, 'Addon_cost' => $request->Addon_cost,'Total_Cost' => $request->Total_Cost,'Cancelled_On' => $CancelledOn,'Cancelled_By'=>$request->Cancelled_By,'Vehicle' => $request->Vehicle,'Additional_Remarks' => $request->Additional_Remarks]);
    return redirect('admin/bikation-bookings');
    }
 /* }
  else{
    return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
  }*/
  }
  public function delete($Booking_ID,Request $request){
    /*if(Session::has('email') && Session::has('First_Name') ){*/
       $No_of_People_bookings= DB::table('bikation_bookings')->select('No_of_People','No_of_Tickets_Cost_1px','No_of_Tickets_Cost_2px')->where('Booking_ID',$Booking_ID)->first();
      $packages_seats= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();
      $delete_seats_Cost_1px=  $packages_seats->Tickes_Remaining_Cost_1px + $No_of_People_bookings->No_of_Tickets_Cost_1px;
      $delete_seats_Cost_2px=  $packages_seats->Tickes_Remaining_Cost_2px + $No_of_People_bookings->No_of_Tickets_Cost_2px;
      
       Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $delete_seats_Cost_1px,'Tickes_Remaining_Cost_2px'=>$delete_seats_Cost_2px]);
       $packages = Bikation_Bookings::where('Booking_ID', '=', $Booking_ID)->delete();
      return redirect('admin/bikation-bookings');
    /*}else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
  
   public function cancel($Booking_ID,Request $request){
    /*if(Session::has('email') && Session::has('First_Name') ){*/
      /* $No_of_People_bookings= DB::table('bikation_bookings')->select('No_of_People','No_of_Tickets_Cost_1px','No_of_Tickets_Cost_2px')->where('Booking_ID',$Booking_ID)->first();
      $packages_seats= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();
      $delete_seats_Cost_1px=  $packages_seats->Tickes_Remaining_Cost_1px + $No_of_People_bookings->No_of_Tickets_Cost_1px;
      $delete_seats_Cost_2px=  $packages_seats->Tickes_Remaining_Cost_2px + $No_of_People_bookings->No_of_Tickets_Cost_2px;
      
       Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $delete_seats_Cost_1px,'Tickes_Remaining_Cost_2px'=>$delete_seats_Cost_2px]);*/
	   Bikation_Bookings::where('Booking_ID',$Booking_ID)->update(['book_status' => 'Cancel']);
       //$packages = Bikation_Bookings::where('Booking_ID', '=', $Booking_ID)->delete();
      return redirect('admin/bikation-bookings');
    /*}else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
  
    
}
