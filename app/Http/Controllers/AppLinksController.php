<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Area;
use App\Models\City;
use App\Models\BikeModel;
use App\Models\Bike;
use App\Models\Price;
use App\Models\Faq;
use App\Models\AppNotification;
use Session;
use App\Models\Parameters;
use DB;




class AppLinksController extends AppController
{
    
    public function faq(){

        $faqs = Faq::all();
        
    	return view('front/app/faq',compact('faqs'));
    }
    
    public function tariff(Request $request){

        if(!$request->city_id)
            $cityId = "1";
        else
            $cityId = $request->city_id;
        
        $areas = Area::where('city_id',$cityId)->where('status',1)->get()->lists('id');
        $modelIDs = Bike::whereIn('area_id',$areas)->where('status',1)->get()->lists('model_id');
        $modelIDs=$modelIDs->toArray();
        $modelIDs = array_unique($modelIDs);
        
        $bikeModels=BikeModel::whereIn('id',$modelIDs)->get();
        
        foreach ($bikeModels as $bikeModel)
        {
          
            $price = Price::where('model_id',$bikeModel->id)->whereIn('area_id',$areas)->orderBy('id','desc')->first();
            if(!$price)
            {
                $price = Price::where('model_id',$bikeModel->id)->orderBy('area_id','asc')->first();
            }
            
            $bikeModel['price_per_hour'] = $price->price_per_hour;
            $bikeModel['price_per_day'] = $price->price_per_hour*24;
            
            $bikeModel['hour_weekday'] = $price['hour_weekday'];
            $bikeModel['hour_weekend'] = $price['hour_weekend'];
            $bikeModel['day_weekday'] = $price['day_weekday'];
            $bikeModel['day_weekend'] = $price['day_weekend'];
            $bikeModel['five_weekday'] = $price['five_weekday'];
            $bikeModel['week'] = $price['week'];
            $bikeModel['month'] = $price['month'];
            
            $bikeModel['minimum_hours'] = $price->minimum_hours;
            $bikeModel['minimum_price'] = $price->minimum_hours*$price->hour_weekday;
            
            $bikeModel['bike_make_name'] = $bikeModel->getBikeMakeName($bikeModel->bike_make_id);
            
            
            
        }

        return view('front/app/tariff',compact('bikeModels','cityId'));
    }
    
    public function aboutUs()
    {
        return view('front/app/aboutUs');
    }
    
    public function reviews()
    {
        return view('front/app/reviews');
    }
    
    public function contactUs()
    {
        return view('front/app/contactUs');
    }
    
    public function docsReq(Request $request)
    {
        if(isset($request->a2b))
            return view('front/app/docsReqA2b');
        else
            return view('front/app/docsReq');
    }
    
    public function notification($url_path, Request $request)
    {
        $appNotification = AppNotification::where('url_path',$url_path)->first();
        if(!$appNotification)
            return redirect(env('IMAGEURL'));
        
    	return view('front/app/notification',compact('appNotification'));
    }
    
    
    
    
    public function indexAppNotification(Request $request)
    {
        $pageTitle = "App Notification";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>80,"title"=>"URL"],
                                            ["width"=>200,"title"=>"Title"],
                                            ["width"=>80,"title"=>"Offer"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $appNotificationObjects = AppNotification::all();
        
        $objectTableArray['body'] = [];
        
        foreach($appNotificationObjects as $appNotificationObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$appNotificationObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$appNotificationObject->url_path,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$appNotificationObject->title,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>$appNotificationObject->is_offer?"Yes":"No","pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_type_btn",
                                                            "id"=>"edit_accessories_type_id",
                                                            "data_id"=>$appNotificationObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_type_btn",
                                                            "id"=>"delete_accessories_type_id",
                                                            "data_id"=>$appNotificationObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_type_modal";
        $addObject['add_button_text'] = "Add Type";
        $addObject['modal_title'] = "Add Accessories Type";
        $addObject['add_url'] = "/admin/app-notification/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"URL Path","input_type"=>"text","name"=>"url_path_add"],
                ["type"=>"input","label"=>"Title","input_type"=>"text","name"=>"title_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"input","label"=>"Button Text","input_type"=>"text","name"=>"button_text_add"],
                ["type"=>"input","label"=>"Button Link","input_type"=>"text","name"=>"button_link_add"],
                ["type"=>"select","label"=>"Include in Offer Page", "name"=>"is_offer_add", "id"=>"is_offer_id_add", "default_value"=>"not_set", "default_text"=>"-- Select --", "select_items"=>[["text"=>"No","value"=>0],["text"=>"Yes","value"=>1]]],
                ["type"=>"image","label"=>"Image","name"=>"image_add"],
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_type_modal";
        $editObject['edit_btn_class'] = "edit_accessories_type_btn";
        $editObject['modal_title'] = "Edit Accessories Type";
        $editObject['edit_url'] = "/admin/app-notification/edit";
        $editObject['ajax_url'] = "/admin/app-notification/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"URL Path","input_type"=>"text","name"=>"url_path_edit"],
                ["type"=>"input","label"=>"Title","input_type"=>"text","name"=>"title_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"input","label"=>"Button Text","input_type"=>"text","name"=>"button_text_edit"],
                ["type"=>"input","label"=>"Button Link","input_type"=>"text","name"=>"button_link_edit"],
                ["type"=>"select","label"=>"Include in Offer Page", "name"=>"is_offer_edit", "id"=>"is_offer_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select --", "select_items"=>[["text"=>"No","value"=>0],["text"=>"Yes","value"=>1]]],
                ["type"=>"image","label"=>"Image","name"=>"image_edit","old_image_id"=>"old_full_id_edit"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_type_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_type_btn";
        $deleteObject['modal_title'] = "Delete Accessories Type";
        $deleteObject['delete_url'] = "/admin/app-notification/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Type?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAppNotification(Request $request)
    {
        $notificationImg = $request->file('image_add');
        $notificationImgId = null;
        if($notificationImg)
        {
          $notificationImgExt = $notificationImg->guessClientExtension();
          $notificationImgFn = $notificationImg->getClientOriginalName();

          $notificationImgHashName = $request->url_path_add."-".rand(0,99).'.'.$notificationImgExt;
          $notificationImgDestinationPath = 'img/slider/';
          $notificationImg->move($notificationImgDestinationPath, $notificationImgHashName);

          $notificationImgUrl = env('HTTPSORHTTP').'://'.env('IMAGEURL').'/'.$notificationImgDestinationPath.$notificationImgHashName;

            
        }
        
        AppNotification::create([
                'url_path'=>$request->url_path_add, 
                'image_url'=>$notificationImgUrl, 
                'title'=>$request->title_add, 
                'description'=>$request->description_add, 
                'button_text'=>$request->button_text_add, 
                'button_link'=>$request->button_link_add,
                'is_offer'=>$request->is_offer_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsAppNotification(Request $request)
    {
        $appNotification = AppNotification::where('id',$request->id)->first();
        $image_html = "<img src=\"".$appNotification->image_url."\" width=\"90px\">";
        $data = [
                ["type"=>"input","name"=>"url_path_edit","value"=>$appNotification->url_path],
                ["type"=>"input","name"=>"title_edit","value"=>$appNotification->title],
                ["type"=>"textarea","name"=>"description_edit","value"=>$appNotification->description],
                ["type"=>"image","imgId"=>"old_full_id_edit","html"=>$image_html],
                ["type"=>"input","name"=>"button_text_edit","value"=>$appNotification->button_text],
                ["type"=>"input","name"=>"button_link_edit","value"=>$appNotification->button_link],
                ["type"=>"select","name"=>"is_offer_edit","value"=>$appNotification->is_offer]

        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAppNotification($id, Request $request)
    {
        $appNotification = AppNotification::where('id',$id)->first();
        if(!$appNotification)
            return redirect()->back()->withErrors(["Could not find the app notification."]);
        
        $oldNotificationImgUrl = $appNotification->image_url;
        $notificationImg = $request->file('notification_edit');

        if(isset($notificationImg))
        {
            $notificationImgExt = $notificationImg->guessClientExtension();
            $notificationImgFn = $notificationImg->getClientOriginalName();

            $notificationImgHashName = $request->url_path_edit."-".rand(0,99).'.'.$notificationImgExt;
            $notificationImgDestinationPath = 'img/slider/';

            if(file_exists(public_path($notificationImgDestinationPath.$notificationImgHashName)))
            {
               unlink(public_path($notificationImgDestinationPath.$notificationImgHashName));
            }

            $notificationImg->move($notificationImgDestinationPath, $notificationImgHashName);

            $notificationImgUrl =  env('HTTPSORHTTP').'://'.env('IMAGEURL').'/'.$notificationImgDestinationPath.$notificationImgHashName;

        }
        else
        {
              $notificationImgUrl = $oldNotificationImgUrl;
        }
        
        AppNotification::where('id',$id)->update([
            'url_path'=>$request->url_path_edit, 
            'image_url'=>$notificationImgUrl, 
            'title'=>$request->title_edit, 
            'description'=>$request->description_edit, 
            'button_text'=>$request->button_text_edit, 
            'button_link'=>$request->button_link_edit,
            'is_offer'=>$request->is_offer_edit 
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAppNotification($id, Request $request)
    {
        $appNotification = AppNotification::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($appNotification),"table_name"=>"app_notification_page","deleted_by"=>Auth::user()->id]);
        AppNotification::where('id',$id)->delete();
        return redirect()->back();
    }
    
    public function getNotificationElements(Request $request)
    {
        $appNotification = AppNotification::where('url_path',$request->url_path)->first();
        if(!$appNotification)
        {
            $data= [
                "status"=>false,
                "image"=>"",
                "title"=>"",
                "description"=>"",
                "button"=>false,
                "button_text"=>"",
                "button_link"=>""
            ];
        }
        else
        {
            if($appNotification->button_text == "")
            {
                $buttonBool = false;
            }
            else
            {
                $buttonBool = true;
            }
            
            $description = str_replace("<br>","\r\n",$appNotification->description);
            $description = str_replace("&nbsp;"," ",$description);
            
            $data= [
                "status"=>true,
                "image"=>stripslashes($appNotification->image_url),
                "title"=>$appNotification->title,
                "description"=>$description,
                "button"=>$buttonBool,
                "button_text"=>$appNotification->button_text,
                "button_link"=>$appNotification->button_link
            ];
        }            
        return $this->respondWithSuccess($data);
        
    }
    
    public function getTPOffers(Request $request){
        
        $data = [];
        
        $appNotifications = AppNotification::where('is_offer',1)->orderBy('id','desc')->get();
        
        foreach($appNotifications as $appNotification)
        {
            if($appNotification->button_text == "")
            {
                $buttonBool = false;
            }
            else
            {
                $buttonBool = true;
            }
            
            $description = str_replace("<br>","\r\n",$appNotification->description);
            $description = str_replace("&nbsp;"," ",$description);
            
            array_push($data,[
                "status"=>true,
                "image"=>stripslashes($appNotification->image_url),
                "title"=>$appNotification->title,
                "description"=>$description,
                "button"=>$buttonBool,
                "button_text"=>$appNotification->button_text,
                "button_link"=>$appNotification->button_link
            ]);
        }            
        return $this->respondWithSuccess($data);
    }
    
    public function appVersionCheck(Request $request)
    {
        $appVersions = json_decode(Parameters::getParameter('app_version'));
        $appVersionsArr = (array)$appVersions;
        $appVersionsArr['additional_info'] = array(["id"=>"fop_readiness","text"=>"Earn with Us","display"=>false]);
        return $this->respondWithSuccess($appVersionsArr);
    }
    
    public function feedbackSubmit(Request $request)
    {
        if($request->header()['authorization']==[""])
        {
            $userId = 0;
        }
        else
        {
            if($request->header()['authorization']==["Bearer"])
            {
                $userId = 0;
            }
            else
            {
                $user = UserController::getUserByToken();
                $userId = $user->id;
            }
        }
        
        DB::table('app_feedback')->insert([
            "user_id"=>$userId,
            "feedback"=>$request->feedback,
            "platform"=>$request->platform,
            "device"=>$request->device,
            "os_version"=>$request->os_version,
            "app_version"=>$request->app_version
        ]);
        
        return $this->respondWithSuccess(["message"=>"Feedback received. Thanks!!"]);
        
    }
    
    public function getTutorialImagesForA2b(Request $request)
    {
        
    }
    
}
