<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use App\Models\AccessoriesAvailable;
use App\Models\AccessoriesCapacity;
use App\Models\AccessoriesOccupancy;
use App\Models\AccessoriesService;
use App\Models\User;
use App\Models\Accessories;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\AccessoriesModel;
use App\Models\Area;
use App\Models\AccessoriesSize;
use App\Models\City;
use DB;
use Mail;
use App\Transformers\BookingTransformer;
use Carbon\Carbon;
use Auth;

class AccessoriesAvailabilityController extends AppController
{
   
   
    
       
    
    public static function addOrDeleteAccessoriesBooking($id,$nature){
        
        
        if($nature == "add")
        {
            $natureBool = 1;
        }
        else
        {
            $natureBool = 0;    
        }
        
        $booking = Booking::where('id',$id)->first();
        
        
        if(!$booking)
        {
            return ["error"=>"No booking found"];
        }
        $startDateTimeCarbon = Carbon::parse($booking->start_datetime);
        $endDateTimeCarbon = Carbon::parse($booking->end_datetime);
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        
        $availableObjs = AccessoriesAvailable::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();
        
        
        
        
        $availableHourStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {
            $availableHourStr.=$availableObj->hour_string.",";             
        }
        
        
        
        $availableArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$availableHourStr,!$natureBool,$booking->area_id,$booking->model_id,$dates);
        
        
        
        AccessoriesAvailable::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
        
        
        
        DB::table('accessories_available')->insert($availableArray); 
          
        $nonRevenueIds = explode(',',env('NONREVENUEIDS'));
        
        if(in_array($booking->user_id,$nonRevenueIds))
        {
            
            $serviceObjs = AccessoriesService::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

            $serviceHourStr = "";
             
            foreach($serviceObjs as $serviceObj)
            {
                $serviceHourStr.=$serviceObj->hour_string.",";             
            }
            
            
            $serviceArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$serviceHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);
            
            AccessoriesService::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
       
            DB::table('accessories_service')->insert($serviceArray);
            
            
        }
        else
        {
            $occupancyObjs = AccessoriesOccupancy::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

            $occupancyHourStr = "";


            foreach($occupancyObjs as $occupancyObj)
            {
                $occupancyHourStr.=$occupancyObj->hour_string.",";             
            }
            
            $occupancyArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$occupancyHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);
            
            AccessoriesOccupancy::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
        
            DB::table('accessories_occupancy')->insert($occupancyArray); 
            
        }
        
        
        
        
        
        DB::table('booking_add_delete_request')->insert(['booking_id'=>$id,'nature'=>$nature,'area_id'=>$booking->area_id,'model_id'=>$booking->model_id,'action'=>'add_delete_booking']);
        
        return "done";
        
    }
    
    public static function modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature,$area_id,$model_id,$size_id,$dates)
    {
        if($nature === "No Change")
        {
            $returnDateStr = $dateStr;
        }
        else
        {
            $returnDateStr = Self::modifyDateStr($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature);
        }
        $returnDateArray = array_chunk(explode(',',$returnDateStr), 24);
        $i=0;
        $AMHObjArray = [];
        foreach($dates as $date)
        {
            array_push($AMHObjArray,['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'size_id'=>$size_id,'hour_string'=>implode(',',$returnDateArray[$i])]);
            $i++;
        }
        return $AMHObjArray;
    }
    
    public static function modifyDateStr($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature)
    {
        $dateArray = explode(',',$dateStr);    
        
        $i = $startDateTimeCarbon->hour;
                
        for($date = $startDateTimeCarbon->copy()->addHour(); $date->lte($endDateTimeCarbon); $date->addHour()) 
        {
            
            if($nature == 1)
            {
                $dateArray[$i]++;
            }
            else
            {
                $dateArray[$i]--;
            }
            $i++;
            
        }
        $dateStrReturn = implode(',',$dateArray);
        
        return $dateStrReturn;
    }
    
    
    public static function addOrDeleteAccessories($area_id,$model_id,$size_id,$nature,$startDateTimeCarbon,$endDateTimeCarbon)
    {
        
        if($nature == "add")
        {
            $natureBool = 1;
        }
        else
        {
            $natureBool = 0;
        }
        
        $noEndDate = true;
        
        if(gettype($startDateTimeCarbon) == "integer" && gettype($endDateTimeCarbon) == "integer")
        {
            
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay();
            
            $currentTimeCarbon = Carbon::now();
            
        }
        else
        {
            $currentTimeCarbon = $startDateTimeCarbon->copy();
            
            $endDateTimeCarbonCompare = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbonCompare->addDay()->addHours(-1);   
            
            
            if($endDateTimeCarbon->lt($endDateTimeCarbonCompare))
            {
                $noEndDate = false;
                
            }

        }
        
        $startDateTimeCarbon = Carbon::parse(env('STARTDATE'));

        
        $capacity = AccessoriesCapacity::where('area_id',$area_id)->where('model_id',$model_id)->where('size_id',$size_id)->orderBy('date','asc')->get();
        
        
        
        $fullOneHourArray = [];
        $fullZeroHourArray = [];
        
        
        if(count($capacity)==0)
        {
            if($nature != "add")
            {
                return ['error'=>'No Accessories found to delete'];
            }
            
            $moveEndDateTimeCarbon = $endDateTimeCarbon->copy();
                
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay();
            
            $endDateTimeCarbon = $endDateTimeCarbon->addHour(-1);
            
            
            for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
            {
                $dates[] = $date->format('Y-m-d');
            }
            
            for($date = $startDateTimeCarbon->copy(); $date->lte($currentTimeCarbon); $date->addHour()) 
            {
                array_push($fullOneHourArray,0);
                array_push($fullZeroHourArray,0);
            }
            if($noEndDate)
            {
                for($date = $currentTimeCarbon->copy(); $date->lte($endDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,1);
                    array_push($fullZeroHourArray,0);
                }
            }
            else
            {
                $moveEndDateTimeCarbon->addHours(-1);
                
                for($date = $currentTimeCarbon->copy(); $date->lte($moveEndDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,1);
                    array_push($fullZeroHourArray,0);
                }
                                
                for($date = $moveEndDateTimeCarbon->copy(); $date->lte($endDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,0);
                    array_push($fullZeroHourArray,0);
                }
            }
            
                        
            $fullOneStr = implode(',',$fullOneHourArray);
            $fullZeroStr = implode(',',$fullZeroHourArray);
            
            
            $oneAMHObj =  Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$fullOneStr,"No Change",$area_id,$model_id,$size_id,$dates);
            $zeroAMHObj =  Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$fullZeroStr,"No Change",$area_id,$model_id,$size_id,$dates);
            
            
            DB::table('accessories_capacity')->insert($oneAMHObj);
            DB::table('accessories_available')->insert($oneAMHObj);
            DB::table('accessories_service')->insert($zeroAMHObj);
            DB::table('accessories_occupancy')->insert($zeroAMHObj);
            
            
        }
        else
        {   
            $currentTimeCarbon = $currentTimeCarbon->addHour(1);
            
            for($date = Carbon::parse($currentTimeCarbon->format('Y-m-d')); $date->lt($endDateTimeCarbon); $date->addDay()) 
            {
                $dates[] = $date->format('Y-m-d');
            }
            
            $endDateTimeCarbon = $endDateTimeCarbon->addHour(1); 
            
            $availableObjs = AccessoriesAvailable::where('area_id',$area_id)->whereIn('date',$dates)->where('model_id',$model_id)->where('size_id',$size_id)->orderBy('date','asc')->get();
            
            
            $availableHourStr = "";
        
            
            foreach($availableObjs as $availableObj)
            {
                $availableHourStr.=$availableObj->hour_string.",";             
            }
            
            
            $availableArray =  Self::modifyAMHObj($currentTimeCarbon,$endDateTimeCarbon,$availableHourStr,$natureBool,$area_id,$model_id,$size_id,$dates);
            
            
            AccessoriesAvailable::where('area_id',$area_id)->where('model_id',$model_id)->where('size_id',$size_id)->whereIn('date',$dates)->delete();
        
        
        
            DB::table('accessories_available')->insert($availableArray);
            
            
            $capacityObjs = AccessoriesCapacity::where('area_id',$area_id)->whereIn('date',$dates)->where('model_id',$model_id)->where('size_id',$size_id)->orderBy('date','asc')->get();
            
            $capacityHourStr = "";
        
        
            foreach($capacityObjs as $capacityObj)
            {
                $capacityHourStr.=$capacityObj->hour_string.",";             
            }
            
            $capacityArray =  Self::modifyAMHObj($currentTimeCarbon,$endDateTimeCarbon,$capacityHourStr,$natureBool,$area_id,$model_id,$size_id,$dates);
            
            
            AccessoriesCapacity::where('area_id',$area_id)->where('model_id',$model_id)->where('size_id',$size_id)->whereIn('date',$dates)->delete();
        
        
        
            DB::table('accessories_capacity')->insert($capacityArray);
                        
            
        }
        
        DB::table('booking_add_delete_request')->insert(['booking_id'=>0,'nature'=>$nature,'area_id'=>$area_id,'model_id'=>$model_id,'action'=>'add_delete_accessories']);
            
        return "done";
        
    }
    
    
    

    
    public function moveAccessoriesIndex(Request $request)
    {
        $areaList = Area::where('status',1)->get();
        $modelList = AccessoriesModel::where('status',1)->get();
        $sizeList = AccessoriesSize::where('status',1)->get();
        
        return view('admin.moveAccessories.index',compact('areaList','modelList','sizeList'));
    }
    
    public function moveAccessories(Request $request)
    {
        $rules = array(
          'from_area_id' => 'required',
          'to_area_id' => 'required',
          'model_id' => 'required',
          'accessories_id' => 'required',
           
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/move_accessories')->withErrors($validator->errors());
        }
        
        if($request->from_area_id == $request->to_area_id){
          return redirect('/admin/move_accessories')->withErrors(["dates"=>"From and To locations cannot be the same."]);
        }
        
        if($request->start_datetime != "")
        {
            $startDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->start_datetime);
            $startDateTimeCarbon->addHour(-1);
        }
        else
        {
            $startDateTimeCarbon = Carbon::now();
            $startDateTimeCarbon->minute = 0;
            $startDateTimeCarbon->second = 0;
            $startDateTimeCarbon->addHour();
            
        }
        
        if($request->end_datetime != "")
        {
            $endDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->end_datetime);
            $endDateTimeCarbon->addHours(-1);
            $noEndDate = false;
        }
        else
        {
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay()->addHours(-1);
            $noEndDate = true;
        }
                
        
        if($startDateTimeCarbon->gte($endDateTimeCarbon)){
          return redirect('/admin/move_accessories')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
        }
        
        $dispatcher = app('Dingo\Api\Dispatcher');        
        
        $accessoriesAvailability = $dispatcher->with([
                                                'model_id' => $request->model_id, 
                                                'area_id' => $request->from_area_id,
                                                'size_id' => $request->size_id,
                                                'start_date' => $startDateTimeCarbon->format('Y-m-d'), 
                                                'end_date' => $endDateTimeCarbon->format('Y-m-d'),
                                                'start_time' => $startDateTimeCarbon->format('H:i'), 
                                                'end_time' => $endDateTimeCarbon->format('H:i')
                                            ])->get('check-accessories-availability');
        
     
        if($accessoriesAvailability["available"]>0)
        {
            self::addOrDeleteAccessories($request->from_area_id,$request->model_id,$request->size_id,"delete",$startDateTimeCarbon->copy(),$endDateTimeCarbon->copy());
        
            self::addOrDeleteAccessories($request->to_area_id,$request->model_id,$request->size_id,"add",$startDateTimeCarbon->copy(),$endDateTimeCarbon->copy());
            
            $accessories = Accessories::where('id',$request->accessories_id)->first();
            
            DB::table('accessories_movement_log')->insert([
                                                    'created_by'=>Auth::user()->id, 
                                                    'model_id'=>$request->model_id, 
                                                    'from_area_id'=>$request->from_area_id, 
                                                    'start_datetime'=>$startDateTimeCarbon->format('Y-m-d H:i:s'), 
                                                    'end_datetime'=>$endDateTimeCarbon->format('Y-m-d H:i:s'), 
                                                    'accessories_text_id'=>$accessories->text_id, 
                                                    'to_area_id'=>$request->to_area_id,
                                                    'size_id'=>$request->size_id
                                                ]);
            
            Accessories::where('id',$request->accessories_id)->update([
                'updated_by'=>Auth::user()->id,
                'area'=>$request->to_area_id, 
            ]);
            
            return redirect('/admin/move_accessories')->withErrors(["success"=>"The accessories has been successfully moved!!"]);
        
        }
        else
        {
            return redirect('/admin/move_accessories')->withErrors(["availability"=>"There are no accessories available to be moved."]);
        }
        
        
        
        
    }
    
 
    
    
    
    public static function checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableStr)
    {
        
        $availableArray = explode(',',$availableStr);    
        
        $i = $startDateTimeCarbon->hour;
        
        $available = 1000000;
        
        for($date = $startDateTimeCarbon->copy()->addHour(); $date->lte($endDateTimeCarbon); $date->addHour()) 
        {
            $available = min($available, $availableArray[$i]);
            $i++;
        }
        
        return $available;
    }
    
    
    
    public function checkAvailability(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'size_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required' 
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        $availableObjs = AccessoriesAvailable::where('area_id',$request->area_id)->where('model_id',$request->model_id)->where('size_id',$request->size_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        $availableStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {
            $availableStr.=$availableObj->hour_string.",";             
        }

        
        $available = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableStr);
        
         
        
        
        if($available>0)
        {
            return ['available'=>$available];
        }
        else
        {
            $area = Area::where('id',$request->area_id)->first();
            $cityIdWithOffset = $area->city_id + 1000000;
            
            
            $availableInCityObjs = AccessoriesAvailable::where('area_id',$cityIdWithOffset)->where('model_id',$request->model_id)->where('size_id',$request->size_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
            $availableInCityStr = "";


            foreach($availableInCityObjs as $availableInCityObj)
            {
                $availableInCityStr.=$availableInCityObj->hour_string.",";             
            }


            $availableInCity = Self::checkAvailabilityFromString($startDateTimeCarbon->copy()->addHours(-env('ACCESSORIESLEADTIME')),$endDateTimeCarbon,$availableInCityStr);
            if($availableInCity>0)
            {
                return ['available'=>$availableInCity];
            }
            else
            {
                return ['available'=>0];
            }
        }
        
    }
    
    
    public function checkAvailabilityForAreaWithDates(Request $request)
    {
        $rules = array(
            'area_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required' 
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        $availableObjsSuperset = AccessoriesAvailable::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        $availableStr = "";
        
        $availableObjsKeyByModel = $availableObjsSuperset->groupBy('model_id');
        
        $availableAccessories = [];
        $model_ids = [];
        $size_ids = [];
        
        foreach($availableObjsKeyByModel as $availableObjsKeyByModelEl)
        {
            $availableObjsKeyByModelSize = $availableObjsKeyByModelEl->groupBy('size_id');
            
            
            foreach($availableObjsKeyByModelSize as $availableObjsWithoutOrder)
            {
                $availableObjs = $availableObjsWithoutOrder->sortBy('date');
                
                foreach($availableObjs as $availableObj)
                {
                    $model_id = $availableObj->model_id;
                    $size_id = $availableObj->size_id;
                    $availableStr.=$availableObj->hour_string.",";             
                }
                

                $available = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableStr);

                if($available>0)
                {
                    if(!array_key_exists($model_id,$availableAccessories))
                    {
                        $availableAccessories[$model_id] = [];
                    }
                    $availableAccessories[$model_id][$size_id] = $available;
                    array_push($model_ids,$model_id);
                    array_push($size_ids,$size_id);
                }

            }
            
        }
        
        
        $area = Area::where('id',$request->area_id)->first();
        $cityIdWithOffset = $area->city_id + 1000000;
        
        $availableInCityObjsSuperset = AccessoriesAvailable::where('area_id',$cityIdWithOffset)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        $availableInCityStr = "";
        
        $availableInCityObjsKeyByModel = $availableInCityObjsSuperset->groupBy('model_id');
        
        $availableInCityAccessories = [];
        $model_ids = [];
        $size_ids = [];
        
        foreach($availableInCityObjsKeyByModel as $availableInCityObjsKeyByModelEl)
        {
            $availableInCityObjsKeyByModelSize = $availableInCityObjsKeyByModelEl->groupBy('size_id');
            
            
            foreach($availableInCityObjsKeyByModelSize as $availableInCityObjsWithoutOrder)
            {
                $availableInCityObjs = $availableInCityObjsWithoutOrder->sortBy('date');
                
                foreach($availableInCityObjs as $availableInCityObj)
                {
                    $model_id = $availableInCityObj->model_id;
                    $size_id = $availableInCityObj->size_id;
                    $availableInCityStr.=$availableInCityObj->hour_string.",";             
                }
                

                $availableInCity = Self::checkAvailabilityFromString($startDateTimeCarbon->copy()->addHours(-env('ACCESSORIESLEADTIME')),$endDateTimeCarbon,$availableInCityStr);

                if($availableInCity>0)
                {
                    if(!array_key_exists($model_id,$availableAccessories))
                    {
                        $availableAccessories[$model_id] = [];
                        $availableAccessories[$model_id][$size_id] = $availableInCity;
                        
                        array_push($model_ids,$model_id);
                        array_push($size_ids,$size_id);
                    }
                    else
                    {
                        if(!array_key_exists($size_id,$availableAccessories[$model_id]))
                        {
                            
                            $availableAccessories[$model_id][$size_id] = $availableInCity;
                            array_push($model_ids,$model_id);
                            array_push($size_ids,$size_id);
                        }
                        else
                        {
                            $availableAccessories[$model_id][$size_id] += $availableInCity;
                        }
                        
                    }
                    
                }

            }
            
        }
        
        $modelObjs = AccessoriesModel::whereIn('id',$model_ids)->get()->keyBy('id');
        $sizeObjs = AccessoriesSize::whereIn('id',$size_ids)->get()->keyBy('id');
        
        $result = [];
        foreach($availableAccessories as $model_id => $sizes)
        {
            $sizeArray = [];
            
            
            foreach($sizes as $size=>$qty)
            {
                            

                array_push($sizeArray,["size_id"=>$size,"size_name"=>$sizeObjs[$size]->size,"size_qty"=>$qty]);
            }
            
            $dispatcher = app('Dingo\Api\Dispatcher'); 
            $accessoriesPrice = $dispatcher->with([
                                                'model_id' => $model_id, 
                                                'area_id' => $request->area_id,
                                                'start_date' => $request->start_date, 
                                                'end_date' => $request->end_date,
                                                'start_time' => $request->start_time, 
                                                'end_time' => $request->end_time
                                            ])->get('accessories-total-price');
            
            array_push($result,["model_id"=>$model_id,"model_name"=>$modelObjs[$model_id]->model,"type_id"=>$modelObjs[$model_id]->type,"type_name"=>$modelObjs[$model_id]->getType(),"brand_id"=>$modelObjs[$model_id]->brand,"brand_name"=>$modelObjs[$model_id]->getBrand(),"available_sizes"=>$sizeArray,"price"=>$accessoriesPrice,"thumbnail"=>$modelObjs[$model_id]->getImage()]);
        }
        
        
        
        
        
        
        
        return $result;        
        
    }
    
    
}



    
    

