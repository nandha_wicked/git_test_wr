<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use Session;
use DateTime;
use App\Models\BikationVendor;


class Bikation_User_ProfileController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){
    if(Session::has('email') && Session::has('First_Name') ){
      $bikation_venor_user_id=Session::get('Vendor_ID');
      $bikation_user_profile = BikationVendor::where('id',$bikation_venor_user_id)->first();
      return view('bikationvendor.front.user_index',compact('bikation_user_profile'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addProfile(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
     /* $rules = array(
         'Area' => 'required',
          'City' => 'required',
          'State' => 'required',
          'Country' => 'required',
          'ZipCode' => 'required',
          'Phone_Number' => 'required',
          'Website' => 'required'
          );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-user-profile')->withErrors($validator->errors());
      }
      else{*/
       // if(!empty($request->Vendor_ID)){
        $id = Session::get('Vendor_ID');
        BikationVendor::where('id',$id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name,'mobile_num' => $request->mobile_num,'Bank' => $request->Bank,'Bank_Account_Number' => $request->Bank_Account_Number,'IFSC'=>$request->IFSC,'PAN' => $request->PAN,'TAN' => $request->TAN,'SRN' => $request->SRN,'CIN'=>$request->CIN,'TIN' => $request->TIN,'NDA_ID' => $request->NDA_ID,'Agreement_ID'=>$request->Agreement_ID,'Vendor_Image'=>$request->Vendor_Image,'Contact_person'=>$request->Contact_person,'Contact_Email'=>$request->Contact_Email,'Contact_Phone_Number'=>$request->Contact_Phone_Number]);
        return redirect('bikation-user-profile');
      //}else{
        /*BikationVendor::create(['first_name' => $request->first_name,'last_name' => $request->last_name, 'mobile_num' => $request->mobile_num,'Bank' => $request->Bank,'Bank_Account_Number' => $request->Bank_Account_Number,'IFSC'=>$request->IFSC,'PAN' => $request->PAN,'TAN' => $request->TAN,'SRN' => $request->SRN,'CIN'=>$request->CIN,'TIN' => $request->TIN,'NDA_ID' => $request->NDA_ID,'Agreement_ID'=>$request->Agreement_ID,'Vendor_Image'=>$request->Vendor_Image,'Contact_person'=>$request->Contact_person,'Contact_Email'=>$request->Contact_Email,'Contact_Phone_Number'=>$request->Contact_Phone_Number]);
        return redirect('bikation-user-profile');
      }*/
   // }
  }else{
    return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
  }
}
   
  
    
}
