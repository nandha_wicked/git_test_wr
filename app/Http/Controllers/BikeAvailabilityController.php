<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;

use App\Models\Available;
use App\Models\Capacity;
use App\Models\Occupancy;
use App\Models\Service;

use App\Models\AccessoriesAvailable;
use App\Models\AccessoriesCapacity;
use App\Models\AccessoriesOccupancy;
use App\Models\AccessoriesService;

use App\Models\User;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\ReservationInfo;
use App\Models\Slot;
use App\Models\BikeModel;
use App\Models\Area;
use App\Models\Availability;
use App\Models\BikeNumbers;
use App\Models\City;
use DB;
use Mail;
use App\Transformers\BookingTransformer;
use Carbon\Carbon;
use Auth;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\PriceController;


class BikeAvailabilityController extends AppController
{
   
   
    
       
    
    public static function addOrDeleteBooking($id,$nature){
        
        
        if($nature == "add")
        {
            $natureBool = 1;
        }
        else
        {
            $natureBool = 0;    
        }
        
        $booking = Booking::where('id',$id)->first();
        
        
        if(!$booking)
        {
            return ["error"=>"No booking found"];
        }
        $startDateTimeCarbon = Carbon::parse($booking->start_datetime);
        $endDateTimeCarbon = Carbon::parse($booking->end_datetime);
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        
        $availableObjs = Available::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();
        
        
        
        
        $availableHourStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {
            $availableHourStr.=$availableObj->hour_string.",";             
        }
        
        
        
        $availableArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$availableHourStr,!$natureBool,$booking->area_id,$booking->model_id,$dates);
        
        
        
        Available::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
        
        
        
        DB::table('available')->insert($availableArray); 
          
        $nonRevenueIds = explode(',',env('NONREVENUEIDS'));
        
        if(in_array($booking->user_id,$nonRevenueIds))
        {
            
            $serviceObjs = Service::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

            $serviceHourStr = "";
             
            foreach($serviceObjs as $serviceObj)
            {
                $serviceHourStr.=$serviceObj->hour_string.",";             
            }
            
            
            $serviceArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$serviceHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);
            
            Service::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
       
            DB::table('service')->insert($serviceArray);
            
            
        }
        else
        {
            $occupancyObjs = Occupancy::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

            $occupancyHourStr = "";


            foreach($occupancyObjs as $occupancyObj)
            {
                $occupancyHourStr.=$occupancyObj->hour_string.",";             
            }
            
            $occupancyArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$occupancyHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);
            
            Occupancy::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
        
            DB::table('occupancy')->insert($occupancyArray); 
            
        }
        
        
        
        
        
        DB::table('booking_add_delete_request')->insert(['booking_id'=>$id,'nature'=>$nature,'area_id'=>$booking->area_id,'model_id'=>$booking->model_id,'action'=>'add_delete_booking']);
        
        return "done";
        
    }
    
    public static function addOrDeleteReservation($id,$nature){
        
        $addOrDeleteReservation = DB::table('booking_add_delete_request')->where('booking_id',$id)->where('nature',$nature)->where('action','add_delete_booking')->first();
        
        if(!$addOrDeleteReservation)
        {
            if($nature == "add")
            {
                $natureBool = 1;
            }
            else
            {
                $natureBool = 0;    
            }

            $booking = ReservationInfo::where('id',$id)->first();


            if(!$booking)
            {
                return ["error"=>"No booking found"];
            }
            $startDateTimeCarbon = Carbon::parse($booking->start_datetime);
            $endDateTimeCarbon = Carbon::parse($booking->end_datetime);

            for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
            {
                $dates[] = $date->format('Y-m-d');
            }


            $availableObjs = Available::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();


            $availableHourStr = "";


            foreach($availableObjs as $availableObj)
            {
                $availableHourStr.=$availableObj->hour_string.",";             
            }


            $availableArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$availableHourStr,!$natureBool,$booking->area_id,$booking->model_id,$dates);


            Available::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();


            DB::table('available')->insert($availableArray); 

            $nonRevenueIds = explode(',',env('NONREVENUEIDS'));

            if(in_array($booking->user_id,$nonRevenueIds))
            {

                $serviceObjs = Service::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

                $serviceHourStr = "";

                foreach($serviceObjs as $serviceObj)
                {
                    $serviceHourStr.=$serviceObj->hour_string.",";             
                }


                $serviceArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$serviceHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);

                Service::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();

                DB::table('service')->insert($serviceArray);


            }
            else
            {
                $occupancyObjs = Occupancy::where('area_id',$booking->area_id)->whereIn('date',$dates)->where('model_id',$booking->model_id)->orderBy('date','asc')->get();

                $occupancyHourStr = "";


                foreach($occupancyObjs as $occupancyObj)
                {
                    $occupancyHourStr.=$occupancyObj->hour_string.",";             
                }

                $occupancyArray = Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$occupancyHourStr,$natureBool,$booking->area_id,$booking->model_id,$dates);

                Occupancy::where('area_id',$booking->area_id)->where('model_id',$booking->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();

                DB::table('occupancy')->insert($occupancyArray); 

            }


            DB::table('booking_add_delete_request')->insert(['booking_id'=>$id,'nature'=>$nature,'area_id'=>$booking->area_id,'model_id'=>$booking->model_id,'action'=>'add_delete_booking']);
        }
        
        return "done";
        
    }
    
    public static function modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature,$area_id,$model_id,$dates)
    {
        if($nature === "No Change")
        {
            $returnDateStr = $dateStr;
        }
        else
        {
            $returnDateStr = Self::modifyDateStr($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature);
        }
        $returnDateArray = array_chunk(explode(',',$returnDateStr), 24);
        $i=0;
        $AMHObjArray = [];
        foreach($dates as $date)
        {
            array_push($AMHObjArray,['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>implode(',',$returnDateArray[$i])]);
            $i++;
        }
        return $AMHObjArray;
    }
    
    public static function modifyDateStr($startDateTimeCarbon,$endDateTimeCarbon,$dateStr,$nature)
    {
        $dateArray = explode(',',$dateStr);    
        
        $i = $startDateTimeCarbon->hour;
                
        for($date = $startDateTimeCarbon->copy()->addHour(); $date->lte($endDateTimeCarbon); $date->addHour()) 
        {
            
            if($nature == 1)
            {
                $dateArray[$i]++;
            }
            else
            {
                $dateArray[$i]--;
            }
            $i++;
            
        }
        $dateStrReturn = implode(',',$dateArray);
        
        return $dateStrReturn;
    }
    
    
    public static function addOrDeleteBike($area_id,$model_id,$nature,$startDateTimeCarbon,$endDateTimeCarbon)
    {
        
        if($nature == "add")
        {
            $natureBool = 1;
        }
        else
        {
            $natureBool = 0;
        }
        
        $noEndDate = true;
        
        if(gettype($startDateTimeCarbon) == "integer" && gettype($endDateTimeCarbon) == "integer")
        {
            
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay();
            
            $currentTimeCarbon = Carbon::now();
            
        }
        else
        {
            $currentTimeCarbon = $startDateTimeCarbon->copy();
            
            $endDateTimeCarbonCompare = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbonCompare->addDay()->addHours(-1);   
            
            
            if($endDateTimeCarbon->lt($endDateTimeCarbonCompare))
            {
                $noEndDate = false;
                
            }

        }
        
        $startDateTimeCarbon = Carbon::parse(env('STARTDATE'));

        
        $capacity = Capacity::where('area_id',$area_id)->where('model_id',$model_id)->orderBy('date','asc')->get();
        
        
        
        $fullOneHourArray = [];
        $fullZeroHourArray = [];
        
        
        if(count($capacity)==0)
        {
            if($nature != "add")
            {
                return ['error'=>'No bike found to delete'];
            }
            
            $moveEndDateTimeCarbon = $endDateTimeCarbon->copy();
                
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay();
            
            $endDateTimeCarbon = $endDateTimeCarbon->addHour(-1);
            
            
            for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
            {
                $dates[] = $date->format('Y-m-d');
            }
            
            for($date = $startDateTimeCarbon->copy(); $date->lte($currentTimeCarbon); $date->addHour()) 
            {
                array_push($fullOneHourArray,0);
                array_push($fullZeroHourArray,0);
            }
            if($noEndDate)
            {
                for($date = $currentTimeCarbon->copy(); $date->lte($endDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,1);
                    array_push($fullZeroHourArray,0);
                }
            }
            else
            {
                $moveEndDateTimeCarbon->addHours(-1);
                
                for($date = $currentTimeCarbon->copy(); $date->lte($moveEndDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,1);
                    array_push($fullZeroHourArray,0);
                }
                                
                for($date = $moveEndDateTimeCarbon->copy(); $date->lte($endDateTimeCarbon); $date->addHour()) 
                {
                    array_push($fullOneHourArray,0);
                    array_push($fullZeroHourArray,0);
                }
            }
            
                        
            $fullOneStr = implode(',',$fullOneHourArray);
            $fullZeroStr = implode(',',$fullZeroHourArray);
            
            
            $oneAMHObj =  Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$fullOneStr,"No Change",$area_id,$model_id,$dates);
            $zeroAMHObj =  Self::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$fullZeroStr,"No Change",$area_id,$model_id,$dates);
            
            
            DB::table('capacity')->insert($oneAMHObj);
            DB::table('available')->insert($oneAMHObj);
            DB::table('service')->insert($zeroAMHObj);
            DB::table('occupancy')->insert($zeroAMHObj);
            
            
        }
        else
        {   
            $currentTimeCarbon = $currentTimeCarbon->addHour(1);
            
            for($date = Carbon::parse($currentTimeCarbon->format('Y-m-d')); $date->lt($endDateTimeCarbon); $date->addDay()) 
            {
                $dates[] = $date->format('Y-m-d');
            }
            
            $endDateTimeCarbon = $endDateTimeCarbon->addHour(1); 
            
            $availableObjs = Available::where('area_id',$area_id)->whereIn('date',$dates)->where('model_id',$model_id)->orderBy('date','asc')->get();
            
            
            $availableHourStr = "";
        
            
            foreach($availableObjs as $availableObj)
            {
                $availableHourStr.=$availableObj->hour_string.",";             
            }
            
            
            $availableArray =  Self::modifyAMHObj($currentTimeCarbon,$endDateTimeCarbon,$availableHourStr,$natureBool,$area_id,$model_id,$dates);
            
            
            Available::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->delete();
        
        
        
            DB::table('available')->insert($availableArray);
            
            
            $capacityObjs = Capacity::where('area_id',$area_id)->whereIn('date',$dates)->where('model_id',$model_id)->orderBy('date','asc')->get();
            
            $capacityHourStr = "";
        
        
            foreach($capacityObjs as $capacityObj)
            {
                $capacityHourStr.=$capacityObj->hour_string.",";             
            }
            
            $capacityArray =  Self::modifyAMHObj($currentTimeCarbon,$endDateTimeCarbon,$capacityHourStr,$natureBool,$area_id,$model_id,$dates);
            
            
            Capacity::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->delete();
        
        
        
            DB::table('capacity')->insert($capacityArray);
                        
            
        }
        
        DB::table('booking_add_delete_request')->insert(['booking_id'=>0,'nature'=>$nature,'area_id'=>$area_id,'model_id'=>$model_id,'action'=>'add_delete_bike']);
            
        return "done";
        
    }
    
    
    
    
    public function addOneDayCapacity(Request $request){
   
       
        $now = Carbon::today()->addDays(env('FUTUREDAYS'));
        $nowMinusOne = $now->copy()->addDays(-1);
        
        $capacityObjs = Capacity::where('date',$nowMinusOne->format('Y-m-d'))->get();
                

        foreach($capacityObjs as $capacityObj)
        {
            Capacity::create(['date'=>$now->format('Y-m-d'),'area_id'=>$capacityObj->area_id,'model_id'=>$capacityObj->model_id,'hour_string'=>$capacityObj->hour_string]);
        }
        
        $availableObjs = Available::where('date',$nowMinusOne->format('Y-m-d'))->get();
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";        

        foreach($availableObjs as $availableObj)
        {
            Available::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'hour_string'=>$availableObj->hour_string]);
            
            Occupancy::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'hour_string'=>$zero_str]);
            Service::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'hour_string'=>$zero_str]);
        }
        
        
        $capacityObjs = AccessoriesCapacity::where('date',$nowMinusOne->format('Y-m-d'))->get();
                

        foreach($capacityObjs as $capacityObj)
        {
            AccessoriesCapacity::create(['date'=>$now->format('Y-m-d'),'area_id'=>$capacityObj->area_id,'model_id'=>$capacityObj->model_id,'size_id'=>$capacityObj->size_id,'hour_string'=>$capacityObj->hour_string]);
        }
        
        $availableObjs = AccessoriesAvailable::where('date',$nowMinusOne->format('Y-m-d'))->get();
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";        

        foreach($availableObjs as $availableObj)
        {
            AccessoriesAvailable::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'size_id'=>$capacityObj->size_id,'hour_string'=>$availableObj->hour_string]);
            
            AccessoriesOccupancy::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'size_id'=>$capacityObj->size_id,'hour_string'=>$zero_str]);
            AccessoriesService::create(['date'=>$now->format('Y-m-d'),'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'size_id'=>$capacityObj->size_id,'hour_string'=>$zero_str]);
        }
        
         
        $start_date = Carbon::parse('today')->addDays(-1);
        $start_date = $start_date->format('Y-m-d');

        $dispatcher = app('Dingo\Api\Dispatcher');
        $reportData = $dispatcher->with(['data_start_date' => $start_date,'data_end_date' => $start_date])->get('getReportDataEveryDay');

        if(env('SERVER_STATUS')=="live")
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $reportData = $dispatcher->with([])->get('sendOpsEmail');

        }
        

        return "done";
   
       
   }
 

    public function testView(Request $request,$area_id,$model_id){  
        $model = BikeModel::where('bike_model',$model_id)->first();
        $availables = Available::where('area_id',$area_id)->where('model_id',$model->id)->get();
        return view('admin.testView.index',compact('availables'));
    }
    
    

    
    public function moveBikeIndex(Request $request)
    {
        $areaList = Area::where('status',1)->get();
        $modelList = BikeModel::where('status',1)->get();
        
        return view('admin.moveBike.index',compact('areaList','modelList'));
    }
    
    public function moveBike(Request $request)
    {
        $rules = array(
          'from_area_id' => 'required',
          'to_area_id' => 'required',
          'model_id' => 'required',
          'bike_number' => 'required',
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/moveBike')->withErrors($validator->errors());
        }
        
        if($request->from_area_id == $request->to_area_id){
          return redirect('/admin/moveBike')->withErrors(["dates"=>"From and To locations cannot be the same."]);
        }
        
        if($request->start_datetime != "")
        {
            $startDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->start_datetime);
            $startDateTimeCarbon->addHour(-1);
        }
        else
        {
            $startDateTimeCarbon = Carbon::now();
            $startDateTimeCarbon->minute = 0;
            $startDateTimeCarbon->second = 0;
            $startDateTimeCarbon->addHour();
            
        }
        
        if($request->end_datetime != "")
        {
            $endDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->end_datetime);
            $endDateTimeCarbon->addHours(-1);
            $noEndDate = false;
        }
        else
        {
            $endDateTimeCarbon = Carbon::today()->addDays(env('FUTUREDAYS'));
            $endDateTimeCarbon->addDay()->addHours(-1);
            $noEndDate = true;
        }
                
        
        if($startDateTimeCarbon->gte($endDateTimeCarbon)){
          return redirect('/admin/moveBike')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
        }
        
        $moveBikeresponse = Self::moveBikeMethod($request->from_area_id,$request->model_id,$startDateTimeCarbon,$endDateTimeCarbon,$request->to_area_id,$request->bike_number);
        
        if($moveBikeresponse)
        {   
            return redirect('/admin/moveBike')->withErrors(["success"=>"The bike has been successfully moved!!"]);
        }
        else
        {
            return redirect('/admin/moveBike')->withErrors(["availability"=>"There are no bikes available to be moved."]);
        }
        
        
    }
    
    
    public static function moveBikeMethod($fromAreaId,$modelId,$startDateTimeCarbon,$endDateTimeCarbon,$toAreaId,$bikeId)
    {
        $dispatcher = app('Dingo\Api\Dispatcher');        
        
        $bikeAvailability = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $fromAreaId,
            'start_date' => $startDateTimeCarbon->format('Y-m-d'), 
            'end_date' => $endDateTimeCarbon->format('Y-m-d'),
            'start_time' => $startDateTimeCarbon->format('H:i'), 
            'end_time' => $endDateTimeCarbon->format('H:i')
        ])->get('check-availability');
        
     
        if($bikeAvailability["bike_id"]!="none")
        {
            self::addOrDeleteBike($fromAreaId,$modelId,"delete",$startDateTimeCarbon->copy(),$endDateTimeCarbon->copy());
        
            self::addOrDeleteBike($toAreaId,$modelId,"add",$startDateTimeCarbon->copy(),$endDateTimeCarbon->copy());
            
            $bike_number = Bike::where('id',$bikeId)->first()->reg_number;
            
            if($startDateTimeCarbon->diffInDays($endDateTimeCarbon)==env('FUTUREDAYS'))
            {
               $endDateTimeCarbon->addYears(31);
            } 

            DB::table('movement_log')->insert([
                                                    'created_by'=>Auth::user()->id, 
                                                    'model_id'=>$modelId, 
                                                    'from_area_id'=>$fromAreaId, 
                                                    'start_datetime'=>$startDateTimeCarbon->format('Y-m-d H:i:s'), 
                                                    'end_datetime'=>$endDateTimeCarbon->format('Y-m-d H:i:s'), 
                                                    'bike_number'=>$bike_number, 
                                                    'to_area_id'=>$toAreaId, 
                                                ]);
            Bike::where('id',$bikeId)->update([
                'area_id'=>$toAreaId
            ]);
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static function returnBikesToLocation()
    {
        $currentDateTime = Carbon::now();
        $currentDateTime->minute = 0;
        $currentDateTime->second = 0;
        
        $movements = DB::table('movement_log')->where('end_datetime',$currentDateTime->toDateTimeString())->get();
        foreach($movements as $movement)
        {
            Bike::where('reg_number',$movement->bike_number)->update([
                "area_id"=>$movement->from_area_id,
                "updated_by"=>1
            ]);
        }
        
        return "done";
    }
    
    
    public static function editEntryForOneDay($table,$date,$from_hour,$to_hour,$model_id,$area_id,$addOrSubtract)
    {
        $changeObject = DB::table($table)->where('date',$date)->where('model_id',$model_id)->where('area_id',$area_id)->first();
        $hour_str = $changeObject->hour_string;
        $hour_array = explode(',',$hour_str);
        for ($x = 0; $x <= 23; $x++) {
            if($x>=$from_hour && $x<$to_hour)
            {
                if($addOrSubtract=="add")
                {
                    $hour_array[$x]++;
                }
                else
                {
                    $hour_array[$x]--;
                }
            }
            
        }
        $hour_str = implode(',',$hour_array);
        DB::table($table)->where('date',$date)->where('model_id',$model_id)->where('area_id',$area_id)->update(['hour_string'=>$hour_str]);
        
        return "done";

    }
    
    public static function editEntryForRange($table,$from_date,$to_date,$model_id,$area_id,$addOrSubtract)
    {
        $changeObjectArray = DB::table($table)->where('date','>=',$from_date)->where('date','<=',$to_date)->where('model_id',$model_id)->where('area_id',$area_id)->orderBy('date','asc')->get();
        
        
        $begin = new \DateTime($from_date);
        $end = new \DateTime($to_date);
        \date_add($end, \date_interval_create_from_date_string('1 day'));
        
        $interval = \DateInterval::createFromDateString('1 day');
        
        $period = new \DatePeriod($begin, $interval, $end);
        
        $index = 0;
        $modifiedArray = [];
        
        foreach ($period as $dt)
        {
            $date = $dt->format('Y-m-d');
            $hour_str = $changeObjectArray[$index]->hour_string;
            $hour_array = explode(',',$hour_str);
            for ($x = 0; $x <= 23; $x++) 
            {
                  if($addOrSubtract=="add")
                    {
                        $hour_array[$x]++;
                    }
                    else
                    {
                        $hour_array[$x]--;
                    }
            }
            $hour_str = implode(',',$hour_array);
            
            $modifiedArrayElement = ['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$hour_str];
            
            array_push($modifiedArray,$modifiedArrayElement);
            
            $index++;
            
        }
        
        DB::table($table)->where('date','>=',$from_date)->where('date','<=',$to_date)->where('model_id',$model_id)->where('area_id',$area_id)->delete();
        
        DB::table($table)->insert($modifiedArray);
        
        return "done";
        

    }
    
    public static function createAddEntryForRange($from_date,$to_date,$model_id,$area_id)
    {
           
        $begin = new \DateTime($from_date);
        $end = new \DateTime($to_date);
        \date_add($end, \date_interval_create_from_date_string('1 day'));
        
        $interval = \DateInterval::createFromDateString('1 day');
        
        $period = new \DatePeriod($begin, $interval, $end);
        
        $zeroArray = [];
        $oneArray = [];
        
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        $one_str = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        
        foreach ($period as $dt)
        {
            $date = $dt->format('Y-m-d');
            
            
            $zeroArrayElement = ['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$zero_str];
            
            $oneArrayElement = ['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$one_str];
            
            array_push($zeroArray,$zeroArrayElement);
            array_push($oneArray,$oneArrayElement);
            
        }
        
        
        DB::table('available')->insert($oneArray);
        DB::table('capacity')->insert($oneArray);
        DB::table('occupancy')->insert($zeroArray);
        DB::table('service')->insert($zeroArray);
        
        return "done";
        

    }
    
    public static function createZeroEntryForRange($from_date,$to_date,$model_id,$area_id)
    {
           
        $begin = new \DateTime($from_date);
        $end = new \DateTime($to_date);
        \date_add($end, \date_interval_create_from_date_string('1 day'));
        
        $interval = \DateInterval::createFromDateString('1 day');
        
        $period = new \DatePeriod($begin, $interval, $end);
        
        $zeroArray = [];
        
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        
        foreach ($period as $dt)
        {
            $date = $dt->format('Y-m-d');
            
            $zeroArrayElement = ['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$zero_str];
            
            array_push($zeroArray,$zeroArrayElement);
        }
        
        
        DB::table('available')->insert($zeroArray);
        DB::table('capacity')->insert($zeroArray);
        DB::table('occupancy')->insert($zeroArray);
        DB::table('service')->insert($zeroArray);
        
        return "done";
        
    }
    
    public static function createEntryForOneDay($date,$from_hour,$to_hour,$model_id,$area_id)
    {
        
        
        $hour_array = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        for ($x = 0; $x <= 23; $x++) {
            if($x>=$from_hour && $x<$to_hour)
            {
                $hour_array[$x]++;
            }
            
        }
        $hour_str = implode(',',$hour_array);
        DB::table('available')->insert(['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$hour_str]);
        DB::table('capacity')->insert(['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$hour_str]);
        DB::table('occupancy')->insert(['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$zero_str]);
        DB::table('service')->insert(['date'=>$date,'area_id'=>$area_id,'model_id'=>$model_id,'hour_string'=>$zero_str]);
        
        return "done";

    }
    
    
}



    
    

