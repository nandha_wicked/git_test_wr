<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\Faq;


class FaqController extends AppController
{
    
    public function indexFaq(Request $request)
    {
        $pageTitle = "Faq";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>250,"title"=>"Question"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $faqObjects = Faq::all();
        
        $objectTableArray['body'] = [];
        
        foreach($faqObjects as $faqObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$faqObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$faqObject->question,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($faqObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_faq_btn",
                                                            "id"=>"edit_faq_id",
                                                            "data_id"=>$faqObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_faq_btn",
                                                            "id"=>"delete_faq_id",
                                                            "data_id"=>$faqObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_faq_modal";
        $addObject['add_button_text'] = "Add Faq";
        $addObject['modal_title'] = "Add Faq";
        $addObject['add_url'] = "/admin/faq/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"textarea","label"=>"Question","name"=>"question_add"],
                ["type"=>"textarea","label"=>"Answer","name"=>"answer_add"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_add","ckbxId1"=>"status_ckbxId1_add","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_add","ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_faq_modal";
        $editObject['edit_btn_class'] = "edit_faq_btn";
        $editObject['modal_title'] = "Edit Faq";
        $editObject['edit_url'] = "/admin/faq/edit";
        $editObject['ajax_url'] = "/admin/faq/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"textarea","label"=>"Question","name"=>"question_edit"],
                ["type"=>"textarea","label"=>"Answer","name"=>"answer_edit"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_edit","ckbxId1"=>"status_ckbxId1_edit","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_edit","ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_faq_modal";
        $deleteObject['delete_btn_class'] = "delete_faq_btn";
        $deleteObject['modal_title'] = "Delete Faq";
        $deleteObject['delete_url'] = "/admin/faq/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Faq?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addFaq(Request $request)
    {
            Faq::create([
                'created_by'=>Auth::user()->id,
                'question'=>$request->question_add, 
                'answer'=>$request->answer_add,
                'status'=>$request->status_add
            ]);
        
        return redirect()->back();
    }
    
    public function detailsFaq(Request $request)
    {
        $faq = Faq::where('id',$request->id)->first();
        $data = [
                ["type"=>"textarea","name"=>"question_edit","value"=>$faq->question],
                ["type"=>"textarea","name"=>"answer_edit","value"=>$faq->answer],
                ["type"=>"checkbox","status"=>$faq->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editFaq($id, Request $request)
    {
        Faq::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'question'=>$request->question_edit, 
            'answer'=>$request->answer_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteFaq($id, Request $request)
    {
        $faq = Faq::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($faq),"table_name"=>"faq","deleted_by"=>Auth::user()->id]);
        Faq::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    
    
    
    
}
