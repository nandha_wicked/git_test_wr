<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeMake;
use App\Models\Bike;
use App\Models\Price;
use App\Models\Packages;
use App\Models\BikeModelImage;
use App\Models\Image;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\City;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use Safeurl;


class PackagesController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){
    $packagesMakeList = Packages::all();
    $packagesModelList = Packages::all(); 
    foreach ($packagesModelList as $model) {
      $coverImg = Image::where('id', $model->cover_img_id)->first();
      
      $model['cover_img'] = ($coverImg) ? $coverImg['full'] : '';
     
    }
    return view('admin.packages.index',compact('packagesMakeList','packagesModelList'));
  }

  public function add(Request $request){
    $rules = array(
      'name' => 'required',
      'desc' => 'required',
      'destination' => 'required',
      'available_seat' => 'required',
      'total_seats' => 'required',
      'ride_include' => 'required',
      'ride_exclude' => 'required',
      'ride_rule' => 'required',
      'meeting_point' => 'required',
      'recommanded_bike' => 'required',
      'total_km' => 'required',
      'packagesStatus' => 'required',
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/packages')->withErrors($validator->errors());
    }

   
   $coverImg = $request->file('coverImgUpload');
    $coverImgId = null;
    if($coverImg){
      $coverImgExt = $coverImg->guessClientExtension();
      $coverImgFn = $coverImg->getClientOriginalName();

      $coverImgHashName = time().'.'.$coverImgExt;
      $coverImgDestinationPath = 'img/packages_image/cover/';
      $coverImg->move($coverImgDestinationPath, $coverImgHashName);
      
      $coverImgObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName]);
      $coverImgId = $coverImgObj['id'];
    }

    $date_time=$request['date_time'];
    $date= date('Y-m-d', strtotime($date_time));

    $url=Safeurl::make($request->name);
    Packages::create(['name' => $request->name, 'desc' => $request->desc,'destination' => $request->destination,'date_time' => $date,'price' => $request->price,'cover_img_id' => $coverImgId ,'available_seat' => $request->available_seat,'total_seats'=> $request->total_seats,'ride_include' => $request->ride_include,'ride_exclude' => $request->ride_exclude,'ride_rule' => $request->ride_rule,'meeting_point' => $request->meeting_point,'recommanded_bike' => $request->recommanded_bike,'total_km' => $request->total_km,'total_days' => $request->total_days,'status' => $request->packagesStatus,'url'=>$url]);
    return redirect('/admin/packages');

  }


  public function edit($bikeModelId, Request $request){  
    $rules = array(
      'name' => 'required',
      'desc' => 'required',
      'destination' => 'required',
      'available_seat' => 'required',
      'ride_include' => 'required',
      'ride_exclude' => 'required',
      'ride_rule' => 'required',
      'meeting_point' => 'required',
      'recommanded_bike' => 'required',
      'total_km' => 'required'
    );
    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/packages')->withErrors($validator->errors());        
    }
    else{
      $bikeModel = Packages::where('id',$bikeModelId)->first();
      $bikeModelDesc = $request->modelDesc;
      /* Cover Image Upload*/
      $cImgId = $bikeModel->cover_img_id;
      $coverImg = $request->file('coverImgUpload');

      if($coverImg){
        $coverImgExt = $coverImg->guessClientExtension();
        $coverImgFn = $coverImg->getClientOriginalName();

        $coverImgHashName = time().'.'.$coverImgExt;
        $coverImgDestinationPath = 'img/packages_image/cover/';
        $coverImg->move($coverImgDestinationPath, $coverImgHashName);
        
        $coverImgObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName]);

        $coverImgId = $coverImgObj['id'];
      }
      else{
        if(isset($cImgId))
          $coverImgId = $cImgId;
        else
          $coverImgId = null;
      }

      Packages::where('id', $bikeModel['id'])
        ->update([ 'name' => $request->name,
        'desc' => $request->desc,
        'destination' => $request->destination,
        'date_time' => $request->date_time, 
        'price' => $request->price,
        'cover_img_id' => $coverImgId,
        'available_seat' => $request->available_seat,
        'total_seats'=> $request->total_seats,
        'ride_include' => $request->ride_include,
        'ride_exclude' => $request->ride_exclude,
        'ride_rule' => $request->ride_rule,
        'meeting_point' => $request->meeting_point,
        'recommanded_bike' => $request->recommanded_bike,
        'total_km' => $request->total_km,
        'total_days' => $request->total_days,
        'status' => $request->packagesStatus
      ]);
      
      return redirect('/admin/packages');      
    }   
  }

  public function delete($id){
  $packages = Packages::where('id', '=', $id)->delete();
      
    return redirect('/admin/packages');

  }
    
}
