<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeMake;
use App\Models\BikeModel;
use App\Models\Bike;
use App\Models\City;
use App\Models\Area;
use App\Models\Image;
use App\Transformers\BikeMakeTransformer;

use Input;
use DB;

class BikeMakeController extends AppController{
    protected $bikeMakeTransformer;

    function __construct(BikeMakeTransformer $bikeMakeTransformer){
      $this->bikeMakeTransformer = $bikeMakeTransformer;
    }
    
    public function index(){

        $bikeMakeList = BikeMake::all();
        return view('admin.bikeMake.index',compact('bikeMakeList'));
    }


    public function add(Request $request){
        $rules = array(
            'bikeMake' => 'required',
            'bikeMakeStatus' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
           return redirect('/admin/bikeMake')->withErrors($validator->errors());        
        }
        $logoImg = $request->file('logoUpload');
        if($logoImg){
            $logoExt = $logoImg->guessClientExtension();

            $logoImgHashName = time().'.'.$logoExt;
            $logoImgDestinationPath = 'img/bikes_upload/logo/';
            $logoImg->move($logoImgDestinationPath, $logoImgHashName);

            $logoObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$logoImgDestinationPath.$logoImgHashName]);

            $logoId = $logoObj['id'];
        }
        BikeMake::create(['bike_make' => $request->bikeMake, 'status' => $request->bikeMakeStatus, 'logo_id' => $logoId]);
        return redirect('/admin/bikeMake');
    }


    public function edit($bikeMakeId, Request $request){
        $rules = array(
            'bikeMake' => 'required',
            'bikeMakeStatus' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
          return redirect('/admin/bikeMake')->withErrors($validator->errors());        
        }
        /* Logo Upload */
        $bikeMake = BikeMake::find($bikeMakeId);
        $lImgId = $bikeMake->logo_id;
        $logoImg = $request->file('logoUpload');
        if($logoImg){
            $logoExt = $logoImg->guessClientExtension();

            $logoImgHashName = time().'.'.$logoExt;
            $logoImgDestinationPath = 'img/bikes_upload/logo/';
            $logoImg->move($logoImgDestinationPath, $logoImgHashName);

            $logoObj = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$logoImgDestinationPath.$logoImgHashName]);

            $logoId = $logoObj['id'];
        }
        else{
            if(isset($lImgId))
                $logoId = $lImgId;
            else
                $logoId = null;
        }
        
        BikeMake::where('id',$bikeMakeId)->update(['bike_make' => $request->bikeMake, 'logo_id' => $logoId, 'status' => $request->bikeMakeStatus]);
        
        if (isset($request->bikeMakeStatus)) {
            if($request->bikeMakeStatus!=$bikeMake->status)
            {
                $modelIds = BikeModel::where('bike_make_id', $bikeMakeId)->get()->lists('id');
                foreach ($modelIds as $modelId) {
                    BikeModel::where('id', $modelId)->update(['status' => $request->bikeMakeStatus]);
                    Bike::where('model_id', $modelId)->update(['status' => $request->bikeMakeStatus]);
                }
            }
        }
        
        return redirect('/admin/bikeMake'); 
    }

    public function getMakesByCity(Request $request){
        $city = City::find($request->city_id);

        if (!$city) {
          return $this->respondNotFound('City not found');
        }
        $makesList = $city->brandsList();
        foreach ($makesList as $make) {
          $logo = Image::find($make['logo_id']);
          $logo = $logo->transform();
          $make['logo_id'] = $logo['id'];
          $make['logo'] = $logo;
        }
        $data = $this->bikeMakeTransformer->transformCollection($makesList->toArray());
        return $this->respondWithSuccess($data);
  }
}
