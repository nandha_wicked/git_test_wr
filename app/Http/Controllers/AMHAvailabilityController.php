<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Dingo\Api\Routing\Helpers;

use App\Models\Area;
use App\Models\City;
use App\Models\Bike;
use App\Models\Available;
use App\Models\Capacity;
use App\Models\Occupancy;
use App\Models\Service;
use App\Models\BikeModel;

use App\Models\Parameters;


use Carbon\Carbon;

class AMHAvailabilityController extends AppController
{
    public static function checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableStr)
    {
        
        $availableArray = explode(',',$availableStr);    
        
        $i = $startDateTimeCarbon->hour;
        
        $available = 1000000;
        
        for($date = $startDateTimeCarbon->copy()->addHour(); $date->lte($endDateTimeCarbon); $date->addHour()) 
        {
            $available = min($available, $availableArray[$i]);
            $i++;
        }
        
        return $available;
    }
    
    
    public function checkAvailabilityByArea(Request $request)
    {
        $rules = array(
          'start_date' => 'required',
          'end_date' => 'required',
          'start_time' => 'required',
          'end_time' => 'required',
          'area_id'  => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return $this->respondWithValidationError($validator->errors());
        }
        
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        $endDateTimeCarbon = Carbon::parse($request->end_date." ".$request->end_time);
        
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        
        $capacityObjs = Capacity::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $capacityObjByModel = [];
        
        
        foreach($capacityObjs as $capacityObj)
        {
            if(!array_key_exists($capacityObj->model_id,$capacityObjByModel))
            {
                $capacityObjByModel[$capacityObj->model_id] = "";
            }
            $capacityObjByModel[$capacityObj->model_id].=$capacityObj->hour_string.",";             
            
        }
        
        $availableObjs = Available::where('area_id',$request->area_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        $availableObjByModel = [];
        
        
        foreach($availableObjs as $availableObj)
        {
            if(!array_key_exists($availableObj->model_id,$availableObjByModel))
            {
                $availableObjByModel[$availableObj->model_id] = "";
            }
            $availableObjByModel[$availableObj->model_id].=$availableObj->hour_string.",";             
            
        }
        
        
        $modelData = [];
        
        foreach($capacityObjByModel as $modelId => $dateStr)
        {
            $capacity = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$dateStr);
            
            
            if($capacity>0)
            {
                $model = BikeModel::where('id',$modelId)->first();
                
                $dispatcher = app('Dingo\Api\Dispatcher');
                $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $model->id, 
                                      'area_id' => $request->area_id,
                                      'start_date' => $request->start_date, 'end_date' => $request->end_date, 'start_time' => $request->start_time, 'end_time' => $request->end_time])
                                      ->get('bookings/total-price');


                $available = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableObjByModel[$model->id]);
                
                
                $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

                $counts = ['model_id' => $model['id'], 'model_name' => $model['bike_model'], 'total_bikes' => (int)$capacity, 'available_count' => (int)$available, 'not_available_count' =>($capacity-$available) ,'price'=>round($bikeAvailabilityWithPrice['total_price']*round((1+($taxDetails->amount/100)),2))];

                array_push($modelData, $counts);
                
            }
        }
        return $modelData;
        
    }
    
    
    public static function checkAvailabilityByCityForFrontPage($startDateTimeCarbon,$endDateTimeCarbon,$city_id)
    {    
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        $areas = Area::where('city_id',$city_id)->where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        
        $areaIds = $areas->lists('id');
        $modelIds = $models->lists('id');
        
        $capacityObjs = Capacity::whereIn('area_id',$areaIds)->whereIn('model_id',$modelIds)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $capacityObjByModel = [];
        
        foreach($capacityObjs as $capacityObj)
        {
            if(!array_key_exists($capacityObj->model_id,$capacityObjByModel))
            {
                $capacityObjByModel[$capacityObj->model_id] = [];
            }
            if(!array_key_exists($capacityObj->area_id,$capacityObjByModel[$capacityObj->model_id]))
            {
                $capacityObjByModel[$capacityObj->model_id][$capacityObj->area_id] = "";
            }
            
            $capacityObjByModel[$capacityObj->model_id][$capacityObj->area_id].=$capacityObj->hour_string.",";      
        }
        
        
        $availableObjs = Available::whereIn('area_id',$areaIds)->whereIn('model_id',$modelIds)->whereIn('date',$dates)->orderBy('model_id','asc')->orderBy('date','asc')->get();
        
        $availableObjByModel = [];
        
        
        foreach($availableObjs as $availableObj)
        {
            if(!array_key_exists($availableObj->model_id,$availableObjByModel))
            {
                $availableObjByModel[$availableObj->model_id] = [];
            }
            if(!array_key_exists($availableObj->area_id,$availableObjByModel[$availableObj->model_id]))
            {
                $availableObjByModel[$availableObj->model_id][$availableObj->area_id] = "";
            }
            
            $availableObjByModel[$availableObj->model_id][$availableObj->area_id].=$availableObj->hour_string.",";      
        }
        
        
        $modelData = [];

        foreach($capacityObjByModel as $modelId => $areaArray)
        {
            $modelPresentInCity = "false";
            $areasWithoutModel = [];
            
            foreach($areaArray as $areaId => $dateStr)
            {
                $capacity = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$dateStr);
                if($capacity>0)
                {
                    $modelPresentInCity = "true";

                    $available = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableObjByModel[$modelId][$areaId]);
                    if($available>0)
                    {
                        
                        if(!array_key_exists($modelId,$modelData))
                        {
                            $modelData[$modelId] = [];   
                        }
                        
                        if(!array_key_exists('available areas',$modelData[$modelId]))
                        {
                            $modelData[$modelId]['available areas'] = $areaId;
                        }
                        else
                        {
                            $modelData[$modelId]['available areas'] .= ",".$areaId;       
                        }
                    }
                    else
                    {
                        if(!array_key_exists($modelId,$modelData))
                        {
                            $modelData[$modelId] = [];   
                        }
                        
                        if(!array_key_exists('not available areas',$modelData[$modelId]))
                        {
                            $modelData[$modelId]['not available areas'] = $areaId;
                        }
                        else
                        {
                            $modelData[$modelId]['not available areas'] .= ",".$areaId;       
                        }
                    }
                                        
                }
                else
                {
                    array_push($areasWithoutModel,$areaId);
                }
                
            }
            if($modelPresentInCity == "false")
            {
                $modelData[$modelId]['areasWithoutModel'] =implode(",",$areasWithoutModel);
            }
            
        }
                
        return $modelData;
        
    }
    
    public function fromAvailabilityCalendarApp(Request $request){
        
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'month' => 'required',
            'year' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }

        $dispatcher = app('Dingo\Api\Dispatcher');

        $modelId = $request->model_id;
        $cityId = $request->city_id;
        $areaId = $request->area_id;
        $month = $request->month;
        $year = $request->year;
        
        $area = Area::where('id',$areaId)->first();
        $cityId = $area->city_id;
        
        $currentDateTimeCarbon = Carbon::now()->addHour()->minute(0)->second(0);
        
        if($currentDateTimeCarbon->month == $month && $currentDateTimeCarbon->year == $year)
        {
            $startDateTimeCarbon = $currentDateTimeCarbon->copy()->startOfDay();
        }
        else
        {
            $startDateTimeCarbon = Carbon::create($year, $month, 1, 00, 00, 00);
        }
        
        $endDateTimeCarbon = $startDateTimeCarbon->copy()->endOfMonth();
        
        
        for($date = $startDateTimeCarbon->copy(); $date->lt($endDateTimeCarbon); $date->addDay()) 
        {

            $dates[] = $date->format('Y-m-d');
        }
        
        $availableObj = Available::where('area_id',$areaId)->where('model_id',$modelId)->whereIn('date',$dates)->get();
        
        $availableObjByDate = $availableObj->keyBy('date');
        
        if($availableObj->isEmpty())
        {
            return ['error'=>'Model not found in the Area'];
        }
        else
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            
            $calendar = [];
            
            foreach($dates as $date)
            {
                $status = "A";
                $updated = false;
                $atLeastOneAvailable = false;
            
                $working_slots = $dispatcher->with(['date' => $date,'city_id'=>$cityId,'area_id'=>$areaId])->get('from-working-hours-for-area');
                
                $hourStr = $availableObjByDate[$date]['hour_string'];
                
                $hourArray = explode(',',$hourStr);
                
                foreach($working_slots as $working_slot)
                {
                    if($hourArray[$working_slot->id-1]>0)
                    {
                        $working_slot['status'] = true;
                        $atLeastOneAvailable = true;
                        
                    }
                    else
                    {
                        $working_slot['status'] = false;
                        $updated = true;
                        $status = "P";
                    }
                }
                
                if(!$atLeastOneAvailable)
                    $status = "N";
                
                array_push($calendar,['date'=>$date,'slots'=>$working_slots,'status'=>$status,'updated'=>$updated]);
                
                
            }
            
            return $calendar;
            
        }
         
        
    }
    
    
    public function toAvailabilityCalendarApp(Request $request){
        
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required',
            'start_time' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $area = Area::where('id',$areaId)->first();
        $cityId = $area->city_id;
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        
        $startDateTimeCarbon = Carbon::parse($request->start_date." ".$request->start_time);
        
        
        $endDateTimeCarbon = Carbon::today()->addYear();
        $working_slots = $dispatcher->with(['date' => $endDateTimeCarbon->format('Y-m-d'),'city_id'=>$cityId,'area_id'=>$areaId])->get('to-working-hours-for-area');
        if(!is_array($working_slots))
            $working_slots = $working_slots->toArray();
        $lastSlot = end($working_slots);
        $endDate = $endDateTimeCarbon->format('Y-m-d');
        
        $startDate = $startDateTimeCarbon->format('Y-m-d');
        
        $availableObj = Available::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->orderBy('date','asc')->get();
        
        
        
        foreach($availableObj as $available)
        {
            
            $hourArray = explode(',',$available->hour_string);
            if($startDate == $available->date)
            {
                if(in_array(0,$hourArray))
                {
                    
                    $i = 0;
                    foreach($hourArray as $hour)
                    {
                        if($i>$startDateTimeCarbon->hour)
                        {
                            if($hour == 0)
                            {
                                $working_slots = $dispatcher->with(['date' => $available->date,'city_id'=>$cityId,'area_id'=>$areaId])->get('to-working-hours-for-area');
                                $working_slots_by_id = $working_slots->keyBy('id');
                                if(array_key_exists(($i+1),$working_slots_by_id))
                                {
                                    $lastSlot = $working_slots_by_id[$i+1];
                                }
                                else
                                {
                                    $lastSlot = end($working_slots_by_id);
                                }
                                
                                $endDate = $available->date;

                                return ['bike_id'=>1,'end_date'=>$endDate,'last_slot'=>$lastSlot];
                            }
                        }
                        $i++;
                    }
                }
            }
            else
            {
                if(in_array(0,$hourArray))
                {
                    $i = 0;
                    foreach($hourArray as $hour)
                    {
                        if($hour == 0 )
                        {
                            break;
                        }
                        $i++;
                    }


                    $working_slots = $dispatcher->with(['date' => $available->date,'city_id'=>$cityId,'area_id'=>$areaId])->get('to-working-hours-for-area');

                    $working_slots_array = $working_slots->toArray(); 
                    if(($i+1)<(current($working_slots_array)['id']))
                    {

                        $previousDay = Carbon::parse($available->date)->addDays(-1);
                        $working_slots = $dispatcher->with(['date' => $previousDay->format('Y-m-d'),'city_id'=>$cityId,'area_id'=>$areaId])->get('to-working-hours-for-area');
                        $working_slots = $working_slots->toArray();
                        $lastSlot = end($working_slots);
                        $endDate = $previousDay->format('Y-m-d');

                        return ['bike_id'=>1,'end_date'=>$endDate,'last_slot'=>$lastSlot];
                    }
                    else
                    {
                        $working_slots = $dispatcher->with(['date' => $available->date,'city_id'=>$cityId,'area_id'=>$areaId])->get('to-working-hours-for-area');

                        $working_slots_by_id = $working_slots->keyBy('id');
                        $lastSlot = $working_slots_by_id[$i+1];
                        $endDate = $available->date;

                        return ['bike_id'=>1,'end_date'=>$endDate,'last_slot'=>$lastSlot];
                    }

                }
            }
            
        }
        
        return ['bike_id'=>1,'end_date'=>$endDate,'last_slot'=>$lastSlot];
        
    }
    
    
    public function checkAvailability(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required_without_all:start_datetime,end_datetime',
            'start_time' => 'required_without_all:start_datetime,end_datetime',
            'end_date' => 'required_without_all:start_datetime,end_datetime',
            'end_time' => 'required_without_all:start_datetime,end_datetime',
            'start_datetime' =>'required_without_all:start_date,start_time,end_date,end_time',
            'end_datetime' => 'required_without_all:start_date,start_time,end_date,end_time'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        if(isset($request->start_date))
        {
            
            $startDateTimeStr = $request->start_date.' '.$request->start_time;
            $endDateTimeStr = $request->end_date.' '.$request->end_time;
            
        }
        else
        {
            $startDateTimeStr = $request->start_datetime;
            $endDateTimeStr = $request->end_datetime;
            
        }
        
        $startDateTimeCarbon = Carbon::parse($startDateTimeStr);
        $endDateTimeCarbon = Carbon::parse($endDateTimeStr);

       
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        $availableObjs = Available::where('area_id',$request->area_id)->where('model_id',$request->model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
                
        if(count($availableObjs) == 0)
        {
            return ['bike_id'=>"none"];
        }
        
        $availableStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {
            $availableStr.=$availableObj->hour_string.",";             
        }

        
        $available = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$availableStr);
        
         
        
        
        if($available>0)
        {
            return ['bike_id'=>$available];
        }
        else
        {
            return ['bike_id'=>"none"];
        }
        
    }
    
    public function checkCapacity(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_datetime' => 'required',
            'end_datetime' => 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        
        $startDateTimeCarbon = Carbon::parse($request->start_datetime);
        $endDateTimeCarbon = Carbon::parse($request->end_datetime);
        
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        $capacityObjs = Capacity::where('area_id',$request->area_id)->where('model_id',$request->model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        if($capacityObjs->count() == 0)
        {
            $capacity = 0;
        }
        else
        {
            $capacityStr = "";


            foreach($capacityObjs as $capacityObj)
            {
                $capacityStr.=$capacityObj->hour_string.",";             
            }


            $capacity = Self::checkAvailabilityFromString($startDateTimeCarbon,$endDateTimeCarbon,$capacityStr);
            
        }
         
                
        return ['capacity'=>$capacity];
        
    }
    
    public static function returnDatesWithZeroAvailability($startDateTimeCarbon,$endDateTimeCarbon,$availableStr)
    {
        $availableArray = explode(',',$availableStr);    
        
        $i = $startDateTimeCarbon->hour;
        
        $dateArray = [];
        
        $zeroStart = true;
        
        $j = 0;
        
        for($date = $startDateTimeCarbon->copy(); $date->lt($endDateTimeCarbon); $date->addHour()) 
        {            
            
            if($availableArray[$i]==0 && $zeroStart)
            {
                $dateArray[$j]['start_datetime'] = $date->toDateTimeString();
                $zeroStart = false;
            }
            if($availableArray[$i]>0 && !$zeroStart)
            {
                $dateArray[$j]['end_datetime'] = $date->toDateTimeString();
                $zeroStart = true;
                                
                $j++;
            }
            $i++;
        }
        
        if(count($dateArray)>0)
        {
            if(!array_key_exists('end_datetime',end($dateArray)))
            {
                $dateArray[$j]['end_datetime'] = $endDateTimeCarbon->toDateTimeString();
            }
        }
        
        
        return $dateArray;
    }
    
    
}
