<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests;
// use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Auth;
use Hash;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Partner;


class LoginController extends AppController
{   
	public function dologin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/admin/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            

			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($user->isAdmin == 1){
					return Redirect::to('/admin/dashboard');
                }
                else {
    				Auth::logout();
    				$validator->errors()->add('email', 'Permission Denied');
					return Redirect::to('/admin/login')->withErrors($validator);	
                }
				//echo 'SUCCESS!';
			} else {        
				// validation not successful, send back to form 
				$validator->errors()->add('email', 'Password mismatch');
				return Redirect::to('/admin/login')->withErrors($validator);				
			}

		}
    }
    
    
    public function doErplogin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/erp/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            

			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($user->isOperation == 1){
					return Redirect::to('/erp/dashboard');
                }
                else {
    				Auth::logout();
    				$validator->errors()->add('email', 'Permission Denied');
					return Redirect::to('/erp/login')->withErrors($validator);	
                }
				//echo 'SUCCESS!';
			} else {        
				// validation not successful, send back to form 
				$validator->errors()->add('email', 'Password mismatch');
				return Redirect::to('/erp/login')->withErrors($validator);				
			}

		}
    }
    
    
    public function doA2blogin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/a2b/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            

			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($user->isOperation == 1){
					return Redirect::to('/a2b/dashboard');
                }
                else {
    				Auth::logout();
					return Redirect::to('/a2b');	
                }
				//echo 'SUCCESS!';
			} else {        
				// validation not successful, send back to form 
				return Redirect::to('/a2b');				
			}

		}
    }
    
    public function doRandMlogin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/randm/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            

			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($user->isOperation == 1){
					return Redirect::to('/randm/dashboard');
                }
                else {
    				Auth::logout();
					return Redirect::to('/randm');	
                }
				//echo 'SUCCESS!';
			} else {        
				// validation not successful, send back to form 
				return Redirect::to('/randm');				
			}

		}
    }
    
    public function doVendorlogin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/vendor/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            $vendor = Vendor::where('user_id', $uid)->first();
            
			
			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($uid == 1)
					return Redirect::to('/admin/dashboard');
                elseif(!$vendor){
    				Auth::logout();
    				$validator->errors()->add('email', 'Permission Denied');
					return Redirect::to('/vendor/login')->withErrors($validator);	    				
                }
				else{
                    return Redirect::to('/vendor/dashboard');
				}
				//echo 'SUCCESS!';
			} else {       
				$validator->errors()->add('email', 'Password mismatch');
				return Redirect::to('/vendor/login')->withErrors($validator);				
				// validation not successful, send back to form 
			}

		}
    }
    
    
    public function doPartnerlogin()
	{
    	// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|min:3' 
		);

		

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);


		$validator->after(function($validator){
			$email = Input::get('email');
			$user = User::where('email', $email)->first();
		    if(!$user){
		        $validator->errors()->add('email', 'Invalid Email Id');
		    }
		});

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {			
			return Redirect::to('/partner/login')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);			

			$email = Input::get('email');
			$user = User::where('email', $email)->first();
			$uid = $user->id;
            $partner = Partner::where('user_id', $uid)->first();
            
			
			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{				
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				if($uid == 1)
					return Redirect::to('/admin/dashboard');
                elseif(!$partner){
    				Auth::logout();
    				$validator->errors()->add('email', 'Permission Denied');
					return Redirect::to('/partner/login')->withErrors($validator);	    				
                }
				else{
                    return Redirect::to('/partner/dashboard');
				}
				//echo 'SUCCESS!';
			} else {        
				// validation not successful, send back to form 
				$validator->errors()->add('email', 'Password mismatch');
				return Redirect::to('/partner/login')->withErrors($validator);				
			}

		}
    }
    
    
    public function doLogout()
	{
	    Auth::logout(); // log the user out of our application
	    return Redirect::to('/'); // redirect the user to the homepage
	}
}
