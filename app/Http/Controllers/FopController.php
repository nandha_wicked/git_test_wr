<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Area;
use Session;
use App\Models\User;
use App\Models\Parameters;
use App\Models\FopEnquiry;
use Auth;
use Dingo\Api\Routing\Helpers;




class FopController extends AppController
{
    use Helpers;

 
    public function getEarnInfoElements(Request $request)
    {
        $fopInfo = json_decode(Parameters::getParameter('fopInfo'));
        $check_box_options = json_decode(Parameters::getParameter('check_box_options'));
        $data = [
            "info"=>$fopInfo,
            "check_box_options"=>$check_box_options
        ];
        return $this->respondWithSuccess($data);
    }
    
    public function postEnquiryForm(Request $request)
    {
        $rules = array(
            'email' => 'required',
            'phone' => 'required',
            'name' => 'required',
            'check_box_responses' => 'required',
        );
    

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());
        }
        
        if($request->header()['authorization']==["Bearer"])
        {
            $userId = 0;
        }
        else
        {
            $user = UserController::getUserByToken();
            $userId = $user->id;
        }
        
        foreach($request->check_box_responses as $responseObj)
        {
            if($responseObj['id']==1)
            {
                $bike_branding = $responseObj['response']=="true"?1:0;
            }
            elseif($responseObj['id']==2)
            {
                $rent_bike = $responseObj['response']=="true"?1:0;
            }
        }
        
        FopEnquiry::create([
            'email'=>$request->email, 
            'phone'=>$request->phone, 
            'name'=>$request->name, 
            'bike_branding'=>$bike_branding, 
            'rent_bike'=>$rent_bike, 
            'location'=>$request->location, 
            'model'=>$request->model, 
            'odo'=>$request->odo, 
            'registration_year'=>$request->registration_year, 
            'created_by'=>$userId
        ]);
        
        $data = [
            "status"=>true,
            "message"=>"Enquiry successfully saved."
        ];
        
        
        return $this->respondWithSuccess($data);
        
    }
    
}
