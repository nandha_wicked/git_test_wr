<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeMake;
use App\Models\Bike;
use App\Models\Price;
use App\Models\BikeModel;
use App\Models\BikeModelImage;
use App\Models\Image;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\City;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use Auth;
use DB;
use Storage;


class BikeModelController extends AppController{

    protected $bikeModelTransformer;

    function __construct(BikeModelTransformer $bikeModelTransformer){
        $this->bikeModelTransformer = $bikeModelTransformer;
    }
    
    public function index(){
        $bikeMakeList = BikeMake::all();
        $bikeModelList = BikeModel::all(); 
        foreach ($bikeModelList as $model) 
        {
            $coverImg = Image::where('id', $model->cover_img_id)->first();
            $thumbImg = Image::where('id', $model->thumbnail_img_id)->first();
            $model['cover_img'] = ($coverImg) ? $coverImg['full'] : '';
            $model['thumb_img'] = ($thumbImg) ? $thumbImg['full'] : '';
        }
        return view('admin.bikeModel.index',compact('bikeMakeList','bikeModelList'));
    }

    public function add(Request $request){
        $rules = array(
            'bikeMake' => 'required',
            'bikeModel' => 'required',
            'bikeModelStatus' => 'required',
            'thumbnailImgUpload' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/admin/bikeModel')->withErrors($validator->errors());
        }

        /* Cover Image Upload*/
        $coverImg = $request->file('coverImgUpload');
        $coverImgId = null;
        if($coverImg)
        {
            $coverImgExt = $coverImg->guessClientExtension();
            $coverImgFn = $coverImg->getClientOriginalName();

            $coverImgHashName = time().'.'.$coverImgExt;
            $coverImgDestinationPath = 'img/bikes_upload/cover/';
            $coverImg->move($coverImgDestinationPath, $coverImgHashName);

            $coverImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$coverImgDestinationPath.$coverImgHashName]);

            $coverImgId = $coverImgObj['id'];
        }

        /* Thumbnail Image Upload */
        $thumbnailImg = $request->file('thumbnailImgUpload');
        $thumbnailImgId = null;
        if($thumbnailImg)
        {
            $thumbnailImgExt = $thumbnailImg->guessClientExtension();
            $thumbnailImgFn = $thumbnailImg->getClientOriginalName();

            $bikeMake = BikeMake::where('id',$request->bikeMake)->first();

            $thumbnailImgHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->bikeModel)."-".rand(0,99).'.'.$thumbnailImgExt;
            $thumbnailImgDestinationPath = 'img/bikes_upload/thumbnail/';
            $thumbnailImg->move($thumbnailImgDestinationPath, $thumbnailImgHashName);

            $thumbnailImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$thumbnailImgDestinationPath.$thumbnailImgHashName]);

            $thumbnailImgId = $thumbnailImgObj['id'];
        }

        /* Gallery Image Upload */
        $galleryImg = $request->file('galleryImgUpload');
        $newGallery = Gallery::create(['name' => $request->bikeModel, 'description' => '']);

        if(isset($_FILES['galleryImgUpload'])){
            $errors= array();
            foreach($_FILES['galleryImgUpload']['tmp_name'] as $key => $tmp_name )
            {
                $file_name = $key.$_FILES['galleryImgUpload']['name'][$key];
                $file_size = $_FILES['galleryImgUpload']['size'][$key];
                $file_tmp = $_FILES['galleryImgUpload']['tmp_name'][$key];
                $file_type = $_FILES['galleryImgUpload']['type'][$key];
                $expensions= array("jpeg","jpg","png");
                if(empty($errors)==true)
                {
                    $imageName = $file_name;
                    move_uploaded_file($file_tmp,"img/bikes_upload/gallery/".$imageName);
                    $imgObj = Image::create(['full' => '//'.env('IMAGEURL').'/img/bikes_upload/gallery/'.$imageName]);
                    $galleryImage = GalleryImage::create(['gallery_id' => $newGallery['id'], 'image_id' => $imgObj['id']]);
                }
            }
        }

        BikeModel::create(['bike_make_id' => $request->bikeMake, 'bike_model' => $request->bikeModel, 'cover_img_id' => $coverImgId, 'thumbnail_img_id' => $thumbnailImgId, 'gallery_id' => $newGallery['id'], 'status' => $request->bikeModelStatus]);
        return redirect('/admin/bikeModel');

    }


    public function edit($bikeModelId, Request $request)
    {  
        $rules = array(
        'bikeModel' => 'required',
        'bikeModelStatus' => 'required',
        'coverImgUpload' => 'image|max:3000',
        'thumbnailImgUpload' => 'image|max:3000'
        );
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return redirect('/admin/bikeModel')->withErrors($validator->errors());        
        }
        else{
        $bikeModel = BikeModel::where('id',$bikeModelId)->first();
        $bikeModelDesc = $request->modelDesc;
        /* Cover Image Upload*/
        $cImgId = $bikeModel->cover_img_id;
        $coverImg = $request->file('coverImgUpload');

        if($coverImg){
        $coverImgExt = $coverImg->guessClientExtension();
        $coverImgFn = $coverImg->getClientOriginalName();

        $coverImgHashName = time().'.'.$coverImgExt;
        $coverImgDestinationPath = 'img/bikes_upload/cover/';
        $coverImg->move($coverImgDestinationPath, $coverImgHashName);

        $coverImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$coverImgDestinationPath.$coverImgHashName]);

        $coverImgId = $coverImgObj['id'];
        }
        else{
        if(isset($cImgId))
        $coverImgId = $cImgId;
        else
        $coverImgId = null;
        }

        /* Thumbnail Image Upload */
        $tImgId = $bikeModel->thumbnail_img_id;
        $thumbnailImg = $request->file('thumbnailImgUpload');

        if($thumbnailImg){
        $thumbnailImgExt = $thumbnailImg->guessClientExtension();
        $thumbnailImgFn = $thumbnailImg->getClientOriginalName();

        $bikeMake = BikeMake::where('id',$bikeModel->bike_make_id)->first();

        $thumbnailImgHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->bikeModel)."-".rand(0,99).'.'.$thumbnailImgExt;
        $thumbnailImgDestinationPath = 'img/bikes_upload/thumbnail/';

        if(file_exists(public_path($thumbnailImgDestinationPath.$thumbnailImgHashName)))
        {
        unlink(public_path($thumbnailImgDestinationPath.$thumbnailImgHashName));
        }



        $thumbnailImg->move($thumbnailImgDestinationPath, $thumbnailImgHashName);

        $thumbnailImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$thumbnailImgDestinationPath.$thumbnailImgHashName]);

        $thumbnailImgId = $thumbnailImgObj['id'];
        }
        else{
        if(isset($tImgId))
        $thumbnailImgId = $tImgId;
        else
        $thumbnailImgId = null;
        }

        /* Gallery Image Upload */

        $galleryImg = $request->file('galleryImgUpload');
        $galleryId = $bikeModel->gallery_id;
        $gallery = Gallery::where('id',$galleryId)->first();
        if (!isset($gallery)) {
        $newGallery = Gallery::create(['name' => $request->bikeModel, 'description' => '']);
        $bikeModel->gallery_id = $newGallery['id'];
        $bikeModel->save();
        $galleryId = $newGallery['id'];
        }

        if(isset($_FILES['galleryImgUpload'])){
        $errors= array();
        foreach($_FILES['galleryImgUpload']['tmp_name'] as $key => $tmp_name ){
        $file_name = $key.$_FILES['galleryImgUpload']['name'][$key];
        $file_size = $_FILES['galleryImgUpload']['size'][$key];
        $file_tmp = $_FILES['galleryImgUpload']['tmp_name'][$key];
        $file_type = $_FILES['galleryImgUpload']['type'][$key];
        $expensions= array("jpeg","jpg","png");

        if(empty($errors)==true){
        $imageName = $file_name;
        move_uploaded_file($file_tmp,"img/bikes_upload/gallery/".$imageName);
        $imgObj = Image::create(['full' => '//'.env('IMAGEURL').'/img/bikes_upload/gallery/'.$imageName]);
        $galleryImage = GalleryImage::create(['gallery_id' => $galleryId, 'image_id' => $imgObj['id']]);
        }
        }
        $galleryImgId = $galleryId;
        }
        else{
        if(isset($galleryId) && $galleryId != 0)
        $galleryImgId = $galleryId;
        else
        $galleryImgId = null;
        }


        BikeModel::where('id', $bikeModel['id'])
        ->update([ 'bike_model' => $request->bikeModel, 
        'cover_img_id' => $coverImgId,
        'thumbnail_img_id' => $thumbnailImgId,
        'gallery_id' => $galleryImgId,
        'description' => $bikeModelDesc,
        'status' => $request->bikeModelStatus,
        'priority' => $request->priority
        ]);
        if (isset($request->bikeModelStatus)) {
        if($request->bikeModelStatus!=$bikeModel->status)
        {
        Bike::where('model_id', $bikeModel['id'])->update(['status' => $request->bikeModelStatus]);
        }
        }
        return redirect('/admin/bikeModel');      
        }   
    }

    public function spec($bikeModel_id, Request $request)
    {
        $key1 = $request->spec1Key;
        $key2 = $request->spec2Key;
        $key3 = $request->spec3Key;
        $key4 = $request->spec4Key;

        $value1 = $request->spec1Value;
        $value2 = $request->spec2Value;
        $value3 = $request->spec3Value;
        $value4 = $request->spec4Value;

        $spec1Array = array($key1,$value1);
        $spec2Array = array($key2,$value2);
        $spec3Array = array($key3,$value3);
        $spec4Array = array($key4,$value4);

        $bikeSpec = BikeSpecification::find($bikeModel_id);

        if (!$bikeSpec) {
        BikeSpecification::create(['bike_model_id' => $bikeModel_id, 'engine' => serialize($spec1Array), 'power' => serialize($spec2Array),
        'seats' => serialize($spec3Array), 'fuel_capacity' => serialize($spec4Array)]);
        }else{
        BikeSpecification::where('bike_model_id', $bikeModel_id)
        ->update([
        'engine' => serialize($spec1Array),
        'power' => serialize($spec2Array),
        'seats' => serialize($spec3Array),
        'fuel_capacity' => serialize($spec4Array)
        ]);
        }


        return redirect('/admin/bikeModel');      
    }

    public function getModelDetails($id, Request $request){

    $dispatcher = app('Dingo\Api\Dispatcher');
    $startDate = $request->start_date;
    $endDate = $request->end_date;
    $startTime = $request->start_time;
    $endTime = $request->end_time;

    if(!$request->city_id)
    $city_id = 1;
    else
    $city_id =$request->city_id;


    /* Optional parameter checks */
    if (isset($startDate) && isset($endDate) && isset($startTime) && isset($endTime)) 
    {  
    $data = $dispatcher->with([    
    'model_id' => $id,
    'start_date' => $startDate, 
    'end_date' => $endDate,
    'start_time' => $startTime, 
    'end_time' => $endTime
    ])->get('cities/'.$city_id.'/models');
    }
    else
    {
    $data = $dispatcher->with([    
    'model_id' => $id
    ])->get('cities/'.$city_id.'/models');
    }
    return $data;
    }

    public function getModelByCity(Request $request){
    $city = City::find($request->city_id);
    if (!$city) {
    return $this->respondNotFound('City not found');
    }
    $models = $city->getModelList();
    $data = $this->bikeModelTransformer->transformCollection($models->toArray());
    return $this->respondWithSuccess($data);
    }



    public function getGalleryImages($id){
    $imgUrls = [];
    $bikeModel = BikeModel::where('id', $id)->first();
    $gallery = Gallery::where('id', $bikeModel['gallery_id'])->first();
    if(!$gallery)
    return $this->respondNotFound('Gallery for this model does not exist.');

    $imageIds = GalleryImage::where('gallery_id', $bikeModel['gallery_id'])->get()->lists('image_id');
    foreach ($imageIds as $imageId) {
    $image = Image::where('id', $imageId)->first();
    array_push($imgUrls, $image['full']);
    }
    return $imgUrls;
    }
    
    public function indexBikeModel(Request $request)
    {
        $pageTitle = "Bike Model";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Make"],
                                            ["width"=>100,"title"=>"Model"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>120,"title"=>"Image"],
                                            ["width"=>120,"title"=>"Manual"],
                                            ["width"=>80,"title"=>"Priority"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $bikeModelObjects = BikeModel::all();
        
        $objectTableArray['body'] = [];
        
        foreach($bikeModelObjects as $bikeModelObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->getBikeMake(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->bike_model,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->description,"pre"=>"true"]],
                        ["type"=>"image","value"=>["src"=>$bikeModelObject->getThumbnailImgUrl(),"width"=>100]],
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->manual_url,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeModelObject->priority,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($bikeModelObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_bike_model_btn",
                                                            "id"=>"edit_bike_model_id",
                                                            "data_id"=>$bikeModelObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_bike_model_btn",
                                                            "id"=>"delete_bike_model_id",
                                                            "data_id"=>$bikeModelObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_bike_model_modal";
        $addObject['add_button_text'] = "Add Model";
        $addObject['modal_title'] = "Add Bike Model";
        $addObject['add_url'] = "/admin/bike-model/add";
        $addObject['add_modal_form_items'] = [];
        
        
        $makeList = [];
        $makeObjects = BikeMake::where('status',1)->get();
        foreach($makeObjects as $makeObject)
        {
            array_push($makeList,["value"=>$makeObject->id,"text"=>$makeObject->bike_make]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Bike Model","input_type"=>"text","name"=>"model_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"select","label"=>"Make", "name"=>"make_add", "id"=>"make_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$makeList],
                ["type"=>"image","label"=>"Model Image","name"=>"model_image_add"],
                ["type"=>"image","label"=>"Model Manual","name"=>"model_manual_add"],
                ["type"=>"input","label"=>"Priority","input_type"=>"text","name"=>"priority_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_bike_model_modal";
        $editObject['edit_btn_class'] = "edit_bike_model_btn";
        $editObject['modal_title'] = "Edit Bike Model";
        $editObject['edit_url'] = "/admin/bike-model/edit";
        $editObject['ajax_url'] = "/admin/bike-model/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Bike Model","input_type"=>"text","name"=>"model_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"select","label"=>"Make", "name"=>"make_edit", "id"=>"make_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$makeList],
                ["type"=>"image","label"=>"Model Image","name"=>"model_image_edit","old_image_id"=>"old_model_image_id_edit"],
                ["type"=>"image","label"=>"Model Manual","name"=>"model_manual_edit","old_image_id"=>"old_model_manual_id_edit"],
                ["type"=>"input","label"=>"Priority","input_type"=>"text","name"=>"priority_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_bike_model_modal";
        $deleteObject['delete_btn_class'] = "delete_bike_model_btn";
        $deleteObject['modal_title'] = "Delete Bike Model";
        $deleteObject['delete_url'] = "/admin/bike-model/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Bike Model";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addBikeModel(Request $request)
    {
        
        $rules = array(
            'model_add' => 'required',
            'make_add' => 'required',
            'status_add' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());        
        }
        
        $bikeMake = BikeMake::where('id',$request->make_add)->first();
        
        $modelImg = $request->file('model_image_add');
        $modelImgId = null;
        
        if($modelImg)
        {
            $modelImgExt = $modelImg->guessClientExtension();
            $modelImgFn = $modelImg->getClientOriginalName();

            $modelImgHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->model_add)."-".rand(0,99).'.'.$modelImgExt;
            $modelImgDestinationPath = 'img/bikes_upload/thumbnail/';
            $modelImg->move($modelImgDestinationPath, $modelImgHashName);

            $modelImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$modelImgDestinationPath.$modelImgHashName]);

            $modelImgId = $modelImgObj['id'];
        }
        
        
        $modelManual = $request->file('model_manual_add');
        $modelManualUrl = "";
        
        if($modelManual)
        {
            $modelManualExt = $modelManual->guessClientExtension();
            $modelManualFn = $modelManual->getClientOriginalName();

            $modelManualHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->model_add)."-".rand(0,99).'.'.$modelManualExt;
            $modelManualDestinationPath = 'img/bikes_upload/manual/';
            $modelManual->move($modelManualDestinationPath, $modelManualHashName);

            $modelManualUrl =  env('HTTPSORHTTP').'://'.env('IMAGEURL').'/'.$modelManualDestinationPath.$modelManualHashName;

        }
               
        
        BikeModel::create([
            'bike_model'=>$request->model_add,
            'bike_make_id'=>$request->make_add,
            'description'=>$request->description_add,
            'thumbnail_img_id'=>$modelImgId,
            'manual_url'=>$modelManualUrl,
            'priority'=>$request->priority_add==""?1000:$request->priority_add,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back()->withErrors(["Model Successfully Added."]);
    }
    
    public function detailsBikeModel(Request $request)
    {
        $bikeModel = BikeModel::where('id',$request->id)->first();
        $model_image_html = "<img src=\"".$bikeModel->getThumbnailImgUrl()."\" width=\"90px\">";
        $manual_image_html = "<p>".$bikeModel->manual_url."</p>";
        $data = [
                ["type"=>"input","name"=>"model_edit","value"=>$bikeModel->bike_model],
                ["type"=>"textarea","name"=>"description_edit","value"=>$bikeModel->description],
                ["type"=>"select","name"=>"make_edit","value"=>$bikeModel->bike_make_id],
                ["type"=>"image","imgId"=>"old_model_image_id_edit","html"=>$model_image_html],
                ["type"=>"image","imgId"=>"old_model_manual_id_edit","html"=>$manual_image_html],
                ["type"=>"input","name"=>"priority_edit","value"=>$bikeModel->priority],
                ["type"=>"checkbox","status"=>$bikeModel->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editBikeModel($id, Request $request)
    {
        
        
        $rules = array(
            'model_edit' => 'required',
            'make_edit' => 'required',
            'status_edit' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());        
        }
        
        $bikeModel = BikeModel::where('id',$id)->first();
        
        $bikeMake = BikeMake::where('id',$request->make_edit)->first();
        
        $modelImg = $request->file('model_image_edit');
        $modelImgId = $bikeModel->thumbnail_img_id;
        
        if($modelImg)
        {
            $modelImgExt = $modelImg->guessClientExtension();
            $modelImgFn = $modelImg->getClientOriginalName();

            $modelImgHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->model_edit)."-".rand(0,99).'.'.$modelImgExt;
            $modelImgDestinationPath = 'img/bikes_upload/thumbnail/';
            $modelImg->move($modelImgDestinationPath, $modelImgHashName);

            $modelImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$modelImgDestinationPath.$modelImgHashName]);

            $modelImgId = $modelImgObj['id'];
        }
        
        
        $modelManual = $request->file('model_manual_edit');
        $modelManualUrl = $bikeModel->manual_url;
        
        if($modelManual)
        {
            $modelManualExt = $modelManual->guessClientExtension();
            $modelManualFn = $modelManual->getClientOriginalName();

            $modelManualHashName = str_replace(' ', '-', $bikeMake->bike_make)."-".str_replace(' ', '-', $request->model_edit)."-".rand(0,99).'.'.$modelManualExt;
            $modelManualDestinationPath = 'img/bikes_upload/manual/';
            $modelManual->move($modelManualDestinationPath, $modelManualHashName);

            $modelManualUrl =  env('HTTPSORHTTP').'://'.env('IMAGEURL').'/'.$modelManualDestinationPath.$modelManualHashName;

        }
               
        
        BikeModel::where('id',$id)->update([
            'bike_model'=>$request->model_edit,
            'bike_make_id'=>$request->make_edit,
            'description'=>$request->description_edit,
            'thumbnail_img_id'=>$modelImgId,
            'manual_url'=>$modelManualUrl,
            'priority'=>$request->priority_edit==""?1000:$request->priority_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back()->withErrors(["Model Successfully Edited."]);
    }
    
    public function deleteBikeModel($id, Request $request)
    {
        $bikeModel = BikeModel::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($bikeModel),"table_name"=>"bike_model","deleted_by"=>Auth::user()->id]);
        BikeModel::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    
}
