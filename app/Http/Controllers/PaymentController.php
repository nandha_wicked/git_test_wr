<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use Auth;
use App\Models\Booking;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\Image;
use App\Models\Slot;
use App\Models\PromoCode;
use App\Models\BikerPlanSubscription;
use App\Models\SentTransaction;
use App\Models\Parameters;
use Session;
use Mail;
use Config;
use Carbon\Carbon;
use App\Jobs\RazorPay;
use App\Jobs\RazorPayApp;
use Razorpay\Api\Api;
use App\Models\CancelledEnquiry;
use DB;



class PaymentController extends AppController
{

    public function paymentRedirect(Request $request)
    {

        if (Auth::check())
        {
            $modelId = Session::get('bkmodelId');
            $areaId = Session::get('bkareaId');
            $cityId = Session::get('bkcityId');
            $startDate = Session::get('bkStartDate');
            $endDate = Session::get('bkEndDate');
            $startTime = Session::get('bkStartTime');
            $endTime = Session::get('bkEndTime');
            $coupon = Session::get('bkPromoCode');
            $note = $request->note;
            Session::put('bknote', $note);
            $dispatcher = app('Dingo\Api\Dispatcher');
            $bikeAvailabilityWithPrice = $dispatcher->with([
                'model_id' => $modelId, 
                'area_id' => $areaId, 
                'city_id' => $cityId,
                'start_date' => $startDate,
                'end_date' => $endDate,
                'start_time' => $startTime, 
                'end_time' => $endTime, 
                'coupon' => $coupon
            ])->get('bookings/price-and-check-availability');

            $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

            $totalPrice = $bikeAvailabilityWithPrice['total_price'];
            //TAX
            $totalPrice = $totalPrice + round(($totalPrice * ($taxDetails->amount/100)),2);
            // 2 decimal points round up
            $totalPrice = floor($totalPrice * 100) / 100;

            $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];

            if($bikeAvailabilityWithPrice['bike_id'] != "none")
            {
                $payu_base_url = env('PAYU_BASE_URL', 'https://test.payu.in/_payment');
                $MERCHANT_KEY = env('PAYU_MERCHANT_KEY', 'gtKFFx');
                $SALT = env('PAYU_SALT', 'eCwWELxi');

                $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

                $productInfo='PayUMoney product information';
                $firstName = Auth::user()->first_name;
                $lastName = Auth::user()->last_name;;           
                $zipCode = "";
                $email = Auth::user()->email;
                $phone = Auth::user()->mobile_num;

                // Get site base URL
                if(isset($_SERVER['HTTPS']))
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";       
                else
                $protocol = 'http';

                $baseUrl = $protocol . "://" . $_SERVER['HTTP_HOST'];


                $surl = $baseUrl.'/payment/success';//.'/modules/'.'payu'.'/success.php'; // Success URL
                $curl = $baseUrl.'/payment/cancel';//.'/modules/'.'payu'.'/cancel.php'; //Cancel URL
                $furl = $baseUrl.'/payment/failed';//.'/modules/'.'payu'.'/failure.php';  // Fail URL
                $pg = 'CC';
                $udf1 = $udf2 = $udf3 = $udf4 = $udf5 = "";
                $hash = hash('sha512', $MERCHANT_KEY.'|'.$txnid.'|'.$totalPrice.'|'.$productInfo.'|'.$firstName.'|'.$email.'|'.$udf1.'|'.$udf2.'|'.$udf3.'|'.$udf4.'|'.$udf5.'||||||'.$SALT); 

                $payuInfo=array(
                'merchant_key' => $MERCHANT_KEY,
                'key' => $MERCHANT_KEY,
                'salt' => $SALT,
                'base_url' => $baseUrl,
                'txnid' => $txnid,
                'payu_base_url' => $payu_base_url,
                'amount' => (float)$totalPrice,
                'productinfo' => $productInfo,        
                'firstname' => $firstName,
                'lastname' => $lastName,
                'zipcode' => $zipCode,
                'email' => $email,
                'phone' => $phone,
                'surl' => $surl,
                'furl' => $furl,
                'curl' => $curl,
                'hash' => $hash,
                'pg' => $pg       
                );

                $startDateTime = Carbon::parse($startDate.' '.$startTime)->toDateTimeString();
                $endDateTime = Carbon::parse($endDate.' '.$endTime)->toDateTimeString();

                $sentTransaction = SentTransaction::create(['user_id' => Auth::user()->id,'model_id'=> $modelId,'area_id' => $areaId,'start_date_time' => $startDateTime,'end_date_time' => $endDateTime,'payment_gateway' => 'PayUmoney','order_id' => $txnid,'amount' => (float)$totalPrice,'promocode' => $coupon,'status'=>'sent']);

                return view('front.payment_redirect',compact('payuInfo'));
            }
            else
            {
                $message = "Sorry! Bike already Booked";
                return view('front/error', compact('message'));
            }
        }
        else
        {
            $message = "Sorry! User is not logged in.";
            return view('front/error', compact('message'));
        }
    }

    public function paymentSuccess(Request $request)
    { 

        $payment_success_call  = DB::table('payment_success_calls')->where('payment_id',$request->txnid)->first();

        if(!$payment_success_call)
        {
            $modelId = Session::get('bkmodelId');
            $areaId = Session::get('bkareaId');
            $cityId = Session::get('bkcityId');
            $startDateStr = Session::get('bkStartDate');
            $endDateStr = Session::get('bkEndDate');
            $startTimeStr = Session::get('bkStartTime');
            $endTimeStr = Session::get('bkEndTime');
            $note = $request->note;
            $coupon = Session::get('bkPromoCode');
            $fuelIncluded = Session::get('bkFuelncluded');

            $requestArray = [];
            
            foreach($request->request as $key=>$value)
            {
                array_push($requestArray,[$key=>$value]);
            }

            $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
            $area = Area::where('id', $areaId)->where('status', 1)->first();

            DB::table('payment_success_calls')->insert(['payment_id'=>$request->txnid,'request'=>json_encode($requestArray)." model - ".$modelId." ".$model->bike_model." area_id - ".$areaId." ".$area->area." Start Date - ".$startDateStr." ".$startTimeStr." End Date - ".$endDateStr." ".$endTimeStr." Coupon - ".$coupon]);

            $startDateTimeCarbon  = Carbon::parse($startDateStr." ".$startTimeStr);
            $endDateTimeCarbon  = Carbon::parse($endDateStr." ".$endTimeStr);

            $startDate = $startDateTimeCarbon->format('Y-m-d');
            $endDate = $endDateTimeCarbon->format('Y-m-d');

            $startTime = $startDateTimeCarbon->format('H:i');
            $endTime = $endDateTimeCarbon->format('H:i');


            $dispatcher = app('Dingo\Api\Dispatcher');
            $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $modelId, 'area_id' => $areaId, 'city_id' => $cityId, 'start_date' => $startDate, 'end_date' => $endDate, 'start_time' => $startTime, 'end_time' => $endTime, 'coupon' => $coupon,'device'=>'WEB'])->get('bookings/price-and-check-availability');


            $max_hours = max($bikeAvailabilityWithPrice['number_of_hours'],$bikeAvailabilityWithPrice['minimum_hours']);
            $extrakmprice = Price::getExtraKMPriceformodel($modelId);
                        
            $freekm = $max_hours*env('KMLIMIT');

            $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";


            $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

            $totalPrice = $bikeAvailabilityWithPrice['total_price'];
            //TAX
            $totalPrice = $totalPrice + round(($totalPrice * ($taxDetails->amount/100)),2);
            // 2 decimal points round up
            $totalPrice = floor($totalPrice * 100) / 100;

            $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];

            $user = Auth::user();
            $user_id = Auth::user()->id;
            $userFname = Auth::user()->first_name;
            $userLname = Auth::user()->last_name;
            $userEmail = Auth::user()->email;
            $userMob = Auth::user()->mobile_num;


            $make = $model->make;
            $modelImage = Session::get('bkmodelImg');
            /* For mail */
            /* 'custFromDate','custToDate','amount','modelName','makeName','modelThumbnail','areaName','areaAddress','userFname'*/
            $custFromDate = $startDate;
            $custToDate = $endDate;
            $custFromTime = $startTime;
            $custToTime = $endTime;

            $modelName = $model['bike_model'];
            $makeName = $make['bike_make'];
            $modelThumbnail = $modelImage;
            $areaName = $area['area'];
            $areaAddress = $area['address'];
            $areaMapLink = $area->maps_link;
            $unit = "Days";
            $sdCarbonObj = Carbon::parse($startDate);
            $edCarbonObj = Carbon::parse($endDate);
            $hours = $edCarbonObj->diffInHours($sdCarbonObj);
            $duration = PaymentController::seconds2human($hours*60*60);


            


            $txnid  = $request->txnid;
            $amount = $totalPrice;


            $existingBooking = Booking::where('pg_txn_id', $txnid)->first();
            if ($existingBooking)
            {
                $txnid = $existingBooking['reference_id'];
                $referencename = "Booking ID";

                return view('front/payment_success', compact('txnid','custFromDate', 'custToDate', 'custFromTime', 'custToTime', 'amount','modelName','makeName','modelThumbnail','areaName','areaAddress','userFname','referencename'));
            }

            $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

            $payment = $api->payment->fetch($txnid);
            
            $str = strtolower($payment->status);
        
            if($str=='authorized')
            {
                
                if($payment->amount/100 > $totalPrice-2)
                {
                    if($bikeAvailabilityWithPrice['bike_id'] != "none")
                    {

                        $startDateTime = Carbon::parse($startDate.' '.$startTime);
                        $endDateTime = Carbon::parse($endDate.' '.$endTime);
                        $dateStr = $startDateTime->format('D j My ha').' to '.$endDateTime->format('D j My ha');
                        $enquiry = $model->bike_model." - ".$area->area."  From ".$dateStr;

                        CancelledEnquiry::where('enquiry',$enquiry)->where('email',$userEmail)->delete();

                        $referencename = "Booking ID";
                        
                        $status = 'COMPLETE';
                        $dispatcher = app('Dingo\Api\Dispatcher');

                        $booking = $dispatcher->with([
                            'start_date' => $startDate, 
                            'end_date' => $endDate, 
                            'start_time' => $startTime, 
                            'end_time' => $endTime, 
                            'user_id' =>$user_id, 
                            'actual_user_id'=>$user_id, 
                            'model_id' => $modelId, 
                            'area_id' => $areaId, 
                            'status' => $status,
                            'total_price' => $totalPrice,
                            'pg_status' => 'success', 
                            'pg_txn_id' => $txnid, 
                            'bank_txn_id' => $txnid, 
                            'order_id' => $txnid, 
                            'pg_mode' => 'RazorPay', 
                            'note' => $note, 
                            'pg_txn_amount' => $amount, 
                            'coupon'=>$coupon,
                            'payment_method'=>'RazorPay',
                            'kmlimit'=>$freekm,
                            'fuel_included'=>$fuelIncluded
                        ])->get('bookingswebBglojd76');


                        if(isset($booking['error'])){
                        $message = $booking['error'];
                        return view('front/error', compact('message'));
                        }
                        
                        $cashbackPct = env('CASHBACKPCT');
                        $cashback = $amount*$cashbackPct/100;

                        if($cashback>0)
                        {
                            $dispatcher = app('Dingo\Api\Dispatcher');
                            $addWalletBalance = $dispatcher->with([
                                'user_email_add' => $userEmail,
                                'amount_add' => $cashback,
                                'nature_add' => 'Cashback for booking',
                                'notes_add' => $cashbackPct."% cashback for all bookings."
                            ])->get('wallet/add_amount_int');
                        }
                        
                        $refId = $booking['reference_id'];
                        
                        
                
                        $manualFile = str_replace(env('HOME_URL'),"",$model->manual_url);
                        $manualFile = "/var/www/html/public".$manualFile;



                        $data = [
                            'fromDate' => $startDate, 
                            'toDate' => $endDate, 
                            'fromTime' => $startTime, 
                            'toTime' => $endTime, 
                            'readable_start_datetime'=>Self::readableDateTime($startDate." ".$startTime),
                            'readable_end_datetime'=>Self::readableDateTime($endDate." ".$endTime),
                            'areaName' => $areaName, 
                            'amount' => $amount, 
                            'price_after_discount'=>round($amount/(1+($taxDetails->amount/100)),2),
                            'tax'=>round(($amount - ($amount/(1+($taxDetails->amount/100)))),2),
                            'price_with_tax'=>$amount,
                            'duration' => $duration, 
                            'areaAddress' => $areaAddress, 
                            'userFname' => $userFname, 
                            'modelName' => $modelName, 
                            'unit' => $unit, 
                            'modelThumbnail' => $modelThumbnail, 
                            'userEmail' => $userEmail, 
                            'userMob' => $userMob, 
                            'note' => $note, 
                            'bookingId' => $refId, 
                            'kmLimitMessage'=>$kmLimitMessage, 
                            'areaMapLink'=>$areaMapLink,
                            'tax_name' => $taxDetails->tax_name,
                            'tax_amount' => $taxDetails->amount,
                            'manualPresent'=>($model->manual_url==""?false:true),
                            'manualFile'=>$manualFile
                        ];

                        Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
                            $message->to($data['userEmail']);
                            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                            $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
                            if($data['manualPresent'])
                                $message->attach($data['manualFile']);
                        });

                        Mail::send('emails.admin_booking_mail',$data,function($message) use ($data) {
                            $message->to(env('ADMIN_EMAIL'));
                            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                            $message->subject(env('SITENAMECAP').' '.$data['userFname'].' Booking - '.$data['bookingId']);
                        });

                        // PROMOCODE CHANGE AVAILABLE COUNT
                        $promocode = PromoCode::where('code', $coupon)->first();
                        if ($promocode) 
                        {
                            $promocode['available_count'] = $promocode['available_count'] - 1;
                            $promocode->save();
                        }
                        
                        $parameters = [
                            'modelId'=>$modelId,
                            'areaId'=>$areaId,
                            'startDate'=>$startDate,
                            'endDate'=>$endDate,
                            'startTime'=>$startTime,
                            'endTime'=>$endTime,
                            'coupon'=>$coupon,
                            'txnid'=>$txnid,
                            'booking_id'=>$booking['id']
                        ];

                        $parametersEn = json_encode($parameters);
                        
                        $job = (new RazorPay($parametersEn))->delay(600);

                        $this->dispatch($job);
                        
                        $txnid = $booking['reference_id'];

                        return view('front/payment_success', compact('custFromDate', 'custToDate', 'custFromTime', 'custToTime', 'amount','modelName','makeName','modelThumbnail','areaName','areaAddress','userFname','txnid','referencename'));

                    }
                    else
                    {
                        $payment_success_call  = DB::table('payment_success_calls')->where('payment_id',$request->txnid)->first();

                        DB::table('payment_success_calls')->where('payment_id',$request->txnid)->update(['request'=>$payment_success_call['request']." Error -  bike was booked by someone while the payment being made."]);

                        $message = "We are sorry, it seems bike has been booked by someone while the payment being made. Please contact our customer care center, we will resolve the issue.";
                        
                        return view('front/error', compact('message'));
                    }
                }
                else
                {
                    $payment_success_call  = DB::table('payment_success_calls')->where('payment_id',$request->txnid)->first();

                    DB::table('payment_success_calls')->where('payment_id',$request->txnid)->update(['request'=>$payment_success_call['request']." Error -  The amount paid is wrong."]);

                    $message = "The amount paid is wrong. Please contact our customer care center, we will resolve the issue.";
                    return view('front/error', compact('message'));
                }

            }
        
        }

    }

    public function paymentSuccessApp(Request $request)
    { 
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        DB::table('appRequest')->insert(['request'=>json_encode($requestArray)]);

        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $areaById = Area::where('id',$areaId)->first();
        $cityId = $areaById->city_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $note = $request->note;
        if(isset($request->coupon))
            $coupon = $request->coupon;
        else
            $coupon = "";
        
        $applied_wallet_amount = $request->applied_wallet_amount;
        

        if($request->payment_method == "RazorPayApp")
        {
            $payment_method = "RazorPayApp";

        }
        elseif($request->payment_method == "RazorPayAppiOS")
        {
            $payment_method = "RazorPayAppiOS";
        }
        else
        {
            $payment_method = "PayTM";
        }


        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId, 
            'area_id' => $areaId, 
            'city_id' => $cityId, 
            'start_date' => $startDate, 
            'end_date' => $endDate, 
            'start_time' => $startTime, 
            'end_time' => $endTime, 
            'coupon' => $coupon,
            'device'=>'MOBILE'
        ])->get('bookings/price-and-check-availability');

        $max_hours = max($bikeAvailabilityWithPrice['number_of_hours'],$bikeAvailabilityWithPrice['minimum_hours']);

        $extrakmprice = Price::getExtraKMPriceformodel($modelId);
        
        $freekm = $max_hours*env('KMLIMIT');

        $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";

        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

        $totalPrice = $bikeAvailabilityWithPrice['total_price'];
        
        //TAX
        $totalPrice = $totalPrice + round(($totalPrice * ($taxDetails->amount/100)),2);
        // 2 decimal points round up
        $totalPrice = floor($totalPrice * 100) / 100;

        $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];

        if(env('SERVER_STATUS'=='test'))
        {
            $user = User::where('email','varunagni@gmail.com')->first();
        }
        else
        {
            $user = UserController::getLoggedInUser();
        }
        
        $user_id = $user->id;
        $userFname = $user->first_name;
        $userLname = $user->last_name;
        $userEmail = $user->email;
        $userMob = $user->mobile_num;

        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $area = Area::where('id', $areaId)->where('status', 1)->first();
        $make = $model->make;
        $modelImage = Image::where('id', $model['thumbnail_img_id'])->first();
        /* For mail */
        /* 'custFromDate','custToDate','amount','modelName','makeName','modelThumbnail','areaName','areaAddress','userFname'*/
        $custFromDate = $startDate;
        $custToDate = $endDate;
        $custFromTime = $startTime;
        $custToTime = $endTime;
        $amount = $request->total_price;
        $modelName = $model['bike_model'];
        $makeName = $make['bike_make'];
        $modelThumbnail = $modelImage['full'];
        $areaName = $area['area'];
        $areaAddress = $area['address'];
        $areaMapLink =  $area->maps_link;
        $unit = "Days";
        $sdCarbonObj = Carbon::parse($startDate);
        $edCarbonObj = Carbon::parse($endDate);
        $hours = $edCarbonObj->diffInHours($sdCarbonObj);
        $duration = PaymentController::seconds2human($hours*60*60);

        $raw_data = json_encode($request->all());

        $udf1 = $udf2 = $udf3 = $udf4 = $udf5 = "";
        //CHECK IF BOOKING ALREADY EXIST
        $existingBooking = Booking::where('pg_txn_id', $request->pg_txn_id)->first();




        if ($existingBooking)
        {
            $refId = $existingBooking['reference_id'];
            $instruction = $existingBooking['instructions'];
            $data = ['id' => $existingBooking['id'], 'reference_id' => $refId, 'instructions' => $instruction];
            return $data;
        }
        
        if((($request->pg_txn_amount+($applied_wallet_amount*(1+($taxDetails->amount/100)))+2) >= $totalPrice)||($request->payment_method == "PayLaterAndroid")||($request->payment_method == "PayLateriOS"))
        {
            if($bikeAvailabilityWithPrice['bike_id'] != "none")
            {
                $bikeId = $bikeAvailabilityWithPrice['bike_id'];
                $status = 'COMPLETE';
                
                $fuelIncluded = 0;
                
                if(isset($request->conditions))
                {
                    foreach($request->conditions as $condition)
                    {
                        if($condition['id'] == 1 && $condition['response'] == true)
                        {
                            $fuelIncluded = 1;
                            
                            $kmLimitMessage = "please note that the fuel is included with the rental and each KM is charged Rs ".env('PERKMCHARGEWITHFUEL')." per KM";
                        }
                    }
                }
                    
                
                                
                $dispatcher = app('Dingo\Api\Dispatcher');


                $booking = $dispatcher->with([
                    'start_date' => $startDate,
                    'end_date' => $endDate, 
                    'start_time' => $startTime, 
                    'end_time' => $endTime,                 
                    'user_id' => $user_id, 
                    'actual_user_id'=>$user_id, 
                    'model_id' => $modelId,
                    'area_id'=>$areaId, 
                    'status' => $status,
                    'total_price' => $totalPrice,
                    'pg_status' => $request->status, 
                    'pg_txn_id' => $request->pg_txn_id, 
                    'bank_txn_id' => $request->bank_txn_id,
                    'order_id' => $request->order_id, 
                    'pg_mode' => $request->pg_mode, 
                    'note' => $note, 
                    'pg_txn_amount' => $request->pg_txn_amount, 
                    'raw_data' => $raw_data,
                    'coupon'=>$coupon,
                    'payment_method'=>$payment_method,
                    'kmlimit'=>$freekm,
                    'applied_wallet_amount'=>$applied_wallet_amount,
                    'fuel_included'=>$fuelIncluded
                ])->get('bookingswebBglojd76');


                if(isset($booking['error']))
                {
                    return $booking;
                }
                
                $cashbackPct = env('CASHBACKPCT');
                $cashback = $request->pg_txn_amount*$cashbackPct/100;
                
                if($cashback>0)
                {
                    $dispatcher = app('Dingo\Api\Dispatcher');
                    $addWalletBalance = $dispatcher->with([
                        'user_email_add' => $userEmail,
                        'amount_add' => $cashback,
                        'booking_id_add'=>$booking['reference_id'],
                        'nature_add' => 'Cashback for booking',
                        'notes_add' => $cashbackPct."% cashback for all bookings."
                    ])->get('wallet/add_amount_int');
                }

                $refId = $booking['reference_id'];
                $instruction = $booking['instructions'];
                
                if($model->manual_url == "")
                {
                    $manualFile = "";
                }
                else
                {
                    $manualFile = str_replace(env('HOME_URL'),"",$model->manual_url);
                    $manualFile = "/var/www/html/public".$manualFile;
                }
                


                $data = [
                    'fromDate' => $startDate, 
                    'toDate' => $endDate, 
                    'fromTime' => $startTime, 
                    'toTime' => $endTime, 
                    'readable_start_datetime'=>Self::readableDateTime($startDate." ".$startTime),
                    'readable_end_datetime'=>Self::readableDateTime($endDate." ".$endTime),
                    'areaName' => $areaName, 
                    'amount' => $amount, 
                    'price_after_discount'=>round($amount/(1+($taxDetails->amount/100)),2),
                    'tax'=>round(($amount - ($amount/(1+($taxDetails->amount/100)))),2),
                    'price_with_tax'=>$amount,
                    'duration' => $duration, 
                    'areaAddress' => $areaAddress, 
                    'userFname' => $userFname, 
                    'modelName' => $modelName, 
                    'unit' => $unit, 
                    'modelThumbnail' => $modelThumbnail, 
                    'userEmail' => $userEmail, 
                    'userMob' => $userMob, 
                    'note' => $note, 
                    'bookingId' => $refId, 
                    'instruction' => $instruction, 
                    'kmLimitMessage'=>$kmLimitMessage,
                    'areaMapLink'=>$areaMapLink,
                    'tax_name' => $taxDetails->tax_name,
                    'tax_amount' => $taxDetails->amount,
                    'manualPresent'=>($model->manual_url==""?false:true),
                    'manualFile'=>$manualFile
                ];

                Mail::send('emails.user_booking_mail',$data,function($message) use ($data,$userEmail) {
                    $message->to($userEmail);
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
                    if($data['manualPresent'])
                        $message->attach($data['manualFile']);
                });

                Mail::send('emails.admin_booking_mail',$data,function($message) use ($data,$user) {
                    $message->to(env('ADMIN_EMAIL'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' '.$user->first_name .' Booking - '.$data['bookingId']);
                });

                // PROMOCODE CHANGE AVAILABLE COUNT
                $promocode = PromoCode::where('code',$coupon)->first();
                if ($promocode) 
                {
                    $promocode['available_count'] = $promocode['available_count'] - 1;
                    $promocode->save();
                }       


                //Send job to RazorPayApp to capture payment
                if(!isset($request->applied_wallet_amount))
                {
                    $applied_wallet_amount = 0;
                }
                else
                {
                    $applied_wallet_amount = $request->applied_wallet_amount;
                }

                if($request->payment_method == "RazorPayApp"||$request->payment_method == "RazorPayAppiOS")
                {
                    $parameters =   ['modelId'=>$modelId,
                        'areaId'=>$areaId,
                        'cityId'=>$cityId,
                        'startDate'=>$startDate,
                        'endDate'=>$endDate,
                        'startTime'=>$startTime,
                        'endTime'=>$endTime,
                        'coupon'=>$coupon,
                        'txnid'=>$request->pg_txn_id,
                        'booking_id'=>$booking['id'],
                        'applied_wallet_amount'=>$applied_wallet_amount,
                        'tax_amount' => $taxDetails->amount
                    ];

                    $parametersEn = json_encode($parameters);



                    $this->dispatch(new RazorPayApp($parametersEn));

                }
                
                return $booking;

            }
            else
            {
                $message = "It seems bike has been booked by someone while the payment being made.";
                return $this->setStatusCode(200)->respondWithError($message);
            }
        }
        else
        {
            $data = [
                'bookingId'=>$request->pg_txn_id,
                'txnAmount'=>$request->pg_txn_amount,
                'totalPrice'=>$totalPrice,
                'reason'=>"Incorrect amount paid"
            ];


            Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
                  $message->to(env('ADMIN_EMAIL'));
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject('RazorPay App Problem - '.$data['bookingId']);
            });
            
            $text = "We are sorry, your booking attempt failed because of tax mismatch. The current applicable tax is 18% GST, the older version of the app still has 5.5% VAT. We will get in touch with you soon to get this rectified. We also request you to update the app to avoid any inconvenience.";
            
            $dataUser = [
                'bookingId'=>$request->pg_txn_id,
                'fname'=>$userFname,
                'email_body'=>$text,
                'email'=>$userEmail,
                'reason'=>"Incorrect amount paid"
            ];


            Mail::send('emails.user_gst_problem_mail',$dataUser,function($message) use ($dataUser) {
                  $message->to($dataUser['email']);
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject(env('SITENAMECAP').' App Problem - '.$dataUser['bookingId']);
            });
            
            $message = "We are sorry, your booking attempt failed because of tax mismatch. The current applicable tax is 18% GST, the older version of the app still has 5.5% VAT. We will get in touch with you soon to get this rectified. We also request you to update the app to avoid any inconvenience.";
            return $this->setStatusCode(200)->respondWithError($message);
        }

    }



    public function paymentSuccessZero(Request $request){ 

        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $areaById = Area::where('id',$areaId)->first();
        $cityId = $areaById->city_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $note = $request->note;
        $coupon = $request->coupon;


        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $modelId, 'area_id' => $areaId, 'city_id' => $cityId, 'start_date' => $startDate, 'end_date' => $endDate, 'start_time' => $startTime, 'end_time' => $endTime, 'coupon' => $coupon])->get('bookings/price-and-check-availability');

        $max_hours = max($bikeAvailabilityWithPrice['number_of_hours'],$bikeAvailabilityWithPrice['minimum_hours']);
       
        $extrakmprice = Price::getExtraKMPriceformodel($modelId);
        
        $freekm = $max_hours*env('KMLIMIT');

        $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";



        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

        $totalPrice = $bikeAvailabilityWithPrice['total_price'];
        //TAX
        $totalPrice = $totalPrice + round(($totalPrice * ($taxDetails->amount/100)),2);
        // 2 decimal points round up
        $totalPrice = floor($totalPrice * 100) / 100;

        $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];

        $user = Auth::user();
        $user_id = $user->id;
        $userFname = $user->first_name;
        $userLname = $user->last_name;
        $userEmail = $user->email;
        $userMob = $user->mobile_num;

        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $area = Area::where('id', $areaId)->where('status', 1)->first();
        $make = $model->make;
        $modelImage = Image::where('id', $model['thumbnail_img_id'])->first();

        $custFromDate = $startDate;
        $custToDate = $endDate;
        $custFromTime = $startTime;
        $custToTime = $endTime;
        $amount = $request->total_price;
        $modelName = $model['bike_model'];
        $makeName = $make['bike_make'];
        $modelThumbnail = $modelImage['full'];
        $areaName = $area['area'];
        $areaAddress = $area['address'];
        $areaMapLink = $area->maps_link;
        $unit = "Days";
        $sdCarbonObj = Carbon::parse($startDate);
        $edCarbonObj = Carbon::parse($endDate);
        $hours = $edCarbonObj->diffInHours($sdCarbonObj);
        $duration = PaymentController::seconds2human($hours*60*60);

        $raw_data = json_encode($request->all());

        $udf1 = $udf2 = $udf3 = $udf4 = $udf5 = "";




        if($totalPrice==0)
        {
            if($bikeAvailabilityWithPrice['bike_id'] != "none")
            {
                $bikeId = $bikeAvailabilityWithPrice['bike_id'];
                $status = 'COMPLETE';
                $dispatcher = app('Dingo\Api\Dispatcher');

                $fuelIncluded = 0;
                    
                if(isset($request->conditions))
                {
                    foreach($request->conditions as $condition)
                    {
                        if($condition['id'] == 1 && $condition['response'] == true)
                        {
                            $fuelIncluded = 1;
                            $kmLimitMessage = "please note that the fuel is included with the rental and each KM is charged Rs ".env('PERKMCHARGEWITHFUEL')." per KM";
                        }
                    }
                }
                
                $booking = $dispatcher->with([
                    'start_date' => $startDate, 
                    'end_date' => $endDate, 
                    'start_time' => $startTime, 
                    'end_time' => $endTime, 
                    'user_id' => $user['id'], 
                    'actual_user_id'=>$user['id'], 
                    'model_id' => $modelId,
                    'area_id'=>$areaId, 
                    'status' => $status,
                    'total_price' => $totalPrice,
                    'pg_status' => 'COMPLETE', 
                    'pg_txn_id' => 'NA', 
                    'bank_txn_id' => 'NA', 
                    'order_id' => 'NA', 
                    'pg_mode' => 'Free', 
                    'note' => $note, 
                    'pg_txn_amount' => 0, 
                    'raw_data' => $raw_data, 
                    'coupon'=>$coupon,
                    'payment_method'=>'Free',
                    'kmlimit'=>$freekm,
                    'fuel_included'=>$fuelIncluded
                ])->get('bookingswebBglojd76');
                
                if(isset($booking['error']))
                {
                    return $booking;
                }

                $refId = $booking['reference_id'];
                $instruction = $booking['instructions'];
                $manualFile = str_replace(env('HOME_URL'),"",$model->manual_url);
                $manualFile = "/var/www/html/public".$manualFile;


                $data = [
                            'fromDate' => $startDate, 
                            'toDate' => $endDate, 
                            'fromTime' => $startTime, 
                            'toTime' => $endTime,
                            'readable_start_datetime'=>Self::readableDateTime($startDate." ".$startTime),
                            'readable_end_datetime'=>Self::readableDateTime($endDate." ".$endTime),
                            'areaName' => $areaName, 
                            'amount' => $amount, 
                            'price_after_discount'=>round($amount/(1+($taxDetails->amount/100)),2),
                            'tax'=>round(($amount - ($amount/(1+($taxDetails->amount/100)))),2),
                            'price_with_tax'=>$amount,
                            'duration' => $duration, 
                            'areaAddress' => $areaAddress, 
                            'userFname' => $userFname, 
                            'modelName' => $modelName, 
                            'unit' => $unit, 
                            'modelThumbnail' => $modelThumbnail, 
                            'userEmail' => $userEmail, 
                            'userMob' => $userMob, 
                            'note' => $note, 
                            'bookingId' => $refId, 
                            'instruction' => $instruction, 
                            'kmLimitMessage'=>$kmLimitMessage, 
                            'areaMapLink'=>$areaMapLink,
                            'tax_name' => $taxDetails->tax_name,
                            'tax_amount' => $taxDetails->amount,
                            'manualPresent'=>($model->manual_url==""?false:true),
                            'manualFile'=>$manualFile
                        ];

                Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
                    $message->to(Auth::user()->email);
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
                    if($data['manualPresent'])
                        $message->attach($data['manualFile']);
                });

                Mail::send('emails.admin_booking_mail',$data,function($message) use ($data) {
                    $message->to(env('ADMIN_EMAIL'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' '.Auth::user()->first_name .' Booking - '.$data['bookingId']);
                });

                // PROMOCODE CHANGE AVAILABLE COUNT
                $promocode = PromoCode::where('code', $coupon)->first();
                if ($promocode) 
                {
                    if($promocode->code!='DAYS')
                    {
                        $promocode['available_count'] = $promocode['available_count'] - 1;
                        $promocode->save();
                    }
                    else
                    {
                        $noOfDays = ceil($hours/24);
                        $promocode['available_count'] = $promocode['available_count'] - $noOfDays;
                        $promocode->save();
                    }

                }

                $referencename = "Booking ID";
                $txnid =$refId;

                return view('front/payment_success', compact('custFromDate', 'custToDate', 'custFromTime', 'custToTime', 'amount','modelName','makeName','modelThumbnail','areaName','areaAddress','userFname','txnid','referencename'));

            }
            else
            {
                $message = "It seems bike has been booked by someone while the payment being made.";
                return $this->setStatusCode(200)->respondWithError($message);
            }
        }
        else
        {
            return  "The promocode entered is wrong. Try again.";
        }

    }

    public function paymentFailed(Request $request)
    {
        return view('front/payment_failed');
    }

    public function paymentCanceled(Request $request)
    {
        return view('front/payment_canceled');
    }

    public static function seconds2human($ss) {
        $s = $ss%60;
        $m = floor(($ss%3600)/60);
        $h = floor(($ss%86400)/3600);
        $d = floor(($ss%2592000)/86400);
        $M = floor($ss/2592000);

        return $d. " days, .".$h." hours";
    }





    public function paymentBikerPlanRedirect(Request $request){
        /*return view('admin.area.index',compact('cityList','areaList'));*/

        if (Auth::check()){

        $planCost = Session::get('planCost');
        $planName = Session::get('planName');
        $planId = Session::get('planId');
        $planImage = Session::get('planImage');
        $planLongDescription = Session::get('planLongDescription');

        $payu_base_url = env('PAYU_BASE_URL', 'https://test.payu.in/_payment');
        $MERCHANT_KEY = env('PAYU_MERCHANT_KEY', 'gtKFFx');
        $SALT = env('PAYU_SALT', 'eCwWELxi');

        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $totalPrice = $planCost;

        $productInfo=$planName;
        $firstName = Auth::user()->first_name;
        $lastName = Auth::user()->last_name;;           
        $zipCode = "";
        $email = Auth::user()->email;
        $phone = Auth::user()->mobile_num;

        // Get site base URL
        if(isset($_SERVER['HTTPS']))
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";       
        else
        $protocol = 'http';

        $baseUrl = $protocol . "://" . $_SERVER['HTTP_HOST'];


        $surl = $baseUrl.'/biker-plan/payment/success';//.'/modules/'.'payu'.'/success.php'; // Success URL
        $curl = $baseUrl.'/biker-plan/payment/cancel';//.'/modules/'.'payu'.'/cancel.php'; //Cancel URL
        $furl = $baseUrl.'/biker-plan/payment/failed';//.'/modules/'.'payu'.'/failure.php';  // Fail URL
        $pg = 'CC';
        $udf1 = $udf2 = $udf3 = $udf4 = $udf5 = "";
        $hash = hash('sha512', $MERCHANT_KEY.'|'.$txnid.'|'.$totalPrice.'|'.$productInfo.'|'.$firstName.'|'.$email.'|'.$udf1.'|'.$udf2.'|'.$udf3.'|'.$udf4.'|'.$udf5.'||||||'.$SALT); 

        $payuInfo=array(
        'merchant_key' => $MERCHANT_KEY,
        'key' => $MERCHANT_KEY,
        'salt' => $SALT,
        'base_url' => $baseUrl,
        'txnid' => $txnid,
        'payu_base_url' => $payu_base_url,
        'amount' => (float)$totalPrice,
        'productinfo' => $productInfo,        
        'firstname' => $firstName,
        'lastname' => $lastName,
        'zipcode' => $zipCode,
        'email' => $email,
        'phone' => $phone,
        'surl' => $surl,
        'furl' => $furl,
        'curl' => $curl,
        'hash' => $hash,
        'pg' => $pg       
        );


        return view('front.plan_payment_redirect',compact('payuInfo'));

        }else{
        $message = "Sorry! User is not logged in.";
        return view('front/error', compact('message'));
        }
    }

    public function paymentBikerPlanSuccess(Request $request){ 


        if( $request->status == "success" ){


        $totalPrice = Session::get('planCost');
        $planName = Session::get('planName');
        $planId = Session::get('planId');
        $planImage = Session::get('planImage');
        $planLongDescription = Session::get('planLongDescription');




        $user = Auth::user();
        $user_id = Auth::user()->id;
        $userFname = Auth::user()->first_name;
        $userLname = Auth::user()->last_name;
        $userEmail = Auth::user()->email;
        $userMob = Auth::user()->mobile_num;


        $amount = $request->amount;




        $raw_data = json_encode($request->all());

        $udf1 = $udf2 = $udf3 = $udf4 = $udf5 = "";
        //CHECK IF BOOKING ALREADY EXIST
        $existingSubscription = BikerPlanSubscription::where('pg_txn_id', $request->mihpayid)->first();
        if ($existingSubscription){


        return view('front/plan_payment_success', compact('userFname'));
        }

        //sha512(<SALT>|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key)
        //$hashKey = sha512($SALT.'|'.$request->status.'|'.'||||||'.$udf5.'|'.$udf4.'|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$request->email.'|'.$request->firstname.'|'.$request->productinfo.'|'.$request->amount.'|'.$request->txnid.'|'.$request->key);

        // if($hashKey == $request->hash){
        if($amount == $totalPrice){

        $status = 'COMPLETE';
        $dispatcher = app('Dingo\Api\Dispatcher');

        $bikerPlanSubscription = $dispatcher->with([
        'user_id' => $user['id'], 
        'biker_plan_id' => $planId, 
        'status' => $status,
        'total_price' => $totalPrice,
        'pg_status' => $request->status, 
        'pg_txn_id' => $request->mihpayid, 
        'bank_txn_id' => $request->bank_ref_num, 
        'order_id' => $request->txnid, 
        'pg_mode' => $request->mode, 
        'pg_txn_amount' => $amount, 
        'raw_data' => $raw_data])->post('bikerPlanSubscription');
        if(isset($bikerPlanSubscription['error'])){
        $message = $bikerPlanSubscription['error'];
        return view('front/error', compact('message'));
        }

        $data = [ 'amount' => $amount, 
        'userFname' => $userFname, 
        'userEmail' => $userEmail, 'userMob' => $userMob,
        'plan'=>$planName];



        Mail::send('emails.admin_plan_mail',$data,function($message) use ($data) {
        //var_dump($data);
        $message->to(env('ADMIN_EMAIL'));
        $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
        $message->subject(env('SITENAMECAP').' - '.Auth::user()->first_name .' - Biker Plan Subscription');
        });



        }else{
        $message = "The amount paid is wrong. Something went wrong. Do not place the order.";
        return view('front/error', compact('message'));
        }
        // }else
        //   echo "HashKey MisMash: Sorry something went wrong.";
        }
        return view('front/plan_payment_success', compact('userFname','amount'));
    }

    public function paymentBikerPlanFailed(Request $request)
    {
    return view('front/payment_failed');
    }

    public function paymentBikerPlanCanceled(Request $request)
    {
    return view('front/payment_canceled');
    }
    
    public static function readableDateTime($datetime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($datetime)->format('jS M-y ga');
        }
        return Carbon::parse($datetime)->format('jS M ga');
    }


}
