<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\City;
use App\Models\Area;
use App\Models\Bike;
use App\Models\Price;
use App\Models\User;
use App\Models\News;
use App\Models\SentTransaction;
use App\Models\BikeNumbers;
use App\Models\BikeMake;
use App\Models\BikeModel;
use App\Models\Booking;
use App\Models\SalesReport;
use App\Models\Capacity;

use App\Models\Accessories;

use Hash;
use Auth;
use Response;
use Input;
use Mail;
use Config;
use DB;
use Carbon\Carbon;
use App\Jobs\PayTM;

use Excel;


class ApiController extends AppController
{
    

    public function cityModel(){

    	return [
    			'result' => [
    						'status_code' => 200,
    						'location_list' => [ 
    							['location_name' => 'Bangalore', 'location_id' => 777],
    							['location_name' => 'Pune', 'location_id' => 888],
    							['location_name' => 'Mumbai', 'location_id' => 999],
    						],
    						'brands_list' => [ 
    							['name' => 'Harley', 'id' => 123, 'image_url' => 'imageURL'],
    							['name' => 'Ducati', 'id' => 1234, 'image_url' => 'imageURL'],
    							['name' => 'Kawasaki', 'id' => 12, 'image_url' => 'imageURL'],
    						]				
    			]
    	];
    }

    public function bikeList(){

    	return [
    			'result' => [
    						'status_code' => 200,
    						'bike_list' => [ 
    							['id' => 4646, 'name' => 'Night Rod', 'image_url' => 'imageUrl', 'bike_location_id' => [123, 456], 'bike_rent_amount' => 12000],
    							['id' => 1234, 'name' => 'Iron 883', 'image_url' => 'imageUrl', 'bike_location_id' => [123], 'bike_rent_amount' => 1000],
    							['id' => 4646, 'name' => 'Night Rod', 'image_url' => 'imageUrl', 'bike_location_id' => [456], 'bike_rent_amount' => 2000],
    						]   									
    			]
    	];
    }

    public function loginApi(){

    	return [
    			'result' => [
    						'status_code' => 200,
    						'message' => 'Success Message',
    						'user_id' => 1100,
    						'name' => 'Ajith Kumar',
    						'email_id' => 'ajith@inkoniq.com',
    						'dob' => 378894574598,
    						'profile_image' => 'profile_image_url' 									
    			]
    	];
    }

    public function bikeDetails(){

    	return [
    			'result' => [
    						'images' => ['imageurl1', 'imageurl2', 'imageurl3', 'imageurl4'],
    						'id' => 1234,
    						'name' => 'iron 883',
    						'bike_location' => [123, 456],
    						'rent_summary_list' => [
    							['rent_summary_id' => 1, 'rent_number_of_days' => '1-2days', 'rent_amount' => 4000],
    							['rent_summary_id' => 2, 'rent_number_of_days' => '2-4days', 'rent_amount' => 3500],
    							['rent_summary_id' => 3, 'rent_number_of_days' => '3-6days', 'rent_amount' => 3000]	
    						]							
    			]
    	];
    }

    public function getAllCities(){

        $city = new City();
        $cities = $city->getAllCities();
        return ['result' => $cities];
    }

    public function getAllAreas($city_id){

        $area = new Area();
        $areas = $area->getAllAreas($city_id);
        foreach($areas as $area){
            $area->latitude = "0.0001234";
            $area->longitude = "0.0003456";
        }
        return ['result' => $areas];
    }

    public function getAllMakeLogo($city_id){

       /* $bikeMake = BikeMake::all();

        foreach($bikeMake as $bm){
            $bm->bike_make_logo = '4.jpg';
        }

        return ['result' => $bikeMake];*/
        $bm = new BikeMake();

        $uniqueBikeMakes = DB::table('bikes')
            ->join('bike_models', 'bikes.model_id', '=', 'bike_models.id' )
            ->join('areas', 'bikes.area_id', '=', 'areas.id')
            ->select('bike_models.bike_make_id')
            ->where('areas.city_id', $city_id)
            ->groupBy('bike_models.bike_make_id')
            ->get();

        foreach ($uniqueBikeMakes as $bikeMake){
            $bikeMake->make_name = $bm->getBikeMakeName($bikeMake->bike_make_id);
            $bikeMake->make_logo = '2.jpg';
        }    

        return ['result' => $uniqueBikeMakes];

        /*echo "<pre>";
        echo print_r($uniqueBikeMakes);
        echo "</pre>"; */   
            
    }


    public function get_areas_by_model_id($model_id,$city_id,$from_date,$to_date)
    {
        $areas =  \DB::table('areas')
            //->select('bikes.*','areas.*')
            ->join('bikes', 'areas.id', '=', 'bikes.area_id')
            //->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->select('bikes.area_id','areas.area','bikes.price_id')
            ->where('areas.city_id', $city_id)
            ->where('bikes.model_id', $model_id)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            //->where('bikes.model_id', "1")
            ->groupBy('areas.id')         
            ->get();


            $bike = new Bike();
            $price = new Price();
        foreach($areas as $area){
            //$area->bikeBooked = $bike->checkBikeAvailability($from_date, $to_date, $model_id, $area->area_id);
            //$area->bikeCount = $bike->getBikeCount($model_id, $area->area_id);
            $area->bikeAvailability = $bike->checkBikeAvailability($from_date, $to_date, $model_id, $area->area_id);
            $area->totalPrice = $price->getPriceByDate($from_date,$to_date,$area->price_id);
            $area->perDayPrice = $price->perDayPrice($from_date,$to_date,$area->totalPrice);
            $area->latitude = 0.000567;
            $area->longitude = 0.000765;
        }    

        return $areas;
    }

    public function getUniqueModelList($from_date, $to_date, $city_id){

        $fmdtArr = explode(" ", $from_date);
        $todtArr = explode(" ", $to_date);

        // $fromDate = $fmdtArr[0];
        // $toDate = $todtArr[0];

        $fm=date('Y-m-d', strtotime($fromDate[0]));
        $to=date('Y-m-d', strtotime($toDate[0]));
        $bm = new BikeMake();

        $get_all_models_with_detail = \DB::table('bikes')
            //->select('bikes.*','bike_models.*')
            ->join('areas', 'areas.id', '=', 'bikes.area_id')
            ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->select('bikes.model_id','bike_models.bike_model','bike_models.thumbnail_img_id','bike_models.bike_make_id')
            ->where('areas.city_id', $city_id)
            ->where('bike_models.status', 1)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            ->groupBy('bikes.model_id')         
            ->get();

        // For each model add list of areas that model is available in.
       foreach($get_all_models_with_detail as $model){    
            $model->make_name = $bm->getBikeMakeName($model->bike_make_id);           
            $model->areas = $this->get_areas_by_model_id($model->model_id,$city_id, $fm, $to);
        } 

        return ['result' => $get_all_models_with_detail];

    }

   /* public function getModelProfile($from_date, $to_date, $city_id, $area_id, $model_id){
        
        $get_all_models_with_detail = \DB::table('bikes')
            //->select('bikes.*','bike_models.*')
            ->join('areas', 'areas.id', '=', 'bikes.area_id')
            ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->select('bike_models.bike_model','bikes.model_id','areas.area')
            ->where('areas.city_id', $city_id)
            ->where('areas.id', $area_id)
            ->where('bike_models.id', $model_id)
            ->where('bike_models.status', 1)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            ->groupBy('bikes.model_id')         
            ->get();

        return $get_all_models_with_detail;                 
    }*/

    public function getUniqueAccessoriesList($from_date, $to_date, $area_id){
        
        return [
                'result' => [
                                ['name' => 'Go Pro', 'image' => '1.jpg', 'description' => 'World most versatile camera', 'price' => '500'],
                                ['name' => 'Protective Gear', 'image' => '2.jpg', 'description' => 'Jacket with protective padding', 'price' => '600'],
                                ['name' => 'Saddle Bag', 'image' => '3.jpg', 'description' => 'Saddle bags for cruiser', 'price' => '700']
                            ]
            ];        
    }

    public function getUserAuthenticationDetails(Request $request){
        
        $user_email = $request->email;
        $user_password = $request->password;

        $userdata = ['email' => $user_email, 'password' => $user_password];
        
        if(!$user_email)
            return [
                        'result' => [
                                        'status' => 'Authentication failure',
                                        'error_msg' => 'Email id required'
                                    ]
                    ];                                        
        else if(!$user_password)
            return [
                        'result' => [
                                        'status' => 'Authentication failure',
                                        'error_msg' => 'Password required'
                                    ]
                    ];
        else if(Auth::attempt($userdata)){
            $fname = Auth::user()->first_name;
            $lname = Auth::user()->last_name;
            $email = Auth::user()->email;
            $mob = Auth::user()->mobile_num;
            $uid = Auth::user()->id;

            return [
                        'result' => [
                                        'status' => 'Authentication success',
                                        'user_id' => $uid,
                                        'first_name' => $fname,
                                        'last_name' => $lname,
                                        'email' => $email,
                                        'mobile_number' => $mob
                                    ]
                   ];
        }
        else{
            return [
                        'result' => ['status' => 'Authentication failure']
                   ];
        }            
    }


    public function userRegistration(Request $request){
            
            $auth_type = $request->auth_type;
            $first_name = $request->first_name;
            $last_name = $request->last_name;
            $email_id = $request->email_id;
            $password =$request->password;
            $mob_no = $request->mob_no;
            $profile_img = $request->profile_img;
            $dob = $request->dob;    

            $length = 5;
            $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);

            if(!$auth_type)
                return [
                            'result' => [
                                            'status' => 'Authentication failure',
                                            'error_msg' => 'Authentication type required'
                                        ]
                        ];  
            else if($auth_type == "email"){

                $user = User::where('email', $email_id)->first();
                
                if($user)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id already exists'
                                            ]
                            ];  
                else if(!$first_name)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'First name required'
                                            ]
                            ];
                else if(!$email_id)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id required'
                                            ]
                            ];                                        
                else if(!$password)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Password required'
                                            ]
                            ];
                else{
                    
                    $fname = ucwords(strtolower($first_name));
                    $lname = ucwords(strtolower($last_name));
                    
                    User::create(['first_name' => $fname, 'last_name' => $lname, 'email' => $email_id,
                     'password' => Hash::make($password), 'mobile_num' => $mob_no, 'referral_code'=>$referral_code]);

                    $user = User::where('email', $email_id)->first();
                    $uid = $user->id;

                    return [

                            'result' => [
                                            'status' => 'Authentication success',
                                            'user_id' => $uid,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'dob' => $dob,
                                            'mob_no' => $mob_no,
                                            'auth_type' => $auth_type,
                                            'profile_img' => '1.jpg'
                                        ]
                    ];
                }  
            }
            else if($auth_type == "fb"){

                $user = User::where('email', $email_id)->first();

                if($user)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id already exists'
                                            ]
                            ];  
                else if(!$first_name)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'First name required'
                                            ]
                            ];
                else if(!$email_id)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id required'
                                            ]
                            ];                         

                else{
                    
                    $fname = ucwords(strtolower($first_name));
                    $lname = ucwords(strtolower($last_name));
                    
                    User::create(['first_name' => $fname, 'last_name' => $lname, 'email' => $email_id, 'referral_code'=>$referral_code]);

                    $user = User::where('email', $email_id)->first();
                    $uid = $user->id;

                    return [

                            'result' => [
                                            'status' => 'Authentication success',
                                            'user_id' => $uid,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'dob' => $dob,
                                            'mob_no' => $mob_no,
                                            'auth_type' => $auth_type,
                                            'profile_img' => '1.jpg'
                                        ]
                    ];
                }
            }
            else if($auth_type == "g+"){

                $user = User::where('email', $email_id)->first();
                
                if($user)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id already exists'
                                            ]
                            ];  
                else if(!$first_name)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'First name required'
                                            ]
                            ];
                else if(!$email_id)
                    return [
                                'result' => [
                                                'status' => 'Authentication failure',
                                                'error_msg' => 'Email id required'
                                            ]
                            ];                         

                else{
                    
                    $fname = ucwords(strtolower($first_name));
                    $lname = ucwords(strtolower($last_name));
                    
                    User::create(['first_name' => $fname, 'last_name' => $lname, 'email' => $email_id, 'referral_code'=>$referral_code]); 

                    $user = User::where('email', $email_id)->first();
                    $uid = $user->id;

                    return [

                            'result' => [
                                            'status' => 'Authentication success',
                                            'user_id' => $uid,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'dob' => $dob,
                                            'mob_no' => $mob_no,
                                            'auth_type' => $auth_type,
                                            'profile_img' => '1.jpg'
                                        ]
                    ];       
                }
            }
            else
                return [
                            'result' => [
                                            'status' => 'Authentication failure',
                                            'error_msg' => 'Invalid Authentication type'
                                        ]
                        ];   
    }




    public function getAllNews(){

        $news = new News();
        $news = $news->getAllNews();
        return ['result' => $news];
    }


    public function generate_password( $length = 8 ){

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$&?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        
        return $password;
    }


    public function forgotPassword($email_id){

        $password = $this->generate_password(); 

        $user = User::where('email',$email_id)
                ->update(['password' => Hash::make($password)]);

        $usr = User::where('email', $email_id)->first();  

        $fname = $usr->first_name;

        if($user)
        {    
            $data = ['fname'=>$fname, 'password'=>$password];
            Mail::send('emails.user_forgotPassword_mail',$data,function($message) use ($email_id){
                $message->to($email_id);
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->subject('WickedRide User ForgotPassword');
            });

            return ['result' => ['status' => true]];
        }
        else
            return ['result' => ['status' => false]];
    }


    public function resetPassword(Request $request){

        $email_id = $request->email;
        $old_password = $request->old_password;
        $new_password = $request->new_password;

        $userdata = ['email' => $email_id, 'password' => $old_password];


        if(!$email_id)
            return $this->respondWithError('Authentication failure-');
        else if(!$old_password)
            return $this->respondWithError('Password reset failed');
        else if(!$new_password)
            return $this->respondWithError('Password reset failed');
        else if(Auth::attempt($userdata)){

            $password = Auth::user()->password;
            
            User::where('email', $email_id)
                    ->where('password', $password)
                    ->update(['password' => Hash::make($new_password)]); 

            return $this->respondWithSuccess('Password reset successFul');
        }
        else
            return $this->respondWithError('Password reset failed');               
    }


    public function getGalleryImages($model_id){

        return [
                    'result' => [
                                    ['img_url' => '1.jpg', 'title' => 'Img1'],
                                    ['img_url' => '2.jpg', 'title' => 'Img2'],
                                    ['img_url' => '3.jpg', 'title' => 'Img3'],
                                    ['img_url' => '4.jpg', 'title' => 'Img4'],
                                    ['img_url' => '5.jpg', 'title' => 'Img5'],
                                    ['img_url' => '6.jpg', 'title' => 'Img6'],
                                    ['img_url' => '7.jpg', 'title' => 'Img7']
                    ]
        ];
    }

    public function getFaq(){

        return ['result' =>    [
                                'content' => '<div class="our_fleet_sec"><h2>how does it work?</h2>
    <div class="width_960">
        <div class="accordian_main">
            <ul>
                <li>
                    <div class="accordian_title"><h6>How do I reserve a bike?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Click <a href="/booking/select-date" target="_blank">here</a> and select the dates you are interested in. Select the bike you like from the list of available bikes and also select the pickup location of your choice. You will be asked to login or sign up. Sign up is easy with few essential details like email address and phone number. After that, reconfirm your selected bike, dates and pickup location and you will be taken to the payment gateway. Your reservation is confirmed after you make the payment.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>From where do I pick up the bike?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            You can pick up the bike on the day of your reservation from your selected location. The address will be mentioned in the confirmation email. Click here *link* for the addresses of all the Wicked Ride locations.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6> When can I pick up the bike?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            All bikes are due to be back by 7:30 pm, so you can pick up bikes after that and before 9pm. 
                            If you want the bike on any other hour of the day, do call us and we will try our best to accommodate you.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>When should I drop off the bike?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            All bikes are due back at 7:30pm.  A grace period of 30 minutes is extended before penalties are applied.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>Do you have late return penalties?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Yes, all bikes are due back at 7:30pm. A grace period of 30 minutes is extended before penalties are applied. After the grace period, three day’s rental will be charged as penalty. If you suspect that you will be late, please call ahead and extend your reservation to avoid the penalty. We will not be able to extend if there’s another confirmed reservation for the period.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>What are the documents that I need to submit to take the bike?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            The following would be the documents that need to be submitted for renting the bike-
                            i. Valid Driving License to ride a motorcycle with gear in India.
                            ii. Address Proof. (Copy of Passport/Adhaar Card/Voter ID)
                            For international visitors, a valid driving license from their home country with International Driving Permit is necessary along with valid Visa
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>When does my order get confirmed?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Your booking will be confirmed on receipt of payment. You will receive an email from Wicked Ride confirming your booking and the address of the pick up location.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>What is the cancellation policy?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            If you cancel the booking the refund would be made as per the following schedule-
                            i.  Less than one week – No refund
                            ii. One week to two weeks – 80% refund.
                            iii. More than two weeks – 100% refund.
                            If we cancel the booking due to break down, non-availability of the bike or any other reason then we provide 100% refund of the rentals received.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>Is there any limit on the age of the rider?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Yes, the rider has to be at least 21 years of age to ride the bikes.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6> Is fuel included in the tariff?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            No, fuel is not included in the rental. The bike would be given with full tank of fuel and it has to be returned with full tank of fuel.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>Can I have the bike delivered to my location?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Yes, you can get the bike delivered to your location. Please discuss with our team on this. 
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>What is the mileage we get on these premium/high end bikes?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            We have personally experienced anywhere around 15 to 25 Kms/Litre with respect to Harleys and Triumph. However the mileage depends on riding conditions.
                            Royal Enfield will give a mileage of 20-25 kms per litre.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>Can these premium bikes break down?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Yes absolutely! Though these bikes are regularly serviced and maintained they can break down. 
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6>Is there any limit on the number of kilometers that I can ride for a day?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            Yes, there is a limit of 250 kms per day. Every 50 km above that attracts an extra charge of 10% of bike\'s daily rental. This limit is not applicable for Royal Enfield motorcycles. For example, when Street 750, which has a daily rental of Rs 4000 is rented out for 2 days, you have a limit of 500 kms. If you return with 540 km, the extra charge is 10% of Rs 4000, i.e. Rs 400. If you return with 595 km, the extra charge is 20% of Rs 4000, i.e. Rs 800.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6> What kind of fuel should I refill it with?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            All our bikes are run on Petrol (only). High Octane fuel is suggested, however normal petrol suffices.
                            No mixing of oil or any other substance is required along with the petrol.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="accordian_title"><h6> Is there any deposit that I have to pay?</h6><span></span></div>
                    <div class="open_para">
                        <p>
                            There is no requirement for cash deposit.
                        </p>
                    </div>
                </li>
            </ul>
        </div>  
    </div></div>'

                                ]
                ];
    }




    public function getTc(){

        return [ 'result' =>   ['content' => 'Wickedride terms and condition content'] ];
    }
    
    public function sentTransaction(Request $request){        
     
        
        if($request->payment_gateway == "RazorPayApp")
        {
            $startDateTime = Carbon::createFromFormat('Y-m-d H:i',$request->start_date." ".$request->start_time)->toDateTimeString();
            $endDateTime = Carbon::createFromFormat('Y-m-d H:i',$request->end_date." ".$request->end_time)->toDateTimeString();
            
            $sentTransaction = SentTransaction::create(['user_id' => $request->user_id,'model_id'=> $request->model_id,'area_id' => $request->area_id,'start_date_time' => $startDateTime,'end_date_time' => $endDateTime,'payment_gateway' => $request->payment_gateway,'order_id' => "",'amount' => $request->amount,'promocode' => $request->promocode,'status'=>'sent']);
        }
        else
        {
            $startDateTime = Carbon::createFromFormat('d-m-Y H:i',$request->start_date." ".$request->start_time)->toDateTimeString();
            $endDateTime = Carbon::createFromFormat('d-m-Y H:i',$request->end_date." ".$request->end_time)->toDateTimeString();

            $sentTransaction = SentTransaction::create(['user_id' => $request->user_id,'model_id'=> $request->model_id,'area_id' => $request->area_id,'start_date_time' => $startDateTime,'end_date_time' => $endDateTime,'payment_gateway' => $request->payment_gateway,'order_id' => $request->order_id,'amount' => $request->amount,'promocode' => $request->promocode,'status'=>'sent']);


            $parameters = ['user_id' => $request->user_id,
                           'model_id'=> $request->model_id,
                           'area_id' => $request->area_id,
                           'start_date_time' => $startDateTime,
                           'end_date_time' => $endDateTime,
                           'start_time' => $request->start_time,
                           'start_date' => $request->start_date,
                           'end_date' => $request->end_date,
                           'end_time' => $request->end_time,
                           'payment_gateway' => $request->payment_gateway,
                           'order_id' => $request->order_id,
                           'amount' => $request->amount,
                           'promocode' => $request->promocode,
                           'status'=>'sent'];

            $parametersEn = json_encode($parameters);

            $job = (new PayTM($parametersEn))->delay(600);
            $this->dispatch($job);
        }
        
        
        return $this->respondWithSuccess('Done');
        
    }
    
    public function getBikeNumbersForModel(Request $request){ 
     
        $bike_numbers = BikeNumbers::where('model',$request->model_id)->get();
        $result = "<select name=\"bike_number\" class=\"form-control\" id=\"bikeNumberId\">
        <option selected disabled> Select the bike</option>";
        foreach($bike_numbers as $bike_number)
        {
            $result.="<option value=\"".$bike_number->id."\">".$bike_number->number."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    
    
    public function getBikeNumbersForAreaModel(Request $request){ 
     
        $bike_numbers = Bike::where('model_id',$request->model_id)->where('area_id',$request->area_id)->where('reg_number','<>',"")->get();
        $result = "<select name=\"bike_number\" class=\"form-control\" id=\"bikeNumberId\">
        <option selected disabled> Select the bike</option>";
        foreach($bike_numbers as $bike_number)
        {
            $result.="<option value=\"".$bike_number->id."\">".$bike_number->reg_number."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    
    public function getAccessoriesTextIdForModelSize(Request $request){ 
     
        $accessoriesArray = Accessories::where('area',$request->from_area_id)->where('model',$request->model_id)->where('size',$request->size_id)->get();
        $result = "<select name=\"accessories_id\" class=\"form-control\" id=\"accessoriesId\">
        <option selected disabled> Select the accessories</option>";
        foreach($accessoriesArray as $accessories)
        {
            $result.="<option value=\"".$accessories->id."\">".$accessories->text_id."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    
    public function getBikeNumbersForModelAdd(Request $request){ 
     
        $booking = Booking::where('reference_id',$request->booking_id)->first();
        $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
        $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
                
        $bikeNumbersTaken = $bikeNumbersTaken->filter(function ($value) use($booking) {
            return $value != $booking->bike_number_id;
        });
        

        
        $bike_numbers = BikeNumbers::where('model',$request->model_id)->whereNotIn('id',$bikeNumbersTaken)->get();
        $result = "<select name=\"bike_number_add\" class=\"form-control\" id=\"bikeNumberIdAdd\">
        <option selected disabled> Select the bike</option>";
        foreach($bike_numbers as $bike_number)
        {
            $result.="<option value=\"".$bike_number->id."\">".$bike_number->number."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    
    public function getBikeNumbersForBooking(Request $request){ 
     
        $booking = Booking::where('id',$request->booking_id)->first();
        $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
        $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
                
        $bikeNumbersTaken = $bikeNumbersTaken->filter(function ($value) use($booking) {
            return $value != $booking->bike_number_id;
        });
        

        
        $bike_numbers = BikeNumbers::where('model',$booking->model_id)->whereNotIn('id',$bikeNumbersTaken)->get();
        
        $result = "<select id=\"bikeNumber".$request->device.$booking->id."\" class=\"form-control bikeNumberSelect\" data-id=\"".$booking->id."\"  onChange=\"applyBikeNumberChange".$request->device."(".$booking->id.")\" onfocusout=\"focusOutBikeNumber".$request->device."(".$booking->id.")\">
        <option selected disabled> Select the bike</option>";
        foreach($bike_numbers as $bike_number)
        {
            if($bike_number->id == $booking->bike_number_id)
                $result.="<option value=\"".$bike_number->id."\" selected>".$bike_number->number."</option>";
            else
                $result.="<option value=\"".$bike_number->id."\">".$bike_number->number."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    public function applyBikeNumbersForBooking(Request $request){
        
        if(isset($request->booking_id)&&isset($request->bike_number_id))
        {
            $booking_id = $request->booking_id;
            $bike_number_id = $request->bike_number_id;
            
            $booking = Booking::where('id',$booking_id)->first();
            if(!$booking)
            {
                return ["success"=>false,"message"=>"No booking found"];
            }
            else
            {
                $bikeNumber = BikeNumbers::where('id',$bike_number_id)->first();
                if(!$bikeNumber)
                {
                    return ["success"=>false,"message"=>"No Bike Number found"];
                }
                else
                {
                    $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
                    $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
                    $bikeNumbersTakenArray = $bikeNumbersTaken->toArray();
                    
                    if(in_array($bike_number_id,$bikeNumbersTakenArray))
                    {
                        if($booking->bike_number_id != $bikeNumber->id)
                        {
                            return ["success"=>false,"message"=>"Bike Number already assigned to another booking. Please refresh the screen"];
                        }
                        else
                        {
                            return ["success"=>false,"message"=>"This Bike Number is assigned for this booking. Please refresh the screen"];
                        }
                    }
                    else
                    {
                        Booking::where('id',$booking->id)->update(['bike_number_id'=>$bike_number_id]);
                        $bookingAfter = Booking::where('id',$booking->id)->first();
                        
                        $html = "<select class=\"form-control\" data-id=\"".$bookingAfter->id."\" onClick=\"onClickBikeNumber".$request->device."(".$booking->id.")\" ><option value=\"".$bookingAfter->bike_number_id."\">".$bookingAfter->getBikeNumber()."</option></select>";
                        
                        return ["success"=>true,"message"=>"This Bike Number has been successfully assigned for this booking","html"=>$html];
                    }
                }
            }

        }
        else
        {
            return ["success"=>false,"message"=>"Booking ID or Bike Number missing"];
        }
    }
    
    public function getBikeNumbersHTMLForBooking(Request $request){
        
        $booking = Booking::where('id',$request->booking_id)->first();
        if(!$booking)
        {
            return "<button type=\"button\" class=\"form-control\" onClick=\"onClickBikeNumber".$request->device."(".$booking->id.")\" data-id=\"".$booking->id."\">Assign a bike</button>";
        }
        else
        {
            if($booking->bike_number_id != 0)
            {
                return "<select class=\"form-control\" data-id=\"".$booking->id."\" onClick=\"onClickBikeNumber".$request->device."(".$booking->id.")\" ><option value=\"".$booking->bike_number_id."\">".$booking->getBikeNumber()."</option></select>";
            }
            else
            {
                return "<button type=\"button\" class=\"form-control\" onClick=\"onClickBikeNumber".$request->device."(".$booking->id.")\" data-id=\"".$booking->id."\">Assign a bike</button>";
            }
        }
    }
    public function getBikeNumbersForModelEdit(Request $request){ 
     
        $bike_numbers = BikeNumbers::where('model',$request->model_id)->get();
        $result = "<select name=\"bike_number_edit\" class=\"form-control\" id=\"bikeNumberIdEdit\">
        <option selected disabled> Select the bike</option>";
        foreach($bike_numbers as $bike_number)
        {
            $result.="<option value=\"".$bike_number->id."\">".$bike_number->number."</option>";
        }
        $result.="</select>";
        
        return $result;
        
    }
    
    
    
    
    public function getBikeNumbersForBookingApp(Request $request){ 
        
        $rules = array(
        'booking_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
     
        $booking = Booking::where('id',$request->booking_id)->first();
        $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
        $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
                
        $bikeNumbersTaken = $bikeNumbersTaken->filter(function ($value) use($booking) {
            return $value != $booking->bike_number_id;
        });
        

        
        $bike_numbers = BikeNumbers::where('model',$booking->model_id)->whereNotIn('id',$bikeNumbersTaken)->get();
        
        $result = [];
        foreach($bike_numbers as $bike_number)
        {
            if($bike_number->id == $booking->bike_number_id)
                array_push($result,["id"=>$bike_number->id,"number"=>$bike_number->number,'selected'=>true]);
            else
                array_push($result,["id"=>$bike_number->id,"number"=>$bike_number->number,'selected'=>false]);
        }
        
        return $result;
        
    }
    public function applyBikeNumbersForBookingApp(Request $request){
        
        $rules = array(
            'booking_id' => 'required',
            'bike_number_id'=> 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        
        $booking_id = $request->booking_id;
        $bike_number_id = $request->bike_number_id;

        $booking = Booking::where('id',$booking_id)->first();
        if(!$booking)
        {
            $data =  ["success"=>false,"message"=>"No booking found"];
        }
        else
        {
            $bikeNumber = BikeNumbers::where('id',$bike_number_id)->first();
            if(!$bikeNumber)
            {
                $data =  ["success"=>false,"message"=>"No Bike Number found"];
            }
            else
            {
                $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
                $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
                $bikeNumbersTakenArray = $bikeNumbersTaken->toArray();

                if(in_array($bike_number_id,$bikeNumbersTakenArray))
                {
                    if($booking->bike_number_id != $bikeNumber->id)
                    {
                        $data =  ["success"=>false,"message"=>"Bike Number already assigned to another booking. Please refresh the screen"];
                    }
                    else
                    {
                        $data =  ["success"=>false,"message"=>"This Bike Number is assigned for this booking. Please refresh the screen"];
                    }
                }
                else
                {
                    Booking::where('id',$booking->id)->update(['bike_number_id'=>$bike_number_id]);

                    $data =  ["success"=>true,"message"=>"This Bike Number has been successfully assigned for this booking"];
                }
            }
        }

        return $this->respondWithSuccess($data);

    
    }
    
    
    
    public function getBookingDetailsFromId(Request $request){ 
     
        $booking = Booking::where('id',$request->booking_id)->first();
        $delivered_bike_number = BikeNumbers::where('id',$booking->bike_number_id)->first();
        $booking['delivered_model_id'] = $delivered_bike_number->model;
        
        $collidingBookings = BookingController::getBookingsInBetweenDates($booking->start_datetime,$booking->end_datetime,0,$booking->model_id);
        $bikeNumbersTaken = $collidingBookings->pluck('bike_number_id');
        
        $bikeNumbersTaken = $bikeNumbersTaken->filter(function ($value) use($booking) {
            return $value != $booking->bike_number_id;
        });
        
        
        $bike_numbers = BikeNumbers::where('model',$delivered_bike_number->model)->whereNotIn('id',$bikeNumbersTaken)->get();
        
        $result = "<select name=\"bike_number_edit\" class=\"form-control\" id=\"bikeNumberIdEdit\">";
        foreach($bike_numbers as $bike_number)
        {
            if($bike_number->id==$booking->bike_number_id)
            {
                $result.="<option selected value=\"".$bike_number->id."\">".$bike_number->number."</option>";
            }
            else
            {
                $result.="<option value=\"".$bike_number->id."\">".$bike_number->number."</option>";
            }
        }
        $result.="</select>";
        $booking['bike_numbers_options']=$result;
        $booking['delivered_at_area_name'] = Area::where('id',$booking->delivered_at_area)->pluck('area');
        $booking['delivered_model_name'] = BikeModel::where('id',$booking->delivered_model_id)->pluck('bike_model');
        $booking['username']=$booking->getUserNameThis();
        
        $booking['ExtraKMPrice']=$booking->getExtraKMPrice();
        $booking['accessories_label'] = $booking->getAccessoryLabel();
        $booking['delivered_bike_number'] = $delivered_bike_number->number;
        
        
        
        return $booking;
        
    }
    
    
    public function getReportData(Request $request)
    {
        $state = $request->state;
        if($state == "KA")
            $areasInState = [1,2,3,6,8,10,11,14,15,16,17,18,19,20,22,23,27,28,29,30];
        elseif($state == "GJ")
            $areasInState = [7,9];
        elseif($state == "RJ")
            $areasInState = [4,5,12];
        else
            $areasInState = [1,2,3,6,8,10,11,14,15,16,17,18,19,20,22,23,27,28,29,30,7,9,4,5,12,13,21,24,25];        
     
        $result = Self::getReportDataFromDB($request->data_start_date,$request->data_end_date);
        
        return $result;

    }
    
    public function getReportDataEveryDay(Request $request)
    {
        
        
        $data_start_date = $request->data_start_date;
        $data_end_date = $request->data_end_date;
        $data_start_date_ts = date('Y-m-d H:i:s',strtotime($data_start_date));
        $data_end_date_ts = date('Y-m-d H:i:s',strtotime($data_end_date . "+1 days"));
        
        $app_bookings_for_data_period['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->count();
        
        $app_bookings_for_data_period['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('total_price');
        
        if($app_bookings_for_data_period['total_txn_count']!=0)
        {
            $app_bookings_for_data_period['average_txn_value'] = round($app_bookings_for_data_period['total_txn_amount']/$app_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $app_bookings_for_data_period['average_txn_value']=0;
        }
        
        $app_bookings_for_data_period['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('weekend_hours');
        
        $app_bookings_for_data_period['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('weekday_hours');
        
        if($app_bookings_for_data_period['total_txn_count']!=0)
        {
            $app_bookings_for_data_period['average_txn_duration'] = round(($app_bookings_for_data_period['total_weekend_hours']+$app_bookings_for_data_period['total_weekday_hours'])/$app_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $app_bookings_for_data_period['average_txn_duration'] = 0;
            
        }
        if(($app_bookings_for_data_period['total_weekend_hours']+$app_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $app_bookings_for_data_period['weekend_hours_perc'] = round($app_bookings_for_data_period['total_weekend_hours']/($app_bookings_for_data_period['total_weekend_hours']+$app_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $app_bookings_for_data_period['weekend_hours_perc']=0;
        }
        
        
        if(($app_bookings_for_data_period['total_weekend_hours']+$app_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $app_bookings_for_data_period['weekday_hours_perc'] = round($app_bookings_for_data_period['total_weekday_hours']/($app_bookings_for_data_period['total_weekend_hours']+$app_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $app_bookings_for_data_period['weekday_hours_perc']=0;   
        }

        $app_bookings_for_data_period['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->avg('res_lead_time'));


        
        
        $web_bookings_for_data_period['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->count();
        
        $web_bookings_for_data_period['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('total_price');
        
        if($web_bookings_for_data_period['total_txn_count']!=0)
        {
            $web_bookings_for_data_period['average_txn_value'] = round($web_bookings_for_data_period['total_txn_amount']/$web_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $web_bookings_for_data_period['average_txn_value'] = 0;
        }
        
        $web_bookings_for_data_period['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('weekend_hours');
        
        
        $web_bookings_for_data_period['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->sum('weekday_hours');
        
        if($web_bookings_for_data_period['total_txn_count']!=0)
        {
            $web_bookings_for_data_period['average_txn_duration'] = round(($web_bookings_for_data_period['total_weekend_hours']+$web_bookings_for_data_period['total_weekday_hours'])/$web_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $web_bookings_for_data_period['average_txn_duration'] = 0;
        }
        
        if(($web_bookings_for_data_period['total_weekend_hours']+$web_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $web_bookings_for_data_period['weekend_hours_perc'] = round($web_bookings_for_data_period['total_weekend_hours']/($web_bookings_for_data_period['total_weekend_hours']+$web_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $web_bookings_for_data_period['weekend_hours_perc'] = 0;
        }
        
        if(($web_bookings_for_data_period['total_weekend_hours']+$web_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $web_bookings_for_data_period['weekday_hours_perc'] = round($web_bookings_for_data_period['total_weekday_hours']/($web_bookings_for_data_period['total_weekend_hours']+$web_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $web_bookings_for_data_period['weekday_hours_perc'] = 0;
        }
        
       
        $web_bookings_for_data_period['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->avg('res_lead_time'));
        
        
        
        $assisted_bookings_for_data_period['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->count();
        
        
        $assisted_bookings_for_data_period['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->sum('total_price');
        
        if($assisted_bookings_for_data_period['total_txn_count']!=0)
        {
            $assisted_bookings_for_data_period['average_txn_value'] = round($assisted_bookings_for_data_period['total_txn_amount']/$assisted_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $assisted_bookings_for_data_period['average_txn_value'] = 0;
        }
        
        $assisted_bookings_for_data_period['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->sum('weekend_hours');
        
        $assisted_bookings_for_data_period['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->sum('weekday_hours');
        
        if($assisted_bookings_for_data_period['total_txn_count']!=0)
        {
            $assisted_bookings_for_data_period['average_txn_duration'] = round(($assisted_bookings_for_data_period['total_weekend_hours']+$assisted_bookings_for_data_period['total_weekday_hours'])/$assisted_bookings_for_data_period['total_txn_count']);
        }
        else
        {
            $assisted_bookings_for_data_period['average_txn_duration'] = 0;
        }
        
        if(($assisted_bookings_for_data_period['total_weekend_hours']+$assisted_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $assisted_bookings_for_data_period['weekend_hours_perc'] = round($assisted_bookings_for_data_period['total_weekend_hours']/($assisted_bookings_for_data_period['total_weekend_hours']+$assisted_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $assisted_bookings_for_data_period['weekend_hours_perc'] = 0;
        }
        
        if(($assisted_bookings_for_data_period['total_weekend_hours']+$assisted_bookings_for_data_period['total_weekday_hours'])!=0)
        {
            $assisted_bookings_for_data_period['weekday_hours_perc'] = round($assisted_bookings_for_data_period['total_weekday_hours']/($assisted_bookings_for_data_period['total_weekend_hours']+$assisted_bookings_for_data_period['total_weekday_hours'])*100);
        }
        else
        {
            $assisted_bookings_for_data_period['weekday_hours_perc'] = 0;
        }
        
        $assisted_bookings_for_data_period['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->avg('res_lead_time'));
        
        if(($app_bookings_for_data_period['total_txn_count']+$web_bookings_for_data_period['total_txn_count']+$assisted_bookings_for_data_period['total_txn_count'])!=0)
        {
            $app_bookings_for_data_period['txn_count_perc'] = round($app_bookings_for_data_period['total_txn_count']/($app_bookings_for_data_period['total_txn_count']+$web_bookings_for_data_period['total_txn_count']+$assisted_bookings_for_data_period['total_txn_count'])*100);
                
            $web_bookings_for_data_period['txn_count_perc'] = round($web_bookings_for_data_period['total_txn_count']/($app_bookings_for_data_period['total_txn_count']+$web_bookings_for_data_period['total_txn_count']+$assisted_bookings_for_data_period['total_txn_count'])*100);

            $assisted_bookings_for_data_period['txn_count_perc'] = round($assisted_bookings_for_data_period['total_txn_count']/($app_bookings_for_data_period['total_txn_count']+$web_bookings_for_data_period['total_txn_count']+$assisted_bookings_for_data_period['total_txn_count'])*100);
        
        }
        else
        {
            $app_bookings_for_data_period['txn_count_perc'] = 0;
            $web_bookings_for_data_period['txn_count_perc'] = 0;
            $assisted_bookings_for_data_period['txn_count_perc'] = 0;
            
        }
        if(($app_bookings_for_data_period['total_txn_amount']+$web_bookings_for_data_period['total_txn_amount']+$assisted_bookings_for_data_period['total_txn_amount']) != 0)
        {
        
            $app_bookings_for_data_period['txn_amount_perc'] = round($app_bookings_for_data_period['total_txn_amount']/($app_bookings_for_data_period['total_txn_amount']+$web_bookings_for_data_period['total_txn_amount']+$assisted_bookings_for_data_period['total_txn_amount'])*100);

            $web_bookings_for_data_period['txn_amount_perc'] = round($web_bookings_for_data_period['total_txn_amount']/($app_bookings_for_data_period['total_txn_amount']+$web_bookings_for_data_period['total_txn_amount']+$assisted_bookings_for_data_period['total_txn_amount'])*100);

            $assisted_bookings_for_data_period['txn_amount_perc'] = round($assisted_bookings_for_data_period['total_txn_amount']/($app_bookings_for_data_period['total_txn_amount']+$web_bookings_for_data_period['total_txn_amount']+$assisted_bookings_for_data_period['total_txn_amount'])*100);
        }
        else
        {
            $app_bookings_for_data_period['txn_amount_perc'] = 0;
            $web_bookings_for_data_period['txn_amount_perc'] = 0;
            $assisted_bookings_for_data_period['txn_amount_perc'] = 0;

        }
        
        
        $app_bookings_for_data_period['areas'] = [];
        $web_bookings_for_data_period['areas'] = [];
        $assisted_bookings_for_data_period['areas'] = [];
        
        $app_bookings_for_data_period['models'] = [];
        $web_bookings_for_data_period['models'] = [];
        $assisted_bookings_for_data_period['models'] = [];
        
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        
        foreach($areas as $area)
        {
            
            $app_bookings_for_data_period_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->count();
            
            if($app_bookings_for_data_period_area['total_txn_count']>0)
            {
        
                $app_bookings_for_data_period_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('total_price');
                
                $app_bookings_for_data_period_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('weekend_hours');

                $app_bookings_for_data_period_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('weekday_hours');

                if($app_bookings_for_data_period_area['total_txn_count']!=0)
                {
                    $app_bookings_for_data_period_area['average_txn_value'] = round($app_bookings_for_data_period_area['total_txn_amount']/$app_bookings_for_data_period_area['total_txn_count']);
                    
                    $app_bookings_for_data_period_area['average_txn_duration'] = round(($app_bookings_for_data_period_area['total_weekend_hours']+$app_bookings_for_data_period_area['total_weekday_hours'])/$app_bookings_for_data_period_area['total_txn_count']);
                }
                else
                {
                    $app_bookings_for_data_period_area['average_txn_value'] = 0;
                    $app_bookings_for_data_period_area['average_txn_duration'] = 0;
                    
                }

                

                if(($app_bookings_for_data_period_area['total_weekend_hours']+$app_bookings_for_data_period_area['total_weekday_hours'])!=0)
                {
                    $app_bookings_for_data_period_area['weekend_hours_perc'] = round($app_bookings_for_data_period_area['total_weekend_hours']/($app_bookings_for_data_period_area['total_weekend_hours']+$app_bookings_for_data_period_area['total_weekday_hours'])*100);

                    $app_bookings_for_data_period_area['weekday_hours_perc'] = round($app_bookings_for_data_period_area['total_weekday_hours']/($app_bookings_for_data_period_area['total_weekend_hours']+$app_bookings_for_data_period_area['total_weekday_hours'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_area['weekend_hours_perc'] = 0;
                    $app_bookings_for_data_period_area['weekday_hours_perc'] = 0;
                }
                $app_bookings_for_data_period_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->avg('res_lead_time'));
                
                if($app_bookings_for_data_period['total_txn_count']!=0)
                {
                    $app_bookings_for_data_period_area['txn_count_perc'] = round(($app_bookings_for_data_period_area['total_txn_count']/$app_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_area['txn_count_perc'] = 0;   
                }
                
                if($app_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $app_bookings_for_data_period_area['txn_amount_perc'] = round(($app_bookings_for_data_period_area['total_txn_amount']/$app_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_area['txn_amount_perc'] = 0;
                }

                $app_bookings_for_data_period_area['area_name'] = $area->area;

                $app_bookings_for_data_period_area['models_in_area'] = [];
                
                
                foreach($models as $model)
                {
                    $app_bookings_for_data_period_area_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->count();

                    if($app_bookings_for_data_period_area_model['total_txn_count']>0)
                    {

                        $app_bookings_for_data_period_area_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('total_price');
                        
                        $app_bookings_for_data_period_area_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekend_hours');

                        $app_bookings_for_data_period_area_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekday_hours');
                        
                        if($app_bookings_for_data_period_area_model['total_txn_count']!=0)
                        {
                            $app_bookings_for_data_period_area_model['average_txn_value'] = round($app_bookings_for_data_period_area_model['total_txn_amount']/$app_bookings_for_data_period_area_model['total_txn_count']);
                            
                            $app_bookings_for_data_period_area_model['average_txn_duration'] = round(($app_bookings_for_data_period_area_model['total_weekend_hours']+$app_bookings_for_data_period_area_model['total_weekday_hours'])/$app_bookings_for_data_period_area_model['total_txn_count']);
                        }
                        else
                        {
                            $app_bookings_for_data_period_area_model['average_txn_value'] = 0;
                            $app_bookings_for_data_period_area_model['average_txn_duration'] = 0;
                        }

            
                        if(($app_bookings_for_data_period_area_model['total_weekend_hours']+$app_bookings_for_data_period_area_model['total_weekday_hours'])!=0)
                        {

                            $app_bookings_for_data_period_area_model['weekend_hours_perc'] = round($app_bookings_for_data_period_area_model['total_weekend_hours']/($app_bookings_for_data_period_area_model['total_weekend_hours']+$app_bookings_for_data_period_area_model['total_weekday_hours'])*100);

                        
                            $app_bookings_for_data_period_area_model['weekday_hours_perc'] = round($app_bookings_for_data_period_area_model['total_weekday_hours']/($app_bookings_for_data_period_area_model['total_weekend_hours']+$app_bookings_for_data_period_area_model['total_weekday_hours'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_area_model['weekend_hours_perc'] = 0;
                            $app_bookings_for_data_period_area_model['weekday_hours_perc'] = 0;
                        }

                        $app_bookings_for_data_period_area_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->avg('res_lead_time'));
                        
                        
                        if($app_bookings_for_data_period_area['total_txn_count']!=0)
                        {
                            $app_bookings_for_data_period_area_model['txn_count_perc'] = round(($app_bookings_for_data_period_area_model['total_txn_count']/$app_bookings_for_data_period_area['total_txn_count'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_area_model['txn_count_perc'] = 0;
                        }
                        
                        if($app_bookings_for_data_period_area['total_txn_amount']!=0)
                        {
                            $app_bookings_for_data_period_area_model['txn_amount_perc'] = round(($app_bookings_for_data_period_area_model['total_txn_amount']/$app_bookings_for_data_period_area['total_txn_amount'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_area_model['txn_amount_perc'] = 0;
                        }

                        $app_bookings_for_data_period_area_model['model_name'] = $model->bike_model;
                        
                        $app_bookings_for_data_period_area['models_in_area'][$model->bike_model]= $app_bookings_for_data_period_area_model;

                    }
                    
                }
                $app_bookings_for_data_period['areas'][$area->area]=$app_bookings_for_data_period_area;
                
            }
            




            $web_bookings_for_data_period_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->count();
            
            if($web_bookings_for_data_period_area['total_txn_count']>0)
            {

                $web_bookings_for_data_period_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('total_price');
                
                $web_bookings_for_data_period_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('weekend_hours');

                $web_bookings_for_data_period_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->sum('weekday_hours');
                
                if($web_bookings_for_data_period_area['total_txn_count']!=0)
                {
                    $web_bookings_for_data_period_area['average_txn_value'] = round($web_bookings_for_data_period_area['total_txn_amount']/$web_bookings_for_data_period_area['total_txn_count']);
                    $web_bookings_for_data_period_area['average_txn_duration'] = round(($web_bookings_for_data_period_area['total_weekend_hours']+$web_bookings_for_data_period_area['total_weekday_hours'])/$web_bookings_for_data_period_area['total_txn_count']);
                }
                else
                {
                    $web_bookings_for_data_period_area['average_txn_value'] = 0;
                    $web_bookings_for_data_period_area['average_txn_duration'] = 0;
                }
                
                

                

                if(($web_bookings_for_data_period_area['total_weekend_hours']+$web_bookings_for_data_period_area['total_weekday_hours'])!=0)
                {
                    $web_bookings_for_data_period_area['weekend_hours_perc'] = round($web_bookings_for_data_period_area['total_weekend_hours']/($web_bookings_for_data_period_area['total_weekend_hours']+$web_bookings_for_data_period_area['total_weekday_hours'])*100);

                    $web_bookings_for_data_period_area['weekday_hours_perc'] = round($web_bookings_for_data_period_area['total_weekday_hours']/($web_bookings_for_data_period_area['total_weekend_hours']+$web_bookings_for_data_period_area['total_weekday_hours'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_area['weekend_hours_perc'] = 0;
                    $web_bookings_for_data_period_area['weekday_hours_perc'] = 0;
                }

                $web_bookings_for_data_period_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->avg('res_lead_time'));

                if($web_bookings_for_data_period['total_txn_count']!=0)
                {
                    $web_bookings_for_data_period_area['txn_count_perc'] = round(($web_bookings_for_data_period_area['total_txn_count']/$web_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_area['txn_count_perc'] = 0;   
                }
                

                if($web_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $web_bookings_for_data_period_area['txn_amount_perc'] = round(($web_bookings_for_data_period_area['total_txn_amount']/$web_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_area['txn_amount_perc'] = 0;
                }

                $web_bookings_for_data_period_area['area_name'] = $area->area;

                $web_bookings_for_data_period_area['models_in_area'] = [];
                
                foreach($models as $model)
                {

                    $web_bookings_for_data_period_area_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->count();

                    if($web_bookings_for_data_period_area_model['total_txn_count']>0)
                    {

                        $web_bookings_for_data_period_area_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('total_price');

                        $web_bookings_for_data_period_area_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekend_hours');

                        $web_bookings_for_data_period_area_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekday_hours');
                        
                        if($web_bookings_for_data_period_area_model['total_txn_count']!=0)
                        {
                            $web_bookings_for_data_period_area_model['average_txn_value'] = round($web_bookings_for_data_period_area_model['total_txn_amount']/$web_bookings_for_data_period_area_model['total_txn_count']);
                            $web_bookings_for_data_period_area_model['average_txn_duration'] = round(($web_bookings_for_data_period_area_model['total_weekend_hours']+$web_bookings_for_data_period_area_model['total_weekday_hours'])/$web_bookings_for_data_period_area_model['total_txn_count']);
                        }
                        else
                        {
                            $web_bookings_for_data_period_area_model['average_txn_value'] = 0;
                            $web_bookings_for_data_period_area_model['average_txn_duration'] = 0;
                        }
                        

                        if(($web_bookings_for_data_period_area_model['total_weekend_hours']+$web_bookings_for_data_period_area_model['total_weekday_hours'])!=0)
                        {
                            $web_bookings_for_data_period_area_model['weekend_hours_perc'] = round($web_bookings_for_data_period_area_model['total_weekend_hours']/($web_bookings_for_data_period_area_model['total_weekend_hours']+$web_bookings_for_data_period_area_model['total_weekday_hours'])*100);

                            $web_bookings_for_data_period_area_model['weekday_hours_perc'] = round($web_bookings_for_data_period_area_model['total_weekday_hours']/($web_bookings_for_data_period_area_model['total_weekend_hours']+$web_bookings_for_data_period_area_model['total_weekday_hours'])*100);
                        }
                        else
                        {
                            $web_bookings_for_data_period_area_model['weekend_hours_perc'] = 0;
                            $web_bookings_for_data_period_area_model['weekday_hours_perc'] = 0;
                        }

                        $web_bookings_for_data_period_area_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('area_id',$area->id)->where('model_id',$model->id)->avg('res_lead_time'));

                        if($web_bookings_for_data_period_area['total_txn_count']!=0)
                        {
                            $web_bookings_for_data_period_area_model['txn_count_perc'] = round(($web_bookings_for_data_period_area_model['total_txn_count']/$web_bookings_for_data_period_area['total_txn_count'])*100);
                        }
                        else
                        {
                            $web_bookings_for_data_period_area_model['txn_count_perc'] = 0;
                        }
                        
                        if($web_bookings_for_data_period_area['total_txn_amount']!=0)
                        {
                            $web_bookings_for_data_period_area_model['txn_amount_perc'] = round(($web_bookings_for_data_period_area_model['total_txn_amount']/$web_bookings_for_data_period_area['total_txn_amount'])*100);
                        }
                        else
                        {
                            $web_bookings_for_data_period_area_model['txn_amount_perc'] = 0;   
                        }
                        
                        $web_bookings_for_data_period_area_model['model_name'] = $model->bike_model;
                        
                        $web_bookings_for_data_period_area['models_in_area'][$model->bike_model]= $web_bookings_for_data_period_area_model;

                    }
                    
                }
                $web_bookings_for_data_period['areas'][$area->area]=$web_bookings_for_data_period_area;
            }



            $assisted_bookings_for_data_period_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->count();
            
            if($assisted_bookings_for_data_period_area['total_txn_count']>0)
            {

                $assisted_bookings_for_data_period_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->sum('total_price');
                
                $assisted_bookings_for_data_period_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->sum('weekend_hours');

                $assisted_bookings_for_data_period_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->sum('weekday_hours');

                if($assisted_bookings_for_data_period_area['total_txn_count']!=0)
                {
                    $assisted_bookings_for_data_period_area['average_txn_value'] = round($assisted_bookings_for_data_period_area['total_txn_amount']/$assisted_bookings_for_data_period_area['total_txn_count']);
                    $assisted_bookings_for_data_period_area['average_txn_duration'] = round(($assisted_bookings_for_data_period_area['total_weekend_hours']+$assisted_bookings_for_data_period_area['total_weekday_hours'])/$assisted_bookings_for_data_period_area['total_txn_count']);
                }
                else
                {
                    $assisted_bookings_for_data_period_area['average_txn_value'] = 0;
                    $assisted_bookings_for_data_period_area['average_txn_duration'] = 0;
                }
                

                if(($assisted_bookings_for_data_period_area['total_weekend_hours']+$assisted_bookings_for_data_period_area['total_weekday_hours'])!=0)
                {
                    $assisted_bookings_for_data_period_area['weekend_hours_perc'] = round($assisted_bookings_for_data_period_area['total_weekend_hours']/($assisted_bookings_for_data_period_area['total_weekend_hours']+$assisted_bookings_for_data_period_area['total_weekday_hours'])*100);

                    $assisted_bookings_for_data_period_area['weekday_hours_perc'] = round($assisted_bookings_for_data_period_area['total_weekday_hours']/($assisted_bookings_for_data_period_area['total_weekend_hours']+$assisted_bookings_for_data_period_area['total_weekday_hours'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_area['weekend_hours_perc'] = 0;
                    $assisted_bookings_for_data_period_area['weekday_hours_perc'] = 0;
                }
                
                $assisted_bookings_for_data_period_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->avg('res_lead_time'));

                if($assisted_bookings_for_data_period['total_txn_count']!=0)
                {
                    $assisted_bookings_for_data_period_area['txn_count_perc'] = round(($assisted_bookings_for_data_period_area['total_txn_count']/$assisted_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_area['txn_count_perc'] = 0;
                }
                
                if($assisted_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $assisted_bookings_for_data_period_area['txn_amount_perc'] = round(($assisted_bookings_for_data_period_area['total_txn_amount']/$assisted_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_area['txn_amount_perc'] = 0;
                }
                
                $assisted_bookings_for_data_period_area['area_name'] = $area->area;
                
                $assisted_bookings_for_data_period_area['models_in_area'] = [];
                
                foreach($models as $model)
                {

                    $assisted_bookings_for_data_period_area_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->where('model_id',$model->id)->count();

                    if($assisted_bookings_for_data_period_area_model['total_txn_count']>0)
                    {

                        $assisted_bookings_for_data_period_area_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->where('model_id',$model->id)->sum('total_price');
                        
                        $assisted_bookings_for_data_period_area_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekend_hours');

                        $assisted_bookings_for_data_period_area_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->where('model_id',$model->id)->sum('weekday_hours');

                        if($assisted_bookings_for_data_period_area_model['total_txn_count']!=0)
                        {
                            $assisted_bookings_for_data_period_area_model['average_txn_value'] = round($assisted_bookings_for_data_period_area_model['total_txn_amount']/$assisted_bookings_for_data_period_area_model['total_txn_count']);
                            $assisted_bookings_for_data_period_area_model['average_txn_duration'] = round(($assisted_bookings_for_data_period_area_model['total_weekend_hours']+$assisted_bookings_for_data_period_area_model['total_weekday_hours'])/$assisted_bookings_for_data_period_area_model['total_txn_count']);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_area_model['average_txn_value'] = 0;
                            $assisted_bookings_for_data_period_area_model['average_txn_duration'] = 0;
                        }

                        
                        if(($assisted_bookings_for_data_period_area_model['total_weekend_hours']+$assisted_bookings_for_data_period_area_model['total_weekday_hours'])!=0)
                        {
                            $assisted_bookings_for_data_period_area_model['weekend_hours_perc'] = round($assisted_bookings_for_data_period_area_model['total_weekend_hours']/($assisted_bookings_for_data_period_area_model['total_weekend_hours']+$assisted_bookings_for_data_period_area_model['total_weekday_hours'])*100);

                            $assisted_bookings_for_data_period_area_model['weekday_hours_perc'] = round($assisted_bookings_for_data_period_area_model['total_weekday_hours']/($assisted_bookings_for_data_period_area_model['total_weekend_hours']+$assisted_bookings_for_data_period_area_model['total_weekday_hours'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_area_model['weekend_hours_perc'] = 0;
                            $assisted_bookings_for_data_period_area_model['weekday_hours_perc'] = 0;
                        }

                        $assisted_bookings_for_data_period_area_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('area_id',$area->id)->where('model_id',$model->id)->avg('res_lead_time')); 
                        
                        if($assisted_bookings_for_data_period_area['total_txn_count']!=0)
                        {
                            $assisted_bookings_for_data_period_area_model['txn_count_perc'] = round(($assisted_bookings_for_data_period_area_model['total_txn_count']/$assisted_bookings_for_data_period_area['total_txn_count'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_area_model['txn_count_perc'] = 0;
                        }
                        
                        if($assisted_bookings_for_data_period_area['total_txn_amount']!=0)
                        {
                            $assisted_bookings_for_data_period_area_model['txn_amount_perc'] = round(($assisted_bookings_for_data_period_area_model['total_txn_amount']/$assisted_bookings_for_data_period_area['total_txn_amount'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_area_model['txn_amount_perc'] = 0;
                        }
                        
                        $assisted_bookings_for_data_period_area_model['model_name'] = $model->bike_model;
                        
                        $assisted_bookings_for_data_period_area['models_in_area'][$model->bike_model]= $assisted_bookings_for_data_period_area_model;

                    }
                    
                }
                $assisted_bookings_for_data_period['areas'][$area->area]=$assisted_bookings_for_data_period_area;
            }
            
            
                
        }
        
        
        
        foreach($models as $model)
        {
                        
            $app_bookings_for_data_period_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->count();
            
            if($app_bookings_for_data_period_model['total_txn_count']>0)
            {
        
                $app_bookings_for_data_period_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('total_price');

                $app_bookings_for_data_period_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('weekend_hours');

                $app_bookings_for_data_period_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('weekday_hours');
                
                if($app_bookings_for_data_period_model['total_txn_count']!=0)
                {
                    $app_bookings_for_data_period_model['average_txn_value'] = round($app_bookings_for_data_period_model['total_txn_amount']/$app_bookings_for_data_period_model['total_txn_count']);
                    $app_bookings_for_data_period_model['average_txn_duration'] = round(($app_bookings_for_data_period_model['total_weekend_hours']+$app_bookings_for_data_period_model['total_weekday_hours'])/$app_bookings_for_data_period_model['total_txn_count']);
                }
                else
                {
                    $app_bookings_for_data_period_model['average_txn_value'] = 0;
                    $app_bookings_for_data_period_model['average_txn_duration'] = 0;
                }
                
                if(($app_bookings_for_data_period_model['total_weekend_hours']+$app_bookings_for_data_period_model['total_weekday_hours'])!=0)
                {
                    $app_bookings_for_data_period_model['weekend_hours_perc'] = round($app_bookings_for_data_period_model['total_weekend_hours']/($app_bookings_for_data_period_model['total_weekend_hours']+$app_bookings_for_data_period_model['total_weekday_hours'])*100);

                    $app_bookings_for_data_period_model['weekday_hours_perc'] = round($app_bookings_for_data_period_model['total_weekday_hours']/($app_bookings_for_data_period_model['total_weekend_hours']+$app_bookings_for_data_period_model['total_weekday_hours'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_model['weekend_hours_perc'] = 0;
                    $app_bookings_for_data_period_model['weekday_hours_perc'] = 0;                 
                }
                
                $app_bookings_for_data_period_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->avg('res_lead_time'));

                if($app_bookings_for_data_period['total_txn_count']!=0)
                {
                    $app_bookings_for_data_period_model['txn_count_perc'] = round(($app_bookings_for_data_period_model['total_txn_count']/$app_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_model['txn_count_perc'] = 0;
                }
                
                if($app_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $app_bookings_for_data_period_model['txn_amount_perc'] = round(($app_bookings_for_data_period_model['total_txn_amount']/$app_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $app_bookings_for_data_period_model['txn_amount_perc'] = 0;   
                }
                
                $app_bookings_for_data_period_model['model_name'] = $model->bike_model;

                $app_bookings_for_data_period_model['areas_with_model'] = [];
                
                
                foreach($areas as $area)
                {
                    $app_bookings_for_data_period_model_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->count();

                    if($app_bookings_for_data_period_model_area['total_txn_count']>0)
                    {

                        $app_bookings_for_data_period_model_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('total_price');

                        $app_bookings_for_data_period_model_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekend_hours');

                        $app_bookings_for_data_period_model_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekday_hours');
                        
                        if($app_bookings_for_data_period_model_area['total_txn_count']!=0)
                        {
                            $app_bookings_for_data_period_model_area['average_txn_value'] = round($app_bookings_for_data_period_model_area['total_txn_amount']/$app_bookings_for_data_period_model_area['total_txn_count']);
                            $app_bookings_for_data_period_model_area['average_txn_duration'] = round(($app_bookings_for_data_period_model_area['total_weekend_hours']+$app_bookings_for_data_period_model_area['total_weekday_hours'])/$app_bookings_for_data_period_model_area['total_txn_count']);

                        }
                        else
                        {
                            $app_bookings_for_data_period_model_area['average_txn_value'] = 0;
                            $app_bookings_for_data_period_model_area['average_txn_duration'] = 0;
                        }
                        
                        if(($app_bookings_for_data_period_model_area['total_weekend_hours']+$app_bookings_for_data_period_model_area['total_weekday_hours'])!=0)
                        {
                            $app_bookings_for_data_period_model_area['weekend_hours_perc'] = round($app_bookings_for_data_period_model_area['total_weekend_hours']/($app_bookings_for_data_period_model_area['total_weekend_hours']+$app_bookings_for_data_period_model_area['total_weekday_hours'])*100);

                            $app_bookings_for_data_period_model_area['weekday_hours_perc'] = round($app_bookings_for_data_period_model_area['total_weekday_hours']/($app_bookings_for_data_period_model_area['total_weekend_hours']+$app_bookings_for_data_period_model_area['total_weekday_hours'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_model_area['weekend_hours_perc'] = 0;
                            $app_bookings_for_data_period_model_area['weekday_hours_perc'] = 0;
                        }

                        $app_bookings_for_data_period_model_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->whereIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->avg('res_lead_time'));

                        if($app_bookings_for_data_period_model['total_txn_count']!=0)
                        {
                            $app_bookings_for_data_period_model_area['txn_count_perc'] = round(($app_bookings_for_data_period_model_area['total_txn_count']/$app_bookings_for_data_period_model['total_txn_count'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_model_area['txn_count_perc'] = 0;   
                        }
                        
                        if($app_bookings_for_data_period_model['total_txn_amount']!=0)
                        {
                            $app_bookings_for_data_period_model_area['txn_amount_perc'] = round(($app_bookings_for_data_period_model_area['total_txn_amount']/$app_bookings_for_data_period_model['total_txn_amount'])*100);
                        }
                        else
                        {
                            $app_bookings_for_data_period_model_area['txn_amount_perc'] = 0;   
                        }
                        
                        $app_bookings_for_data_period_model_area['area_name'] = $area->area;
                        
                        $app_bookings_for_data_period_model['areas_with_model'][$area->area] = $app_bookings_for_data_period_model_area;

                    }
                    
                }
                
                $app_bookings_for_data_period['models'][$model->bike_model] = $app_bookings_for_data_period_model;
                
            }
            




            $web_bookings_for_data_period_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->count();
            
            if($web_bookings_for_data_period_model['total_txn_count']>0)
            {

                $web_bookings_for_data_period_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('total_price');
                
                $web_bookings_for_data_period_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('weekend_hours');

                $web_bookings_for_data_period_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->sum('weekday_hours');

                if($web_bookings_for_data_period_model['total_txn_count']!=0)
                {
                    $web_bookings_for_data_period_model['average_txn_value'] = round($web_bookings_for_data_period_model['total_txn_amount']/$web_bookings_for_data_period_model['total_txn_count']);

                    $web_bookings_for_data_period_model['average_txn_duration'] = round(($web_bookings_for_data_period_model['total_weekend_hours']+$web_bookings_for_data_period_model['total_weekday_hours'])/$web_bookings_for_data_period_model['total_txn_count']);
                }
                else
                {
                    $web_bookings_for_data_period_model['average_txn_value'] = 0;
                    $web_bookings_for_data_period_model['average_txn_duration'] = 0;
                }
                
                if(($web_bookings_for_data_period_model['total_weekend_hours']+$web_bookings_for_data_period_model['total_weekday_hours'])!=0)
                {
                    $web_bookings_for_data_period_model['weekend_hours_perc'] = round($web_bookings_for_data_period_model['total_weekend_hours']/($web_bookings_for_data_period_model['total_weekend_hours']+$web_bookings_for_data_period_model['total_weekday_hours'])*100);

                    $web_bookings_for_data_period_model['weekday_hours_perc'] = round($web_bookings_for_data_period_model['total_weekday_hours']/($web_bookings_for_data_period_model['total_weekend_hours']+$web_bookings_for_data_period_model['total_weekday_hours'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_model['weekend_hours_perc'] = 0;
                    $web_bookings_for_data_period_model['weekday_hours_perc'] = 0;
                }
                
                $web_bookings_for_data_period_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->avg('res_lead_time'));

                if($web_bookings_for_data_period['total_txn_count']!=0)
                {
                    $web_bookings_for_data_period_model['txn_count_perc'] = round(($web_bookings_for_data_period_model['total_txn_count']/$web_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_model['txn_count_perc'] = 0;
                }
                
                if($web_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $web_bookings_for_data_period_model['txn_amount_perc'] = round(($web_bookings_for_data_period_model['total_txn_amount']/$web_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $web_bookings_for_data_period_model['txn_amount_perc'] = 0;
                }
                
                $web_bookings_for_data_period_model['model_name'] = $model->bike_model;

                $web_bookings_for_data_period_model['areas_with_model'] = [];
                
                foreach($areas as $area)
                {

                    $web_bookings_for_data_period_model_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->count();

                    if($web_bookings_for_data_period_model_area['total_txn_count']>0)
                    {

                        $web_bookings_for_data_period_model_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('total_price');
                        
                        $web_bookings_for_data_period_model_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekend_hours');

                        $web_bookings_for_data_period_model_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekday_hours');

                        if($web_bookings_for_data_period_model_area['total_txn_count']!=0)
                        {
                            $web_bookings_for_data_period_model_area['average_txn_value'] = round($web_bookings_for_data_period_model_area['total_txn_amount']/$web_bookings_for_data_period_model_area['total_txn_count']);
                            $web_bookings_for_data_period_model_area['average_txn_duration'] = round(($web_bookings_for_data_period_model_area['total_weekend_hours']+$web_bookings_for_data_period_model_area['total_weekday_hours'])/$web_bookings_for_data_period_model_area['total_txn_count']);
                        }
                        else
                        {
                            $web_bookings_for_data_period_model_area['average_txn_value'] = 0;
                            $web_bookings_for_data_period_model_area['average_txn_duration'] = 0;
                            
                        }
                        

                        if(($web_bookings_for_data_period_model_area['total_weekend_hours']+$web_bookings_for_data_period_model_area['total_weekday_hours'])!=0)
                        {
                            $web_bookings_for_data_period_model_area['weekend_hours_perc'] = round($web_bookings_for_data_period_model_area['total_weekend_hours']/($web_bookings_for_data_period_model_area['total_weekend_hours']+$web_bookings_for_data_period_model_area['total_weekday_hours'])*100);

                            $web_bookings_for_data_period_model_area['weekday_hours_perc'] = round($web_bookings_for_data_period_model_area['total_weekday_hours']/($web_bookings_for_data_period_model_area['total_weekend_hours']+$web_bookings_for_data_period_model_area['total_weekday_hours'])*100);
                        }
                        
                        $web_bookings_for_data_period_model_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('user_id','>',25)->whereNotIn('payment_method',array('Paytm','PayTM','RazorPayApp','RazorPayAppiOS'))->where('model_id',$model->id)->where('area_id',$area->id)->avg('res_lead_time'));

                        if($web_bookings_for_data_period_model['total_txn_count']!=0)
                        {
                            $web_bookings_for_data_period_model_area['txn_count_perc'] = round(($web_bookings_for_data_period_model_area['total_txn_count']/$web_bookings_for_data_period_model['total_txn_count'])*100);
                        }
                        else
                        {
                            $web_bookings_for_data_period_model_area['txn_count_perc'] = 0;
                        }
                        
                        if($web_bookings_for_data_period_model['total_txn_amount']!=0)
                        {
                            $web_bookings_for_data_period_model_area['txn_amount_perc'] = round(($web_bookings_for_data_period_model_area['total_txn_amount']/$web_bookings_for_data_period_model['total_txn_amount'])*100);
                        }
                        else
                        {
                            $web_bookings_for_data_period_model_area['txn_amount_perc'] = 0;
                        }
                        
                        $web_bookings_for_data_period_model_area['area_name'] = $area->area;
                        
                        $web_bookings_for_data_period_model['areas_with_model'][$area->area] = $web_bookings_for_data_period_model_area;

                    }
                    
                }
                
                $web_bookings_for_data_period['models'][$model->bike_model] = $web_bookings_for_data_period_model;
            }



            $assisted_bookings_for_data_period_model['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->count();
            
            if($assisted_bookings_for_data_period_model['total_txn_count']>0)
            {

                $assisted_bookings_for_data_period_model['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->sum('total_price');
                
                $assisted_bookings_for_data_period_model['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->sum('weekend_hours');

                $assisted_bookings_for_data_period_model['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->sum('weekday_hours');

                if($assisted_bookings_for_data_period_model['total_txn_count']!=0)
                {
                    $assisted_bookings_for_data_period_model['average_txn_value'] = round($assisted_bookings_for_data_period_model['total_txn_amount']/$assisted_bookings_for_data_period_model['total_txn_count']);
                    $assisted_bookings_for_data_period_model['average_txn_duration'] = round(($assisted_bookings_for_data_period_model['total_weekend_hours']+$assisted_bookings_for_data_period_model['total_weekday_hours'])/$assisted_bookings_for_data_period_model['total_txn_count']);
                }
                else
                {
                    $assisted_bookings_for_data_period_model['average_txn_value'] = 0;
                    $assisted_bookings_for_data_period_model['average_txn_duration'] = 0;
                }

                if(($assisted_bookings_for_data_period_model['total_weekend_hours']+$assisted_bookings_for_data_period_model['total_weekday_hours'])!=0)
                {
                    $assisted_bookings_for_data_period_model['weekend_hours_perc'] = round($assisted_bookings_for_data_period_model['total_weekend_hours']/($assisted_bookings_for_data_period_model['total_weekend_hours']+$assisted_bookings_for_data_period_model['total_weekday_hours'])*100);

                    $assisted_bookings_for_data_period_model['weekday_hours_perc'] = round($assisted_bookings_for_data_period_model['total_weekday_hours']/($assisted_bookings_for_data_period_model['total_weekend_hours']+$assisted_bookings_for_data_period_model['total_weekday_hours'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_model['weekend_hours_perc'] = 0;
                    $assisted_bookings_for_data_period_model['weekday_hours_perc'] = 0;
                }
                
                $assisted_bookings_for_data_period_model['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->avg('res_lead_time'));

                if($assisted_bookings_for_data_period['total_txn_count']!=0)
                {
                    $assisted_bookings_for_data_period_model['txn_count_perc'] = round(($assisted_bookings_for_data_period_model['total_txn_count']/$assisted_bookings_for_data_period['total_txn_count'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_model['txn_count_perc'] = 0;
                }
                
                if($assisted_bookings_for_data_period['total_txn_amount']!=0)
                {
                    $assisted_bookings_for_data_period_model['txn_amount_perc'] = round(($assisted_bookings_for_data_period_model['total_txn_amount']/$assisted_bookings_for_data_period['total_txn_amount'])*100);
                }
                else
                {
                    $assisted_bookings_for_data_period_model['txn_amount_perc'] = 0;
                }
                
                $assisted_bookings_for_data_period_model['model_name'] = $model->bike_model;
                
                $assisted_bookings_for_data_period_model['areas_with_model'] = [];
                
                foreach($areas as $area)
                {

                    $assisted_bookings_for_data_period_model_area['total_txn_count'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->where('area_id',$area->id)->count();

                    if($assisted_bookings_for_data_period_model_area['total_txn_count']>0)
                    {

                        $assisted_bookings_for_data_period_model_area['total_txn_amount'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->where('area_id',$area->id)->sum('total_price');
                        
                        $assisted_bookings_for_data_period_model_area['total_weekend_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekend_hours');

                        $assisted_bookings_for_data_period_model_area['total_weekday_hours'] = Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->where('area_id',$area->id)->sum('weekday_hours');


                        if($assisted_bookings_for_data_period_model_area['total_txn_count']!=0)
                        {
                            $assisted_bookings_for_data_period_model_area['average_txn_value'] = round($assisted_bookings_for_data_period_model_area['total_txn_amount']/$assisted_bookings_for_data_period_model_area['total_txn_count']);
                            $assisted_bookings_for_data_period_model_area['average_txn_duration'] = round(($assisted_bookings_for_data_period_model_area['total_weekend_hours']+$assisted_bookings_for_data_period_model_area['total_weekday_hours'])/$assisted_bookings_for_data_period_model_area['total_txn_count']);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_model_area['average_txn_value'] = 0;
                            $assisted_bookings_for_data_period_model_area['average_txn_duration'] = 0;
                        }
                        
                        if(($assisted_bookings_for_data_period_model_area['total_weekend_hours']+$assisted_bookings_for_data_period_model_area['total_weekday_hours'])!=0)
                        {
                            $assisted_bookings_for_data_period_model_area['weekend_hours_perc'] = round($assisted_bookings_for_data_period_model_area['total_weekend_hours']/($assisted_bookings_for_data_period_model_area['total_weekend_hours']+$assisted_bookings_for_data_period_model_area['total_weekday_hours'])*100);

                            $assisted_bookings_for_data_period_model_area['weekday_hours_perc'] = round($assisted_bookings_for_data_period_model_area['total_weekday_hours']/($assisted_bookings_for_data_period_model_area['total_weekend_hours']+$assisted_bookings_for_data_period_model_area['total_weekday_hours'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_model_area['weekend_hours_perc'] = 0;
                            $assisted_bookings_for_data_period_model_area['weekday_hours_perc'] = 0;
                        }
                        

                        $assisted_bookings_for_data_period_model_area['average_time_to_res_start'] = round( Booking::where('created_at','>=',$data_start_date_ts)->where('created_at','<=',$data_end_date_ts)->where('assisted',1)->where('model_id',$model->id)->where('area_id',$area->id)->avg('res_lead_time'));  
                        
                        if($assisted_bookings_for_data_period_model['total_txn_count']!=0)
                        {
                            $assisted_bookings_for_data_period_model_area['txn_count_perc'] = round(($assisted_bookings_for_data_period_model_area['total_txn_count']/$assisted_bookings_for_data_period_model['total_txn_count'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_model_area['txn_count_perc'] = 0;
                        }
                        
                        if($assisted_bookings_for_data_period_model['total_txn_amount']!=0)
                        {
                            $assisted_bookings_for_data_period_model_area['txn_amount_perc'] = round(($assisted_bookings_for_data_period_model_area['total_txn_amount']/$assisted_bookings_for_data_period_model['total_txn_amount'])*100);
                        }
                        else
                        {
                            $assisted_bookings_for_data_period_model_area['txn_amount_perc'] = 0;
                        }
                        
                        $assisted_bookings_for_data_period_model_area['area_name'] = $area->area;
                        
                        $assisted_bookings_for_data_period_model['areas_with_model'][$area->area] = $assisted_bookings_for_data_period_model_area;

                    }
                    
                }
                
                $assisted_bookings_for_data_period['models'][$model->bike_model] = $assisted_bookings_for_data_period_model;
            }
        }
        
       
        $result = ['app'=>$app_bookings_for_data_period,'web'=>$web_bookings_for_data_period,'assisted'=>$assisted_bookings_for_data_period];
        
        SalesReport::create(['date'=>$request->data_start_date,'report'=>json_encode($result)]); 
        
    }
    
    public static function getReportDataFromDB($data_start_date,$data_end_date)
    {
        $begin = new \DateTime( $data_start_date );
        $end = new \DateTime($data_end_date );
        \date_add($end, \date_interval_create_from_date_string('1 day'));
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);
        
        $dateArray = [];
        
        $result = [];
        
        foreach ($period as $dt)
        {
            array_push($dateArray,$dt->format('Y-m-d'));
        }
        
        
        
        $salesReports = SalesReport::whereIn('date',$dateArray)->get();
        $areas = Area::all();
        $models = BikeModel::all();
        
        
        
        $appArray = [];
        
        //Values filled in Sales Report for loop
        $appArray['total_txn_count'] = 0;
        $appArray['total_txn_amount'] = 0;
        
        $appArray['total_txn_duration'] = 0;
        $appArray['total_weekend_hours'] = 0;
        $appArray['total_weekday_hours'] = 0;
        $appArray['total_time_to_res_start'] = 0;
        
        $appArray['areas'] = [];
        $appArray['models'] = [];
        
        //Values filled After Sales Report for loop
        $appArray['txn_count_perc'] = 0;
        $appArray['txn_amount_perc'] = 0;
        
        $appArray['average_txn_value'] = 0;

        $appArray['average_txn_duration'] = 0;
        
        $appArray['weekend_hours_perc'] = 0;
        $appArray['weekday_hours_perc'] = 0;
        
        $appArray['average_time_to_res_start'] = 0;
        
                
        
        $appArray['areas'] = [];
        
        
        foreach($salesReports as $salesReport)
        {
            $salesReportArray = json_decode($salesReport->report,true);
            
            
            $appArray['total_txn_count'] += $salesReportArray['app']['total_txn_count'];
            $appArray['total_txn_amount'] += $salesReportArray['app']['total_txn_amount'];
            
            $appArray['total_txn_duration'] += $salesReportArray['app']['average_txn_duration']*$salesReportArray['app']['total_txn_count'];
            
            $appArray['total_weekend_hours'] += round($salesReportArray['app']['average_txn_duration']*$salesReportArray['app']['total_txn_count']*$salesReportArray['app']['weekend_hours_perc']/100);

            $appArray['total_weekday_hours'] += round($salesReportArray['app']['average_txn_duration']*$salesReportArray['app']['total_txn_count']*$salesReportArray['app']['weekday_hours_perc']/100);
            
            $appArray['total_time_to_res_start'] += $salesReportArray['app']['average_time_to_res_start']*$salesReportArray['app']['total_txn_count'];
            
                        
            
            foreach($areas as $area)
            {
                
                if(!array_key_exists('areas',$salesReportArray['app']))
                    $salesReportArray['app']['areas']=[];
                if(!$salesReportArray['app']['areas'])
                    $salesReportArray['app']['areas']=[];
                if(array_key_exists($area->area,$salesReportArray['app']['areas']))
                {
                    
                    if(array_key_exists($area->area,$appArray['areas']))
                    {
                        $appArray['areas'][$area->area]['total_txn_count'] += $salesReportArray['app']['areas'][$area->area]['total_txn_count'];
                        
                        $appArray['areas'][$area->area]['total_txn_amount'] += $salesReportArray['app']['areas'][$area->area]['total_txn_amount'];

                        $appArray['areas'][$area->area]['total_txn_duration'] += $salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count'];

                        $appArray['areas'][$area->area]['total_weekend_hours'] += round($salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['weekend_hours_perc']/100);

                        $appArray['areas'][$area->area]['total_weekday_hours'] += round($salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['weekday_hours_perc']/100);

                        $appArray['areas'][$area->area]['total_time_to_res_start'] += $salesReportArray['app']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['app']['areas'][$area->area]['total_txn_count']; 
                        
                    }
                    else
                    {
                        $appArray['areas'][$area->area] = [];
                        $appArray['areas'][$area->area]['models_in_area'] = [];
                        
                        $appArray['areas'][$area->area]['total_txn_count'] = $salesReportArray['app']['areas'][$area->area]['total_txn_count'];
                        
                        $appArray['areas'][$area->area]['total_txn_amount'] = $salesReportArray['app']['areas'][$area->area]['total_txn_amount'];

                        $appArray['areas'][$area->area]['total_txn_duration'] = $salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count'];

                        $appArray['areas'][$area->area]['total_weekend_hours'] = round($salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['weekend_hours_perc']/100);

                        $appArray['areas'][$area->area]['total_weekday_hours'] = round($salesReportArray['app']['areas'][$area->area]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['weekday_hours_perc']/100);

                        $appArray['areas'][$area->area]['total_time_to_res_start'] = $salesReportArray['app']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['app']['areas'][$area->area]['total_txn_count'];
                        
                        $appArray['areas'][$area->area]['area_name'] = $area->area;
                        
                    }
                 
                
                    foreach($models as $model)
                    {
                        if(array_key_exists($model->bike_model,$salesReportArray['app']['areas'][$area->area]['models_in_area']))
                        {
                            if(array_key_exists($model->bike_model,$appArray['areas'][$area->area]['models_in_area']))
                            {
                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] += $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] += $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] += $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 


                            }
                            else
                            {
                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model] = [];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] = $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] = $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] = $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['app']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 

                                $appArray['areas'][$area->area]['models_in_area'][$model->bike_model]['model_name']=$model->bike_model;
                            }
                            
                        }
                    
                    }
                }
            }
            
            foreach($models as $model)
            {
                if(!array_key_exists('models',$salesReportArray['app']))
                    $salesReportArray['app']['models']=[];
                if(!$salesReportArray['app']['models'])
                    $salesReportArray['app']['models']=[];
                if(array_key_exists($model->bike_model,$salesReportArray['app']['models']))
                {
                    if(array_key_exists($model->bike_model,$appArray['models']))
                    {
                        $appArray['models'][$model->bike_model]['total_txn_count'] += $salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];
                        
                        $appArray['models'][$model->bike_model]['total_txn_amount'] += $salesReportArray['app']['models'][$model->bike_model]['total_txn_amount'];

                        $appArray['models'][$model->bike_model]['total_txn_duration'] += $salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];

                        $appArray['models'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $appArray['models'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $appArray['models'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['app']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']; 
                        
                        
                    }
                    else
                    {
                        $appArray['models'][$model->bike_model] = [];
                        $appArray['models'][$model->bike_model]['areas_with_model'] = [];
                            
                        $appArray['models'][$model->bike_model]['total_txn_count'] = $salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];
                        
                        $appArray['models'][$model->bike_model]['total_txn_amount'] = $salesReportArray['app']['models'][$model->bike_model]['total_txn_amount'];

                        $appArray['models'][$model->bike_model]['total_txn_duration'] = $salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];

                        $appArray['models'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $appArray['models'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['app']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $appArray['models'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['app']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];  
                        
                        $appArray['models'][$model->bike_model]['model_name'] = $model->bike_model;
                    }
                 
                
                    foreach($areas as $area)
                    {
                       
                        if(array_key_exists($area->area,$salesReportArray['app']['models'][$model->bike_model]['areas_with_model']))
                        {
                            if(array_key_exists($area->area,$appArray['models'][$model->bike_model]['areas_with_model']))
                            {
                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] += $salesReportArray['app']['models'][$model->bike_model]['total_txn_count'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] += $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] += $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] += round($salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] += round($salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] += $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                            }
                            else
                            {
                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area] = [];
                                    
                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] = $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] = $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] = $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] = round($salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] = round($salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] = $salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['app']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                                $appArray['models'][$model->bike_model]['areas_with_model'][$area->area]['area_name']=$area->area;
                            }
                        }
                    }
                }
            }            
        }
        
        
        $appArray['average_txn_value'] = round($appArray['total_txn_amount']/$appArray['total_txn_count']);

        $appArray['average_txn_duration'] = round($appArray['total_txn_duration']/$appArray['total_txn_count']);
        
        $appArray['weekend_hours_perc'] = round($appArray['total_weekend_hours']/$appArray['total_txn_duration']*100);
        $appArray['weekday_hours_perc'] = round($appArray['total_weekday_hours']/$appArray['total_txn_duration']*100);
        
        $appArray['average_time_to_res_start'] = round($appArray['total_time_to_res_start']/$appArray['total_txn_count']);
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                    $appArrayArea['average_txn_value'] = round($appArrayArea['total_txn_amount']/$appArrayArea['total_txn_count']);
                    
                    $appArrayArea['average_txn_duration'] = round($appArrayArea['total_txn_duration']/$appArrayArea['total_txn_count']);
        
                    $appArrayArea['weekend_hours_perc'] = round($appArrayArea['total_weekend_hours']/$appArrayArea['total_txn_duration']*100);
                    $appArrayArea['weekday_hours_perc'] = round($appArrayArea['total_weekday_hours']/$appArrayArea['total_txn_duration']*100);

                    $appArrayArea['average_time_to_res_start'] = round($appArrayArea['total_time_to_res_start']/$appArrayArea['total_txn_count']);
                    
                    $appArrayArea['txn_count_perc'] = round($appArrayArea['total_txn_count']/$appArray['total_txn_count']*100);
                    
                    $appArrayArea['txn_amount_perc'] = round($appArrayArea['total_txn_amount']/$appArray['total_txn_amount']*100);
                    
                    foreach($appArrayArea['models_in_area'] as $key => &$appArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                $appArrayAreaModel['average_txn_value'] = round($appArrayAreaModel['total_txn_amount']/$appArrayAreaModel['total_txn_count']);
                    
                                $appArrayAreaModel['average_txn_duration'] = round($appArrayAreaModel['total_txn_duration']/$appArrayAreaModel['total_txn_count']);

                                $appArrayAreaModel['weekend_hours_perc'] = round($appArrayAreaModel['total_weekend_hours']/$appArrayAreaModel['total_txn_duration']*100);
                                $appArrayAreaModel['weekday_hours_perc'] = round($appArrayAreaModel['total_weekday_hours']/$appArrayAreaModel['total_txn_duration']*100);

                                $appArrayAreaModel['average_time_to_res_start'] = round($appArrayAreaModel['total_time_to_res_start']/$appArrayAreaModel['total_txn_count']);

                                $appArrayAreaModel['txn_count_perc'] = round($appArrayAreaModel['total_txn_count']/$appArray['areas'][$area->area]['total_txn_count']*100);

                                $appArrayAreaModel['txn_amount_perc'] = round($appArrayAreaModel['total_txn_amount']/$appArray['areas'][$area->area]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
        
        foreach($appArray['models'] as $key => &$appArrayModel)
        {
            foreach($models as $model)
            {
                if($key == $model->bike_model)
                {
                    $appArrayModel['average_txn_value'] = round($appArrayModel['total_txn_amount']/$appArrayModel['total_txn_count']);
                    
                    $appArrayModel['average_txn_duration'] = round($appArrayModel['total_txn_duration']/$appArrayModel['total_txn_count']);
        
                    $appArrayModel['weekend_hours_perc'] = round($appArrayModel['total_weekend_hours']/$appArrayModel['total_txn_duration']*100);
                    $appArrayModel['weekday_hours_perc'] = round($appArrayModel['total_weekday_hours']/$appArrayModel['total_txn_duration']*100);

                    $appArrayModel['average_time_to_res_start'] = round($appArrayModel['total_time_to_res_start']/$appArrayModel['total_txn_count']);
                    
                    $appArrayModel['txn_count_perc'] = round($appArrayModel['total_txn_count']/$appArray['total_txn_count']*100);
                    
                    $appArrayModel['txn_amount_perc'] = round($appArrayModel['total_txn_amount']/$appArray['total_txn_amount']*100);
                    
                    foreach($appArrayModel['areas_with_model'] as $key => &$appArrayModelArea)
                    {
                        foreach($areas as $area)
                        {
                            if($key == $area->area)
                            {
                                $appArrayModelArea['average_txn_value'] = round($appArrayModelArea['total_txn_amount']/$appArrayModelArea['total_txn_count']);
                    
                                $appArrayModelArea['average_txn_duration'] = round($appArrayModelArea['total_txn_duration']/$appArrayModelArea['total_txn_count']);

                                $appArrayModelArea['weekend_hours_perc'] = round($appArrayModelArea['total_weekend_hours']/$appArrayModelArea['total_txn_duration']*100);
                                $appArrayModelArea['weekday_hours_perc'] = round($appArrayModelArea['total_weekday_hours']/$appArrayModelArea['total_txn_duration']*100);

                                $appArrayModelArea['average_time_to_res_start'] = round($appArrayModelArea['total_time_to_res_start']/$appArrayModelArea['total_txn_count']);

                                $appArrayModelArea['txn_count_perc'] = round($appArrayModelArea['total_txn_count']/$appArray['models'][$model->bike_model]['total_txn_count']*100);

                                $appArrayModelArea['txn_amount_perc'] = round($appArrayModelArea['total_txn_amount']/$appArray['models'][$model->bike_model]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
        
        
        $webArray = [];
        
        //Values filled in Sales Report for loop
        $webArray['total_txn_count'] = 0;
        $webArray['total_txn_amount'] = 0;
        
        $webArray['total_txn_duration'] = 0;
        $webArray['total_weekend_hours'] = 0;
        $webArray['total_weekday_hours'] = 0;
        $webArray['total_time_to_res_start'] = 0;
        
        $webArray['areas'] = [];
        $webArray['models'] = [];
        
        //Values filled After Sales Report for loop
        $webArray['txn_count_perc'] = 0;
        $webArray['txn_amount_perc'] = 0;
        
        $webArray['average_txn_value'] = 0;

        $webArray['average_txn_duration'] = 0;
        
        $webArray['weekend_hours_perc'] = 0;
        $webArray['weekday_hours_perc'] = 0;
        
        $webArray['average_time_to_res_start'] = 0;
        
                
        
        $webArray['areas'] = [];
        
        
        foreach($salesReports as $salesReport)
        {
            $salesReportArray = json_decode($salesReport->report,true);
            
            
            $webArray['total_txn_count'] += $salesReportArray['web']['total_txn_count'];
            $webArray['total_txn_amount'] += $salesReportArray['web']['total_txn_amount'];
            
            $webArray['total_txn_duration'] += $salesReportArray['web']['average_txn_duration']*$salesReportArray['web']['total_txn_count'];
            
            $webArray['total_weekend_hours'] += round($salesReportArray['web']['average_txn_duration']*$salesReportArray['web']['total_txn_count']*$salesReportArray['web']['weekend_hours_perc']/100);

            $webArray['total_weekday_hours'] += round($salesReportArray['web']['average_txn_duration']*$salesReportArray['web']['total_txn_count']*$salesReportArray['web']['weekday_hours_perc']/100);
            
            $webArray['total_time_to_res_start'] += $salesReportArray['web']['average_time_to_res_start']*$salesReportArray['web']['total_txn_count'];
            
                        
            
            foreach($areas as $area)
            {
                if(!array_key_exists('areas',$salesReportArray['web']))
                    $salesReportArray['web']['areas']=[];
                if(!$salesReportArray['web']['areas'])
                    $salesReportArray['web']['areas']=[];
                if(array_key_exists($area->area,$salesReportArray['web']['areas']))
                {
                    
                    if(array_key_exists($area->area,$webArray['areas']))
                    {
                        $webArray['areas'][$area->area]['total_txn_count'] += $salesReportArray['web']['areas'][$area->area]['total_txn_count'];
                        
                        $webArray['areas'][$area->area]['total_txn_amount'] += $salesReportArray['web']['areas'][$area->area]['total_txn_amount'];

                        $webArray['areas'][$area->area]['total_txn_duration'] += $salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count'];

                        $webArray['areas'][$area->area]['total_weekend_hours'] += round($salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['weekend_hours_perc']/100);

                        $webArray['areas'][$area->area]['total_weekday_hours'] += round($salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['weekday_hours_perc']/100);

                        $webArray['areas'][$area->area]['total_time_to_res_start'] += $salesReportArray['web']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['web']['areas'][$area->area]['total_txn_count']; 
                        
                    }
                    else
                    {
                        $webArray['areas'][$area->area] = [];
                        $webArray['areas'][$area->area]['models_in_area'] = [];
                        
                        $webArray['areas'][$area->area]['total_txn_count'] = $salesReportArray['web']['areas'][$area->area]['total_txn_count'];
                        
                        $webArray['areas'][$area->area]['total_txn_amount'] = $salesReportArray['web']['areas'][$area->area]['total_txn_amount'];

                        $webArray['areas'][$area->area]['total_txn_duration'] = $salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count'];

                        $webArray['areas'][$area->area]['total_weekend_hours'] = round($salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['weekend_hours_perc']/100);

                        $webArray['areas'][$area->area]['total_weekday_hours'] = round($salesReportArray['web']['areas'][$area->area]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['weekday_hours_perc']/100);

                        $webArray['areas'][$area->area]['total_time_to_res_start'] = $salesReportArray['web']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['web']['areas'][$area->area]['total_txn_count'];
                        
                        $webArray['areas'][$area->area]['area_name'] = $area->area;
                        
                    }
                 
                
                    foreach($models as $model)
                    {
                        if(array_key_exists($model->bike_model,$salesReportArray['web']['areas'][$area->area]['models_in_area']))
                        {
                            if(array_key_exists($model->bike_model,$webArray['areas'][$area->area]['models_in_area']))
                            {
                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] += $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] += $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] += $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 


                            }
                            else
                            {
                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model] = [];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] = $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] = $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] = $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['web']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 

                                $webArray['areas'][$area->area]['models_in_area'][$model->bike_model]['model_name']=$model->bike_model;
                            }
                            
                        }
                    
                    }
                }
            }
            
            foreach($models as $model)
            {
                if(!array_key_exists('models',$salesReportArray['web']))
                    $salesReportArray['web']['models']=[];
                if(!$salesReportArray['web']['models'])
                    $salesReportArray['web']['models']=[];
                if(array_key_exists($model->bike_model,$salesReportArray['web']['models']))
                {
                    if(array_key_exists($model->bike_model,$webArray['models']))
                    {
                        $webArray['models'][$model->bike_model]['total_txn_count'] += $salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];
                        
                        $webArray['models'][$model->bike_model]['total_txn_amount'] += $salesReportArray['web']['models'][$model->bike_model]['total_txn_amount'];

                        $webArray['models'][$model->bike_model]['total_txn_duration'] += $salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];

                        $webArray['models'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $webArray['models'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $webArray['models'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['web']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']; 
                        
                        
                    }
                    else
                    {
                        $webArray['models'][$model->bike_model] = [];
                        $webArray['models'][$model->bike_model]['areas_with_model'] = [];
                            
                        $webArray['models'][$model->bike_model]['total_txn_count'] = $salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];
                        
                        $webArray['models'][$model->bike_model]['total_txn_amount'] = $salesReportArray['web']['models'][$model->bike_model]['total_txn_amount'];

                        $webArray['models'][$model->bike_model]['total_txn_duration'] = $salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];

                        $webArray['models'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $webArray['models'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['web']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $webArray['models'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['web']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];  
                        
                        $webArray['models'][$model->bike_model]['model_name'] = $model->bike_model;
                    }
                 
                
                    foreach($areas as $area)
                    {
                       
                        if(array_key_exists($area->area,$salesReportArray['web']['models'][$model->bike_model]['areas_with_model']))
                        {
                            if(array_key_exists($area->area,$webArray['models'][$model->bike_model]['areas_with_model']))
                            {
                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] += $salesReportArray['web']['models'][$model->bike_model]['total_txn_count'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] += $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] += $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] += round($salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] += round($salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] += $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                            }
                            else
                            {
                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area] = [];
                                    
                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] = $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] = $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] = $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] = round($salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] = round($salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] = $salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['web']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                                $webArray['models'][$model->bike_model]['areas_with_model'][$area->area]['area_name']=$area->area;
                            }
                        }
                    }
                }
            }            
        }
        
        
        $webArray['average_txn_value'] = round($webArray['total_txn_amount']/$webArray['total_txn_count']);

        $webArray['average_txn_duration'] = round($webArray['total_txn_duration']/$webArray['total_txn_count']);
        
        $webArray['weekend_hours_perc'] = round($webArray['total_weekend_hours']/$webArray['total_txn_duration']*100);
        $webArray['weekday_hours_perc'] = round($webArray['total_weekday_hours']/$webArray['total_txn_duration']*100);
        
        $webArray['average_time_to_res_start'] = round($webArray['total_time_to_res_start']/$webArray['total_txn_count']);
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                    $webArrayArea['average_txn_value'] = round($webArrayArea['total_txn_amount']/$webArrayArea['total_txn_count']);
                    
                    $webArrayArea['average_txn_duration'] = round($webArrayArea['total_txn_duration']/$webArrayArea['total_txn_count']);
        
                    $webArrayArea['weekend_hours_perc'] = round($webArrayArea['total_weekend_hours']/$webArrayArea['total_txn_duration']*100);
                    $webArrayArea['weekday_hours_perc'] = round($webArrayArea['total_weekday_hours']/$webArrayArea['total_txn_duration']*100);

                    $webArrayArea['average_time_to_res_start'] = round($webArrayArea['total_time_to_res_start']/$webArrayArea['total_txn_count']);
                    
                    $webArrayArea['txn_count_perc'] = round($webArrayArea['total_txn_count']/$webArray['total_txn_count']*100);
                    
                    $webArrayArea['txn_amount_perc'] = round($webArrayArea['total_txn_amount']/$webArray['total_txn_amount']*100);
                    
                    foreach($webArrayArea['models_in_area'] as $key => &$webArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                $webArrayAreaModel['average_txn_value'] = round($webArrayAreaModel['total_txn_amount']/$webArrayAreaModel['total_txn_count']);
                    
                                $webArrayAreaModel['average_txn_duration'] = round($webArrayAreaModel['total_txn_duration']/$webArrayAreaModel['total_txn_count']);

                                $webArrayAreaModel['weekend_hours_perc'] = round($webArrayAreaModel['total_weekend_hours']/$webArrayAreaModel['total_txn_duration']*100);
                                $webArrayAreaModel['weekday_hours_perc'] = round($webArrayAreaModel['total_weekday_hours']/$webArrayAreaModel['total_txn_duration']*100);

                                $webArrayAreaModel['average_time_to_res_start'] = round($webArrayAreaModel['total_time_to_res_start']/$webArrayAreaModel['total_txn_count']);

                                $webArrayAreaModel['txn_count_perc'] = round($webArrayAreaModel['total_txn_count']/$webArray['areas'][$area->area]['total_txn_count']*100);

                                $webArrayAreaModel['txn_amount_perc'] = round($webArrayAreaModel['total_txn_amount']/$webArray['areas'][$area->area]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
        
        foreach($webArray['models'] as $key => &$webArrayModel)
        {
            foreach($models as $model)
            {
                if($key == $model->bike_model)
                {
                    $webArrayModel['average_txn_value'] = round($webArrayModel['total_txn_amount']/$webArrayModel['total_txn_count']);
                    
                    $webArrayModel['average_txn_duration'] = round($webArrayModel['total_txn_duration']/$webArrayModel['total_txn_count']);
        
                    $webArrayModel['weekend_hours_perc'] = round($webArrayModel['total_weekend_hours']/$webArrayModel['total_txn_duration']*100);
                    $webArrayModel['weekday_hours_perc'] = round($webArrayModel['total_weekday_hours']/$webArrayModel['total_txn_duration']*100);

                    $webArrayModel['average_time_to_res_start'] = round($webArrayModel['total_time_to_res_start']/$webArrayModel['total_txn_count']);
                    
                    $webArrayModel['txn_count_perc'] = round($webArrayModel['total_txn_count']/$webArray['total_txn_count']*100);
                    
                    $webArrayModel['txn_amount_perc'] = round($webArrayModel['total_txn_amount']/$webArray['total_txn_amount']*100);
                    
                    foreach($webArrayModel['areas_with_model'] as $key => &$webArrayModelArea)
                    {
                        foreach($areas as $area)
                        {
                            if($key == $area->area)
                            {
                                $webArrayModelArea['average_txn_value'] = round($webArrayModelArea['total_txn_amount']/$webArrayModelArea['total_txn_count']);
                    
                                $webArrayModelArea['average_txn_duration'] = round($webArrayModelArea['total_txn_duration']/$webArrayModelArea['total_txn_count']);

                                $webArrayModelArea['weekend_hours_perc'] = round($webArrayModelArea['total_weekend_hours']/$webArrayModelArea['total_txn_duration']*100);
                                $webArrayModelArea['weekday_hours_perc'] = round($webArrayModelArea['total_weekday_hours']/$webArrayModelArea['total_txn_duration']*100);

                                $webArrayModelArea['average_time_to_res_start'] = round($webArrayModelArea['total_time_to_res_start']/$webArrayModelArea['total_txn_count']);

                                $webArrayModelArea['txn_count_perc'] = round($webArrayModelArea['total_txn_count']/$webArray['models'][$model->bike_model]['total_txn_count']*100);

                                $webArrayModelArea['txn_amount_perc'] = round($webArrayModelArea['total_txn_amount']/$webArray['models'][$model->bike_model]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
        
        
        $assistedArray = [];
        
        //Values filled in Sales Report for loop
        $assistedArray['total_txn_count'] = 0;
        $assistedArray['total_txn_amount'] = 0;
        
        $assistedArray['total_txn_duration'] = 0;
        $assistedArray['total_weekend_hours'] = 0;
        $assistedArray['total_weekday_hours'] = 0;
        $assistedArray['total_time_to_res_start'] = 0;
        
        $assistedArray['areas'] = [];
        $assistedArray['models'] = [];
        
        //Values filled After Sales Report for loop
        $assistedArray['txn_count_perc'] = 0;
        $assistedArray['txn_amount_perc'] = 0;
        
        $assistedArray['average_txn_value'] = 0;

        $assistedArray['average_txn_duration'] = 0;
        
        $assistedArray['weekend_hours_perc'] = 0;
        $assistedArray['weekday_hours_perc'] = 0;
        
        $assistedArray['average_time_to_res_start'] = 0;
        
                
        
        $assistedArray['areas'] = [];
        
        
        foreach($salesReports as $salesReport)
        {
            $salesReportArray = json_decode($salesReport->report,true);
            
            
            $assistedArray['total_txn_count'] += $salesReportArray['assisted']['total_txn_count'];
            $assistedArray['total_txn_amount'] += $salesReportArray['assisted']['total_txn_amount'];
            
            $assistedArray['total_txn_duration'] += $salesReportArray['assisted']['average_txn_duration']*$salesReportArray['assisted']['total_txn_count'];
            
            $assistedArray['total_weekend_hours'] += round($salesReportArray['assisted']['average_txn_duration']*$salesReportArray['assisted']['total_txn_count']*$salesReportArray['assisted']['weekend_hours_perc']/100);

            $assistedArray['total_weekday_hours'] += round($salesReportArray['assisted']['average_txn_duration']*$salesReportArray['assisted']['total_txn_count']*$salesReportArray['assisted']['weekday_hours_perc']/100);
            
            $assistedArray['total_time_to_res_start'] += $salesReportArray['assisted']['average_time_to_res_start']*$salesReportArray['assisted']['total_txn_count'];
            
                        
            
            foreach($areas as $area)
            {
                if(!array_key_exists('areas',$salesReportArray['assisted']))
                    $salesReportArray['assisted']['areas']=[];
                if(!$salesReportArray['assisted']['areas'])
                    $salesReportArray['assisted']['areas']=[];
                
                if(array_key_exists($area->area,$salesReportArray['assisted']['areas']))
                {
                    
                    if(array_key_exists($area->area,$assistedArray['areas']))
                    {
                        $assistedArray['areas'][$area->area]['total_txn_count'] += $salesReportArray['assisted']['areas'][$area->area]['total_txn_count'];
                        
                        $assistedArray['areas'][$area->area]['total_txn_amount'] += $salesReportArray['assisted']['areas'][$area->area]['total_txn_amount'];

                        $assistedArray['areas'][$area->area]['total_txn_duration'] += $salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count'];

                        $assistedArray['areas'][$area->area]['total_weekend_hours'] += round($salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['weekend_hours_perc']/100);

                        $assistedArray['areas'][$area->area]['total_weekday_hours'] += round($salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['weekday_hours_perc']/100);

                        $assistedArray['areas'][$area->area]['total_time_to_res_start'] += $salesReportArray['assisted']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count']; 
                        
                    }
                    else
                    {
                        $assistedArray['areas'][$area->area] = [];
                        $assistedArray['areas'][$area->area]['models_in_area'] = [];
                        
                        $assistedArray['areas'][$area->area]['total_txn_count'] = $salesReportArray['assisted']['areas'][$area->area]['total_txn_count'];
                        
                        $assistedArray['areas'][$area->area]['total_txn_amount'] = $salesReportArray['assisted']['areas'][$area->area]['total_txn_amount'];

                        $assistedArray['areas'][$area->area]['total_txn_duration'] = $salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count'];

                        $assistedArray['areas'][$area->area]['total_weekend_hours'] = round($salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['weekend_hours_perc']/100);

                        $assistedArray['areas'][$area->area]['total_weekday_hours'] = round($salesReportArray['assisted']['areas'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['weekday_hours_perc']/100);

                        $assistedArray['areas'][$area->area]['total_time_to_res_start'] = $salesReportArray['assisted']['areas'][$area->area]['average_time_to_res_start']*$salesReportArray['assisted']['areas'][$area->area]['total_txn_count'];
                        
                        $assistedArray['areas'][$area->area]['area_name'] = $area->area;
                        
                    }
                 
                
                    foreach($models as $model)
                    {
                        if(array_key_exists($model->bike_model,$salesReportArray['assisted']['areas'][$area->area]['models_in_area']))
                        {
                            if(array_key_exists($model->bike_model,$assistedArray['areas'][$area->area]['models_in_area']))
                            {
                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] += $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] += $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] += $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 


                            }
                            else
                            {
                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model] = [];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'] = $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'] = $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_amount'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_duration'] = $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count'];

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekend_hours_perc']/100);

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['weekday_hours_perc']/100);

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['assisted']['areas'][$area->area]['models_in_area'][$model->bike_model]['total_txn_count']; 

                                $assistedArray['areas'][$area->area]['models_in_area'][$model->bike_model]['model_name']=$model->bike_model;
                            }
                            
                        }
                    
                    }
                }
            }
            
            foreach($models as $model)
            {
                if(!array_key_exists('models',$salesReportArray['assisted']))
                    $salesReportArray['assisted']['models']=[];
                if(!$salesReportArray['assisted']['models'])
                    $salesReportArray['assisted']['models']=[];
                
                if(array_key_exists($model->bike_model,$salesReportArray['assisted']['models']))
                {
                    if(array_key_exists($model->bike_model,$assistedArray['models']))
                    {
                        $assistedArray['models'][$model->bike_model]['total_txn_count'] += $salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];
                        
                        $assistedArray['models'][$model->bike_model]['total_txn_amount'] += $salesReportArray['assisted']['models'][$model->bike_model]['total_txn_amount'];

                        $assistedArray['models'][$model->bike_model]['total_txn_duration'] += $salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];

                        $assistedArray['models'][$model->bike_model]['total_weekend_hours'] += round($salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $assistedArray['models'][$model->bike_model]['total_weekday_hours'] += round($salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $assistedArray['models'][$model->bike_model]['total_time_to_res_start'] += $salesReportArray['assisted']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']; 
                        
                        
                    }
                    else
                    {
                        $assistedArray['models'][$model->bike_model] = [];
                        $assistedArray['models'][$model->bike_model]['areas_with_model'] = [];
                            
                        $assistedArray['models'][$model->bike_model]['total_txn_count'] = $salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];
                        
                        $assistedArray['models'][$model->bike_model]['total_txn_amount'] = $salesReportArray['assisted']['models'][$model->bike_model]['total_txn_amount'];

                        $assistedArray['models'][$model->bike_model]['total_txn_duration'] = $salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];

                        $assistedArray['models'][$model->bike_model]['total_weekend_hours'] = round($salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['weekend_hours_perc']/100);

                        $assistedArray['models'][$model->bike_model]['total_weekday_hours'] = round($salesReportArray['assisted']['models'][$model->bike_model]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['weekday_hours_perc']/100);

                        $assistedArray['models'][$model->bike_model]['total_time_to_res_start'] = $salesReportArray['assisted']['models'][$model->bike_model]['average_time_to_res_start']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];  
                        
                        $assistedArray['models'][$model->bike_model]['model_name'] = $model->bike_model;
                    }
                 
                
                    foreach($areas as $area)
                    {
                       
                        if(array_key_exists($area->area,$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model']))
                        {
                            if(array_key_exists($area->area,$assistedArray['models'][$model->bike_model]['areas_with_model']))
                            {
                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] += $salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] += $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] += $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] += round($salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] += round($salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] += $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                            }
                            else
                            {
                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area] = [];
                                    
                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'] = $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'] = $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_amount'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_duration'] = $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count'];

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekend_hours'] = round($salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekend_hours_perc']/100);

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_weekday_hours'] = round($salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_txn_duration']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['weekday_hours_perc']/100);

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['total_time_to_res_start'] = $salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['average_time_to_res_start']*$salesReportArray['assisted']['models'][$model->bike_model]['areas_with_model'][$area->area]['total_txn_count']; 

                                $assistedArray['models'][$model->bike_model]['areas_with_model'][$area->area]['area_name']=$area->area;
                            }
                        }
                    }
                }
            }            
        }
        
        
        $assistedArray['average_txn_value'] = round($assistedArray['total_txn_amount']/$assistedArray['total_txn_count']);

        $assistedArray['average_txn_duration'] = round($assistedArray['total_txn_duration']/$assistedArray['total_txn_count']);
        
        $assistedArray['weekend_hours_perc'] = round($assistedArray['total_weekend_hours']/$assistedArray['total_txn_duration']*100);
        $assistedArray['weekday_hours_perc'] = round($assistedArray['total_weekday_hours']/$assistedArray['total_txn_duration']*100);
        
        $assistedArray['average_time_to_res_start'] = round($assistedArray['total_time_to_res_start']/$assistedArray['total_txn_count']);
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                    $assistedArrayArea['average_txn_value'] = round($assistedArrayArea['total_txn_amount']/$assistedArrayArea['total_txn_count']);
                    
                    $assistedArrayArea['average_txn_duration'] = round($assistedArrayArea['total_txn_duration']/$assistedArrayArea['total_txn_count']);
        
                    $assistedArrayArea['weekend_hours_perc'] = round($assistedArrayArea['total_weekend_hours']/$assistedArrayArea['total_txn_duration']*100);
                    $assistedArrayArea['weekday_hours_perc'] = round($assistedArrayArea['total_weekday_hours']/$assistedArrayArea['total_txn_duration']*100);

                    $assistedArrayArea['average_time_to_res_start'] = round($assistedArrayArea['total_time_to_res_start']/$assistedArrayArea['total_txn_count']);
                    
                    $assistedArrayArea['txn_count_perc'] = round($assistedArrayArea['total_txn_count']/$assistedArray['total_txn_count']*100);
                    
                    $assistedArrayArea['txn_amount_perc'] = round($assistedArrayArea['total_txn_amount']/$assistedArray['total_txn_amount']*100);
                    
                    foreach($assistedArrayArea['models_in_area'] as $key => &$assistedArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                $assistedArrayAreaModel['average_txn_value'] = round($assistedArrayAreaModel['total_txn_amount']/$assistedArrayAreaModel['total_txn_count']);
                    
                                $assistedArrayAreaModel['average_txn_duration'] = round($assistedArrayAreaModel['total_txn_duration']/$assistedArrayAreaModel['total_txn_count']);

                                $assistedArrayAreaModel['weekend_hours_perc'] = round($assistedArrayAreaModel['total_weekend_hours']/$assistedArrayAreaModel['total_txn_duration']*100);
                                $assistedArrayAreaModel['weekday_hours_perc'] = round($assistedArrayAreaModel['total_weekday_hours']/$assistedArrayAreaModel['total_txn_duration']*100);

                                $assistedArrayAreaModel['average_time_to_res_start'] = round($assistedArrayAreaModel['total_time_to_res_start']/$assistedArrayAreaModel['total_txn_count']);

                                $assistedArrayAreaModel['txn_count_perc'] = round($assistedArrayAreaModel['total_txn_count']/$assistedArray['areas'][$area->area]['total_txn_count']*100);

                                if($assistedArray['areas'][$area->area]['total_txn_amount'] == 0)
                                    $assistedArrayAreaModel['txn_amount_perc'] = 0;
                                else
                                    $assistedArrayAreaModel['txn_amount_perc'] = round($assistedArrayAreaModel['total_txn_amount']/$assistedArray['areas'][$area->area]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
        
        foreach($assistedArray['models'] as $key => &$assistedArrayModel)
        {
            foreach($models as $model)
            {
                if($key == $model->bike_model)
                {
                    $assistedArrayModel['average_txn_value'] = round($assistedArrayModel['total_txn_amount']/$assistedArrayModel['total_txn_count']);
                    
                    $assistedArrayModel['average_txn_duration'] = round($assistedArrayModel['total_txn_duration']/$assistedArrayModel['total_txn_count']);
        
                    $assistedArrayModel['weekend_hours_perc'] = round($assistedArrayModel['total_weekend_hours']/$assistedArrayModel['total_txn_duration']*100);
                    $assistedArrayModel['weekday_hours_perc'] = round($assistedArrayModel['total_weekday_hours']/$assistedArrayModel['total_txn_duration']*100);

                    $assistedArrayModel['average_time_to_res_start'] = round($assistedArrayModel['total_time_to_res_start']/$assistedArrayModel['total_txn_count']);
                    
                    $assistedArrayModel['txn_count_perc'] = round($assistedArrayModel['total_txn_count']/$assistedArray['total_txn_count']*100);
                    
                    $assistedArrayModel['txn_amount_perc'] = round($assistedArrayModel['total_txn_amount']/$assistedArray['total_txn_amount']*100);
                    
                    foreach($assistedArrayModel['areas_with_model'] as $key => &$assistedArrayModelArea)
                    {
                        foreach($areas as $area)
                        {
                            if($key == $area->area)
                            {
                                $assistedArrayModelArea['average_txn_value'] = round($assistedArrayModelArea['total_txn_amount']/$assistedArrayModelArea['total_txn_count']);
                    
                                $assistedArrayModelArea['average_txn_duration'] = round($assistedArrayModelArea['total_txn_duration']/$assistedArrayModelArea['total_txn_count']);

                                $assistedArrayModelArea['weekend_hours_perc'] = round($assistedArrayModelArea['total_weekend_hours']/$assistedArrayModelArea['total_txn_duration']*100);
                                $assistedArrayModelArea['weekday_hours_perc'] = round($assistedArrayModelArea['total_weekday_hours']/$assistedArrayModelArea['total_txn_duration']*100);

                                $assistedArrayModelArea['average_time_to_res_start'] = round($assistedArrayModelArea['total_time_to_res_start']/$assistedArrayModelArea['total_txn_count']);

                                $assistedArrayModelArea['txn_count_perc'] = round($assistedArrayModelArea['total_txn_count']/$assistedArray['models'][$model->bike_model]['total_txn_count']*100);

                                if($assistedArray['models'][$model->bike_model]['total_txn_amount'] == 0)
                                    $assistedArrayAreaModel['txn_amount_perc'] = 0;
                                else
                                    $assistedArrayModelArea['txn_amount_perc'] = round($assistedArrayModelArea['total_txn_amount']/$assistedArray['models'][$model->bike_model]['total_txn_amount']*100);
                            }
                        }
                    }
                    
                }
            }
            
        }
  

    
        $totalCount = $appArray['total_txn_count']+$webArray['total_txn_count']+$assistedArray['total_txn_count'];
        $totalAmount = $appArray['total_txn_amount']+$webArray['total_txn_amount']+$assistedArray['total_txn_amount'];
        
        
        
        
        $appArray['txn_count_perc'] = round($appArray['total_txn_count']/$totalCount*100);
        $appArray['txn_amount_perc'] = round($appArray['total_txn_amount']/$totalAmount*100);
        
        $webArray['txn_count_perc'] = round($webArray['total_txn_count']/$totalCount*100);
        $webArray['txn_amount_perc'] = round($webArray['total_txn_amount']/$totalAmount*100);
        
        $assistedArray['txn_count_perc'] = round($assistedArray['total_txn_count']/$totalCount*100);
        $assistedArray['txn_amount_perc'] = round($assistedArray['total_txn_amount']/$totalAmount*100);
        
        $result = ['result'=>['app'=>$appArray,'web'=>$webArray,'assisted'=>$assistedArray]];
        
        return $result;
    }
    
    public function getReportChart(Request $request)
    {
        
        $result = Self::getReportDataFromDB($request->data_start_date,$request->data_end_date);
        
        $appArray = $result['result']['app'];
        $webArray = $result['result']['web'];
        $assistedArray = $result['result']['assisted'];
        
        $areas = Area::all();
        $models = BikeModel::all();
        
        
        
       
        $mainSeriesCount = array(['name'=>'Categories','colorByPoint' => true,'data'=>[["name"=>'App',
                            "y"=>$appArray['txn_count_perc'],
                            "drilldown"=>'App Areas'],
                            ["name"=>'Web',
                            "y"=>$webArray['txn_count_perc'],
                            "drilldown"=>'Web Areas'],
                            ["name"=>'Assisted',
                            "y"=>$assistedArray['txn_count_perc'],
                            "drilldown"=>'Assisted Areas']]]);
        
        
        
        $appDrillDownCountData = [];
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($appDrillDownCountData,["name"=>$area->area,'y'=>$appArrayArea['txn_count_perc'],'drilldown'=>'app '.$area->area.' Models']); 
                }
            }
        }
        
        $webDrillDownCountData = [];
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($webDrillDownCountData,["name"=>$area->area,'y'=>$webArrayArea['txn_count_perc'],'drilldown'=>'web '.$area->area.' Models']); 
                }
            }
        }
        
        $assistedDrillDownCountData = [];
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($assistedDrillDownCountData,["name"=>$area->area,'y'=>$assistedArrayArea['txn_count_perc'],'drilldown'=>'assisted '.$area->area.' Models']); 
                }
            }
        }
        
        
        
                    
                    
        $drillDownSeriesCount = [["name" => 'App Areas',
                            "id" => 'App Areas',
                            "data"=>$appDrillDownCountData],
                            ["name" => 'Web Areas',
                            "id" => 'Web Areas',
                            "data"=>$webDrillDownCountData],
                            ["name" => 'Assisted Areas',
                            "id" => 'Assisted Areas',
                            "data"=>$assistedDrillDownCountData]];
        
        
        $appAreaDrillDownCount = [];
        
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                $appAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($appArrayArea['models_in_area'] as $key => &$appArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($appAreaDrillDownCountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $appArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"app ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesCount,["name"=>"app ".$area->area." Models",'id'=>"app ".$area->area." Models",'data'=>$appAreaDrillDownCountData]); 
                }
            }
        }
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                $appAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($appArrayArea['models_in_area'] as $key => &$appArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesCount, ["name"=>$model->bike_model, 
                    	                                           "y" => $appArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"app ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesCount,["name"=>"app ".$area->area." ".$model->bike_model,'id'=>"app ".$area->area." ".$model->bike_model,'data'=>[['weekday',$appArrayAreaModel['weekday_hours_perc']],['weekend',$appArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        $webAreaDrillDownCount = [];
        
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                $webAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($webArrayArea['models_in_area'] as $key => &$webArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($webAreaDrillDownCountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $webArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"web ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesCount,["name"=>"web ".$area->area." Models",'id'=>"web ".$area->area." Models",'data'=>$webAreaDrillDownCountData]); 
                }
            }
        }
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                $webAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($webArrayArea['models_in_area'] as $key => &$webArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesCount, ["name"=>$model->bike_model, 
                    	                                           "y" => $webArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"web ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesCount,["name"=>"web ".$area->area." ".$model->bike_model,'id'=>"web ".$area->area." ".$model->bike_model,'data'=>[['weekday',$webArrayAreaModel['weekday_hours_perc']],['weekend',$webArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        
        $assistedAreaDrillDownCount = [];
        
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                $assistedAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($assistedArrayArea['models_in_area'] as $key => &$assistedArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($assistedAreaDrillDownCountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $assistedArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"assisted ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesCount,["name"=>"assisted ".$area->area." Models",'id'=>"assisted ".$area->area." Models",'data'=>$assistedAreaDrillDownCountData]); 
                }
            }
        }
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                $assistedAreaDrillDownCountData = [];
                
                if($key == $area->area)
                {
                    foreach($assistedArrayArea['models_in_area'] as $key => &$assistedArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesCount, ["name"=>$model->bike_model, 
                    	                                           "y" => $assistedArrayAreaModel['txn_count_perc'], 
                    	                                           "drilldown"=>"assisted ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesCount,["name"=>"assisted ".$area->area." ".$model->bike_model,'id'=>"assisted ".$area->area." ".$model->bike_model,'data'=>[['weekday',$assistedArrayAreaModel['weekday_hours_perc']],['weekend',$assistedArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        
        
        $mainSeriesCount = json_encode($mainSeriesCount,true);
        $drillDownSeriesCount = json_encode($drillDownSeriesCount,true);
        
       
        
        
        $mainSeriesAmount = array(['name'=>'Categories','colorByPoint' => true,'data'=>[["name"=>'App',
                            "y"=>$appArray['txn_amount_perc'],
                            "drilldown"=>'App Areas'],
                            ["name"=>'Web',
                            "y"=>$webArray['txn_amount_perc'],
                            "drilldown"=>'Web Areas'],
                            ["name"=>'Assisted',
                            "y"=>$assistedArray['txn_amount_perc'],
                            "drilldown"=>'Assisted Areas']]]);
        
        
        
        $appDrillDownAmountData = [];
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($appDrillDownAmountData,["name"=>$area->area,'y'=>$appArrayArea['txn_amount_perc'],'drilldown'=>'app '.$area->area.' Models']); 
                }
            }
        }
        
        $webDrillDownAmountData = [];
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($webDrillDownAmountData,["name"=>$area->area,'y'=>$webArrayArea['txn_amount_perc'],'drilldown'=>'web '.$area->area.' Models']); 
                }
            }
        }
        
        $assistedDrillDownAmountData = [];
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                if($key == $area->area)
                {
                   array_push($assistedDrillDownAmountData,["name"=>$area->area,'y'=>$assistedArrayArea['txn_amount_perc'],'drilldown'=>'assisted '.$area->area.' Models']); 
                }
            }
        }
        
        
        
                    
                    
        $drillDownSeriesAmount = [["name" => 'App Areas',
                            "id" => 'App Areas',
                            "data"=>$appDrillDownAmountData],
                            ["name" => 'Web Areas',
                            "id" => 'Web Areas',
                            "data"=>$webDrillDownAmountData],
                            ["name" => 'Assisted Areas',
                            "id" => 'Assisted Areas',
                            "data"=>$assistedDrillDownAmountData]];
        
        
        $appAreaDrillDownAmount = [];
        
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                $appAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($appArrayArea['models_in_area'] as $key => &$appArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($appAreaDrillDownAmountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $appArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"app ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesAmount,["name"=>"app ".$area->area." Models",'id'=>"app ".$area->area." Models",'data'=>$appAreaDrillDownAmountData]); 
                }
            }
        }
        
        foreach($appArray['areas'] as $key => &$appArrayArea)
        {
            foreach($areas as $area)
            {
                $appAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($appArrayArea['models_in_area'] as $key => &$appArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesAmount, ["name"=>$model->bike_model, 
                    	                                           "y" => $appArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"app ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesAmount,["name"=>"app ".$area->area." ".$model->bike_model,'id'=>"app ".$area->area." ".$model->bike_model,'data'=>[['weekday',$appArrayAreaModel['weekday_hours_perc']],['weekend',$appArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        $webAreaDrillDownAmount = [];
        
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                $webAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($webArrayArea['models_in_area'] as $key => &$webArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($webAreaDrillDownAmountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $webArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"web ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesAmount,["name"=>"web ".$area->area." Models",'id'=>"web ".$area->area." Models",'data'=>$webAreaDrillDownAmountData]); 
                }
            }
        }
        
        foreach($webArray['areas'] as $key => &$webArrayArea)
        {
            foreach($areas as $area)
            {
                $webAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($webArrayArea['models_in_area'] as $key => &$webArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesAmount, ["name"=>$model->bike_model, 
                    	                                           "y" => $webArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"web ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesAmount,["name"=>"web ".$area->area." ".$model->bike_model,'id'=>"web ".$area->area." ".$model->bike_model,'data'=>[['weekday',$webArrayAreaModel['weekday_hours_perc']],['weekend',$webArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        
        $assistedAreaDrillDownAmount = [];
        
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                $assistedAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($assistedArrayArea['models_in_area'] as $key => &$assistedArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($assistedAreaDrillDownAmountData, ["name"=>$model->bike_model, 
                    	                                           "y" => $assistedArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"assisted ".$area->area." ".$model->bike_model]); 
                            }
                        }
                    }
                    
                    array_push($drillDownSeriesAmount,["name"=>"assisted ".$area->area." Models",'id'=>"assisted ".$area->area." Models",'data'=>$assistedAreaDrillDownAmountData]); 
                }
            }
        }
        
        foreach($assistedArray['areas'] as $key => &$assistedArrayArea)
        {
            foreach($areas as $area)
            {
                $assistedAreaDrillDownAmountData = [];
                
                if($key == $area->area)
                {
                    foreach($assistedArrayArea['models_in_area'] as $key => &$assistedArrayAreaModel)
                    {
                        foreach($models as $model)
                        {
                            if($key == $model->bike_model)
                            {
                                array_push($drillDownSeriesAmount, ["name"=>$model->bike_model, 
                    	                                           "y" => $assistedArrayAreaModel['txn_amount_perc'], 
                    	                                           "drilldown"=>"assisted ".$area->area." ".$model->bike_model]); 
                                
                                array_push($drillDownSeriesAmount,["name"=>"assisted ".$area->area." ".$model->bike_model,'id'=>"assisted ".$area->area." ".$model->bike_model,'data'=>[['weekday',$assistedArrayAreaModel['weekday_hours_perc']],['weekend',$assistedArrayAreaModel['weekend_hours_perc']]]]); 
                            }
                        }
                    }
                }
            }
        }
        
        
        
        $mainSeriesAmount = json_encode($mainSeriesAmount,true);
        $drillDownSeriesAmount = json_encode($drillDownSeriesAmount,true);
        
        
        
        
        $result = ['mainSeriesCount'=>$mainSeriesCount,'drillDownSeriesCount'=>$drillDownSeriesCount,'mainSeriesAmount'=>$mainSeriesAmount,'drillDownSeriesAmount'=>$drillDownSeriesAmount];
        
        
        
        return $result;
        
        
    }
    
    
    public function pushNotification(Request $request)
    {
        $rules = array(
        'gcm_token' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        else
        {

            if($request->signed_in_user == "true")
            {
                $user = UserController::getUserByToken();
                if(!$user)
                {
                    $data = [
                        'message' => 'User not found',
                    ];
                    return $this->respondWithSuccess($data);
                }
                else
                {
                    User::where('id',$user->id)->update(['gcm_token'=>$request->gcm_token]);
                    $data = [
                        'message' => 'Token Stored',
                    ];
                    return $this->respondWithSuccess($data);   
                }
                
                
            }
            else
            {
                DB::table('gcm_token')->insert(['gcm_token'=>$request->gcm_token]);

                $data = [
                    'message' => 'Token Stored',
                ];
                return $this->respondWithSuccess($data);
            }


        }
    }
    
    public function receiveSMS(Request $request)
    {
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        array_push($requestArray,$request->url());

        DB::table('log_requests')->insert(['request'=>json_encode($requestArray)]);
                
        $number = substr($request->text,0,4);
        
        $bike_number = BikeNumbers::where('number','LIKE','%'.$number.'%')->first();
        
        if(!$bike_number)
        {
            return "No Bike Number";
        }
        else
        {
            $todayCarbon = Carbon::now();
            
            $model = BikeModel::where('id',$bike_number->model)->first();
            
            
            $areas = Area::where('status',1)->get();
            
            $message = $model->bike_model." %0a";
            
            $dispatcher = app('Dingo\Api\Dispatcher');
            
            
            foreach($areas as $area)
            {
                $capacity = Capacity::where('date',$todayCarbon->format('Y-m-d'))
                    ->where('area_id',$area->id)
                    ->where('model_id',$bike_number->model)
                    ->first();
                
                if(!$capacity)
                {
                    continue;
                }
                elseif($capacity->hour_string == "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
                {
                    continue;
                }
                else
                {
                    
                    
                    $toCalendar = $dispatcher->with([
                        'start_date' => $todayCarbon->format('Y-m-d'),
                        'start_time' => $todayCarbon->format('H:i'),
                        'model_id'=>$bike_number->model, 
                        'area_id'=>$area->id
                    ])->get('to-availability-calendar');
                    
                    $toDateCarbon = Carbon::parse($toCalendar['end_date']." ".$toCalendar['last_slot']['start_time']);
                    
                    if($toDateCarbon->year!=$todayCarbon->year)
                    {
                        $toDateStr = $toDateCarbon->format('jS M-y ga');
                    }
                    else
                    {
                        $toDateStr = $toDateCarbon->format('jS M ga');
                    }
                    
                    if($toDateCarbon->lte($todayCarbon->copy()->addHour()))
                    {
                        $message .=$area->area." - Not Available, %0a";
                    }
                    else
                    {
                        $message .=$area->area." - available till ".$toDateStr.", %0a";
                    }
                    
                }
            }
        }
        
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, "https://control.msg91.com/api/sendhttp.php?authkey=104833AuprICK12Eb56c1991f&mobiles=".$request->from."&message=".urlencode($message)."&sender=WCKDRD&route=4");
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        curl_exec($ch);



        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return ["error" =>"No Bike Number"];
    }
    
    public function receiveSMSAPI(Request $request)
    {
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        array_push($requestArray,$request->url());

        DB::table('log_requests')->insert(['request'=>json_encode($requestArray)]);
                
        $number = substr($request->text,0,4);
        
        $bike_number = BikeNumbers::where('number','LIKE','%'.$number.'%')->first();
        
        if(!$bike_number)
        {
            return ["error" =>"No Bike Number"];
        }
        else
        {
            
            
            $model = BikeModel::where('id',$bike_number->model)->first();
            
            $areas = Area::where('status',1)->get();
            
            $message = $model->bike_model." %0a";
                        
            foreach($areas as $area)
            {
                $todayCarbon = Carbon::now();
            
                $dispatcher = app('Dingo\Api\Dispatcher');
            
                $working_slots = $dispatcher->with(['date' => $todayCarbon->format('Y-m-d'),'city_id'=>$area->city_id])->get('from-working-hours-web');
                
                if(!$working_slots)
                {
                    $todayCarbon->addDay();
                }
                else
                {
                    if($todayCarbon->hour < $working_slots[0]['id'])
                    {
                        $todayCarbon->hour = $working_slots[0]['id'];
                    }
                }
                
                $capacity = Capacity::where('date',$todayCarbon->format('Y-m-d'))
                    ->where('area_id',$area->id)
                    ->where('model_id',$bike_number->model)
                    ->first();
                
                if(!$capacity)
                {
                    continue;
                }
                elseif($capacity->hour_string == "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
                {
                    continue;
                }
                else
                {
                    
                    
                    $toCalendar = $dispatcher->with([
                        'start_date' => $todayCarbon->format('Y-m-d'),
                        'start_time' => $todayCarbon->format('H:i'),
                        'model_id'=>$bike_number->model, 
                        'area_id'=>$area->id
                    ])->get('to-availability-calendar');
                    
                    $toDateCarbon = Carbon::parse($toCalendar['end_date']." ".$toCalendar['last_slot']['start_time']);
                    
                    if($toDateCarbon->year!=$todayCarbon->year)
                    {
                        $toDateStr = $toDateCarbon->format('jS M-y ga');
                    }
                    else
                    {
                        $toDateStr = $toDateCarbon->format('jS M ga');
                    }
                    
                    if($toDateCarbon->lte($todayCarbon->copy()->addHour()))
                    {
                        $message .=$area->area." - Not Available, %0a";
                    }
                    else
                    {
                        $message .=$area->area." - available till ".$toDateStr.", %0a";
                    }
                    
                }
            }
        }
        
        return ["message"=>$message, "from"=>$request->from];
    }
    
    public function getAreasWithModels(Request $request)
    {
        $rules = array(
          'start_datetime' => 'required',
          'end_datetime' => 'required',
          'model_id' => 'required'
           
        );
        
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return "<p style=\"color:red\">Select Start Date, End Date, and Model<p>";
        }
        
        
        $startDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->start_datetime);
           
        $endDateTimeCarbon = Carbon::createFromFormat('Y-m-d H:i',$request->end_datetime);
            
        
        if($startDateTimeCarbon->gte($endDateTimeCarbon)){
          return "<p style=\"color:red\">Starting Date should be before Ending Date<p>";
        }
        
        $areas = Area::where('status',1)->get();
        $dispatcher = app('Dingo\Api\Dispatcher');
        
        $presentAreas = [];
        
        $result = "<select name=\"area\" class=\"form-control\" id=\"areaId\">
        <option selected disabled> Select the Area</option>";
        
        foreach($areas as $area)
        {
        
            $capacity = $dispatcher->with(['start_datetime' => $request->start_datetime,'end_datetime' => $request->end_datetime,'model_id'=>$request->model_id,'area_id'=>$area->id])->get('check-capacity');
            
            if($capacity['capacity']>0)
            {
                $result.="<option value=\"".$area->id."\">".$area->area."</option>";
            }
            
        }
        
        $result.="</select>";
        
        return $result;
        
    }
    
    
    public function indexLog(Request $request)
    {
        $appRequests = DB::table('log_requests')->orderBy('created_at','desc')->get();
        
        return view('admin.appRequestLog.index',compact('appRequests'));
        
        
    }
    
    public function healthcheck() {
        try {
            $bookings = Booking::take(2)->get();
        } catch(\Exception $e) {
            return response('Db Failed', 500);
        }
        return response('OK', 200);
    }

  
    //END OF CLASS
}

