<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Models\City;
use App\Models\Area;

use App\Models\Price;
use App\Models\Wallet;

use App\Models\User;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\Booking;
use App\Models\ReservationInfo;
use App\Models\Parameters;
use App\Models\OneWayRentalPrice;
use App\Models\OneWayRentalBooking;
use Auth;
use Crypt;
use Response;
use Session;
use Mail;
use Hash;
use DB;
use Config;
use Input;
use Carbon\Carbon;

class WalletController extends AppController
{
    
    public function checkWalletBalance(Request $request)
    {
        if(env('SERVER_STATUS') == "test")
        {
            $user = User::where('email','varunagni@gmail.com')->first();
        }
        else
        {
            $user = UserController::getUserByToken();
        }
        
        $walletTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        
        
        if(!$walletTransaction)
        {
            $balance = 0;
        }
        else
        {
            $balance = $walletTransaction->promotional_balance + $walletTransaction->non_promotional_balance;
        }
        
        $referStr = DB::table('parameter')->where('parameter_name','refer_string')->first();
        $shareStr = DB::table('parameter')->where('parameter_name','share_string')->first();
        $iosUrl = Parameters::getParameter('iosAppLink');
        $androidLink = Parameters::getParameter('androidAppLink');
        $data = [
                    'balance' => $balance,
                    'refer_string'=>$referStr->parameter_value,
                    'share_str'=>$shareStr->parameter_value,
                    'iosAppLink'=>$iosUrl,
                    'androidAppLink'=>$androidLink
                ];
        return $this->respondWithSuccess($data); 
    }
    
    public function checkWalletBalanceErp(Request $request)
    { 
        $data = Self::checkWalletBalanceErpFunction($request->user_id);
        
        return $this->respondWithSuccess($data); 
    }
    
    public static function checkWalletBalanceErpFunction($user_id)
    {
        
        $user = User::where('id',$user_id)->first();
        
        
        $walletTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        
        if(!$walletTransaction)
        {
            $balance = 0;
        }
        else
        {
            $balance = $walletTransaction->non_promotional_balance;
        }
        
        $referStr = DB::table('parameter')->where('parameter_name','refer_string')->first();
        $shareStr = DB::table('parameter')->where('parameter_name','share_string')->first();
        $iosUrl = Parameters::getParameter('iosAppLink');
        $androidLink = Parameters::getParameter('androidAppLink');
        $data = [
                    'balance' => $balance,
                ];
        return $data; 
    }
    
    public function applyWalletAmountBkp(Request $request)
    {
        if(env('SERVER_STATUS') == "test")
        {
            $user = User::where('email','varunagni@gmail.com')->first();
        }
        else
        {
            $user = UserController::getUserByToken();
        }
        
        
        if(isset($request->model_id))
        {
            $rules = array(
                'start_date' => 'required',
                'end_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'area_id' => 'required',
                'model_id'=>'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return $this->respondWithValidationError($validator->errors());        
            }
            
            $dispatcher = app('Dingo\Api\Dispatcher');
            $priceObj = $dispatcher->with([
                'model_id' => $request->model_id,
                'area_id' => $request->area_id,
                'start_date' => $request->start_date, 
                'end_date' => $request->end_date,
                'start_time' => $request->start_time, 
                'end_time' => $request->end_time,
                'coupon'=>$request->coupon
            ])->get('bookings/total-price');
            
            $price = $priceObj->final_price;
                
        }
        else
        {
            $price = $request->price;
        }
        
        $walletTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        if(!$walletTransaction)
        {
            $balance = 0;
            $promotionalBalance = 0;
            $nonPromotionalBalance = 0;
        }
        else
        {
            $balance = $walletTransaction->balance;
            $promotionalBalance = $walletTransaction->promotional_balance;
            $nonPromotionalBalance = $walletTransaction->non_promotional_balance;
        }
        
        
        $maxWalletClaim = DB::table('parameter')->where('parameter_name','MAXWALLETCLAIM')->first()->parameter_value;
        
        if(($maxWalletClaim!=0 && !(isset($request->category))) || (($maxWalletClaim!=0) && ($request->category == "AtoA")))
        {

            $promotionalBalanceToBeApplied = min(($price*$maxWalletClaim/100),($promotionalBalance*$maxWalletClaim/100),env('MAXWALLETCLAIMRS'));
            if($price > $promotionalBalance)
            {
                if(env('MAXWALLETCLAIMRS')>($promotionalBalance*$maxWalletClaim/100))
                {
                    $maxClaimMessage = " Maximum of 50% of the wallet balance can be redeemed at a time.";
                }
                else
                {
                    $maxClaimMessage = " Maximum of Rs".env('MAXWALLETCLAIMRS')." of the promotional wallet balance can be redeemed at a time.";
                }
                
            }
            else
            {
                $maxClaimMessage = " Wallet balance can be redeemed for upto 50% of the booking amount.";
            }

            if($request->amount_to_apply > $price)
            {
                if($price < ($nonPromotionalBalance+$promotionalBalanceToBeApplied))
                {
                    $applied_balance = $price;
                    $price_after_applying_balance = $price - $applied_balance;
                    $message = "Wallet balance applied successfully.";
                }
                else
                {
                    $applied_balance = $nonPromotionalBalance+$promotionalBalanceToBeApplied;
                    $price_after_applying_balance = $price - $applied_balance;
                    $message = "Wallet balance applied successfully.".$maxClaimMessage;
                }
            }
            else
            {
                if($request->amount_to_apply < ($nonPromotionalBalance+$promotionalBalanceToBeApplied))
                {
                    $applied_balance = $request->amount_to_apply;
                    $price_after_applying_balance = $price - $applied_balance;
                    $message = "Wallet balance applied successfully.";
                }
                else
                {
                    $applied_balance = $nonPromotionalBalance+$promotionalBalanceToBeApplied;
                    $price_after_applying_balance = $price - $applied_balance;
                    $message = "Wallet balance applied successfully.".$maxClaimMessage;
                }
            }
        }
        else
        {
            if($request->amount_to_apply > $price)
            {
                $applied_balance = $price;
                $price_after_applying_balance = $price - $applied_balance;
            }
            else
            {
                $applied_balance = $request->amount_to_apply;
                $price_after_applying_balance = $price - $applied_balance;
            }

            $message = "Wallet balance applied successfully.";

        }
        
        $applied_balance = min($applied_balance, $price);
        
        $price_after_applying_balance = max(0,$price_after_applying_balance);
        
        
        
        
        
        $data = [
                    'status' => true,
                    'message' => $message,
                    'applied_amount' => $applied_balance,
                    'price_after_applying_amount' =>$price_after_applying_balance,
                    'tax'=>$tax,
                    'price_with_tax'=>$price_with_tax
                ];
        return $this->respondWithSuccess($data); 
    }
    
    
    public function applyWalletAmount(Request $request)
    {
        
        $user = UserController::getUserByToken();
        
        
        $request['user_id'] = $user->id;
        
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $response = $dispatcher->with($request->all())->get('wallet/function_apply_amount');
        
        return $this->respondWithSuccess($response);
        
    }
    
    
    public function applyWalletAmountFunction(Request $request)
    {

        $user = User::where('id',$request->user_id)->first();
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        $tax = (1+($taxDetails->amount/100));
        
        if(isset($request->category))
        {
            $category = $request->category;
        }
        else
        {
            $category = "AtoA";
        }
        
        if(isset($request->model_id)&&($category!="AtoB"))
        {
            $rules = array(
                'start_date' => 'required_without_all:start_datetime,end_datetime',
                'start_time' => 'required_without_all:start_datetime,end_datetime',
                'end_date' => 'required_without_all:start_datetime,end_datetime',
                'end_time' => 'required_without_all:start_datetime,end_datetime',
                'start_datetime' =>'required_without_all:start_date,start_time,end_date,end_time',
                'end_datetime' => 'required_without_all:start_date,start_time,end_date,end_time',
                'area_id' => 'required',
                'model_id'=>'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return $this->respondWithValidationError($validator->errors());        
            }
            
            if(isset($request->start_date))
            {

                $startDateTimeStr = $request->start_date.' '.$request->start_time.':00';
                $endDateTimeStr = $request->end_date.' '.$request->end_time.':00';

            }
            else
            {
                $startDateTimeStr = $request->start_datetime;
                $endDateTimeStr = $request->end_datetime;

            }
        
            
            $dispatcher = app('Dingo\Api\Dispatcher');
            $priceObj = $dispatcher->with([
                'model_id' => $request->model_id,
                'area_id' => $request->area_id,
                'start_datetime' => $startDateTimeStr, 
                'end_datetime' => $endDateTimeStr,
                'coupon'=>$request->coupon
            ])->get('bookings/total-price');
            $price = $priceObj['final_price'];
                
        }
        else
        {
            if(isset($request->estimated_distance))
            {
                if($request->estimated_distance != "Based on usage")
                {
                    $estimated_distance = floatval($request->estimated_distance);
                    $estimated_duration = floatval($request->estimated_duration);

                    $priceObj = OneWayRentalPrice::where('model_id',$request->model_id)->first();
                    $price = ($priceObj->price_per_km*$estimated_distance)+($priceObj->price_per_min*$estimated_duration);

                    $nowHour = Carbon::now()->hour;

                    if($nowHour > 15)
                    {
                        $discount = 0.8;
                    }
                    else
                    {
                        $discount = 1;
                    }
                    $price = $price*$discount;
                }
                else
                {
                    $price = round($request->price/$tax,2);
                }
            }
            else
            {
                    $price = round($request->price/$tax,2);
            }
        }
                
        $amountToBeRedeemed = min($price,$request->amount_to_apply);
        
        $walletTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        if(!$walletTransaction)
        {
            $balance = 0;
            $promotionalBalance = 0;
            $nonPromotionalBalance = 0;
        }
        else
        {
            $balance = $walletTransaction->balance;
            $promotionalBalance = $walletTransaction->promotional_balance;
            $nonPromotionalBalance = $walletTransaction->non_promotional_balance;
        }
        
        $nonPromotionalBalanceCanBeRedeemed = $nonPromotionalBalance;
        
        $maxWalletClaimPerc = DB::table('parameter')->where('parameter_name','MAXWALLETCLAIM')->first()->parameter_value;
        $maxWalletClaimRs = env('MAXWALLETCLAIMRS');
        
        $promotionalBalanceCanBeRedeemed = min($maxWalletClaimRs,($promotionalBalance*$maxWalletClaimPerc/100),($price*$maxWalletClaimPerc/100));
        
        
        if($amountToBeRedeemed > ($nonPromotionalBalanceCanBeRedeemed+$promotionalBalanceCanBeRedeemed))
        {
            
            if($price > $promotionalBalance)
            {
                if(env('MAXWALLETCLAIMRS')>($promotionalBalance*$maxWalletClaimPerc/100))
                {
                    $maxClaimMessage = " Maximum of 50% of the promotional balance can be redeemed at a time.";
                }
                else
                {
                    $maxClaimMessage = " Maximum of Rs".env('MAXWALLETCLAIMRS')." of the promotional balance can be redeemed at a time.";
                }
            }
            else
            {
                $maxClaimMessage = " Promotional wallet balance can be redeemed for upto 50% of the booking amount.";
            }
            
            $message = "Wallet balance applied successfully.".$maxClaimMessage;
            
            $promotionalBalanceToBeRedeemed = floor($promotionalBalanceCanBeRedeemed);
            $nonPromotionalBalanceToBeRedeemed = floor($nonPromotionalBalanceCanBeRedeemed);
            
            $priceToBeTaxed = ($price - $promotionalBalanceToBeRedeemed);
            $taxAmount = round($priceToBeTaxed*($taxDetails->amount/100),2);
                        
        }
        else
        {
            
            $message = "Wallet balance applied successfully.";
            
            if($amountToBeRedeemed  == $price)
            {
                if($nonPromotionalBalanceCanBeRedeemed >= ($price*(1+($taxDetails->amount/100))))
                {
                    $promotionalBalanceToBeRedeemed = 0;
                    $nonPromotionalBalanceToBeRedeemed = ($price*(1+($taxDetails->amount/100)));
                    $taxAmount = 0;
                }
                else
                {
                    $nonPromotionalBalanceToBeRedeemed = $nonPromotionalBalanceCanBeRedeemed;
                    
                    if($promotionalBalanceCanBeRedeemed >= ($price - ($nonPromotionalBalanceToBeRedeemed/(1+($taxDetails->amount/100)))))
                    {
                        $promotionalBalanceToBeRedeemed = floor($price - ($nonPromotionalBalanceToBeRedeemed/(1+($taxDetails->amount/100))));
                        $taxAmount = 0;
                    }
                    else
                    {
                        $promotionalBalanceToBeRedeemed = floor($promotionalBalanceCanBeRedeemed);
                        $priceToBeTaxed = ($price - $promotionalBalanceToBeRedeemed);
                        $taxAmount = round(($priceToBeTaxed*($taxDetails->amount/100)),2);
                        
                        $totalPricePlusTax = $priceToBeTaxed + $taxAmount;
                        $taxAmount = $totalPricePlusTax - $nonPromotionalBalanceToBeRedeemed;
                    }
                }
            }
            else
            {
                if($amountToBeRedeemed > $nonPromotionalBalanceCanBeRedeemed)
                {
                    $nonPromotionalBalanceToBeRedeemed = floor($nonPromotionalBalanceCanBeRedeemed);
                    $promotionalBalanceToBeRedeemed = min($promotionalBalanceCanBeRedeemed,($amountToBeRedeemed - $nonPromotionalBalanceCanBeRedeemed));
                    $priceToBeTaxed = $price - $promotionalBalanceToBeRedeemed;
                    $taxAmount = round($priceToBeTaxed*($taxDetails->amount/100),2);
                }
                else
                {
                    $nonPromotionalBalanceToBeRedeemed = $amountToBeRedeemed;
                    $promotionalBalanceToBeRedeemed = 0;
                    $taxAmount = round($price*($taxDetails->amount/100),2);
                }
            }
        }
        
        $applied_balance = floor($nonPromotionalBalanceToBeRedeemed) + floor($promotionalBalanceToBeRedeemed);
        $price_after_applying_balance = max(0,($price - $applied_balance));        
        
        $data = [
            'status' => true,
            'message' => $message,
            'applied_amount' => $applied_balance,
            'price_after_applying_amount' => $price_after_applying_balance,
            'tax_amount'=>$taxAmount,
            'price_with_tax'=>$price_after_applying_balance+$taxAmount,
            'promotional_credit_used'=>floor($promotionalBalanceToBeRedeemed),
            'non_promotional_credit_used'=>floor($nonPromotionalBalanceToBeRedeemed)
        ];
        
        return $data; 
        
    }
    
    
    public static function checkAppliedWalletAmount($category,$price,$userId,$amountToBeRedeemed)
    {
                
        $walletTransaction = Wallet::where('user_id',$userId)->orderBy('id','desc')->first();
        
        if(!$walletTransaction)
        {
            $balance = 0;
            $promotionalBalance = 0;
            $nonPromotionalBalance = 0;
        }
        else
        {
            $balance = $walletTransaction->balance;
            $promotionalBalance = $walletTransaction->promotional_balance;
            $nonPromotionalBalance = $walletTransaction->non_promotional_balance;
        }
        
        $nonPromotionalBalanceCanBeRedeemed = $nonPromotionalBalance;
        
        $maxWalletClaimPerc = DB::table('parameter')->where('parameter_name','MAXWALLETCLAIM')->first()->parameter_value;
        $maxWalletClaimRs = env('MAXWALLETCLAIMRS');
        
        $promotionalBalanceCanBeRedeemed = min($maxWalletClaimRs,($promotionalBalance*$maxWalletClaimPerc/100),($price*$maxWalletClaimPerc/100));
        
        
        if($amountToBeRedeemed > ($nonPromotionalBalanceCanBeRedeemed+$promotionalBalanceCanBeRedeemed))
        {
                        
            $promotionalBalanceToBeRedeemed = floor($promotionalBalanceCanBeRedeemed);
            $nonPromotionalBalanceToBeRedeemed = floor($nonPromotionalBalanceCanBeRedeemed);
                       
        }
        else
        {
            if($amountToBeRedeemed > $nonPromotionalBalanceCanBeRedeemed)
            {
                $nonPromotionalBalanceToBeRedeemed = floor($nonPromotionalBalanceCanBeRedeemed);
                $promotionalBalanceToBeRedeemed = $amountToBeRedeemed - $nonPromotionalBalanceCanBeRedeemed;
            }
            else
            {
                $nonPromotionalBalanceToBeRedeemed = $amountToBeRedeemed;
                $promotionalBalanceToBeRedeemed = 0;
            }
            
        }
        
        $applied_balance = floor($nonPromotionalBalanceToBeRedeemed) + floor($promotionalBalanceToBeRedeemed);
        
        $data = [
            'status' => true,
            'applied_amount' => $applied_balance,
            'promotional_credit_used'=>floor($promotionalBalanceToBeRedeemed),
            'non_promotional_credit_used'=>floor($nonPromotionalBalanceToBeRedeemed)
        ];
        
        return $data; 
        
    }
    
    
    
    
    public function indexWallet(Request $request)
    {
        $pageTitle = "Wallet Transactions";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>150,"title"=>"User"],
                                            ["width"=>80,"title"=>"Nature"],
                                            ["width"=>110,"title"=>"Booking ID"],
                                            ["width"=>80,"title"=>"Amount"],
                                            ["width"=>80,"title"=>"Credit/Debit"],
                                            ["width"=>80,"title"=>"Balance"],
                                            ["width"=>200,"title"=>"Notes"],
                                            ["width"=>150,"title"=>"Created By"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }

        if(isset($request->search_str))
        {
            $user = User::where('email',$request->search_str)->first();
            if(!$user)
                $walletObjects = [];
            else
                $walletObjects = Wallet::where('user_id',$user->id)->orderBy('id', 'desc')->take(500)->get();
        }
        else
            $walletObjects = Wallet::orderBy('id','desc')->take(500)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($walletObjects as $walletObject)
        {
            if(($walletObject->nature == "Credit added as refund"||$walletObject->nature == "Credit added offline")&&$walletObject->cross_reference_wallet_id == 0)
            {
                $body = [
                            ["type"=>"text","value"=>["text"=>$walletObject->id,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getUserEmail(),"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->nature,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getBookingId(),"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->amount,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->credit_or_debit,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->balance,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->notes,"pre"=>"true"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getCreatedByUserEmail(),"pre"=>"false"]],
                            ["type"=>"button","value"=>
                                                        [
                                                            [
                                                                "style_class"=>"btn btn-danger wk-btn",
                                                                "js_class"=>"delete_wallet_btn",
                                                                "id"=>"delete_wallet_id",
                                                                "data_id"=>$walletObject->id,
                                                                "icon"=>"fa fa-trash-o",
                                                                "text"=>"Reverse"
                                                            ]
                                                        ]
                            ]
                        ];
            }
            else
            {
                $body = [
                            ["type"=>"text","value"=>["text"=>$walletObject->id,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getUserEmail(),"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->nature,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getBookingId(),"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->amount,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->credit_or_debit,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->balance,"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->notes,"pre"=>"true"]],
                            ["type"=>"text","value"=>["text"=>$walletObject->getCreatedByUserEmail(),"pre"=>"false"]],
                            ["type"=>"text","value"=>["text"=>"","pre"=>"false"]]   
                    ];   
            }
        
            array_push($objectTableArray['body'],$body);
            
        }

        $searchBar = [];
        $searchBar['url'] = '/admin/add_wallet_to_user';
        $searchBar['method'] = 'get';
        $searchBar['placeholder'] = "Search By Email";
        
        $addObject = [];
        $addObject['modal_name'] = "add_wallet_modal";
        $addObject['add_button_text'] = "Add Balance";
        $addObject['modal_title'] = "Add Wallet Transaction";
        $addObject['add_url'] = "/admin/add_wallet_to_user/add";
        $addObject['add_modal_form_items'] = [];
        
        $walletTransactionNatureList = [];
        $walletTransactionNatureListComma = DB::table('parameter')->where('parameter_name','wallet_transaction_nature')->first();
        $walletTransactionNatureListCommaExploded = explode(',',$walletTransactionNatureListComma->parameter_value);
        foreach($walletTransactionNatureListCommaExploded as $walletTransactionNatureListCommaExplodedEl)
        {
            array_push($walletTransactionNatureList,["value"=>$walletTransactionNatureListCommaExplodedEl,"text"=>$walletTransactionNatureListCommaExplodedEl]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"User","input_type"=>"text","name"=>"user_email_add"], ["type"=>"select","label"=>"Nature","name"=>"nature_add","id"=>"nature_add","default_value"=>"not_set","default_text"=>"","select_items"=>$walletTransactionNatureList],
                ["type"=>"input","label"=>"Booking ID","input_type"=>"text","name"=>"booking_id_add"],
                ["type"=>"input","label"=>"Amount","input_type"=>"text","name"=>"amount_add"],
                ["type"=>"textarea","label"=>"Notes","name"=>"notes_add"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_wallet_modal";
        $editObject['edit_btn_class'] = "edit_wallet_btn";
        $editObject['modal_title'] = "Edit Wallet";
        $editObject['edit_url'] = "/admin/add_wallet_to_user/edit";
        $editObject['ajax_url'] = "/admin/add_wallet_to_user/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"User","input_type"=>"text","name"=>"user_email_edit"], ["type"=>"select","label"=>"Nature","name"=>"nature_edit","id"=>"nature_edit","default_value"=>"not_set","default_text"=>"","select_items"=>$walletTransactionNatureList],
                ["type"=>"input","label"=>"Booking ID","input_type"=>"text","name"=>"booking_id_edit"],
                ["type"=>"input","label"=>"Amount","input_type"=>"text","name"=>"amount_edit"],
                ["type"=>"textarea","label"=>"Notes","name"=>"notes_edit"]
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_wallet_modal";
        $deleteObject['delete_btn_class'] = "delete_wallet_btn";
        $deleteObject['modal_title'] = "Reverse Wallet Transaction";
        $deleteObject['delete_url'] = "/admin/add_wallet_to_user/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to reverse this Wallet Transaction?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject','searchBar'));
        
    }
    
    public function addWallet(Request $request)
    {
        $user = User::where('email',$request->user_email_add)->first();
        if(!$user)
        {
            return redirect()->back()->withErrors(['User does not exist with the given email address.']);
        }
        if(!isset($request->onewayrental))
        {
            $booking = Booking::where('reference_id',$request->booking_id_add)->first();
        }
        else
        {
            $booking = OneWayRentalBooking::where('reference_id',$request->booking_id_add)->first();
        }
            
        if(!$booking)
        {
            $booking_id = 0;
        }
        else
        {
            $booking_id = $booking->id;
        }  
        
        $lastTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        if(!$lastTransaction)
        {
            $balance = $request->amount_add;
            $balanceInt = (int)$balance;
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline"||$request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $balanceInt;
                $promotionalBalance = 0;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
                
            }
            else if($request->nature_add == "Debit against redemption")
            {
                
                $response = Self::createWalletBalanceEntry($user->id,abs($request->amount_add),"Redeemed for a boooking",$booking_id,Auth::user()->id);   
                return redirect()->back();
            }
            else
            {
                
                $nonPromotionalBalance = 0;
                $promotionalBalance = $balanceInt;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
            
        }
        else
        {
            $balance = $lastTransaction->balance + $request->amount_add;
            $balanceInt = (int)$balance;
            
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline"||$request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance+$request->amount_add;
                $promotionalBalance = $lastTransaction->promotional_balance;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
            }
            else if($request->nature_add == "Debit against redemption")
            {
                $response = Self::createWalletBalanceEntry($user->id,$request->amount_add,"Redeemed for a boooking",$booking_id,Auth::user()->id);   
                return redirect()->back();
            }
            else
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance;
                $promotionalBalance = $lastTransaction->promotional_balance+$request->amount_add;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
        }
                
          
        
                
        Wallet::create([
                'user_id'=>$user->id, 
                'nature'=>$request->nature_add, 
                'notes'=>$request->notes_add, 
                'booking_id'=>$booking_id, 
                'amount'=>$request->amount_add, 
                'credit_or_debit'=>"C", 
                'balance'=>$balanceInt, 
                'created_by'=>Auth::user()->id,
                'promotional_balance'=>$promotionalBalance,
                'non_promotional_balance'=>$nonPromotionalBalance,
                'promotional_amount'=>$promotionalAmount,
                'non_promotional_amount'=>$nonpromotionalAmount
            ]);
        
        return redirect()->back();
    }
    
    public function addWalletApi(Request $request)
    {
        $user = User::where('email',$request->user_email_add)->first();
        if(!$user)
        {
            $this->respondWithValidationErrorStatusCode(['User does not exist with the given email address.']);
        }
        
        $lastTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        if(!$lastTransaction)
        {
            $balance = $request->amount_add;
            $balanceInt = (int)$balance;
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline"||$request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $balanceInt;
                $promotionalBalance = 0;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
            }
            else
            {
                $nonPromotionalBalance = 0;
                $promotionalBalance = $balanceInt;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
            
        }
        else
        {
            $balance = $lastTransaction->balance + $request->amount_add;
            $balanceInt = (int)$balance;
            
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline"||$request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance+$request->amount_add;
                $promotionalBalance = $lastTransaction->promotional_balance;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
            }
            else
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance;
                $promotionalBalance = $lastTransaction->promotional_balance+$request->amount_add;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
        }
                
        if(!isset($request->onewayrental))
        {
            $booking = Booking::where('reference_id',$request->booking_id_add)->first();
        }
        else
        {
            $booking = OneWayRentalBooking::where('reference_id',$request->booking_id_add)->first();
        }
        
        if(!$booking)
        {
            $booking_id = 0;
        }
        else
        {
            $booking_id = $booking->id;
        }    
        
                
        Wallet::create([
                'user_id'=>$user->id, 
                'nature'=>$request->nature_add, 
                'notes'=>$request->notes_add, 
                'booking_id'=>$booking_id, 
                'amount'=>$request->amount_add, 
                'credit_or_debit'=>"C", 
                'balance'=>$balanceInt, 
                'created_by'=>Auth::user()->id,
                'promotional_balance'=>$promotionalBalance,
                'non_promotional_balance'=>$nonPromotionalBalance,
                'promotional_amount'=>$promotionalAmount,
                'non_promotional_amount'=>$nonpromotionalAmount
            ]);
        
        $data = [
                    "status"=>true,
                    "message"=>"Wallet balance added."
            ];
            
        
        return $this->respondWithSuccess($data);
    }
    
    
    public function addWalletApiNew(Request $request)
    {
        $user = User::where('email',$request->user_email_add)->first();
        if(!$user)
        {
            $this->respondWithValidationErrorStatusCode(['User does not exist with the given email address.']);
        }
        
        $lastTransaction = Wallet::where('user_id',$user->id)->orderBy('id','desc')->first();
        
        if(!$lastTransaction)
        {
            $balance = $request->amount_add;
            $balanceInt = (int)$balance;
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline" || $request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $balanceInt;
                $promotionalBalance = 0;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
            }
            else
            {
                $nonPromotionalBalance = 0;
                $promotionalBalance = $balanceInt;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
            
        }
        else
        {
            $balance = $lastTransaction->balance + $request->amount_add;
            $balanceInt = (int)$balance;
            
            if($request->nature_add == "Credit added as refund" || $request->nature_add == "Credit added offline"||$request->nature_add == "Refunded for extra money in one way rental")
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance+$request->amount_add;
                $promotionalBalance = $lastTransaction->promotional_balance;
                
                $promotionalAmount = 0;
                $nonpromotionalAmount = (int)$request->amount_add;
            }
            else
            {
                $nonPromotionalBalance = $lastTransaction->non_promotional_balance;
                $promotionalBalance = $lastTransaction->promotional_balance+$request->amount_add;
                
                $nonpromotionalAmount = 0;
                $promotionalAmount = (int)$request->amount_add;
            }
        }
                
        if(!isset($request->onewayrental))
        {
            $booking = ReservationInfo::where('id',$request->reservation_id)->first();
        }
        else
        {
            $booking = OneWayRentalBooking::where('id',$request->booking_id_add)->first();
        }
        
        if(!$booking)
        {
            $booking_id = 0;
        }
        else
        {
            $booking_id = $booking->id;
        }    
        
                
        $walletTxn = Wallet::create([
                'user_id'=>$user->id, 
                'nature'=>$request->nature_add, 
                'notes'=>$request->notes_add, 
                'booking_id'=>$booking_id, 
                'amount'=>$request->amount_add, 
                'credit_or_debit'=>"C", 
                'balance'=>$balanceInt, 
                'created_by'=>Auth::user()->id,
                'promotional_balance'=>$promotionalBalance,
                'non_promotional_balance'=>$nonPromotionalBalance,
                'promotional_amount'=>$promotionalAmount,
                'non_promotional_amount'=>$nonpromotionalAmount
            ]);
        
        $data = [
                    "status"=>true,
                    "message"=>"Wallet balance added.",
                    "wallet_txn_id"=>$walletTxn->id
            ];
            
        
        return $this->respondWithSuccess($data);
    }
    
    public function deleteWallet($id, Request $request)
    {
        $wallet = Wallet::where('id',$id)->first();
        
        $lastTransaction = Wallet::where('user_id',$wallet->user_id)->orderBy('id','desc')->first();
        
        $nonPromotionalBalance = $lastTransaction->non_promotional_balance-$wallet->amount;
        $promotionalBalance = $lastTransaction->promotional_balance;
        
        
        $newWallet = Wallet::create([
                'user_id'=>$wallet->user_id, 
                'nature'=>"Wallet balance reversed", 
                'notes'=>(isset($request->notes_delete)?$request->notes_delete:""), 
                'booking_id'=>$wallet->booking_id, 
                'amount'=>$wallet->amount, 
                'credit_or_debit'=>"D", 
                'balance'=>($lastTransaction->balance-$wallet->amount),
                'cross_reference_wallet_id'=>$wallet->id,
                'created_by'=>Auth::user()->id,
                'promotional_balance'=>$promotionalBalance,
                'non_promotional_balance'=>$nonPromotionalBalance,
                'promotional_amount'=>0,
                'non_promotional_amount'=>$wallet->amount
        ]);
        
        Wallet::where('id',$id)->update(['cross_reference_wallet_id'=>$newWallet->id]);
        
        return redirect()->back();
    }
    
    public static function createWalletBalanceEntry($userId,$applied_wallet_amount,$nature,$bookingId,$creatingUserId)
    {
        $nonpromotionalAmount = 0;
        $promotionalAmount = 0;
        
        $last_transaction = Wallet::where('user_id',$userId)->orderBy('id','desc')->first();
        if(!$last_transaction)
        {
            $walletTxnId = 0;
        }
        else
        {
            $balance = $last_transaction->balance;

            if(($applied_wallet_amount<$balance)&&($applied_wallet_amount>0))
            {
                $walletAmount = $applied_wallet_amount;
                $walletBalance = $balance - $applied_wallet_amount;
            }
            else
            {
                $walletAmount = $balance;
                $walletBalance = 0;
            }

            if($last_transaction->non_promotional_balance > $walletAmount)
            {
                $nonPromotionalBalance = $last_transaction->non_promotional_balance - $walletAmount;
                $promotionalBalance = $last_transaction->promotional_balance;
                
                $nonpromotionalAmount = $walletAmount;
                $promotionalAmount = 0;
            }
            else
            {
                $nonPromotionalBalance = 0;
                $promotionalBalance = $last_transaction->promotional_balance - ($walletAmount - $last_transaction->non_promotional_balance);
                
                $promotionalAmount = ($walletAmount - $last_transaction->non_promotional_balance);
                $nonpromotionalAmount = $last_transaction->non_promotional_balance;
            }

            $wallet = Wallet::create([
                'user_id'=>$userId, 
                'nature'=>$nature, 
                'notes'=>"", 
                'booking_id'=>$bookingId, 
                'amount'=>$walletAmount, 
                'credit_or_debit'=>"D",
                'created_by'=>$creatingUserId,
                'balance'=>$walletBalance,
                'promotional_balance'=>$promotionalBalance,
                'non_promotional_balance'=>$nonPromotionalBalance,
                'promotional_amount'=>$promotionalAmount,
                'non_promotional_amount'=>$nonpromotionalAmount
            ]);
            
            $walletTxnId = $wallet->id;
        }
        
        return $walletTxnId;
    }
}
