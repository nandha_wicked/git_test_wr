<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Enquiry;
use App\Models\CancelledEnquiry;
use Auth;
use Mail;
use App\Jobs\PayTM;


class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquirylist=Enquiry::orderBy('id', 'desc')->get();
        return view('admin.bookingenquiry.index',compact('enquirylist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requests = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.env('GOOGLE_RECAPTCHA_SECRET').'&response='.$request['g-recaptcha-response']);
        $response = json_decode($requests);
        if(!$response->success)
        {
            return 'Please, try again. You must complete the Captcha!'; 
        }


        try {
            $enquiry = new Enquiry;
            $enquiry->firstname = $request->first_name;
            $enquiry->lastname = $request->last_name;
            $enquiry->email = $request->email;
            $enquiry->phone = $request->phone;
            $enquiry->message = $request->message;
            $enquiry->startdate = $request->startdate;
            $enquiry->enddate = $request->enddate;
            $enquiry->starttime = $request->starttime;
            $enquiry->endtime = $request->endtime;
            $enquiry->bikemodel = $request->bikemodel;
            $enquiry->save();

            $data = ['fname'=>$request->first_name, 'lname'=>$request->last_name, 'email'=>$request->email, 'mob'=>$request->phone, 'comment'=>$request->message, 'bike'=>$request->bikemodel, 'startdate'=>$request->startdate, 'starttime'=>$request->starttime,  'enddate'=>$request->enddate, 'endtime'=>$request->endtime ];


            Mail::send('emails.admin_enquiry_mail',$data,function($message) use ($data) {
                $message->to(env('ADMIN_EMAIL'));
                $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                $message->subject('WickedRide Enquiry - '.$data['fname'].' - '.$data['bike']);
            });
                       


            return 'success';
        }
        catch(Exception $e){
            // do task when error
            //echo $e->getMessage();   // insert query
            return 'error';
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function cancelledAdd(Request $request)
    {
        $count = CancelledEnquiry::where('email',$request->email)->where('enquiry',$request->enquiry)->count();
        if($count == 0)
        {
        $cancelledEnquiry = CancelledEnquiry::create(['name'=>$request->name, 'phone'=>$request->phone, 'email'=>$request->email, 'enquiry'=>$request->enquiry, 'amount'=>$request->amount,'mode'=>'RazorPay','customer_notes'=>$request->notes,'coupon'=>$request->coupon]);
        return ['cancelledId'=>$cancelledEnquiry->id];
        }
        
    }
    
    public function cancelledEnquiryNoteEdit($id,Request $request)
    {
        
        CancelledEnquiry::where('id',$request->id)->update(['notes'=>$request->note, 'updated_by'=>Auth::user()->email]);
        return redirect('admin/cancelledEnquiry');
    }
    public function cancelledEnquiry()
    {
    
        $enquiryList = CancelledEnquiry::orderBy('id', 'desc')->take(1000)->get();
        
        
         return view('admin.cancelledEnquiry.index',compact('enquiryList'));
        
    }
    
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
