<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Partner;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\City;
use App\Models\Area;
use App\Models\User;
use App\Models\BikationVendor;
use App\Models\Bikation_Reviews;
use Hash;
use DB;
use DateTime;
use App\Models\Bikation_Bookings;
use App\Models\Bikation_People_On_Trip;
use App\Models\Bikation_Trip;
use App\Models\Bikation_Refund;
use App\Models\Bikation_Banner;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\Validator;
use Response;
use Mail;

class BikationvendorAdminController extends AppController
{
    public function getuser(){

    	$users=DB::table('bikationvendor_users')
            ->join('bikation_address', 'bikationvendor_users.Address_ID', '=', 'bikation_address.Address_ID')
            ->select('bikationvendor_users.id', 'bikationvendor_users.first_name', 'bikationvendor_users.last_name', 'bikationvendor_users.email', 'bikationvendor_users.mobile_num', 'bikationvendor_users.Bank', 'bikationvendor_users.Bank_Account_Number', 'bikationvendor_users.IFSC', 'bikationvendor_users.PAN', 'bikationvendor_users.TAN', 'bikationvendor_users.SRN', 'bikationvendor_users.CIN', 'bikationvendor_users.TIN', 'bikationvendor_users.Overall_Rating', 'bikationvendor_users.Vendor_Image', 'bikationvendor_users.Contact_person', 'bikationvendor_users.Contact_Phone_Number', 'bikation_address.Door_No', 'bikation_address.Street_1', 'bikation_address.Street_2', 'bikation_address.Area', 'bikation_address.City', 'bikation_address.State', 'bikation_address.Country', 'bikation_address.ZipCode', 'bikation_address.LandLine_Number', 'bikation_address.Phone_Number', 'bikation_address.Alternate_Number', 'bikation_address.Website','bikation_address.about_me','bikationvendor_users.status')
            ->get();
        
    	return view('bikationvendor.admin.user',compact('users'));
    }
    public function activeBikation_vendor($id){
		
       $Status= DB::table ('bikationvendor_users')->select('status','first_name','last_name','email')->where('id',$id)->first();
    			
        if($Status->status == 1 ){
        $value=0;  
        BikationVendor::where('id', $id)->update([ 'status' => $value]);
      }
      if($Status->status == 0 ){
        $value=1;  
        BikationVendor::where('id', $id)->update([ 'status' => $value]);
		
		$adm_email = $Status->email;
				$data = ['fname'=>$Status->first_name .' '.$Status->last_name,'email'=>$Status->email];
                Mail::send('emails.bikationvendor_active_user', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('WickedRide Bikation Vendor Active');

                });
      }
  
    	return redirect('admin/bikation-vendor');
    }
    public function getreview(){
    
  $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->get();
  $bikation_reviewsList = Bikation_Reviews::all();
      foreach ($bikation_reviewsList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }  
    return view('bikationvendor.admin.review',compact('bikation_reviewsList','bikation_tripList'));

  }
    public function activeReviews($id){
     $status= DB::table ('bikation_reviews')->select('Status')->where('id',$id)->first();
      
      if($status->Status == 1 ){
        $value=0;  
        Bikation_Reviews::where('id', $id)->update([ 'Status' => $value]);
      }
      if($status->Status == 0 ){
        $value=1;  
        Bikation_Reviews::where('id', $id)->update([ 'Status' => $value]);
      }
      return redirect('admin/bikation-vendor-review');
    }
    public function getbookings(Request $request){
  
    $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->get();
     $selected="";
      if (isset($request->btn_submit)) {
         $bikation_bookingsList = Bikation_Bookings::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_bookingsList = Bikation_Bookings::all();
        $selected="";
      }
    
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->get();
      $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name')->where('Status', 1)->get();
      //$bikation_bookingsList = Bikation_Bookings::all();
      foreach ($bikation_bookingsList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name','Trip_Start_Date','Trip_End_Date','Start_Location','End_Location','Total_Distance','Total_riding_hours','No_of_stops')->where('Trip_ID',$model->Trip_ID)->first();
        $model['Trip_Name'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
		$model['Trip_Start_Date'] = ($Trip_Name) ? $Trip_Name->Trip_Start_Date : '';
		$model['Trip_End_Date'] = ($Trip_Name) ? $Trip_Name->Trip_End_Date : '';
		$model['Start_Location'] = ($Trip_Name) ? $Trip_Name->Start_Location : '';
		$model['End_Location'] = ($Trip_Name) ? $Trip_Name->End_Location : '';
		$model['Total_Distance'] = ($Trip_Name) ? $Trip_Name->Total_Distance : '';
		$model['Total_riding_hours'] = ($Trip_Name) ? $Trip_Name->Total_riding_hours : '';
		$model['No_of_stops'] = ($Trip_Name) ? $Trip_Name->No_of_stops : '';
      }
      foreach ($bikation_bookingsList as $model) {
        $Package_Name= DB::table('bikation_package')->select('name')->where('Package_ID',$model->Package_ID)->first();
        $model['PackageName'] = ($Package_Name) ? $Package_Name->name : '';
      }
	  
	  foreach ($bikation_bookingsList as $model) {
        // if($model->Booked_Type	 != 'Bikation_Vender'){
		
				$Customer_Name= DB::table('users')->select('first_name','last_name','email','mobile_num','location')->where('id',$model->Customer_ID)->first();
				$model['first_name'] = ($Customer_Name) ? $Customer_Name->first_name : '';
				$model['last_name'] = ($Customer_Name) ? $Customer_Name->last_name : '';
				$model['email'] = ($Customer_Name) ? $Customer_Name->email : '';
				$model['mobile_num'] = ($Customer_Name) ? $Customer_Name->mobile_num : '';
				$model['location'] = ($Customer_Name) ? $Customer_Name->location : '';
			/*   }
			   else{
				$Customer_Name= DB::table('bikationvendor_users')->select('first_name','last_name','email','mobile_num')->where('id',$model->Customer_ID)->first();
				$model['first_name'] = ($Customer_Name) ? $Customer_Name->first_name : '';
				$model['last_name'] = ($Customer_Name) ? $Customer_Name->last_name : '';
				$model['email'] = ($Customer_Name) ? $Customer_Name->email : '';
				$model['mobile_num'] = ($Customer_Name) ? $Customer_Name->mobile_num : '';
				$model['location'] =  '';
			 
			   }*/
      }
	 // echo '<pre>';print_r($bikation_bookingsList);exit;
      return view('bikationvendor.admin.booking',compact('bikation_bookingsList','bikation_packageList','bikation_tripList','bikation_AlltripList','selected'));
    
  }
  public function get_refundList(Request $request){
  
    $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->get();
     $selected="";
      if (isset($request->btn_submit)) {
         $bikation_bookingsList = Bikation_Bookings::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_bookingsList = Bikation_Bookings::where('Payment_status',1)->where('Booked_Type','User')->where('Razorpay_refund_status','<>','refund')->where('Razorpay_refund_id','')->get();
        $selected="";
      }
    
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->get();
      $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name')->where('Status', 1)->get();
      //$bikation_bookingsList = Bikation_Bookings::all();
      foreach ($bikation_bookingsList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      
	   foreach ($bikation_bookingsList as $model) {
				$Customer_Name= DB::table('users')->select('first_name','last_name','email','mobile_num','location')->where('id',$model->Customer_ID)->first();
				$model['first_name'] = ($Customer_Name) ? $Customer_Name->first_name : '';
				$model['last_name'] = ($Customer_Name) ? $Customer_Name->last_name : '';
				$model['email'] = ($Customer_Name) ? $Customer_Name->email : '';
				$model['mobile_num'] = ($Customer_Name) ? $Customer_Name->mobile_num : '';
				$model['location'] = ($Customer_Name) ? $Customer_Name->location : '';
      }
      return view('bikationvendor.admin.refund',compact('bikation_bookingsList','bikation_packageList','bikation_tripList','bikation_AlltripList','selected'));
    
  }
  public function getpeople_on_trip(Request $request){
    
     $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->get();
     $selected="";
      if (isset($request->btn_submit)) {
         $bikation_People_On_TripList = Bikation_People_On_Trip::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_People_On_TripList = Bikation_People_On_Trip::all();
        $selected="";
      }
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->get();
      //$bikation_People_On_TripList = Bikation_People_On_Trip::all();
      foreach ($bikation_People_On_TripList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      return view('bikationvendor.admin.peopleontrip',compact('bikation_People_On_TripList','bikation_tripList','bikation_AlltripList','selected'));
    
  }
  public function payment_refund($id){
      
     $pay_id =  DB::table('bikation_bookings')->select('Razorpay_id','Trip_ID','Booking_ID','Customer_ID')->where('Booking_ID',$id)->first();

      $reference_ID =  DB::table('bikation_refund')->select('transaction_ID_from_PG')->where('transaction_ID_from_PG',$pay_id->Razorpay_id)->first();

    if(empty($reference_ID->transaction_ID_from_PG)){
    $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

    $payment = $api->payment->fetch($pay_id->Razorpay_id);
	
	if($payment['status'] == 'captured')
	{
    $refund = $payment->refund();
	
     Bikation_Refund::create(['reference_ID'=>$refund->id,'amount'=>$refund->amount,'transaction_ID_from_PG'=>$refund->payment_id,'created_on'=>$refund->created_at,'Created_By'=>'admin','Booking_ID'=>$pay_id->Booking_ID,'user'=>$pay_id->Customer_ID,'Trip_ID'=>$pay_id->Trip_ID,'status'=>1]);
       Bikation_Bookings::where('Booking_ID',$id)->update(['Razorpay_refund_status' =>$refund->entity, 'Razorpay_refund_id'=>$refund->id,'Razorpay_amount_refunded'=> $refund->amount]);
	 return redirect('admin/bikation-vendor-refund');
	}
	else{
		 return redirect('admin/bikation-vendor-refund')->withErrors('The payment has been fully refunded already.');
	}
	
   }
   return redirect('admin/bikation-vendor-refund')->withErrors('The payment has been fully refunded already');
  }
   public function show_refund_list(){
    
      $bikation_RefundList = Bikation_Refund::all();
	foreach ($bikation_RefundList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
	  
      return view('bikationvendor.admin.show_refund_list',compact('bikation_RefundList'));
   
  }
  public function get_trip(){
    
      //$bikation_tripList = Bikation_Trip::where('Approve_By_Admin',1)->orwhere('Approve_By_Admin',2)->orwhere('Approve_By_Admin',3)->get();
	  
	  $bikation_tripList = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID','bikation_trip.Trip_type','bikation_trip.Total_riding_hours','bikation_trip.No_of_stops', 'bikation_trip.Status', 'bikation_trip.Status','bikation_trip.ticket_denomination','bikation_trip.Experience_Level_required','bikation_trip.Approve_By_Admin','bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Start_Location', 'bikation_trip.End_Location', 'bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Preffered_Bikes','bikation_trip.Trip_url','bikation_trip.No_of_tickets_available','bikation_trip.Trip_Description','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.name','bikation_package.Description','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikationvendor_users.first_name','bikationvendor_users.last_name','bikationvendor_users.email','bikationvendor_users.mobile_num')
            ->where('bikation_trip.Approve_By_Admin', '=', 1)
			->orwhere('bikation_trip.Approve_By_Admin', '=', 2)
			->orwhere('bikation_trip.Approve_By_Admin', '=', 3)
			->groupby('Trip_ID')
            ->get();
			
      return view('bikationvendor.admin.trip',compact('bikation_tripList'));
   
  }
   public function change_trip($Trip_ID){
     $status= DB::table ('bikation_trip')->select('Approve_By_Admin')->where('Trip_ID',$Trip_ID)->first();
      
      if($status->Approve_By_Admin == 2 ){
       $value=1; 
        $Approved_By="session_id"; 
        Bikation_Trip::where('Trip_ID', $Trip_ID)->update([ 'Approve_By_Admin' => $value,'Approved_By'=>$Approved_By]);
		
		$bikation_trip_data = DB::table('bikation_trip')->select('Trip_ID','Trip_Name','Vendor_ID')->where('Trip_ID',$Trip_ID)->first();
		$bikation_vendor_data = DB::table('bikationvendor_users')->select('first_name','last_name','email','mobile_num')->where('id',$bikation_trip_data->Vendor_ID)->first();
		
		$data = ['Trip_Name'=>$bikation_trip_data->Trip_Name, 'fname'=>$bikation_vendor_data->first_name .' '.$bikation_vendor_data->last_name, 'email'=>$bikation_vendor_data->email, 'mob'=>$bikation_vendor_data->mobile_num];
		$adm_email = $bikation_vendor_data->email;
                Mail::send('emails.admin_trip_approve_mail', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('WickedRide Bikation Trip Approve');

                });
      }
       if($status->Approve_By_Admin == 3 ){
       $value=1; 
        $Approved_By="session_id"; 
        Bikation_Trip::where('Trip_ID', $Trip_ID)->update([ 'Approve_By_Admin' => $value,'Approved_By'=>$Approved_By]);
      }
      if($status->Approve_By_Admin == 1 ){
       $value=3;  
          $Cancel_approved_by="session_id";
          $Cancelled_on = new DateTime();
          $Cancelledon = $Cancelled_on->format('Y-m-d H:i:s');
        Bikation_Trip::where('Trip_ID', $Trip_ID)->update([ 'Approve_By_Admin' => $value,'Cancelled_on'=> $Cancelledon,'Cancel_approved_by'=>$Cancel_approved_by]);
      }
      return redirect('admin/bikation-vendor-trip');
    }
	
	 public function get_packages(Request $request)
   {
	   $rules = array();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
	     $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name','Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Status', 1)->where('Trip_ID', $request->Trip_ID)->get();
		}
		 return Response::json(array(
			 'success' => $status,
            'bikation_packageList' => $bikation_packageList,
            'errors' => $validator->getMessageBag()->toArray()
            
        ));
		 //return $bikation_packageList;
   }

   public function get_banner(Request $request)
   {
	   $rules = array();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
	     $bikation_bannerList = DB::table('bikation_banner')->select('banner_title','image')->where('status', 1)->first();
		}
		/* return Response::json(array(
			 'success' => $status,
            'bikation_bannerList' => $bikation_bannerList,
            'errors' => $validator->getMessageBag()->toArray()
            
        ));*/
		  return view('bikationvendor.admin.banner',compact('bikation_bannerList'));
		 //return $bikation_packageList;
   }
   
public function add_banner(Request $request)
   {
	   if($request->file('banner_img')) 
	   {
	       $file = $request->file('banner_img');
           $size = filesize( $file);
           $tmpFileName = time() . '-' . $file->getClientOriginalName();
           list($width, $height) = getimagesize($file);
           $tmpFilePath_orignal = 'img/trip_banner/';
           $file->move($tmpFilePath_orignal, $tmpFileName);
           $image_path = '//'.$_SERVER['HTTP_HOST'].'/'.$tmpFilePath_orignal . $tmpFileName;
	   }
	  else	   
	  {
		  $image_path = $request->prev_img;
	  }
	  
	
			DB::table('bikation_banner')->update(['status' => 0]);
		//Bikation_Banner:: update(['status' => 0]);
	
	  $inseart= Bikation_Banner::create(['banner_title' =>$request->banner_text, 'image' => $image_path,'status' => 1]);
 	  return redirect('admin/bikation-banner');
   }
   
    public function get_footer(Request $request)
   {
	   $rules = array();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
	     $bikation_bannerList = DB::table('bikation_footer')->select('desc')->first();
		}
		
		  return view('bikationvendor.admin.bikation_footer',compact('bikation_bannerList'));
   }
   
public function add_footer(Request $request)
   {
	   	DB::table('bikation_footer')->where('id',1)->update(['desc' => $request->footer_text]);
		//Bikation_Banner:: update(['status' => 0]);
	  return redirect('admin/bikation-footer');
   }
}
