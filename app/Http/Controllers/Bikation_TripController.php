<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Trip;
use Session;
use Safeurl;
use Mail;

class Bikation_TripController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){
    if(Session::has('email') && Session::has('First_Name') ){
      $bikation_tripList = Bikation_Trip::where('Vendor_ID',Session::get('Vendor_ID'))->get();
      $Last_Trip_ID = DB::table('bikation_trip')->select('Trip_ID','Vendor_ID','Trip_Name')->where('Trip_ID', DB::raw("(select max(`Trip_ID`) from bikation_trip)"))->where('Vendor_ID',Session::get('Vendor_ID'))->first();
      /*if(isset($Last_Trip_ID)){

       $bikation_package =  DB::table('bikation_package')->select('Trip_ID')->where('Trip_ID',$Last_Trip_ID->Trip_ID)->first();
        if(empty($bikation_package)){
         return redirect('bikation-package')->withErrors('Please add package information for "'.$Last_Trip_ID->Trip_Name.'" trip.');
      }

      $bikation_addon =  DB::table('bikation_addon')->select('Trip_ID')->where('Trip_ID',$Last_Trip_ID->Trip_ID)->first();
      if(empty($bikation_addon)){
         return redirect('bikation-addon')->withErrors('Please add addon information for "'.$Last_Trip_ID->Trip_Name.'" trip.');
      }
        $bikation_itinerary =  DB::table('bikation_itinerary')->select('Trip_ID')->where('Trip_ID',$Last_Trip_ID->Trip_ID)->first();
         if(empty($bikation_itinerary)){
         return redirect('bikation-itinerary')->withErrors('Please add itinerary information for "'.$Last_Trip_ID->Trip_Name.'" trip.');
      }
         $bikation_trip_photo =  DB::table('bikation_trip_photo')->select('Trip_ID')->where('Trip_ID',$Last_Trip_ID->Trip_ID)->first();
          if(empty($bikation_trip_photo)){
         return redirect('bikation-trip-photo')->withErrors('Please add Trip photo for "'.$Last_Trip_ID->Trip_Name.'" trip.');
      }
     }*/
      return view('bikationvendor.front.trip_index',compact('bikation_tripList'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addTrip(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array('Trip_type' => 'required',
                      'Trip_Name'    => 'required',
                      'Status' => 'required',
                      //'Total_No_of_Tickets' => 'required',
                     // 'No_of_tickets_available' => 'required',
                      'Start_Location' => 'required',
                      'End_Location' => 'required',
                      'Total_Distance' => 'required',
                      'Total_riding_hours' => 'required',
                      'No_of_stops' => 'required',
                      'Preffered_Bikes' => 'required',
                      'General_Instructions' => 'required',
                      'Trip_Description' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-trip')->withErrors($validator->errors());
      }
      else{
        $Trip_url=Safeurl::make($request->Trip_Name);
         $old_Trip_url =  DB::table('bikation_trip')->select('Trip_url')->where('Trip_url',$Trip_url)->first();
         if(empty($old_Trip_url)){
           $Safeurl= $Trip_url;
         }else{
           $First_Name = Session::get('First_Name');
           $encode_first_name= str_replace(' ', '-', $First_Name);
           $Safeurl= $Trip_url.'-'.$encode_first_name;
         }
        $Created_On = new DateTime();
        $CreatedOn= $Created_On->format('Y-m-d H:i:s');
        $Created_by=Session::get('Vendor_ID');
        $Approve_requested_on=$request['Approve_requested_on'];
        $Approverequestedon= date('Y-m-d', strtotime($Approve_requested_on));
        $Approved_on=$request['Approved_on'];
        $Approvedon= date('Y-m-d', strtotime($Approved_on));
        $Cancelled_Request_on=$request['Cancelled_Request_on'];
        $CancelledRequeston= date('Y-m-d', strtotime($Cancelled_Request_on));
        $Cancelled_on=$request['Cancelled_on'];
        $Cancelledon= date('Y-m-d', strtotime($Cancelled_on));
        $Trip_Start_Date=$request['Trip_Start_Date'];
        $TripStartDate= date('Y-m-d  H:i:s', strtotime($Trip_Start_Date));
        $Trip_End_Date=$request['Trip_End_Date'];
        $TripEndDate= date('Y-m-d  H:i:s', strtotime($Trip_End_Date));
        $Approve_By_Admin=0;
        $Send_Approve_By_Admin=0;
        Bikation_Trip::create(['Vendor_ID' => $request->Vendor_ID,'Trip_type' => $request->Trip_type,'Trip_Name' => $request->Trip_Name,'Trip_url'=>$Safeurl, 'Created_On' => $CreatedOn,'Created_by' => $Created_by,'Status' => $request->Status,'Approve_requested_on'=>$Approverequestedon,'Approved_By' => $request->Approved_By,'Approved_on' => $Approvedon, 'Cancelled_Request_on' => $CancelledRequeston,'Cancel_approved_by' => $request->Cancel_approved_by,'Cancelled_on' => $Cancelledon,'Total_No_of_Tickets'=>$request->Total_No_of_Tickets,'No_of_tickets_available' => $request->No_of_tickets_available,'Trip_Start_Date' => $TripStartDate, 'Trip_End_Date' => $TripEndDate,'Start_Location' => $request->Start_Location,'End_Location' => $request->End_Location,'Total_Distance'=>$request->Total_Distance,'Total_riding_hours' => $request->Total_riding_hours,'No_of_stops' => $request->No_of_stops,'ticket_denomination' => $request->ticket_denomination,'Experience_Level_required'=>$request->Experience_Level_required,'Preffered_Bikes' => $request->Preffered_Bikes,'General_Instructions'=>$request->General_Instructions,'Trip_Description'=>$request->Trip_Description,'Approve_By_Admin'=>$Approve_By_Admin,'Send_Approve_By_Admin'=>$Send_Approve_By_Admin]);
        return redirect('bikation-trip');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($Trip_ID, Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array('Trip_type' => 'required',
                      'Trip_Name'    => 'required',
                      'Status' => 'required',
                     // 'Total_No_of_Tickets' => 'required',
                    //  'No_of_tickets_available' => 'required',
                      'Start_Location' => 'required',
                      'End_Location' => 'required',
                      'Total_Distance' => 'required',
                      'Total_riding_hours' => 'required',
                      'No_of_stops' => 'required',
                      'Preffered_Bikes' => 'required',
                      'General_Instructions' => 'required',
                      'Trip_Description' => 'required');
      $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('bikation-trip')->withErrors($validator->errors());        
    }
    else{
      $Trip_url = Safeurl::make($request->Trip_Name);
      $old_Trip_url =  DB::table('bikation_trip')->where('Trip_ID',$Trip_ID)->first();
        if($old_Trip_url->Trip_url != $Trip_url){
          $First_Name = Session::get('First_Name');
           $encode_first_name= str_replace(' ', '-', $First_Name);
           $Safeurl= $Trip_url.'-'.$encode_first_name;
         }else{
            $Safeurl= $Trip_url;
         }
      $Created_On = $request['Created_On'];
      $CreatedOn = date('Y-m-d',strtotime($Created_On));
      $Approve_requested_on = $request['Approve_requested_on'];
      $Approverequestedon = date('Y-m-d',strtotime($Approve_requested_on));
      $Approved_on = $request['Approved_on'];
      $Approvedon = date('Y-m-d', strtotime($Approved_on));
      $Cancelled_Request_on = $request['Cancelled_Request_on'];
      $CancelledRequeston = date('Y-m-d',strtotime($Cancelled_Request_on));
      $Cancelled_on = $request['Cancelled_on'];
      $Cancelledon = date('Y-m-d', strtotime($Cancelled_on));
      $Trip_Start_Date = $request['TripStartDate'];
      $TripStartDate = date('Y-m-d H:i:s', strtotime($Trip_Start_Date));
      $Trip_End_Date = $request['TripEndDate'];
      $TripEndDate = date('Y-m-d H:i:s', strtotime($Trip_End_Date));
	//  echo $TripEndDate;exit;
      Bikation_Trip::where('Trip_ID', $Trip_ID)
        ->update(['Vendor_ID' => $request->Vendor_ID,'Trip_type' => $request->Trip_type,'Trip_Name' => $request->Trip_Name,'Trip_url'=>$Safeurl, 'Created_On' => $CreatedOn,'Created_by' => $request->Created_by,'Status' => $request->Status,'Approve_requested_on'=>$Approverequestedon,'Approved_By' => $request->Approved_By,'Approved_on' => $Approvedon, 'Cancelled_Request_on' => $CancelledRequeston,'Cancel_approved_by' => $request->Cancel_approved_by,'Cancelled_on' => $Cancelledon,'Total_No_of_Tickets'=>$request->Total_No_of_Tickets,'No_of_tickets_available' => $request->No_of_tickets_available,'Trip_Start_Date' => $TripStartDate, 'Trip_End_Date' => $TripEndDate,'Start_Location' => $request->Start_Location,'End_Location' => $request->End_Location,'Total_Distance'=>$request->Total_Distance,'Total_riding_hours' => $request->Total_riding_hours,'No_of_stops' => $request->No_of_stops,'ticket_denomination' => $request->ticket_denomination,'Experience_Level_required'=>$request->Experience_Level_required,'Preffered_Bikes' => $request->Preffered_Bikes,'General_Instructions'=>$request->General_Instructions,'Trip_Description'=>$request->Trip_Description]);
        return redirect('bikation-trip');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function delete($Trip_ID){
    if(Session::has('email') && Session::has('First_Name') ){
      $packages = Bikation_Trip::where('Trip_ID', '=', $Trip_ID)->delete();
      return redirect('bikation-trip');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function send_approve_request($Trip_ID){
  
    if(Session::has('email') && Session::has('First_Name') ){
      $send_approve_request = DB::table('bikation_trip')->select('Approve_By_Admin')->where('Trip_ID',$Trip_ID)->first();

      if($send_approve_request->Approve_By_Admin == 0){
         $value=2;
        $Created_On = new DateTime();
        $CreatedOn= $Created_On->format('Y-m-d H:i:s');

      $Approve_requested_on = new DateTime();
      $Approverequestedon = $Approve_requested_on->format('Y-m-d H:i:s');
      
        Bikation_Trip::where('Trip_ID', $Trip_ID)->update([ 'Approve_By_Admin' => $value,'Approve_requested_on'=>$Approverequestedon,'Send_Approve_By_Admin'=> $value]);
		
		$bikation_trip_data = DB::table('bikation_trip')->select('Trip_ID','Trip_Name','Vendor_ID')->where('Trip_ID',$Trip_ID)->first();
		$bikation_vendor_data = DB::table('bikationvendor_users')->select('first_name','last_name','email','mobile_num')->where('id',$bikation_trip_data->Vendor_ID)->first();
		$data = ['Trip_Name'=>$bikation_trip_data->Trip_Name, 'fname'=>$bikation_vendor_data->first_name .' '.$bikation_vendor_data->last_name, 'email'=>$bikation_vendor_data->email, 'mob'=>$bikation_vendor_data->mobile_num];
		$adm_email = env('BIKATION_ADMIN_EMAIL');
                Mail::send('emails.vendor_trip_approve_mail', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('WickedRide Bikation Vendor Trip');

                });
      }
      /* if($send_approve_request->Approve_By_Admin == 1){
        $value=3;
        $Cancelled_Request_on = new DateTime();
        $CancelledRequeston = $Cancelled_Request_on->format('Y-m-d H:i:s');

        Bikation_Trip::where('Trip_ID', $Trip_ID)->update([ 'Approve_By_Admin' => $value,'Cancelled_Request_on'=>$CancelledRequeston,'Send_Approve_By_Admin'=> $value]);
      }*/
      return redirect('bikation-trip');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  
}
