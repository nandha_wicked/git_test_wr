<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Dingo\Api\Routing\Helpers;
use App\Models\User;
use App\Models\Booking;
use App\Models\Slot;
use App\Models\Bike;
use App\Models\Image;
use App\Models\Wallet;
use App\Models\TempUser;
use App\Models\UserCohort;
use App\Models\Area;
use App\Models\City;
use App\Models\BikeModel;
use App\Models\Parameters;
use App\Models\UserDocImage;
use App\Models\OneWayRentalBike;
use App\Models\OneWayRentalBooking;
use App\Models\OneWayRentalLocation;
use App\Models\ReservationInfo;
use App\Models\OTP;
use Crypt;
use Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\UserTransformer;
use Mail;
use Config;
use Auth;
use Carbon\Carbon;
use DB;
use Input;
use Curl;

class UserController extends AppController
{
    use Helpers;

    protected $userTransformer;

    function __construct(UserTransformer $userTransformer){
      $this->userTransformer = $userTransformer;
    }
    
    public function index(Request $request){

		$selected="";
		$search_val ="";
		$user_sta ="";
		if (isset($request->btn_submit)) 
        {
			$seach_by=$request->seach_by;
			$search_text=$request->tx_search;
			$user_status=$request->user_status;
			if($user_status == 'Approve')
			{ $chk_with = '=';}
			else
			{ $chk_with = '!='; }
			
            $userList = User::search($search_text)->get();
            $userList = $userList->keyBy('id');
			$selected=$request->seach_by;
			$search_val = $search_text;
			$user_sta = $request->user_status;
         
      }
	  else
	  {
		  $userList = User::orderBy('id','desc')->take(500)->get();
		  $selected="";
		  $search_val = "";
		  $user_sta = "";
	  }
        
		foreach ($userList as $model) {
        $user_doc_List = DB::table ('user_doc_image')->select('doc_type','doc','doc_reason','status','id','user_id')
		->where('user_id', $model->id)
		->orderBy('created_at', 'desc')
		->get();
            foreach($user_doc_List as $user_doc_ListEl)
            {
                if(!(substr($user_doc_ListEl->doc,0,4) == "http"))
                {
                    $user_doc_ListEl->doc = env('HTTPSORHTTP').":".$user_doc_ListEl->doc;
                }
            }
		
		$model->user_doc_List = $user_doc_List;
		
      }
	  
		
        return view('admin.user.index',compact('userList','selected','search_val','user_sta'));
    }
	
	public function document_delete($doc_id,Request $request)
	{
		UserDocImage::where('id', '=', $doc_id)->delete();
		return redirect('admin/user');
	}
	public function document_user_delete($doc_id,Request $request)
	{
		UserDocImage::where('id', '=', $doc_id)->delete();
		return redirect('edituser');
	}
	public function document_status(Request $request)
	{

        UserDocImage::where('id', $request->doc_id)->update([ 'doc_reason' => $request->user_document_reason,'status' => $request->status]);
        if($request->status == 0)
        {   
            $status = 'rejected';
            if($request->user_document_reason == "")
            {
                $user_document_reason = "Reason Not Set";
            }
            else
            {
                $user_document_reason = $request->user_document_reason;
            }
        }
        else
        {
            $status = 'approved';
            if($request->user_document_reason == "")
            {
                $user_document_reason = "Document Approved";
            }
            else
            {
                $user_document_reason = $request->user_document_reason;
            }
        }
        

        $data = ['doc_type'=>$request->user_doc, 'reason'=>$user_document_reason , 'status'=>$status];

        $user_details = DB::table ('users')->select('first_name','last_name','email')->where('id', $request->user_id)->first();

            $adm_email = $user_details->email;
        Mail::send('emails.admin_document_status_mail', $data, function ($message) use ($adm_email){
                    $message->from(env('ADMIN_EMAIL'), env('SITENAMECAP').' User');
                    $message->to($adm_email)->subject('User Document Upload');
                });

         /*return redirect('admin/user');*/


        $selected="";
        $search_val ="";
        $user_sta ="";
        if (!empty($request->search_txtname) && !empty($request->search_seach_by) && !empty($request->search_user_status)) 
        {
            $seach_by=$request->search_seach_by;
            $search_text=$request->search_txtname;
            $user_status=$request->search_user_status;
            if($user_status == 'Approve')
            { $chk_with = '=';}
            else
            { $chk_with = '!='; }
            $userList = User::where($seach_by, 'LIKE', '%' . $search_text . '%')
                    ->where('user_document_status', $chk_with , 'Approve')
                    ->get();
            $selected=$request->search_seach_by;
            $search_val = $search_text;
            $user_sta = $request->search_user_status;
            //->where('Trip_Name', 'LIKE', '%' . $search_text . '%')

        }
        else
        {
            $userList = User::all();
            $selected="";
            $search_val = "";
            $user_sta = "";
        }

        foreach ($userList as $model) 
        {
            $user_doc_List = DB::table ('user_doc_image')->select('doc_type','doc','doc_reason','status','id','user_id')
            ->where('user_id', $model->id)
            ->orderBy('created_at', 'desc')
            ->get(); 

            $model->user_doc_List= $user_doc_List;

        }


        return view('admin.user.index',compact('userList','selected','search_val','user_sta'));
	}

     public function Change_doc_status($id){
     $status= DB::table ('users')->select('user_document_status','email')->where('id',$id)->first();
      
    if($status->user_document_status == "Approve" ){
        $value="Pending"; 
		$status_data = 'rejected';	
        User::where('id', $id)->update([ 'user_document_status' => $value]);
      }
      //if($status->user_document_status == "Approve" )
	else
	  {
        $value="Approve"; 
		$status_data = 'approved';
        User::where('id', $id)->update([ 'user_document_status' => $value]);
      }
	  
	   $data = ['status'=>$status_data];
			
				
				$adm_email = $status->email;
				Mail::send('emails.admin_status_mail', $data, function ($message) use ($adm_email){
					$message->from(env('ADMIN_EMAIL'), env('SITENAMECAP').' User');
                    $message->to($adm_email)->subject('User Document Upload');
                });
				
      return redirect('admin/user');
    }


    public function add(Request $request){
      $rules = array(
        'firstName' => 'required',
        'lastName' => 'required',
        'userEmail' => 'required|unique:users,email',
        'mobileNumber' => 'required',
        'userPassword' => 'required|alphaNum|min:3',
        'confirmPassword' => 'required|alphaNum|min:3'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/user')->withErrors($validator->errors());   
      }
      else{
        $fname = ucwords(strtolower($request->firstName));
        $lname = ucwords(strtolower($request->lastName));
          
          $length = 5;
            $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
          
        User::create(['first_name' => $fname, 'last_name' => $lname, 'email' => $request->userEmail, 'mobile_num' => $request->mobileNumber, 'password' => Hash::make($request->userPassword), 'referral_code'=>$referral_code]);
          
          
        
          
        return redirect('/admin/user');
      }   

    }


    public function edit($user_id,Request $request){
        
        User::where('id',$user_id)
            ->update(['first_name' => $request->firstName, 'last_name' => $request->lastName, 'email' => $request->userEmail,
                 'mobile_num' => $request->mobileNumber]);
        return redirect('/admin/user'); 
    }

    public function signin(Request $request)
    {
      $rules = array(
        'email' => 'required|email',
        'password' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->respondInvalidCredentialsError('Invalid credentials');
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->respondTokenError('Could not create Token');
        }

        // all good so return the token
        if($token){
          $user = User::where('email', $request->email)->first(); 
          $image = Image::where('id', $user['profile_img_id'])->first();
          $image = ($image) ? $image->transform() : [];
            
            if($request->gcm_token)
            {
                User::where('id',$user->id)->update(['gcm_token'=>$request->gcm_token]);
            }
            
        }
         $emptyImage = [
        'id' => "",
        'full' => "",
        'small' => "",
        'medium' => "",
        'large' => ""
      ];
        $data = [
          'id' => $user['id'],
          'first_name' => $user['first_name'],
          'last_name' => $user['last_name'],
          'email' => $user['email'],
          'dob' => $user['dob'],
          "mobile" => $user['mobile_num'],
          'token' => $token,
          'image_id' => ($image) ? $image['id'] : null,
          'image' => ($image) ? $image : $emptyImage,
          'referral_code' => $user['referral_code'],
          'referral_code_used' => $user['referral_code_used']
        ];
          
        
        
        return $this->respondWithSuccess($data);
      }
    }

    public static function getUserByToken(){
        
        if(env('SITENAMECAP')!= 'TestLocal')
        {
            try {

                if (! $user = JWTAuth::toUser(JWTAuth::getToken())) { //JWTAuth::parseToken()->authenticate()

                    return false;
                }

            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return false;

            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return false;

            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return false;

            }
        }
        else
        {
            $user = User::where('email','varunagni@gmail.com')->first();
        }
        return $user;
    }

    public static function getLoggedInUser(){
      if (Auth::check()){
        return Auth::user();
      }
      return self::getUserByToken();
    }

    public function signup(Request $request) {
      $rules = array(
        'first_name' => 'required',
        'email' => 'required|unique:users,email',
        'password' => 'required',
        'confirm_password' => 'required|same:password',
        'mobile' => 'required',
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        
        $mobileNum = str_replace("+91","",$request->mobile);
        $mobileNum = str_replace(" ","",$mobileNum);
        $mobileNum = str_replace("-","",$mobileNum);
        $mobileNum = str_replace("(","",$mobileNum);
        $mobileNum = str_replace(")","",$mobileNum);
        
        $user = User::where('mobile_num','LIKE','%'.$mobileNum.'%')->first();
        if(!(!$user))
        {
            $message = "This mobile number is already used in a sign up";
            return $this->respondLikeValidationError($message,['mobile'=>[$message]]);
        }
        
        if($request->dob)
          $dob = date('Y-m-d', strtotime($request->dob));
        else
          $dob = null;
        
        $password = password_hash($request->password, PASSWORD_DEFAULT);
          
        $fname = ucwords(strtolower($request->first_name));
        $lname = ucwords(strtolower($request->last_name));
        $referral_code_used = strtolower($request->referral_code_used);
        if(isset($request->referral_code))
        {
            $referral_code_used = strtolower($request->referral_code);
        }
        
        $length = 5;
          
        $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
          
          
        if(!isset($request->app_version))
        {
            $user = User::create(['first_name' => $fname, 'last_name' => $lname, 
            'mobile_num' => $request->mobile, 'email' => $request->email, 'password' => $password, 
            'dob' => $dob,'gcm_token'=>$request->gcm_token,'referral_code'=>$referral_code,'referral_code_used'=>$referral_code_used]);
            $credentials = $request->only('email', 'password');
            
            
            try 
            {
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) 
                {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } 
            catch (JWTException $e) 
            {
                // something went wrong whilst attempting to encode the token
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            
            // all good so return the token
            $data = [
                'id' => $user['id'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'email' => $user['email'],
                'dob' => $user['dob'],
                "mobile" => $user['mobile_num'],
                'token' => $token,
                'referral_code' => $referral_code,
                'referral_code_used' => $referral_code_used
            ];
            
            $data2 = ['fname'=>$user['first_name'], 'lname'=>$user['first_name'], 'email'=>$user['first_name'], 'mob'=>$user['first_name']];
            if(env('SERVER_STATUS')!="test")
            {
                Mail::send('emails.user_signup_mail',$data2,function($message){
                    $message->to($data2['email']);
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' SignUp');
                });

                Mail::send('emails.admin_signup_mail',$data2,function($message){
                    $message->to(env('ADMIN_EMAIL'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject(env('SITENAMECAP').' User SignUp');
                });
            }

            return $this->respondWithSuccess($data);
        }
          
        $otp = rand(1000,9999);
          
        TempUser::where('mobile_num','LIKE','%'.$request->mobile.'%')->delete();
          
        $user = TempUser::create(['first_name' => $fname, 'last_name' => $lname, 
          'mobile_num' => $request->country_code.$request->mobile, 'email' => $request->email, 'password' => $password, 
          'dob' => $dob,'gcm_token'=>$request->gcm_token,'referral_code'=>$referral_code,'referral_code_used'=>$referral_code_used,'mobile_otp'=>$otp]);
        
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');
          
        $phone_number = str_replace("+","",$request->mobile);
        $phone_number = str_replace(" ","",$phone_number);
        $phone_number = str_replace("-","",$phone_number);
        $phone_number = str_replace("(","",$phone_number);
        $phone_number = str_replace(")","",$phone_number);
        
        
          
        $smsMessage = "Greetings! Your OTP for signing up with ".env('SITENAMECAP')." is ".$otp;

          
        $response = Curl::to($smsGatewayUrl)
        ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
        ->get();
      
        $responseDecoded = json_decode($response);
        
        if($responseDecoded->type == "success")
        {
            $data = [
                    "status"=>true,
                    "message"=>"OTP sent to the registered phone number."
            ];
        }
        else
        {
            $data = [
                    "status"=>false,
                    "message"=>"OTP could not be sent to the registered phone number."
            ];
        }
        return $this->respondWithSuccess($data);
      } 

    }
    
    public function signupFromAdmin(Request $request) {
      $rules = array(
        'first_name' => 'required',
        'email' => 'required'
      );
        
      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        $user = User::where('email',$request->email)->first();
        if(!$user)
        {
        
            $newPassword = Str::random(8);
            $password = password_hash($newPassword, PASSWORD_DEFAULT);
            
            $fname = ucwords(strtolower($request->first_name));
            $lname = ucwords(strtolower($request->last_name));
            $length = 5;
            $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
 
            $user = User::create(['first_name' => $fname, 'last_name' => $lname, 
              'mobile_num' => $request->mobile, 'email' => strtolower($request->email), 'password' => $password, 'referral_code'=>$referral_code]);




            $data = ['fname'=>$request->first_name, 'lname'=>$request->last_name, 'email'=>$request->email, 'mob'=>$request->mobile, 'password'=>$newPassword,'reservationId'=>$request->reservationId];

                    Mail::send('emails.user_signup_mail_admin',$data,function($message){
                        $message->to(Input::get('email'));
                        $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                        $message->subject(env('SITENAMECAP').' SignUp');
                    });

                    Mail::send('emails.admin_signup_mail',$data,function($message){
                        $message->to(env('ADMIN_EMAIL'));
                        $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                        $message->subject(env('SITENAMECAP').' User SignUp');
                    });
            
            Booking::where('reference_id', $request->reservationId)->update(['user_id' => $user->id]);


            return "user_created";
        }
        else
        {
            Booking::where('reference_id', $request->reservationId)->update(['user_id' => $user->id]);
            return "user_exists";
        }
      } 

    }

    public function forgotPassword(Request $request){

      $rules = array(
        'email' => 'required|email'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        $email_id =$request->email;
        $password = UserController::generate_random_password(6);
        $newPassword = password_hash($password, PASSWORD_DEFAULT);
       
        $user = User::where('email', $email_id)->first();  

        if(!$user){
          return $this->respondNotFound('User not found in our system.');
        }

        $user->password = $newPassword;
        $user->save();
        $fname = $user->first_name;

        $data = ['fname'=>$fname, 'password'=>$password];
        Mail::send('emails.user_forgotPassword_mail',$data,function($message) use ($email_id){
            $message->to($email_id);
            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
            $message->subject(env('SITENAMECAP').' User ForgotPassword');
        });

        return $this->respondWithSuccess('Message successfully sent.');
      }
    }

    public static function generate_random_password($length = 5) {
      $alphabets = range('A','Z');
      $numbers = range('0','9');
      $final_array = array_merge($alphabets,$numbers);
           
      $password = '';
    
      while($length--) {
        $key = array_rand($final_array);
        $password .= $final_array[$key];
      }
    
      return $password;
    }

    public function resetPassword(Request $request){
      $email = $request->email;
      $oldPassword = $request->old_password;
      $newPassword = $request->new_password;

      $userdata = ['email' => $email, 'password' => $oldPassword];

      $rules = array(
        'email' => 'required',
        'old_password' => 'required',
        'new_password' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
          return $this->respondWithValidationError($validator->errors());
      }
      else{
        if(Auth::attempt($userdata)){
          $user = User::where('email', $email)->first();
          $user->password = password_hash($newPassword, PASSWORD_DEFAULT);
          $user->save();
          return $this->respondWithSuccess('Password reset successful');
        }
        else
          return $this->respondWithError('Password reset failed');         
      }
                  
  }

    public function getUpcomingBookingDetails(Request $request){
      $user = UserController::getUserByToken();
    
      if (!$user) {
        return $this->respondNotFound('User not found');
      }
      //$userProfileImage = Image::find($user->profile_img_id);

      $bookingsArr = array();
        $oneWayRentals = OneWayRentalBooking::where('user_id',$user->id)->get();
        foreach($oneWayRentals as $oneWayRental)
        {
            $fromLocation = OneWayRentalLocation::where('id',$oneWayRental->from_location)->first();
            $toLocation = OneWayRentalLocation::where('id',$oneWayRental->to_location)->first();
            
            $area = [
                "area"=>$fromLocation->name." to ".$toLocation->name,
                "address"=>""
            ];
            
            $model = BikeModel::where('id',$oneWayRental->model_id)->first();    
            $modelImage = $model->getThumbnailImg();
            $modelImage = $modelImage->transform();
            $model->image = $modelImage;
            $model = $model->transform();


            $bike = [
                "id" => $oneWayRental->bike_id,
                "model_id" => $model['id'],
                "model" => $model
            ];
            $items = [
                "bike" => $bike,
                "accessories" => [
                ]
            ];
            
            $data = [
                "booking_id" => $oneWayRental->reference_id,
                "items" => $items,
                "price" => $oneWayRental->price,
                "pickup_area" => $area,
                "pickup_date" => date("Y-m-d", strtotime($oneWayRental['created_at'])),
                "pickup_time" => date("H:i", strtotime($oneWayRental['created_at'])),
                "profile_image_id" => null,
                "profile_image" => null
            ];
            array_push($bookingsArr, $data);
        }
        
    
      $bookings = Booking::where('user_id', $user->id)->get();
      foreach ($bookings as $booking) {
  
        if($booking->bike_id != 0){

          $area = Area::where('id',$booking->area_id)->first();
          $model = BikeModel::where('id',$booking->model_id)->first();
            
          $modelImage = $model->getThumbnailImg();
          $modelImage = $modelImage->transform();
          $model->image = $modelImage;
          $model = $model->transform();


          $bike = [
            "id" => $booking->bike_id,
            "model_id" => $model['id'],
            "model" => $model
          ];
          $items = [
            "bike" => $bike,
            "accessories" => [
              
            ]
          ];
         
          $data = [
            "booking_id" => $booking->reference_id,
            "items" => $items,
            "price" => $booking->total_price,
            "pickup_area" => $area,
            "pickup_date" => date("Y-m-d", strtotime($booking['start_datetime'])),
            "pickup_time" => date("H:i", strtotime($booking['start_datetime'])),
            "profile_image_id" => null,
            "profile_image" => null
          ];
        
          array_push($bookingsArr, $data);
        }
      }
            
        
      return $bookingsArr;
    }

    public function getSubscriptionDetails(){
      $subscription = [
        "subscription_type" => 'MONTHLY',
        "rides_available" => "0",
        "rides_total" => "0",
        "subscription_expiry_date" => "2016-12-12 00:00:00"
      ];
      return $subscription;
    }

    public function getUpcomingEventsDetails(){
      $upcomingEvent = [
        "bikation_booking_id" => "11",
        "bikation_id" => "1", 
        "bikation" => [
          "id" => "1",
          "title" => "Leh to Ladakh", 
          "conductor" => [
            "id" => "1",
            "name" => "WickedRide",
            "image_id" => "14",  
            "image" => [
              "id" => "14",
              "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
              "small" => "",
              "medium" => "",
              "large" => ""
            ],
            "contact_number" => "7795338870",
            "avg_rating" => "3.5"
          ], 
          "start_date" => "2015-12-05", 
          "start_time" => "13:00:00",
          "duration" => "9 days", 
          "distance" => "2000 kms", 
          "price" => "Rs. 10000", 
          "start_city" => "Delhi", 
          "image_id" => "13",
          "image" => [
            "id" => "13",
            "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
            "small" => "",
            "medium" => "",
            "large" => ""
          ]
        ]
      ];

      return ['upcoming' => [$upcomingEvent], 'attended' => [$upcomingEvent]];
    }

    public function getProfile(Request $request){
      $user = UserController::getUserByToken();
      $dispatcher = app('Dingo\Api\Dispatcher');
      $bookings = $dispatcher->get('me/bookings');
      $subscription = $dispatcher->get('me/subscription');
      $events = $dispatcher->get('me/events');
      
      return [
        "bookings" => $bookings,
        "subscription" => $subscription,
        "bikations" => $events
      ];
    }

    public function uploadImage(Request $request){
      $rules = array(
        'profile_image' => 'image|max:3000',
      );
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      
      $user = UserController::getUserByToken();
      $updatedUser = false;

      $profileImg = $request->profile_image;
      
      if(isset($profileImg)){
        $profileExt = $profileImg->guessClientExtension();
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10);
        $profileImgHashName = $str.'.'.$profileExt;
        $profileImgDestinationPath = 'img/user_profile/';
        $profileImg->move($profileImgDestinationPath, $profileImgHashName);

        $image = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$profileImgDestinationPath.$profileImgHashName]);
        $updatedUser = User::where('id', $user['id'])->update(['profile_img_id' => $image['id']]);
      }
      if($updatedUser == true){
        return $this->respondWithSuccess('Profile successfully updated.');
      }      
      return $this->respondWithError('There was a problem updating the profile. Please try again.');
    }

    public function update(Request $request){
      
      $rules = array(
        'first_name' => 'min:3',
        'mobile' => 'min:10',
        'profile_image' => 'image|max:3000',
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      
      $user = UserController::getUserByToken();

      if($request->has('first_name')){
        User::where('id', $user['id'])->update(['first_name' => $request->first_name]);
      }

      if($request->has('last_name')){
        User::where('id', $user['id'])->update(['last_name' => $request->last_name]);
      }

      if($request->has('dob')){
        $date = Carbon::parse($request->dob);
        User::where('id', $user['id'])->update(['dob' => $date->toDateString()]);
      }

      $profileImg = $request->profile_image;
      
      if(isset($profileImg)){
        $profileExt = $profileImg->guessClientExtension();
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10);  
        $profileImgHashName = $str.'.'.$profileExt;
        $profileImgDestinationPath = 'img/user_profile/';
        $profileImg->move($profileImgDestinationPath, $profileImgHashName);

        $image = Image::create(['full' => '//'.$_SERVER['HTTP_HOST'].'/'.$profileImgDestinationPath.$profileImgHashName]);
        User::where('id', $user['id'])->update(['profile_img_id' => $image['id']]);
      }

      $updatedUser = User::where('id', $user['id'])->first();

      $image = Image::where('id', $updatedUser['profile_img_id'])->first();
      $image = ($image) ? $image->transform() : [];
        
        $emptyImage = [
        'id' => "",
        'full' => "",
        'small' => "",
        'medium' => "",
        'large' => ""
      ];

      $data = [
        'id' => $updatedUser['id'],
        'first_name' => $updatedUser['first_name'],
        'last_name' => $updatedUser['last_name'],
        'email' => $updatedUser['email'],
        'dob' => $updatedUser['dob'],
        'mobile' => $updatedUser['mobile_num'],
        'image_id' => ($image) ? $image['id'] : null,
        'image' => ($image) ? $image : $emptyImage
      ];
      return $this->respondWithSuccess($data);
    }
    
    
    public function repeatUsersBh6fddu()
    {
        $userListCohort = UserCohort::where('total_number_of_bookings','>',0)->take(3000)->orderBy('total_amount','desc')->orderBy('total_number_of_bookings','desc')->get();
        $userList = [];
        foreach ($userListCohort as $userCohort)
        {
            $user = User::where('id',$userCohort->user_id)->first();
            
            $bookingsCount = $userCohort->total_number_of_bookings;
            if($user->id>15)
            {
                $bookingIds = explode(',',$userCohort->booking_ids);
                $bookings = Booking::whereIn('id',$bookingIds)->get();
                $user->counts = 0;
                $user->total_amount = 0;
                $usermodels = [];
                $user->weekend_hours = 0;
                $user->weekday_hours = 0;
                $userareas = [];
                $userdates = [];
                $userids = [];
                        
                foreach($bookings as $booking)
                {
                    $user->counts++;
                    $user->total_amount += $booking->total_price;
                    array_push($usermodels,$booking->getBikeModelName($booking->bike_id));
                    
                    //Start of weekday and weekend hour calculation 
                    
                      $startDateObj = Carbon::parse($booking['start_datetime']);
                      $endDateObj = Carbon::parse($booking['end_datetime']);
                      
                    
                    $user->weekend_hours += $booking->weekend_hours;
                    $user->weekday_hours += $booking->weekday_hours;
                    
                    //End of weekday and weekend hour calculation 
                    array_push($userareas,$booking->getAreaName($booking->bike_id));
                    
                    $dateStr = $startDateObj->format('D j My ha').' to '.$endDateObj->format('D j My ha');
                    array_push($userdates, $dateStr);
                    array_push($userids, $booking->reference_id);
                    
                   
                    

                    
                    
                }
                $user->name = $user->first_name." ".$user->last_name;
                $user->models = $usermodels;
                $user->areas = $userareas;
                $user->dates = $userdates;
                $user->ids = $userids;
                
                array_push($userList, $user);
                
            }
            
            
        }
        return view('admin.repeatUsers.index',compact('userList'));
        
    }
    
    public function repeatUsersCsvBh6fddu()
    {
        $userListCohort = UserCohort::where('total_number_of_bookings','>',0)->take(3000)->orderBy('total_amount','desc')->orderBy('total_number_of_bookings','desc')->get();
        $userList = [];
        foreach ($userListCohort as $userCohort)
        {
            $user = User::where('id',$userCohort->user_id)->first();
            
            $bookingsCount = $userCohort->total_number_of_bookings;
            if($user->id>20)
            {
                $bookingIds = explode(',',$userCohort->booking_ids);
                $bookings = Booking::whereIn('id',$bookingIds)->get();
                $user->counts = 0;
                $user->total_amount = 0;
                $usermodels = [];
                $user->weekend_hours = 0;
                $user->weekday_hours = 0;
                $userareas = [];
                $userdates = [];
                $userids = [];
                        
                foreach($bookings as $booking)
                {
                    $user->counts++;
                    $user->total_amount += $booking->total_price;
                    array_push($usermodels,$booking->getBikeModelName($booking->bike_id));
                    
                    //Start of weekday and weekend hour calculation 
                    
                      $startDateObj = Carbon::parse($booking['start_datetime']);
                      $endDateObj = Carbon::parse($booking['end_datetime']);
                      
                    
                    $user->weekend_hours += $booking->weekend_hours;
                    $user->weekday_hours += $booking->weekday_hours;
                    
                    //End of weekday and weekend hour calculation 
                    array_push($userareas,$booking->getAreaName($booking->bike_id));
                    
                    $dateStr = $startDateObj->format('D j My ha').' to '.$endDateObj->format('D j My ha');
                    array_push($userdates, $dateStr);
                    array_push($userids, $booking->reference_id);
                    
                   
                    

                    
                    
                }
                $user->name = $user->first_name." ".$user->last_name;
                $user->models = $usermodels;
                $user->areas = $userareas;
                $user->dates = $userdates;
                $user->ids = $userids;
                
                array_push($userList, $user);
                
            }
            
            
        }
        return $userList;
        
    }
    
    public function repeatUserNoteEdit($id,Request $request)
    {
        User::where('id',$request->id)->update(['note'=>$request->note]);
        return redirect('admin/repeatUsersBh6fddu');
    }
    
    public function addUserToErpIndex()
    {
        $userList = User::where('email', 'like', '%wickedride.com')->get();
        $erpUsers = User::where('isOperation',1)->get();
        
        return view('admin.addUserToErp.index',compact('userList','erpUsers'));
    }
    public function addUserToErpAdd(Request $request)
    {
        User::where('id',$request->user)->update(['isOperation'=>1]);
        
        return redirect('admin/addUserToErp');
    }
    public function addUserToErpDelete($id,Request $request)
    {
        User::where('id',$id)->update(['isOperation'=>0]);
        
        return redirect('admin/addUserToErp');
    }
    
    public function userProfileIndex(Request $request)
    {
        if(Auth::check())
        {
           $user = Auth::user();
           $user->image=$user['avatar_original'];
           return view('front/user_profile',compact('user'));
            
        }
        return redirect('/');
    }
    public function updateProfile(Request $request, $id)
    {
            $user = User::findOrFail($id);
   
            $input = $request->all();
            $first_name =$input['first_name'];
            $last_name=$input['last_name'];
            $email=$input['email'];
            $mobile_num=$input['mobile_num'];
            $dob=$input['dob'];
            $old_password=$input['old_password'];
            $password=$input['password'];
            $gender=$input['gender'];
            $location=$input['location'];

            $save['first_name']=$first_name;
            $save['last_name']=$last_name;
            $save['email']=$email;
            $save['mobile_num']=$mobile_num;
            $save['dob']=$date = date('Y-m-d', strtotime($dob));
            if($old_password  != "" ){
                $save['password']=Hash::make($old_password);
            }else{
                 $save['password']=Hash::make($password);
            }
           
            $save['gender']=$gender;
            $save['location']=$location;
           
            $user->fill($save)->save();
                        
            return Response::json($user);
          
    }
    
    
    public function usersWithDocumentUploadedIndex(Request $request)
    {
        $todayCarbon = Carbon::today();
        $bookings = Booking::where('user_id','>',25)->where('start_datetime','>',$todayCarbon->format('Y-m-d H:i:s'))->orderBy('start_datetime','asc')->get();
        
        $bookingOfUsers = $bookings->groupBy('user_id');
        
        $bookingOfUsers->toArray();
        
        $users = User::whereIn('id',$bookings->pluck('user_id'))->get()->keyBy('id');
        
        $documentsByUsersObj = UserDocImage::whereIn('user_id',$bookings->pluck('user_id'))->get();
        
        $documentsByUsers = $documentsByUsersObj->groupBy('user_id');
        
        $documentsByUsers->toArray();
        
        
        foreach($bookingOfUsers as $user_id=>$bookings)
        {
            
            $users[$user_id]['latest_booking'] = $bookings[0]->start_datetime;
            $users[$user_id]['latest_booking_ref'] = $bookings[0]->reference_id;
            
        }
        
        $users = $users->sortBy('latest_booking');
        
        $usersWithDocumentUploadedPending = [];
                
        foreach($users as $user_id=>$user)
        {
            
            if(isset($documentsByUsers[$user->id]))
            {
                $approved = "true";
                
                foreach($documentsByUsers[$user_id] as $document)
                {
                    
                    if($document['status'] == 0)
                    {
                        $approved = "false";
                    }
                    
                    if(!(substr($document->doc,0,4) == "http"))
                    {
                        $document->doc = env('HTTPSORHTTP').":".$document->doc;
                    }
                }
                if($approved == "false")
                {
                    array_push($usersWithDocumentUploadedPending,['userObj'=>$user,'document'=>$documentsByUsers[$user_id]]);
                }
            }
        }
        
        return view('admin.usersWithDocumentUploadedPending.index',compact('usersWithDocumentUploadedPending'));
        
    }
    
    public function usersWithDocumentUploadedSearch(Request $request)
    {
        $todayCarbon = Carbon::today();
        
        $userSearchResult = User::search($request->search_text)->get();
        $users = (clone $userSearchResult)->keyBy('id');
        $userList = (clone $userSearchResult)->lists('id');
        
        $bookings = Booking::whereIn('user_id',$userList)->where('start_datetime','>',$todayCarbon->format('Y-m-d H:i:s'))->orderBy('start_datetime','asc')->get();
        
        $bookingOfUsers = $bookings->groupBy('user_id');
        
        $bookingOfUsers->toArray();
        
        $documentsByUsersObj = UserDocImage::whereIn('user_id',$bookings->pluck('user_id'))->get();
        
        $documentsByUsers = $documentsByUsersObj->groupBy('user_id');
        
        $documentsByUsers->toArray();
        
        
        foreach($bookingOfUsers as $user_id=>$bookings)
        {
            
            $users[$user_id]['latest_booking'] = $bookings[0]->start_datetime;
            $users[$user_id]['latest_booking_ref'] = $bookings[0]->reference_id;
            
        }
        
        
        $users = $users->sortBy('latest_booking');
        
        $usersWithDocumentUploadedPending = [];
                
        foreach($users as $user_id=>$user)
        {
            
            if(!array_key_exists($user->id,$documentsByUsers))
            {
                $documentsByUsers[$user_id] = [];
            }
            $approved = "true";

            foreach($documentsByUsers[$user_id] as $document)
            { 

                if($document['status'] == 0)
                {
                    $approved = "false";
                }

            }
            if($approved == "false"||$user->work_email=="")
            {
                array_push($usersWithDocumentUploadedPending,['userObj'=>$user,'document'=>$documentsByUsers[$user_id]]);
            }
            
        }
        
        return view('admin.usersWithDocumentUploadedPending.index',compact('usersWithDocumentUploadedPending'));
        
    }
    
    public function usersWithDocumentUploadedUpdate(Request $request)
	{

        UserDocImage::where('id', $request->doc_id)->update([ 'doc_reason' => $request->user_document_reason,'status' => $request->status]);
        
        $bikation_doc = UserDocImage::where('id', $request->doc_id)->first();
        $user = User::where('id',$bikation_doc->user_id)->first();
        
        if($request->source == "admin")
            return redirect('/admin/users_with_doc');
        elseif($request->source == "erp")
            return redirect('/erp/todaybooking');
        elseif($request->source == "a2b")
            return redirect('/a2b/get-deliveries?locationId='.$request->selected);
        else
        {
            
            //$dispatcher = app('Dingo\Api\Dispatcher');
            //$bookings = $dispatcher->with(['btn_submit'=>"true",'seach_by'=>$user->email])->post('/admin/user');
            return redirect('/admin/user?btn_submit=true&tx_search='.$user->email);
        }
            
    }
    
    public function usersWithDocumentUploadedDeleteErp(Request $request)
	{

        $bikation_doc = UserDocImage::where('id', $request->doc_id)->first();
        UserDocImage::where('id', $request->doc_id)->delete();
        $user = User::where('id',$bikation_doc->user_id)->first();
        
        if($request->source == "admin")
            return redirect('/admin/users_with_doc');
        elseif($request->source == "erp")
            return redirect()->back();
        elseif($request->source == "a2b")
            return redirect('/a2b/get-deliveries?locationId='.$request->selected);
        else
        {
            return redirect('/admin/user?btn_submit=true&tx_search='.$user->email);
        }
            
    }
    
    
    
    public function usersWithDocumentUploadedDelete($doc_id,Request $request)
	{
		UserDocImage::where('id', '=', $doc_id)->delete();
		return redirect('/admin/users_with_doc');
	}
    
    public function usersWithDocumentUploadedUpdateWorkEmailNotes($id, Request $request)
    {
        
        if($request->work_email)
        {
            if($request->nature == "add")
            {
                User::where('id',$id)->update(['work_email'=>$request->work_email]);
                return redirect('/admin/users_with_doc');
            }
            else
            {
                User::where('id',$id)->update(['work_email'=>""]);
                return redirect('/admin/users_with_doc');
            }
        }
        else
        {
            if($request->nature == "add")
            {
                User::where('id',$id)->update(['user_doc_notes'=>$request->user_doc_notes]);
                return redirect('/admin/users_with_doc');
            }
            else
            {
                User::where('id',$id)->update(['user_doc_notes'=>""]);
                return redirect('/admin/users_with_doc');
            }
        }
        
    }
    
    public function verifyOTP(Request $request)
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }
        
        $credentials = $request->only('email', 'password');
        
        $temp_user = TempUser::where('email',$request->email)->orderBy('created_at','desc')->first();
        
        if(!$temp_user)
        {
            $data = [
                "status"=>false,
                "message"=>"User not found. Please re-attempt signup"
            ];
            return $this->respondWithSuccess($data);
        }
        
        if($temp_user->otp_verification_tries > 15)
        {
            $data = [
                    "status"=>false,
                    "message"=>"Maximum OTP verification attempts crossed."
            ];
        }
        elseif($request->otp == $temp_user->mobile_otp)
        {
            $tempUserArray = $temp_user->toArray();
                        
            $user = User::create($tempUserArray);
            
            if(!$user)
            {
                $data = [
                        "status"=>false,
                        "message"=>"OTP verification failed."
                ];
                return $this->respondWithSuccess($data);
            }
            
            try {
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
       
        
            // all good so return the token
            $data = [
              "status"=>true,
              'id' => $user['id'],
              'first_name' => $user['first_name'],
              'last_name' => $user['last_name'],
              'email' => $user['email'],
              'dob' => $user['dob'],
              "mobile" => $user['mobile_num'],
              'token' => $token,
              'referral_code' => $user->referral_code,
              'referral_code_used' => $user->referral_code_used
            ];
            
            TempUser::where('email',$request->email)->delete();
            
            $dispatcher = app('Dingo\Api\Dispatcher');
            $addWalletBalance = $dispatcher->with([
                'user_email_add' => $user['email'],
                'amount_add' => env('SIGNUPBONUS'),
                'nature_add' => 'Sign up bonus',
                'notes_add' => 'Sign up bonus for signing up with referral code '.$user->referral_code_used
            ])->post('wallet/add_amount');
            
            $referringUser = User::where('referral_code',$user->referral_code_used)->first();
            if(!(!$referringUser))
            {
                $dispatcher = app('Dingo\Api\Dispatcher');
                $addWalletBalance = $dispatcher->with([
                    'user_email_add' => $referringUser->email,
                    'amount_add' => env('REFERRALBONUS'),
                    'nature_add' => 'Referral bonus',
                    'notes_add' => 'Bonus for referring a friend'
                ])->post('wallet/add_amount');
            }
        }
        else
        {
            $temp_user = TempUser::where('email',$request->email)->orderBy('created_at','desc')->update(['otp_verification_tries'=>$temp_user->otp_verification_tries+1]);
            
            $data = [
                    "status"=>false,
                    "message"=>"OTP verification failed."
            ];
            
        }
        return $this->respondWithSuccess($data);
        
    }
    
    public function generateotp_signin(Request $request)
    {

    $rules = array('mobile' => 'required');

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        
        $mobileNum = str_replace("+91","",$request->mobile);
        $mobileNum = str_replace(" ","",$mobileNum);
        $mobileNum = str_replace("-","",$mobileNum);
        $mobileNum = str_replace("(","",$mobileNum);
        $mobileNum = str_replace(")","",$mobileNum);

        $user = User::where('mobile_num','LIKE','%'.$mobileNum.'%')->first();
        if(!$user)
        {
            $message = "This mobile number is not registered";
            return $this->respondLikeValidationError($message,['mobile'=>[$message]]);
        }

        $otp = rand(1000,9999);
          
        OTP::where('mobile_num','LIKE','%'.$mobileNum.'%')->delete();
          
        $user = OTP::create(['mobile_num' => $mobileNum,'mobile_otp'=>$otp,'otp_for'=>'walk_in_book']);
        
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');      
          
        $smsMessage = "Greetings! Your OTP for WalkinBooking up with ".env('SITENAMECAP')." is ".$otp;
          
        $response = Curl::to($smsGatewayUrl)
        ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$mobileNum, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
        ->get();
      
        $responseDecoded = json_decode($response);
        
        if($responseDecoded->type == "success")
        {
            $data = [
                    "status"=>true,
                    "message"=>"OTP sent to the registered phone number."
            ];
        }
        else
        {
            $data = [
                    "status"=>false,
                    "message"=>"OTP could not be sent to the registered phone number."
            ];
        }
        return $this->respondWithSuccess($data);

        }

        
    }

   //Sign IN
    public function otp_signin_signup(Request $request)
    {

    $rules = array(
            'mobile' => 'required',
            'token' => 'required'
        );
      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
      }
      else{
        
        $mobileNum = str_replace("+91","",$request->mobile);
        $mobileNum = str_replace(" ","",$mobileNum);
        $mobileNum = str_replace("-","",$mobileNum);
        $mobileNum = str_replace("(","",$mobileNum);
        $mobileNum = str_replace(")","",$mobileNum);
        if(!empty($request->referral_code_used)) {
            $referringUser = User::where('referral_code',$request->referral_code_used)->first();
            if(!$referringUser)
            {
                $data = [
                    "status"=>false,
                    "message"=>"Entered Referral Code is Invalid."

                ];
                return $this->respondWithSuccess($data);

            }
        }
        $user = User::where('mobile_num','LIKE','%'.$mobileNum.'%')->first();
        $otpfor="signin";
        if(!$user)
        {
         $otpfor="signup";
        }

        $otp = rand(1000,9999);
          
        OTP::where('mobile_num','LIKE','%'.$mobileNum.'%')->delete();
          
        $user = OTP::create(['mobile_num' => $mobileNum,'mobile_otp'=>$otp,'otp_for'=>$otpfor]);
        
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');      
          
        $smsMessage = "Greetings! ".$otp." is your OTP for signup with ".env('SITENAMECAP');
          
        $response = Curl::to($smsGatewayUrl)
        ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$mobileNum, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
        ->get();
      
        $responseDecoded = json_decode($response);
        
        if($responseDecoded->type == "success")
        {
            $data = [
                    "status"=>true,
                    "message"=>"OTP sent to the registered phone number."
            ];
        }
        else
        {
            $data = [
                    "status"=>false,
                    "message"=>"OTP could not be sent to the registered phone number."
            ];
        }
        return $this->respondWithSuccess($data);

        }

        
    }
    public function signin_verifyOTP(Request $request)
    {

        $rules = array(
            'mobile' => 'required',
            'otp' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationError($validator->errors());
        }

        $mobileNum = str_replace("+91","",$request->mobile);
        $mobileNum = str_replace(" ","",$mobileNum);
        $mobileNum = str_replace("-","",$mobileNum);
        $mobileNum = str_replace("(","",$mobileNum);
        $mobileNum = str_replace(")","",$mobileNum);
        
        $a2buser = OTP::where('mobile_num',$mobileNum)->orderBy('created_at','desc')->first();
        
        if(!$a2buser)
        {
            $data = [
                "status"=>false,
                "message"=>"User not found. Please re-attempt signup"
            ];
            return $this->respondWithSuccess($data);
        }
        
        if($a2buser->otp_verification_tries > 15)
        {
            $data = [
                    "status"=>false,
                    "message"=>"Maximum OTP verification attempts crossed."
            ];
        }
        elseif($request->otp == $a2buser->mobile_otp)
        {
            $user=User::where('mobile_num','=',$mobileNum)->first();

        if (!$token=JWTAuth::fromUser($user)) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
      
        
            // all good so return the token
            $data = [
              "status"=>true,
              'id' => $user['id'],
              'first_name' => $user['first_name'],
              'last_name' => $user['last_name'],
              'email' => $user['email'],
              'dob' => $user['dob'],
              "mobile" => $user['mobile_num'],
              'token' => $token,
              'referral_code' => $user->referral_code,
              'referral_code_used' => $user->referral_code_used
            ];
            
            OTP::where('mobile_num',$mobileNum)->delete();
            
        }
        else
        {
            $a2buser = OTP::where('mobile_num',$mobileNum)->orderBy('created_at','desc')->update(['otp_verification_tries'=>$a2buser->otp_verification_tries+1]);
            
            $data = [
                    "status"=>false,
                    "message"=>"OTP verification failed."
            ];
            
        }
        return $this->respondWithSuccess($data);
 
    }
    
    public function resendOTP(Request $request)
    {

        $phone_number = str_replace("+","",$request->mobile);
        $phone_number = str_replace(" ","",$phone_number);
        $phone_number = str_replace("-","",$phone_number);
        $phone_number = str_replace("(","",$phone_number);
        $phone_number = str_replace(")","",$phone_number);
        //Signin for WalkinBooking
        $temp_user = OTP::where('mobile_num',$phone_number)->orderBy('created_at','desc')->first();
        if(!$temp_user) {
              $temp_user = TempUser::where('email',$request->email)->orderBy('created_at','desc')->first();
        }
        if(!$temp_user)
        {
            $data = [
                    "status"=>false,
                    "message"=>"Resending OTP failed."
            ];
            return $this->respondWithSuccess($data);
        }
        
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');

        $smsMessage = "Greetings! ".$temp_user->mobile_otp." is your OTP for signup with ".env('SITENAMECAP');
          
        $response = Curl::to($smsGatewayUrl)
        ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
        ->get();
          
        $responseDecoded = json_decode($response);
        
        if($responseDecoded->type == "success")
        {
            $data = [
                    "status"=>true,
                    "message"=>"OTP resent successfully."
            ];
        }
        else
        {
            $data = [
                    "status"=>false,
                    "message"=>"Resending OTP failed."
            ];
        }
        
        return $this->respondWithSuccess($data);
        
    }
    
    public function getReferralCode(Request $request)
    {
        $user = UserController::getUserByToken();
        $data = [
                    "referral_code"=>$user->referral_code,
            ];
        
        return $this->respondWithSuccess($data);
    }
    
    public function indexTopReferers(Request $request)
    {
        $walletTxns = Wallet::orderBy('promotional_balance','desc')->take(1000)->get()->groupBy('user_id');
        $userDetails = [];
        foreach($walletTxns as $userId=>$walletTxn)
        {
            $user = User::where('id',$userId)->first();
            $referredUsers = User::where('referral_code_used',$user->referral_code)->get();
            $referredUsersDetails = [];
            foreach($referredUsers as $referredUser)
            {
                $bookings = Booking::where('user_id',$referredUser->id)->get();
                $bookingsCount = $bookings->count();
                $bookingsTotal = $bookings->sum('total_price');
                array_push($referredUsersDetails,$referredUser->email." - Booking count - ".$bookingsCount." - Total Rs".$bookingsTotal);
                
            }
            
            $referredUsersDetailsStr = implode('<br>',$referredUsersDetails);
            
            array_push($userDetails,["email"=>$user->email,"balance"=>$walletTxn[0]->promotional_balance,"referred_emails"=>$referredUsersDetailsStr]);
            
        }
        
        return view('admin.referralUsers.index',compact('userDetails'));
        
    }
    
    
    public function indexZoneAssignment(Request $request)
    {
        $pageTitle = "Zone Assignment";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>150,"title"=>"Name"],
                                            ["width"=>500,"title"=>"Zones"],
                                            ["width"=>150,"title"=>"Email"],
                                            ["width"=>100,"title"=>"Mobile"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        if(isset($request->search_str))
            $userObjects = User::where('email',$request->search_str)->get();
        else    
            $userObjects = User::where(function ($query) {
                            $query->where('email','like','%wickedride.com');
                        })->orWhere(function ($query) {
                            $query->where('zones_assigned','<>',"");
                        })->orderBy('id','desc')->get();
        
        $objectTableArray['body'] = [];
        
        foreach($userObjects as $userObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$userObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->first_name." ".$userObject->last_name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->zonesAssigned(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->email,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->mobile_num,"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_user_btn",
                                                            "id"=>"edit_user_id",
                                                            "data_id"=>$userObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_user_btn",
                                                            "id"=>"delete_user_id",
                                                            "data_id"=>$userObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $searchBar = [];
        $searchBar['url'] = '/admin/zone-assign';
        $searchBar['method'] = 'get';
        $searchBar['placeholder'] = "Search By Email";
        
        $addObject = [];
        $addObject['modal_name'] = "add_user_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['add_button_display'] =false;
        $addObject['modal_title'] = "Add Zone Assignment";
        $addObject['add_url'] = "";
        $addObject['add_modal_form_items'] = [];
        
        
        

        
        $addObject['add_modal_form_items'] = 
            [
                
            ];
      
        
        $editObject = [];
        $editObject['modal_name'] = "edit_user_modal";
        $editObject['edit_btn_class'] = "edit_user_btn";
        $editObject['modal_title'] = "Edit User";
        $editObject['edit_url'] = "/admin/zone-assign/edit";
        $editObject['ajax_url'] = "/admin/zone-assign/details";
        $editObject['edit_modal_form_items'] = [];
        
        $zoneList = [];
        $areas = Area::where('status',1)->get();
        
        array_push($zoneList,["value"=>0,"text"=>"All Areas", "selected"=>false]);
        
        $cities = City::where('status',1)->get();
        
        foreach($cities as $city)
        {
            array_push($zoneList,["value"=>$city->city,"text"=>$city->city, "selected"=>false]);
        }
        
        foreach($areas as $area)
        {
            array_push($zoneList,["value"=>$area->id,"text"=>$area->area, "selected"=>false]);
        }
        
        $editObject['edit_modal_form_items'] = 
                [
                    
                    ["type"=>"multiSelect","label"=>"Zones", "name"=>"zone_edit", "id"=>"zone_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$zoneList, "selection_header"=>"Zones","selected_header"=>"Zones Assigned"],


                ];
        
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_user_modal";
        $deleteObject['delete_btn_class'] = "delete_user_btn";
        $deleteObject['modal_title'] = "Delete Zone Assignment";
        $deleteObject['delete_url'] = "";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Zone Assignment";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject','searchBar')); 
        
    }
    
    public function detailsZoneAssignment(Request $request)
    {
        $user = User::where('id',$request->id)->first();
        
        $data = [
                
                ["type"=>"multiselect", "id"=>"zone_id_edit", "name"=>"zone_edit","value"=>$user->zones_assigned]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editZoneAssignment($id, Request $request)
    {
        $zones = "";
      
        if(isset($request->zone_edit))
        {
            if(in_array("0",$request->zone_edit))
            {
                $zones = "0";
            }
            else
            {
                $zoneArray = [];
                foreach($request->zone_edit as $zone)
                {
                    $city = City::where('city',$zone)->first();
                    if(!$city)
                    {
                        $area = Area::where('id',$zone)->first();
                        if(!(!$area))
                        {
                            array_push($zoneArray,$area->id);
                        }
                    }
                    else
                    {
                        array_push($zoneArray,$city->city);
                    }
                }
                
                $zones = implode(',',$zoneArray);
            }
        }        

        $user = User::where('id',$id)->first();
        if(!(!$user))
        {
            DB::table('edited_users')->insert($user->toArray());

            User::where('id',$id)->update([
                'zones_assigned'=>$zones,
            ]);
        }
        
        return redirect()->back();
    }
    
    public function deleteZoneAssignment($id, Request $request)
    {
        return redirect()->back();
    }
    
    
    public function indexUserPermission(Request $request)
    {
        $pageTitle = "API User";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>150,"title"=>"Name"],
                                            ["width"=>100,"title"=>"Role"],
                                            ["width"=>150,"title"=>"Email"],
                                            ["width"=>100,"title"=>"Mobile"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        if(isset($request->search_str))
            $userObjects = User::where('email',$request->search_str)->get();
        else    
            $userObjects = User::where('email','like','%wickedride.com')->get();
        
                
        $objectTableArray['body'] = [];
        
        foreach($userObjects as $userObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$userObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->first_name." ".$userObject->last_name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->role_string,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->email,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$userObject->mobile_num,"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_user_btn",
                                                            "id"=>"edit_user_id",
                                                            "data_id"=>$userObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_user_btn",
                                                            "id"=>"delete_user_id",
                                                            "data_id"=>$userObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $searchBar = [];
        $searchBar['url'] = '/admin/user-permission';
        $searchBar['method'] = 'get';
        $searchBar['placeholder'] = "Search By Email";
        
        $addObject = [];
        $addObject['modal_name'] = "add_user_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['add_button_display'] = false;
        $addObject['modal_title'] = "Add API User";
        $addObject['add_url'] = "";
        $addObject['add_modal_form_items'] = [];
        
        $roleListArray = explode(',',Parameters::getParameter('role_list'));
        $roleList = [];
        foreach($roleListArray as $roleListArrayEl)
        {
            array_push($roleList,["value"=>$roleListArrayEl,"text"=>$roleListArrayEl, "selected"=>false]);
        }
        

        
        $addObject['add_modal_form_items'] = 
            [
                
            ];
      
        
        $editObject = [];
        $editObject['modal_name'] = "edit_user_modal";
        $editObject['edit_btn_class'] = "edit_user_btn";
        $editObject['modal_title'] = "Edit User";
        $editObject['edit_url'] = "/admin/user-permission/edit";
        $editObject['ajax_url'] = "/admin/user-permission/details";
        $editObject['edit_modal_form_items'] = [];
        
        
        $editObject['edit_modal_form_items'] = 
                [
                    
                    ["type"=>"multiSelect","label"=>"Role", "name"=>"role_edit", "id"=>"role_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$roleList, "selection_header"=>"Roles","selected_header"=>"Roles Assigned"],
                    ["type"=>"select","label"=>"Admin", "name"=>"admin_edit", "id"=>"admin_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>[["value"=>0,"text"=>"No"],["value"=>1,"text"=>"Yes"]]],
                    ["type"=>"select","label"=>"Operations", "name"=>"operations_edit", "id"=>"operations_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>[["value"=>0,"text"=>"No"],["value"=>1,"text"=>"Yes"]]]

            

                ];
        
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_user_modal";
        $deleteObject['delete_btn_class'] = "delete_user_btn";
        $deleteObject['modal_title'] = "Delete API User";
        $deleteObject['delete_url'] = "";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this API User";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject','searchBar')); 
        
    }
    
    public function detailsUserPermission(Request $request)
    {
        $user = User::where('id',$request->id)->first();
        
        $data = [
                
                ["type"=>"multiselect", "id"=>"role_id_edit", "name"=>"role_edit","value"=>$user->role_string],
                ["type"=>"select","name"=>"admin_edit","value"=>$user->isAdmin],
                ["type"=>"select","name"=>"operations_edit","value"=>$user->isOperation]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editUserPermission($id, Request $request)
    {
      
            
        if(isset($request->role_edit))
        {
            $roles = implode(',',$request->role_edit);
        }
        else
        {
            $roles = "";
        }

        $user = User::where('id',$id)->first();
        if(!(!$user))
        {
            DB::table('edited_users')->insert($user->toArray());

            User::where('id',$id)->update([
                'role_string'=>$roles,
                'isAdmin'=>$request->admin_edit,
                'isOperation'=>$request->operations_edit
            ]);
        }
        
        
        
        return redirect()->back();
    }
    
    public function deleteUserPermission($id, Request $request)
    {
        
        return redirect()->back();
    }
    
    public function getBookingHistory(Request $request)
    {
        $user = UserController::getUserByToken();
        $now = Carbon::now();
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];

        $upcomingReservations = ReservationInfo::where('user_id',$user->id)->where('end_datetime','>',$now->toDateTimeString())->where('deleted',0)->orderBy('created_at','desc')->get();
        $upcomingReservationsDetails = [];
        
        foreach($upcomingReservations as $upcomingReservation)
        {
            $details = [];
            $details['booking_id'] = $upcomingReservation->bookingHeader->ref_id;
            $details['location'] = [
                "city"=>$upcomingReservation->area->city->city,
                "area" => $upcomingReservation->area->area,
                "latitude" => $upcomingReservation->area->latitude,
                "longitude" => $upcomingReservation->area->longitude,
                "address" => strip_tags($upcomingReservation->area->address),
            ];
            $details['model'] = $upcomingReservation->model->bike_model;
            $details['make'] = $upcomingReservation->model->make->bike_make;
            $details['model_image'] = $upcomingReservation->model->getThumbnail();
            
            $details['start_datetime'] = $upcomingReservation->readableStartDateTime();
            $details['end_datetime'] = $upcomingReservation->readableEndDateTime();
                        
            $details['tax_name'] = $taxDetails->tax_name;
            $details['tax_amount'] = $taxDetails->amount;
            
            $details['price_after_discount'] = $upcomingReservation->pricingInfo->price_after_discount;
            $details['tax_amount'] = $upcomingReservation->pricingInfo->tax_amount;
            $details['price_after_tax'] = $upcomingReservation->pricingInfo->price_after_tax;
            
            array_push($upcomingReservationsDetails,$details);
        }
        
        
        $pastReservations = ReservationInfo::where('user_id',$user->id)->where('end_datetime','<=',$now->toDateTimeString())->where('deleted',0)->orderBy('created_at','desc')->get();
        $pastReservationsDetails = [];
        
        foreach($pastReservations as $pastReservation)
        {
            $details = [];
            $details['booking_id'] = $pastReservation->bookingHeader->ref_id;
            $details['location'] = [
                "city"=>$pastReservation->area->city->city,
                "area" => $pastReservation->area->area,
                "latitude" => $pastReservation->area->latitude,
                "longitude" => $pastReservation->area->longitude,
                "address" => strip_tags($pastReservation->area->address),
            ];
            $details['model'] = $pastReservation->model->bike_model;
            $details['make'] = $pastReservation->model->make->bike_make;
            $details['model_image'] = $pastReservation->model->getThumbnail();
            
            $details['start_datetime'] = $pastReservation->readableStartDateTime();
            $details['end_datetime'] = $pastReservation->readableEndDateTime();
                        
            $details['tax_name'] = $taxDetails->tax_name;
            $details['tax_amount'] = $taxDetails->amount;
            
            $details['price_after_discount'] = $pastReservation->pricingInfo->price_after_discount;
            $details['tax'] = $pastReservation->pricingInfo->tax_amount;
            $details['price_after_tax'] = $pastReservation->pricingInfo->price_after_tax;
            
            array_push($pastReservationsDetails,$details);
        }
        
        
        
        $upcomingA2BBookings = OneWayRentalBooking::where('user_id',$user->id)->where('status','Booking Made')->orderBy('created_at','desc')->get();
        $upcomingA2BBookingsDetails = [];
        foreach($upcomingA2BBookings as $upcomingA2BBooking)
        {
            $details = [];
            $details['booking_id'] = $upcomingA2BBooking->reference_id;
            $details['from_location'] = [
                "area" => $upcomingA2BBooking->fromArea->name,
                "latitude" => $upcomingA2BBooking->fromArea->latitude,
                "longitude" => $upcomingA2BBooking->fromArea->longitude,
                "address" => strip_tags($upcomingA2BBooking->fromArea->address),
                "contact_person"=>$upcomingA2BBooking->fromArea->contact_person,
                "contact_number"=>$upcomingA2BBooking->fromArea->contact_number
            ];
            
            $details['to_location'] = [
                "area" => $upcomingA2BBooking->toArea->name,
                "latitude" => $upcomingA2BBooking->toArea->latitude,
                "longitude" => $upcomingA2BBooking->toArea->longitude,
                "address" => strip_tags($upcomingA2BBooking->toArea->address),
                "contact_person"=>$upcomingA2BBooking->toArea->contact_person,
                "contact_number"=>$upcomingA2BBooking->toArea->contact_number
            ];
            
            
                        
            $details['tax_name'] = $taxDetails->tax_name;
            $details['tax_amount'] = $taxDetails->amount;
                        
            $details['price_after_discount'] = $upcomingA2BBooking->pricingInfo->price_after_discount;
            $details['tax'] = $upcomingA2BBooking->pricingInfo->tax_amount;
            $details['price_after_tax'] = $upcomingA2BBooking->pricingInfo->price_after_tax;
            
            array_push($upcomingA2BBookingsDetails,$details);
        }
        
        
        
        $pastA2BBookings = OneWayRentalBooking::where('user_id',$user->id)->where('status','Bike returned')->orderBy('created_at','desc')->get();
        $pastA2BBookingsDetails = [];
        foreach($pastA2BBookings as $pastA2BBooking)
        {
            $details = [];
            $details['booking_id'] = $pastA2BBooking->reference_id;
            $details['from_location'] = [
                "area" => $pastA2BBooking->fromArea->name,
                "latitude" => $pastA2BBooking->fromArea->latitude,
                "longitude" => $pastA2BBooking->fromArea->longitude,
                "address" => strip_tags($pastA2BBooking->fromArea->address),
                "contact_person"=>$pastA2BBooking->fromArea->contact_person,
                "contact_number"=>$pastA2BBooking->fromArea->contact_number
            ];
            
            $details['to_location'] = [
                "area" => $pastA2BBooking->toArea->name,
                "latitude" => $pastA2BBooking->toArea->latitude,
                "longitude" => $pastA2BBooking->toArea->longitude,
                "address" => strip_tags($pastA2BBooking->toArea->address),
                "contact_person"=>$pastA2BBooking->toArea->contact_person,
                "contact_number"=>$pastA2BBooking->toArea->contact_number
            ];
            
            $details['start_datetime'] = $pastA2BBooking->getReadableDateTimeWithMinutes($pastA2BBooking->delivered_at);
            $details['end_datetime'] = $pastA2BBooking->getReadableDateTimeWithMinutes($pastA2BBooking->returned_at);
                        
            $details['tax_name'] = $taxDetails->tax_name;
            $details['tax_amount'] = $taxDetails->amount;
            
            $details['price_after_discount'] = $pastA2BBooking->pricingInfo->price_after_discount;
            $details['tax'] = $pastA2BBooking->pricingInfo->tax_amount;
            $details['price_after_tax'] = $pastA2BBooking->pricingInfo->price_after_tax;
            
            array_push($pastA2BBookingsDetails,$details);
        }
        
        
        $data = [
            "upcoming_hourly_rentals"=>$upcomingReservationsDetails,
            "past_hourly_rentals"=>$pastReservationsDetails,
            "upcoming_a2b_rentals"=>$upcomingA2BBookingsDetails,
            "past_a2b_rentals"=>$pastA2BBookingsDetails
        ];
        
        return $this->respondWithSuccess($data); 
        
    }
    
    
    public function indexAddUserToDontSendList(Request $request)
    {
        $pageTitle = "Add user to dont send list";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
                
        $objectTableArray['body'] = [];
        
        
        
        $addObject = [];
        $addObject['modal_name'] = "add_user_to_dont_send_list";
        $addObject['add_button_text'] = "Add Type";
        $addObject['modal_title'] = "Add user to dont send list";
        $addObject['add_url'] = "/admin/add-user-to-dont-send-list/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Email","input_type"=>"text","name"=>"email_add"]                
            ];
        
        $editObject = [];
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    
    public function addUserToDontSendList(Request $request)
    {
        $rules = array(
            'email_add' => 'exists:users,email',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());        
        }
        
        $user = User::where('email',$request->email_add)->first();
        
        $dontSendList = explode(',',Parameters::getParameter('dont_send_email_ids'));
        
        array_push($dontSendList,$user->id);
        
        Parameters::where('parameter_name','dont_send_email_ids')->update([
            'parameter_value'=>implode(',',$dontSendList)
        ]);
        
        return redirect()->back();
    }
    
        

}
