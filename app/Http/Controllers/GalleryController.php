<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BikeModel;
use App\Models\GalleryImage;
use App\Models\Image;
use App\Models\Gallery;
use App\Transformers\ImageTransformer;
use App\Transformers\GalleryTransformer;

class GalleryController extends AppController
{

    protected $galleryTransformer;
    protected $imageTransformer;

    function __construct(GalleryTransformer $galleryTransformer, ImageTransformer $imageTransformer){
      $this->galleryTransformer = $galleryTransformer;
      $this->imageTransformer = $imageTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $gallery = new Gallery();
      // $gallery->name = $request->name;
      // $gallery->description = $request->desc;
      // $gallery->save;
      // $gallery = Gallery::create(['name' => $request->name, 'description' => $request->desc]);
      // return $gallery;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGalleryImages(Request $request){
      try{
        $bm = BikeModel::findOrFail($request->model_id);
        if (!$bm) {
          return $this->respondNotFound('Model not found');
        }
        $gallery = $bm->gallery;
        $imageData = $this->imageTransformer->transformCollection($gallery->getImages()->toArray());
        
        $gallery['images'] = $imageData;
        $galleryData = $this->galleryTransformer->transform($gallery);
        
        return $this->respondWithSuccess($galleryData);
      }
      catch(ModelNotFoundException $e){
        return $this->respondNotFound('BikeModel Not Found');
      }
      catch(Exception $e) {
        return $this->respondNotFound($e->getMessage());
      }
    }

}
