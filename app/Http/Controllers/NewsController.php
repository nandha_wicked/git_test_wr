<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Transformers\NewsTransformer;

class NewsController extends AppController{

  protected $newsTransformer;

  function __construct(NewsTransformer $newsTransformer){
    $this->newsTransformer = $newsTransformer;
  }

  public function index(){
    $newsList = News::all();
    return view('admin.news.index',compact('newsList'));
  }

  public function add(Request $request){
    $rules = array(
      'news' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/news')->withErrors($validator->errors());        
    }
    else{
      $message="successfully inserted";
      News::create(['news' => $request->news, 'status' => $request->publishStatus]);
      return redirect('/admin/news');
    }   
  }


  public function edit($news_id,Request $request){
    $rules = array(
      'news' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/news')->withErrors($validator->errors());        
    }
    News::where('id',$news_id)->where('status', 1)
      ->update(['news' => $request->news, 'status' => $request->publishStatus]);
    return redirect('/admin/news'); 
  }


  public function delete($news_id){
    News::destroy($news_id);
    return redirect('/admin/news');
  }

  public function getAllNews(){
    $news = News::where('status', 1)->get();
    $data = $this->newsTransformer->transformCollection($news->toArray());
    return $this->respondWithSuccess($data);
  }
}
