<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\ContactEnquiry;

class ContactEnquiryController extends AppController
{
    
    public function index(){

    	$contactEnquiryList=ContactEnquiry::orderBy('id','desc')->take(50)->get();
    	return view('admin.contactEnquiry.index',compact('contactEnquiryList'));
    }
}
