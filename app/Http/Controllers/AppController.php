<?php 
  namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Response;
  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use App\Models\User;
  use App\Transformers\UserTransformer;

  class AppController extends Controller{

    protected $code = 200;

    public function getStatusCode(){
      return $this->code;
    }

    public function setStatusCode($code){
      $this->code = $code;
      return $this;
    }

    public function respondNotFound($message = 'Not Found'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Error'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respondInvalidCredentialsError($message = 'Invalid creddentials'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respondTokenError($message = 'Token error'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respondEmailAlreadyTaken($message = 'The email is already registered ,Please login to continue.'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respondBookingExist($message = 'Booking Found'){
      return $this->setStatusCode(200)->respondWithError($message);
    }

    public function respond($data, $headers = []){
      return Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message){
      return $this->respond([
          'result' => [
            'message' => $message,
            'status' => false,
            'status_code' => $this->getStatusCode()
          ]
        ]);
    }
    public function respondLikeValidationError($message,$error){
      return $this->respond([
            'result' => [
                'error' => $error,
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }
    public function respondWithSuccess($data, $message = 'Success!'){
      return $this->respond([
        'result' => [
          'data' => $data,
          'message' => $message,
          'status' => true,
          'status_code' => $this->getStatusCode()
          ]
        ]);
    }
    public function respondWithValidationError($data, $message = 'Validation Failed!'){
       
        $errorCode= $data->getMessages();
        $messageArray = reset($errorCode);

        
            return $this->respond([
                'result' => [
                  'errors' => $data,
                  'message' => $messageArray[0],
                  'status_code' => 200
                  ]
                ]);
        
    }
      
    public function respondWithValidationErrorStatusCode($data, $message = 'Validation Failed!'){
       
        $errorCode= $data->getMessages();
        $messageArray = reset($errorCode);

        
        return $this->setStatusCode(400)->respondWithError($messageArray);
        
    }
      
    public function respondWith400StatusCode($data, $message = 'Validation Failed!'){
        
        return $this->setStatusCode(400)->respondWithError($data);
        
    }

    public function respondWithPromoCodeError($code, $message){
      return ['code' => $code, 'status' => false, 'message' => $message, 'status_code' => 200];
    }    

    public function getUrls(){
      $baseUrl = "http://".env('IMAGEURL');
      $about = $baseUrl.'/app/about-us';
      $reviews = $baseUrl.'/app/reviews';
      $contact = $baseUrl.'/app/contact-us';
      $tariff = $baseUrl.'/app/tariff';
      $faq =  $baseUrl.'/app/faqs';
      
      $data = ['about' => $about, 'review' => $reviews, 'contact' => $contact, 'tariff' => $tariff, 'faq' => $faq];
      return $data;
    }
  }
