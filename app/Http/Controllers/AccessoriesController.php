<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\Area;
use App\Models\City;
use App\Models\User;
use App\Models\AccessoriesType;
use App\Models\AccessoriesSize;
use App\Models\AccessoriesColor;
use App\Models\AccessoriesBrand;
use App\Models\AccessoriesModel;
use App\Models\AccessoriesImage;
use App\Models\Accessories;
use App\Models\Image;


class AccessoriesController extends AppController
{
    
    public function indexAccessoriesType(Request $request)
    {
        $pageTitle = "Accessories Type";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>80,"title"=>"Type"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesTypeObjects = AccessoriesType::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesTypeObjects as $accessoriesTypeObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesTypeObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesTypeObject->type,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesTypeObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesTypeObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_type_btn",
                                                            "id"=>"edit_accessories_type_id",
                                                            "data_id"=>$accessoriesTypeObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_type_btn",
                                                            "id"=>"delete_accessories_type_id",
                                                            "data_id"=>$accessoriesTypeObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_type_modal";
        $addObject['add_button_text'] = "Add Type";
        $addObject['modal_title'] = "Add Accessories Type";
        $addObject['add_url'] = "/admin/accessories-type/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Type","input_type"=>"text","name"=>"type_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_add","ckbxId1"=>"status_ckbxId1_add","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_add","ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_type_modal";
        $editObject['edit_btn_class'] = "edit_accessories_type_btn";
        $editObject['modal_title'] = "Edit Accessories Type";
        $editObject['edit_url'] = "/admin/accessories-type/edit";
        $editObject['ajax_url'] = "/admin/accessories-type/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Type","input_type"=>"text","name"=>"type_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_edit","ckbxId1"=>"status_ckbxId1_edit","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_edit","ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_type_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_type_btn";
        $deleteObject['modal_title'] = "Delete Accessories Type";
        $deleteObject['delete_url'] = "/admin/accessories-type/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Type?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesType(Request $request)
    {
            AccessoriesType::create([
                'created_by'=>Auth::user()->id,
                'type'=>$request->type_add, 
                'description'=>$request->description_add,
                'status'=>$request->status_add
            ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesType(Request $request)
    {
        $accessoriesType = AccessoriesType::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"type_edit","value"=>$accessoriesType->type],
                ["type"=>"textarea","name"=>"description_edit","value"=>$accessoriesType->description],
                ["type"=>"checkbox","status"=>$accessoriesType->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesType($id, Request $request)
    {
        AccessoriesType::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'type'=>$request->type_edit, 
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesType($id, Request $request)
    {
        $accessoriesType = AccessoriesType::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesType),"table_name"=>"accessories_type","deleted_by"=>Auth::user()->id]);
        AccessoriesType::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    public function indexAccessoriesSize(Request $request)
    {
        $pageTitle = "Accessories Size";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>80,"title"=>"Size"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesSizeObjects = AccessoriesSize::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesSizeObjects as $accessoriesSizeObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesSizeObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesSizeObject->size,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesSizeObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesSizeObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_size_btn",
                                                            "id"=>"edit_accessories_size_id",
                                                            "data_id"=>$accessoriesSizeObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_size_btn",
                                                            "id"=>"delete_accessories_size_id",
                                                            "data_id"=>$accessoriesSizeObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_size_modal";
        $addObject['add_button_text'] = "Add Size";
        $addObject['modal_title'] = "Add Accessories Size";
        $addObject['add_url'] = "/admin/accessories-size/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Size","input_type"=>"text","name"=>"size_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_add","ckbxId1"=>"status_ckbxId1_add","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_add","ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_size_modal";
        $editObject['edit_btn_class'] = "edit_accessories_size_btn";
        $editObject['modal_title'] = "Edit Accessories Size";
        $editObject['edit_url'] = "/admin/accessories-size/edit";
        $editObject['ajax_url'] = "/admin/accessories-size/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Size","input_type"=>"text","name"=>"size_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_edit","ckbxId1"=>"status_ckbxId1_edit","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_edit","ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_size_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_size_btn";
        $deleteObject['modal_title'] = "Delete Accessories Size";
        $deleteObject['delete_url'] = "/admin/accessories-size/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Size?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesSize(Request $request)
    {
            AccessoriesSize::create([
                'created_by'=>Auth::user()->id,
                'size'=>$request->size_add, 
                'description'=>$request->description_add,
                'status'=>$request->status_add
            ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesSize(Request $request)
    {
        $accessoriesSize = AccessoriesSize::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"size_edit","value"=>$accessoriesSize->size],
                ["type"=>"textarea","name"=>"description_edit","value"=>$accessoriesSize->description],
                ["type"=>"checkbox","status"=>$accessoriesSize->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesSize($id, Request $request)
    {
        AccessoriesSize::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'size'=>$request->size_edit, 
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesSize($id, Request $request)
    {
        $accessoriesSize = AccessoriesSize::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesSize),"table_name"=>"accessories_size","deleted_by"=>Auth::user()->id]);
        AccessoriesSize::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    public function indexAccessoriesColor(Request $request)
    {
        $pageTitle = "Accessories Color";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>80,"title"=>"Color"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesColorObjects = AccessoriesColor::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesColorObjects as $accessoriesColorObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesColorObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesColorObject->color,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesColorObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesColorObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_color_btn",
                                                            "id"=>"edit_accessories_color_id",
                                                            "data_id"=>$accessoriesColorObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_color_btn",
                                                            "id"=>"delete_accessories_color_id",
                                                            "data_id"=>$accessoriesColorObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_color_modal";
        $addObject['add_button_text'] = "Add Color";
        $addObject['modal_title'] = "Add Accessories Color";
        $addObject['add_url'] = "/admin/accessories-color/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Color","input_type"=>"text","name"=>"color_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_add","ckbxId1"=>"status_ckbxId1_add","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_add","ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_color_modal";
        $editObject['edit_btn_class'] = "edit_accessories_color_btn";
        $editObject['modal_title'] = "Edit Accessories Color";
        $editObject['edit_url'] = "/admin/accessories-color/edit";
        $editObject['ajax_url'] = "/admin/accessories-color/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Color","input_type"=>"text","name"=>"color_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_edit","ckbxId1"=>"status_ckbxId1_edit","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_edit","ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_color_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_color_btn";
        $deleteObject['modal_title'] = "Delete Accessories Color";
        $deleteObject['delete_url'] = "/admin/accessories-color/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Color";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesColor(Request $request)
    {
            AccessoriesColor::create([
                'created_by'=>Auth::user()->id,
                'color'=>$request->color_add, 
                'description'=>$request->description_add,
                'status'=>$request->status_add
            ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesColor(Request $request)
    {
        $accessoriesColor = AccessoriesColor::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"color_edit","value"=>$accessoriesColor->color],
                ["type"=>"textarea","name"=>"description_edit","value"=>$accessoriesColor->description],
                ["type"=>"checkbox","status"=>$accessoriesColor->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesColor($id, Request $request)
    {
        AccessoriesColor::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'color'=>$request->color_edit, 
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesColor($id, Request $request)
    {
        $accessoriesColor = AccessoriesColor::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesColor),"table_name"=>"accessories_color","deleted_by"=>Auth::user()->id]);
        AccessoriesColor::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    
    public function indexAccessoriesBrand(Request $request)
    {
        $pageTitle = "Accessories Brand";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>100,"title"=>"Brand"],
                                            ["width"=>240,"title"=>"Logo"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>120,"title"=>"Country Of Origin"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesBrandObjects = AccessoriesBrand::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesBrandObjects as $accessoriesBrandObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesBrandObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesBrandObject->brand,"pre"=>"false"]],
                        ["type"=>"image","value"=>["src"=>$accessoriesBrandObject->getLogo(),"width"=>200]],
                        ["type"=>"text","value"=>["text"=>$accessoriesBrandObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesBrandObject->country_of_origin,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesBrandObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_brand_btn",
                                                            "id"=>"edit_accessories_brand_id",
                                                            "data_id"=>$accessoriesBrandObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_brand_btn",
                                                            "id"=>"delete_accessories_brand_id",
                                                            "data_id"=>$accessoriesBrandObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_brand_modal";
        $addObject['add_button_text'] = "Add Brand";
        $addObject['modal_title'] = "Add Accessories Brand";
        $addObject['add_url'] = "/admin/accessories-brand/add";
        $addObject['add_modal_form_items'] = [];
        
        $countryList = [];
        $countryListComma = DB::table('parameter')->where('parameter_name','country_select')->first();
        $countryListCommaExploded = explode(',',$countryListComma->parameter_value);
        foreach($countryListCommaExploded as $countryListCommaExplodedEl)
        {
            array_push($countryList,["value"=>$countryListCommaExplodedEl,"text"=>$countryListCommaExplodedEl]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Brand","input_type"=>"text","name"=>"brand_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"image","label"=>"Logo Image","name"=>"logo_add"],
                ["type"=>"select","label"=>"Country of Origin","name"=>"country_of_origin_add","id"=>"country_of_origin_id_add","default_value"=>"not_set","default_text"=>"India","select_items"=>$countryList],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_add","ckbxId1"=>"status_ckbxId1_add","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_add","ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_brand_modal";
        $editObject['edit_btn_class'] = "edit_accessories_brand_btn";
        $editObject['modal_title'] = "Edit Accessories Brand";
        $editObject['edit_url'] = "/admin/accessories-brand/edit";
        $editObject['ajax_url'] = "/admin/accessories-brand/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Brand","input_type"=>"text","name"=>"brand_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"image","label"=>"Logo Image","name"=>"logo_edit","old_image_id"=>"old_logo_id_edit"],
                ["type"=>"select","label"=>"Country of Origin","name"=>"country_of_origin_edit","id"=>"country_of_origin_id_edit","default_value"=>"not_set","default_text"=>"India","select_items"=>$countryList],
                ["type"=>"checkbox","label"=>"Status","name"=>"status_edit","ckbxId1"=>"status_ckbxId1_edit","ckbxLabel1"=>"Active","ckbxId2"=>"status_ckbxId2_edit","ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_brand_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_brand_btn";
        $deleteObject['modal_title'] = "Delete Accessories Brand";
        $deleteObject['delete_url'] = "/admin/accessories-brand/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Brand";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesBrand(Request $request)
    {
        $logoImg = $request->file('logo_add');
        $logoImgId = null;
        if($logoImg)
        {
          $logoImgExt = $logoImg->guessClientExtension();
          $logoImgFn = $logoImg->getClientOriginalName();

          $logoImgHashName = str_replace(' ', '-', $request->brand_add)."-".rand(0,99).'.'.$logoImgExt;
          $logoImgDestinationPath = 'img/accessories_upload/brand/';
          $logoImg->move($logoImgDestinationPath, $logoImgHashName);

          $logoImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$logoImgDestinationPath.$logoImgHashName]);

          $logoImgId = $logoImgObj['id'];
        }
        
        AccessoriesBrand::create([
            'created_by'=>Auth::user()->id,
            'brand'=>$request->brand_add,
            'logo'=>$logoImgId,
            'country_of_origin'=>$request->country_of_origin_add,
            'description'=>$request->description_add,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesBrand(Request $request)
    {
        $accessoriesBrand = AccessoriesBrand::where('id',$request->id)->first();
        $logo_html = "<img src=\"".$accessoriesBrand->getLogo()."\" width=\"90px\">";        
        $data = [
                ["type"=>"input","name"=>"brand_edit","value"=>$accessoriesBrand->brand],
                ["type"=>"textarea","name"=>"description_edit","value"=>$accessoriesBrand->description],
                ["type"=>"image","imgId"=>"old_logo_id_edit","html"=>$logo_html],
                ["type"=>"select","name"=>"country_of_origin_edit","value"=>$accessoriesBrand->country_of_origin],
                ["type"=>"checkbox","status"=>$accessoriesBrand->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesBrand($id, Request $request)
    {
        
        $accessoriesBrand = AccessoriesBrand::where('id',$id)->first();
        if(!$accessoriesBrand)
            return redirect()->back();
        
        $oldLogoImgId = $accessoriesBrand->logo;
        $logoImg = $request->file('logo_edit');

        if($logoImg)
        {
            $logoImgExt = $logoImg->guessClientExtension();
            $logoImgFn = $logoImg->getClientOriginalName();

            $logoImgHashName = str_replace(' ', '-', $request->brand_edit)."-".rand(0,99).'.'.$logoImgExt;
            $logoImgDestinationPath = 'img/accessories_upload/brand/';

            if(file_exists(public_path($logoImgDestinationPath.$logoImgHashName)))
            {
               unlink(public_path($logoImgDestinationPath.$logoImgHashName));
            }

            $logoImg->move($logoImgDestinationPath, $logoImgHashName);

            $logoImgObj = Image::create(['full' => '//'.env('IMAGEURL').'/'.$logoImgDestinationPath.$logoImgHashName]);

            $logoImgId = $logoImgObj['id'];
        }
        else
        {
            if(isset($oldLogoImgId))
              $logoImgId = $oldLogoImgId;
            else
              $logoImgId = null;
        }
        
        AccessoriesBrand::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'brand'=>$request->brand_edit,
            'logo'=>$logoImgId,
            'country_of_origin'=>$request->country_of_origin_edit,
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesBrand($id, Request $request)
    {
        $accessoriesBrand = AccessoriesBrand::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesBrand),"table_name"=>"accessories_brand","deleted_by"=>Auth::user()->id]);
        AccessoriesBrand::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    public function indexAccessoriesModel(Request $request)
    {
        $pageTitle = "Accessories Model";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>100,"title"=>"Model"],
                                            ["width"=>200,"title"=>"Description"],
                                            ["width"=>120,"title"=>"Type"],
                                            ["width"=>80,"title"=>"Brand"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesModelObjects = AccessoriesModel::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesModelObjects as $accessoriesModelObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesModelObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesModelObject->model,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesModelObject->description,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesModelObject->getType(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesModelObject->getBrand(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesModelObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_model_btn",
                                                            "id"=>"edit_accessories_model_id",
                                                            "data_id"=>$accessoriesModelObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_model_btn",
                                                            "id"=>"delete_accessories_model_id",
                                                            "data_id"=>$accessoriesModelObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_model_modal";
        $addObject['add_button_text'] = "Add Model";
        $addObject['modal_title'] = "Add Accessories Model";
        $addObject['add_url'] = "/admin/accessories-model/add";
        $addObject['add_modal_form_items'] = [];
        
        $typeList = [];
        $typeObjects = AccessoriesType::where('status',1)->get();
        foreach($typeObjects as $typeObject)
        {
            array_push($typeList,["value"=>$typeObject->id,"text"=>$typeObject->type]);
        }
        
        $brandList = [];
        $brandObjects = AccessoriesBrand::where('status',1)->get();
        foreach($brandObjects as $brandObject)
        {
            array_push($brandList,["value"=>$brandObject->id,"text"=>$brandObject->brand]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Model","input_type"=>"text","name"=>"model_add"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_add"],
                ["type"=>"select", "label"=>"Type", "name"=>"type_add", "id"=>"type_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$typeList], 
                ["type"=>"select","label"=>"Brand", "name"=>"brand_add", "id"=>"brand_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$brandList],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_model_modal";
        $editObject['edit_btn_class'] = "edit_accessories_model_btn";
        $editObject['modal_title'] = "Edit Accessories Model";
        $editObject['edit_url'] = "/admin/accessories-model/edit";
        $editObject['ajax_url'] = "/admin/accessories-model/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory Model","input_type"=>"text","name"=>"model_edit"],
                ["type"=>"textarea","label"=>"Description","name"=>"description_edit"],
                ["type"=>"select", "label"=>"Type", "name"=>"type_edit", "id"=>"type_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$typeList], 
                ["type"=>"select","label"=>"Brand", "name"=>"brand_edit", "id"=>"brand_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Type --", "select_items"=>$brandList],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_model_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_model_btn";
        $deleteObject['modal_title'] = "Delete Accessories Model";
        $deleteObject['delete_url'] = "/admin/accessories-model/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Model";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesModel(Request $request)
    {
        
        AccessoriesModel::create([
            'created_by'=>Auth::user()->id,
            'model'=>$request->model_add,
            'type'=>$request->type_add,
            'brand'=>$request->brand_add,
            'description'=>$request->description_add,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesModel(Request $request)
    {
        $accessoriesModel = AccessoriesModel::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"model_edit","value"=>$accessoriesModel->model],
                ["type"=>"textarea","name"=>"description_edit","value"=>$accessoriesModel->description],
                ["type"=>"select","name"=>"type_edit","value"=>$accessoriesModel->type],
                ["type"=>"select","name"=>"brand_edit","value"=>$accessoriesModel->brand],
                ["type"=>"checkbox","status"=>$accessoriesModel->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesModel($id, Request $request)
    {
        
        
        AccessoriesModel::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'model'=>$request->model_edit,
            'type'=>$request->type_edit,
            'brand'=>$request->brand_edit,
            'description'=>$request->description_edit,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesModel($id, Request $request)
    {
        $accessoriesModel = AccessoriesModel::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesModel),"table_name"=>"accessories_model","deleted_by"=>Auth::user()->id]);
        AccessoriesModel::where('id',$id)->delete();
        return redirect()->back();
    }
    
    public function indexAccessoriesImage(Request $request)
    {
        $pageTitle = "Accessories Image";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Model"],
                                            ["width"=>80,"title"=>"Color"],
                                            ["width"=>240,"title"=>"Image"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesImageObjects = AccessoriesImage::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesImageObjects as $accessoriesImageObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesImageObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesImageObject->getModel(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesImageObject->getColor(),"pre"=>"false"]],
                        ["type"=>"image","value"=>["src"=>$accessoriesImageObject->full,"width"=>200]],
                        ["type"=>"text","value"=>["text"=>($accessoriesImageObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_image_btn",
                                                            "id"=>"edit_accessories_image_id",
                                                            "data_id"=>$accessoriesImageObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_image_btn",
                                                            "id"=>"delete_accessories_image_id",
                                                            "data_id"=>$accessoriesImageObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_image_modal";
        $addObject['add_button_text'] = "Add Image";
        $addObject['modal_title'] = "Add Accessories Image";
        $addObject['add_url'] = "/admin/accessories-image/add";
        $addObject['add_modal_form_items'] = [];
        
        $modelList = [];
        $modelObjects = AccessoriesModel::where('status',1)->get();
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->model]);
        }
        
        $colorList = [];
        $colorObjects = AccessoriesColor::where('status',1)->get();
        foreach($colorObjects as $colorObject)
        {
            array_push($colorList,["value"=>$colorObject->id,"text"=>$colorObject->color]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"select", "label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList], 
                ["type"=>"select","label"=>"Color", "name"=>"color_add", "id"=>"color_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Color --", "select_items"=>$colorList],
                ["type"=>"image","label"=>"Image","name"=>"full_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_image_modal";
        $editObject['edit_btn_class'] = "edit_accessories_image_btn";
        $editObject['modal_title'] = "Edit Accessories Image";
        $editObject['edit_url'] = "/admin/accessories-image/edit";
        $editObject['ajax_url'] = "/admin/accessories-image/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"select", "label"=>"Model", "name"=>"model_edit", "id"=>"model_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select model --", "select_items"=>$modelList], 
                ["type"=>"select","label"=>"Color", "name"=>"color_edit", "id"=>"color_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Color --", "select_items"=>$colorList],
                ["type"=>"image","label"=>"Image","name"=>"full_edit","old_image_id"=>"old_full_id_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_image_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_image_btn";
        $deleteObject['modal_title'] = "Delete Accessories Image";
        $deleteObject['delete_url'] = "/admin/accessories-image/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories Image";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessoriesImage(Request $request)
    {
        $fullImg = $request->file('full_add');
        $fullImgId = null;
        if($fullImg)
        {
          $fullImgExt = $fullImg->guessClientExtension();
          $fullImgFn = $fullImg->getClientOriginalName();

          $fullImgHashName = str_replace(' ', '-', $request->image_add)."-".rand(0,99).'.'.$fullImgExt;
          $fullImgDestinationPath = 'img/accessories_upload/image/';
          $fullImg->move($fullImgDestinationPath, $fullImgHashName);

          $fullImgUrl =  '//'.env('IMAGEURL').'/'.$fullImgDestinationPath.$fullImgHashName;

        }
        else
        {
            $fullImgUrl = "";
        }
        
        AccessoriesImage::create([
            'created_by'=>Auth::user()->id,
            'model'=>$request->model_add,
            'color'=>$request->color_add,
            'full'=> $fullImgUrl,
            'status'=>$request->status_add
        ]);
        
        return redirect()->back();
    }
    
    public function detailsAccessoriesImage(Request $request)
    {
        $accessoriesImage = AccessoriesImage::where('id',$request->id)->first();
        $img_html = "<img src=\"".$accessoriesImage->full."\" width=\"90px\">";
        $data = [
                ["type"=>"select","name"=>"model_edit","value"=>$accessoriesImage->model],
                ["type"=>"select","name"=>"color_edit","value"=>$accessoriesImage->color],
                ["type"=>"image","imgId"=>"old_full_id_edit","html"=>$img_html],
                ["type"=>"checkbox","status"=>$accessoriesImage->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessoriesImage($id, Request $request)
    {
        $accessoriesImage = AccessoriesImage::where('id',$id)->first();
        if(!$accessoriesImage)
            return redirect()->back();
        
        $oldFullImgUrl = $accessoriesImage->full;
        $fullImg = $request->file('full_edit');

        if($fullImg)
        {
            $fullImgExt = $fullImg->guessClientExtension();
            $fullImgFn = $fullImg->getClientOriginalName();

            $fullImgHashName = str_replace(' ', '-', $request->image_edit)."-".rand(0,99).'.'.$fullImgExt;
            $fullImgDestinationPath = 'img/accessories_upload/image/';

            if(file_exists(public_path($fullImgDestinationPath.$fullImgHashName)))
            {
               unlink(public_path($fullImgDestinationPath.$fullImgHashName));
            }

            $fullImg->move($fullImgDestinationPath, $fullImgHashName);

            $fullImgUrl = '//'.env('IMAGEURL').'/'.$fullImgDestinationPath.$fullImgHashName;

        }
        else
        {
            if(isset($oldFullImgUrl))
              $fullImgUrl = $oldFullImgUrl;
            else
              $fullImgUrl = "";
        }
        
        AccessoriesImage::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'model'=>$request->model_edit,
            'color'=>$request->color_edit,
            'full'=>$fullImgUrl,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessoriesImage($id, Request $request)
    {
        $accessoriesImage = AccessoriesImage::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessoriesImage),"table_name"=>"accessories_image","deleted_by"=>Auth::user()->id]);
        AccessoriesImage::where('id',$id)->delete();
        return redirect()->back();
    }
    
    public function indexAccessories(Request $request)
    {
        $pageTitle = "Accessories ";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Type"],
                                            ["width"=>120,"title"=>"Brand"],
                                            ["width"=>100,"title"=>"Model"],
                                            ["width"=>120,"title"=>"Color"],
                                            ["width"=>80,"title"=>"Size"],
                                            ["width"=>120,"title"=>"Current Area"],
                                            ["width"=>120,"title"=>"Owner"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $accessoriesObjects = Accessories::all();
        
        $objectTableArray['body'] = [];
        
        foreach($accessoriesObjects as $accessoriesObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->text_id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getType(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getBrand(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getModel(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getColor(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getSize(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getArea(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$accessoriesObject->getOwner(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($accessoriesObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_accessories_btn",
                                                            "id"=>"edit_accessories_id",
                                                            "data_id"=>$accessoriesObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_accessories_btn",
                                                            "id"=>"delete_accessories_id",
                                                            "data_id"=>$accessoriesObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_accessories_modal";
        $addObject['add_button_text'] = "Add";
        $addObject['modal_title'] = "Add Accessories";
        $addObject['add_url'] = "/admin/accessories/add";
        $addObject['add_modal_form_items'] = [];
        
        $modelList = [];
        $modelObjects = AccessoriesModel::where('status',1)->get();
        
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->model]);
        }
        
        $sizeList = [];
        $sizeObjects = AccessoriesSize::where('status',1)->get();
        
        foreach($sizeObjects as $sizeObject)
        {
            array_push($sizeList,["value"=>$sizeObject->id,"text"=>$sizeObject->size]);
        }
        
        $colorList = [];
        $colorObjects = AccessoriesColor::where('status',1)->get();
        
        foreach($colorObjects as $colorObject)
        {
            array_push($colorList,["value"=>$colorObject->id,"text"=>$colorObject->color]);
        }
        
        $areaList = [];
        $areaObjects = Area::where('status',1)->get();
        $cityObjects = City::where('status',1)->get();
        
        foreach($areaObjects as $areaObject)
        {
            array_push($areaList,["value"=>$areaObject->id,"text"=>$areaObject->area]);
        }
        
        foreach($cityObjects as $cityObject)
        {
            array_push($areaList,["value"=>($cityObject->id+1000000),"text"=>$cityObject->city]);
        }
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory ID","input_type"=>"text","name"=>"text_id_add"],
                ["type"=>"select","label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"select","label"=>"Size", "name"=>"size_add", "id"=>"size_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Size --", "select_items"=>$sizeList],
                ["type"=>"select","label"=>"Color", "name"=>"color_add", "id"=>"color_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Color --", "select_items"=>$colorList],
                ["type"=>"select","label"=>"Current Area", "name"=>"area_add", "id"=>"area_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Area --", "select_items"=>$areaList],
                ["type"=>"input","label"=>"Owner Email","input_type"=>"text","name"=>"owner_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_accessories_modal";
        $editObject['edit_btn_class'] = "edit_accessories_btn";
        $editObject['modal_title'] = "Edit Accessories";
        $editObject['edit_url'] = "/admin/accessories/edit";
        $editObject['ajax_url'] = "/admin/accessories/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Accessory ID","input_type"=>"text","name"=>"text_id_edit"],
                ["type"=>"select","label"=>"Model", "name"=>"model_edit", "id"=>"model_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"select","label"=>"Size", "name"=>"size_edit", "id"=>"size_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Size --", "select_items"=>$sizeList],
                ["type"=>"select","label"=>"Color", "name"=>"color_edit", "id"=>"color_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Color --", "select_items"=>$colorList],
                ["type"=>"select","label"=>"Current Area", "name"=>"area_edit", "id"=>"area_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Area --", "select_items"=>$areaList],
                ["type"=>"input","label"=>"Owner Email","input_type"=>"text","name"=>"owner_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_accessories_modal";
        $deleteObject['delete_btn_class'] = "delete_accessories_btn";
        $deleteObject['modal_title'] = "Delete Accessories";
        $deleteObject['delete_url'] = "/admin/accessories/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Accessories";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addAccessories(Request $request)
    {
        
        $modelObject = AccessoriesModel::where('id',$request->model_add)->first();
        $ownerObject = User::where('email',$request->owner_add)->first();
        
        if(!$ownerObject)
        {
            return redirect()->back()->withErrors(['Please enter a valid email id for owner']);
        }
        
        Accessories::create([
            'created_by'=>Auth::user()->id,
            'text_id'=>$request->text_id_add,
            'model'=>$request->model_add,
            'type'=>$modelObject->type,
            'brand'=>$modelObject->brand,
            'color'=>$request->color_add,
            'size'=>$request->size_add,
            'area'=>$request->area_add,
            'owner'=>$ownerObject->id,
            'status'=>$request->status_add
        ]);
        
        if($request->status_add == 1)
            AccessoriesAvailabilityController::addOrDeleteAccessories($request->area_add,$request->model_add,$request->size_add,"add",0,0);
        
        return redirect()->back();
    }
    
    public function detailsAccessories(Request $request)
    {
        $accessories = Accessories::where('id',$request->id)->first();
        
        $data = [
                ["type"=>"input","name"=>"text_id_edit","value"=>$accessories->text_id],
                ["type"=>"select","name"=>"model_edit","value"=>$accessories->model],
                ["type"=>"select","name"=>"size_edit","value"=>$accessories->size],
                ["type"=>"select","name"=>"color_edit","value"=>$accessories->color],
                ["type"=>"select","name"=>"area_edit","value"=>$accessories->area],
                ["type"=>"input","name"=>"owner_edit","value"=>$accessories->getOwner()],
                ["type"=>"checkbox","status"=>$accessories->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editAccessories($id, Request $request)
    {
        
        
        $modelObject = AccessoriesModel::where('id',$request->model_edit)->first();
        $ownerObject = User::where('email',$request->owner_edit)->first();
        
        if(!$ownerObject)
        {
            return redirect()->back()->withErrors(['Please enter a valid email id for owner']);
        }
        
        $accessories = Accessories::where('id',$request->id)->first();
        
        if($accessories->status!=$request->status_edit)
        {
            if($request->status_edit==1)
            {
                AccessoriesAvailabilityController::addOrDeleteAccessories($request->area_edit,$request->model_edit,$request->size_edit,"add",0,0);
            }
            else
            {
                AccessoriesAvailabilityController::addOrDeleteAccessories($request->area_edit,$request->model_edit,$request->size_edit,"delete",0,0);

            }
        }
        
        Accessories::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'text_id'=>$request->text_id_edit,
            'model'=>$request->model_edit,
            'type'=>$modelObject->type,
            'brand'=>$modelObject->brand,
            'color'=>$request->color_edit,
            'size'=>$request->size_edit,
            'area'=>$request->area_edit,
            'owner'=>$ownerObject->id,
            'status'=>$request->status_edit
        ]);
        
        return redirect()->back();
    }
    
    public function deleteAccessories($id, Request $request)
    {
        $accessories = Accessories::where('id',$id)->first();
        
        if($accessories->status==1)
        {
            AccessoriesAvailabilityController::addOrDeleteAccessories($accessories->area,$accessories->model,$accessories->size,"delete",0,0);
        }
        
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($accessories),"table_name"=>"accessories","deleted_by"=>Auth::user()->id]);
        Accessories::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    
}
