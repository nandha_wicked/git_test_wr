<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use Session;
use DateTime;
use App\Models\Bikation_Reviews;


class Bikation_ReviewsController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
    if(Session::has('email') && Session::has('First_Name')){
	$bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Vendor_ID',Session::get('Vendor_ID'))->get();
	   $selected="";
      if (isset($request->btn_submit)) {
         $bikation_reviewsList = Bikation_Reviews::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
       /* $bikation_reviewsList = DB::table('bikation_reviews')
            ->leftJoin('bikation_trip', 'bikation_trip.Trip_ID', '=', 'bikation_reviews.Trip_ID')
            ->select('bikation_trip.Trip_ID','bikation_trip.Vendor_ID','bikation_reviews.id', 'bikation_reviews.Trip_ID', 'bikation_reviews.Customer_ID','bikation_reviews.Overall','bikation_reviews.Planning','bikation_reviews.On_Schedule','bikation_reviews.Food','bikation_reviews.Stay','bikation_reviews.Activities','bikation_reviews.Remarks','bikation_reviews.Status')
            ->where('bikation_trip.Vendor_ID', '=', Session::get('Vendor_ID'))
            ->get();*/
		$bikation_reviewsList = Bikation_Reviews::where('Vendor_ID',Session::get('Vendor_ID'))->get();
        $selected="";
      }	
  $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
 // $bikation_reviewsList = Bikation_Reviews::all();
      foreach ($bikation_reviewsList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }  
    return view('bikationvendor.front.reviews_index',compact('bikation_reviewsList','bikation_tripList','bikation_AlltripList','selected'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }

  }
  public function activeReviews($id){
    if(Session::has('email') && Session::has('First_Name'))
      {
        $status= DB::table ('bikation_reviews')->select('Status')->where('id',$id)->first();
      
      if($status->Status == 1 ){
        $value=0;  
        Bikation_Reviews::where('id', $id)->update(['Status' => $value]);
      }
      if($status->Status == 0 ){
        $value=1;  
        Bikation_Reviews::where('id', $id)->update([ 'Status' => $value]);
      }
      return redirect('bikation-reviews');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  
  
    
}
