<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use DateTime;
use App\Models\Bikation_Reviews;
use App\Models\BikationVendor;
use App\Models\Bikation_Trip;
use App\Models\Bikation_Package;
use App\Models\Bikation_Address;
use App\Models\Bikation_Bookings;
use App\Models\Bikation_Itinerary;
use App\Models\User;
use Session;
use Auth;
use Hash;
use Mail;
use Response;
use Razorpay\Api\Api;

class Bikation_PackagesdashboardController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){

    $packagesMakeList = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_reviews', 'bikation_reviews.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Trip_url','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikation_reviews.Overall','bikationvendor_users.first_name','bikationvendor_users.last_name')
            ->where('bikation_trip.Status', '=', 1)
            ->where('bikation_trip.Trip_Start_Date', '>=', date("Y-m-d"))
			->where('bikation_trip.Approve_By_Admin', '=', 1)
            ->groupby('Trip_ID')->orderBy('Trip_Start_Date','asc')
            ->get();

			 $trip_banner = DB::table ('bikation_banner')->select('banner_title','image')->where('status', 1)->first();  
            $trip_banner_mobile = DB::table ('bikation_banner')->select('banner_title','image')->where('status', 1)->where('mobile_image',1)->first(); 
			 
						
        foreach ($packagesMakeList as $model) {
        $Total_reviews = DB::table('bikation_reviews')->select('id','Trip_ID')->where('Trip_ID',$model->Trip_ID)->where('Status', '=', 1)->get();
        $model->Total_reviews= count($Total_reviews);
      }
      $pastpackages = DB::table('bikation_trip')
      ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
      ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
      ->leftJoin('bikation_reviews', 'bikation_reviews.Trip_ID', '=', 'bikation_trip.Trip_ID')
      ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
      ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
      ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Trip_url','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikation_reviews.Overall','bikationvendor_users.first_name','bikationvendor_users.last_name')
      ->where('bikation_trip.Status', '=', 1)
      ->where('bikation_trip.Trip_Start_Date', '<', date("Y-m-d"))
      ->where('bikation_trip.Approve_By_Admin', '=', 1)
      ->groupby('Trip_ID')->orderBy('Trip_Start_Date','desc')
      ->limit(12)
      ->get();
      foreach ($pastpackages as $pasttrip) {
        $Total_reviews = DB::table('bikation_reviews')->select('id','Trip_ID')->where('Trip_ID',$pasttrip->Trip_ID)->where('Status', '=', 1)->get();
        $pasttrip->Total_reviews= count($Total_reviews);
    }

     // echo "<pre>";print_r($packagesMakeList);exit;
    return view('bikationvendor.packages.packages',compact('packagesMakeList','trip_banner','trip_banner_mobile','pastpackages'));
  }

  public function detail($url){

    $Trip_url = DB::table('bikation_trip')->select('Trip_ID','Vendor_ID')->where('Trip_url', $url)->first();
    $Trip_ID = $Trip_url->Trip_ID;
	$Vendor_ID = $Trip_url->Vendor_ID;
	
    $packageDetail = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
			->leftJoin('bikation_address', 'bikation_address.Vendor_ID', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Start_Location', 'bikation_trip.End_Location', 'bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Preffered_Bikes','bikation_trip.Trip_url','bikation_trip.No_of_tickets_available','bikation_trip.Trip_Description','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikationvendor_users.first_name','bikationvendor_users.last_name','bikation_address.about_me')
            ->where('bikation_trip.Status', '=', 1)
            ->where('bikation_trip.Approve_By_Admin', '=', 1)
            ->where('bikation_trip.Trip_ID', '=', $Trip_ID)
            ->first();
   
    $ride_include_all = DB::table('bikation_addon')->select('Activity_ID')
            ->where('Exclusion_Inclusion', '=', 1)
            ->where('Trip_ID', '=', $Trip_ID)
            ->get();
  /*  foreach ($ride_include as $model) {
     
      $ride_include['Activity_ID'] =  $model->Activity_ID;
    }*/

     $ride_exclude_all = DB::table('bikation_addon')->select('Activity_ID')
            ->where('Exclusion_Inclusion', '=', 0)
            ->where('Trip_ID', '=', $Trip_ID)
            ->get();
		//print_r($ride_include_all);exit;
          /*  foreach ($ride_exclude as $model) {
     
      $ride_exclude['Activity_ID'] =  $model->Activity_ID;
    }*/
    $bikation_itinerary_all = Bikation_Itinerary::where('Trip_ID', $Trip_ID)->where('Status',1)->get();
	

    $reviews = Bikation_Reviews::where('Trip_ID', $Trip_ID)->where('Status',1)->orderBy('created_at','Remarks')->paginate(3);

    foreach ($reviews as $model) {
      $username = BikationVendor::where('id', $model->Customer_ID)->first();
      $model['first_name'] = ($username) ? $username['first_name'] : '';
      $model['last_name'] = ($username) ? $username['last_name'] : '';
    }
	
	$total_vendor_rating = DB::table ('bikation_reviews')
                ->select(DB::raw ('AVG(Overall) as average_Overall'))
                ->where('Vendor_ID', $Vendor_ID)->first();
	 $total_no_vendor_rating = DB::table('bikation_reviews')
                ->select ('id', DB::raw ('AVG(Overall) as average_Overall'))
                ->where('Vendor_ID', $Vendor_ID)->count();
				
    $total_rating = DB::table ('bikation_reviews')
                ->select(DB::raw ('AVG(Overall) as average_Overall'))
                ->where('Trip_ID', $Trip_ID)->where('Status', '=', 1)->first();
    $total_no_rating = DB::table('bikation_reviews')
                ->select ('id', DB::raw ('AVG(Overall) as average_Overall'))
                ->where('Trip_ID', $Trip_ID)->where('Status', '=', 1)->count();
    $trip_phtos = DB::table ('bikation_trip_photo')
                  ->where('Trip_ID', $Trip_ID)
                        ->where('Status', 1)
                        ->get();            
    
    $trip_banner = DB::table ('bikation_banner')->select('banner_title','image')->where('status', 1)->where('mobile_image',0)->first(); 
    $trip_banner_mobile = DB::table ('bikation_banner')->select('banner_title','image')->where('status', 1)->where('mobile_image',1)->first(); 
    
	 
    return view('bikationvendor.packages.detail',compact('packageDetail','reviews','total_rating','total_vendor_rating','total_no_vendor_rating','total_no_rating','ride_include','ride_exclude','trip_phtos','bikation_itinerary_all','ride_include_all','ride_exclude_all','trip_banner','trip_banner_mobile'));
  }

public function addreviews(Request $request){

   $user_id=Auth::user()->id;
   
   Bikation_Reviews::create(['Trip_ID' => $request->Trip_ID,'Customer_ID' => $user_id, 'Overall' => $request->Overall,'Remarks'=>$request->Remarks,'Status'=>0]);
   
    return redirect('trip/'.$request->Trip_url);
  }
  public function trip_signup(Request $request){

        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email'    => 'required|unique:users,email', // make sure the 
            'mobile_num' => 'required|numeric',
            'password' => 'required|alphaNum|min:5', // password can only 
            'confirmPassword' => 'required|alphaNum|min:5'
        );

        $validator = Validator::make($request->all(), $rules);
        $password1 = $request->password;
        $password2 = $request->confirmPassword;
        $fname = $request->first_name;
        $lname = $request->last_name;
        $email = $request->email;
        $mob=$request->mobile_num;

        if ($validator->fails()) { 
            $status = false;
        } 
        else {
            if($password1 != $password2){ 
                $status = false;
                $validator->errors()->add('Error', 'Confirm Password Mismatch!');
            }
            else if( strlen($mob) != 10 ){
                $status = false;
                $validator->errors()->add('Error', 'Mobile number must be 10 digit!');
            }
            else{
                $status = true;
                $length = 5;
                $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
                
                User::create(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email,'mobile_num' => $request->mobile_num, 'password' => Hash::make($request->password), 'referral_code'=>$referral_code]);

                $data = ['fname'=>$fname, 'lname'=>$lname, 'email'=>$email, 'mob'=>$mob, 'password'=>$password1];
                Mail::send('emails.bikation_user_registration', $data, function ($message) use ($email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($email)->subject('WickedRide Bikation User SignUp');

                });
					$adm_email = env('BIKATION_ADMIN_EMAIL');
				Mail::send('emails.bikation_admin_user_registration', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('WickedRide Bikation User SignUp');

                });
               /* Mail::send('emails.bikation_user_registration',$data,function($message){
                    $message->to(env('ADMIN_EMAIL'));
                    $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                    $message->subject('WickedRide User SignUp');
                });*/


                $userdata = array(
                    'email'     => $request->email,
                    'password'  => $request->password
                );          

                // attempt to do the login
                Auth::attempt($userdata);        
            }  
        }

        return Response::json(array(
            'success' => $status,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }

    public function post_triplogin(Request $request){
        $rules = array(
            'email'    => 'required|email', 
            'password' => 'required|min:3' 
        );
       
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) { 
            $status = false;
        } else {
            // create our user data for the authentication
            $userdata = array(
                'email'     => $request->email,
                'password'  => $request->password
            );          

            // attempt to do the login
            if (Auth::attempt($userdata)) 
            {               
                $status = true;
            } else {        
                // validation not successful, send back to form 
                $status = false;
                $validator->errors()->add('Error', 'Invalid username or password!');
            }
        }
        return Response::json(array(
            'success' => $status,
            'Trip_url' => $request->Trip_url,
            'errors' => $validator->getMessageBag()->toArray()
        ));
      }

  public function price_calculation(Request $request){
        $rules = array();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
        
        $cost= Bikation_Package::where('Trip_ID',$request->Trip_ID)->first();
        $total_cost_1px = $request->No_of_Tickets_Cost_1px * $cost->Cost_1px ;
        $total_cost_2px = $request->No_of_Tickets_Cost_2px * $cost->Cost_2px ;
        $total_cost = $total_cost_1px + $total_cost_2px;
        if($request->rent_bike=='No') {
            $total_no_of_tickets = $request->No_of_Tickets_Cost_1px+$request->No_of_Tickets_Cost_2px;
            $totalbikerentalamount = $total_no_of_tickets*$cost->rental_bike_cost;
            if($total_cost>$totalbikerentalamount) {
                $total_cost = $total_cost - $totalbikerentalamount;
            }
        }
        }
        return Response::json(array(
            'success' => $status,
            'total_cost' => $total_cost,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }
   /* public function add_no_seats(Request $request){
        $rules = array('No_of_People_1px' => 'required');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
        $Payment_status=0;
        $total_no_of_booktickits=  $request->No_of_People_1px + $request->No_of_People_2px ;
        $packages_seats= DB::table('bikation_trip')->select('No_of_tickets_available')->where('Trip_ID',$request->Trip_ID)->first();
        $available = $packages_seats->No_of_tickets_available - $total_no_of_booktickits;
        Bikation_Trip::where('Trip_ID',$request->Trip_ID)->update(['No_of_tickets_available' => $available]);
        $user_id=Auth::user()->id;
        $Booked_On = new DateTime();
        $BookedOn= $Booked_On->format('Y-m-d H:i:s');
        $Booked_By=Auth::user()->id;
        $inseart= Bikation_Bookings::create(['Trip_ID' => $request->Trip_ID,'Customer_ID' => $user_id, 'Package_ID' => $request->Trip_ID,'Booked_On' => $BookedOn,'No_of_People'=>$total_no_of_booktickits,'Package_Cost' => $request->Package_Cost,'Booked_Type'=>$request->Booked_Type,'Booked_Type'=>$request->Booked_Type,'Booked_By' => $Booked_By,'Payment_status'=>$Payment_status]);
        $Booking_ID=$inseart->id;
        $Package_Cost=DB::table('bikation_bookings')->select('Package_Cost','Trip_ID')->where('Booking_ID', $Booking_ID)->first();
        $user= DB::table('users')->select('email','first_name','last_name','mobile_num')->where('id', $user_id)->first();
        $Package_Cost_1=$Package_Cost->Package_Cost ;
        $Trip_ID=$Package_Cost->Trip_ID;
        $email=$user->email;
        $first_name=$user->first_name;
        $last_name=$user->last_name;
        $mobile_num=$user->mobile_num;
        Session::put('price_payment',$Package_Cost_1);
        $Trip= DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$Trip_ID)->first();
        }
        return Response::json(array(
            'success' => $status,
            'Trip_url' => $request->Trip_url,
            'Booking_ID' => $Booking_ID,
            'Package_Cost' => $Package_Cost_1,
            'Trip_ID' => $Trip_ID,
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'mobile_num' => $mobile_num,
            'Trip_name' => $Trip->Trip_Name,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }*/
      public function add_no_seats(Request $request){
        $rules = array('Total_Cost' => 'required');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            return Response::json(array(
            'success' => 'false',
            'errors' => $validator->getMessageBag()->toArray()
				));
        } 
        else {	
		
		$cost= Bikation_Package::where('Trip_ID',$request->Trip_ID)->first();
        $total_cost_1px = $request->No_of_Tickets_Cost_1px * $cost->Cost_1px ;
        $total_cost_2px = $request->No_of_Tickets_Cost_2px * $cost->Cost_2px ;
        $total_cost =  $total_cost_1px + $total_cost_2px;
        if($request->Vehicle=='No') {
            $total_no_of_tickets = $request->No_of_Tickets_Cost_1px+$request->No_of_Tickets_Cost_2px;
            $totalbikerentalamount = $total_no_of_tickets*$cost->rental_bike_cost;
            if($total_cost>$totalbikerentalamount) {
                $total_cost = $total_cost - $totalbikerentalamount;
            }
        }
		$Trip_check = DB::table('bikation_trip')->select('Trip_url')->where('Trip_ID',$request->Trip_ID)->first();
			if($total_cost == $request->Total_Cost && $Trip_check->Trip_url == $request->Trip_url_get)
			{
				$status = 'true';	
			
        
        $Payment_status=0;
        $book_No_of_Tickets_Cost_1px = $request->No_of_Tickets_Cost_1px;
        $book_No_of_Tickets_Cost_2px = $request->No_of_Tickets_Cost_2px;
        $total_no_of_booktickits =  $request->No_of_Tickets_Cost_1px + $request->No_of_Tickets_Cost_2px ;
        $packages_seats= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();
        $available_Tickes_Remaining_Cost_1px = $packages_seats->Tickes_Remaining_Cost_1px - $book_No_of_Tickets_Cost_1px;
        $available_Tickes_Remaining_Cost_2px = $packages_seats->Tickes_Remaining_Cost_2px - $book_No_of_Tickets_Cost_2px;
        Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $available_Tickes_Remaining_Cost_1px,'Tickes_Remaining_Cost_2px'=>$available_Tickes_Remaining_Cost_2px]);
        $user_id=Auth::user()->id;
        $Booked_On = new DateTime();
        $BookedOn= $Booked_On->format('Y-m-d H:i:s');
        $Booked_By=Auth::user()->id;
        $inseart= Bikation_Bookings::create(['Trip_ID' => $request->Trip_ID,'Customer_ID' => $user_id,'Package_ID' => $request->Trip_ID,'Booked_On' => $BookedOn,'No_of_People'=>$total_no_of_booktickits,'No_of_Tickets_Cost_1px'=>$request->No_of_Tickets_Cost_1px,'No_of_Tickets_Cost_2px'=>$request->No_of_Tickets_Cost_2px,'Package_Cost' => $request->Package_Cost,'Addon_cost' => $request->Addon_cost,'Total_Cost' => $request->Total_Cost,'Booked_Type'=>$request->Booked_Type,'Booked_By' => $Booked_By,'Payment_status'=>$Payment_status,'Vehicle'=>$request->Vehicle]);
        $Booking_ID=$inseart->id;
        $Total_Cost=DB::table('bikation_bookings')->select('Total_Cost','Trip_ID')->where('Booking_ID', $Booking_ID)->first();
        $user= DB::table('users')->select('email','first_name','last_name','mobile_num')->where('id', $user_id)->first();
        $Total_price=$Total_Cost->Total_Cost;
        $Trip_ID=$Total_Cost->Trip_ID;
        $email=$user->email;
        $first_name=$user->first_name;
        $last_name=$user->last_name;
        $mobile_num=$user->mobile_num;
        Session::put('price_payment',$Total_price);
        $Trip= DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$Trip_ID)->first();
		}
		else
		{
			 return Response::json(array(
            'success' => 'false',
            'errors' => $validator->getMessageBag()->toArray()
				));
		}
		
        }
		 return Response::json(array(
            'success' => $status,
            'Trip_url' => $request->Trip_url,
            'Booking_ID' => $Booking_ID,
            'Total_Cost' => $Total_price,
            'Trip_ID' => $Trip_ID,
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'mobile_num' => $mobile_num,
            'Trip_name' => $Trip->Trip_Name,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }
     public function update_payment_demo_post(Request $request){

        $price = Session::get('price_payment');

        $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));
        $id = $_POST['razorpay_payment_id'];
        $payment = $api->payment->fetch($id)->capture(array('amount'=>$price*100));
       //echo "<pre>";print_r($payment['notes']->opencart_order_id);exit;
        $Payment_status=1;
        $update= Bikation_Bookings::where('Booking_ID', $payment['notes']->opencart_order_id)->update(['Razorpay_id'=>$payment->id,'Razorpay_entity'=>$payment->entity,'Razorpay_amount'=>$payment->amount,'Razorpay_currency'=>$payment->currency,'Razorpay_status'=>$payment->status,'Razorpay_order_id'=>$payment->order_id,'Razorpay_method'=>$payment->method,'Razorpay_amount_refunded'=>$payment->amount_refunded,'Razorpay_refund_status'=>$payment->refund_status,'Razorpay_captured'=>$payment->captured,'Razorpay_description'=>$payment->description,'Razorpay_card_id'=>$payment->card_id,'Razorpay_bank'=>$payment->bank,'Razorpay_wallet'=>$payment->wallet,'Razorpay_email'=>$payment->email,'Razorpay_contact'=>$payment->contact,'Razorpay_fee'=>$payment->fee,'Razorpay_service_tax'=>"",'Razorpay_error_code'=>$payment->error_code,'Razorpay_error_description'=>$payment->error_description,'Razorpay_created_at'=>$payment->created_at,'Payment_status' => $Payment_status,'book_status' => 'Booking' ]);
        Session::put('Booking_ID',$request->Booking_ID);
        Session::forget('price_payment');
		
		$bikation_booking = DB::table('bikation_bookings')->select('Total_Cost','Trip_ID','Customer_ID','Additional_Remarks')->where('Booking_ID', $request->Booking_ID)->first();
		$bikation_trip_booking = DB::table('bikation_trip')->select('Trip_Name','Trip_Start_Date','Trip_End_Date','Start_Location','Trip_Description')->where('Trip_ID',$bikation_booking->Trip_ID)->first();
		$bikation_booking_user= DB::table('users')->select('email','first_name','last_name','mobile_num')->where('id', $bikation_booking->Customer_ID)->first();
		$bikation_img_booking = DB::table('bikation_trip_photo')->select('Media_URL_details')->where('Trip_ID',$bikation_booking->Trip_ID)->first();
		
		 $data = ['Booking_ID'=>$request->Booking_ID, 'fname'=>$bikation_booking_user->first_name .' '.$bikation_booking_user->last_name, 'email'=>$bikation_booking_user->email, 'mob'=>$bikation_booking_user->mobile_num, 'trip_name'=>$bikation_trip_booking->Trip_Name, 'start_date'=>$bikation_trip_booking->Trip_Start_Date, 'end_date'=>$bikation_trip_booking->Trip_End_Date, 'meeting_loc'=>$bikation_trip_booking->Start_Location, 'cust_inst'=>$bikation_trip_booking->Trip_Description, 'price'=>$bikation_booking->Total_Cost,'image'=>'http:'.$bikation_img_booking->Media_URL_details];
		// print_r($data);exit;
		 $email = $bikation_booking_user->email;
                Mail::send('emails.bikation_user_booking', $data, function ($message) use ($email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($email)->subject('Booking Details');

                });
					$adm_email = env('BIKATION_ADMIN_EMAIL');
				Mail::send('emails.bikation_user_booking', $data, function ($message) use ($adm_email){
					$message->from(env('BIKATION_ADMIN_EMAIL'), env('BIKATION_ADMIN_NAME'));
                    $message->to($adm_email)->subject('Booking Details');

                });
				
       return redirect('thankyou');
       
    }

  

     public function cancel_payment(Request $request){
        $rules = array('' => '');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) { 
            $status = false;
        } 
        else {
        $status = true;
        //  $Booking_Seats=DB::table('bikation_bookings')->select('No_of_People')->where('Booking_ID', $request->Booking_ID)->first();
        // $total_no_of_booktickits=$Booking_Seats->No_of_People;
        // $packages_seats= DB::table('bikation_trip')->select('No_of_tickets_available')->where('Trip_ID',$request->Trip_ID)->first();
        // $available = $packages_seats->No_of_tickets_available + $total_no_of_booktickits;
        // Bikation_Trip::where('Trip_ID',$request->Trip_ID)->update(['No_of_tickets_available' => $available]);
        // }

         $Booking_Seats=DB::table('bikation_bookings')->select('No_of_People','No_of_Tickets_Cost_1px','No_of_Tickets_Cost_2px')->where('Booking_ID', $request->Booking_ID)->first();
        $total_no_of_booktickits=$Booking_Seats->No_of_People;
        $total_no_of_booktickits_Cost_1px=$Booking_Seats->No_of_Tickets_Cost_1px;
        $total_no_of_booktickits_Cost_2px=$Booking_Seats->No_of_Tickets_Cost_2px;
        $packages_seats= DB::table('bikation_package')->select('Tickes_Remaining_Cost_1px','Tickes_Remaining_Cost_2px')->where('Trip_ID',$request->Trip_ID)->first();
        $available_Tickes_Remaining_Cost_1px = $packages_seats->Tickes_Remaining_Cost_1px + $total_no_of_booktickits_Cost_1px;
        $available_Tickes_Remaining_Cost_2px = $packages_seats->Tickes_Remaining_Cost_2px + $total_no_of_booktickits_Cost_2px;
        Bikation_Package::where('Trip_ID',$request->Trip_ID)->update(['Tickes_Remaining_Cost_1px' => $available_Tickes_Remaining_Cost_1px,'Tickes_Remaining_Cost_2px'=>$available_Tickes_Remaining_Cost_2px]);

        return Response::json(array(
            'success' => $status,
            'errors' => $validator->getMessageBag()->toArray()
        ));
    }
    }
    public function update_payment_thankyou(Request $request){

       return view('bikationvendor.packages.thankyou');
       
    }
     public function update_payment_trip(Request $request){

       return view('bikationvendor.packages.payment');
       
    }
    public function filter_price(Request $request){
       
        $filter_value = $request->value;
      $where = '';
      $value = '';
      if( $filter_value== 'filer-price') {$where = "Cost_1px"; $value="asc"; }
      else if($filter_value == 'filer-popularity') {$where = "Overall";$value="desc";}
      else if($filter_value == 'filer-locations') {$where = "Start_Location";$value="asc";} 
      else {$where = "Trip_ID";$value="desc";}
     // echo $where;
      $packagesMakeList = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_reviews', 'bikation_reviews.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.General_Instructions','bikation_trip.Trip_End_Date','bikation_trip.Start_Location','bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Trip_url','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikation_reviews.Overall','bikationvendor_users.first_name','bikationvendor_users.last_name')
            ->where('bikation_trip.Status', '=', 1)
			->where('bikation_trip.Trip_Start_Date', '>', date("Y-m-d H:i:s"))
            ->where('bikation_trip.Approve_By_Admin', '=', 1)
            ->groupby('Trip_ID')
            ->orderBy($where, 'asc')
            ->get();


    foreach ($packagesMakeList as $model) {
        $Total_reviews = DB::table('bikation_reviews')->select('id','Trip_ID')->where('Trip_ID',$model->Trip_ID)->where('Status', '=', 1)->get();
        $model->Total_reviews= count($Total_reviews);
      }
      
      foreach ($packagesMakeList as $model) {
        $Trip_Start_Date = date('d F, Y H:i:s', strtotime($model->Trip_Start_Date));
        $model->Trip_Start_Date_new= $Trip_Start_Date;
      }

        return Response::json($packagesMakeList);
    }
     
    public function search_trip(Request $request){
       
      $search_text=$request->search_text;
	  $filter_value = $request->value;
      $where = '';
      $value = '';
      if( $filter_value== 'filer-price') {$where = "Cost_1px"; $value="asc"; }
      else if($filter_value == 'filer-popularity') {$where = "Overall";$value="desc";}
      else if($filter_value == 'filer-locations') {$where = "Start_Location";$value="asc";} 
      else {$where = "Trip_ID";$value="desc";}
	  
      $packagesMakeList = DB::table('bikation_trip')
            ->leftJoin('bikation_trip_photo', 'bikation_trip.Trip_ID', '=', 'bikation_trip_photo.Trip_ID')
            ->leftJoin('bikation_package', 'bikation_package.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_reviews', 'bikation_reviews.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikation_itinerary', 'bikation_itinerary.Trip_ID', '=', 'bikation_trip.Trip_ID')
            ->leftJoin('bikationvendor_users', 'bikationvendor_users.id', '=', 'bikation_trip.Vendor_ID')
            ->select('bikation_trip.Trip_ID', 'bikation_trip.Trip_Name', 'bikation_trip.Trip_Start_Date', 'bikation_trip.Trip_End_Date', 'bikation_trip.General_Instructions','bikation_trip.Total_Distance','bikation_trip.Total_No_of_Tickets','bikation_trip.Trip_url','bikation_trip.Start_Location','bikation_trip_photo.Media_URL','bikation_trip_photo.Media_URL_trip1','bikation_trip_photo.Media_URL_trip2','bikation_trip_photo.Media_URL_trip3','bikation_trip_photo.Media_URL_details','bikation_package.No_of_Tickets_Cost_1px','bikation_package.Tickes_Remaining_Cost_1px','bikation_package.Cost_1px','bikation_package.No_of_Tickets_Cost_2px','bikation_package.Tickes_Remaining_Cost_2px','bikation_package.Cost_2px','bikation_itinerary.Day','bikation_reviews.Overall','bikationvendor_users.first_name','bikationvendor_users.last_name')
            ->where('bikation_trip.Status', '=', 1)
            ->where('bikation_trip.Approve_By_Admin', '=', 1)
            ->where('Trip_Name', 'LIKE', '%' . $search_text . '%')
			->where('bikation_trip.Trip_Start_Date', '>', date("Y-m-d H:i:s"))
			->orderBy($where, 'asc')
            ->groupby('Trip_ID')
            ->get();

      foreach ($packagesMakeList as $model) {
        $Total_reviews = DB::table('bikation_reviews')->select('id','Trip_ID')->where('Trip_ID',$model->Trip_ID)->where('Status', '=', 1)->get();
        $model->Total_reviews= count($Total_reviews);
      }
      foreach ($packagesMakeList as $model) {
        $Trip_Start_Date = date('d F, Y H:i:s', strtotime($model->Trip_Start_Date));
        $model->Trip_Start_Date_new= $Trip_Start_Date;
      }
            
        return Response::json($packagesMakeList);
    }
}
