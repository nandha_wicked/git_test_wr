<?php

  namespace App\Http\Controllers;

  use Illuminate\Foundation\Bus\DispatchesJobs;
  use Illuminate\Routing\Controller as BaseController;
  use Illuminate\Foundation\Validation\ValidatesRequests;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Response;
  use App\Http\Requests;

  abstract class Controller extends BaseController
  {
      use DispatchesJobs, ValidatesRequests;

         
  }
