<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VendorController;
use Illuminate\Support\Facades\Validator;
use App\Models\AccessoriesPrice;
use App\Models\AccessoriesModel;
use App\Models\Area;
use App\Models\Bike;
use App\Models\Slot;
use App\Models\Vendor;
use App\Models\Parameters;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use DB;

class AccessoriesPriceController extends AppController
{
    use Helpers;
    public function index(){

        $priceList = AccessoriesPrice::all();
        $modelList = AccessoriesModel::where('status',1)->get();
        $areaList = Area::where('status',1)->get();
        foreach ($priceList as $price) {
          
          $price->model_name = $price->getModel();
          $price->area_name = $price->getArea();
        }
        return view('admin.accessoriesPrice.index', compact('priceList', 'modelList', 'areaList'));
    }

    public function add(Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'minimum_hours' => 'required',
        'hour_weekday' => 'required',
        'hour_weekend' => 'required',
        'day_weekday' => 'required',
        'day_weekend' => 'required',
        'five_weekday' => 'required',
        'week' => 'required',
        'month' => 'required' 
        );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/accessories_price')->withErrors($validator->errors());        
      }
      
      AccessoriesPrice::create(['model_id' => $request->modelId, 
                     'area_id' => $request->areaId,  
                     'minimum_hours' => $request->minimum_hours,
                    'hour_weekday' => $request->hour_weekday,
                    'hour_weekend' => $request->hour_weekend,
                    'day_weekday' => $request->day_weekday,
                    'day_weekend' => $request->day_weekend,
                    'five_weekday' => $request->five_weekday,
                    'week' => $request->week,
                    'month' => $request->month
                    ]);
      return redirect('/admin/accessories_price');            
    }

    public function edit($price_id,Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'minimum_hours' => 'required',
        'hour_weekday' => 'required',
        'hour_weekend' => 'required',
        'day_weekday' => 'required',
        'day_weekend' => 'required',
        'five_weekday' => 'required',
        'week' => 'required',
        'month' => 'required' 
        
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/accessories_price')->withErrors($validator->errors());        
      }

      AccessoriesPrice::where('id',$price_id)->update(['model_id' => $request->modelId, 
                                            'area_id' => $request->areaId,
                                            'minimum_hours' => $request->minimum_hours,
                                            'hour_weekday' => $request->hour_weekday,
                                            'hour_weekend' => $request->hour_weekend,
                                            'day_weekday' => $request->day_weekday,
                                            'day_weekend' => $request->day_weekend,
                                            'five_weekday' => $request->five_weekday,
                                            'week' => $request->week,
                                            'month' => $request->month
                                           ]);
      return redirect('/admin/accessories_price');    
    }   
    


    public function getTotalPrice(Request $request)
    {
        $rules = array(
            'model_id' => 'required',
            'area_id' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_date' => 'required',
            'end_time' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());
        }
        $dispatcher = app('Dingo\Api\Dispatcher');

        $startDate = Carbon::parse($request->start_date);
        $endDate = Carbon::parse($request->end_date);
        $no_of_days = $startDate->diffInDays($endDate);
        if($no_of_days == 0)
        $no_of_days = 1;
        $sd = $startDate->toDateString();
        $ed = $endDate->toDateString();

        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $lastHour = '24:00';
        $firstHour = '00:00';

        $modelId = $request->model_id;
        $areaId = $request->area_id;

        $areaisvendor = Area::where('id',$areaId)->first();
        $areaisvendorBool = $areaisvendor->vendor_id;

        $priceObj = AccessoriesPrice::where('model_id', $modelId)->where('area_id', $areaId)->orderBy('id','desc')->first();
        if(!$priceObj)
        {
            $priceObj = AccessoriesPrice::where('model_id', $modelId)->where('area_id',0)->orderBy('id','desc')->first();
            if(!$priceObj)
            {
                return ['total_price' => null, 'effective_price' => null, 'no_of_hours' => null];
            }
        }

        $price = 0;
        $weekDayHours = 0;
        $weekEndHours = 0;
        $weekday = 0;
        $weekend = 0;
        $totalHours = 0;

        if ($startDate == $endDate) {
            $noOfHours = AccessoriesPriceController::getNoOfHours($startTime, $endTime);
            if($startDate->isWeekday())
                $weekDayHours = $noOfHours;
            else
                $weekEndHours = $noOfHours;

        }
        else 
        {

            while ($startDate <= $endDate) 
            {
                if($startDate->isWeekday()){
                    $weekday++;
                    $noOfHours = AccessoriesPriceController::getTotalHours($startDate->toDateString(), $sd, $ed, $startTime, $endTime, $firstHour, $lastHour);
                    $weekDayHours = $weekDayHours + $noOfHours;
                }
                if($startDate->isWeekend())
                {

                    $noOfHours = AccessoriesPriceController::getTotalHours($startDate->toDateString(), $sd, $ed, $startTime, $endTime, $firstHour, $lastHour);

                    $weekEndHours = $weekEndHours + $noOfHours;
                }
                $startDate->addDay();
            }
        }
        
        $totalHours = $weekEndHours + $weekDayHours;
        
                
        $totalHoursTrue = $weekEndHours + $weekDayHours;
        
       

        $minimum_weekday_hours = $priceObj->minimum_hours;

        $minimum_weekend_hours = Parameters::where('parameter_name','minimum_weekend_hours')->pluck('parameter_value');

        $minimum_weekend_hours = (int)$minimum_weekend_hours;


        if($totalHours<$minimum_weekday_hours&&$weekEndHours==0)
        {
            $weekDayHours = $minimum_weekday_hours;
        }
        if($totalHours<$minimum_weekend_hours&&$weekEndHours>0)
        {
            $weekEndHours = 24;
            $weekDayHours = 0;
        }

        $totalHours = $weekEndHours + $weekDayHours;

        $hours_for_weekday_daily_cieling = ceil($priceObj->day_weekday/$priceObj->hour_weekday);
        $hours_for_weekend_daily_cieling = ceil($priceObj->day_weekend/$priceObj->hour_weekend);


        if($totalHours>=672)
        {
            $price = $totalHours*($priceObj->month/672);
        }
        elseif($totalHours>=168)
        {
            $price = $totalHours*($priceObj->week/168);
        }
        elseif($totalHours>=120)
        {
            if($weekDayHours>=96+$hours_for_weekday_daily_cieling)
            {
                //1 5-weekday price applies for the 5 weekdays
                if($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->week;
                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->five_weekday + $priceObj->day_weekend + (($weekEndHours-24)*$priceObj->day_weekend/24);
                }
                elseif($weekEndHours>=$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->five_weekday + $priceObj->day_weekend;
                }
                else
                {
                    $price = $priceObj->five_weekday + ($weekEndHours*$priceObj->hour_weekend);  
                }
            }
            else
            {
                if($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = 2*$priceObj->day_weekend + (floor($weekDayHours/24)*$priceObj->day_weekday);
                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->day_weekend + (($weekEndHours-24)*$priceObj->day_weekend/24) + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                elseif($weekEndHours>=$hours_for_weekend_daily_cieling)
                {
                    $price = $priceObj->day_weekend + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }
                }
                else
                {
                    $price = ($weekEndHours*$priceObj->hour_weekend) + (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }  
                }

            }
        }
        elseif($weekDayHours>=96+$hours_for_weekday_daily_cieling)
        {
            if($weekEndHours==0)
                $price = $priceObj->five_weekday;
            else
            {
                $price = $priceObj->five_weekday;
                if(fmod($weekEndHours,24)>=$hours_for_weekend_daily_cieling)
                {
                    $price += $priceObj->day_weekend;
                }
                else
                {
                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;
                }   
            }
        }
        else
        {
            if($weekDayHours == 0)
            {
                $price = floor($weekEndHours/24)*$priceObj->day_weekend;

                if(fmod($weekEndHours,24)>=$hours_for_weekend_daily_cieling)
                {
                    $price += $priceObj->day_weekend;
                }
                else
                {
                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;
                }                
            }
            elseif($weekEndHours == 0)
            {

                $price = floor($weekDayHours/24)*$priceObj->day_weekday;

                if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                {
                    $price += $priceObj->day_weekday;
                }
                else
                {
                    $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;

                }
            }
            else
            {
                if($weekEndHours>=48)
                {
                    $price = 2*$priceObj->day_weekend;

                    $price += floor($weekDayHours/24)*$priceObj->day_weekday;

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }

                }
                elseif($weekEndHours>=24+$hours_for_weekend_daily_cieling)
                {
                    $price = 2*$priceObj->day_weekend;
                    $weekDayHoursAfter2Weekends = $weekDayHours - (24-fmod($weekEndHours,24));
                    if($weekDayHoursAfter2Weekends<0)
                        $weekDayHoursAfter2Weekends = 0;

                    $price += (floor($weekDayHoursAfter2Weekends/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHoursAfter2Weekends,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHoursAfter2Weekends,24)*$priceObj->hour_weekday;
                    }

                }
                elseif($weekEndHours>=24)
                {
                    $price = $priceObj->day_weekend;

                    $price += fmod($weekEndHours,24)*$priceObj->hour_weekend;

                    $price += (floor($weekDayHours/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHours,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHours,24)*$priceObj->hour_weekday;
                    }

                }
                else
                {
                    $price = $priceObj->day_weekend;
                    $weekDayHoursAfter2Weekends = $weekDayHours - (24-$weekEndHours);
                    if($weekDayHoursAfter2Weekends<0)
                        $weekDayHoursAfter2Weekends = 0;

                    $price += (floor($weekDayHoursAfter2Weekends/24)*$priceObj->day_weekday);

                    if(fmod($weekDayHoursAfter2Weekends,24)>=$hours_for_weekday_daily_cieling)
                    {
                        $price += $priceObj->day_weekday;
                    }
                    else
                    {
                        $price += fmod($weekDayHoursAfter2Weekends,24)*$priceObj->hour_weekday;
                    }
                }
            }
        }
                
        // Need $totalHours and $price
        /* DISCOUNT PRICE */
        
        if($totalHoursTrue==0)
        {
            $effectivePrice = 0;   
        }
        else
        {
            $effectivePrice = round($price/$totalHoursTrue);   
            
        }
        if($weekEndHours==0)
        {
            $pricePerDay = $priceObj->day_weekday;
        }
        else
        {
            $pricePerDay = $priceObj->day_weekend;
        }


        $totalPrice = $price;
        $finalPrice = $price;

        $coupon = '';
        $couponStatus = false;
        $couponMessage = 'No coupon code applied';
        $discount = 0;
        Session::put('bkPromoCode', null);
        $device = $request->device;
        if (empty($device))
            $device = 'MOBILE';
        
        
        
        $full_slab_price = max($totalHours,$totalHoursTrue)*$priceObj['hour_weekend'];
        

        if($request->source!="apply_promocode")
        {
            if ($request->coupon) 
            {

                $couponData = $dispatcher->with(['code' => $request->coupon, 'model_id' => $modelId, 'area_id' => $areaId, 'start_date' => $sd, 'end_date' => $ed, 'start_time' => $startTime, 'end_time' => $endTime, 'price' => $price, 'device' => $device,'full_slab_price'=>$full_slab_price])->get('promocodes/apply');


                if ($couponData['status']) {
                  Session::put('bkPromoCode', $request->coupon);
                  $coupon = $request->coupon;
                  $couponStatus = $couponData['status'];
                  $couponMessage = $couponData['message'];
                  $discount = $couponData['discount'];
                  $finalPrice = $couponData['final_price'];
                } else {
                  $coupon = $request->coupon;
                  $couponStatus = $couponData['status'];
                  $couponMessage = $couponData['message'];
                }
            }
            
        }
        
        
        
         
        if($totalHoursTrue < $minimum_weekend_hours){

            if($weekEndHours>0 && $weekEndHours<$minimum_weekend_hours)
            {
                $minimum_billing_message = "The price shown is for ".$minimum_weekend_hours." hours due to minimum billing policy.";
                $minimum_hours = $minimum_weekend_hours;

            }
            else if($totalHoursTrue < $priceObj['minimum_hours'])
            {
                $minimum_billing_message = "The price shown is for ".$priceObj['minimum_hours']." hours due to minimum billing policy.";
                $minimum_hours = $priceObj['minimum_hours'];

            }
            else
            {
                $minimum_billing_message = "";
                $minimum_hours = $priceObj['minimum_hours'];
            }
        }
        else
        {
            $minimum_billing_message="";
            $minimum_hours = $priceObj['minimum_hours'];
        }
        
        
        

        return [
            
            'promocode' => [
                            'code' => $coupon,
                            'status' => $couponStatus,
                            'message' => $couponMessage
                          ],
            'total_price' => round($totalPrice),
            'total_price_with_tax' => round($totalPrice*env('TOTALWITHTAXFACTOR'),2),
            'total_tax' => round($totalPrice*(env('TOTALWITHTAXFACTOR')-1),2),
            'price_after_promo' => round($finalPrice), 
            'discount' => round($discount),
            'effective_price' => round($effectivePrice), 
            'no_of_hours' => round($totalHoursTrue), 
            "price_per_day" => round($pricePerDay), 
            "no_of_days" => round($no_of_days),
            "minimum_billing_message" => $minimum_billing_message,
            "minimum_hours" => $minimum_hours,
            "price_per_hour" => $priceObj['hour_weekend'],
            'full_slab_price'=>$full_slab_price,
            'weekday_price'=>$priceObj['day_weekday'],
            'weekend_price'=>$priceObj['day_weekend']
        ]
            ;
        
    }

    public static function getTotalHours($date, $sd, $ed, $startTime, $endTime, $firstHour, $lastHour){
      if($date == $sd){
        $noOfHours = AccessoriesPriceController::getNoOfHours($startTime, $lastHour);
      }
      if($date == $ed){
        $noOfHours = AccessoriesPriceController::getNoOfHours($firstHour, $endTime);
      }
      if($date != $sd && $date != $ed){
        $noOfHours = AccessoriesPriceController::getNoOfHours($firstHour, $lastHour);
      }
      return $noOfHours;
    }

    public static function getNoOfHours($startTime, $endTime){
      $start = explode(':', $startTime);
      $end = explode(':', $endTime);
      $hours = $end[0] - $start[0];       

      return $hours;
    }

    
    
    
    
    public function indexVendor(){

        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();
        $areaListArray = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        $priceList = AccessoriesPrice::whereIn('area_id',$areaListArray)->get();
        $modelList = AccessoriesModel::where('status',1)->get();
        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get();
        foreach ($priceList as $price) {
          $model = $price->model;
          $area = $price->area;
          $price->model_name = $model['model'];
          $price->area_name = $area['area'];
        }
        return view('vendor.accessoriesPrice.index', compact('priceList', 'modelList', 'areaList'));
    }

    public function addVendor(Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'price_per_hour' => 'required',
        'minimum_hours' => 'required',
        
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/vendor/accessories_price')->withErrors($validator->errors());        
      }
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        if (in_array($request->areaId, $areaList->toArray())) {
      
            AccessoriesPrice::create(['model_id' => $request->modelId, 'area_id' => $request->areaId, 'price_per_hour' => $request->price_per_hour, 'minimum_hours' => $request->minimum_hours]);
            return redirect('/vendor/accessories_price'); 
        }
        else {
            return redirect('/vendor/accessories_price'); 

        }
    }

    public function editVendor($price_id,Request $request){
      $rules = array(
        'modelId' => 'required',
        'areaId' => 'required',
        'price_per_hour' => 'required',
        'minimum_hours' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/vendor/accessories_price')->withErrors($validator->errors());        
      }
        $user = Auth::user();
        $vendor = Vendor::where('user_id',$user->id)->first();

        $areaList = Area::where('status',1)->where('vendor_id', $vendor['id'])->get()->lists('id');
        
        if (in_array($request->areaId, $areaList->toArray())) {
            
            AccessoriesPrice::where('id',$price_id)->update(['model_id' => $request->modelId, 'area_id' => $request->areaId, 'price_per_hour' => $request->price_per_hour, 'minimum_hours' => $request->minimum_hours]);
            
            return redirect('/vendor/accessories_price');   
        }
        else {
            return redirect('/vendor/accessories_price'); 

        }
    }   

}
