<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\City;
use App\Models\Bike;
use App\Models\Area;
use App\Models\Image;
use App\Transformers\CityTransformer;
use App\Transformers\BikeMakeTransformer;
use App\Transformers\BikeModelTransformer;
use Dingo\Api\Routing\Helpers;
use Carbon\Carbon;

class CityController extends AppController{
  use Helpers;

  protected $cityTransformer;
  protected $bikeMakeTransformer;
  protected $bikeModelTransformer;

  function __construct(CityTransformer $cityTransformer, BikeMakeTransformer $bikeMakeTransformer, BikeModelTransformer $bikeModelTransformer){
    $this->cityTransformer = $cityTransformer;
    $this->bikeMakeTransformer = $bikeMakeTransformer;
    $this->bikeModelTransformer = $bikeModelTransformer;
  }

  public function index(){
    $cityList = City::all();
    return view('admin.city.index',compact('cityList'));
  }


  public function add(Request $request){
    $rules = array(
      'cityName' => 'required',
      'cityStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/city')->withErrors($validator->errors());      
    }
    else{
      City::create(['city' => $request->cityName, 'status' => $request->cityStatus]);
      return redirect('/admin/city');
    }   
  }


  public function edit($city_id,Request $request){
    $rules = array(
      'cityName' => 'required',
      'cityStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/city')->withErrors($validator->errors());      
    }
    City::where('id',$city_id)->update(['city' => $request->cityName, 'status' => $request->cityStatus]);
      
    $city = City::where('id',$city_id)->first();
      
    if (isset($request->cityStatus)) {
        if($request->cityStatus!=$city->status)
        {
            $areaIds = Area::where('city_id', $city_id)->get()->lists('id');
            foreach ($areaIds as $areaId) 
            {
                Area::where('id', $areaId)->update(['status' => $request->cityStatus]);
                Bike::where('area_id', $areaId)->update(['status' => $request->cityStatus]);
            }
        }
    }
    return redirect('/admin/city'); 
  }

  public function cities(){
    $cities = City::where('status', 1)->get();
    $data = $this->cityTransformer->transformCollection($cities->toArray());
    return $this->response->array($data);
  }

  public function show($id){
    $city = City::find($id);
    if (!$city) {
      return $this->respondNotFound('Not Found');
    }
    $data = $this->cityTransformer->transform($city);
    return $this->response->array($data);
  }
    
  

}
