<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use DateTime;
use App\Models\Bikation_People_On_Trip;
use App\Models\Bikation_Bookings;
use Session;


class Bikation_People_On_Trip_AdminController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
    /*if(Session::has('email') && Session::has('First_Name') ){*/
		 $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->get();
	   $selected="";
      if (isset($request->btn_submit)) {
         $bikation_People_On_TripList = Bikation_People_On_Trip::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_People_On_TripList = Bikation_People_On_Trip::all();
        $selected="";
      }
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Approve_By_Admin', 1)->get();
      //$bikation_People_On_TripList = Bikation_People_On_Trip::all();
      foreach ($bikation_People_On_TripList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      return view('bikationvendor.admin.peopleontrip',compact('bikation_People_On_TripList','bikation_tripList','bikation_AlltripList','selected'));
   /* }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
  public function addPeople_On_Trip(Request $request){
    /*if(Session::has('email') && Session::has('First_Name') ){*/
      $rules = array(
          'Name_of_Co_Px' => 'required',
          'Contact_No' => 'required',
          'Email_ID' => 'required',
          'Sex' => 'required',
          'DoB' => 'required',
          'Rider' => 'required'
           );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('admin/bikation-people_on_trip')->withErrors($validator->errors());
      }
      else{
        Bikation_People_On_Trip::create(['Book_ID' => $request->Book_ID,'Trip_ID' => $request->Trip_ID,'Name_of_Co_Px' => $request->Name_of_Co_Px, 'Contact_No' => $request->Contact_No,'Email_ID' => $request->Email_ID,'Sex' => $request->Sex,'DoB'=>date('Y-m-d', strtotime($request->DoB)),'Rider' => $request->Rider, 'DL' => $request->DL,'Cancelled_On' =>date('Y-m-d', strtotime($request->Cancelled_On)),'Cancelled_By' => $request->Cancelled_By,'Status'=>$request->Status,'Booked_Type'=>$request->Booked_Type]);
        return redirect('admin/bikation-people_on_trip');
      }
    /*}
    else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
  public function edit($People_On_Trip,Request $request){
  /*if(Session::has('email') && Session::has('First_Name') ){*/
  $rules = array('Contact_No' => 'required',
                  'Email_ID' => 'required',
                  'Book_ID' => 'required',
                  'Trip_ID' => 'required',
                  'Rider' => 'required');
  $validator = Validator::make($request->all(),$rules);
  if($validator->fails()){
    return redirect('admin/bikation-people_on_trip')->withErrors($validator->errors());
  }
  else{
    Bikation_People_On_Trip::where('id', $People_On_Trip)
    ->update(['Book_ID' => $request->Book_ID,'Trip_ID' => $request->Trip_ID,'Name_of_Co_Px' => $request->Name_of_Co_Px, 'Contact_No' => $request->Contact_No,'Email_ID' => $request->Email_ID,'Sex' => $request->Sex,'DoB'=>date('Y-m-d', strtotime($request->DoB)),'Rider' => $request->Rider, 'DL' => $request->DL,'Cancelled_On' =>date('Y-m-d', strtotime($request->Cancelled_On)),'Cancelled_By' => $request->Cancelled_By,'Status'=>$request->Status]);
    return redirect('admin/bikation-people_on_trip');
    }
 /* }
  else{
    return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
  }*/
  }
  public function delete($People_On_Trip){
   /* if(Session::has('email') && Session::has('First_Name') ){*/
      $packages = Bikation_People_On_Trip::where('id', '=', $People_On_Trip)->delete();
      return redirect('admin/bikation-people_on_trip');
    /*}else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }*/
  }
}
