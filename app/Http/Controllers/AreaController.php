<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Area;
use App\Models\City;
use App\Models\Bike;
use App\Models\User;
use App\Transformers\AreaTransformer;
use Carbon\Carbon;
use Auth;
use DB;

class AreaController extends AppController
{

    protected $areaTransformer;

    function __construct(AreaTransformer $areaTransformer){
      $this->areaTransformer = $areaTransformer;
    }

    public function index()
    {

        $cityList=City::all();
        $areaList=Area::all();


        return view('admin.area.index',compact('cityList','areaList'));
    }


    public function add(Request $request){
        $rules = array(
            'cityName' => 'required',
            'areaName' => 'required',
            'areaAddress' => 'required',
            'areaCode' => 'required',
            'areaStatus' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return redirect('/admin/area')->withErrors($validator->errors());
        }


        if($request->lat!==""&&$request->long!="")
        {

            $gmapLink = "https://www.google.com/maps/embed/v1/place?q=".$request->lat.",".$request->long."&key=AIzaSyDWDQ9kPc2wjwc--c4RVm9XJHa9AbYzYcE";
            $mapsLink = "https://www.google.co.in/maps/place/".$request->lat.",".$request->long;
        }

        $fromOpenHours = $request->from_open_hours;
        $toOpenHours = $request->to_open_hours;
        $closedHours = $request->closed_hours;

        $listOfOpenHours = [];

        for($i=0;$i<7;$i++)
        {
            $closedHourNumbers = explode(",",$closedHours[$i]);
            $listOfOpenHours[$i] = [];
            foreach(range($fromOpenHours[$i],$toOpenHours[$i]) as $number) 
            {
                if(!in_array($number,$closedHourNumbers))
                {
                    array_push($listOfOpenHours[$i],$number);
                }
            }
        }

        Area::create(['city_id' => $request->cityName, 'area' => $request->areaName, 'address' => $request->areaAddress, 'status' => $request->areaStatus, 'code' => $request->areaCode, 'gmapLink'=>$gmapLink, 'latitude'=>$request->lat, 'longitude'=>$request->long, 'maps_link'=>$mapsLink, 'hours_open'=>json_encode($listOfOpenHours)]);
        

        return redirect('/admin/area');

    }

    public function edit($area_id,Request $request){
      $rules = array(
        'cityName' => 'required',
        'areaName' => 'required',
        'areaAddress' => 'required',
        'areaCode' => 'required',
        'areaStatus' => 'required'
      );

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){
        return redirect('/admin/area')->withErrors($validator->errors());
      }
        $area = Area::where('id',$area_id)->first();

        $gmapLink = "";

        $mapsLink = "";

        if($request->lat!==""&&$request->long!="")
        {

            $gmapLink = "https://www.google.com/maps/embed/v1/place?q=".$request->lat.",".$request->long."&key=AIzaSyDWDQ9kPc2wjwc--c4RVm9XJHa9AbYzYcE";
            $mapsLink = "https://www.google.co.in/maps/place/".$request->lat.",".$request->long;
        }

        $fromOpenHours = $request->from_open_hours;
        $toOpenHours = $request->to_open_hours;
        $closedHours = $request->closed_hours;

        $listOfOpenHours = [];

        for($i=0;$i<7;$i++)
        {
            $closedHourNumbers = explode(",",$closedHours[$i]);
            $listOfOpenHours[$i] = [];
            foreach(range($fromOpenHours[$i],$toOpenHours[$i]) as $number) 
            {
                if(!in_array($number,$closedHourNumbers))
                {
                    array_push($listOfOpenHours[$i],$number);
                }
            }
        }


        Area::where('id',$area_id)->update(['area' => $request->areaName, 'address'=>$request->areaAddress, 'status' => $request->areaStatus, 'code' => $request->areaCode, 'gmapLink'=>$gmapLink, 'latitude'=>$request->lat, 'longitude'=>$request->long, 'maps_link'=>$mapsLink, 'hours_open'=>json_encode($listOfOpenHours)]);
        
        
        if($request->kirana_users != "")
        {
            DB::table('kirana_user_area_relation')->where('area',$area_id)->delete();
            $request_kirana_users = str_replace(" ","",$request->kirana_users);
            $kiranaEmails = explode(",",$request_kirana_users);
            foreach($kiranaEmails as $kiranaEmail)
            {
                $kiranUser = User::where('email',$kiranaEmail)->first();
                if(!$kiranUser)
                {
                    $errorStr = "Kirana user with email ID ".$kiranaEmail." not found. Please try again.";
                    return redirect()->back()->withErrors([$errorStr]);
                }
                else
                {
                    DB::table('kirana_user_area_relation')->insert([
                        'user'=>$kiranUser->id,
                        'area'=>$area_id,
                        'created_by'=>Auth::user()->id
                    ]);
                    User::where('id',$kiranUser->id)->update([
                        'isOperation'=>1,
                        "role_string"=>"kirana"
                    ]);
                }
            }
        }


      return redirect('/admin/area'); 
    }


    public function getAllAreas($id){
      $city = City::find($id);
      if (!$city) {
        return $this->respondNotFound('City not found');
      }

      $areas = $city->areas()->where('status', 1)->get();
      foreach($areas as $area){
        $area->latitude = "0.0001234";
        $area->longitude = "0.0003456";
      }
      $data = $this->areaTransformer->transformCollection($areas->toArray());
      return $this->respondWithSuccess($data);
    }
    
    public function isAreaOpen(Request $request)
    {
            
        $startDateTimeCarbon = Carbon::parse($request->start_datetime);
        
        $endDateTimeCarbon = Carbon::parse($request->end_datetime);
                
        $startDayOfTheWeek = $startDateTimeCarbon->dayOfWeek;
        
        $endDayOfTheWeek = $endDateTimeCarbon->dayOfWeek;
        
        $startHour = $startDateTimeCarbon->hour;
        
        $endHour = $endDateTimeCarbon->hour;
        
        if(isset($request->area_id))
            $areas = Area::where('id',$request->area_id)->get();
        else
            $areas = Area::where('status',1)->get();
        
        
        $startAvailableArea = [];
        $endAvailableArea = [];
        
        $startNotAvailableArea = [];
        $endNotAvailableArea = [];
        
        foreach($areas as $area)
        {
            $areaTimings = json_decode($area->hours_open);
            
            if(in_array($startHour,$areaTimings[$startDayOfTheWeek]))
            {
                array_push($startAvailableArea,$area->id);
            }
            else
            {
                $closestHour = Self::getClosest($startHour,$areaTimings[$startDayOfTheWeek],true);
                $startNotAvailableArea[$area->id] = $closestHour;                
            }
            
            if(in_array($endHour,$areaTimings[$endDayOfTheWeek]))
            {
                array_push($endAvailableArea,$area->id);
            }
            else
            {
                $closestHour = Self::getClosest($endHour,$areaTimings[$endDayOfTheWeek],false);
                $endNotAvailableArea[$area->id] = $closestHour;                
            }
            
        }
        
        $data = [
            'start_available_areas'=>$startAvailableArea,
            'end_available_areas'=>$endAvailableArea,
            'start_not_available_areas'=>$startNotAvailableArea,
            'end_not_available_areas'=>$endNotAvailableArea,
            'start_day'=>$startDayOfTheWeek,
            'end_day'=>$endDayOfTheWeek,
        ];
        
        return $this->respondWithSuccess($data);  
        
        
    }
    
    function getClosest($search, $arr, $start) {
        $closest = 0;
        foreach ($arr as $item) {
            if($start)
            {
                if ($item > $search) {
                    $closest = $item;
                    break;
                }
            }
            else
            {
                if ($item > $search) {
                    break;
                }
                $closest = $item;
            }
        }
        return $closest;
    }





}
