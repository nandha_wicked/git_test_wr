<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Area;
use App\City;
use Auth;
use DB;
use App\Models\FrontEndText;

class FrontEndTextController extends AppController
{
    
    public function indexFrontEndText(Request $request)
    {
        $pageTitle = "Front End Text";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>80,"title"=>"Text key"],
                                            ["width"=>1200,"title"=>"Text"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $frontEndTextObjects = FrontEndText::all();
        
        $objectTableArray['body'] = [];
        
        foreach($frontEndTextObjects as $frontEndTextObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$frontEndTextObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$frontEndTextObject->text_key,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$frontEndTextObject->text_value,"pre"=>"true"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_front_end_text_btn",
                                                            "id"=>"edit_front_end_text_id",
                                                            "data_id"=>$frontEndTextObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_front_end_text_btn",
                                                            "id"=>"delete_front_end_text_id",
                                                            "data_id"=>$frontEndTextObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_front_end_text_modal";
        $addObject['add_button_text'] = "Add Text";
        $addObject['modal_title'] = "Add Front End Text";
        $addObject['add_url'] = "/admin/front-end-text/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Text Key","input_type"=>"text","name"=>"text_key_add"],
                ["type"=>"textarea","label"=>"Text","name"=>"text_value_add"],
            
                
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_front_end_text_modal";
        $editObject['edit_btn_class'] = "edit_front_end_text_btn";
        $editObject['modal_title'] = "Edit Front End Text";
        $editObject['edit_url'] = "/admin/front-end-text/edit";
        $editObject['ajax_url'] = "/admin/front-end-text/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input","label"=>"Text Key","input_type"=>"text","name"=>"text_key_edit"],
                ["type"=>"textarea","label"=>"Text","name"=>"text_value_edit"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_front_end_text_modal";
        $deleteObject['delete_btn_class'] = "delete_front_end_text_btn";
        $deleteObject['modal_title'] = "Delete Front End Text";
        $deleteObject['delete_url'] = "/admin/front-end-text/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this Front End Text?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addFrontEndText(Request $request)
    {
            FrontEndText::create([
                'created_by'=>Auth::user()->id,
                'text_key'=>$request->text_key_add, 
                'text_value'=>$request->text_value_add,
            ]);
        
        return redirect()->back();
    }
    
    public function detailsFrontEndText(Request $request)
    {
        $frontEndText = FrontEndText::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"text_key_edit","value"=>$frontEndText->text_key],
                ["type"=>"textarea","name"=>"text_value_edit","value"=>$frontEndText->text_value],
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editFrontEndText($id, Request $request)
    {
        FrontEndText::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'text_key'=>$request->text_key_edit, 
            'text_value'=>$request->text_value_edit,
        ]);
        
        return redirect()->back();
    }
    
    public function deleteFrontEndText($id, Request $request)
    {
        $frontEndText = FrontEndText::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($frontEndText),"table_name"=>"front_end_text","deleted_by"=>Auth::user()->id]);
        FrontEndText::where('id',$id)->delete();
        return redirect()->back();
    }
    
}
