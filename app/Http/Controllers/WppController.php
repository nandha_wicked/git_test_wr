<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Bike;
use App\Models\City;
use App\Models\Area;
use App\Models\News;
use App\Models\Price;
use App\Models\Image;
use App\Models\ContactEnquiry;
use App\Models\WppEnquiry;
use App\Models\EmailSubscribe;
use App\Models\User;
use App\Models\BikeModel;
use App\Models\BikeMake;
use Auth;
use Crypt;
use Response;
use Session;
use Mail;
use Hash;
use DB;
use Config;
use Input;
use Carbon\Carbon;
class WppController extends AppController
{
    public function addWppEnquiry(Request $request) {       

        $uri=$request->path();

        if($uri == "wpp-add"){

            $rules = array(
                'fullName' => 'required',
                'email' => 'required',
                'mobileNumber' => 'required|digits:10',
                'bikes_count' => 'required',
                'loan' => 'required'
            );

            $fullName = $request->fullName;
            
            $email = $request->email;
            $mob = $request->mobileNumber;
            $bikes = implode(",", $request->get('bikeinterested'));
            $bikes_count =$request->bikes_count;
            $loan = $request->loan;
            $data = ['fullName'=>$fullName, 'bikes_count'=>$bikes_count, 'email'=>$email, 'mobileNumber'=>$mob, 'loan' => $loan, 'bikeinterested'=>$bikes];
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails())
                return $this->respondWithValidationError($validator->errors());        
           WppEnquiry::create(['fullName' => $request->fullName,  'email' => $request->email, 'mobileNumber' => $request->mobileNumber, 'bikes_count'=>$request->bikes_count,'bikeinterested'=>$bikes, 'loan' => $request->loan ]);

        echo "<script>alert('Thank you for signing up for WPP, Our team will get in touch with you shortly. '); window.location = '/'</script>";

                   
        }
    }
       
    public function wppEnquiryNoteEdit($id,Request $request)
    {

        CancelledEnquiry::where('id',$request->id)->update(['notes'=>$request->note, 'updated_by'=>Auth::user()->email]);
        return redirect('admin/wppEnquiry');
    }
    public function wppEnquiryIndex()
    {

        $enquiryList = WppEnquiry::orderBy('id', 'desc')->take(5000)->get();

        return view('admin.wppEnquiry.index',compact('enquiryList'));

    }
    
    
    
}
