<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Addon;
use App\Models\Bikation_Trip;
use App\Models\Bikation_Package;
use Session;

class Bikation_AddonController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){

    if(Session::has('email') && Session::has('First_Name') ){

      $selected="";
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
      $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Vendor_ID',Session::get('Vendor_ID'))->get();
      $bikation_packageList = DB::table('bikation_package')->select('Package_ID','name')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
      if (isset($request->btn_submit)) {
         $bikation_addonList = Bikation_Addon::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_addonList = Bikation_Addon::where('Vendor_ID',Session::get('Vendor_ID'))->get();
        $selected="";
      }

      foreach ($bikation_addonList as $model) {

        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      foreach ($bikation_addonList as $model) {
        $Package_Name= DB::table('bikation_package')->select('name')->where('Package_ID',$model->Package_ID)->first();
        $model['PackageName'] = ($Package_Name) ? $Package_Name->name : '';
      }
      return view('bikationvendor.front.addon_index',compact('bikation_addonList','bikation_packageList','bikation_tripList','bikation_AlltripList','selected'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addAddon(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array('Trip_ID' => 'required',
                      'Exclusion_Inclusion' => 'required',
                      'Cost' => 'required',
                      'Status' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-addon')->withErrors($validator->errors());
      }
      else{
        $Vendor_ID = Session::get('Vendor_ID');
        Bikation_Addon::create(['Trip_ID' => $request->Trip_ID,'Package_ID' => $request->Package_ID, 'Activity_ID' => $request->Activity_ID,'Vendor_ID'=>$Vendor_ID,'Exclusion_Inclusion' => $request->Exclusion_Inclusion,'Std_Opt' => $request->Std_Opt,'Cost'=>$request->Cost,'Status'=>$request->Status]);
        return redirect('bikation-addon');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($addonId, Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array('Trip_ID' => 'required',
                      'Exclusion_Inclusion' => 'required',
                      'Cost' => 'required',
                      'Status' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-addon')->withErrors($validator->errors());
      }
      else{
        $Vendor_ID = Session::get('Vendor_ID');
        Bikation_Addon::where('id', $addonId)
        ->update(['Trip_ID' => $request->Trip_ID,'Package_ID' => $request->Package_ID, 'Activity_ID' => $request->Activity_ID,'Vendor_ID'=>$Vendor_ID,'Exclusion_Inclusion' => $request->Exclusion_Inclusion,'Std_Opt' => $request->Std_Opt,'Cost'=>$request->Cost,'Status'=>$request->Status]);
        return redirect('bikation-addon');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }

  public function delete($addonId){
    if(Session::has('email') && Session::has('First_Name') ){
      $packages = Bikation_Addon::where('id', '=', $addonId)->delete();
      return redirect('bikation-addon');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }

}
