<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;


use App\Http\Requests;
use Auth;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\User;
use App\Models\PromoCode;
use App\Models\SentTransaction;
use App\Models\Parameters;
use App\Models\ErrorInfo;
use App\Models\ReservationInfo;
use App\Models\BookingHeader;
use App\Models\DeliveryInfo;
use App\Models\FinancialInfo;
use App\Models\NotesInfo;
use App\Models\PricingInfo;
use App\Models\ReturnInfo;
use App\Models\AccessoriesList;
use App\Models\Booking;
use App\Models\PaytmOrderDetails;

use Session;
use Mail;
use Config;
use Curl;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use App\Jobs\RazorPay;
use App\Jobs\RazorPayApp;
use App\Jobs\RazorPayCapture;
use Razorpay\Api\Api;
use App\Models\CancelledEnquiry;
use DB;
use JWTAuth;
use Illuminate\Support\Facades\Bus;

use App\Http\Controllers\BikeAvailabilityController;

require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");





class BookingCreationController extends AppController
{
    public function paymentApp(Request $request)
    {
        
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        DB::table('appRequest')->insert(['request'=>json_encode($requestArray)]);

        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $note = $request->notes;
        $coupon = isset($request->coupon)?$request->coupon:"";
        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $booking_platform=isset($request->booking_platform)?$request->booking_platform:"app";

        
        $appliedWalletAmount = isset($request->applied_wallet_amount)?$request->applied_wallet_amount:0;
        
        $paymentMethod = $request->payment_method;
        $pgTxnId = $request->pg_txn_id;
        $pgTxnAmount = $request->pg_txn_amount;
        $pgStatus = $request->status;
        $conditions = $request->conditions;
        
        $user = UserController::getLoggedInUser();
        
        $model = BikeModel::where('id',$modelId)->first();
        $area = Area::where('id',$areaId)->first();
        
        $startDateTimeCarbon = Carbon::parse($startDate." ".$startTime);
        $endDateTimeCarbon = Carbon::parse($endDate." ".$endTime);
        
        
        if(in_array($paymentMethod,array("RazorPayApp","PayLaterAndroid","ZeroPaymentAndroid")))
            $creation_mode = "A";
        else
            $creation_mode = "I";
        
        if((($request->payment_method == "PayLaterAndroid")||($request->payment_method == "PayLateriOS"))&&(env('FUELINCPAYLATER')))
        {
            $paylater = $request->payment_method;
        }
        else
        {
            $paylater = "false";
        }
        
        $responseCreateBooking = Self::createBooking($user,$pgTxnAmount,$pgTxnId,$pgStatus,$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,null,$paylater,0,false,$booking_platform);
        
        if($responseCreateBooking['status'] == "error")
        {
            return $this->respondWithSuccess($responseCreateBooking['data']);
        }
        elseif($responseCreateBooking['status'] == "validationError")
        {
            return $this->respondWithValidationErrorStatusCode($responseCreateBooking['data']);  
        }
        elseif($responseCreateBooking['status'] == "success")
        {
            return $this->respondWithSuccess($responseCreateBooking['data']);
        }
    }
    
    
    public function paymentWeb(Request $request)
    {
        $payment_success_call  = DB::table('payment_success_calls')->where('payment_id',$request->txnid)->first();

        if(!$payment_success_call)
        {
            $modelId = Session::get('bkmodelId');
            $areaId = Session::get('bkareaId');
            $startDateStr = Session::get('bkStartDate');
            $endDateStr = Session::get('bkEndDate');
            $startTimeStr = Session::get('bkStartTime');
            $endTimeStr = Session::get('bkEndTime');
            $note = $request->note;
            $coupon = (Session::get('bkPromoCode')!== null)?Session::get('bkPromoCode'):"";
            $pgTxnId  = $request->txnid;
            $booking_platform=Session::get('utm_sources');   
            if(!isset($booking_platform))
            {
                $booking_platform="web"; 
            }
            if($pgTxnId == null || $pgTxnId == "")
            {
                $pgTxnAmount = 0;
                $pgStatus = "na";
                $appliedWalletAmount = 0;  
            }
            else
            {
                $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));
                $payment = $api->payment->fetch($pgTxnId);
                $pgTxnAmount = $payment->amount/100;
                $pgStatus = $payment->status;
                $appliedWalletAmount = 0;   
            }
            

            $requestArray = [];
            
            foreach($request->request as $key=>$value)
            {
                array_push($requestArray,[$key=>$value]);
            }

            $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
            $area = Area::where('id', $areaId)->where('status', 1)->first();

            DB::table('payment_success_calls')->insert(['payment_id'=>$request->txnid,'request'=>json_encode($requestArray)." model - ".$modelId." ".$model->bike_model." area_id - ".$areaId." ".$area->area." Start Date - ".$startDateStr." ".$startTimeStr." End Date - ".$endDateStr." ".$endTimeStr." Coupon - ".$coupon]);

            $startDateTimeCarbon  = Carbon::parse($startDateStr." ".$startTimeStr);
            $endDateTimeCarbon  = Carbon::parse($endDateStr." ".$endTimeStr);
            
            $user = UserController::getLoggedInUser();
            
            $creation_mode = "W";
            
            if($request->donated == "yes")
            {
                $donated = true;
            }
            else
            {
                $donated = false;
            }
            
            $responseCreateBooking = Self::createBooking($user,$pgTxnAmount,$pgTxnId,$pgStatus,$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,null,"false",0,$donated,$booking_platform);
            
            if($responseCreateBooking['status'] == "error")
            {
                $message = $responseCreateBooking['data'];  
                return view('front/error', compact('message'));
            }
            elseif($responseCreateBooking['status'] == "validationError")
            {
                $message = $responseCreateBooking['data'];  
                return view('front/error', compact('message'));
            }
            elseif($responseCreateBooking['status'] == "success")
            {
                $readableStart = Self::readableDateTime($startDateTimeCarbon->toDateTimeString());
                $readableEnd = Self::readableDateTime($endDateTimeCarbon->toDateTimeString());
         
                $reservationInfo = $responseCreateBooking['data'];
                $txnid = $reservationInfo->bookingHeader->ref_id;       
                $pricingInfo = $reservationInfo->pricingInfo;
                $amount = $pricingInfo->price_after_tax;
                $modelName = $model->bike_model;
                $makeName = $model->make->bike_make;
                $areaName = $area->area;
                $areaAddress = $area->address;
                $userFname = $user->first_name;
                $referencename = "Booking ID";
                
                return view('front/payment_success', compact('txnid', 'readableStart', 'readableEnd', 'amount', 'modelName', 'makeName', 'areaName', 'areaAddress', 'userFname', 'referencename'));
            }
        }
    }
    
    public function paymentWebPaytm(Request $request)
    {
        if($request->STATUS == "TXN_SUCCESS")
        {
            
            $checkSum = "";
            $payTMParams = array();

            // Create an array having all required parameters for creating checksum.
            $payTMParams["MID"] = env('PAYTM_MERCHANT_MID');
            $payTMParams["ORDER_ID"] = $request->ORDERID;

            $checkSum = getChecksumFromArray($payTMParams,env('PAYTM_MERCHANT_KEY'));
            $PAYTM_DOMAIN = "pguat.paytm.com";
            if (env('PAYTM_STATUS') == "PROD") 
            {
                $PAYTM_DOMAIN = 'secure.paytm.in';
            }
            $paytmTxnStatusUrl = 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/TXNSTATUS?JsonData={%22MID%22:%22'.$payTMParams["MID"].'%22,%22ORDERID%22:%22'.$payTMParams["ORDER_ID"].'%22}';
            $response = Curl::to($paytmTxnStatusUrl)->get();
            
            $responseDec = json_decode($response);
            
            $payment_success_call  = DB::table('payment_success_calls')->where('payment_id',$request->TXNID)->first();

            if(!$payment_success_call)
            {
                $paytmOrder = PaytmOrderDetails::where('id',$responseDec->ORDERID)->first();
                $modelId = $paytmOrder->model_id;
                $areaId = $paytmOrder->area_id;
                $startDateTimeStr = $paytmOrder->start_datetime;
                $endDateTimeStr = $paytmOrder->end_datetime;
                $note = ($paytmOrder->note !== null)?$paytmOrder->note:"";
                $coupon = ($paytmOrder->promocode !== null)?$paytmOrder->promocode:"";
                $pgTxnId  = $responseDec->TXNID;

                $pgTxnAmount = $responseDec->TXNAMOUNT;
                $pgStatus = $responseDec->STATUS;
                $appliedWalletAmount = 0;


                $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
                $area = Area::where('id', $areaId)->where('status', 1)->first();

                DB::table('payment_success_calls')->insert(['payment_id'=>$responseDec->TXNID,'request'=>$response." model - ".$modelId." ".$model->bike_model." area_id - ".$areaId." ".$area->area." Start Date - ".$startDateTimeStr." End Date - ".$endDateTimeStr." Coupon - ".$coupon]);

                $startDateTimeCarbon  = Carbon::parse($startDateTimeStr);
                $endDateTimeCarbon  = Carbon::parse($endDateTimeStr);

                $user = UserController::getLoggedInUser();

                $creation_mode = "W";

                $responseCreateBooking = Self::createBooking($user,$pgTxnAmount,$pgTxnId,$pgStatus,$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,null,"false",0,false,'webpaytm');

                if($responseCreateBooking['status'] == "error")
                {
                    $message = $responseCreateBooking['data'];  
                    return view('front/error', compact('message'));
                }
                elseif($responseCreateBooking['status'] == "validationError")
                {
                    $message = $responseCreateBooking['data'];  
                    return view('front/error', compact('message'));
                }
                elseif($responseCreateBooking['status'] == "success")
                {
                    $readableStart = Self::readableDateTime($startDateTimeCarbon->toDateTimeString());
                    $readableEnd = Self::readableDateTime($endDateTimeCarbon->toDateTimeString());

                    $reservationInfo = $responseCreateBooking['data'];
                    $txnid = $reservationInfo->bookingHeader->ref_id;       
                    $pricingInfo = $reservationInfo->pricingInfo;
                    $amount = $pricingInfo->price_after_tax;
                    $modelName = $model->bike_model;
                    $makeName = $model->make->bike_make;
                    $areaName = $area->area;
                    $areaAddress = $area->address;
                    $userFname = $user->first_name;
                    $referencename = "Booking ID";

                    return view('front/payment_success', compact('txnid', 'readableStart', 'readableEnd', 'amount', 'modelName', 'makeName', 'areaName', 'areaAddress', 'userFname', 'referencename'));
                }
            }
        }
        else
        {
            $message = "Payment failed. Please try again";
            return view('front/error', compact('message'));
        }
    }
    
    
    public function paymentWebZero(Request $request)
    {
        $modelId = $request->model_id;
        $areaId = $request->area_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $note = $request->note;
        $coupon = $request->coupon;
        
        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $area = Area::where('id', $areaId)->where('status', 1)->first();

        $pgTxnId = "na";
        $pgTxnAmount = 0;
        $pgStatus = "Complete";
        $appliedWalletAmount = 0;
            
        $startDateTimeCarbon  = Carbon::parse($startDate." ".$startTime);
        $endDateTimeCarbon  = Carbon::parse($endDate." ".$endTime);

        $user = UserController::getLoggedInUser();

        $creation_mode = "W";

        $responseCreateBooking = Self::createBooking($user,$pgTxnAmount,$pgTxnId,$pgStatus,$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,null,"false",0,false,'webzero');

        if($responseCreateBooking['status'] == "error")
        {
            $message = $responseCreateBooking['data'];  
            return view('front/error', compact('message'));
        }
        elseif($responseCreateBooking['status'] == "validationError")
        {
            $message = $responseCreateBooking['data'];  
            return view('front/error', compact('message'));
        }
        elseif($responseCreateBooking['status'] == "success")
        {
            $readableStart = Self::readableDateTime($startDateTimeCarbon->toDateTimeString());
            $readableEnd = Self::readableDateTime($endDateTimeCarbon->toDateTimeString());

            $reservationInfo = $responseCreateBooking['data'];
            $txnid = $reservationInfo->bookingHeader->ref_id;       
            $pricingInfo = $reservationInfo->pricingInfo;
            $amount = $pricingInfo->price_after_tax;
            $modelName = $model->bike_model;
            $makeName = $model->make->bike_make;
            $areaName = $area->area;
            $areaAddress = $area->address;
            $userFname = $user->first_name;
            $referencename = "Booking ID";

            return view('front/payment_success', compact('txnid', 'readableStart', 'readableEnd', 'amount', 'modelName', 'makeName', 'areaName', 'areaAddress', 'userFname', 'referencename'));
        }
    }
    
    
    public function paymentErpAdmin(Request $request)
    {
        $rules = array(
            'model' => 'required',
            'area' => 'required',
            'startDate' => 'required',
            'startTime' => 'required',
            'endDate' => 'required',
            'endTime' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($request->source == "ERP")
        {
            $source = "erp";
            $leadSource = "In-Store";
            $creation_mode = "S";
        }
        else
        {
            $source = "admin";
            $leadSource = "Inside-Sales";
            $creation_mode = "C";
        }

        if($validator->fails()){
            return redirect('/'.$source.'/booking')->withErrors($validator->errors());        
        }
        
        if($request->customer == env('RESERVATIONSID'))
        {
            $rules = array(
                'customer_email' => 'email|required',
                'customer_mobile' => 'required',
                'customer_fname' => 'required',
                'customer_lname' => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails())
            {
                return redirect('/'.$source.'/booking')->withErrors($validator->errors());        
            }
            
            $user = User::where('email',$request->customer_email)->first();
            if(!$user)
            {
                $user = Self::createUserAndSendEmail($request->customer_fname, $request->customer_lname, $request->customer_mobile, $request->customer_email);
            }            
        }
        else
        {
            $user = User::where('id',$request->customer)->first();
        }
        
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $startTime = $request->startTime;
        $endTime = $request->endTime;
        
             
        $startDateTimeCarbon  = Carbon::parse($startDate." ".$startTime);
        $endDateTimeCarbon  = Carbon::parse($endDate." ".$endTime);

        
        $no_of_days = $startDateTimeCarbon->diffInDays($endDateTimeCarbon);
        if($no_of_days>90)
        {  
            return redirect('/'.$source.'/booking')->withErrors(['block'=>['Block too long.']]);
        }
        if($startDateTimeCarbon->gte($endDateTimeCarbon))
        {
            return redirect('/'.$source.'/booking')->withErrors(['block'=>['End Date Time has to be greater than Start Date Time.']]);
        }
        
        $modelId = $request->model;
        $areaId = $request->area;
        $area = Area::where('id', $areaId)->where('status', 1)->first();
        $model = BikeModel::where('id',$modelId)->first();
        
        $coupon = $request->coupon;
        $note = $request->customer_note;
        
        $count = count($request->payment_method);
        
        if(($count == 0)&&($coupon != "FREEBOOKING"))
        {
            return redirect('/'.$source.'/booking')->withErrors(['block'=>['Payment Information cannot be blank.']]);
        }
        
        $i = 0;
        $financialInfoFromBackend = [];
        $pgTxnIdArr = [];
        $pgTxnAmount = 0;
        $appliedWalletAmount = 0;
        while($i<$count)
        {
            $financialInfo = [
                'payment_method'=>$request->payment_method[$i],
                'amount'=>$request->amount[$i],
                'payment_id'=>$request->payment_id[$i],
            ];
            if($request->payment_method[$i] != 2)
            {
                $pgTxnAmount+=$request->amount[$i];
            }
            else
            {
                $appliedWalletAmount+=$request->amount[$i];
            }
            array_push($financialInfoFromBackend,$financialInfo);
            array_push($pgTxnIdArr,$request->payment_id[$i]);
            $i++;
        }
        
        if($request->source == "ERP")
        {
            $agreedPrice = $pgTxnAmount+$appliedWalletAmount;
        }
        else
        {
            $agreedPrice = $request->override_price;
        }
        
        $pgTxnId = implode(',',$pgTxnIdArr);
        $responseCreateBooking = Self::createBooking($user,$pgTxnAmount,$pgTxnId,"na",$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,$financialInfoFromBackend,"false",$agreedPrice,false,'erp');
        
        
        if($responseCreateBooking['status'] == "error")
        {
            $message = $responseCreateBooking['data'];    
            return redirect('/'.$source.'/booking')->withErrors([$message]);
        }
        elseif($responseCreateBooking['status'] == "validationError")
        {
            $message = $responseCreateBooking['data'];  
            return redirect('/'.$source.'/booking')->withErrors([$message]);
        }
        elseif($responseCreateBooking['status'] == "success")
        {
            return redirect('/'.$source.'/booking')->withErrors(['Booking has been made successfully Reference ID: '.$responseCreateBooking['data']['reference_id']]);
        }
    }
    
    public function createServiceBlock(Request $request)
    {
        $user = User::where('id',env('SERVICEID'))->first();
        $model = BikeModel::where('id',$request->model_id)->first();
        $area = Area::where('id',$request->area_id)->first();
        
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        
             
        $startDateTimeCarbon  = Carbon::parse($startDate." ".$startTime);
        $endDateTimeCarbon  = Carbon::parse($endDate." ".$endTime);

        $note = $request->note;
        $financialInfo = [];
        
        $response = Self::createBooking($user,0,"","",0,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,"","S",$note,$financialInfo,"false",0,false,'serviceblock');
        
        if(isset($response['data']['status']))
        {
            return ['error'=>$response];
        }
        return ['id'=>$response['data']['id']];
    }
    
    
    public static function createBooking($user,$pgTxnAmount,$pgTxnId,$pgStatus,$appliedWalletAmount,$model,$area,$startDateTimeCarbon,$endDateTimeCarbon,$coupon,$creation_mode,$note,$financialInfoFromBackend,$paylater,$agreedPrice,$donated,$booking_platform)
    {
        if($donated)
        {
            $pgTxnAmount = $pgTxnAmount-1;
        }
        
        $readableStart = Self::readableDateTime($startDateTimeCarbon->toDateTimeString());
        $readableEnd = Self::readableDateTime($endDateTimeCarbon->toDateTimeString());
            
        
        $paymentStr = $pgTxnAmount." with payment ID - ".$pgTxnId.(($appliedWalletAmount>0)?(" Wallet Balance of Rs.".$appliedWalletAmount." has been applied."):"");
        
        if(!$user)
        {
            $message = "Sorry, your session has expired. We regret the inconvenience caused and will be contacting you to remedy it shortly. You can also reach us at ".env('CUSTOMER_CARE')." or call us at 08046801054";
            
            $dataAdmin = [
                'subject'=>"Booking failed for App Payment ID ".$pgTxnId,
                'reason'=>"User session logged out",
                'details'=>$model->bike_model." - ".$area->area." - ".$readableStart." to ".$readableEnd.". Has made payment of Rs.".$paymentStr
            ];
            
            Self::sendProblemMailToAdmin($dataAdmin);
            
            if($creation_mode == "A" || $creation_mode == "I")
            {
                return ['status'=>"error",'data'=>$message];
            }
            elseif($creation_mode == "W")
            {
                return ['status'=>"error",'data'=>$message];
            }
            
        }
        
        if($pgTxnAmount>0 && !($creation_mode == "S"||$creation_mode == "C"))
        {
            $bookingForPaymentId = FinancialInfo::where('payment_id',$pgTxnId)->first();
            if(!(!$bookingForPaymentId))
            {
                $bookingHeader = BookingHeader::where('id',$bookingForPaymentId->booking_id)->first();
                $data = [
                    'id'=>$bookingForPaymentId->booking_id,
                    'reference_id'=>$bookingHeader->ref_id,
                    'instructions'=>""
                ];

                if($creation_mode == "A" || $creation_mode == "I")
                {
                    return ['status'=>"error",'data'=>$data];
                }
                elseif($creation_mode == "W")
                {
                    return ['status'=>"error",'data'=>$data];
                }
                elseif($creation_mode == "C" || $creation_mode == "S")
                {
                    return ['status'=>"error",'data'=>$data];
                }
            }
        }
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $model->id, 
            'area_id' => $area->id, 
            'start_datetime' => $startDateTimeCarbon->toDateTimeString(), 
            'end_datetime' => $endDateTimeCarbon->toDateTimeString(),
            'coupon' => $coupon
        ])->get('bookings/price-and-check-availability');
        
        if(!$bikeAvailabilityWithPrice['bike_availability'])
        {
            $message = "We are sorry, your booking attempt failed because bike has been booked by someone while the payment being made. We regret the inconvenience caused and will be contacting you to remedy it shortly. You can also reach us at ".env('CUSTOMER_CARE')." or call us at 08046801054";
            
           
            $dataAdmin = [
                'subject'=>"Booking failed for App Payment ID ".$pgTxnId,
                'reason'=>"Bike blocked before the payment was made.",
                'details'=>$model->bike_model." - ".$area->area." - ".$readableStart." to ".$readableEnd.". Has made payment of Rs.".$paymentStr." The user is ".$user->first_name." - ".$user->email." - ".$user->mobile_num
            ];
            
            Self::sendProblemMailToAdmin($dataAdmin);
            
            $dataUser = [
                'email'=>$user->email,
                'subject'=>"Booking failed due to unavailability.",
                'fname'=>$user->first_name,
                'email_body'=>$message
            ];
            
            Self::sendProblemMailToUser($dataUser);
            
            if($creation_mode == "A" || $creation_mode == "I")
            {
                return ['status'=>"error",'data'=>$message];
            }
            elseif($creation_mode == "W")
            {
                return ['status'=>"error",'data'=>$message];
            }
            elseif($creation_mode == "C" || $creation_mode == "S")
            {
                return ['status'=>"error",'data'=>$message];
            }
        }
        
        $totalHours = $bikeAvailabilityWithPrice['number_of_hours'];
        
        if($creation_mode == "C" || $creation_mode == "S")
        {
            $paymentSatisfactory = true;    
        }
        else
        {
            $paymentSatisfactory = Self::doesPriceAndPaymentMatch($user->id,$bikeAvailabilityWithPrice['total_price'],$pgTxnAmount,$appliedWalletAmount,$area->id,$totalHours);    
        }
        
        if($paymentSatisfactory)
        {
            $fuelIncluded  = Self::isFuelIncluded($area->id,$totalHours);
            
            $reservation_details = array([
                'bike_or_accessory'=>"B",
                'model_id' => $model->id, 
                'area_id' => $area->id,
                'start_datetime'=>$startDateTimeCarbon->toDateTimeString(),
                'end_datetime'=>$endDateTimeCarbon->toDateTimeString(),
                'fuel_included'=>$fuelIncluded
            ]);
            
            
            
            if($creation_mode == "S"||$creation_mode == "C")
            {
                $financialInfoArray = [];
                foreach($financialInfoFromBackend as $financialInfo)
                {
                    $financialInfo['payment_status'] = "Paid";
                    array_push($financialInfoArray,$financialInfo);
                }
            }
            else
            {
                if(env('WEBGATEWAY') == "PAYTM")
                {
                    if($creation_mode == "W")
                        $razorPayOrPaytm = 4;
                    else
                        $razorPayOrPaytm = 3;
                }
                else
                {
                    $razorPayOrPaytm = 3;
                }
                
                
                $financialInfoArray = [];
                if($paylater == "false")
                {
                    if($pgTxnAmount > 0)
                    {
                        $financialInfo = [
                            'payment_method'=>$razorPayOrPaytm,
                            'amount'=>$pgTxnAmount,
                            'payment_id'=>$pgTxnId,
                            'payment_status'=>$pgStatus
                        ];

                        array_push($financialInfoArray,$financialInfo);
                    }
                    if($appliedWalletAmount > 0)
                    {
                        $financialInfo = [
                            'payment_method'=>2,
                            'amount'=>$appliedWalletAmount,
                            'payment_id'=>"NA",
                            'payment_status'=>"Paid"
                        ];

                        array_push($financialInfoArray,$financialInfo);
                    }
                }
            }
                                
            $reservationResponse = ReservationController::createBooking($user,$creation_mode,$coupon,$reservation_details,$financialInfoArray,$note,$agreedPrice,$booking_platform);
            
            if(!array_key_exists('bookingId',$reservationResponse))
            {
                if($creation_mode == "A" || $creation_mode == "I")
                {
                    return ['status'=>"validationError",'data'=>$reservationResponse];
                }
                elseif($creation_mode == "W")
                {
                    return ['status'=>"validationError",'data'=>$reservationResponse];
                }
                elseif($creation_mode == "C" || $creation_mode == "S")
                {
                    return ['status'=>"validationError",'data'=>$reservationResponse];
                }
            }
            
            if(count($reservationResponse['successfulReservations'])>0)
            {
                $reservationInfo = ReservationInfo::where('id',$reservationResponse['successfulReservations'][0]['id'])->first();
            }
            
            $response = BikeAvailabilityController::addOrDeleteReservation($reservationInfo->id,"add");
            
            if($response == "done")
            {
                if($user->id != env('SERVICEID'))
                {
                    if($pgTxnAmount>0 && !($creation_mode == "S"||$creation_mode == "C"))
                    {
                        Self::cashback($pgTxnAmount,$user->email,$reservationInfo->id); 
                        
                        if($donated)
                        {
                            $captureAmount = $pgTxnAmount+1;
                            
                            $financialInfo = FinancialInfo::create([
                                'booking_id'=>$reservationInfo->booking_id,
                                'user_id'=>$user->id,
                                'debit_or_credit'=>"D",
                                'new_or_edit'=>"N",
                                'amount'=>1,
                                'payment_method'=>0,
                                'payment_id'=>0,
                                'payment_nature'=>39,
                                'status'=>"na",
                                'created_by'=>$user->id
                            ]);
                            $financialInfo = FinancialInfo::create([
                                'booking_id'=>$reservationInfo->booking_id,
                                'user_id'=>$user->id,
                                'debit_or_credit'=>"C",
                                'new_or_edit'=>"N",
                                'amount'=>1,
                                'payment_method'=>3,
                                'payment_id'=>$pgTxnId,
                                'payment_nature'=>40,
                                'status'=>"paid",
                                'created_by'=>$user->id
                            ]);
                        }
                        else
                        {
                            $captureAmount = $pgTxnAmount;
                        }                        
                        
                    }
                }
                
                $pgMethodArr = [];
                
                foreach($reservationInfo->getCreditNonReversedFinancialInfo() as $financialInfo)
                {
                    array_push($pgMethodArr,$financialInfo->paymentMethodName->method);
                }
                
                $pgMethod = implode(',',$pgMethodArr);
                
                if($paylater!="false")
                {
                    $pgMethod = $paylater;
                }
                
                Self::createOldBooking($reservationInfo,$pgMethod,$pgTxnId,$pgTxnAmount,$appliedWalletAmount);

                if($user->id != env('SERVICEID'))
                {
                    Self::sendSuccessEmailToUserAndAdmin($reservationInfo);
                    Self::sendKiranaSMS($reservationInfo);
                     if($pgTxnAmount>0 && !($creation_mode == "S"||$creation_mode == "C"))
                    {
                        if(($paylater == "false") && ($razorPayOrPaytm == 3))
                        {
                            Self::captureRazorPay($reservationInfo->id,$pgTxnId,$captureAmount);
                        }

                    }

                }


            }
            
            $data = [
                'id'=>$reservationInfo->id,
                'reference_id'=>$reservationInfo->bookingHeader->ref_id,
                'instructions'=>""
            ];
            
            if($creation_mode == "A" || $creation_mode == "I")
            {
                return ['status'=>"success",'data'=>$data];
            }
            elseif($creation_mode == "W")
            {
                return ['status'=>"success",'data'=>$reservationInfo];
            }
            elseif($creation_mode == "C" || $creation_mode == "S")
            {
                return ['status'=>"success",'data'=>$data];
            }

        }
        else
        {
            $message = "We are sorry, your booking attempt failed because of price mismatch. We will get in touch with you soon to get this rectified. We also request you to update the app to avoid any inconvenience. You can also reach us at ".env('CUSTOMER_CARE')." or call us at 08046801054";
            
            $dataAdmin = [
                'subject'=>"Booking failed for App Payment ID ".$pgTxnId,
                'reason'=>"Price mismatch",
                'details'=>$model->bike_model." - ".$area->area." - ".$readableStart." to ".$readableEnd.". ".$user->first_name." ".$user->last_name." ".$user->email." - ".$user->mobile_num." - "." Has made payment of Rs.".$paymentStr
            ];
            
            Self::sendProblemMailToAdmin($dataAdmin);
            
            $dataUser = [
                'email'=>$user->email,
                'subject'=>"Booking failed due to tax mismatch.",
                'fname'=>$user->first_name,
                'email_body'=>$message
            ];
            
            Self::sendProblemMailToUser($dataUser);
            
            if($creation_mode == "A" || $creation_mode == "I")
            {
                return ['status'=>"error",'data'=>$message];
            }
            elseif($creation_mode == "W")
            {
                return ['status'=>"error",'data'=>$message];
            }
            elseif($creation_mode == "C" || $creation_mode == "S")
            {
                return ['status'=>"error",'data'=>$message];
            }
        }
    }
    
    
    public function editReservationApi(Request $request)
    {
        $rules = array(
            'reservation_id' => 'required|exists:reservation_info,id',
            'edited_model_id' => 'required_without:edited_accessory_model_id,edited_accessory_size_id|exists:bike_models,id',
            'edited_area_id' => 'required|exists:areas,id',
            'edited_accessory_model_id' => 'required_without:edited_model_id|exists:accessories_model,id',
            'edited_accessory_size_id' => 'required_without:edited_model_id|exists:accessories_size,id',
            'edited_invoice_amount' => 'required_if:bike_or_accessory,A',
            'creation_mode' => 'required',
            'edited_start_datetime' => 'required|date',
            'edited_end_datetime' => 'required|date|after:start_datetime',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        if(isset($request->user_id))
        {
            $user = User::where('id',$request->user_id)->first();
        }
        else
        {
            $user = UserController::getUserByToken();
        }
        
        $creation_mode = $request->creation_mode;
        $reservation_id = $request->reservation_id;
        $edited_start_datetime = $request->edited_start_datetime;
        $edited_end_datetime = $request->edited_end_datetime;
        $edited_model_id = $request->edited_model_id;
        $edited_area_id = $request->edited_area_id;
        $edited_fuel_included = $request->edited_fuel_included;
        $creation_reason = $request->creation_reason;
        $edited_invoice_amount = $request->edited_invoice_amount;
        $coupon = $request->coupon;
        $count = count($request->payment_method);
        $i = 0;
        $financialInfoFromBackend = [];
        $pgTxnAmount = 0;
        $appliedWalletAmount = 0;
        while($i<$count)
        {
            $financialInfo = [
                'payment_method'=>$request->payment_method[$i],
                'amount'=>$request->amount[$i],
                'payment_id'=>$request->payment_id[$i],
            ];
            if($request->payment_method[$i] != 2)
            {
                $pgTxnAmount+=$request->amount[$i];
            }
            else
            {
                $appliedWalletAmount+=$request->amount[$i];
            }
            array_push($financialInfoFromBackend,$financialInfo);
            $i++;
        }
        
        $notes = $request->edit_note;
        
        $response = ReservationController::editReservation($user,$creation_mode,$reservation_id,$edited_start_datetime,$edited_end_datetime,$edited_model_id,$edited_area_id,$edited_fuel_included,$creation_reason,$edited_invoice_amount,$coupon,$financialInfoFromBackend,$notes);
        
        
        
    }
    
    public function editReservationApiLegacy($id,Request $request)
    {
        $rules = array(
            'model' => 'required',
            'area' => 'required',
            'startDate' => 'required',
            'startTime' => 'required',
            'endDate' => 'required',
            'endTime' => 'required',
            'reason_for_editing' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());        
        }
        
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $startTime = $request->startTime;
        $endTime = $request->endTime;

        
        if((Carbon::parse($startDate." ".$startTime)->diffInHours(Carbon::parse($endDate." ".$endTime)))<1)
        {
            return redirect()->back()->withErrors(['block'=>['End Date Time has to be greater than Start Date Time.']]);
        }
        
        if(($request->override_price == 0)&&($request->coupon != "FREEBOOKING"))
        {
            return redirect()->back()->withErrors(['block'=>['Payment Information cannot be blank.']]);
        }
                
        $existingReservation = ReservationInfo::where('id',$id)->where('deleted',0)->orderBy('id','desc')->first();
        
        $user = User::where('id',$existingReservation->user_id)->first();
                  
        $creation_mode = "C";
        $reservation_id = $id;
        $edited_start_datetime = $startDate." ".$startTime;
        $edited_end_datetime = $endDate." ".$endTime;
        $edited_model_id = $request->model;
        $edited_area_id = $request->area;
        $creation_reason = $request->reason_for_editing;
        $edited_invoice_amount = 0;
        $promo_code = $request->coupon;
        
        $count = count($request->payment_method);
        $i = 0;
        $financialInfoFromBackend = [];
        $pgTxnAmount = 0;
        $appliedWalletAmount = 0;
        while($i<$count)
        {
            $financialInfo = [
                'payment_method'=>$request->payment_method[$i],
                'amount'=>$request->amount[$i],
                'payment_id'=>$request->payment_id[$i],
                'payment_status'=>"Paid"
            ];
            if($request->payment_method[$i] != 2)
            {
                $pgTxnAmount+=$request->amount[$i];
            }
            else
            {
                $appliedWalletAmount+=$request->amount[$i];
            }
            array_push($financialInfoFromBackend,$financialInfo);
            $i++;
        }
        
        $notes = $request->notes;
        
        $responseEditBooking = ReservationController::editReservation($user,$creation_mode,$reservation_id,$edited_start_datetime,$edited_end_datetime,$edited_model_id,$edited_area_id,$creation_reason,$edited_invoice_amount,$promo_code,$financialInfoFromBackend,$notes,$request->override_price);
        
        if($responseEditBooking['status'] == "error")
        {
            $message = $responseEditBooking['data'];    
            return redirect('/admin/booking')->withErrors([$message]);
        }
        elseif($responseEditBooking['status'] == "success")
        {
            $pgMethodArr = [];
            $pgTxnIdArr = [];
            $pgTxnAmount = 0;
            
            $reservationInfo = ReservationInfo::where('id',$responseEditBooking['data']['new_reservation_id'])->first();
                
            foreach($reservationInfo->getCreditNonReversedFinancialInfo() as $financialInfo)
            {
                array_push($pgMethodArr,$financialInfo->paymentMethodName->method);
                array_push($pgTxnIdArr,$financialInfo->payment_id);
                if(!($financialInfo->payment_method == 12 || $financialInfo->payment_method == 13))
                    $pgTxnAmount += $financialInfo->amount;
            }

            $pgMethod = implode(',',$pgMethodArr);
            $pgTxnId = implode(',',$pgTxnIdArr);

            Self::editOldBooking($reservationInfo,$pgMethod,$pgTxnId,$pgTxnAmount,$appliedWalletAmount,$id);
            
            $response = BikeAvailabilityController::addOrDeleteReservation($reservationInfo->id,"add");
            if($response == "done")
            {
                Self::sendKiranaSMS($reservationInfo);
                if($request->notify_user == "yes")
                {
                    Self::sendSuccessEmailToUserAndAdmin($reservationInfo);
                }

                return redirect('/admin/booking')->withErrors(['Booking has been edited successfully Reference ID: '.$responseEditBooking['data']['new_reservation_id']]);
            }
            
        }
        
    }
    
    public function deleteReservationApi(Request $request)
    {
        $rules = array(
            'reservation_id' => 'required|exists:reservation_info,id',
            'creation_mode' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
    }
    
    public function deleteReservationApiLegacy($booking_id, Request $request)
    {
        $user_id = Auth::user()->id;
        $creation_mode = "C";
        $refund_financial_info = [];
        
        $response = ReservationController::deleteReservation($user_id,$creation_mode,$booking_id,$refund_financial_info,$request->reason);
        
        BikeAvailabilityController::addOrDeleteReservation($booking_id,"delete");
        
        Self::deleteOldBooking($booking_id,$request->reason);
        
        return redirect('/admin/booking')->withErrors($response['data']);
    }
    
    
    public static function sendSuccessEmailToUserAndAdmin($reservationInfo)
    {
        
        $dontSendList = explode(',',Parameters::getParameter('dont_send_email_ids'));
        
        if(in_array($reservationInfo->user_id,$dontSendList))
        {
            return "done";
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        $notes = $reservationInfo->notes()->where('nature',"User creation notes")->first();
        
        if(!$notes)
            $notesStr = "";
        else
            $notesStr = $notes->text;
        
    
        $walletAmount = $reservationInfo->getAppliedWalletAmount();
        
        $data = [
            'readable_start_datetime'=>$reservationInfo->readableStartDateTime(),
            'readable_end_datetime'=>$reservationInfo->readableEndDateTime(),
            'areaName' => $reservationInfo->area->area,
            'price_after_discount'=>$reservationInfo->pricingInfo->price_after_discount,
            'tax'=>$reservationInfo->pricingInfo->tax_amount,
            'price_with_tax'=>$reservationInfo->pricingInfo->price_after_tax,
            'wallet_amount'=>$walletAmount,
            'duration' => $reservationInfo->getDuration(), 
            'areaAddress' => $reservationInfo->area->address, 
            'userFname' => $reservationInfo->user->first_name, 
            'modelName' => $reservationInfo->model->bike_model, 
            'modelThumbnail' => $reservationInfo->model->getThumbnail(), 
            'userEmail' => $reservationInfo->user->email, 
            'userMob' => $reservationInfo->user->mobile_num, 
            'note' => $notesStr, 
            'bookingId' => $reservationInfo->bookingHeader->ref_id, 
            'kmLimitMessage'=>$reservationInfo->KMmessage(), 
            'areaMapLink'=>$reservationInfo->area->maps_link,
            'tax_name' => $taxDetails->tax_name,
            'tax_amount' => $taxDetails->amount,
            'amount'=>$reservationInfo->pricingInfo->price_after_tax,
            'manualPresent'=>($reservationInfo->model->manual_url==""?false:true),
            'manualFile'=>$reservationInfo->model->getManualFile()
        ];

        Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
            $message->to($data['userEmail']);
            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
            $message->subject(env('SITENAMECAP').' Booking - '.$data['bookingId']);
            if($data['manualPresent'])
                $message->attach($data['manualFile']);
        });
        
        Mail::send('emails.admin_booking_mail',$data,function($message) use ($data) {
            $message->to(env('ADMIN_EMAIL'));
            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
            $message->subject(env('SITENAMECAP').' '.$data['userFname'].' Booking - '.$data['bookingId']);
        });
        
        return "done";
    }
    
    public static function sendKiranaSMS($reservationInfo)
    {
        $kiranaAreaRelations = DB::table('kirana_user_area_relation')->where('area',$reservationInfo->area_id)->get();
        if(count($kiranaAreaRelations) != 0)
        {
            $smsGatewayUrl = env('SMSGATEWAYURL');
            $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
            $smsSenderId = env('SMSSENDERID');


            $smsMessage = "Booking ID ".$reservationInfo->bookingHeader->ref_id." created for your store. The user is ".$reservationInfo->user->first_name." ".$reservationInfo->user->last_name." Ph - ".$reservationInfo->user->mobile_num." The booking is for ".$reservationInfo->model->bike_model." at ".$reservationInfo->area->area." from ".$reservationInfo->readableStartDateTime()." to ".$reservationInfo->readableEndDateTime();

            foreach($kiranaAreaRelations as $kiranaAreaRelation)
            {
                $kiranaUser = User::where('id',$kiranaAreaRelation->user)->first();
                if(!$kiranaUser)
                {
                }
                else
                {
                    $phone_number = str_replace("+","",$kiranaUser->mobile_num);
                    $phone_number = str_replace(" ","",$phone_number);
                    $phone_number = str_replace("-","",$phone_number);
                    $phone_number = str_replace("(","",$phone_number);
                    $phone_number = str_replace(")","",$phone_number);

                    if(env('TESTSMS')!=true)
                    {
                        $response = Curl::to($smsGatewayUrl)->withData(array(
                            'authkey' => $smsGatewayAPIKey, 
                            'mobiles'=>$phone_number, 
                            'message'=>$smsMessage, 
                            'sender'=>$smsSenderId, 
                            'route'=>4, 
                            'country'=>91, 
                            'response'=>'json'
                        ))->get();
                    }
                }
            }
        }
        
        return "done";
    }
    
    public static function createUserAndSendEmail($firstName, $lastName, $mobile, $email)
    {
        $newPassword = Str::random(8);
        $password = password_hash($newPassword, PASSWORD_DEFAULT);

        $fname = ucwords(strtolower($firstName));
        $lname = ucwords(strtolower($lastName));
        $length = 5;
        $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);

        $user = User::create([
            'first_name' => $fname, 
            'last_name' => $lname, 
            'mobile_num' => $mobile, 
            'email' => strtolower($email), 
            'password' => $password, 
            'referral_code'=>$referral_code
        ]);

        $data = [
            'fname'=>$firstName,
            'lname'=>$lastName, 
            'email'=>$email, 
            'mob'=>$mobile, 
            'password'=>$newPassword
        ];

        Mail::send('emails.user_signup_mail_admin',$data,function($message) use($data){
            $message->to($data['email']);
            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
            $message->subject(env('SITENAMECAP').' SignUp');
        });

        Mail::send('emails.admin_signup_mail',$data,function($message){
            $message->to(env('ADMIN_EMAIL'));
            $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
            $message->subject(env('SITENAMECAP').' User SignUp');
        });

        return $user;
    }
    
    public static function doesPriceAndPaymentMatch($userId,$price,$pgAmount,$walletAmount,$area_id,$totalHours)
    {

        if((in_array($area_id,explode(",",env('AREAINCLUDEDFUEL')))) && ($totalHours < env('FUELINCLUDEDMAXHOURS')) && env('FUELINCPAYLATER'))
        {
            return true;
        }
        elseif(in_array($userId,explode(',',env('NONREVENUEIDS'))))
        {
            return true;
        }
        else
        {
            $data = WalletController::checkAppliedWalletAmount("AtoA",$price,$userId,$walletAmount);
            
            $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
            
            
            $taxablePrice = $price - $data['promotional_credit_used'];
            $tax = $taxablePrice*($taxDetails->amount/100);
            
            $priceWithTax = $taxablePrice + $tax;
            
            if(($pgAmount+$data['applied_amount']+2) > $priceWithTax)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    
    public static function cashback($pg_txn_amount,$userEmail,$reservationInfoId)
    {
        $cashbackPct = env('CASHBACKPCT');
        $cashback = $pg_txn_amount*$cashbackPct/100;

        if($cashback>0)
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $addWalletBalance = $dispatcher->with([
                'user_email_add' => $userEmail,
                'amount_add' => $cashback,
                'booking_id_add'=>$reservationInfoId,
                'nature_add' => 'Cashback for booking',
                'notes_add' => $cashbackPct."% cashback for all bookings."
            ])->get('wallet/add_amount_int_new');
        }
    }
    
    public static function sendProblemMailToUser($dataUser)
    {
        Mail::send('emails.user_gst_problem_mail',$dataUser,function($message) use ($dataUser) {
              $message->to($dataUser['email']);
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject(env('SITENAMECAP').' '.$dataUser['subject']);
        });
    }
    
    public static function sendProblemMailToAdmin($dataAdmin)
    {        
        $errorinfo = ErrorInfo::create(['subject' => $dataAdmin['subject'], 'reason' => $dataAdmin['reason'],'type'=>'hourly rental','data' => $dataAdmin['details']]);

        Mail::send('emails.admin_booking_problem_mail',$dataAdmin,function($message) use ($dataAdmin) {
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject($dataAdmin['subject']);
        });
    }
    
    public static function isFuelIncluded($area_id,$totalHours)
    {

        if((in_array($area_id,explode(",",env('AREAINCLUDEDFUEL')))) && ($totalHours < env('FUELINCLUDEDMAXHOURS')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static function captureRazorPay($reservationInfoId,$pgTxnId,$amount)
    {
        
        $parameters =   [
            'txnid'=>$pgTxnId,
            'booking_id'=>$reservationInfoId,
            'amount'=>$amount
        ];

        $parametersEn = json_encode($parameters);

        $job = new RazorPayCapture($parametersEn);
        
        Bus::dispatch($job);
                
    }
    
    public static function createOldBooking($reservationInfo,$pgMethod,$pgTxnId,$pgAmount,$appliedWalletAmount)
    {
        if(in_array($reservationInfo->creation_mode,explode(',',env('DIRECTCREATIONMODES'))))
        {
            $assisted = 0;
        }
        else
        {
            $assisted = 1;
        }
        
        if($reservationInfo->creation_mode == "S")
        {
            $leadSource = "In-Store";
        }
        elseif($reservationInfo->creation_mode == "C")
        {
            $leadSource = "Inside-Sales";
        }
        else
        {
            $leadSource = null;
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
            
        $fullPrice = $reservationInfo->pricingInfo->full_price*(1+($taxDetails->amount/100));
        
        Booking::create([
            'id'=>$reservationInfo->id, 
            'start_datetime'=>$reservationInfo->start_datetime, 
            'end_datetime'=>$reservationInfo->end_datetime, 
            'user_id'=>$reservationInfo->user_id, 
            'bike_id'=>1, 
            'note'=>(count($reservationInfo->bookingHeader->notes)>0)?($reservationInfo->bookingHeader->notes()->first()->text):"", 
            'total_price'=>$reservationInfo->pricingInfo->price_after_tax, 
            'payment_method'=>$pgMethod, 
            'promocode'=>$reservationInfo->pricingInfo->promo_code, 
            'status'=>"COMPLETE", 
            'pg_txn_id'=>$pgTxnId,
            'pg_txn_amount'=>$pgAmount, 
            'reference_id'=>$reservationInfo->bookingHeader->ref_id, 
            'createdBy'=>$reservationInfo->created_by, 
            'active'=>1, 
            'KMlimit'=>$reservationInfo->KMlimit, 
            'weekday_hours'=>$reservationInfo->weekday_hours, 
            'weekend_hours'=>$reservationInfo->weekend_hours, 
            'total_hours'=>$reservationInfo->total_hours,  
            'area_id'=>$reservationInfo->area_id, 
            'model_id'=>$reservationInfo->model_id, 
            'booking_status'=>1, 
            'full_price'=>$fullPrice, 
            'res_lead_time'=>$reservationInfo->res_lead_time, 
            'assisted'=>$assisted, 
            'lead_source'=>$leadSource, 
            'wallet_amount_applied'=>$appliedWalletAmount, 
            'fuel_included'=>$reservationInfo->fuel_included,
            'delivered_at_area'=>$reservationInfo->area_id 
        ]);
    }
    
    
    public static function editOldBooking($reservationInfo,$pgMethod,$pgTxnId,$pgAmount,$appliedWalletAmount,$id)
    {
        if(in_array($reservationInfo->creation_mode,explode(',',env('DIRECTCREATIONMODES'))))
        {
            $assisted = 0;
        }
        else
        {
            $assisted = 1;
        }
    
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
            
        $fullPrice = $reservationInfo->pricingInfo->full_price*(1+($taxDetails->amount/100));
        
        
        Booking::where('id',$id)->update([
            'id'=>$reservationInfo->id, 
            'start_datetime'=>$reservationInfo->start_datetime, 
            'end_datetime'=>$reservationInfo->end_datetime, 
            'user_id'=>$reservationInfo->user_id, 
            'bike_id'=>1, 
            'note'=>(count($reservationInfo->notes)>0)?($reservationInfo->notes()->first()->text):"", 
            'total_price'=>$reservationInfo->pricingInfo->price_after_tax, 
            'payment_method'=>$pgMethod, 
            'promocode'=>$reservationInfo->pricingInfo->promo_code, 
            'status'=>"COMPLETE", 
            'pg_txn_id'=>$pgTxnId,
            'pg_txn_amount'=>$pgAmount, 
            'reference_id'=>$reservationInfo->bookingHeader->ref_id, 
            'active'=>1, 
            'KMlimit'=>$reservationInfo->KMlimit, 
            'weekday_hours'=>$reservationInfo->weekday_hours, 
            'weekend_hours'=>$reservationInfo->weekend_hours, 
            'total_hours'=>$reservationInfo->total_hours,  
            'area_id'=>$reservationInfo->area_id, 
            'model_id'=>$reservationInfo->model_id, 
            'booking_status'=>1, 
            'full_price'=>$fullPrice, 
            'res_lead_time'=>$reservationInfo->res_lead_time, 
            'assisted'=>$assisted, 
            'wallet_amount_applied'=>$appliedWalletAmount, 
            'fuel_included'=>$reservationInfo->fuel_included,
            'delivered_at_area'=>$reservationInfo->area_id
        ]);
    }
    
    public static function deleteOldBooking($booking_id,$reason)
    {
        $bookingParse=Booking::where('id',$booking_id)->first();
    
        $bookingParse['reason_for_delete'] = $reason;
        $bookingParse['deleted_by'] = Auth::user()->id;
        $bookingParse['deleted_at'] = date('Y-m-d H:i:s',strtotime("now"));

        $deleted_booking = DB::table('deleted_bookings')->where('id',$booking_id)->first();
        if(!$deleted_booking)
        {
            DB::table('deleted_bookings')->insert($bookingParse->toArray());  
            Booking::where('id',$booking_id)->update(['booking_status'=>0]);

            Booking::destroy($booking_id);
        }
        
        return "done";
    
    }
    
    public static function readableDateTime($datetime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($datetime)->format('jS M-y ga');
        }
        return Carbon::parse($datetime)->format('jS M ga');
    }
}
