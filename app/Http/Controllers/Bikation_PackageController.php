<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Package;
use Session;


class Bikation_PackageController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->orderBy('Trip_ID','desc')->get();
	   $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Vendor_ID',Session::get('Vendor_ID'))->orderBy('Trip_ID','desc')->get();
	   $selected="";
      if (isset($request->btn_submit)) {
         $bikation_packageList = Bikation_Package::where('Trip_ID',$request->Trip_ID)->orderBy('Trip_ID','desc')->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_packageList = Bikation_Package::where('Vendor_ID',Session::get('Vendor_ID'))->orderBy('Trip_ID','desc')->get();
        $selected="";
      }
      //$bikation_packageList = Bikation_Package::all();
      foreach ($bikation_packageList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      return view('bikationvendor.front.package_index',compact('bikation_packageList','bikation_tripList','bikation_AlltripList','selected'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addPackage(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array('Trip_ID' => 'required',
              'name' => 'required',
              'Description' => 'required',
              'No_of_Tickets_Cost_1px' => 'required',
              'Tickes_Remaining_Cost_1px' => 'required',
              'Cost_1px' => 'required',
              'No_of_Tickets_Cost_2px' => 'required',
              'Tickes_Remaining_Cost_2px' => 'required',
              'Cost_2px'=> 'required',
              'Status' => 'required'
            );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-package')->withErrors($validator->errors());
      }else{
        $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Package::create(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'name' => $request->name,'Description' => $request->Description, 'No_of_Tickets_Cost_1px' => $request->No_of_Tickets_Cost_1px,'Tickes_Remaining_Cost_1px' => $request->Tickes_Remaining_Cost_1px,'Cost_1px' => $request->Cost_1px,'No_of_Tickets_Cost_2px'=>$request->No_of_Tickets_Cost_2px,'Tickes_Remaining_Cost_2px'=>$request->Tickes_Remaining_Cost_2px,'Cost_2px'=>$request->Cost_2px,'rental_bike_cost'=>$request->rental_bike_cost,'Status'=>$request->Status]);
        return redirect('bikation-package');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($packageId, Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
       $rules = array('Trip_ID' => 'required',
              'name' => 'required',
              'Description' => 'required',
              'No_of_Tickets_Cost_1px' => 'required',
              'Tickes_Remaining_Cost_1px' => 'required',
              'Cost_1px' => 'required',
              'No_of_Tickets_Cost_2px' => 'required',
              'Tickes_Remaining_Cost_2px' => 'required',
              'Cost_2px'=> 'required',
              'Status' => 'required'
            );
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-package')->withErrors($validator->errors());
      }
      else{
        $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Package::where('Package_ID', $packageId)
        ->update(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'name' => $request->name,'Description' => $request->Description, 'No_of_Tickets_Cost_1px' => $request->No_of_Tickets_Cost_1px,'Tickes_Remaining_Cost_1px' => $request->Tickes_Remaining_Cost_1px,'Cost_1px' => $request->Cost_1px,'No_of_Tickets_Cost_2px'=>$request->No_of_Tickets_Cost_2px,'Tickes_Remaining_Cost_2px'=>$request->Tickes_Remaining_Cost_2px,'Cost_2px'=>$request->Cost_2px,'rental_bike_cost'=>$request->rental_bike_cost,'Status'=>$request->Status]);
        return redirect('bikation-package');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function delete($packageId){
    if(Session::has('email') && Session::has('First_Name') ){
      $packages = Bikation_Package::where('Package_ID', '=', $packageId)->delete();
      return redirect('bikation-package');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }

}
