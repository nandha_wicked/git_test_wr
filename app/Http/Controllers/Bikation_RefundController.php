<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\BikeMake;
use App\Models\Bike;
use App\Models\Price;
use App\Models\Image;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\City;
use App\Models\BikeSpecification;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use App\Models\PackagesReview;
use DateTime;
use App\Models\Bikation_Addon;


class Bikation_RefundController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(){

    $bikation_refundList = Bikation_Addon::all();
    
    return view('bikationvendor.front.refund_index',compact('bikation_refundList'));
  }

 

public function addRefund(Request $request){

    
    Bikation_Addon::create(['Trip_ID' => $request->Trip_ID,'Package_ID' => $request->Package_ID, 'Activity_ID' => $request->Activity_ID,'Exclusion_Inclusion' => $request->Exclusion_Inclusion,'Std_Opt' => $request->Std_Opt,'Cost'=>$request->Cost]);
   
    return redirect();

  
  }
  
  
    
}
