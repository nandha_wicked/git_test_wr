<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use Carbon\Carbon;

use App\Models\PromoCode;
use App\Models\Booking;
use App\Models\BikeMake;
use App\Models\BikeModel;
use App\Models\City;
use App\Models\Area;
use App\Models\Parameters;
use App\Transformers\PromoCodeTransformer;
use Auth;

class PromoCodeController extends AppController
{
    use Helpers;
    
    function __construct(PromoCodeTransformer $promoCodeTransformer){
        $this->promoCodeTransformer = $promoCodeTransformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promoCodes = PromoCode::where('status',1)->orderBy('id','desc')->take(100)->get();
        $makes = BikeMake::where('status', 1)->get();
        foreach ($makes as $make) {
            $models = BikeModel::where('status', 1)->where('bike_make_id', $make['id'])->get();
            $make['models'] = $models;
        }

        $cities = City::where('status', 1)->get();
        foreach ($cities as $city) {
            $areas = Area::where('status', 1)->where('city_id', $city['id'])->get();
            $city['areas'] = $areas;
        }

        foreach ($promoCodes as $promocode) {
            $modelNames = [];
            $areaNames = [];
            $promocode['newUserTxt'] = ($promocode['new_user'] == true) ? 'Yes' : 'No';
            $promocode['weekdayTxt'] = ($promocode['weekday'] == true) ? 'Yes' : 'No';
            $promocode['weekendTxt'] = ($promocode['weekend'] == true) ? 'Yes' : 'No';
            $promocode['statusTxt'] = ($promocode['status'] == true) ? 'Active' : 'Inactive';
            $promocode['exemptTxt'] = ($promocode['weekday_exempt'] == true) ? 'yes' : 'no';
            $promocode['modelNames'] = '';
            $promocode['areaNames'] = '';
            if (!empty($promocode['model_ids'])) {
                $modelIds = explode(',', $promocode['model_ids']);
                foreach ($modelIds as $modelId) {
                    $model = BikeModel::where('id', $modelId)->first();
                    if(!$model)
                    {
                        
                    }
                    else
                    {
                        array_push($modelNames, $model->bike_model);
                    }
                }
                $promocode['modelNames'] = implode(', ', $modelNames);
            }

            if (!empty($promocode['area_ids'])) {
                $areaIds = explode(',', $promocode['area_ids']);
                foreach ($areaIds as $areaId) {
                    $area = Area::where('id', $areaId)->first();
                    array_push($areaNames, $area->area);
                }
                $promocode['areaNames'] = implode(', ', $areaNames);
            }
        }
        
        return view('admin.promocodes.index', compact('promoCodes', 'makes', 'cities'));
    }
    
    
    public function search(Request $request)
    {
        $promoCodes = PromoCode::where('code',$request->code)->orderBy('id','desc')->take(100)->get();
        $makes = BikeMake::where('status', 1)->get();
        foreach ($makes as $make) {
            $models = BikeModel::where('status', 1)->where('bike_make_id', $make['id'])->get();
            $make['models'] = $models;
        }

        $cities = City::where('status', 1)->get();
        foreach ($cities as $city) {
            $areas = Area::where('status', 1)->where('city_id', $city['id'])->get();
            $city['areas'] = $areas;
        }

        foreach ($promoCodes as $promocode) {
            $modelNames = [];
            $areaNames = [];
            $promocode['newUserTxt'] = ($promocode['new_user'] == true) ? 'Yes' : 'No';
            $promocode['weekdayTxt'] = ($promocode['weekday'] == true) ? 'Yes' : 'No';
            $promocode['weekendTxt'] = ($promocode['weekend'] == true) ? 'Yes' : 'No';
            $promocode['statusTxt'] = ($promocode['status'] == true) ? 'Active' : 'Inactive';
            $promocode['exemptTxt'] = ($promocode['weekday_exempt'] == true) ? 'yes' : 'no';
            $promocode['modelNames'] = '';
            $promocode['areaNames'] = '';
            if (!empty($promocode['model_ids'])) {
                $modelIds = explode(',', $promocode['model_ids']);
                foreach ($modelIds as $modelId) {
                    $model = BikeModel::where('id', $modelId)->first();
                    array_push($modelNames, $model->bike_model);
                }
                $promocode['modelNames'] = implode(', ', $modelNames);
            }

            if (!empty($promocode['area_ids'])) {
                $areaIds = explode(',', $promocode['area_ids']);
                foreach ($areaIds as $areaId) {
                    $area = Area::where('id', $areaId)->first();
                    array_push($areaNames, $area->area);
                }
                $promocode['areaNames'] = implode(', ', $areaNames);
            }
        }
        
        return view('admin.promocodes.index', compact('promoCodes', 'makes', 'cities'));
    }

    public function getAll()
    {
        $promoCodes = PromoCode::all();
        return $this->respondWithSuccess($promoCodes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code' => 'required|unique:promo_codes,code',
            'value_type' => 'required',
            'value' => 'required',
            'total_count' => 'required',
            'status' => 'required',
            'weekday_exempt' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/admin/promocode')->withErrors($validator->errors());
        }

        $code = strtoupper($request->code);
        $title = $request->title;
        $description = $request->description;
        $firstDayObj = '';
        $lastDayObj = '';
        $dates = '';

        if ($request->has('dates')){
            $dates = $request->dates;
            $datesArr = explode(',', $request->dates);
            $datesArr = array_map('trim', $datesArr);
            $firstDayObj = Carbon::parse(reset($datesArr));
            $lastDayObj = Carbon::parse(end($datesArr));
        }

        $startDateTimeObj = (!empty($firstDayObj)) ? $firstDayObj : Carbon::parse($request->start_date.''.$request->start_time);
        $endDateTimeObj = (!empty($lastDayObj)) ? $lastDayObj : Carbon::parse($request->end_date.''.$request->end_time);
        $currentDateObj = Carbon::today();

        if($startDateTimeObj->toDateString() < $currentDateObj->toDateString())
            return redirect('/admin/promocode')->withErrors('Please create a promocode with future date');

        $valueType = strtoupper($request->value_type);
        $availabaleCount = $request->total_count;
        $newUser = 0;
        $minOrder = $request->min_order;
        $modelIds = null;
        $areaIds = null;
        $weekDay = 0;
        $weekEnd = 0;
        $status = 1;
        $device = 'BOTH';
        
        if ($request->has('new_user')){
            $newUser = $request->new_user;
        }

        if ($request->has('weekday')){
            $weekDay = $request->weekday;
        }

        if ($request->has('weekend')){
            $weekEnd = $request->weekend;
        }

        if ($request->has('status')){
            $status = $request->status;
        }

        if ($request->has('device')){
            $device = $request->device;
        }

        if ($request->has('model_ids')) {
            $modelIds = implode(',', $request->model_ids);
        }

        if ($request->has('area_ids')) {
            $areaIds = implode(',', $request->area_ids);
        }
     
        $promoCode = PromoCode::create(['code' => $code, 'title' => $title, 'description' => $description, 'value_type' => $valueType, 'value' => $request->value, 'total_count' => $request->total_count, 'available_count' => $availabaleCount,'start_set' => $startDateTimeObj->toDateTimeString(), 'end_set' => $endDateTimeObj->toDateTimeString(), 'new_user' => $newUser, 'min_order' => $minOrder, 'area_ids' => $areaIds, 'model_ids' => $modelIds, 'weekday' => $weekDay, 'weekend' => $weekEnd, 'dates' => $dates, 'device' => $device, 'status' => $status, 'max_value'=>$request->max_value, 'weekday_exempt'=>$request->weekday_exempt]);

        if ($promoCode == true)
            return redirect('/admin/promocode')->with(['msg', 'Promo Code created successfully']);
        else
            return redirect('/admin/promocode')->withErrors(['Sorry something went wrong']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promoCode = PromoCode::where('id', $id)->first();

        if(!$promoCode)
            return $this->respondNotFound('Promo code not found');
        
        $data = $this->promoCodeTransformer->transform($promoCode->toArray());

        return $this->respondWithSuccess($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing = PromoCode::where('id', $id)->first();
        
        if (!$existing)
            return redirect('/admin/promocode')->withErrors('Promo code not found');

        if ($existing['code'] != $request->code){
            $rules = array(
                'code' => 'required|unique:promo_codes,code',
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return redirect('/admin/promocode')->withErrors($validator->errors());
            }
        }

        $code = $request->code;
        $title = $request->title;
        $description = $request->description;
        $totalCount = $request->total_count;
        $availabaleCount = $request->available_count;
        $valueType = strtoupper($request->value_type);
        $value = $request->value;
        $newUser = $request->new_user;
        $minOrder = $request->min_order;
        $areaIds = $request->area_ids;
        $modelIds = $request->model_ids;
        $weekDay = $request->weekday;
        $weekEnd = $request->weekend;
        $device = $request->device;
        $status = $request->status;

        $firstDayObj = '';
        $lastDayObj = '';
        $dates = '';

        if ($request->has('model_ids')) {
            $modelIds = implode(',', $request->model_ids);
        }

        if ($request->has('area_ids')) {
            $areaIds = implode(',', $request->area_ids);
        }
       
        if ($request->has('dates')){
            $dates = $request->dates;
            $datesArr = explode(',', $request->dates);
            $datesArr = array_map('trim', $datesArr);
            $firstDayObj = Carbon::parse(reset($datesArr).' '.'00:00:00');
            $lastDayObj = Carbon::parse(end($datesArr).' '.'23:59:59');
        }
        
        $currentDateObj = Carbon::today();
        $startDateTimeObj = (!empty($firstDayObj)) ? $firstDayObj : Carbon::parse($request->start_date.' '.$request->start_time);
        $endDateTimeObj = (!empty($lastDayObj)) ? $lastDayObj : Carbon::parse($request->end_date.' '.$request->end_time);

        if ($totalCount < $availabaleCount)
            return redirect('/admin/promocode')->withErrors('Available count is greater than total count.');
        
        if ($valueType == "PERCENTAGE" && $value > 100)
            return redirect('/admin/promocode')->withErrors('Percentage value can not be greater than 100.');

        if($startDateTimeObj->toDateString() < $currentDateObj->toDateString())
            return redirect('/admin/promocode')->withErrors('Please create a promocode with future date');

        if ($startDateTimeObj->toDateString() > $endDateTimeObj->toDateString())
            return redirect('/admin/promocode')->withErrors('Start Date is greater than end date.');

        $promoCode = PromoCode::where('id', $id)->update(['code' => $code, 'title' => $title, 'description' => $description, 'value_type' => $valueType, 'value' => $value, 'total_count' => $totalCount, 'available_count' => $availabaleCount,'start_set' => $startDateTimeObj->toDateTimeString(), 'end_set' => $endDateTimeObj->toDateTimeString(), 'new_user' => $newUser, 'min_order' => $minOrder, 'area_ids' => $areaIds, 'model_ids' => $modelIds, 'weekday' => $weekDay, 'weekend' => $weekEnd, 'dates' => $dates, 'device' => $device, 'status' => $status, 'max_value'=>$request->max_value, 'weekday_exempt'=>$request->weekday_exempt]);

        if ($promoCode == true)
            return redirect('/admin/promocode')->with(['msg', 'Promo Code updated successfully']);
        else
            return redirect('/admin/promocode')->withErrors(['Sorry something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PromoCode::destroy($id);
        return $this->respondWithSuccess('PromoCode successfully deleted.');
    }

    public function applyPromoCode(Request $request){
        $code = $request->code;
        $existing = PromoCode::where('code', $code)->first();
        $device = $request->device;
        $device = strtoupper($device);
        
        
        // CHECK IF PROMO CODE EXIST
        if (!$existing)
            return $this->respondWithPromoCodeError($code, 'Promo Code entered is invalid.');
        
        // CHECK IF PROMOCODE HAS NOT EXPIRED
        $currentDateObj = Carbon::now();
        $endSetObj = Carbon::parse($existing['end_set']);
        if ($currentDateObj->gte($endSetObj))
            return $this->respondWithPromoCodeError($code, 'Promo Code is expired.');

        // CHECK THE STATUS OF PROMO CODE
        if (!$existing['status'])
            return $this->respondWithPromoCodeError($code, 'Promo Code is not currently active.');
        
        // IF CODE IS APPLICABLE FOR SPECIFIC DEVICE
        if (!empty($existing->device) && $existing->device != 'BOTH') {
            
            
            if ($existing->device == 'MOBILE') {
                if($device == 'WEB'){
                    return $this->respondWithPromoCodeError($code, 'Promo Code applicable only for mobile app users.');
                }
            }
            else
            {
                if($device != 'WEB')
                {
                    return $this->respondWithPromoCodeError($code, 'Promo Code applicable only for web users.');
                }
            }
            
        }
        
        
        if(isset($request->start_time))
        {
            $fromDateObj = Carbon::parse($request->start_date.' '.$request->start_time.':00');
            $toDateObj = Carbon::parse($request->end_date.' '.$request->end_time.':00');
            $hours = $fromDateObj->diffInHours($toDateObj);
            
            $startDateTimeStr = $request->start_date.' '.$request->start_time.':00';
            $endDateTimeStr = $request->end_date.' '.$request->end_time.':00';
        }
        else
        {
            $fromDateObj = Carbon::parse($request->start_datetime);
            $toDateObj = Carbon::parse($request->end_datetime);
            $hours = $fromDateObj->diffInHours($toDateObj);
            
            $startDateTimeStr = $request->start_datetime;
            $endDateTimeStr = $request->end_datetime;
        }
        
        
        

        $startDateObj = Carbon::parse($existing['start_set']);
        $endDateObj = Carbon::parse($existing['end_set']);

        // CHECK THE USER'S DATE RANGE AGAINST PROMO CODE VALIDITY
        if ($startDateObj->gt($fromDateObj) || $endDateObj->lt($toDateObj))
            return $this->respondWithPromoCodeError($code, 'Promo Code is not valid between these dates.');
        
        // CHECK IF PROMO CODE IS AVAILABLE
        if (!$existing['available_count'])
            return $this->respondWithPromoCodeError($code, 'Promo Code was valid only for first '.$existing['total_count'].' users');

        // IF CODE IS ONLY FOR NEW USER, THEN CHECK IF USER HAS ANY BOOKING
        if ($existing['new_user']) {
            if(env('SERVER_STATUS')!='test')
            {
                $userBooking = Booking::where('user_id', UserController::getLoggedInUser()->id)->get();
                if (!empty($userBooking->toArray()))
                return $this->respondWithPromoCodeError($code, 'PromoCode is valid only for new user.');
            }
        }

        // IF CODE HAS ANY MIN ORDER SET
        if($existing['value_type'] != "HOURS")
        {
            if (!empty($existing['min_order']) && $request->price < $existing['min_order']) {
                return $this->respondWithPromoCodeError($code, 'Order total is less than promo code requirement.');
            }
        }
        else
        {
            if (!empty($existing['min_order']) && $hours < $existing['min_order']) {
                return $this->respondWithPromoCodeError($code, 'Hours booked is less than promo code requirement.');
            }
        }

        // IF CODE IS APPLICABLE FOR SPECIFIC AREAS
        if (!empty($existing['area_ids'])) {
            $areaIds = explode(',', $existing['area_ids']);
            $areaIds = array_map('trim', $areaIds);
            if (!in_array($request->area_id, $areaIds)) 
                return $this->respondWithPromoCodeError($code, 'Promo Code not applicable for this area.');
        }

        // IF CODE IS APPLICABLE FOR SPECIFIC MODELS
        if (!empty($existing['model_ids'])) {
            $modelIds = explode(',', $existing['model_ids']);
            $modelIds = array_map('trim', $modelIds);
            if (!in_array($request->model_id, $modelIds)) 
                return $this->respondWithPromoCodeError($code, 'Promo Code not applicable for this model.');
        }

        // IF CODE IS APPLICABLE FOR SPECIFIC DATES
        if (!empty($existing->dates)) {
            $dates = explode(',', $existing->dates);
            $dates = array_map('trim', $dates);
            $tempDateObj = $fromDateObj->copy();
            while ($tempDateObj <= $toDateObj) {
                if (!in_array($tempDateObj->toDateString(), $dates)) 
                    return $this->respondWithPromoCodeError($code, 'Promo Code applicable only for specific dates.');
                $tempDateObj->addDay();
            }
        }

        $userDatesArr = [];
        $weekdaysArr = [];
        $weekendsArr = [];
        $runnerFromDateObj = $fromDateObj->copy();
        $runnerStartDateObj = $startDateObj->copy();
        while ($runnerFromDateObj <= $toDateObj) {
            array_push($userDatesArr, $runnerFromDateObj->toDateString());
            $runnerFromDateObj->addDay();
        }
        while ($runnerStartDateObj <= $endDateObj) {
            if ($runnerStartDateObj->isWeekday())
                array_push($weekdaysArr, $runnerStartDateObj->toDateString());
            else
                array_push($weekendsArr, $runnerStartDateObj->toDateString());
            $runnerStartDateObj->addDay();
        }

        // IF CODE IS APPLICABLE ONLY FOR WEEKDAYS
        if ($existing['weekday'] && $existing['weekend'] == 0) {
            foreach ($userDatesArr as $dates) {
                if (!in_array($dates, $weekdaysArr)){
                    return $this->respondWithPromoCodeError($code, 'Promo Code applicable only for weekdays.');
                }
            }
        }

        // IF CODE IS APPLICABLE ONLY FOR WEEKENDS
        if ($existing['weekend'] && $existing['weekday'] == 0) {
            foreach ($userDatesArr as $dates) {
                if (!in_array($dates, $weekendsArr)){
                    return $this->respondWithPromoCodeError($code, 'Promo Code applicable only for weekends.');
                }
            }
        }
        
        // IF CODE IS APPLICABLE ONLY FOR WEEKENDS
        if ($existing['minimum_hours'] != 0) {
            $t1 = StrToTime ($startDateTimeStr);
            $t2 = StrToTime ($endDateTimeStr);
            $diff = $t2 - $t1;
            $hours = $diff / ( 60 * 60 );
            if($hours<$existing['minimum_hours']){
                    return $this->respondWithPromoCodeError($code, 'Rental duration too short for this promo code.');
                }
        }

        switch ($existing['value_type']) {
            case "RUPEES":
                if($existing['max_value']>0)
                {
                    $discount = min($existing['max_value'],$existing['value']);
                }
                else
                {
                    $discount = $existing['value'];
                }
                
                $finalPrice = $request->price - $discount;
                break;
            case "PERCENTAGE":
                if($existing['weekday_exempt']==0)
                {
                    if($request->device == "WEB")
                    {
                        if($existing['max_value']>0)
                        {
                            $discountCalc = ($existing['value'] * $request->full_slab_price)/100;
                            $discount = min($existing['max_value'],$discountCalc);
                        }
                        else
                        {
                            $discount = ($existing['value'] * $request->full_slab_price)/100;
                        }
                        
                        $finalPrice = $request->full_slab_price - $discount;
                        if($finalPrice > $request->price) 
                        {
                            return $this->respondWithPromoCodeError($code, 'The Promocode OFFPEAK applied automatically has a greater discount than '.$code.'.');
                        }
                    }
                    else
                    {
                        $dispatcher = app('Dingo\Api\Dispatcher');
                        $totalPrice = $dispatcher->with(['model_id' => $request->model_id, 'area_id' => $request->area_id, 'start_datetime' => $startDateTimeStr, 'end_datetime' => $endDateTimeStr, 'source'=>'apply_promocode'])->get('bookings/total-price');

                        $full_slab_price = $totalPrice['full_slab_price'];

                        if($existing['max_value']>0)
                        {
                            $discountCalc = ($existing['value'] * $full_slab_price)/100;
                            $discount = min($existing['max_value'],$discountCalc);
                        }
                        else
                        {
                            $discount = ($existing['value'] * $full_slab_price)/100;
                        }
                        $finalPrice = $full_slab_price - $discount;                        
                        if($finalPrice > $request->price) 
                        {
                            return $this->respondWithPromoCodeError($code, 'The Promocode OFFPEAK applied automatically has a greater discount than '.$code.'.');
                        }

                    }
                }
                else
                {
                    if($existing['max_value']>0)
                    {
                        $discountCalc = ($existing['value'] * $request->price)/100;
                        $discount = min($existing['max_value'],$discountCalc);
                    }
                    else
                    {
                        $discount = ($existing['value'] * $request->price)/100;
                    }
                    
                    $finalPrice = $request->price - $discount;
                }
                
                break;
            case "DAYS":
                $noOfDays = ceil($hours/24);
                if($noOfDays<=$existing['available_count'])
                {
                    $discount = $request->price;
                    $finalPrice = 0;   
                }
                else
                {
                    return $this->respondWithPromoCodeError($code, 'All the days for the promo code have been used.'); 
                }
                break;
            case "HOURS":
                $discountPercentage = $existing['value']/$hours;
                $discount = $request->price*$discountPercentage;
                $finalPrice = $request->price - $discount;
                break;
                
        }
        
        
        if ($finalPrice < 0){
            return $this->respondWithPromoCodeError($code, 'Promo Code can not be applied on this amount.');
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];


        $message = 'PromoCode successfully applied.';
        return ['code' => $code, 'status' => true, 'message' => $message, 'price_with_tax'=>round(($finalPrice*(1+($taxDetails->amount/100))),2), 'final_price' => round($finalPrice), 'discount' => round($discount)];
    }

}
