<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Bikation_Itinerary;
use App\Transformers\BikeModelTransformer;
use Input;
use DB;
use DateTime;
use Session;


class Bikation_ItineraryController extends AppController{

  protected $bikeModelTransformer;

  function __construct(BikeModelTransformer $bikeModelTransformer){
    $this->bikeModelTransformer = $bikeModelTransformer;
  }
  public function index(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
	  $bikation_AlltripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name')->where('Vendor_ID',Session::get('Vendor_ID'))->get();
	   $selected="";
      if (isset($request->btn_submit)) {
         $bikation_itineraryList = Bikation_Itinerary::where('Trip_ID',$request->Trip_ID)->get();
         $selected=$request->Trip_ID;

      }else{
        $bikation_itineraryList = Bikation_Itinerary::where('Vendor_ID',Session::get('Vendor_ID'))->get();
        $selected="";
      }
	
     $bikation_tripList = DB::table('bikation_trip')->select('Trip_ID','Trip_Name','Trip_Start_Date','Trip_End_Date')->where('Status', 1)->where('Vendor_ID',Session::get('Vendor_ID'))->get();
      foreach ($bikation_itineraryList as $model) {
        $Trip_Name = DB::table('bikation_trip')->select('Trip_Name')->where('Trip_ID',$model->Trip_ID)->first();
        $model['TripName'] = ($Trip_Name) ? $Trip_Name->Trip_Name : '';
      }
      return view('bikationvendor.front.itinerary_index',compact('bikation_itineraryList','bikation_tripList','bikation_AlltripList','selected'));
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function addItinerary(Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array( 'Trip_ID' => 'required',
                      'Day' => 'required',
                      //'Video_ID' => 'required',
                      'Status' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-itinerary')->withErrors($validator->errors());
      }else{
        $coverImg = $request->file('Image_ID');
        if($coverImg){
          $coverImgExt = $coverImg->guessClientExtension();
          $coverImgFn = $coverImg->getClientOriginalName();
          $coverImgHashName = time().'.'.$coverImgExt;
          $coverImgDestinationPath = 'img/vendor_image/Itinerary/';
          $coverImg->move($coverImgDestinationPath, $coverImgHashName);
		  $img_path = '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName;
        }
		else { $coverImgDestinationPath = '';$coverImgHashName = '';$img_path = '';} ;
		
        $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Itinerary::create(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'Day' => $request->Day,'Text_ID' => $request->Text_ID, 'Stay_at' => $request->Stay_at,'Image_ID' => $img_path,'Video_ID' => $request->Video_ID,'Status'=>$request->Status]);
        return redirect('bikation-itinerary');
      }
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function edit($itineraryId, Request $request){
    if(Session::has('email') && Session::has('First_Name') ){
      $rules = array( 'Trip_ID' => 'required',
                      'Day' => 'required',
                      //'Video_ID' => 'required',
                      'Status' => 'required');
      $validator = Validator::make($request->all(),$rules);
      if($validator->fails()){
        return redirect('bikation-itinerary')->withErrors($validator->errors());
      }
      else{
        $coverImg = $request->file('Image_ID');
       
        if($coverImg){
          $coverImgExt = $coverImg->guessClientExtension();
          $coverImgFn = $coverImg->getClientOriginalName();
          $coverImgHashName = time().'.'.$coverImgExt;
          $coverImgDestinationPath = 'img/vendor_image/Itinerary/';
          $coverImg->move($coverImgDestinationPath, $coverImgHashName);
        $path= '//'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName;
        }else{
           $old_image = DB::table('bikation_itinerary')->select('Image_ID')->where('id',$itineraryId)->first();
           $path=$old_image->Image_ID;
        }
         $Vendor_ID= Session::get('Vendor_ID');
        Bikation_Itinerary::where('id', $itineraryId)
        ->update(['Trip_ID' => $request->Trip_ID,'Vendor_ID' => $Vendor_ID,'Day' => $request->Day,'Text_ID' => $request->Text_ID, 'Stay_at' => $request->Stay_at,'Image_ID' => $path,'Video_ID' => $request->Video_ID,'Status'=>$request->Status]);
        return redirect('bikation-itinerary');
      }
    }
    else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }
  public function delete($itineraryId){
    if(Session::has('email') && Session::has('First_Name') ){
      $packages = Bikation_Itinerary::where('id', '=', $itineraryId)->delete();
      return redirect('/bikation-itinerary');
    }else{
      return redirect('bikation-vendor')->withErrors('Sorry You Are Not Login');
    }
  }

}
