<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;

class BikationController extends AppController
{
	use Helpers;
	public function getBikationList(){
		$bikations = [
				[
					"id" => "1", 
					"title" => "Leh to Ladakh", 
					"conductor" => [
						"id" => "1",
						"name" => "WickedRide", 
						"image_id" => "14",
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
						"contact_number" => "7795338870",
						"avg_rating" => "3.5"
					], 
					"start_date" => "2015-12-05", 
					"start_time" => "13:00:00",
					"duration" => "9 days", 
					"distance" => "2000 kms", 
					"price" => "Rs. 10000", 
					"start_city" => "Delhi", 
					"image_id" => "13",
					"image" => [
						"id" => "13",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					]
				],
				[
					"id" => "2", 
					"title" => "South India", 
					"conductor" => [
						"id" => "1",
						"name" => "WickedRide",
						"image_id" => "14", 
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
						"contact_number" => "7795338870",
						"avg_rating" => "3.5"
					], 
					"start_date" => "2015-12-05", 
					"start_time" => "13:00:00",
					"duration" => "5 days", 
					"distance" => "2000 kms", 
					"price" => "5000", 
					"start_city" => "Bangalore", 
					"image_id" => "13",
					"image" => [
						"id" => "13",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					]

				],
				[
					"id" => "3", 
					"title" => "Gujrat", 
					"conductor" => [
						"id" => "1",
						"name" => "WickedRide", 
						"image_id" => "14",
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
						"contact_number" => "7795338870",
						"avg_rating" => "3.5"
					], 
					"start_date" => "2015-12-05", 
					"start_time" => "13:00:00",
					"duration" => "8 days", 
					"distance" => "500 kms", 
					"price" => "2000", 
					"start_city" => "Bhuj", 
					"image_id" => "13",
					"image" => [
						"id" => "13",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					]
				]
		];
		return $bikations;
	}

	public function show($id){
		$bikation = [
			"gallery" => [
				[
						"id" => "13",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
					[
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					]
			],
			"conductor" => [
						"id" => "1",
						"name" => "WickedRide", 
						"image_id" => "14",
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
						"contact_number" => "7795338870",
						"avg_rating" => "3.5"
					], 
			"price" => "5000",
			"start_date" => "2015-12-05",
			"start_time" => "13:00:00",
			"duration" => "5 Days",
			"distance" => "500kms",
			"description" => "Lorem ipsum",
			"total_slots" => "60",
			"available_slots" => "30",
			"meeting_point" => "Jayanagar",
			"recommended_bikes" => ["thunderbird", "bullet"],
			"inclusions" => [
				[
					"id" => '1',
					"icon" => [
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
					"description" => "Lorem Ipsum"
				],
				[
					"id" => '1',
					"icon" => [
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
					"description" => "Lorem Ipsum"
				]

			],
			"exclusions" => [
				[
					"id" => '1',
					"icon" => [
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
					"description" => "Lorem Ipsum"
				],
				[
					"id" => '1',
					"icon" => [
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
					"description" => "Lorem Ipsum"
				]

			],
			"ride_rules" => [ "Rule 1", "Rule 2"],
			"reviews" => [
				[
					"id" => "15",
					"rating" => "3.5", 
					"title" => "Awesome experience", 
					"created_at" => "2015-12-05 13:00:00", 
					"review" => "Lorem Ipsum",
					"reviewer" => [
						"id" => "16",
						"name" => "Ashutosh", 
						"image_id" => "14",
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
					]
				],
				[	
					"id" => "16",
					"rating" => "4.0", 
					"title" => "Fun trip", 
					"created_at" => "2015-12-05 13:00:00", 
					"review" => "Lorem Ipsum",
					"reviewer" => [
						"id" => "16",
						"name" => "Ashutosh", 
						"image_id" => "14",
						"image" => [
							"id" => "14",
			                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
			                "small" => "",
			                "medium" => "",
			                "large" => ""
						],
					]
				]
			]

		];
		return $bikation;
	}

	public function create(){
		$booking = [
			'id' => "1",
		];
		return $booking;
	}

	public function postReview($id, Request $request){
		$user = UserController::getUserByToken();

		if (!$user) {
			return $this->respondNotFound('User not found');
		}
		$rules = array(
	        'rating' => 'required',
	        'review_title' => 'required',
	        'review_text' => 'required'
	    );

	    $validator = Validator::make($request->all(),$rules);

	    if($validator->fails()){
	        return $this->respondWithValidationError($validator->errors());
	    }

	    $review = [
	    	'id' => '5',
	    	'title' => 'Enjoyed the trip.',
	    	'text' => 'Lorem Ipsum Lorem Ipsum',
	    	"reviewer" => [
					"id" => "16",
					"name" => "Ashutosh", 
					"image_id" => "14",
					"image" => [
						"id" => "14",
		                "full" => "http://54.255.177.26/img/bikes_upload/thumbnail/1446715283.png",
		                "small" => "",
		                "medium" => "",
		                "large" => ""
					],
				]
	    	];

	    return $this->response->array($review);
	}

}