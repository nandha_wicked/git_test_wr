<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Auth;
use DB;
use App\Models\OneWayRentalAdditionalTxn;
use App\Models\OneWayRentalBookingNotes;
use App\Models\OneWayRentalBike;
use App\Models\OneWayRentalBooking;
use App\Models\OneWayRentalLocation;
use App\Models\OneWayRentalRoute;
use App\Models\OneWayRentalPrice;
use App\Models\OneWayRentalFinancialInfo;
use App\Models\OneWayRentalPricingInfo;
use App\Models\OneWayRentalMovementLog;
use App\Models\OneWayRentalMovementOrder;
use App\Models\OneWayRentalLocationHubSpoke;
use App\Models\BikeModel;
use App\Models\User;
use App\Models\Parameters;
use Curl;
use App\Jobs\RazorPayOneWay;
use Carbon\Carbon;
use Mail;


class OneWayRentalController extends AppController
{
       
    public function indexOneWayRentalLocation(Request $request)
    {
        $pageTitle = "One Way Rental Location";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Name"],
                                            ["width"=>200,"title"=>"Address"],
                                            ["width"=>120,"title"=>"Latitude"],
                                            ["width"=>120,"title"=>"Longitude"],
                                            ["width"=>120,"title"=>"Contact Person"],
                                            ["width"=>120,"title"=>"Contact Number"],
                                            ["width"=>80,"title"=>"Open Time"],
                                            ["width"=>80,"title"=>"Close Time"],
                                            ["width"=>80,"title"=>"Radius of Delivery"],
                                            ["width"=>80,"title"=>"Radius of Return"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $locationObjects = OneWayRentalLocation::all();
        
        $objectTableArray['body'] = [];
        
        foreach($locationObjects as $locationObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$locationObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->address,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->latitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->longitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->contact_person,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->contact_number,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->open_time,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->close_time,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->radius_delivery." KM","pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->radius_return." KM","pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($locationObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_location_btn",
                                                            "id"=>"edit_location_id",
                                                            "data_id"=>$locationObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_location_btn",
                                                            "id"=>"delete_location_id",
                                                            "data_id"=>$locationObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_location_modal";
        $addObject['add_button_text'] = "Add One Way Rental Location";
        $addObject['modal_title'] = "Add One Way Rental Location";
        $addObject['add_url'] = "/admin/one-way-rental-location/add";
        $addObject['add_modal_form_items'] = [];
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Name","name"=>"name_add"],
                ["type"=>"textarea","label"=>"Address","name"=>"address_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Latitude","name"=>"latitude_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Longitude","name"=>"longitude_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Contact Person","name"=>"contact_person_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Contact Number","name"=>"contact_number_add"],
                ["type"=>"time", "id"=>"open_time_id_add","label"=>"Opening Time","name"=>"open_time_add"],
                ["type"=>"time", "id"=>"close_time_id_add","label"=>"Closing Time","name"=>"close_time_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Delivery Radius","name"=>"radius_delivery_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Return Radius","name"=>"radius_return_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_location_modal";
        $editObject['edit_btn_class'] = "edit_location_btn";
        $editObject['modal_title'] = "Edit One Way Rental Location";
        $editObject['edit_url'] = "/admin/one-way-rental-location/edit";
        $editObject['ajax_url'] = "/admin/one-way-rental-location/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Name","name"=>"name_edit"],
                ["type"=>"textarea","label"=>"Address","name"=>"address_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Latitude","name"=>"latitude_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Longitude","name"=>"longitude_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Contact Person","name"=>"contact_person_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Contact Number","name"=>"contact_number_edit"],
                ["type"=>"time", "id"=>"open_time_id_edit","label"=>"Opening Time","name"=>"open_time_edit"],
                ["type"=>"time", "id"=>"close_time_id_edit","label"=>"Closing Time","name"=>"close_time_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Delivery Radius","name"=>"radius_delivery_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Return Radius","name"=>"radius_return_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_location_modal";
        $deleteObject['delete_btn_class'] = "delete_location_btn";
        $deleteObject['modal_title'] = "Delete One Way Rental Location";
        $deleteObject['delete_url'] = "/admin/one-way-rental-location/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this One Way Rental Location?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addOneWayRentalLocation(Request $request)
    {
        $rules = array(
            'name_add' => 'required',
            'address_add' => 'required',
            'latitude_add' => 'required',
            'longitude_add' => 'required',
            'contact_person_add' => 'required',
            'contact_number_add' => 'required|digits:10',
            'status_add' => 'required',
            'open_time_add'=> 'required',
            'close_time_add'=> 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        OneWayRentalLocation::create([
                'name'=>$request->name_add,
                'address'=>$request->address_add,
                'latitude'=>$request->latitude_add,
                'longitude'=>$request->longitude_add,
                'contact_person'=>$request->contact_person_add,
                'contact_number'=>$request->contact_number_add,
                'created_by'=>Auth::user()->id,
                'open_time'=>$request->open_time_add,
                'close_time'=>$request->close_time_add,
                'radius_delivery'=>$request->radius_delivery_add,
                'radius_return'=>$request->radius_return_add,
                'is_spoke'=>0,
                'status'=>$request->status_add,
        ]);
        
        return redirect()->back();
    }
    
    public function detailsOneWayRentalLocation(Request $request)
    {
        $location = OneWayRentalLocation::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"name_edit","value"=>$location->name],
                ["type"=>"textarea","name"=>"address_edit","value"=>$location->address],
                ["type"=>"input","name"=>"latitude_edit","value"=>$location->latitude],
                ["type"=>"input","name"=>"longitude_edit","value"=>$location->longitude],
                ["type"=>"input","name"=>"contact_person_edit","value"=>$location->contact_person],
                ["type"=>"input","name"=>"contact_number_edit","value"=>$location->contact_number],                ["type"=>"time","name"=>"open_time_edit","value"=>$location->open_time],
                ["type"=>"time","name"=>"close_time_edit","value"=>$location->close_time],
                ["type"=>"input","name"=>"radius_return_edit","value"=>$location->radius_return],
                ["type"=>"input","name"=>"radius_delivery_edit","value"=>$location->radius_delivery],   
                ["type"=>"checkbox","status"=>$location->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editOneWayRentalLocation($id, Request $request)
    {
        $oneWayRentalLocation = OneWayRentalLocation::where('id',$id)->first();
        if(!$oneWayRentalLocation)
            return redirect()->back()->withErrors(['No Location detected.']);
        
        
        if($request->status_edit != $oneWayRentalLocation->status)
        {
            $result = Self::toggleAreaStatus($id,$request->status_edit);            
        }
        
        
        
        OneWayRentalLocation::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'name'=>$request->name_edit,
            'address'=>$request->address_edit,
            'latitude'=>$request->latitude_edit,
            'longitude'=>$request->longitude_edit,
            'contact_person'=>$request->contact_person_edit,
            'contact_number'=>$request->contact_number_edit,
            'open_time'=>$request->open_time_edit,
            'close_time'=>$request->close_time_edit,
            'radius_delivery'=>$request->radius_delivery_edit,
            'radius_return'=>$request->radius_return_edit,
            'status'=>$request->status_edit,
        ]);
        
        return redirect()->back()->withErrors(['Location edited successfully.']);
    }
    
    public function deleteOneWayRentalLocation($id, Request $request)
    {
        $location = OneWayRentalLocation::where('id',$id)->first();
        
        DB::table('on_demand_a2b_hub_spoke')->where('hub_area',$location->id)->update(['deleted'=>1,'deleted_at'=>Carbon::now()->toDateTimeString(),'deleted_by'=>Auth::user()->id]);
        
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($location),"table_name"=>"on_demand_a2b_location","deleted_by"=>Auth::user()->id]);
        return redirect()->back();
    }
    
    public function indexOneWayRentalBikeAdmin(Request $request)
    {
        $page = "admin";
        $response = Self::fnIndexOneWayRentalBike($page,10000000);
        
        $objectTableArray = $response['objectTableArray']; 
        $objectTableWidth = $response['objectTableWidth']; 
        $pageTitle = $response['pageTitle']; 
        $addObject = $response['addObject']; 
        $editObject = $response['editObject']; 
        $deleteObject = $response['deleteObject'];
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
    }
    
    public function indexOneWayRentalBikeA2BList(Request $request)
    {
        $page = "admin";
        $response = Self::fnIndexOneWayRentalBike($page,10000000);
        
        $objectTableArray = $response['objectTableArray']; 
        $objectTableWidth = $response['objectTableWidth']; 
        $pageTitle = $response['pageTitle']; 
        $addObject = $response['addObject']; 
        $editObject = []; 
        $deleteObject = [];
        return view('a2b.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
    }
    
    public function indexOneWayRentalBikeA2b(Request $request)
    {
        $page = "a2b";
        if(isset($request->selected))
        {
            $selected = $request->selected;
        }
        else
        {
            $selected = 10000000;
        }
        
        $response = Self::fnIndexOneWayRentalBike($page,$selected);
        
        
        $areas = OneWayRentalLocation::all();
        $areaList = [];
        array_push($areaList,["value"=>10000000,"text"=>"All Areas"]);
        foreach($areas as $area)
        {
            array_push($areaList,["value"=>$area->id,"text"=>$area->name]);
        }
        array_push($areaList,["value"=>0,"text"=>"In Transit"]);
        
        $filterOptions = [];
        $filterOptions['url'] = "";
        $filterOptions['label'] = "Filter by Locations";
        $filterOptions['name'] = "selected";
        $filterOptions['options'] = $areaList;
        $filterOptions['selected'] = $selected;
        
        
        $objectTableArray = $response['objectTableArray']; 
        $objectTableWidth = $response['objectTableWidth']; 
        $pageTitle = $response['pageTitle']; 
        $addObject = $response['addObject']; 
        $editObject = $response['editObject']; 
        $deleteObject = $response['deleteObject'];
        $layoutOverride = "a2b_layout";
        return view('admin.template.index',compact('filterOptions','objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject','layoutOverride')); 
    }
    
    public static function fnIndexOneWayRentalBike($page,$selected)
    {
        $pageTitle = "One Way Rental Bike";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Model"],
                                            ["width"=>150,"title"=>"Number"],
                                            ["width"=>150,"title"=>"Status"],
                                            ["width"=>120,"title"=>"Last Change At"],
                                            ["width"=>120,"title"=>"Current Location"],
                                            ["width"=>120,"title"=>"Current Latitude"],
                                            ["width"=>120,"title"=>"Current Longitude"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"],
                                            ["width"=>150,"title"=>"Owner"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        if($selected!=10000000)
        {
            $bikeObjects = OneWayRentalBike::where('current_location',$selected)->orderBy('status','desc')->get();
        }
        else
            $bikeObjects = OneWayRentalBike::orderBy('status','desc')->get();
        
        $objectTableArray['body'] = [];
        
        foreach($bikeObjects as $bikeObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$bikeObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->getModel(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->bike_number,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->current_status,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->updated_at,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->getCurrentLocation(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->current_location_latitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$bikeObject->current_location_longitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($bikeObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_bike_btn",
                                                            "id"=>"edit_bike_id",
                                                            "data_id"=>$bikeObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_bike_btn",
                                                            "id"=>"delete_bike_id",
                                                            "data_id"=>$bikeObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"text","value"=>["text"=>$bikeObject->getOwner(),"pre"=>"false"]]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_bike_modal";
        $addObject['add_button_text'] = "Add One Way Rental Bike";
        $addObject['modal_title'] = "Add One Way Rental Bike";
        $addObject['add_url'] = "/".$page."/one-way-rental-bike/add";
        $addObject['add_modal_form_items'] = [];
        
        $modelList = [];
        $modelObjects = BikeModel::where('status',1)->get();
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->bike_model]);
        }
        
        $locationList = [];
        $locationObjects = OneWayRentalLocation::where('status',1)->get();
        foreach($locationObjects as $locationObject)
        {
            array_push($locationList,["value"=>$locationObject->id,"text"=>$locationObject->name]);
        }
        
        $seriesListObj = DB::table('parameter')->where('parameter_name','bike_number_series_list')->first();
        $seriesArray = explode(',',$seriesListObj->parameter_value);
        $seriesList = [];
        foreach($seriesArray as $seriesArrayEl)
        {
            array_push($seriesList,["value"=>$seriesArrayEl,"text"=>$seriesArrayEl]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"select", "label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList], 
                ["type"=>"select", "label"=>"Number Series", "name"=>"number_series_add", "id"=>"number_series_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Series --", "select_items"=>$seriesList], 
                ["type"=>"input", "input_type"=>"text","label"=>"Area Code","name"=>"number_area_code_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Series Letters","name"=>"number_letters_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Number","name"=>"number_four_digits_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Owner Email ID","name"=>"owner_email_add"],
                ["type"=>"select", "label"=>"Current Location", "name"=>"current_location_add", "id"=>"current_location_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Location --", "select_items"=>$locationList],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_bike_modal";
        $editObject['edit_btn_class'] = "edit_bike_btn";
        $editObject['modal_title'] = "Edit One Way Rental Bike";
        $editObject['edit_url'] = "/".$page."/one-way-rental-bike/edit";
        $editObject['ajax_url'] = "/".$page."/one-way-rental-bike/details";
        $editObject['edit_modal_form_items'] = [];
        
        $activateReasonList = explode(',',Parameters::getParameter('activate_reasons'));
        $inactivateReasonList = explode(',',Parameters::getParameter('inactivate_reasons'));
        
        $reasonList = [];
        array_push($reasonList,["value"=>"", "text"=>" -- Reasons for making the bike Inactive -- ", "disabled"=>true]);
        
        foreach($inactivateReasonList as $inactivateReasonListEl)
        {
            array_push($reasonList,["value"=>$inactivateReasonListEl,"text"=>$inactivateReasonListEl]);
        }
        
        array_push($reasonList,["value"=>"", "text"=>" -- Reasons for making the bike Active -- ", "disabled"=>true]);
        
        foreach($activateReasonList as $activateReasonListEl)
        {
            array_push($reasonList,["value"=>$activateReasonListEl,"text"=>$activateReasonListEl]);
        }
        
        $editObject['edit_modal_form_items'] = 
            [
               
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"], 
                ["type"=>"select", "label"=>"Reason for changing the status", "name"=>"status_reason_edit", "id"=>"status_reason_id_edit", "default_value"=>"not_set", "default_text"=>"", "select_items"=>$reasonList], 
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_bike_modal";
        $deleteObject['delete_btn_class'] = "delete_bike_btn";
        $deleteObject['modal_title'] = "Delete One Way Rental Bike";
        $deleteObject['delete_url'] = "/".$page."/one-way-rental-bike/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this One Way Rental Bike?";
        
        return ['objectTableArray'=>$objectTableArray ,'objectTableWidth'=>$objectTableWidth ,'pageTitle'=>$pageTitle ,'addObject'=>$addObject ,'editObject'=>$editObject ,'deleteObject'=>$deleteObject];
        
    }
    
    
    
    public function addOneWayRentalBike(Request $request)
    {
        $rules = array(
            'model_add' => 'required',
            'number_series_add' => 'required|alpha',
            'number_area_code_add' => 'required|digits:2',
            'number_letters_add' => 'required|alpha',
            'number_four_digits_add' => 'required|digits:4',
            'status_add' => 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $owner = User::where('email',$request->owner_email_add)->first();
        if(!$owner)
            return redirect()->back()->withErrors(['Enter valid email address of the bike owner.']);
        $seriesCode = strtoupper($request->number_letters_add);
        $bikeNumber = $request->number_series_add."-".$request->number_area_code_add."-".$seriesCode."-".$request->number_four_digits_add;
        $existing = OneWayRentalBike::where('bike_number',$bikeNumber)->first();
        
        if($request->status_add)
        {
            $currentStatus = "Available for booking";
        }
        else
        {
            $currentStatus = "Bike made inactive";
        }
        
        if(!$existing)
        {
            $location = OneWayRentalLocation::where('id',$request->current_location_add)->first();
                        
            OneWayRentalBike::create([
                'model_id'=>$request->model_add,
                'bike_number'=>$bikeNumber,
                'current_location'=>$location->id,
                'owner_id'=>$owner->id,
                'current_location_latitude'=>$location->latitude,
                'current_location_longitude'=>$location->longitude,
                'created_by'=>Auth::user()->id,
                'status'=>$request->status_add,
                'current_status'=>$currentStatus
            ]);
            
            
        }
        else
        {
            return redirect()->back()->withErrors(['Bike number already exists']);
        }
        
        return redirect()->back();
    }
    
    public function detailsOneWayRentalBike(Request $request)
    {
        $bike = OneWayRentalBike::where('id',$request->id)->first();
        $bikeNumberExploded = explode('-',$bike->bike_number);
        
        $data = [
                
                ["type"=>"checkbox","status"=>$bike->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editOneWayRentalBike($id, Request $request)
    {
        $rules = array(
            'status_edit' => 'required',
            'status_reason_edit' => 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $bike = OneWayRentalBike::where('id',$id)->first();
        if($request->status_edit != $bike->status)
        {
            
            if(!$bike->status)
            {
                
                $currentStatus = "Bike made active";
                
                if(isset($request->status_reason_edit))
                    $currentStatusLog = $request->status_reason_edit;
                else
                    $currentStatusLog = "Bike made active";
                
                
            }
            else
            {
                
                $currentStatus = "Bike made inactive";
                
                if(isset($request->status_reason_edit))
                    $currentStatusLog = $request->status_reason_edit;
                else
                    $currentStatusLog = "Bike made inactive";
            }
                            
            
        }
        else
        {
            return redirect()->back()->withErrors(['No change in status detected.']);
        }
        
        OneWayRentalBike::where('id',$id)->update([
            'status'=>$request->status_edit,
            'current_status'=>$currentStatus,
            'updated_by'=>Auth::user()->id
        ]);
        
        DB::table('on_demand_a2b_bike_activity')->insert([
            'bike_id'=>$bike->id, 
            'old_status'=>$bike->current_status, 
            'new_status'=>$currentStatusLog, 
            'old_status_bool'=>$bike->status, 
            'new_status_bool'=>$request->status_edit, 
            'current_location'=>$bike->current_location, 
            'current_location_latitude'=>$bike->current_location_latitude, 
            'current_location_longitude'=>$bike->current_location_longitude, 
            'created_by'=>Auth::user()->id
        ]);
            
        return redirect()->back();
    }
    
    public function deleteOneWayRentalBike($id, Request $request)
    {
        $bike = OneWayRentalBike::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($bike),"table_name"=>"on_demand_a2b_bike","deleted_by"=>Auth::user()->id]);
        OneWayRentalBike::where('id',$id)->update([
            'deleted'=>1,
            'deleted_by'=>Auth::user()->id,
            'deleted_at'=>Carbon::now()->toDateTimeString(),
        ]);
        return redirect()->back();
    }
    
    
    
    public function indexOneWayRentalRoute(Request $request)
    {
        $pageTitle = "One Way Rental Route";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>150,"title"=>"Model"],
                                            ["width"=>150,"title"=>"From Location"],
                                            ["width"=>150,"title"=>"To Location"],
                                            ["width"=>150,"title"=>"Price"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $routeObjects = OneWayRentalRoute::all();
        
        $objectTableArray['body'] = [];
        
        foreach($routeObjects as $routeObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$routeObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$routeObject->getModel(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$routeObject->getFromLocation(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$routeObject->getToLocation(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$routeObject->price,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($routeObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_route_btn",
                                                            "id"=>"edit_route_id",
                                                            "data_id"=>$routeObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_route_btn",
                                                            "id"=>"delete_route_id",
                                                            "data_id"=>$routeObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_route_modal";
        $addObject['add_button_text'] = "Add One Way Rental Route";
        $addObject['modal_title'] = "Add One Way Rental Route";
        $addObject['add_url'] = "/admin/one-way-rental-route/add";
        $addObject['add_modal_form_items'] = [];
        
        $modelList = [];
        $modelObjects = BikeModel::where('status',1)->get();
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->bike_model]);
        }
        
        $locationList = [];
        $locationObjects = OneWayRentalLocation::where('status',1)->get();
        foreach($locationObjects as $locationObject)
        {
            array_push($locationList,["value"=>$locationObject->id,"text"=>$locationObject->name]);
        }
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"select", "label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList],
                ["type"=>"select", "label"=>"From Location", "name"=>"from_location_add", "id"=>"from_location_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Location --", "select_items"=>$locationList],
                ["type"=>"select", "label"=>"To Location", "name"=>"to_location_add", "id"=>"to_location_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Location --", "select_items"=>$locationList], 
                ["type"=>"input", "input_type"=>"text","label"=>"Price","name"=>"price_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_route_modal";
        $editObject['edit_btn_class'] = "edit_route_btn";
        $editObject['modal_title'] = "Edit One Way Rental Route";
        $editObject['edit_url'] = "/admin/one-way-rental-route/edit";
        $editObject['ajax_url'] = "/admin/one-way-rental-route/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Price","name"=>"price_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_route_modal";
        $deleteObject['delete_btn_class'] = "delete_route_btn";
        $deleteObject['modal_title'] = "Delete One Way Rental Route";
        $deleteObject['delete_url'] = "/admin/one-way-rental-route/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this One Way Rental Route?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addOneWayRentalRoute(Request $request)
    {
        if($request->from_location_add == $request->to_location_add)
            return redirect()->back()->withErrors(['From and To locations cannot be same.']);
        
        
        OneWayRentalRoute::create([
            'model_id'=>$request->model_add,
            'from_location'=>$request->from_location_add,
            'to_location'=>$request->to_location_add,
            'price'=>$request->price_add,
            'created_by'=>Auth::user()->id,
            'status'=>$request->status_add,
        ]);
            
        
        return redirect()->back();
    }
    
    public function detailsOneWayRentalRoute(Request $request)
    {
        $route = OneWayRentalRoute::where('id',$request->id)->first();
        
        $data = [
            
                ["type"=>"input","name"=>"price_edit","value"=>$route->price],
                ["type"=>"checkbox","status"=>$route->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editOneWayRentalRoute($id, Request $request)
    {
        
        OneWayRentalRoute::where('id',$id)->update([
            'price'=>$request->price_edit,
            'updated_by'=>Auth::user()->id,
            'status'=>$request->status_edit,
        ]);
            
        return redirect()->back();
    }
    
    public function deleteOneWayRentalRoute($id, Request $request)
    {
        $route = OneWayRentalRoute::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($route),"table_name"=>"on_demand_a2b_route","deleted_by"=>Auth::user()->id]);
        OneWayRentalRoute::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    public function indexOneWayRentalPrice(Request $request)
    {
        $pageTitle = "One Way Rental Price";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>150,"title"=>"Model"],
                                            ["width"=>150,"title"=>"Price Per KM"],
                                            ["width"=>150,"title"=>"Price Per Min"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $priceObjects = OneWayRentalPrice::all();
        
        $objectTableArray['body'] = [];
        
        foreach($priceObjects as $priceObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$priceObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$priceObject->getModel(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$priceObject->price_per_km,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$priceObject->price_per_min,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($priceObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_price_btn",
                                                            "id"=>"edit_price_id",
                                                            "data_id"=>$priceObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_price_btn",
                                                            "id"=>"delete_price_id",
                                                            "data_id"=>$priceObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_price_modal";
        $addObject['add_button_text'] = "Add One Way Rental Price";
        $addObject['modal_title'] = "Add One Way Rental Price";
        $addObject['add_url'] = "/admin/one-way-rental-price/add";
        $addObject['add_modal_form_items'] = [];
        
        $modelList = [];
        $modelObjects = BikeModel::where('status',1)->get();
        foreach($modelObjects as $modelObject)
        {
            array_push($modelList,["value"=>$modelObject->id,"text"=>$modelObject->bike_model]);
        }
        
        
        
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"select", "label"=>"Model", "name"=>"model_add", "id"=>"model_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Model --", "select_items"=>$modelList], 
                ["type"=>"input", "input_type"=>"text","label"=>"Price Per KM","name"=>"price_per_km_add"], 
                ["type"=>"input", "input_type"=>"text","label"=>"Price Per Minute","name"=>"price_per_min_add"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_price_modal";
        $editObject['edit_btn_class'] = "edit_price_btn";
        $editObject['modal_title'] = "Edit One Way Rental Price";
        $editObject['edit_url'] = "/admin/one-way-rental-price/edit";
        $editObject['ajax_url'] = "/admin/one-way-rental-price/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Price Per KM","name"=>"price_per_km_edit"], 
                ["type"=>"input", "input_type"=>"text","label"=>"Price Per Minute","name"=>"price_per_min_edit"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"]
            
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_price_modal";
        $deleteObject['delete_btn_class'] = "delete_price_btn";
        $deleteObject['modal_title'] = "Delete One Way Rental Price";
        $deleteObject['delete_url'] = "/admin/one-way-rental-price/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this One Way Rental Price?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addOneWayRentalPrice(Request $request)
    {
        $rules = array(
        'model_add' => 'required',
        'price_per_km_add' => 'required',
        'price_per_min_add' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return redirect()->back()->withErrors($validator->errors());        
        }
        
        
        OneWayRentalPrice::create([
            'model_id'=>$request->model_add,
            'price_per_km'=>$request->price_per_km_add,
            'price_per_min'=>$request->price_per_min_add,
            'created_by'=>Auth::user()->id,
            'status'=>$request->status_add,
        ]);
            
        
        return redirect()->back();
    }
    
    public function detailsOneWayRentalPrice(Request $request)
    {
        $price = OneWayRentalPrice::where('id',$request->id)->first();
        
        $data = [
            
                ["type"=>"input","name"=>"price_per_km_edit","value"=>$price->price_per_km],
                ["type"=>"input","name"=>"price_per_min_edit","value"=>$price->price_per_min],
                ["type"=>"checkbox","status"=>$price->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editOneWayRentalPrice($id, Request $request)
    {
        
        OneWayRentalPrice::where('id',$id)->update([
            'price_per_km'=>$request->price_per_km_edit,
            'price_per_min'=>$request->price_per_min_edit,
            'updated_by'=>Auth::user()->id,
            'status'=>$request->status_edit,
        ]);
            
        return redirect()->back();
    }
    
    public function deleteOneWayRentalPrice($id, Request $request)
    {
        $price = OneWayRentalPrice::where('id',$id)->first();
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($price),"table_name"=>"on_demand_a2b_price","deleted_by"=>Auth::user()->id]);
        OneWayRentalPrice::where('id',$id)->delete();
        return redirect()->back();
    }
    
    
    
    public function createOneWayRentalBooking(Request $request)
    {
        
        $user = UserController::getLoggedInUser();
        
        $rules = array(
            'from_location' => 'required',
            'to_location' => 'required',
            'model_id' => 'required',
            'payment_method' => 'required',
            'pg_txn_amount' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        DB::table('appRequest')->insert(['request'=>json_encode($requestArray)]);

        if($request->pg_txn_id != "")
        {
            $booking = OneWayRentalBooking::where('payment_id',$request->pg_txn_id)->first();

            if(!(!$booking))
            {
                $data = [
                    'status'=>true,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/success/".$booking->reference_id,
                    'user_doc_status'=>false
                ];

                return $this->respondWithSuccess($data);
            }
        }
        
        $from_location = OneWayRentalLocation::where('id',$request->from_location)->first();
        $to_location = OneWayRentalLocation::where('id',$request->to_location)->first();
        
        $bike = OneWayRentalBike::where('current_location',$request->from_location)->where('status',1)->orderBy('updated_at','asc')->first();
        
        if(!$bike)
        {
            $shortestDuration = 10000000000;
            $shortestDurationHub = 0;
            
            $hubsForTheSpoke = OneWayRentalLocationHubSpoke::where('spoke_area',$request->from_location)->where('deleted',0)->get()->pluck('hub_area')->all();
            
            array_push($hubsForTheSpoke,$request->from_location);
            
            $locationsThatCanServeTheGivenLocation = OneWayRentalLocationHubSpoke::whereIn('hub_area',$hubsForTheSpoke)->where('deleted',0)->get()->pluck('spoke_area')->all();
            
            $allLocationsThatCanServeTheGivenLocation = array_merge($locationsThatCanServeTheGivenLocation,$hubsForTheSpoke);
            
            foreach($allLocationsThatCanServeTheGivenLocation as $locationId)
            {
                $bike = OneWayRentalBike::where('current_location',$locationId)->where('status',1)->orderBy('updated_at','asc')->first();
                if(!(!$bike))
                {
                    $hubLocation = OneWayRentalLocation::where('id',$locationId)->first();
                    $response = Curl::to("https://maps.googleapis.com/maps/api/directions/json?origin=".$hubLocation->latitude.",".$hubLocation->longitude."&destination=".$to_location->latitude.",".$to_location->longitude."&key=".env('GOOGLEMAPSAPI'))->post();

                    $decoded = json_decode($response,true);
                    $duration = $decoded['routes'][0]['legs'][0]['duration']['value'];

                    if($duration<$shortestDuration)
                    {
                        $shortestDuration = $duration;
                        $shortestDurationHub = $locationId;
                    }
                }
            }
            
            if($shortestDurationHub == 0)
            {
                $success = false; 
                $reason = "Bike Not Available.";
                $failureId = "notavailable";
                $data = [
                    'status'=>false,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/failed/notavailable",
                    'user_doc_status'=>false
                ];
                $bike = false;
            }
            else
            {
                $bike = OneWayRentalBike::where('current_location',$shortestDurationHub)->where('status',1)->orderBy('updated_at','asc')->first();
            }            
        }
        
        $userPhone = Self::sanitizePhoneNumber($user->mobile_num);
        
        if(!isset($request->applied_wallet_amount))
            $applied_wallet_amount = 0;
        else
            $applied_wallet_amount = $request->applied_wallet_amount;
        
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
                
        $pgTxnAmount = $request->pg_txn_amount;
        $pgTxnId = $request->pg_txn_id;

        $model = BikeModel::where('id',$request->model_id)->first();

        $price = 0;
        
        $priceObj = OneWayRentalPrice::where('model_id',env('SCOOTERMODEL'))->first();
            
        $coupon = isset($request->coupon)?$request->coupon:"";

        if($request->from_location == $request->to_location)
        {
            $estimated_distance = 0;
            $estimated_duration = 0;

            $price = env('SAMELOCATIONRENTAL');
        }
        else
        {
            $estimated_distance = floatval($request->estimated_distance);
            $estimated_duration = floatval($request->estimated_duration);

            $priceRes = Self::calculateRental($model->id,$estimated_distance,$estimated_duration);
            $price = $priceRes["price"];
        }            

        $totalPriceDetails = Self::calculatePrice($price,$priceObj,$coupon,$request->payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$user->id);

        if(!$bike)
        {
            $success = false; 
            $reason = "Bike Not Available.";
            $failureId = "notavailable";
            $data = [
                'status'=>false,
                'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/failed/notavailable",
                'user_doc_status'=>false
            ];
        }
        else
        {
            
            if(($request->payment_method == "PayLaterAndroid")||($request->payment_method == "PayLateriOS"))
            {
                $amountDue = 0;
                $pgTxnAmount = 0;
                $applied_wallet_amount = 0;
            }
            else
            {
                $amountDue = $totalPriceDetails['total_with_tax'] - $totalPriceDetails['total_wallet_balance_applied'];
                $applied_wallet_amount = $totalPriceDetails['total_wallet_balance_applied'];
            }
            
            
            if($request->pg_txn_amount > ($amountDue - 2))
            {

                $booking = OneWayRentalBooking::create([
                    'user_id'=>$user->id,
                    'from_location'=>$request->from_location, 
                    'to_location'=>$request->to_location,
                    'model_id'=>$bike->model_id,
                    'bike_id'=>$bike->id, 
                    'status'=>"Booking Made", 
                    'price'=>$price,
                    'amount_paid_payment_gateway'=>$pgTxnAmount,
                    'payment_method'=>$request->payment_method, 
                    'payment_id'=>$pgTxnId,
                    'wallet_amount_applied'=>$applied_wallet_amount,
                    'customer_instructions'=>$request->customer_instructions,
                    'created_by'=>$user->id,
                    'from_location_longitude'=>$from_location->longitude, 
                    'from_location_latitude'=>$from_location->latitude, 
                    'to_location_longitude'=>$to_location->longitude, 
                    'to_location_latitude'=>$to_location->latitude,
                    'estimated_distance'=>$estimated_distance,
                    'estimated_duration'=>$estimated_duration
                ]);
                
                
                $totalPriceDetails = Self::createFinancialInfo($price,$priceObj,$coupon,$request->payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$user->id,$booking->id,$estimated_distance,$estimated_duration);

                $referenceId = "MBAB".str_pad($booking->id,6,0,STR_PAD_LEFT);

                OneWayRentalBooking::where('id',$booking->id)->update(['reference_id'=>$referenceId]);

                OneWayRentalBike::where('id',$bike->id)->update([
                    'current_location'=>0, 
                    'current_location_latitude'=>0, 
                    'current_location_longitude'=>0, 
                    'updated_by'=>$user->id,
                    'current_status'=>"Bike block for booking"
                ]); 
                
                
                $responesForSMSAndEmail  = Self::sendEmailAndSMS($bike,$from_location,$to_location,$referenceId,$user,$estimated_distance,$estimated_duration,$totalPriceDetails);


                if($amountDue > 0)
                {
                    $parameters = [
                        'pg_txn_amount'=>$pgTxnAmount,
                        'pg_txn_id'=>$pgTxnId,
                        'amount_due'=>$amountDue,
                        'bookingId'=>$booking->id
                    ];

                    $parametersEn = json_encode($parameters);

                    $this->dispatch(new RazorPayOneWay($parametersEn));
                }
                $success = true;  
                $data = [
                    'status'=>true,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/success/".$referenceId,
                    'user_doc_status'=>false
                ];
            }
            else
            {
                $success = false; 
                $reason = "Price Mismatch.";
                $failureId = "pricemismatch";
                $data = [
                    'status'=>false,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/failed/pricemismatch",
                    'user_doc_status'=>false
                ];
                
                $requestArray = [];
                foreach($request->request as $key=>$value)
                {
                    array_push($requestArray,[$key=>$value]);
                }
                DB::table('appRequest')->insert(['request'=>json_encode($requestArray),'string'=>"a2b_price_mismatch"]);

            }
        }
        
        
        if($success == false)
        {
            $adminMailData = [
                'name'=>$user->first_name." ".$user->last_name,
                'phone'=>$userPhone,
                'model'=>$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model,
                'from'=>$from_location->name,
                'to'=>$to_location->name,
                'customer_instructions'=>$request->customer_instructions,
                'price'=>$totalPriceDetails['total_with_tax'],
                'wallet_amount_applied'=>$applied_wallet_amount,
                'email'=>$user->email,
                'pgTxnAmount'=>$pgTxnAmount,
                'problem'=>true,
                'pgTxnId'=>$pgTxnId,
                'reason'=>$reason
            ];
            
            Mail::send('emails.admin_a2b_booking_mail',$adminMailData,function($message) use ($adminMailData) {
            //var_dump($data);
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject('Problem with One Way Rental '.$adminMailData['name']);
            });
            
               
            $mailData = [
                    'userEmail'=>$user->email,
                    'fname'=>$user->first_name." ".$user->last_name,
                    'id'=>$failureId
                ];
                    
            Mail::send('emails.user_a2b_booking_problem_mail',$mailData,function($message) use ($mailData) {
              $message->to($mailData['userEmail']);
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject(env('SITENAMECAP').' - Problem with your one-way rental booking');
            });
            
        }
        
        return $this->respondWithSuccess($data);
        
    }
    
    
    public function createOneWayRentalBookingForLatLong(Request $request)
    {
        
        $user = UserController::getLoggedInUser();
        
        $rules = array(
            'from_latitude' => 'required',
            'to_latitude' => 'required',
            'from_longitude' => 'required',
            'to_longitude' => 'required',
            'model_id' => 'required',
            'payment_method' => 'required',
            'pg_txn_amount' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $requestArray = [];
        foreach($request->request as $key=>$value)
        {
            array_push($requestArray,[$key=>$value]);
        }
        DB::table('appRequest')->insert(['request'=>json_encode($requestArray)]);

        $booking = OneWayRentalBooking::where('payment_id',$request->pg_txn_id)->first();
        
        if($request->pg_txn_id != "")
        {
            $booking = OneWayRentalBooking::where('payment_id',$request->pg_txn_id)->first();

            if(!(!$booking))
            {
                $data = [
                    'status'=>true,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/success/".$booking->reference_id,
                    'user_doc_status'=>false
                ];

                return $this->respondWithSuccess($data);
            }
        }
        
        $fromLat = $request->from_latitude;
        $fromLong = $request->from_longitude;
        $toLat = $request->to_latitude;
        $toLong = $request->to_longitude;
        
        $fromLocation = OneWayRentalLocation::where('latitude',$fromLat)->where('longitude',$fromLong)->first();
        if(!$fromLocation)
        {
            $locationsWithBikes = OneWayRentalLocation::whereHas('bikes', function ($query) {
                $query->where('status', 1);
            })->get();

            $destinationArr = [];

            foreach($locationsWithBikes as $locationsWithBike)
            {
                array_push($destinationArr,$locationsWithBike->latitude."%2C".$locationsWithBike->longitude);
            }

            $destinationStr = implode("%7C",$destinationArr);

            $response = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&destinations=".$fromLat.",".$fromLong."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();

            $responseObj = json_decode($response);
            $distanceMatrixArray = $responseObj->rows;

            $distance = 1000000;
            $selected = 0;

            foreach($locationsWithBikes as $i=>$locationsWithBike)
            {
                $distanceMatrixForLocation = $distanceMatrixArray[$i]->elements[0];
                if($distanceMatrixForLocation->status == "OK")
                {
                    if($distanceMatrixForLocation->distance->value < $locationsWithBike->radius_delivery*1000)
                    {
                        if($distance > $distanceMatrixForLocation->distance->value)
                        {
                            $selected = $locationsWithBike->id;
                            $distance = $distanceMatrixForLocation->distance->value;
                        }
                    }
                }
            }
            $fromLocation = OneWayRentalLocation::find($selected);
            $fromLocationFailureStr = $responseObj->origin_addresses[0];
        }
        else
        {
            $fromLocationFailureStr = $fromLocation->name;
        }

        $toLocation = OneWayRentalLocation::where('latitude',$toLat)->where('longitude',$toLong)->first();
        if(!$toLocation)
        {
            $locationsWithBikes = OneWayRentalLocation::where('status',1)->get();

            $destinationArr = [];

            foreach($locationsWithBikes as $locationsWithBike)
            {
                array_push($destinationArr,$locationsWithBike->latitude."%2C".$locationsWithBike->longitude);
            }

            $destinationStr = implode("%7C",$destinationArr);

            $response = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&destinations=".$toLat.",".$toLong."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();

            $responseObj = json_decode($response);
            $distanceMatrixArray = $responseObj->rows;

            $distance = 1000000;
            $selected = 0;

            foreach($locationsWithBikes as $i=>$locationsWithBike)
            {
                $distanceMatrixForLocation = $distanceMatrixArray[$i]->elements[0];
                if($distanceMatrixForLocation->status == "OK")
                {
                    if($distanceMatrixForLocation->distance->value < $locationsWithBike->radius_delivery*1000)
                    {
                        if($distance > $distanceMatrixForLocation->distance->value)
                        {
                            $selected = $locationsWithBike->id;
                            $distance = $distanceMatrixForLocation->distance->value;
                        }
                    }
                }
            }
            $toLocation = OneWayRentalLocation::find($selected);
            $toLocationFailureStr = $responseObj->origin_addresses[0];
        }
        else
        {
            $toLocationFailureStr = $toLocation->name;
        }
        
       
        
        $bike = OneWayRentalBike::where('current_location',$fromLocation->id)->where('status',1)->orderBy('updated_at','asc')->first();
       
        $userPhone = Self::sanitizePhoneNumber($user->mobile_num);
        
        if(!isset($request->applied_wallet_amount))
            $applied_wallet_amount = 0;
        else
            $applied_wallet_amount = $request->applied_wallet_amount;
        
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
                
        $pgTxnAmount = $request->pg_txn_amount;
        $pgTxnId = $request->pg_txn_id;

        $model = BikeModel::where('id',$request->model_id)->first();

        $price = 0;
        
        $priceObj = OneWayRentalPrice::where('model_id',env('SCOOTERMODEL'))->first();
            
        $coupon = isset($request->coupon)?$request->coupon:"";

        if(($request->from_latitude==$request->to_latitude)&&($request->from_longitude==$request->to_longitude))
        {
            $estimated_distance = 0;
            $estimated_duration = 0;

            $price = env('SAMELOCATIONRENTAL');
        }
        else
        {
            $estimated_distance = floatval($request->estimated_distance);
            $estimated_duration = floatval($request->estimated_duration);

            $priceRes = Self::calculateRental($model->id,$estimated_distance,$estimated_duration);
            $price = $priceRes["price"];
        }            

        $totalPriceDetails = Self::calculatePrice($price,$priceObj,$coupon,$request->payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$user->id);

                
        if(!$bike)
        {
            $success = false; 
            $reason = "Bike Not Available.";
            $failureId = "notavailable";
            $data = [
                'status'=>false,
                'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/failed/notavailable",
                'user_doc_status'=>false
            ];
        }
        else
        {
            
            if((($request->payment_method == "PayLaterAndroid")||($request->payment_method == "PayLateriOS"))&&(env('ONEWAYPAYLATER')))
            {
                $amountDue = 0;
                $pgTxnAmount = 0;
                $applied_wallet_amount = 0;
            }
            else
            {
                $amountDue = $totalPriceDetails['total_with_tax'] - $totalPriceDetails['total_wallet_balance_applied'];
                $applied_wallet_amount = $totalPriceDetails['total_wallet_balance_applied'];
            }
            
            
            if($request->pg_txn_amount > ($amountDue - 2))
            {

                $booking = OneWayRentalBooking::create([
                    'user_id'=>$user->id,
                    'from_location'=>$fromLocation->id, 
                    'to_location'=>$toLocation->id,
                    'model_id'=>$bike->model_id,
                    'bike_id'=>$bike->id, 
                    'status'=>"Booking Made", 
                    'price'=>$price,
                    'amount_paid_payment_gateway'=>$pgTxnAmount,
                    'payment_method'=>$request->payment_method, 
                    'payment_id'=>$pgTxnId,
                    'wallet_amount_applied'=>$applied_wallet_amount,
                    'customer_instructions'=>$request->customer_instructions,
                    'created_by'=>$user->id,
                    'from_location_longitude'=>$fromLat, 
                    'from_location_latitude'=>$fromLong, 
                    'to_location_longitude'=>$toLat, 
                    'to_location_latitude'=>$toLong,
                    'estimated_distance'=>$estimated_distance,
                    'estimated_duration'=>$estimated_duration
                ]);
                
                
                $totalPriceDetails = Self::createFinancialInfo($price,$priceObj,$coupon,$request->payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$user->id,$booking->id,$estimated_distance,$estimated_duration);

                $referenceId = "MBAB".str_pad($booking->id,6,0,STR_PAD_LEFT);

                OneWayRentalBooking::where('id',$booking->id)->update(['reference_id'=>$referenceId]);

                OneWayRentalBike::where('id',$bike->id)->update([
                    'current_location'=>0, 
                    'current_location_latitude'=>0, 
                    'current_location_longitude'=>0, 
                    'updated_by'=>$user->id,
                    'current_status'=>"Bike block for booking"
                ]); 
                
                
                $responesForSMSAndEmail  = Self::sendEmailAndSMSForLatLong($bike,$fromLocation,$toLocation,$fromLat,$fromLong,$toLat,$toLong,$referenceId,$user,$estimated_distance,$estimated_duration,$totalPriceDetails);


                if($amountDue > 0)
                {
                    $parameters = [
                        'pg_txn_amount'=>$pgTxnAmount,
                        'pg_txn_id'=>$pgTxnId,
                        'amount_due'=>$amountDue,
                        'bookingId'=>$booking->id
                    ];

                    $parametersEn = json_encode($parameters);

                    $this->dispatch(new RazorPayOneWay($parametersEn));
                }
                $success = true;  
                $data = [
                    'status'=>true,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/success/".$referenceId,
                    'user_doc_status'=>false
                ];
            }
            else
            {
                $success = false; 
                $reason = "Price Mismatch.";
                $failureId = "pricemismatch";
                $data = [
                    'status'=>false,
                    'message_url'=>env('HOME_URL')."/one-way-rental-booking-message/failed/pricemismatch",
                    'user_doc_status'=>false
                ];
            }
        }
        
        
        if($success == false)
        {
            $adminMailData = [
                'name'=>$user->first_name." ".$user->last_name,
                'phone'=>$userPhone,
                'model'=>$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model,
                'from'=>$fromLocationFailureStr,
                'to'=>$toLocationFailureStr,
                'customer_instructions'=>$request->customer_instructions,
                'price'=>$totalPriceDetails['total_with_tax'],
                'wallet_amount_applied'=>$applied_wallet_amount,
                'email'=>$user->email,
                'pgTxnAmount'=>$pgTxnAmount,
                'problem'=>true,
                'pgTxnId'=>$pgTxnId,
                'reason'=>$reason
            ];
            
            Mail::send('emails.admin_a2b_booking_mail',$adminMailData,function($message) use ($adminMailData) {
            //var_dump($data);
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject('Problem with One Way Rental '.$adminMailData['name']);
            });
            
               
            $mailData = [
                    'userEmail'=>$user->email,
                    'fname'=>$user->first_name." ".$user->last_name,
                    'id'=>$failureId
                ];
                    
            Mail::send('emails.user_a2b_booking_problem_mail',$mailData,function($message) use ($mailData) {
              $message->to($mailData['userEmail']);
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject(env('SITENAMECAP').' - Problem with your one-way rental booking');
            });
            
        }
        
        return $this->respondWithSuccess($data);
        
    }
    
    
    public function deleteOneWayRentalBooking($id, Request $request)
    {
        $booking = OneWayRentalBooking::where('id',$id)->first();
        if(!$booking)
            return redirect()->back()->withErrors(['From and To locations cannot be same.']);
        
        OneWayRentalBooking::where('id',$request->id)->update([
            'updated_by'=>Auth::user()->id,
            'status'=>"Booking Cancelled",
            'returned_by'=>Auth::user()->id,
            'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString(),
        ]);

        
        OneWayRentalBike::where('id',$booking->bike_id)->update([
            'current_location'=>$booking->from_location,
            'current_location_latitude'=>$booking->from_location_latitude,
            'current_location_longitude'=>$booking->from_location_longitude,
            'current_status'=>"Available for booking"
        ]);
        
        
        $pricingInfo = OneWayRentalPricingInfo::where('booking_id',$booking->id)->first();
        
        if(!(!$pricingInfo))
        {
            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"C",
                'new_or_edit'=>"D",
                'amount'=>$pricingInfo->price_after_discount,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>10,
                'status'=>"na",
                'created_by'=>Auth::user()->id
            ]);

            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"C",
                'new_or_edit'=>"D",
                'amount'=>$pricingInfo->tax_amount,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>11,
                'status'=>"na",
                'created_by'=>Auth::user()->id
            ]);
        }        
        
        return redirect()->back()->withErrors(['Booking cancelled successfully.']);
        
    }
    
    
    public function indexOneWayRentalBooking(Request $request)
    {
        $bookings = OneWayRentalBooking::orderBy('id','desc')->take(500)->get();
        
        return view('admin.oneWayRentalBooking.index',compact('bookings'));
        
    }
    
    public function applyOneWayRentalPromoCode(Request $request)
    {
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
        if($request->coupon == "TEST")
        {
            $data = [
                'status'=>true,
                'message'=>"Coupon applied successfully.",
                'final_price'=>$request->price,
                'price_with_tax'=>round(($request->price*(1+($taxDetails->amount/100))),2)
                
            ];
        }
        else
        {
            $data = [
                'status'=>false,
                'message'=>"Invalid Coupon",
                'final_price'=>$request->price,
                'price_with_tax'=>round(($request->price*(1+($taxDetails->amount/100))),2)
                
            ];
            
        }
        
        return $this->respondWithSuccess($data);
    }
    
    
    
    
    public function oneWayRentalBookingMessageUrl($result, $id, Request $request)
    {
        
        return view('front/app/oneWayRentalBookingMessage',compact('result','id'));
        
    }
    
    public function oneWayRentalBookingIndex(Request $request)
    {
        if(isset($request->search))
        {
            $bookings = OneWayRentalBooking::where('reference_id','LIKE','%'.$request->search.'%')->get();
        }
        
        $bookings = OneWayRentalBooking::orderBy('id','desc')->get()->take(500);
        
        
        return view('admin.a2bMovement.index',compact('bikeNumbers','locations'));
        
        
    }
    
    public function movementIndex(Request $request)
    {
        $excludedBikes = OneWayRentalBooking::whereIn('status',array('Booking Made','Bike delivered'))->get()->pluck('bike_id')->toArray();
        $bikeNumbers = OneWayRentalBike::whereNotIn('id',$excludedBikes)->where('current_location','<>','0')->get();
        $locations = OneWayRentalLocation::where('is_spoke',0)->get();                                                      
        $locationsArray = $locations->keyBy('id')->toArray();
        
        
                                                      
        foreach($bikeNumbers as $bikeNumber)
        {
            if($bikeNumber->current_location == 0)
            {
                $bikeNumber['number_with_location'] = $bikeNumber->bike_number." - In Transit";
            }
            elseif(array_key_exists($bikeNumber->current_location,$locationsArray))
            {
                $bikeNumber['number_with_location'] = $bikeNumber->bike_number." from ".$locationsArray[$bikeNumber->current_location]['name'];
            }
            else
            {
                $bikeNumber['number_with_location'] = $bikeNumber->bike_number." - Area deleted";
            }
        }
        
        $bookings = OneWayRentalMovementOrder::orderBy('id','desc')->take(500)->get();
        
        $jqueryOverride = "<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>";
        
        return view('admin.a2bMovement.index',compact('bikeNumbers','locations','bookings','jqueryOverride'));
    }
    
    public function createMovementOrder(Request $request)
    {
         $rules = array(
            'bike_id' => 'required',
            'to_location' => 'required',
            'priority' => 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $bike = OneWayRentalBike::where('id',$request->bike_id)->first();
        
        if(!$bike)
        {
            return redirect()->back()->withErrors(['Could not find bike.']);
        }
        else
        {
            if($bike->current_location == $request->to_location)
            {
                return redirect()->back()->withErrors(['From and To locations have to be different.']);
            }
            else
            {   
                if($bike->current_location == 0)
                {
                    return redirect()->back()->withErrors(['Cannot create movement order for a bike in transit.']);
                }
                
                $toLocation = OneWayRentalLocation::where('id',$request->to_location)->first();
                
                $movementOrder = OneWayRentalMovementOrder::create([
                    'from_location'=>$bike->current_location, 
                    'to_location'=>$request->to_location, 
                    'model_id'=>$bike->model_id, 
                    'bike_id'=>$request->bike_id, 
                    'priority'=>$request->priority, 
                    'status'=>"Movement Order Created", 
                    'notes'=>$request->notes, 
                    'created_by'=>Auth::user()->id, 
                    'from_location_longitude'=>$bike->current_location_longitude, 
                    'from_location_latitude'=>$bike->current_location_latitude, 
                    'to_location_longitude'=>$toLocation->longitude, 
                    'to_location_latitude'=>$toLocation->latitude, 
                ]);
                
                if($request->priority == 1)
                {   
                    
                    OneWayRentalBike::where('id',$bike->id)->update([
                        'current_location'=>0, 
                        'current_location_latitude'=>0, 
                        'current_location_longitude'=>0, 
                        'updated_by'=>Auth::user()->id,
                        'current_status'=>"Bike block for movement"
                    ]);
                }
                
                return redirect()->back()->withErrors(['Success!! Movement order created.']);
            }
        }
        
        
    }
    
    public function deleteMovementOrder($id, Request $request)
    {
        $movementOrder = OneWayRentalMovementOrder::where('id',$id)->first();
        if(!$movementOrder)
            return redirect()->back()->withErrors(["Movement order not found."]);
        
        if($movementOrder->status != "Movement Order Created")
            return redirect()->back()->withErrors(["Movement order cannot be deleted."]);
        
        
        OneWayRentalMovementOrder::where('id',$request->id)->update([
            'updated_by'=>Auth::user()->id,
            'status'=>"Movement Cancelled",
            'returned_by'=>Auth::user()->id,
            'returned_at'=>Carbon::now('Asia/Kolkata')->toDateTimeString()
        ]);
        
        
        
        if($movementOrder->priority == 1)
        {
            
            OneWayRentalBike::where('id',$movementOrder->bike_id)->update([
                'current_location'=>$movementOrder->from_location,
                'current_location_latitude'=>$movementOrder->from_location_latitude,
                'current_location_longitude'=>$movementOrder->from_location_longitude,
                'current_status'=>"Available for booking"
            ]);

        }
        return redirect()->back()->withErrors(["Movement order deleted successfully."]);
    }
    
    
    public function getPurposeListForQuestionnaire(Request $request)
    {
        $optionsListObj = Parameters::getParameter('questionnaire_options');
        $optionsArray = explode(',',$optionsListObj);
        $optionsList = [];
        foreach($optionsArray as $optionsArrayEl)
        {
            array_push($optionsList,["value"=>$optionsArrayEl]);
        }
        
        $timingsListObj = json_decode(Parameters::getParameter('questionnaire_timings'));
        $timingsList = [];
        foreach($timingsListObj as $timingsEl)
        {
            array_push($timingsList,["value"=>$timingsEl->string]);
        }
        
        return $this->respondWithSuccess(['options'=>$optionsList,'timings'=>$timingsList]);
    }
    
    
    public function questionnaireResponse(Request $request)
    {
        if(isset($request->gcm_token))
        {
            $user = DB::table('on_demand_a2b_questionnaire_user')->where('gcm_token',$request->gcm_token)->first();
            
            if(!$user)
            {
                $userId = DB::table('on_demand_a2b_questionnaire_user')->insertGetId([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'phone_number'=>$request->phone_number,
                    'gcm_token'=>$request->gcm_token
                ]);
            }
            else
            {
                $userId = $user->id;
            }
        }
        else
        {
            $userId = DB::table('on_demand_a2b_questionnaire_user')->insertGetId([
                'name'=>$request->name,
                'email'=>$request->email,
                'phone_number'=>$request->phone_number
            ]);
        }
        
        
        foreach($request->location_purpose_array as $locationPurposeEl)
        {
            if(isset($locationPurposeEl['time_options']))
                $timingsStrArray = explode(',',$locationPurposeEl['time_options']);
            else
                $timingsStrArray = [];
            
            $timingsListObj = json_decode(Parameters::getParameter('questionnaire_timings'));
            $timingsArray = [];
            foreach($timingsStrArray as $timingsStr)
            {
                foreach($timingsListObj as $timingsListObjEl)
                {                    
                    if($timingsListObjEl->string == $timingsStr)
                    {
                        if(!(in_array("Through-out the day",$timingsStrArray) || in_array("Weekends Only",$timingsStrArray)))
                        {
                            $timingValues = explode(',',$timingsListObjEl->value);
                            foreach($timingValues as $timingValuesEl)
                            {
                                array_push($timingsArray,$timingValuesEl);
                            }
                        }
                        else
                        {
                            if($timingsStr == "Through-out the day" || $timingsStr == "Weekends Only")
                                array_push($timingsArray,$timingsListObjEl->value);   
                        }
                    }
                }
            }
            
            $timingsArrayStr = implode(',',$timingsArray);
            
            DB::table('on_demand_a2b_questionnaire_location_purpose_array')->insert([
                'location_name'=>$locationPurposeEl['location_name'], 
                'location_latitude'=>$locationPurposeEl['location_latitude'], 
                'location_longitude'=>$locationPurposeEl['location_longitude'], 
                'purpose'=>$locationPurposeEl['purpose'],
                'timings_array'=>$timingsArrayStr,
                'questionnaire_user_id'=>$userId
            ]);
        }
        
        return $this->respondWithSuccess(['status'=>true]);
    }
    
    
    public function getOneWayRentalLocations(Request $request)
    {
        $locations = OneWayRentalLocation::where('status',1)->get();
        $hubSpokes = OneWayRentalLocationHubSpoke::where('deleted',0)->get(["hub_area","spoke_area"]);
        $hubSpokesBySpokeId = $hubSpokes->groupBy("spoke_area");
        $clusterByHubId = $hubSpokes->groupBy("hub_area");
        $bikes =  OneWayRentalBike::where('status',1)->where('current_location',"<>",0)->get();
        $bikesByLocation = $bikes->groupBy('current_location');
        $locationsWithBikes = $bikes->pluck('current_location')->all();
        $locationList = [];
        $locationsArray = $locations->keyBy('id');
                
        if(isset($request->latitude))
        {
            $distanceArray = [];
                
            foreach($locations as $location)
            {
                $distance = sqrt((($request->latitude - $location->latitude)*($request->latitude - $location->latitude))+(($request->longitude - $location->longitude)*($request->longitude - $location->longitude)));
                
                $distanceArray[$location->id] = $distance;
            }
            
            asort($distanceArray);
            if(count($distanceArray)>=3)
                $store = 3;
            elseif(count($distanceArray)>=2)
                $store = 2;
            
            $i=1;
            
            $closestAvailableStore = 1000000;
            $closest_latitude = "";
            $closest_longitude = "";
            
            
            
            foreach($distanceArray as $key=>$distance)
            {
                if(($locationsArray[$key]->is_spoke == 0) && !(isset($clusterByHubId[$key])))
                {
                    if(isset($bikesByLocation[$locationsArray[$key]->id]))
                    {
                        $bike_available = true;
                        $closestAvailableStore = $distance;
                    }
                    else
                        $bike_available = false;
                }
                else
                {
                    if(isset($hubSpokesBySpokeId[$locationsArray[$key]->id]) || isset($clusterByHubId[$key]))
                    {
                        $areasInCluster = [];
                        
                        if(isset($clusterByHubId[$key]))
                        {
                            array_push($areasInCluster,$key); 
                            foreach($clusterByHubId[$key] as $hubSpoke2)
                            {
                                array_push($areasInCluster,$hubSpoke2->spoke_area);      
                            }
                        }
                        else
                        {
                            foreach($hubSpokesBySpokeId[$locationsArray[$key]->id] as $hubSpoke)
                            {
                                array_push($areasInCluster,$hubSpoke->hub_area); 
                                foreach($clusterByHubId[$hubSpoke->hub_area] as $hubSpoke2)
                                {
                                    array_push($areasInCluster,$hubSpoke2->spoke_area);      
                                }
                            }
                        }
                        
                        
                        $areasInCluster = array_unique($areasInCluster);
                        
                        if(count(array_intersect($areasInCluster,$locationsWithBikes)) > 0)
                        {
                            $bike_available = true;
                            $closestAvailableStore = $distance;
                        }
                        else
                        {
                            $bike_available = false;
                        }
                    }
                    else
                    {
                        $bike_available = false;
                    }
                }
                
                
                if($i<=$store && $bike_available)
                {
                    if($i == 1)
                    {
                        $closest_latitude = $locationsArray[$key]->latitude;
                        $closest_longitude = $locationsArray[$key]->longitude;
                    }
                    $display = true;
                    $i++;
                }
                else
                    $display = false;
                
                array_push($locationList,["id"=>$locationsArray[$key]->id,"name"=>$locationsArray[$key]->name,"bike_available"=>$bike_available,"latitude"=>$locationsArray[$key]->latitude,"longitude"=>$locationsArray[$key]->longitude,"display"=>$display,"from_suffix"=>"","to_suffix"=>""]);
                
                
            }
            
            
            if($request->header()['authorization']==[""])
            {
                $userId = 0;
            }
            else
            {
                if($request->header()['authorization']==["Bearer"])
                {
                    $userId = 0;
                }
                else
                {
                    $user = UserController::getUserByToken();
                    $userId = $user->id;
                }
            }
            
            
            
            
            DB::table('on_demand_a2b_location_request')->insert([
                'user_id'=>$userId,
                'latitude'=>$request->latitude,
                'longitude'=>$request->longitude,
                'closest_available'=>$closestAvailableStore,
                'closest_latitude'=>$closest_latitude,
                'closest_longitude'=>$closest_longitude,
                'created_by'=>$userId
            ]);
            
            
        }
        else
        {
            
            foreach($locations as $location)
            {
                if(isset($bikesByLocation[$location->id]))
                    array_push($locationList,["id"=>$location->id,"name"=>$location->name,"bike_available"=>true]);
                else
                    array_push($locationList,["id"=>$location->id,"name"=>$location->name,"bike_available"=>false]);
            }
            
        }
        
        return $this->respondWithSuccess(['locations'=>$locationList]);
    }
    
    public function getOneWayRentalModelsAndPrices(Request $request)
    {
        $rules = array(
        'from_location' => 'required|exists:on_demand_a2b_locations,id',
        'to_location' => 'required|exists:on_demand_a2b_locations,id'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $fromLocation = OneWayRentalLocation::where('id',$request->from_location)->first();
        $toLocation = OneWayRentalLocation::where('id',$request->to_location)->first();
        
        if($fromLocation->is_spoke == 0)
        {
            
            $fromLocationIds = OneWayRentalLocationHubSpoke::where('deleted',0)->where('hub_area',$request->from_location)->get()->pluck('spoke_area')->all();
            array_push($fromLocationIds,$request->from_location);
            
        }
        else
        {
            $hubIds = explode(",",$fromLocation->getHubAreaIds());
        }
        
        $model = BikeModel::where('id',env('SCOOTERMODEL'))->first();
        $modelList = [];
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
        
        
        if(!$model)
            $minAge = "I am over 21";
        else
            $minAge = "I am over ".$model->age_limit;

        $conditionsParameter = Parameters::getParameter('dynamic_conditions');

        if($conditionsParameter == "false")
        {
            $conditions = array(
                    [
                        'id'=>2,
                        'text'=>$minAge,
                        'required'=>true
                    ],
                    [
                        'id'=>3,
                        'text'=>"I have a valid driving license and required documents",
                        'required'=>true
                    ],
                    [
                        'id'=>4,
                        'text'=>"I agree to <a href=\"".env('HOME_URL')."/termsandconditions\">Terms and Conditions</a>",
                        'required'=>true
                    ]
                );
        }
        else
        {
            $conditions = array();
        }

        if($request->from_location!=$request->to_location)
        {
            $url = $model->getThumbnailImg()->full;

            $response = Curl::to("https://maps.googleapis.com/maps/api/directions/json?origin=".$fromLocation->latitude.",".$fromLocation->longitude."&destination=".$toLocation->latitude.",".$toLocation->longitude."&key=".env('GOOGLEMAPSAPI'))->post();

            $decoded = json_decode($response,true);
            $distancePre = $decoded['routes'][0]['legs'][0]['distance']['value'];
            $durationPre = $decoded['routes'][0]['legs'][0]['duration']['value'];

            $distance = round(($distancePre/1000),1);
            $duration = ceil($durationPre/60);


            $priceRes = Self::calculateRental($model->id,$distance,$duration);
            $price = $priceRes['price'];
            $priceObj = $priceRes['priceObj'];

            array_push($modelList,[
                "id"=>$model->id,
                "name"=>$model->getBikeMakeName($model->bike_make_id)." - ".$model->bike_model,
                "image_url"=>env('HTTPSORHTTP').":".$url,
                "price"=>$price,
                "estimated_distance"=>$distance." KM",
                "estimated_duration"=>$duration." mins",
                'tax_string' => $taxDetails->string,
                'tax_amount' => $taxDetails->amount,
                'price_with_tax' =>  round($price*(1+($taxDetails->amount/100)),2),
                'price_per_km' => $priceObj->price_per_km,
                'price_per_minute' => $priceObj->price_per_min,
                'conditions'=>$conditions,
                'pay_later'=>env('ONEWAYPAYLATER')
            ]);
        }
        else
        {
            $url = $model->getThumbnailImg()->full;
            array_push($modelList,[
                "id"=>$model->id,
                "name"=>$model->getBikeMakeName($model->bike_make_id)." - ".$model->bike_model,
                "image_url"=>env('HTTPSORHTTP').":".$url,
                "price"=>env('SAMELOCATIONRENTAL'),
                "estimated_distance"=>"Based on usage",
                "estimated_duration"=>"Based on usage",
                'tax_string' => $taxDetails->string,
                'tax_amount' => $taxDetails->amount,
                'price_with_tax' =>  round(env('SAMELOCATIONRENTAL')*(1+($taxDetails->amount/100)),2),
                'conditions'=>$conditions,
                'pay_later'=>env('ONEWAYPAYLATER')
            ]);
        }
        
        
        
        
        return $this->respondWithSuccess(['models'=>$modelList, "from_location"=>$fromLocation, "to_location"=>$toLocation]);
    }
    
    
    public function getOneWayRentalModelsAndPricesForLatLong(Request $request)
    {
        $rules = array(
            'from_latitude' => 'required',
            'to_latitude' => 'required',
            'from_longitude' => 'required',
            'to_longitude' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $modelList = [];
        $model = BikeModel::find(10);
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
        
        $minAge = "I am over 18";

        $conditionsParameter = Parameters::getParameter('dynamic_conditions');

        if($conditionsParameter == "false")
        {
            $conditions = array(
                    [
                        'id'=>2,
                        'text'=>$minAge,
                        'required'=>true
                    ],
                    [
                        'id'=>3,
                        'text'=>"I have a valid driving license and required documents",
                        'required'=>true
                    ],
                    [
                        'id'=>4,
                        'text'=>"I agree to <a href=\"".env('HOME_URL')."/termsandconditions\">Terms and Conditions</a>",
                        'required'=>true
                    ]
                );
        }
        else
        {
            $conditions = array();
        }

        if(($request->from_latitude!=$request->to_latitude)&&($request->from_longitude!=$request->to_longitude))
        {
            $url = $model->getThumbnailImg()->full;

            $response = Curl::to("https://maps.googleapis.com/maps/api/directions/json?origin=".$request->from_latitude.",".$request->from_longitude."&destination=".$request->to_latitude.",".$request->to_longitude."&key=".env('GOOGLEMAPSAPI'))->post();

            $decoded = json_decode($response,true);
            $distancePre = $decoded['routes'][0]['legs'][0]['distance']['value'];
            $durationPre = $decoded['routes'][0]['legs'][0]['duration']['value'];

            $distance = round(($distancePre/1000),1);
            $duration = ceil($durationPre/60);


            $priceRes = Self::calculateRental($model->id,$distance,$duration);
            $price = $priceRes['price'];
            $priceObj = $priceRes['priceObj'];

            array_push($modelList,[
                "id"=>$model->id,
                "name"=>$model->getBikeMakeName($model->bike_make_id)." - ".$model->bike_model,
                "image_url"=>env('HTTPSORHTTP').":".$url,
                "price"=>$price,
                "estimated_distance"=>$distance." KM",
                "estimated_duration"=>$duration." mins",
                'tax_string' => $taxDetails->string,
                'tax_amount' => $taxDetails->amount,
                'price_with_tax' =>  round($price*(1+($taxDetails->amount/100)),2),
                'price_per_km' => $priceObj->price_per_km,
                'price_per_minute' => $priceObj->price_per_min,
                'conditions'=>$conditions,
                'pay_later'=>env('ONEWAYPAYLATER')
            ]);
        }
        else
        {
            $url = $model->getThumbnailImg()->full;
            array_push($modelList,[
                "id"=>$model->id,
                "name"=>$model->getBikeMakeName($model->bike_make_id)." - ".$model->bike_model,
                "image_url"=>env('HTTPSORHTTP').":".$url,
                "price"=>env('SAMELOCATIONRENTAL'),
                "estimated_distance"=>"Based on usage",
                "estimated_duration"=>"Based on usage",
                'tax_string' => $taxDetails->string,
                'tax_amount' => $taxDetails->amount,
                'price_with_tax' =>  round(env('SAMELOCATIONRENTAL')*(1+($taxDetails->amount/100)),2),
                'conditions'=>$conditions,
                'pay_later'=>env('ONEWAYPAYLATER')
            ]);
        }
            
        return $this->respondWithSuccess(['models'=>$modelList]);
    }
    
    
    public function questionnaireShareElements(Request $request)
    {
        return $this->respondWithSuccess(['image_url'=>Parameters::getParameter('questionnare_image'),'text'=>Parameters::getParameter('questionnare_text')]);
    }
    
    public function questionnaireShareText(Request $request)
    {
        return $this->respondWithSuccess(['text'=>Parameters::getParameter('questionnare_share_text')]);
    }
    
    
    public static function toggleAreaStatus($locationId,$newStatus)
    {
        if(Auth::check())
        {
            $userId = Auth::user()->id;
        }
        else
        {
            $userId = 0;
        }
        
        $availbleStatusArray = ["Available for booking","Bike made active"];
        
        if(!$newStatus)
            $bikes = OneWayRentalBike::where('current_location',$locationId)->whereIn('current_status',$availbleStatusArray)->get();
        else
            $bikes = OneWayRentalBike::where('current_location',$locationId)->where('current_status','Bike made inactive by inactivating area')->get();
        
        foreach($bikes as $bike)
        {
            
            
            if($newStatus)
            {
                
                $currentStatus = "Bike made active";
                
                $currentStatusLog = "Bike made active";
                
            }
            else
            {
                
                
                $currentStatus = "Bike made inactive by inactivating area";
                
                $currentStatusLog = "Bike made inactive by inactivating area";
            }
                
            
            OneWayRentalBike::where('id',$bike->id)->update([
                'status'=>$newStatus,
                'current_status'=>$currentStatus,
                'updated_by'=>$userId
            ]);
            
            DB::table('on_demand_a2b_bike_activity')->insert([
                'bike_id'=>$bike->id, 
                'old_status'=>$bike->current_status, 
                'new_status'=>$currentStatusLog, 
                'old_status_bool'=>$bike->status, 
                'new_status_bool'=>$newStatus, 
                'current_location'=>$bike->current_location, 
                'current_location_latitude'=>$bike->current_location_latitude, 
                'current_location_longitude'=>$bike->current_location_longitude, 
                'created_by'=>$userId
            ]);
        }
        
    }
    
    public function toggleAllAreaStatus(Request $request)
    {
        if(isset($request->hour))
        {
            $hour = $request->hour;
        }
        else
        {
            $hour = Carbon::now()->hour;
        }
        
        $hourStr = str_pad($hour,2,"0",STR_PAD_LEFT).":00:00";
        
        $openOneWayRentalLocations = OneWayRentalLocation::where('open_time',$hourStr)->where('status',1)->where('is_spoke',0)->get();
        
        foreach($openOneWayRentalLocations as $openOneWayRentalLocation)
        {
            $response = Self::toggleAreaStatus($openOneWayRentalLocation->id,1);
            OneWayRentalLocation::where('id',$openOneWayRentalLocation->id)->update(['status'=>1]);
        }
        
        $closeOneWayRentalLocations = OneWayRentalLocation::where('close_time',$hourStr)->where('status',1)->where('is_spoke',0)->get();
        
        foreach($closeOneWayRentalLocations as $closeOneWayRentalLocation)
        {
            $response = Self::toggleAreaStatus($closeOneWayRentalLocation->id,0);
        }
        
        return "done";
    }
    
    
    public function toggleAllActiveAreaBikeStatus(Request $request)
    {
        if(isset($request->hour))
        {
            $hour = $request->hour;
        }
        else
        {
            $hour = Carbon::now()->hour;
        }
        
        $hourStr = str_pad($hour,2,"0",STR_PAD_LEFT).":00:00";
        
        $openOneWayRentalLocations = OneWayRentalLocation::where('open_time',$hourStr)->where('status',1)->where('is_spoke',0)->get();
        
        foreach($openOneWayRentalLocations as $openOneWayRentalLocation)
        {
            $response = Self::toggleAreaStatus($openOneWayRentalLocation->id,1);
        }
        
        $closeOneWayRentalLocations = OneWayRentalLocation::where('close_time',$hourStr)->where('status',1)->where('is_spoke',0)->get();
        
        foreach($closeOneWayRentalLocations as $closeOneWayRentalLocation)
        {
            $response = Self::toggleAreaStatus($closeOneWayRentalLocation->id,0);
        }
        
        return "done";
    }
    
    
    
    public function indexOneWayRentalLocationSpoke(Request $request)
    {
        $pageTitle = "One Way Rental Location";
        $objectTableWidth = 0;
        $objectTableArray['headers'] = [];
        $objectTableArray['headers'] =  [
                                            ["width"=>80,"title"=>"ID"],
                                            ["width"=>120,"title"=>"Name"],
                                            ["width"=>200,"title"=>"Address"],
                                            ["width"=>120,"title"=>"Latitude"],
                                            ["width"=>120,"title"=>"Longitude"],
                                            ["width"=>200,"title"=>"Hub Areas"],
                                            ["width"=>80,"title"=>"Status"],
                                            ["width"=>80,"title"=>"Edit"],
                                            ["width"=>80,"title"=>"Delete"]
                                        ];
        
        foreach($objectTableArray['headers'] as $header)
        {
            $objectTableWidth += $header['width'];
        }
        
        $locationObjects = OneWayRentalLocation::where('is_spoke',1)->get();
        
        $objectTableArray['body'] = [];
        
        foreach($locationObjects as $locationObject)
        {
            $body = [
                        ["type"=>"text","value"=>["text"=>$locationObject->id,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->name,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->address,"pre"=>"true"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->latitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->longitude,"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>$locationObject->getHubAreas(),"pre"=>"false"]],
                        ["type"=>"text","value"=>["text"=>($locationObject->status?"Active":"Inactive"),"pre"=>"false"]],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-info wk-btn",
                                                            "js_class"=>"edit_location_btn",
                                                            "id"=>"edit_location_id",
                                                            "data_id"=>$locationObject->id,
                                                            "icon"=>"fa fa-pencil-square-o",
                                                            "text"=>"Edit"
                                                        ]
                                                    ]
                        ],
                        ["type"=>"button","value"=>
                                                    [
                                                        [
                                                            "style_class"=>"btn btn-danger wk-btn",
                                                            "js_class"=>"delete_location_btn",
                                                            "id"=>"delete_location_id",
                                                            "data_id"=>$locationObject->id,
                                                            "icon"=>"fa fa-trash-o",
                                                            "text"=>"Delete"
                                                        ]
                                                    ]
                        ]
                    ];
            
            array_push($objectTableArray['body'],$body);
            
        }
        
        $addObject = [];
        $addObject['modal_name'] = "add_location_modal";
        $addObject['add_button_text'] = "Add One Way Rental Location";
        $addObject['modal_title'] = "Add One Way Rental Location";
        $addObject['add_url'] = "/admin/one-way-rental-location-spoke/add";
        $addObject['add_modal_form_items'] = [];
        
        $hubsObjs = OneWayRentalLocation::where('is_spoke',0)->where('status',1)->get();
        $hubsList = [];
        foreach($hubsObjs as $hubsObj)
        {
            array_push($hubsList,['value'=>$hubsObj->id,'text'=>$hubsObj->name]);
        }
        
        $addObject['add_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Name","name"=>"name_add"],
                ["type"=>"textarea","label"=>"Address","name"=>"address_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Latitude","name"=>"latitude_add"],
                ["type"=>"input", "input_type"=>"text","label"=>"Longitude","name"=>"longitude_add"],
                ["type"=>"multiSelect","label"=>"Hubs", "name"=>"hubs_add", "id"=>"hubs_id_add", "default_value"=>"not_set", "default_text"=>"-- Select Hubs --", "select_items"=>$hubsList, "selection_header"=>"Hubs","selected_header"=>"Hubs Assigned"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_add", "ckbxId1"=>"status_ckbxId1_add", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_add", "ckbxLabel2"=>"Inactive"]
            ];
        
        $editObject = [];
        $editObject['modal_name'] = "edit_location_modal";
        $editObject['edit_btn_class'] = "edit_location_btn";
        $editObject['modal_title'] = "Edit One Way Rental Location";
        $editObject['edit_url'] = "/admin/one-way-rental-location-spoke/edit";
        $editObject['ajax_url'] = "/admin/one-way-rental-location-spoke/details";
        $editObject['edit_modal_form_items'] = [];
        
        $editObject['edit_modal_form_items'] = 
            [
                ["type"=>"input", "input_type"=>"text","label"=>"Name","name"=>"name_edit"],
                ["type"=>"textarea","label"=>"Address","name"=>"address_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Latitude","name"=>"latitude_edit"],
                ["type"=>"input", "input_type"=>"text","label"=>"Longitude","name"=>"longitude_edit"],
                ["type"=>"multiSelect","label"=>"Hubs", "name"=>"hubs_edit", "id"=>"hubs_id_edit", "default_value"=>"not_set", "default_text"=>"-- Select Hubs --", "select_items"=>$hubsList, "selection_header"=>"Hubs","selected_header"=>"Hubs Assigned"],
                ["type"=>"checkbox", "label"=>"Status", "name"=>"status_edit", "ckbxId1"=>"status_ckbxId1_edit", "ckbxLabel1"=>"Active", "ckbxId2"=>"status_ckbxId2_edit", "ckbxLabel2"=>"Inactive"],
            
                
            ];
        
        
        $deleteObject = [];
        $deleteObject['modal_name'] = "delete_location_modal";
        $deleteObject['delete_btn_class'] = "delete_location_btn";
        $deleteObject['modal_title'] = "Delete One Way Rental Location";
        $deleteObject['delete_url'] = "/admin/one-way-rental-location-spoke/delete";
        $deleteObject['delete_description'] = "Are you sure, you want to delete this One Way Rental Location?";
        
        
        return view('admin.template.index',compact('objectTableArray','objectTableWidth','pageTitle','addObject','editObject','deleteObject')); 
        
    }
    
    public function addOneWayRentalLocationSpoke(Request $request)
    {
        $rules = array(
            'name_add' => 'required',
            'address_add' => 'required',
            'latitude_add' => 'required',
            'longitude_add' => 'required',
            'status_add' => 'required',
            'hubs_add' => 'required'
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $spoke = OneWayRentalLocation::create([
                'name'=>$request->name_add,
                'address'=>$request->address_add,
                'latitude'=>$request->latitude_add,
                'longitude'=>$request->longitude_add,
                'is_spoke'=>1,
                'created_by'=>Auth::user()->id,
                'status'=>$request->status_add,
        ]);
        
        foreach($request->hubs_add as $hubId)
        {
            DB::table('on_demand_a2b_hub_spoke')->insert([
                'hub_area'=>$hubId,
                'spoke_area'=>$spoke->id,
                'created_by'=>Auth::user()->id
            ]);
        }
        
        return redirect()->back();
    }
    
    public function detailsOneWayRentalLocationSpoke(Request $request)
    {
        $location = OneWayRentalLocation::where('id',$request->id)->first();
        $data = [
                ["type"=>"input","name"=>"name_edit","value"=>$location->name],
                ["type"=>"textarea","name"=>"address_edit","value"=>$location->address],
                ["type"=>"input","name"=>"latitude_edit","value"=>$location->latitude],
                ["type"=>"input","name"=>"longitude_edit","value"=>$location->longitude],
                ["type"=>"multiselect", "id"=>"hubs_id_edit", "name"=>"hubs_edit","value"=>$location->getHubAreaIds()],
                ["type"=>"checkbox","status"=>$location->status,"ckbxId1"=>"status_ckbxId1_edit","ckbxId2"=>"status_ckbxId2_edit"]
        ];
        
        return $this->respondWithSuccess($data); 
    }
    
    public function editOneWayRentalLocationSpoke($id, Request $request)
    {
        $oneWayRentalLocation = OneWayRentalLocation::where('id',$id)->first();
        if(!$oneWayRentalLocation)
            return redirect()->back()->withErrors(['No Location detected.']);
        
        
        OneWayRentalLocation::where('id',$id)->update([
            'updated_by'=>Auth::user()->id,
            'name'=>$request->name_edit,
            'address'=>$request->address_edit,
            'latitude'=>$request->latitude_edit,
            'longitude'=>$request->longitude_edit,
            'status'=>$request->status_edit,
        ]);
        
        $hubSpokes = DB::table('on_demand_a2b_hub_spoke')->where('spoke_area',$oneWayRentalLocation->id)->where('deleted',0)->get();
        $hubSpokesHubIds = collect($hubSpokes)->pluck('hub_area')->toArray();
        
        foreach($request->hubs_edit as $hubId)
        {
            if(!in_array($hubId,$hubSpokesHubIds))
            {
                DB::table('on_demand_a2b_hub_spoke')->insert([
                    'hub_area'=>$hubId,
                    'spoke_area'=>$oneWayRentalLocation->id,
                    'created_by'=>Auth::user()->id
                ]);
            }
        }
        
        foreach($hubSpokesHubIds as $hubId)
        {
            if(!in_array($hubId,$request->hubs_edit))
            {
                DB::table('on_demand_a2b_hub_spoke')->where('hub_area',$hubId)->where('spoke_area',$oneWayRentalLocation->id)->update(['deleted'=>1,'deleted_at'=>Carbon::now()->toDateTimeString(),'deleted_by'=>Auth::user()->id]);
            }
        }
        
        
        return redirect()->back()->withErrors(['Location edited successfully.']);
    }
    
    public function deleteOneWayRentalLocationSpoke($id, Request $request)
    {
        $location = OneWayRentalLocation::where('id',$id)->first();
        
        DB::table('on_demand_a2b_hub_spoke')->where('spoke_area',$location->id)->update(['deleted'=>1,'deleted_at'=>Carbon::now()->toDateTimeString(),'deleted_by'=>Auth::user()->id]);
        
        DB::table('deleted_rows')->insert(["json_string"=>json_encode($location),"table_name"=>"on_demand_a2b_location","deleted_by"=>Auth::user()->id]);
        OneWayRentalLocation::where('id',$id)->update([
            'deleted'=>1,
            'deleted_by'=>Auth::user()->id,
            'deleted_at'=>Carbon::now()->toDateTimeString(),
        ]);
        return redirect()->back();
    }
    
    public static function createFinancialInfo($price,$priceObj,$promo_code,$payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$userId,$bookingId,$distance,$duration)
    {
        $promoWalletAmountRedeemed = 0;
        $walletBalanceApplied = 0;
        
        if($applied_wallet_amount > 0)
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $walletDetails = $dispatcher->with([
                'category'=>"AtoB",
                'device'=>"MOBILE",
                'amount_to_apply'=>$applied_wallet_amount,
                'price'=>$price,
                'coupon'=>$promo_code,
                'user_id'=>$userId
            ])->get('wallet/function_apply_amount');

            $walletBalanceApplied = $walletDetails['applied_amount'];
            $promoWalletAmountRedeemed = $walletDetails['promotional_credit_used'];
            $nonpromoWalletAmountRedeemed = $walletDetails['non_promotional_credit_used'];
            
            if($walletBalanceApplied>0)
                $walletTxnId = WalletController::createWalletBalanceEntry($userId,$walletBalanceApplied,"Redeemed for a One Way Rental Boooking",$bookingId,$userId);

            if($promoWalletAmountRedeemed>0)
            {
                $financialInfo = OneWayRentalFinancialInfo::create([
                    'booking_id'=>$bookingId,
                    'user_id'=>$userId,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>"N",
                    'amount'=>$promoWalletAmountRedeemed,
                    'payment_method'=>12,
                    'payment_id'=>$walletTxnId,
                    'payment_nature'=>1,
                    'status'=>"Paid",
                    'created_by'=>$userId
                ]);
            }

            if($nonpromoWalletAmountRedeemed>0)
            {
                $financialInfo = OneWayRentalFinancialInfo::create([
                    'booking_id'=>$bookingId,
                    'user_id'=>$userId,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>"N",
                    'amount'=>$nonpromoWalletAmountRedeemed,
                    'payment_method'=>13,
                    'payment_id'=>$walletTxnId,
                    'payment_nature'=>1,
                    'status'=>"Paid",
                    'created_by'=>$userId
                ]);
            }
        }
        
        if($pgTxnAmount > 0)
        {
            $financialInfo = OneWayRentalFinancialInfo::create([
                'booking_id'=>$bookingId,
                'user_id'=>$userId,
                'debit_or_credit'=>"C",
                'new_or_edit'=>"N",
                'amount'=>$pgTxnAmount,
                'payment_method'=>3,
                'payment_id'=>$pgTxnId,
                'payment_nature'=>1,
                'status'=>"COMPLETE",
                'created_by'=>$userId
            ]);
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
        
        $taxablePrice = $price - $promoWalletAmountRedeemed;
        $tax = round($taxablePrice*($taxDetails->amount/100),2);
        $priceWithTax = $price + $tax;
        
        OneWayRentalFinancialInfo::create([
            'booking_id'=>$bookingId,
            'user_id'=>$userId,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$price,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>2,
            'status'=>"na",
            'created_by'=>$userId
        ]);
        
        OneWayRentalFinancialInfo::create([
            'booking_id'=>$bookingId,
            'user_id'=>$userId,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$tax,
            'payment_method'=>0,
            'payment_id'=>"na",
            'payment_nature'=>3,
            'status'=>"na",
            'created_by'=>$userId
        ]);
        
        OneWayRentalPricingInfo::create([
            'booking_id'=>$bookingId,
            'pricing_id'=>$priceObj->id,
            'distance'=>$distance,
            'duration'=>$duration,
            'full_price'=>$price,
            'promo_discount'=> 0,
            'price_after_discount'=>$price,
            'promo_code'=>$promo_code,
            'tax_id'=>2,
            'promotional_balance_used'=>$promoWalletAmountRedeemed,
            'taxable_price'=>$taxablePrice,
            'tax_amount'=>$tax,
            'price_after_tax'=>$priceWithTax
        ]);

        return [
            "price"=>$price,
            "taxable_amount"=>$taxablePrice,
            "tax"=>$tax,
            "total_with_tax"=>$priceWithTax,
            "promo_balance_applied"=>$promoWalletAmountRedeemed,
            "total_wallet_balance_applied"=>$walletBalanceApplied,
            "payment_gateway_amount"=>$pgTxnAmount
        ];
        
    }
    
    
    public static function calculatePrice($price,$priceObj,$promo_code,$payment_method,$pgTxnAmount,$pgTxnId,$applied_wallet_amount,$userId)
    {
        $promoWalletAmountRedeemed = 0;
        $walletBalanceApplied = 0;
        
        if($applied_wallet_amount > 0)
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $walletDetails = $dispatcher->with([
                'category'=>"AtoB",
                'device'=>"MOBILE",
                'amount_to_apply'=>$applied_wallet_amount,
                'price'=>$price,
                'coupon'=>$promo_code,
                'user_id'=>$userId
            ])->get('wallet/function_apply_amount');

            $walletBalanceApplied = $walletDetails['applied_amount'];
            $promoWalletAmountRedeemed = $walletDetails['promotional_credit_used'];
            $nonpromoWalletAmountRedeemed = $walletDetails['non_promotional_credit_used'];
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[1];
        
        $taxablePrice = $price - $promoWalletAmountRedeemed;
        $tax = $taxablePrice*($taxDetails->amount/100);
        $priceWithTax = $price + $tax;
        
        return [
            "price"=>$price,
            "taxable_amount"=>$taxablePrice,
            "tax"=>$tax,
            "total_with_tax"=>$priceWithTax,
            "promo_balance_applied"=>$promoWalletAmountRedeemed,
            "total_wallet_balance_applied"=>$walletBalanceApplied
        ];
    }
    
    public static function sanitizePhoneNumber($number)
    {
        $phone_number = str_replace("+","",$number);
        $phone_number = str_replace(" ","",$phone_number);
        $phone_number = str_replace("-","",$phone_number);
        $phone_number = str_replace("(","",$phone_number);
        $phone_number = str_replace(")","",$phone_number);
        
        return $phone_number;
    }
    
    
    public static function sendEmailAndSMS($bike,$from_location,$to_location,$referenceId,$user,$estimated_distance,$estimated_duration,$totalPriceDetails)
    {
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');

        $model = BikeModel::where('id',$bike->model_id)->first();

        $userPhone = Self::sanitizePhoneNumber($user->mobile_num);

        $mapsLinkFromFrom = "https://www.google.co.in/maps/place/".$from_location->latitude.",".$from_location->longitude;
        $mapsLinkFromTo = "https://www.google.co.in/maps/place/".$to_location->latitude.",".$to_location->longitude;



        $urlConvResponseFrom = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>$mapsLinkFromFrom, 'key'=>env('GOOGLESHORTURLAPI')]))->post();

        $urlConvResponseTo = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>$mapsLinkFromTo, 'key'=>env('GOOGLESHORTURLAPI')]))->post();

        if($from_location->id == $bike->current_location)
        {
            $from_location_address = strip_tags($from_location->address);
            $to_location_address = strip_tags($to_location->address);

            $smsMessageUser = "One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed. Please pick it up at ".$from_location_address." The google maps link - ".json_decode($urlConvResponseFrom)->id." The contact person is ".$from_location->contact_person." Ph:".$from_location->contact_number." Please drop it off at ".$to_location_address." The google maps link - ".json_decode($urlConvResponseTo)->id." The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number;
        }
        else
        {            
            $hub = OneWayRentalLocation::where('id',$bike->current_location)->first();
            $from_location_address = strip_tags($from_location->address);
            $to_location_address = strip_tags($to_location->address);

            $smsMessageUser = "One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed. Please pick it up at ".$from_location_address." The google maps link - ".json_decode($urlConvResponseFrom)->id." The contact person is ".$hub->contact_person." Ph:".$hub->contact_number." Please drop it off at ".$to_location_address." The google maps link - ".json_decode($urlConvResponseTo)->id." The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number;
        }



        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$userPhone, 'message'=>$smsMessageUser, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }


        if($from_location->id == $bike->current_location)
        {
            $phone_number = Self::sanitizePhoneNumber($from_location->contact_number);

            $smsMessage = "One Way rental Booking ID - ".$referenceId." is made for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." ".$bike->bike_number." from ".$from_location->name." to ".$to_location->name.". The customer's name is ".$user->first_name." ".$user->last_name." ph:".$user->mobile_num;
        }
        else
        {
            $phone_number = Self::sanitizePhoneNumber($hub->contact_number);

            $smsMessage = "One Way rental Booking ID - ".$referenceId." is made for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." ".$bike->bike_number." from ".$from_location->name." to ".$to_location->name.". The customer's name is ".$user->first_name." ".$user->last_name." ph:".$user->mobile_num." Please move the bike from ".$hub->name." immediately";
        }


        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }

        $phone_number = Self::sanitizePhoneNumber($to_location->contact_number);

        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }

        if($from_location->id == $to_location->id)
        {
            $estimedDurationDistanceMessage = "";
        }
        else
        {
            $estimedDurationDistanceMessage = "<p>The estimated travel time is ".$estimated_distance."KM and estimated time for the trip is ".$estimated_duration." minutes.</p>";
        }




        if($from_location->id == $bike->current_location)
        {
            $emailMessageUser = "<p>One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed.</p><p>Please pick it up at ".$from_location_address." The google maps link - ".json_decode($urlConvResponseFrom)->id."</p><p>The contact person is ".$from_location->contact_person." Ph:".$from_location->contact_number."</p>".$estimedDurationDistanceMessage."<p>Please drop it off at ".$to_location_address." The google maps link - ".json_decode($urlConvResponseTo)->id."</p><p> The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number."</p>";
        }
        else
        {
            $emailMessageUser = "<p>One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed.</p><p>Please pick it up at ".$from_location_address." The google maps link - ".json_decode($urlConvResponseFrom)->id."</p><p>The contact person is ".$hub->contact_person." Ph:".$hub->contact_number."</p><p>The estimated travel time is ".$estimated_distance."KM and estimated time for the trip is ".$estimated_duration." minutes.</p>"."<p>Please drop it off at ".$to_location_address." The google maps link - ".json_decode($urlConvResponseTo)->id."</p><p> The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number."</p>";
        }

        $mailData = [
            'userEmail'=>$user->email,
            'fname'=>$user->first_name." ".$user->last_name,
            'email_body'=>$emailMessageUser,
            'reference_id'=>$referenceId
        ];

        Mail::send('emails.user_a2b_booking_mail',$mailData,function($message) use ($mailData) {
          $message->to($mailData['userEmail']);
          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
          $message->subject(env('SITENAMECAP').' One Way Rentals - Booking ID - '.$mailData['reference_id']);
        });

        $adminMailData = [
            'name'=>$user->first_name." ".$user->last_name,
            'phone'=>$userPhone,
            'model'=>$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model,
            'from'=>$from_location->name,
            'to'=>$to_location->name,
            'reference_id'=>$referenceId,
            'estimated_distance'=>$estimated_distance,
            'estimated_duration'=>$estimated_duration,
            'customer_instructions'=>"",
            'price'=>$totalPriceDetails['total_with_tax'],
            'wallet_amount_applied'=>$totalPriceDetails['total_wallet_balance_applied'],
            'email'=>$user->email,
            'pgTxnAmount'=>$totalPriceDetails['payment_gateway_amount'],
            'problem'=>false
        ];

        Mail::send('emails.admin_a2b_booking_mail',$adminMailData,function($message) use ($adminMailData) {
        //var_dump($data);
          $message->to(env('ADMIN_EMAIL'));
          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
          $message->subject('One Way Rental '.$adminMailData['name'].' Booking - '.$adminMailData['reference_id']);
        });
        
        return "done";
    }
    
    
    public static function sendEmailAndSMSForLatLong($bike,$from_location,$to_location,$fromLat,$fromLong,$toLat,$toLong,$referenceId,$user,$estimated_distance,$estimated_duration,$totalPriceDetails)
    {
        $smsGatewayUrl = env('SMSGATEWAYURL');
        $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
        $smsSenderId = env('SMSSENDERID');

        $model = BikeModel::where('id',$bike->model_id)->first();

        $userPhone = Self::sanitizePhoneNumber($user->mobile_num);

        $mapsLinkFromFrom = "https://www.google.co.in/maps/place/".$fromLat.",".$fromLong;
        $mapsLinkFromTo = "https://www.google.co.in/maps/place/".$toLat.",".$toLong;



        $urlConvResponseFrom = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>$mapsLinkFromFrom, 'key'=>env('GOOGLESHORTURLAPI')]))->post();

        $urlConvResponseTo = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>$mapsLinkFromTo, 'key'=>env('GOOGLESHORTURLAPI')]))->post();

        $from_location_address = strip_tags($from_location->address);
        $to_location_address = strip_tags($to_location->address);

        $smsMessageUser = "One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed. Please pick it up at ".json_decode($urlConvResponseFrom)->id." The contact person is ".$from_location->contact_person." Ph:".$from_location->contact_number." Please drop it off at ".json_decode($urlConvResponseTo)->id." The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number;
        


        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$userPhone, 'message'=>$smsMessageUser, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }

        
        if($from_location->latitude == $fromLat && $from_location->longitude == $fromLong)
        {
            $fromLocationStr = $from_location->name;
        }
        else
        {
            $fromLocationStr = json_decode($urlConvResponseFrom)->id;
        }
        if($to_location->latitude == $toLat && $to_location->longitude == $toLong)
        {
            $toLocationStr = $to_location->name;
        }
        else
        {
            $toLocationStr = json_decode($urlConvResponseTo)->id;
        }
        
        
        
        $phone_number = Self::sanitizePhoneNumber($from_location->contact_number);

        $smsMessage = "One Way rental Booking ID - ".$referenceId." is made for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." ".$bike->bike_number." from ".$fromLocationStr." to ".$toLocationStr.". The customer's name is ".$user->first_name." ".$user->last_name." ph:".$user->mobile_num;

        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }

        $phone_number = Self::sanitizePhoneNumber($to_location->contact_number);

        if(env('TESTSMS')!=true)
        {
            $response = Curl::to($smsGatewayUrl)
            ->withData( array('authkey' => $smsGatewayAPIKey, 'mobiles'=>$phone_number, 'message'=>$smsMessage, 'sender'=>$smsSenderId, ' route'=>4, 'country'=>91, 'response'=>'json'))
            ->get();
        }

        if($fromLat == $toLat && $fromLong == $toLong)
        {
            $estimedDurationDistanceMessage = "";
        }
        else
        {
            $estimedDurationDistanceMessage = "<p>The estimated travel time is ".$estimated_distance."KM and estimated time for the trip is ".$estimated_duration." minutes.</p>";
        }




        $emailMessageUser = "<p>One Way rental with Booking ID - ".$referenceId." for ".$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model." is confirmed.</p><p>Please pick it up at ".json_decode($urlConvResponseFrom)->id."</p><p>The contact person is ".$from_location->contact_person." Ph:".$from_location->contact_number."</p>".$estimedDurationDistanceMessage."<p>Please drop it off at ".json_decode($urlConvResponseTo)->id."</p><p> The contact person is ".$to_location->contact_person." Ph:".$to_location->contact_number."</p>";
        

        $mailData = [
            'userEmail'=>$user->email,
            'fname'=>$user->first_name." ".$user->last_name,
            'email_body'=>$emailMessageUser,
            'reference_id'=>$referenceId
        ];

        Mail::send('emails.user_a2b_booking_mail',$mailData,function($message) use ($mailData) {
          $message->to($mailData['userEmail']);
          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
          $message->subject(env('SITENAMECAP').' One Way Rentals - Booking ID - '.$mailData['reference_id']);
        });

        $adminMailData = [
            'name'=>$user->first_name." ".$user->last_name,
            'phone'=>$userPhone,
            'model'=>$model->getBikeMakeName($model->bike_make_id)." ".$model->bike_model,
            'from'=>$fromLocationStr,
            'to'=>$toLocationStr,
            'reference_id'=>$referenceId,
            'estimated_distance'=>$estimated_distance,
            'estimated_duration'=>$estimated_duration,
            'customer_instructions'=>"",
            'price'=>$totalPriceDetails['total_with_tax'],
            'wallet_amount_applied'=>$totalPriceDetails['total_wallet_balance_applied'],
            'email'=>$user->email,
            'pgTxnAmount'=>$totalPriceDetails['payment_gateway_amount'],
            'problem'=>false
        ];

        Mail::send('emails.admin_a2b_booking_mail',$adminMailData,function($message) use ($adminMailData) {
        //var_dump($data);
          $message->to(env('ADMIN_EMAIL'));
          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
          $message->subject('One Way Rental '.$adminMailData['name'].' Booking - '.$adminMailData['reference_id']);
        });
        
        return "done";
    }
    
    
    public function fromLocationForLatLong(Request $request)
    {
        $lat = $request->latitude;
        $long = $request->longitude;
        
        $locationsWithBikes = OneWayRentalLocation::whereHas('bikes', function ($query) {
            $query->where('status', 1);
        })->get(['id','latitude','longitude','name','radius_delivery']);
        
        $destinationArr = [];
        
        if(!$locationsWithBikes)
        {
            $data = array(
                "dynamic" => [],
                "static"=>[]
            );

            return $this->respondWithSuccess($data);
        }
        
                
        foreach($locationsWithBikes as $location)
        {
            $distance = sqrt((($lat - $location->latitude)*($lat - $location->latitude))+(($long - $location->longitude)*($long - $location->longitude)));

            $location["distance_from_loc"] = $distance;
        }
        
        $locationsWithBikesArray = $locationsWithBikes->toArray();
        
        $sort = array();
        foreach($locationsWithBikesArray as $k=>$v) {
            $sort['distance_from_loc'][$k] = $v['distance_from_loc'];
        }
        # sort by event_type desc and then title asc
        array_multisort($sort['distance_from_loc'], SORT_ASC,$locationsWithBikesArray);

        $k = 2;
        
        $shortlistedLocationsWithBikesArray = [];
        
        foreach($locationsWithBikes as $i=>$locationsWithBike)
        {
            if($k>0)
            {
                array_push($destinationArr,$locationsWithBikesArray[$i]['latitude']."%2C".$locationsWithBikesArray[$i]['longitude']);
                array_push($shortlistedLocationsWithBikesArray,$locationsWithBikesArray[$i]);
                $k--;
            }
        }
        
        $destinationStr = implode("%7C",$destinationArr);
        
        
        $response = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&destinations=".$lat.",".$long."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();
        
        $responseWalking = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=walking&destinations=".$lat.",".$long."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();
        
        
        $responseObj = json_decode($response);
        $responseWalkingObj = json_decode($responseWalking);
        $distanceMatrixArray = $responseObj->rows;
        $distanceMatrixArrayWalking = $responseWalkingObj->rows;
        
        $distance = 1000000;
        $dynamic = [];
        $selected = 1000000;
    
        $i=0;
        while($i<2)
        {
            $distanceMatrixForLocation = $distanceMatrixArray[$i]->elements[0];
            $distanceMatrixForLocationWalking = $distanceMatrixArrayWalking[$i]->elements[0];
            
            if($distanceMatrixForLocation->status == "OK")
            {
                $shortlistedLocationsWithBikesArray[$i]['distance_from_latlong'] = $distanceMatrixForLocation->distance->value;
                $shortlistedLocationsWithBikesArray[$i]['distance_walk_from_latlong'] = $distanceMatrixForLocationWalking->distance->value;
                $shortlistedLocationsWithBikesArray[$i]['duration_walk_from_latlong'] = $distanceMatrixForLocationWalking->duration->value;
                
                if($distanceMatrixForLocation->distance->value < $shortlistedLocationsWithBikesArray[$i]['radius_delivery']*1000)
                {
                    if($distance > $distanceMatrixForLocation->distance->value)
                    {
                        $selected = $i;
                        $distance = $distanceMatrixForLocation->distance->value;
                    }
                }
            }
            else
            {
                $shortlistedLocationsWithBikesArray[$i]['distance_from_latlong'] = 1000000000;
                $shortlistedLocationsWithBikesArray[$i]['distance_walk_from_latlong'] = 1000000000;
                $shortlistedLocationsWithBikesArray[$i]['duration_walk_from_latlong'] = 1000000000;
            }
            
            $i++;
        }

        if($selected!=1000000)
        {
            $addressArr = explode(',',$responseObj->origin_addresses[0]);
            if(count($addressArr)>1)
            {
                $addressStr = $addressArr[0].", ".$addressArr[1];
            }
            else
            {
                $addressStr = $addressArr[0];
            }

            $message = "The bike will be delivered to this location in ".$distanceMatrixArray[$selected]->elements[0]->duration->text;

            array_push($dynamic,[
                "lat"=>floatval($lat),
                "long"=>floatval($long),
                "label"=>$addressStr,
                "message"=>$message
            ]);
            
        }
        
        usort($shortlistedLocationsWithBikesArray, function($a, $b)
        {
             if ($a['distance_from_latlong'] == $b['distance_from_latlong']) {
                return 0;
            }
            return ($a['distance_from_latlong'] < $b['distance_from_latlong']) ? -1 : 1;
        });
        
        $k=1;
        $static = [];
        
        foreach($shortlistedLocationsWithBikesArray as $locationsWithBike)
        {
            if($locationsWithBike['distance_walk_from_latlong'] < 1000000000 && $k>0)
            {
                $dist = round($locationsWithBike['distance_walk_from_latlong']/1000,1);
                $message = "This pick-up point is ".$dist." KM away.";
                if($locationsWithBike['duration_walk_from_latlong'] < 900)
                {
                    $dur = round($locationsWithBike['duration_walk_from_latlong']/60,0);
                    $message .= "(".$dur." minutes by walk)";
                }

                array_push($static,[
                    "lat"=>$locationsWithBike['latitude'],
                    "long"=>$locationsWithBike['longitude'],
                    "label"=>$locationsWithBike['name'],
                    "message"=>$message,
                    "distance"=>$locationsWithBike['distance_walk_from_latlong']
                ]);
                
                $k--;
            }
        }
        
        $data = array(
            "dynamic" => $dynamic,
            "static"=>$static
        );
        
        return $this->respondWithSuccess($data);  
    }
    
    
    public function toLocationForLatLong(Request $request)
    {
                
        $lat = $request->latitude;
        $long = $request->longitude;
        
        $locationsWithBikes = OneWayRentalLocation::where('status',1)->get(['id','latitude','longitude','name','radius_return']);
        
        $destinationArr = [];
        
        foreach($locationsWithBikes as $location)
        {
            $distance = sqrt((($lat - $location->latitude)*($lat - $location->latitude))+(($long - $location->longitude)*($long - $location->longitude)));

            $location["distance_from_loc"] = $distance;
        }
        
        $locationsWithBikesArray = $locationsWithBikes->toArray();
        
        $sort = array();
        foreach($locationsWithBikesArray as $k=>$v) {
            $sort['distance_from_loc'][$k] = $v['distance_from_loc'];
        }
        # sort by event_type desc and then title asc
        array_multisort($sort['distance_from_loc'], SORT_ASC,$locationsWithBikesArray);

        $k = 3;
        
        $shortlistedLocationsWithBikesArray = [];
        
        foreach($locationsWithBikes as $i=>$locationsWithBike)
        {
            if($k>0)
            {
                array_push($destinationArr,$locationsWithBikesArray[$i]['latitude']."%2C".$locationsWithBikesArray[$i]['longitude']);
                array_push($shortlistedLocationsWithBikesArray,$locationsWithBikesArray[$i]);
                $k--;
            }
        }
        
        $destinationStr = implode("%7C",$destinationArr);
        
        $response = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&destinations=".$lat.",".$long."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();
        
        $responseWalking = Curl::to("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=walking&destinations=".$lat.",".$long."&origins=".$destinationStr."&key=".env('GOOGLEMAPSAPI'))->post();

        $responseObj = json_decode($response);
        $responseWalkingObj = json_decode($responseWalking);
    
        $distanceMatrixArray = $responseObj->rows;
        $distanceMatrixArrayWalking = $responseWalkingObj->rows;
        
        $distance = 1000000;
        $dynamic = [];
        $selected = 1000000;
        
        $i=0;
        while($i<3)
        {
            $distanceMatrixForLocation = $distanceMatrixArray[$i]->elements[0];
            $distanceMatrixForLocationWalking = $distanceMatrixArrayWalking[$i]->elements[0];
            
            if($distanceMatrixForLocation->status == "OK")
            {
                $shortlistedLocationsWithBikesArray[$i]['distance_from_latlong'] = $distanceMatrixForLocation->distance->value;
                $shortlistedLocationsWithBikesArray[$i]['distance_walk_from_latlong'] = $distanceMatrixForLocationWalking->distance->value;
                $shortlistedLocationsWithBikesArray[$i]['duration_walk_from_latlong'] = $distanceMatrixForLocationWalking->duration->value;
                
                if($distanceMatrixForLocation->distance->value < $shortlistedLocationsWithBikesArray[$i]['radius_return']*1000)
                {
                    if($distance > $distanceMatrixForLocation->distance->value)
                    {
                        $selected = $i;
                        $distance = $distanceMatrixForLocation->distance->value;
                    }
                }
            }
            else
            {
                $shortlistedLocationsWithBikesArray[$i]['distance_from_latlong'] = 1000000000;
                $shortlistedLocationsWithBikesArray[$i]['distance_walk_from_latlong'] = 1000000000;
                $shortlistedLocationsWithBikesArray[$i]['duration_walk_from_latlong'] = 1000000000;
            }
            
            $i++;
        }
        
        if($selected!=1000000)
        {
            $addressArr = explode(',',$responseObj->origin_addresses[0]);
            if(count($addressArr)>1)
            {
                $addressStr = $addressArr[0].", ".$addressArr[1];
            }
            else
            {
                $addressStr = $addressArr[0];
            }

            $message = "";

            array_push($dynamic,[
                "lat"=>floatval($lat),
                "long"=>floatval($long),
                "label"=>$addressStr,
                "message"=>$message
            ]);
        }
        
        
        usort($shortlistedLocationsWithBikesArray, function($a, $b)
        {
             if ($a['distance_from_latlong'] == $b['distance_from_latlong']) {
                return 0;
            }
            return ($a['distance_from_latlong'] < $b['distance_from_latlong']) ? -1 : 1;
        });
        
        $static = [];
        if(count($dynamic)==0)
        {
            $k=1;
        
            foreach($shortlistedLocationsWithBikesArray as $locationsWithBike)
            {
                if($locationsWithBike['distance_walk_from_latlong'] < 1000000000 && $k>0)
                {
                    $dist = round($locationsWithBike['distance_walk_from_latlong']/1000,1);
                    $message = "This drop-off point is ".$dist." KM away.";
                    if($locationsWithBike['duration_walk_from_latlong'] < 900)
                    {
                        $dur = round($locationsWithBike['duration_walk_from_latlong']/60,0);
                        $message .= "(".$dur." minutes by walk)";
                    }

                    array_push($static,[
                        "lat"=>$locationsWithBike['latitude'],
                        "long"=>$locationsWithBike['longitude'],
                        "label"=>$locationsWithBike['name'],
                        "message"=>$message
                    ]);

                    $k--;
                }
            }
        }
        
        $data = array(
            "dynamic" => $dynamic,
            "static"=>$static
        );
        
        return $this->respondWithSuccess($data);  
        
    }
    
    public static function calculateRental($modelId,$distance,$duration)
    {
        $priceObj = OneWayRentalPrice::where('model_id',$modelId)->first();
                
        $nowHour = Carbon::now()->hour;

        if($nowHour > 15)
        {
            $discount = 0.8;
        }
        else
        {
            $discount = 1;
        }

        $price = ($priceObj->price_per_km*$distance)+($priceObj->price_per_min*$duration);

        $price = $price*$discount;
        
        return [
            "price"=>$price,
            "priceObj"=>$priceObj
        ];
    }
            
}
