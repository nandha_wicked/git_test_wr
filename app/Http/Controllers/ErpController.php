<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Dingo\Api\Routing\Helpers;
use Illuminate\Contracts\Filesystem\Filesystem;


use Auth;
use App\Models\Partner;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\City;
use App\Models\Area;
use App\Models\User;
use App\Models\BikeNumbers;
use App\Models\Bike;
use App\Models\ServiceBlock;
use App\Models\Booking;
use App\Models\Helmet;
use App\Models\Jacket;
use App\Models\OpsReport;
use App\Models\UserDocImage;
use App\Models\Parameters;
use App\Models\AccessoriesList;
use App\Models\ReasonForRenting;
use App\Models\PaymentMethods;
use App\Models\InvoiceType;
use App\Models\PaymentNature;
use App\Models\ReservationInfo;
use App\Models\BookingHeader;
use App\Models\DeliveryInfo;
use App\Models\ReturnInfo;
use App\Models\PricingInfo;
use App\Models\FinancialInfo;

use App\Models\NotesInfo;

use Carbon\Carbon;
use App\Http\Controllers\BookingController;
use DB;
use Mail;
use Curl;

 



class ErpController extends AppController
{
    public function index(){
    	if(Auth::check())
        {
    	   return redirect('erp/dashboard');
        }
        else
    		return redirect('erp/login');
    }

    public function login(){
    	return view('erp.login');
    }

    public function dashboard(){
    	return view('erp.dashboard');
    }
    
    public function addBikeNumbersIndex()
    {
        $bikeList = BikeNumbers::all();
        
        $bikeModels = BikeModel::where('status',1)->get();
        
        return view('erp.addBikeNumbers.index',compact('bikeList','bikeModels'));
    }
    public function addBikeNumbersAdd(Request $request)
    {
        $seriesCode = strtoupper($request->seriesCode);
        $bikeNumber = $request->stateCode."-".$request->areaCode."-".$seriesCode."-".$request->number;
        $existing = BikeNumbers::where('number',$bikeNumber)->first();
        if(!$existing)
        {
            BikeNumbers::create(['number' =>$bikeNumber, 'model'=>$request->model_id ]);
            return redirect('erp/addBikeNumbers');
        }
        else
        {
            $bikeList = BikeNumbers::where('number',$bikeNumber)->get();
            $bikeModels = BikeModel::where('status',1)->get();
            return view('erp.addBikeNumbers.index',compact('bikeList','bikeModels'))->withErrors(['numberExists'=>'The Number entered already exists. Please delete and re-enter if there is a mistake in the existing number.']);
        }
        
    }
    public function addBikeNumbersDelete($id,Request $request)
    {
        BikeNumbers::where('id',$id)->delete();
        
        return redirect('erp/addBikeNumbers');
    }
    
    public function serviceBlockIndex(Request $request)
    {
        $today = Carbon::today();
        
        $serviceBlocks = ServiceBlock::orderBy('id','desc')->where('end_datetime','>',$today->addDays(-10)->toDateTimeString())->take(500)->get();
        $bike_numbers = BikeNumbers::all();
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        foreach($serviceBlocks as $serviceBlock)
        {
            
            if(time() > strtotime($serviceBlock->end_datetime))
            {
                $serviceBlock['isCurrent']="no";
            }
            else
            {
                $serviceBlock['isCurrent']="yes";
            }
        }
        
        return view('erp.serviceBlockAdd.index',compact('serviceBlocks','bike_numbers','areas','models'));
    }
    
    public function serviceBlockAdd(Request $request)
    {
        $rules = array(
          'bike_number'=>'required',
          'model' => 'required',
          'area' => 'required',
          'start_datetime' => 'required',
          'end_datetime' => 'required|after:start_datetime',
          'note' => 'required' 
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            $errors = $validator->errors();
            foreach ($errors->all() as $error)
            {
               $errorStr = $error;
            }
            $serviceBlocks = ServiceBlock::orderBy('id','desc')->take(500)->get();
            $bike_numbers = BikeNumbers::all();
            $areas = Area::where('status',1)->get();
            $models = BikeModel::where('status',1)->get();
            return view('erp.serviceBlockAdd.index',compact('serviceBlocks','bike_numbers','areas','models','errorStr'));       
        }

        $actual_user = Auth::user();
        
        $modelId = $request->model;
        $areaId = $request->area;
        $bikeNumber = $request->bike_number;

        
        $startDateTime = Carbon::parse($request->start_datetime);
        $startDateTimeStr = $startDateTime->toDateTimeString();
        $endDateTime = Carbon::parse($request->end_datetime);
        $endDateTimeStr = $endDateTime->toDateTimeString();
        
        $colliding_blocks = ServiceBlock::where('bike_number_id',$bikeNumber)->where('start_datetime', '<', $endDateTimeStr)->where('end_datetime', '>', $startDateTimeStr)->where('status',1)->where("approval_status","<>","Rejected")->get();
        
        if($colliding_blocks->count()>0)
        {
            $errors = "There are ".$colliding_blocks->count()." reservations in the same time as the block attempted. The block IDs: ";
            $errorStr = "";
            foreach($colliding_blocks as $colliding_block)
            {
                $errorStr .= $colliding_block->id." ";
            }
            $errorStr = $errors.$errorStr;
            $serviceBlocks = ServiceBlock::orderBy('id','desc')->take(500)->get();
            $bike_numbers = BikeNumbers::all();
            $areas = Area::where('status',1)->get();
            $models = BikeModel::where('status',1)->get();
        
        
            return view('erp.serviceBlockAdd.index',compact('serviceBlocks','bike_numbers','areas','models','errorStr'));
            
            
        }
        
        if($startDateTime->diffInHours($endDateTime)>24)
        {
           $approval_status = 'Pending';
        
        }
        else
        {
            $approval_status = 'NA';
            
        }
        
        
        $serviceBlock = ServiceBlock::create(['bike_number_id'=>$bikeNumber,'model_id'=>$modelId,'area_id'=>$areaId,'created_by'=>$actual_user->id,'created_at'=>new \DateTime(),'approval_status'=>$approval_status,'note'=>$request->note,'start_datetime'=>$startDateTimeStr,'end_datetime'=>$endDateTimeStr]);
        

        
        if($startDateTime->diffInHours($endDateTime)<=24)
        {
            $modelId = $serviceBlock->model_id;
            $areaId = $serviceBlock->area_id;
            $area = Area::where('id',$areaId)->first();
            $cityId =$area->city_id;
            
            $startDate = $startDateTime->format('Y-m-d');
            $startTime = $startDateTime->format('H:i:s');
            
            $endDate = $endDateTime->format('Y-m-d');
            $endTime = $endDateTime->format('H:i:s');

            $dispatcher = app('Dingo\Api\Dispatcher');
            $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $modelId,'area_id'=>$areaId,'city_id'=> $cityId,'start_datetime' => $startDate." ".$startTime, 'end_datetime' => $endDate." ".$endTime])->get('bookings/price-and-check-availability');

            if($bikeAvailabilityWithPrice['bike_id'] != "none")
            {
                if($serviceBlock->booking_status==0)
                {
                    ServiceBlock::where('id',$serviceBlock->id)->update(['booking_status'=>1]);
                    $createBooking = Self::createServiceBlockBooking($serviceBlock,$serviceBlock->id);
                    if(array_key_exists('booking_id',$createBooking))
                    {
                        return redirect('/erp/serviceBlock');
                    }
                }
                else
                {
                    return redirect('/erp/serviceBlock');   
                }
            }
            
            $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail()];
            
            if(env('SERVER_STATUS')=='live')
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                  $message->to('blocking@wickedride.com');
                  $message->from('blocking@wickedride.com', 'Service Block');
                  $message->subject('Service Block for '.$data['bike_number']);
                });
            }
            else
            {
                if(env('SENDBLOCKEMAILTEST')=="true")
                {
                    Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('varunagni@gmail.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject('Service Block for '.$data['bike_number']);
                    });
                }
                
            }
                        
        }
        else
        {
            $roles = "operations manager";
            
            if(strpos(Auth::user()->role_string,$roles) !== false)
            {
                ServiceBlock::where('id',$serviceBlock->id)->update(['approval_status'=>"Approved","approved_by"=>Auth::user()->id]);
                
                Self::checkAndCreateServiceBlockForApprovedBookings($serviceBlock,$serviceBlock->id);
                
                return redirect('/erp/serviceBlock');
            }
            
            $approvingUsers = User::where('role_string',"LIKE","%".$roles."%")->where('isOperation',1)->get();
            foreach($approvingUsers as $approvingUser)
            { 
                $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail(),'to_email'=>$approvingUser->email];
                
                if($approvingUser->email!='admin@wickedride.com')
                {
                    if(env('SERVER_STATUS')=='live')
                    {
                        Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                          $message->to($data['to_email']);
                          $message->from('blocking@wickedride.com', 'Service Block');
                          $message->subject('Service Block for '.$data['bike_number'].' requires approval');
                        });
                    }
                    else
                    {
                        if(env('SENDBLOCKEMAILTEST')=="true")
                        {
                            Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                                $message->to('varunagni@gmail.com');
                                $message->from('blocking@wickedride.com', 'Service Block');
                                $message->subject('Service Block for '.$data['bike_number'].' requires approval - '.$data['to_email']);
                            });
                        }
                    }
                
                     
                }
               
            }
            
        }
        
        
        
        
        return redirect('/erp/serviceBlock');
        
    }
    
    public function serviceBlockDelete($id,Request $request)
    {   
        $serviceBlock = ServiceBlock::where('id',$id)->first();
        if($serviceBlock->booking_status == 0)
        {
            ServiceBlock::where('id',$id)->delete();
        }
        return redirect('/erp/serviceBlock');
        
    }
    
    public function serviceBlockApprovalIndex(Request $request)
    {
        $serviceBlocks = ServiceBlock::where('approval_status','Pending')->orderBy('id','desc')->take(500)->get();
        return view('erp.serviceBlockApprove.index',compact('serviceBlocks','bike_numbers','areas','models'));
    }
    
    public function serviceBlockUnblock($id,Request $request)
    {
        if(Auth::check())
        {
            $serviceBlock = ServiceBlock::where('id',$id)->first();
            $booking = Booking::where('id',$serviceBlock->booking_id)->first();
            
            if(!$booking)
            {
                $unblocked_by_user = "";
            }
            else
            {
                $dispatcher = app('Dingo\Api\Dispatcher');

                $unblock = $dispatcher->with([
                                'booking_id' =>$booking->id,
                                'start_datetime'=>$serviceBlock->start_datetime,
                                'end_datetime'=>$serviceBlock->end_datetime,
                                'model_id'=>$serviceBlock->model_id,
                                'area_id'=>$serviceBlock->area_id,
                                'note'=>$booking->note,
                                'id'=>$id,
                                'actual_user_id'=>Auth::user()->id                    
                                ])->post('unblock327e5gbach5FFD');
            }

            $unblocked_by_user = Auth::user()->id;
        }
        ServiceBlock::where('id',$id)->update(['unblocked_by'=>$unblocked_by_user,'unblocked_at'=>new \DateTime(),'status'=>0]);
        
        
        return redirect('/erp/serviceBlock');
    }
    
    public function serviceBlockApprove($id,Request $request)
    {
        
        ServiceBlock::where('id',$id)->update(['approved_by'=>Auth::user()->id,'approval_status'=>'Approved']);
        $serviceBlock = ServiceBlock::where('id',$id)->first();
        
        $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail()];
        
        if(env('SERVER_STATUS')=='live')
        {
            Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                $message->to('blocking@wickedride.com');
                $message->from('blocking@wickedride.com', 'Service Block');
                $message->subject('Service Block for '.$data['bike_number']);
            });
        }
        else
        {
            if(env('SENDBLOCKEMAILTEST')=="true")
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('varunagni@gmail.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject('Service Block for '.$data['bike_number']);
                });
            }
        }
        
        Self::checkAndCreateServiceBlockForApprovedBookings($serviceBlock,$id);
        
        
        return redirect('/erp/serviceBlockApprove');
    }
    
    
    public function checkAndCreateServiceBlockForApprovedBookings($serviceBlock,$id)
    {
        $modelId = $serviceBlock->model_id;
        $areaId = $serviceBlock->area_id;
        $area = Area::where('id',$areaId)->first();
        $cityId =$area->city_id;
        $start_datetime = new \DateTime($serviceBlock->start_datetime);
        $startDate = $start_datetime->format('Y-m-d');
        $startTime = $start_datetime->format('H:i:s');
        $end_datetime = new \DateTime($serviceBlock->end_datetime);
        $endDate = $end_datetime->format('Y-m-d');
        $endTime = $end_datetime->format('H:i:s');
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId,
            'area_id'=>$areaId,
            'city_id'=> $cityId,
            'start_datetime' => $startDate." ".$startTime, 
            'end_datetime' => $endDate." ".$endTime
        ])->get('bookings/price-and-check-availability');
        
        if($bikeAvailabilityWithPrice['bike_id'] != "none")
        {
            if($serviceBlock->booking_status==0)
            {
                ServiceBlock::where('id',$id)->update(['booking_status'=>1]);
                Self::createServiceBlockBooking($serviceBlock,$id); 
            }
        }
    }
    
    
    
    public function serviceBlockReject($id,Request $request)
    {
        ServiceBlock::where('id',$id)->update(['approved_by'=>Auth::user()->id,'approval_status'=>'Rejected']);
        
        $serviceBlock = ServiceBlock::where('id',$id)->first();
        
        $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail(),'approved_by'=>$serviceBlock->getApprovedByEmail()];
        
        if(env('SERVER_STATUS')=='live')
        {
            Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                $message->to('operations@wickedride.com');
                $message->from('blocking@wickedride.com', 'Service Block');
                $message->subject('Service Block for '.$data['bike_number'].' was rejected by '.$data['approved_by']);
            });
        }
        else
        {
            if(env('SENDBLOCKEMAILTEST')=="true")
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('varunagni@gmail.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject('Service Block for '.$data['bike_number'].' was rejected by '.$data['approved_by']);
                });
            }
        }
        
        
        return redirect('/erp/serviceBlockApprove');
    }
    
    public function serviceBlockCreateBlockIndex(Request $request)
    {
        
        $serviceBlocks = ServiceBlock::where('booking_status',0)->whereIn('approval_status',array('Approved','NA'))->orderBy('id','desc')->take(500)->get();
        $bike_numbers = BikeNumbers::all();
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        
        foreach($serviceBlocks as $serviceBlock)
        {
            $startDateTimeCarbon = Carbon::parse($serviceBlock->start_datetime);
            $endDateTimeCarbon = Carbon::parse($serviceBlock->end_datetime);
            
            $bikeAvailability = $dispatcher->with(['model_id' => $serviceBlock->model_id,'area_id'=>$serviceBlock->area_id, 'start_date' => $startDateTimeCarbon->format('Y-m-d'), 'end_date' => $endDateTimeCarbon->format('Y-m-d'),'start_time' => $startDateTimeCarbon->format('H:i'), 'end_time' => $endDateTimeCarbon->format('H:i')])->get('check-availability');
            
        
            if($bikeAvailability['bike_id']=="none")
            {
                $serviceBlock['available'] = false;
            }
            else
            {
                $serviceBlock['available'] = true;
            }
        }
        
        
        return view('admin.serviceBlockBooking.index',compact('serviceBlocks'));
    }
    
    public function serviceBlockAdminDelete($id,Request $request)
    {   
        $service_block = ServiceBlock::where('id',$id)->first();
        if(!$service_block)
        {
            return redirect('/admin/serviceBlock');
        }
        else
        {
            if($service_block->booking_status==1)
            {
                return redirect('/admin/serviceBlock');
            }
            else
            {
                ServiceBlock::where('id',$id)->delete();
                return redirect('/admin/serviceBlock');
            }
            
        }
        
        
    }
    
    public function serviceBlockCreateBlock($id,Request $request)
    {   
        
        $serviceBlock = ServiceBlock::where('id',$id)->first();
        if(!$serviceBlock)
        {
            $serviceBlocks = ServiceBlock::where('booking_status',0)->whereIn('approval_status',array('Approved','NA'))->orderBy('id','desc')->take(500)->get();
                $bike_numbers = BikeNumbers::all();
                $areas = Area::where('status',1)->get();
                $models = BikeModel::where('status',1)->get();

                $errorStr = "The block has been deleted. Please refresh your screen.";


                return view('admin.serviceBlockBooking.index',compact('serviceBlocks','errorStr'));
            
        }
        else
        {
            if($serviceBlock->booking_status==0)
            {
                
                ServiceBlock::where('id',$id)->update(['booking_status'=>1]);
                $createBooking = Self::createServiceBlockBooking($serviceBlock,$id);
                if(array_key_exists('booking_id',$createBooking))
                {
                    return redirect('/admin/serviceBlock');
                }
                else
                {
                    $serviceBlocks = ServiceBlock::where('booking_status',0)->whereIn('approval_status',array('Approved','NA'))->orderBy('id','desc')->take(500)->get();
                    $bike_numbers = BikeNumbers::all();
                    $areas = Area::where('status',1)->get();
                    $models = BikeModel::where('status',1)->get();

                    $errorStr = "The bike is not available to be booked.";
                    return view('admin.serviceBlockBooking.index',compact('serviceBlocks','errorStr'));
                }
                
            }
            else
            {
                $serviceBlocks = ServiceBlock::where('booking_status',0)->whereIn('approval_status',array('Approved','NA'))->orderBy('id','desc')->take(500)->get();
                $bike_numbers = BikeNumbers::all();
                $areas = Area::where('status',1)->get();
                $models = BikeModel::where('status',1)->get();

                $errorStr = "The block has already been made. Please refresh your screen.";
                return view('admin.serviceBlockBooking.index',compact('serviceBlocks','errorStr'));
            }
        }
        
        
    }
    
    public static function createServiceBlockBooking($serviceBlock,$id)
    {
        $modelId = $serviceBlock->model_id;
        $areaId = $serviceBlock->area_id;
        $area = Area::where('id',$areaId)->first();
        $cityId =$area->city_id;
        $start_datetime = new \DateTime($serviceBlock->start_datetime);
        $startDate = $start_datetime->format('Y-m-d');
        $startTime = $start_datetime->format('H:i:s');
        $end_datetime = new \DateTime($serviceBlock->end_datetime);
        $endDate = $end_datetime->format('Y-m-d');
        $endTime = $end_datetime->format('H:i:s');
        $note = "\"Block Type:Service\"\"Blocked Bike Number:".$serviceBlock->getBikeNumber()."\"\"Block Reason:".$serviceBlock->note."\"\"Block Request By:".$serviceBlock->getCreatedByEmail()."\"\"Block Approved By:".$serviceBlock->getApprovedByEmail()."\"\"Block Created By:".Auth::user()->email."\"";
        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailabilityWithPrice = $dispatcher->with([
            'model_id' => $modelId,
            'area_id'=>$areaId,
            'city_id'=> $cityId,
            'start_datetime' => $startDate." ".$startTime, 
            'end_datetime' => $endDate." ".$endTime
        ])->get('bookings/price-and-check-availability');

        
        $bikeAvailability = $bikeAvailabilityWithPrice['bike_availability'];

        if($bikeAvailabilityWithPrice['bike_id'] != "none")
        {
            $booking = $dispatcher->with([
                'start_date' => $startDate, 
                'end_date' => $endDate,
                'start_time' => $startTime, 
                'end_time' => $endTime, 
                'user_id' => 2, 
                'actual_user_id'=>Auth::user()->id, 
                'model_id' => $modelId,
                'area_id'=>$areaId, 
                'status' => 'COMPLETE',
                'total_price' => 0,
                'pg_status' => '', 
                'pg_txn_id' => '', 
                'bank_txn_id' => '',
                'order_id' => '', 
                'pg_mode' => '', 
                'note' => $note, 
                'pg_txn_amount' => 0, 
                'raw_data' => '', 
                'coupon'=>'',
                'payment_method'=>'',
                'kmlimit'=>''
            ])->get('bookingsweb');
            
            if(isset($booking['error']))
            {
                ServiceBlock::where('id',$id)->update(['booking_status'=>0]);
                return ['error'=>'Booking error. Block not created.'];
            }

            ServiceBlock::where('id',$id)->update(['booking_id'=>$booking['id'],'booking_status'=>1,'booked_by'=>Auth::user()->id]);
            

            //Emails to Ops team after Booking is created for the Service Block

            $serviceBlock=ServiceBlock::where('id',$id)->first();
            
            Booking::where('id',$booking['id'])->update(['bike_number_id'=>$serviceBlock->bike_number_id]);

            $data = ['id' => $serviceBlock['id'], 'bike_number' => $serviceBlock->getBikeNumber(), 'area_name' => $serviceBlock->getAreaName(), 'model_name' => $serviceBlock->getModelName(),'start_datetime' => $serviceBlock->start_datetime, 'end_datetime' => $serviceBlock->end_datetime, 'note' => $serviceBlock['note'], 'created_by' => $serviceBlock->getCreatedByEmail(),'booked_by'=>$serviceBlock->getBookedByEmail()];

            if(env('SERVER_STATUS')=='live')
            {
                Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                    $message->to('operations@wickedride.com');
                    $message->from('blocking@wickedride.com', 'Service Block');
                    $message->subject('Block for '.$data['bike_number'].' was created in backend by '.$data['booked_by']);
                });
            }
            else
            {
                if(env('SENDBLOCKEMAILTEST')=="true")
                {
                    Mail::send('emails.service_block_to_res',$data,function($message) use ($data) {
                        $message->to('varunagni@gmail.com');
                        $message->from('blocking@wickedride.com', 'Service Block');
                        $message->subject('Block for '.$data['bike_number'].' was created in backend by '.$data['booked_by']);
                    });
                }
            }

            return ['booking_id'=>$booking['id']];
        }
        else
        {
            ServiceBlock::where('id',$id)->update(['booking_status'=>0]);
            return ['error'=>'The bike is not available to be booked.'];
        }
        
    }
    
    public function addHelmetJacketIndex()
    {
        $helmetList = Helmet::all();
        $jacketList = Jacket::all();
        
        return view('erp.addHelmetJacket.index',compact('helmetList','jacketList'));
    }
    
    public function addHelmetJacketAdd(Request $request)
    {
        if($request->item=="helmet")
        {
            Helmet::create(['item_id'=>$request->item_id]);
        }
        else
        {
            Jacket::create(['item_id'=>$request->item_id]);
        }
        return redirect('erp/addHelmetJacket');
    }
    
    public function addHelmetJacketDelete($id,Request $request)
    {
        if($request->item=="helmet")
        {
            Helmet::where('id',$id)->delete();
        }
        else
        {
            Jacket::where('id',$id)->delete();
        }
        return redirect('erp/addHelmetJacket');
    }
    
    public function deliver($id,Request $request)
    {
        if($request->platform == "app")
        {
            $rules = array(
              'start_odo'=>'required',
              'bike_number_id'=>'required',
              'platform'=>'required'
            );
            
            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return $this->respondWithValidationErrorStatusCode($validator->errors());
            }
            
        }
        else
        {
            $rules = array(
              'bike_number_add'=>'required',
              'starting_odo_add'=>'required'
            );
            
            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return redirect('/erp/todaybooking')->withErrors($validator->errors());
            }
        }
         
        
        if(env('SERVER_STATUS')=='local')
            $user = User::where('id',1)->first();
        else
            $user = UserController::getLoggedInUser();
        
        $booking = Booking::where('id',$id)->first();
        
        if($request->platform == "app")
        {
            $bike_number_id = $request->bike_number_id;
            $start_odo = $request->start_odo;
            $route_plan = $request->route_plan;
            $helmet_id_1 = $request->helmet_id_1;
            $helmet_id_2 = $request->helmet_id_2;
            $jacket_id_1 = $request->jacket_id_1;
            $jacket_id_2 = $request->jacket_id_2;
            $gloves = $request->gloves;
            $camera = $request->camera;
            $knee_guard = $request->knee_guard;
            $saddle_bags = $request->saddle_bags;
            $tank_bags = $request->tank_bags;
            $mobile_holder = $request->mobile_holder;
            $extra_money = $request->extra_money;
            $collection_mode = $request->collection_mode;
            $delivery_notes = $request->delivery_notes;
            
        }
        else
        {
            $bike_number_id = $request->bike_number_add;
            $model = $request->model_add;
            $start_odo = $request->starting_odo_add;
            $route_plan = $request->route_plan_add;
            $helmet_id_1 = $request->helmet_1_add;
            $helmet_id_2 = $request->helmet_2_add;
            $jacket_id_1 = $request->jacket_1_add;
            $jacket_id_2 = $request->jacket_2_add;
            $gloves = $request->quant_add[5];
            $camera = $request->quant_add[2];
            $knee_guard = $request->quant_add[3];
            $saddle_bags = $request->quant_add[4];
            $tank_bags = $request->quant_add[8];
            $mobile_holder = $request->quant_add[7];
            $extra_money = $request->extra_money_add;
            $collection_mode = $request->collection_mode_add;
            $delivery_notes = $request->delivery_notes_add;
             
        }
        
        $delivered_model_id = $booking->model_id;
        $delivery_cancelled = 0;
        $delivered_at = date('Y-m-d H:i:s',strtotime("now"));
        $delivered_by = $user->id;
        
        
        
        if(isset($request->user_doc))
        {
            
            foreach($request->user_doc as $docArray)
            {
                foreach($docArray['doc_images'] as $docImageObj)
                {
                    $docImageDestinationPath = 'img/user/document/';
                    
                    $createImage = Self::base64_to_image($docImageObj['image'],$docImageDestinationPath);
                    
                    
                    if($createImage['success'])
                    {
                        $insert = UserDocImage::create(['user_id' =>$booking->user_id, 'doc_type' => $docArray['doc_type'],'doc' => $createImage['image'],'doc_reason' => '','status' => 1]);
                    }
                }
                
            }
        }
        
        
        if(isset($request->bike_images))
        {
            
            foreach($request->bike_images as $bikeImagesArray)
            {
                $docImageDestinationPath = 'img/booking/bikeimages/';

                $createImage = Self::base64_to_image($bikeImagesArray['image'],$docImageDestinationPath);

                if($createImage['success'])
                    $insert = DB::table('bike_images_for_bookings')->insert(['booking_id' =>$booking->id, 'full' => $createImage['image'],'status' => 1]);
            }
        }
        
        
        
        if(isset($request->image))
        {
            
            foreach($request->image as $key=>$index)
            {
                
                $docImageDestinationPath = 'img/bike/booking/';
                                    
                $createImage = Self::base64_with_header_to_image($request['imageBase64'.$index],$docImageDestinationPath);

                
                if($createImage['success'])
                {
                    DB::table('booking_images')->insert([
                        'user_id' =>$booking->user_id, 
                        'booking_id' => $booking->id,
                        'image_url' => $createImage['image'],
                        'created_by' => $delivered_by
                    ]);
                }
            }
        }
        
        
        
        Booking::where('id',$id)->update([  
                                            'bike_number_id' => $bike_number_id,
                                            'start_odo' => $start_odo,
                                            'route_plan' => $route_plan,
                                            'helmet_id_1' => $helmet_id_1,
                                            'helmet_id_2' => $helmet_id_2,
                                            'jacket_id_1' => $jacket_id_1,
                                            'jacket_id_2' => $jacket_id_2,
                                            'gloves' => $gloves,
                                            'camera' => $camera,
                                            'knee_guard' => $knee_guard,
                                            'saddle_bags' => $saddle_bags,
                                            'tank_bags' => $tank_bags,
                                            'mobile_holder' => $mobile_holder,
                                            'extra_money' => $extra_money,
                                            'collection_mode' => $collection_mode,
                                            'delivery_notes' => $delivery_notes,
                                            'delivered_model_id' => $delivered_model_id,
                                            'delivery_cancelled' => $delivery_cancelled,
                                            'delivered_at' => $delivered_at,
                                            'delivered_by' => $delivered_by,
                                            'is_delivered' =>1,
                                            'delivery_cancelled' => 0
                                         ]);
        
        
        if($extra_money>0)
        {
            
            $customer = User::where('id',$booking->user_id)->first();
            
            
            $smsGatewayUrl = env('SMSGATEWAYURL');
            $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
            $smsSenderId = env('SMSSENDERID');
            
            $phone_number = str_replace("+","",$customer->mobile_num);
            $phone_number = str_replace(" ","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            
            $smsMessage = "Thank you for riding with us. We have recieved the additional amount of Rs ".$extra_money." you have paid at the time of delivery.";

            if(env('TESTSMS')!=true)
            {
                $response = Curl::to($smsGatewayUrl)->withData(array(
                    'authkey' => $smsGatewayAPIKey, 
                    'mobiles'=>$phone_number, 
                    'message'=>$smsMessage, 
                    'sender'=>$smsSenderId, 
                    'route'=>4, 
                    'country'=>91, 
                    'response'=>'json'
                ))->get();
            }
        }
        
        if($request->platform == "app")
        {
            $invoices = array(
                [
                    "invoice_type"=>"Helmet",
                    "suggested_amount"=>500,
                    "description_string"=>"qty:2"
                ],
                [
                    "invoice_type"=>"Jacket",
                    "suggested_amount"=>600,
                    "description_string"=>"qty:3"
                ],
                [
                    "invoice_type"=>"Saddle Bags",
                    "suggested_amount"=>100,
                    "description_string"=>"qty:1"
                ]
            );
            
            
                
            return $this->respondWithSuccess(['status'=>true, 'message'=>'delivered','invoices'=>$invoices]);
        }
        else
            return redirect()->back();
    }
    
    public function deliverEdit($id,Request $request)
    {
         $rules = array(
          'bike_number_edit'=>'required',
          'starting_odo_edit'=>'required',
          'helmet_1_edit'=>'required',
          'helmet_2_edit'=>'required',
          'jacket_1_edit'=>'required' ,
          'jacket_2_edit'=>'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/erp/todaybooking')->withErrors($validator->errors());
        }
        
        $bike_number = $request->bike_number_edit;
        $model = $request->model_edit;
        $starting_odo = $request->starting_odo_edit;
        $route_plan = $request->route_plan_edit;
        $delivered_at = $request->delivered_at_edit;
        $helmet_1 = $request->helmet_1_edit;
        $helmet_2 = $request->helmet_2_edit;
        $jacket_1 = $request->jacket_1_edit;
        $jacket_2 = $request->jacket_2_edit;
        
        
        
        
        Booking::where('id',$id)->update(['bike_number_id'=>$bike_number,'route_plan'=>$route_plan,'start_odo'=>$starting_odo,'helmet_id_1'=>$helmet_1,'helmet_id_2'=>$helmet_2,'jacket_id_1'=>$jacket_1,'jacket_id_2'=>$jacket_2,'is_delivered'=>1,'delivered_by'=>Auth::user()->id,'delivered_at'=>date('Y-m-d H:i:s',strtotime("now")),'gloves'=>$request->quant_edit[5],'camera'=>$request->quant_edit[2],'knee_guard'=>$request->quant_edit[3],'saddle_bags'=>$request->quant_edit[4],'tank_bags'=>$request->quant_edit[8],'mobile_holder'=>$request->quant_edit[7],'delivered_model_id'=>$request->delivered_model_id_edit,'delivery_cancelled'=>0,'extra_money'=>$request->extra_money_edit,'collection_mode'=>$request->collection_mode_edit,'delivery_notes'=>$request->delivery_notes_edit]);
        
        
        if($request->extra_money_edit>0)
        {
            
            $customer = User::where('id',$booking->user_id)->first();
            
            
            $smsGatewayUrl = env('SMSGATEWAYURL');
            $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
            $smsSenderId = env('SMSSENDERID');
            
            $phone_number = str_replace("+","",$customer->mobile_num);
            $phone_number = str_replace(" ","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            
            $smsMessage = "Thank you for riding with us. We have recieved the additional amount of Rs ".$request->extra_money_edit." you have paid at the time of delivery.";

            if(env('TESTSMS')!=true)
            {
                $response = Curl::to($smsGatewayUrl)->withData(array(
                    'authkey' => $smsGatewayAPIKey, 
                    'mobiles'=>$phone_number, 
                    'message'=>$smsMessage, 
                    'sender'=>$smsSenderId, 
                    'route'=>4, 
                    'country'=>91, 
                    'response'=>'json'
                ))->get();
            }
        }
        
        
     
        return redirect('erp/todaybooking');
    }
    
    public function returnF($id,Request $request)
    {
         
        
        if($request->platform == "app")
        {  
            $rules = array(
              'end_odo'=>'required'

            );
            
            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return $this->respondWithValidationErrorStatusCode($validator->errors());
            }
        }
        else
        {
            $rules = array(
              'end_odo_add'=>'required'

            );
            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                return redirect('/erp/todaybookingdrop')->withErrors($validator->errors());
            }
        }

        
        $booking = Booking::where('id',$id)->first();
        
        
        if($request->platform == "app")
        {
        
            $end_odo = $request->end_odo;
            $extra_km_charge = $request->extra_km_charge;
            $returned_with_problems_accessory = $request->returned_with_problems_accessory;
            $returned_with_problems = $request->returned_with_problems;
            $returned_with_problems_note = $request->returned_with_problems_note;
            $extra_money_return = $request->extra_money_return;
            $collection_mode_return = $request->collection_mode_return;
            $delivery_notes = $request->delivery_notes;
            
        }
        else
        {
            $end_odo = $request->end_odo_add;
            $extra_km_charge = $request->extra_km_charges;
            $returned_with_problems = !$request->bike_received_add;
            $returned_with_problems_accessory = !$request->accessories_received_add;
            $returned_with_problems_note = $request->return_problem_notes_add;
            $extra_money_return = $request->extra_money_add;
            $collection_mode_return = $request->collection_mode_add;
            $delivery_notes = $request->return_notes_add;
        }

        
        $returned_at = date('Y-m-d H:i:s',strtotime("now"));
        $is_returned = 1;
        
        if(env('SERVER_STATUS')=='local')
            $returned_by = 1;
        else
            $returned_by = UserController::getLoggedInUser()->id;
        
        Booking::where('id',$id)->update([
                                            'is_returned'=> $is_returned,
                                            'end_odo'=> $end_odo,
                                            'extra_km_charge'=> $extra_km_charge,
                                            'returned_with_problems_accessory'=> $returned_with_problems_accessory,
                                            'returned_with_problems'=> $returned_with_problems,
                                            'returned_with_problems_note'=> $returned_with_problems_note,
                                            'extra_money_return'=> $extra_money_return,
                                            'collection_mode_return'=> $collection_mode_return,
                                            'delivery_notes'=> $delivery_notes,
                                            'returned_at'=> $returned_at,
                                            'returned_by'=> $returned_by
                                        ]);
        
        if($extra_money_return>0)
        {
            
            $customer = User::where('id',$booking->user_id)->first();
            
            
            $smsGatewayUrl = env('SMSGATEWAYURL');
            $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
            $smsSenderId = env('SMSSENDERID');
            
            $phone_number = str_replace("+","",$customer->mobile_num);
            $phone_number = str_replace(" ","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            
            $smsMessage = "Thank you for riding with us. We have recieved the additional amount of Rs ".$extra_money_return." you have paid at the time of return.";

            if(env('TESTSMS')!=true)
            {
                $response = Curl::to($smsGatewayUrl)->withData(array(
                    'authkey' => $smsGatewayAPIKey, 
                    'mobiles'=>$phone_number, 
                    'message'=>$smsMessage, 
                    'sender'=>$smsSenderId, 
                    'route'=>4, 
                    'country'=>91, 
                    'response'=>'json'
                ))->get();
            }
        }
     
        if($request->platform == "app")
            return $this->respondWithSuccess(['status'=>true, 'message'=>'returned']);
        else
            return redirect('erp/todaybookingdrop');
    }
    
    public function returnEdit($id,Request $request)
    {
          $rules = array(
          'end_odo_edit'=>'required'
          
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('/erp/todaybookingdrop')->withErrors($validator->errors());
        }
        
        $booking = Booking::where('id',$id)->first();
        
        $end_odo_edit = $request->end_odo_edit;
        $extra_km_charges = $request->extra_km_charges;
        $bike_received_edit = $request->bike_received_edit;
        $accessories_received_edit = $request->accessories_received_edit;
        $return_problem_notes_edit = $request->return_problem_notes_edit;
        $extra_money_edit = $request->extra_money_edit;
        $collection_mode_edit = $request->collection_mode_edit;
        $return_notes_edit = $request->return_notes_edit;
        
        
        
        
        
        Booking::where('id',$id)->update(['is_returned'=>1,'end_odo'=>$end_odo_edit,'extra_km_charge'=>$extra_km_charges,'returned_with_problems_accessory'=>!$accessories_received_edit,'returned_with_problems'=>!$bike_received_edit,'returned_with_problems_note'=>$return_problem_notes_edit,'extra_money_return'=>$extra_money_edit,'collection_mode_return'=>$collection_mode_edit,'delivery_notes'=>$return_notes_edit,'returned_at'=>date('Y-m-d H:i:s',strtotime("now")),'returned_by'=>Auth::user()->id]);
        
        if($extra_money_edit>0)
        {
            
            $customer = User::where('id',$booking->user_id)->first();
            
            
            $smsGatewayUrl = env('SMSGATEWAYURL');
            $smsGatewayAPIKey = env('SMSGATEWAYAPIKEY');
            $smsSenderId = env('SMSSENDERID');
            
            $phone_number = str_replace("+","",$customer->mobile_num);
            $phone_number = str_replace(" ","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            
            $smsMessage = "Thank you for riding with us. We have recieved the additional amount of Rs ".$extra_money_edit." you have paid at the time of return.";

            if(env('TESTSMS')!=true)
            {
                $response = Curl::to($smsGatewayUrl)->withData(array(
                    'authkey' => $smsGatewayAPIKey, 
                    'mobiles'=>$phone_number, 
                    'message'=>$smsMessage, 
                    'sender'=>$smsSenderId, 
                    'route'=>4, 
                    'country'=>91, 
                    'response'=>'json'
                ))->get();
            }
        }
     
        return redirect('erp/todaybookingdrop');
    }
    
    public function bookingNotesIndex(){
        
        $models = BikeModel::where('status', 1)->get();
        $areas = Area::where('status', 1)->get();
        $today = Carbon::now();
        //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
        $bookings = Booking::orderBy('id', 'desc')->where('start_datetime','>=',$today->toDateTimeString())->where('backend_notes','<>',"")->take(100)->get();
        foreach ($bookings as $booking) {
          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);
          // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
          // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

          $booking['start_date'] = $startDateObj->toDateString();
          $booking['end_date'] = $endDateObj->toDateString();
          $booking['start_time'] = $startDateObj->toTimeString();
          $booking['end_time'] = $endDateObj->toTimeString();
        }
        $userList = User::orderBy('id', 'asc')->take(20)->get();


        return view('erp.bookingNotes.index',compact('userList','models', 'areas', 'bookings'));
    }
    
    public function bookingNotesAck($id, Request $request)
    {
        $today = Carbon::now();
        $user_id = Auth::user()->id;
        Booking::where('id',$id)->update(['backend_notes_ack_by'=>$user_id,'backend_notes_ack_at'=>$today->toDateTimeString()]);
        
        return redirect('erp/bookingNotes');
    }
    
    public function cancelDelivery($id, Request $request)
    {
        $today = Carbon::now();
        $user_id = Auth::user()->id;
        Booking::where('id',$id)->update(['is_delivered'=>1,'delivery_cancelled'=>1,'delivery_cancelled_notes'=>$request->cancelled_notes,'delivered_by'=>Auth::user()->id,'delivered_at'=>date('Y-m-d H:i:s',strtotime("now")),'bike_number_id'=>0]);
        
        return redirect('erp/todaybooking');
    }
    
    public function deliver_cancel($id, Request $request)
    {
        $today = Carbon::now();
        
        if(env('SERVER_STATUS')=='local')
            $user_id = 1;
        else
            $user_id = UserController::getLoggedInUser()->id;
        
        Booking::where('id',$id)->update(['is_delivered'=>1,'delivery_cancelled'=>1,'delivery_cancelled_notes'=>$request->cancelled_notes,'delivered_by'=>$user_id,'delivered_at'=>date('Y-m-d H:i:s',strtotime("now"))]);
        
        return $this->respondWithSuccess(['status'=>true, 'message'=>'cancelled']);
    }
    
    public function bookingIndex(){
        $models = BikeModel::where('status', 1)->get();
        $areas = Area::where('status', 1)->get();
        $today = Carbon::now();
        $bookings = [];
        $count = 0;

        $userList = User::orderBy('id', 'asc')->take(20)->get();
        $paymentMethods = PaymentMethods::where('display',1)->get(); 
   
        return view('erp.booking.index',compact('userList','models', 'areas','bookings','count','paymentMethods'));
    }
    
    public function bookingSearch(Request $request){
        
        $userList = User::orderBy('id', 'asc')->take(20)->get();
        $models = BikeModel::where('status', 1)->get();
        $areas = Area::where('status', 1)->get();
        $today = Carbon::now();
        //$bookings = Booking::orderBy('created_at', 'desc')->where('end_datetime', '>=', $today->toDateTimeString())->take(200)->get();
        $bookings = Booking::orderBy('created_at', 'desc')->where('reference_id',$request->id)->get();
        foreach ($bookings as $booking) {
          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);
          // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
          // $endTime = Slot::where('id', $booking['end_slot_id'])->first();

          $booking['start_date'] = $startDateObj->toDateString();
          $booking['end_date'] = $endDateObj->toDateString();
          $booking['start_time'] = $startDateObj->toTimeString();
          $booking['end_time'] = $endDateObj->toTimeString();
        }
        $userList = User::orderBy('id', 'asc')->take(20)->get();

        $count =1;
        
        $paymentMethods = PaymentMethods::where('display',1)->get(); 
        
        return view('erp.booking.index',compact('userList','models', 'areas', 'bookings','count','paymentMethods'));
    }
    
    public function sendOpsEmail(Request $request){
    
        
        $areas = Area::where('status', 1)->get();
        
        $undeliveredBookingsByArea = []; 
        $unreturnedBookingsByArea = [];
        
        $walkins = [];
        
        $storeCollectionDelivery = [];
        $storeCollectionReturn = [];
        
        $cancelledDeliveries = [];
        
        $approvedServiceBlocks = [];
        
        $expiringServiceBlocks = [];
        
        foreach($areas as $area)
        {
            
            $tomorrow = Carbon::today()->addDay(0);
            $today = Carbon::today()->addDay(-1);
            
            
            $deliveryBookings = Booking::where('area_id',$area->id)->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->get();
            
            
            $returnBookings = Booking::where('area_id',$area->id)->where('end_datetime', '>=', $today->toDateTimeString())->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->whereNotIn('user_id',array(2,5,7))->where('delivery_cancelled',0)->get();
            
            
            $approvedServiceBlocksByArea = ServiceBlock::where('area_id',$area->id)->where('created_at', '>=', $today->toDateTimeString())->where('created_at', '<=', $tomorrow->toDateTimeString())->where('approval_status','Approved')->get();
            
            if(count($approvedServiceBlocksByArea)>0)
            {
                $approvedServiceBlocksByAreaDetails = [];
                
                foreach($approvedServiceBlocksByArea as $approvedServiceBlocksByAreaEle)
                {
                    array_push($approvedServiceBlocksByAreaDetails,['model_id'=>$approvedServiceBlocksByAreaEle->getModelName(),'duration'=>$approvedServiceBlocksByAreaEle->getDuration(),'notes'=>$approvedServiceBlocksByAreaEle->note]);   
                }
                array_push($approvedServiceBlocks,['area'=>$area->area,'approved_service_blocks'=>$approvedServiceBlocksByAreaDetails]);
            }
            
            $tomorrowS = Carbon::today()->addDay();
            $todayS = Carbon::today();
            
            
            $expiringServiceBlocksByArea = ServiceBlock::where('area_id',$area->id)->where('end_datetime', '>=', $todayS->toDateTimeString())->where('end_datetime', '<=', $tomorrowS->toDateTimeString())->where('approval_status','expiring')->get();
            
            if(count($expiringServiceBlocksByArea)>0)
            {
                $expiringServiceBlocksByAreaDetails = [];
                
                foreach($expiringServiceBlocksByArea as $expiringServiceBlocksByAreaEle)
                {
                    array_push($expiringServiceBlocksByAreaDetails,['model_id'=>$expiringServiceBlocksByAreaEle->getModelName(),'end_datetime'=>$expiringServiceBlocksByAreaEle->end_datetime,'notes'=>$expiringServiceBlocksByAreaEle->note]);   
                }
                array_push($expiringServiceBlocks,['area'=>$area->area,'expiring_service_blocks'=>$expiringServiceBlocksByAreaDetails]);
            }
            
            
            $storeCollectionDeliveryByArea = [];
            $storeCollectionReturnByArea = [];
            
            
            if(count($deliveryBookings)>0)
            {
                
                
                $cancelledDeliveriesByArea = [];
                $noDetailDeliveryByArea = [];
                
                $storeCollectionDeliveryByAreaCashCollection = 0;
                $storeCollectionDeliveryByAreaCollection = 0;
                
                foreach($deliveryBookings as $deliveryBooking)
                {
                    if($deliveryBooking->extra_money > 0)
                    {
                        array_push($storeCollectionDeliveryByArea,['money_collected'=>$deliveryBooking->extra_money,'collection_mode'=>$deliveryBooking->collection_mode,'notes'=>$deliveryBooking->delivery_notes,'bookingReferenceId'=>$deliveryBooking->reference_id]);
                        
                        if($deliveryBooking->collection_mode == "Cash")
                        {
                            $storeCollectionDeliveryByAreaCashCollection += $deliveryBooking->extra_money;
                        }
                        
                        $storeCollectionDeliveryByAreaCollection += $deliveryBooking->extra_money;
                    }
                    
                    if($deliveryBooking->delivery_cancelled == 1)
                    {
                        array_push($cancelledDeliveriesByArea,['bookingReferenceId'=>$deliveryBooking->reference_id, 'notes'=>$deliveryBooking->delivery_cancelled_notes,'model'=>$deliveryBooking->getBikeModelNameThis()]);
                    }
                    
                    if($deliveryBooking->is_delivered == 0)
                    {
                        array_push($noDetailDeliveryByArea,$deliveryBooking->reference_id);
                    }
                    
                }
                
                if(count($cancelledDeliveriesByArea)>0)
                    array_push($cancelledDeliveries,['area'=>$area->area,'cancelled_bookings'=>$cancelledDeliveriesByArea]);
                
                if(count($storeCollectionDeliveryByArea)>0)
                    array_push($storeCollectionDelivery,['area'=>$area->area,'collections'=>$storeCollectionDeliveryByArea,'cash_collection'=>$storeCollectionDeliveryByAreaCashCollection,'total_collection'=>$storeCollectionDeliveryByAreaCollection]);
                
                if(count($noDetailDeliveryByArea)>0)
                {
                    $bookingReferenceIds = implode(',',$noDetailDeliveryByArea);
                    array_push($undeliveredBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>$bookingReferenceIds,'count'=>count($noDetailDeliveryByArea),'total_count'=>count($deliveryBookings)]);
                }
                else
                {
                    array_push($undeliveredBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>'none','count'=>"0",'total_count'=>count($deliveryBookings)]);
                }
                
            }
            
            if(count($returnBookings)>0)
            {
                
                
                $noDetailReturnByArea = [];
                $storeCollectionReturnByAreaCashCollection = 0;
                $storeCollectionReturnByAreaCollection = 0;
                
                foreach($returnBookings as $returnBooking)
                {
                    if($returnBooking->extra_money_return > 0)
                    {
                        array_push($storeCollectionReturnByArea,['money_collected'=>$returnBooking->extra_money_return,'collection_mode'=>$returnBooking->collection_mode,'notes'=>$returnBooking->delivery_notes,'bookingReferenceId'=>$returnBooking->reference_id]);
                        
                        if($returnBooking->collection_mode == "Cash")
                        {
                            $storeCollectionReturnByAreaCashCollection += $returnBooking->extra_money_return;
                        }
                        $storeCollectionReturnByAreaCollection += $returnBooking->extra_money_return;
                        
                    }
                    
                    if($returnBooking->is_returned == 0)
                    {
                        array_push($noDetailReturnByArea,$returnBooking->reference_id);
                    }
                }
                
                if(count($storeCollectionReturnByArea)>0)
                    array_push($storeCollectionReturn,['area'=>$area->area,'collections'=>$storeCollectionReturnByArea,'cash_collection'=>$storeCollectionReturnByAreaCashCollection,'total_collection'=>$storeCollectionReturnByAreaCollection]);
                
                if(count($noDetailReturnByArea)>0)
                {
                    $bookingReferenceIds = implode(',',$noDetailReturnByArea);

                    array_push($unreturnedBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>$bookingReferenceIds,'total_count'=>count($returnBookings),'count'=>count($noDetailReturnByArea)]);
                }
                else
                {
                    array_push($unreturnedBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>'none','total_count'=>count($returnBookings),'count'=>"0"]);
                }
                
            }
            
            
            $walkinsReservations = Booking::where('created_at','>=',$today->toDateTimeString())->where('created_at','<=',$tomorrow->toDateTimeString())->where('area_id',$area->id)->where('lead_source','In-Store')->get();
            
            $walkinsReservationsByArea = [];
            $collection = 0;
            
            if(count($walkinsReservations)>0)
            {
                foreach($walkinsReservations as $walkinsReservation)
                {
                    
                    $collection += $walkinsReservation->total_price;

                    
                    array_push($walkinsReservationsByArea,['reference_id'=>$walkinsReservation->reference_id,'model'=>$walkinsReservation->getBikeModelNameThis(),'created_by'=>$walkinsReservation->getCreatedByEmail($walkinsReservation->createdBy),'notes'=>$walkinsReservation->note,'discount'=>round((($walkinsReservation->full_price-$walkinsReservation->total_price)/$walkinsReservation->full_price)*100),'duration'=>$walkinsReservation->getDuration(),'total_price'=>$walkinsReservation->total_price]);
                        
                    
                }

                array_push($walkins,['area'=>$area->area,'count'=>count($walkinsReservations),'walkin_bookings'=>$walkinsReservationsByArea,'total_collection'=>$collection]);
                
            }
            
        }
        
        
        $data = ['date' => $today->toFormattedDateString(), 'undeliveredBookingsByArea' =>$undeliveredBookingsByArea, 'unreturnedBookingsByArea' =>$unreturnedBookingsByArea,'walkins' =>$walkins,'storeCollectionDelivery' =>$storeCollectionDelivery, 'storeCollectionReturn' =>$storeCollectionReturn,'cancelledDeliveries' =>$cancelledDeliveries,'approvedServiceBlocks' =>$approvedServiceBlocks,'expiringServiceBlocks' =>$expiringServiceBlocks];
        
        
        OpsReport::create(['date'=>$today->toDateString(),'report'=>json_encode($data)]); 
        
        $roles = "operations manager";
        $opManagers = User::where('role_string',"LIKE","%".$roles."%")->where('isOperation',1)->get();
        
        foreach($opManagers as $opManager)
        { 
            
            $data = ['date' => $today->toFormattedDateString(), 'undeliveredBookingsByArea' =>$undeliveredBookingsByArea, 'unreturnedBookingsByArea' =>$unreturnedBookingsByArea,'walkins' =>$walkins,'storeCollectionDelivery' =>$storeCollectionDelivery, 'storeCollectionReturn' =>$storeCollectionReturn,'cancelledDeliveries' =>$cancelledDeliveries,'approvedServiceBlocks' =>$approvedServiceBlocks,'expiringServiceBlocks' =>$expiringServiceBlocks,'to_email'=>$opManager->email];

            Mail::send('emails.erp_admin_summary_mail',$data,function($message) use ($data) {
              $message->to($data['to_email']);
              $message->from('erp@wickedride.com', 'Operations');
              $message->subject(env('SITENAMECAP').' Operations Summary for '.$data['date']);
            });

        }
        
        $data = ['date' => $today->toFormattedDateString(), 'undeliveredBookingsByArea' =>$undeliveredBookingsByArea, 'unreturnedBookingsByArea' =>$unreturnedBookingsByArea,'walkins' =>$walkins,'storeCollectionDelivery' =>$storeCollectionDelivery, 'storeCollectionReturn' =>$storeCollectionReturn,'cancelledDeliveries' =>$cancelledDeliveries,'approvedServiceBlocks' =>$approvedServiceBlocks,'expiringServiceBlocks' =>$expiringServiceBlocks,'to_email'=>'operationsgroup@wickedride.com'];

        Mail::send('emails.erp_admin_summary_mail',$data,function($message) use ($data) {
              $message->to($data['to_email']);
              $message->from('erp@wickedride.com', 'Operations');
              $message->subject(env('SITENAMECAP').' Operations Summary for '.$data['date']);
        });
        
        
    
    }
    
    
    public function getOpsReportDataEveryDay(Request $request)
    {
    
        
        $areas = Area::where('status', 1)->get();
        
        $undeliveredBookingsByArea = []; 
        $unreturnedBookingsByArea = [];
        
        $walkins = [];
        
        $storeCollectionDelivery = [];
        $storeCollectionReturn = [];
        
        $cancelledDeliveries = [];
        
        $approvedServiceBlocks = [];
        
        foreach($areas as $area)
        {
            
            $today = Carbon::parse($request->start_date);
            $tomorrow = $today->copy()->addDay(1);
                        
            
            $deliveryBookings = Booking::where('area_id',$area->id)->where('start_datetime', '>=', $today->toDateTimeString())->where('start_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->get();
            
            
            $returnBookings = Booking::where('area_id',$area->id)->where('end_datetime', '>=', $today->toDateTimeString())->where('end_datetime', '<=', $tomorrow->toDateTimeString())->whereNotIn('user_id',array(2,5,7))->whereNotIn('user_id',array(2,5,7))->get();
            
            
            $approvedServiceBlocksByArea = ServiceBlock::where('area_id',$area->id)->where('created_at', '>=', $today->toDateTimeString())->where('created_at', '<=', $tomorrow->toDateTimeString())->where('approval_status','Approved')->get();
            
            if(count($approvedServiceBlocksByArea)>0)
            {
                $approvedServiceBlocksByAreaDetails = [];
                
                foreach($approvedServiceBlocksByArea as $approvedServiceBlocksByAreaEle)
                {
                    array_push($approvedServiceBlocksByAreaDetails,['model_id'=>$approvedServiceBlocksByAreaEle->getModelName(),'duration'=>$approvedServiceBlocksByAreaEle->getDuration(),'notes'=>$approvedServiceBlocksByAreaEle->note]);   
                }
                array_push($approvedServiceBlocks,['area'=>$area->area,'approved_service_blocks'=>$approvedServiceBlocksByAreaDetails]);
            }
            
            
            $storeCollectionDeliveryByArea = [];
            $storeCollectionReturnByArea = [];
            
            
            if(count($deliveryBookings)>0)
            {
                
                
                $cancelledDeliveriesByArea = [];
                $noDetailDeliveryByArea = [];
                
                $storeCollectionDeliveryByAreaCashCollection = 0;
                $storeCollectionDeliveryByAreaCollection = 0;
                
                foreach($deliveryBookings as $deliveryBooking)
                {
                    if($deliveryBooking->extra_money > 0)
                    {
                        array_push($storeCollectionDeliveryByArea,['money_collected'=>$deliveryBooking->extra_money,'collection_mode'=>$deliveryBooking->collection_mode,'notes'=>$deliveryBooking->delivery_notes,'bookingReferenceId'=>$deliveryBooking->reference_id]);
                        
                        if($deliveryBooking->collection_mode == "Cash")
                        {
                            $storeCollectionDeliveryByAreaCashCollection += $deliveryBooking->extra_money;
                        }
                        
                        $storeCollectionDeliveryByAreaCollection += $deliveryBooking->extra_money;
                    }
                    
                    if($deliveryBooking->delivery_cancelled == 1)
                    {
                        array_push($cancelledDeliveriesByArea,['bookingReferenceId'=>$deliveryBooking->reference_id, 'notes'=>$deliveryBooking->delivery_cancelled_notes,'model'=>$deliveryBooking->getBikeModelNameThis()]);
                    }
                    
                    if($deliveryBooking->is_delivered == 0)
                    {
                        array_push($noDetailDeliveryByArea,$deliveryBooking->reference_id);
                    }
                    
                }
                
                if(count($cancelledDeliveriesByArea)>0)
                    array_push($cancelledDeliveries,['area'=>$area->area,'cancelled_bookings'=>$cancelledDeliveriesByArea]);
                
                if(count($storeCollectionDeliveryByArea)>0)
                    array_push($storeCollectionDelivery,['area'=>$area->area,'collections'=>$storeCollectionDeliveryByArea,'cash_collection'=>$storeCollectionDeliveryByAreaCashCollection,'total_collection'=>$storeCollectionDeliveryByAreaCollection]);
                
                if(count($noDetailDeliveryByArea)>0)
                {
                    $bookingReferenceIds = implode(',',$noDetailDeliveryByArea);
                    array_push($undeliveredBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>$bookingReferenceIds,'count'=>count($deliveryBookings)]);
                }
                else
                {
                    array_push($undeliveredBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>'none','count'=>count($deliveryBookings)]);
                }
                
            }
            
            if(count($returnBookings)>0)
            {
                
                
                $noDetailReturnByArea = [];
                $storeCollectionReturnByAreaCashCollection = 0;
                $storeCollectionReturnByAreaCollection = 0;
                
                foreach($returnBookings as $returnBooking)
                {
                    if($returnBooking->extra_money_return > 0)
                    {
                        array_push($storeCollectionReturnByArea,['money_collected'=>$returnBooking->extra_money_return,'collection_mode'=>$returnBooking->collection_mode,'notes'=>$returnBooking->delivery_notes,'bookingReferenceId'=>$returnBooking->reference_id]);
                        
                        if($returnBooking->collection_mode == "Cash")
                        {
                            $storeCollectionReturnByAreaCashCollection += $returnBooking->extra_money_return;
                        }
                        $storeCollectionReturnByAreaCollection += $returnBooking->extra_money_return;
                        
                    }
                    
                    if($returnBooking->is_returned == 0)
                    {
                        array_push($noDetailReturnByArea,$returnBooking->reference_id);
                    }
                }
                
                if(count($storeCollectionReturnByArea)>0)
                    array_push($storeCollectionReturn,['area'=>$area->area,'collections'=>$storeCollectionReturnByArea,'cash_collection'=>$storeCollectionReturnByAreaCashCollection,'total_collection'=>$storeCollectionReturnByAreaCollection]);
                
                if(count($noDetailReturnByArea)>0)
                {
                    $bookingReferenceIds = implode(',',$noDetailReturnByArea);

                    array_push($unreturnedBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>$bookingReferenceIds,'count'=>count($returnBookings)]);
                }
                else
                {
                    array_push($unreturnedBookingsByArea,['area'=>$area->area,'bookingReferenceIds'=>'none','count'=>count($returnBookings)]);
                }
                
            }
            
            
            $walkinsReservations = Booking::where('created_at','>=',$today->toDateTimeString())->where('created_at','<=',$tomorrow->toDateTimeString())->where('area_id',$area->id)->where('lead_source','In-Store')->get();
            
            $walkinsReservationsByArea = [];
            $collection = 0;
            
            if(count($walkinsReservations)>0)
            {
                foreach($walkinsReservations as $walkinsReservation)
                {
                    
                    $collection += $walkinsReservation->total_price;

                    
                    array_push($walkinsReservationsByArea,['reference_id'=>$walkinsReservation->reference_id,'model'=>$walkinsReservation->getBikeModelNameThis(),'created_by'=>$walkinsReservation->getCreatedByEmail($walkinsReservation->createdBy),'notes'=>$walkinsReservation->note,'discount'=>round((($walkinsReservation->full_price-$walkinsReservation->total_price)/$walkinsReservation->full_price)*100),'duration'=>$walkinsReservation->getDuration(),'total_price'=>$walkinsReservation->total_price]);
                        
                    
                }

                array_push($walkins,['area'=>$area->area,'count'=>count($walkinsReservations),'walkin_bookings'=>$walkinsReservationsByArea,'total_collection'=>$collection]);
                
            }
            
        }
        
        
        
        
        $data = ['date' => $today->toFormattedDateString(), 'undeliveredBookingsByArea' =>$undeliveredBookingsByArea, 'unreturnedBookingsByArea' =>$unreturnedBookingsByArea,'walkins' =>$walkins,'storeCollectionDelivery' =>$storeCollectionDelivery, 'storeCollectionReturn' =>$storeCollectionReturn,'cancelledDeliveries' =>$cancelledDeliveries,'approvedServiceBlocks' =>$approvedServiceBlocks];
        
        OpsReport::create(['date'=>$today->toDateString(),'report'=>json_encode($data)]); 
        
        return "done";
    
    }
    
    public function opsReportIndex(Request $request)
    {   
        
        $start_date="";
        $end_date = "";
       
        return view('admin.opsReport.index',compact('start_date','end_date'));
    }
    
    public function opsReportExport(Request $request)
    {
        if(isset($request->start_date)&&isset($request->end_date))
        {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            
            $begin = $dt = Carbon::parse($request->start_date);
            $end = Carbon::parse($request->end_date);

            $dates = [];
            
            while($end > $dt)
            {
                $start_date = $dt->format('Y-m-d');
                array_push($dates,$start_date);
                $dt->addDay();
            }
            
            $opReports = OpsReport::whereIn('date',$dates)->get();
            
            
        }
        else
        {
            $start_date="";
            $end_date = "";
        }
        return view('admin.opsReport.index',compact('start_date','end_date'));
        
        
    }
    
    public function getModelPricesForDates(Request $request)
    {
        $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'area_id' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
        }
        

        $dispatcher = app('Dingo\Api\Dispatcher');
        $filterModels = $dispatcher->with(['start_date' => $request->start_date, 'end_date' => $request->end_date,'start_time' => $request->start_time, 'end_time' => $request->end_time,'area_id'=>$request->area_id])->get('filter/bike-count');
        
        $models =[];
        
        foreach($filterModels as $filterModel)
        {
            if($filterModel['available_count']>0)
            {
                array_push($models,['model_id'=>$filterModel['model_id'],'model_name'=>$filterModel['model_name'],'model_price'=>$filterModel['price']]);
            }
        }
        
        return $models;
        
        
    }
    
    public function getAreaPricesForDates(Request $request)
    {
        $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'model_id' => 'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationError($validator->errors());        
        }
        
        $areasData =[];
        $areaIds = Bike::where('model_id',$request->model_id)->get()->lists('area_id');
        
        $areas = Area::where('status',1)->whereIn('id',$areaIds)->get();
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        foreach($areas as $area)
        {

            $dispatcher = app('Dingo\Api\Dispatcher');
            $availability = $dispatcher->with(['start_date' => $request->start_date, 'end_date' => $request->end_date,'start_time' => $request->start_time, 'end_time' => $request->end_time,'model_id'=>$request->model_id,'city_id'=>$area->city_id,'area_id'=>$area->id])->get('bookings/price-and-check-availability');
            
            if($availability['bike_id'] != "none")
            {
                
                array_push($areasData,['area_id'=>$area->id,'area_name'=>$area->area,'area_price'=>round($availability['total_price']*round((1+($taxDetails->amount/100)),2))]);
            }
            
        }
        
        
        return $areasData;
        
        
    }
    
    
    public function get_area_dropdown(Request $request)
    {
        $data = [];
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        
        foreach($userAreas as $areaId)
        {
            $area = Area::where('id',$areaId)->first();
            array_push($data,["area_id"=>$area->id,"area_name"=>$area->area]);
        }
        
        array_push($data,["area_id"=>0,"area_name"=>"All Areas"]);
        
        return $this->respondWithSuccess($data);
        
    }
    
    
    public function delivery_today(Request $request)
    {
        
        $rules = array(
        'area_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        
        if($request->area_id == "0")
        {
            $areaIds = $userAreas;
        }
        elseif(!in_array($request->area_id,$userAreas))
        {
            return $this->respondWithSuccess([]);
        }
        else
        {
            $areaIds = array($request->area_id);
        }
        
        if(isset($request->pending))
        {
            $pending = $request->pending;
        }
        else
        {
            $pending = "off";
        }
        
        $bookingsByArea = ReservationController::getResevationsForErp("D",$areaIds,$pending,'today');
        
        return $this->respondWithSuccess($bookingsByArea);
        
        
    }
    
    
    
    public function delivery_tomorrow(Request $request)
    {
        $rules = array(
        'area_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        
        if($request->area_id == "0")
        {
            $areaIds = $userAreas;
        }
        elseif(!in_array($request->area_id,$userAreas))
        {
            return $this->respondWithSuccess([]);
        }
        else
        {
            $areaIds = array($request->area_id);
        }
        
        if(isset($request->pending))
        {
            $pending = $request->pending;
        }
        else
        {
            $pending = "off";
        }
        
        $bookingsByArea = ReservationController::getResevationsForErp("D",$areaIds,$pending,"tomorrow");
        
        return $this->respondWithSuccess($bookingsByArea);
        
        
    }
    
    
    
    public function return_today(Request $request)
    {
        $rules = array(
        'area_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        
        if($request->area_id == "0")
        {
            $areaIds = $userAreas;
        }
        elseif(!in_array($request->area_id,$userAreas))
        {
            return $this->respondWithSuccess([]);
        }
        else
        {
            $areaIds = array($request->area_id);
        }
        
        if(isset($request->pending))
        {
            $pending = $request->pending;
        }
        else
        {
            $pending = "off";
        }
        
        $bookingsByArea = ReservationController::getResevationsForErp("R",$areaIds,$pending,"today");
        
        return $this->respondWithSuccess($bookingsByArea);
        
        
    }
    
    
    
    public function movement(Request $request)
    {
        $rules = array(
        'area_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        if(!in_array($request->area_id,$userAreas))
        {
            return $this->respondWithSuccess([]);
        }
        
        $bookingsByArea = BookingController::getMovement($request->area_id);
        
        $data = [];
        
        foreach($bookingsByArea as $booking)
        {
            array_push($data,["id"=>$booking->id,"reference_id"=>$booking->reference_id,"customer_first_name"=>$booking->getUserNameThis(),"model_name"=>$booking->getBikeModelNameThis(),"model_id"=>$booking->model_id,"start_datetime"=>$booking->getReadableDateTime($booking->start_datetime),"end_datetime"=>$booking->getReadableDateTime($booking->end_datetime),"duration"=>$booking->getDuration(),'notes'=>$booking->note]);
            
        }
        
        return $this->respondWithSuccess($data);
        
        
    }
    
    
    
    public function return_tomorrow(Request $request)
    {
        $rules = array(
        'area_id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $userAreas = UserController::getLoggedInUser()->getUserZonesForOps();
        
        if($request->area_id == "0")
        {
            $areaIds = $userAreas;
        }
        elseif(!in_array($request->area_id,$userAreas))
        {
            return $this->respondWithSuccess([]);
        }
        else
        {
            $areaIds = array($request->area_id);
        }
        
        if(isset($request->pending))
        {
            $pending = $request->pending;
        }
        else
        {
            $pending = "off";
        }
        
        $bookingsByArea = ReservationController::getResevationsForErp("R",$areaIds,$pending,"tomorrow");
        
        return $this->respondWithSuccess($bookingsByArea);
        
        
    }
    
   
        
    
    
    public static function getErpBookingDetails($id)
    {
        $booking = Booking::where('id',$id)->first();
        $user = User::where('id', $booking->user_id)->first();
        $bikeModel = BikeModel::where('id', $booking->model_id)->first();
        $bikeMake = BikeMake::where('id', $bikeModel->bike_make_id)->first();
        $area = Area::where('id', $booking->area_id)->first();
        $city_id = $area->city_id;
        $city = City::where('id', $city_id)->first();
        
        $start =  Carbon::parse($booking->start_datetime);
        $end =  Carbon::parse($booking->end_datetime);

        $booking['customer_first_name'] = $user->first_name;
        $booking['customer_email'] = $user->email;
        $booking['customer_mobile'] = $user->mobile_num;
        $booking['model_name'] = $bikeModel->bike_model;
        $booking['make_name'] = $bikeMake->bike_make;
        $booking['make_id'] = $bikeMake->id;
        $booking['area_name'] = $area->area;
        $booking['city_name'] = $city->city;
        $booking['city_id'] = $city->id;
        $booking['start_date'] = $start->format('Y-m-d');
        $booking['end_date'] = $end->format('h:i');
        $booking['start_time'] = $start->format('Y-m-d');
        $booking['end_time'] = $end->format('h:i');
        $booking['customer_instructions'] = $booking->note;
        $booking['notes_for_operations'] = $booking->backend_notes;
        $booking['notes_for_operations_ack_by'] = $booking->getBackEndNotesAckByEmail();
        $booking['created_by'] = $booking->getCreatedByEmail($booking->createdBy);
        $booking['delivered_by'] = $booking->getDeliveredByEmail();
        
        return $booking;
    
    }
    
    
    public static function getErpReservationDetails($id)
    {
        $booking = ReservationInfo::where('id',$id)->first();
        $user = $booking->user;
        $bikeModel = $booking->model;
        $bikeMake = $bikeModel->make;
        $area = $booking->area;
        $city_id = $area->city_id;
        $city = City::where('id', $city_id)->first();
        
        $start =  Carbon::parse($booking->start_datetime);
        $end =  Carbon::parse($booking->end_datetime);
        
        $booking['reference_id'] = $booking->bookingHeader->ref_id;
        $booking['customer_first_name'] = $user->first_name;
        $booking['customer_email'] = $user->email;
        $booking['customer_mobile'] = $user->mobile_num;
        $booking['model_name'] = $bikeModel->bike_model;
        $booking['make_name'] = $bikeMake->bike_make;
        $booking['make_id'] = $bikeMake->id;
        $booking['area_name'] = $area->area;
        $booking['city_name'] = $city->city;
        $booking['city_id'] = $city->id;
        $booking['start_date'] = $start->format('Y-m-d');
        $booking['end_date'] = $end->format('h:i');
        $booking['start_time'] = $start->format('Y-m-d');
        $booking['end_time'] = $end->format('h:i');
        $booking['readableStartDate'] = $booking->readableStartDateTime();
        $booking['readableEndDate'] = $booking->readableEndDateTime();
        $booking['notes'] = $booking->activeNotes();
        
        return $booking;
    
    }
    
    
    public function delivery_details(Request $request)
    {
        $rules = array(
            'id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $booking = self::getErpBookingDetails($request->id);
        
        $user = User::where('id',$booking->user_id)->first();
        
        $userDocs = UserDocImage::where('user_id',$user->id)->get();
        
        $documentsByTypes = $userDocs->groupBy('doc_type');
        
        $userDocsResult = [];
        
        foreach($documentsByTypes as $doc_type => $documentsByTypesEl)
        {
            $docUrls = [];
            foreach($documentsByTypesEl as $doc)
            {
                if(substr($doc->doc,0,4) == "http")
                {
                    $url = $doc->doc;
                }
                else
                {
                    $url = env('HTTPSORHTTP').":".$doc->doc;
                }
                array_push($docUrls,["url"=>$url,'status'=>$doc->status?true:false]);
            }
            array_push($userDocsResult,["doc_type"=>$doc_type,"doc_images"=>$docUrls]);
        }
        $booking["user_doc"] = $userDocsResult;
        
        if(($booking->is_delivered == 1)&&($booking->delivery_cancelled == 0))
        {
            $accessories_delivered_edit = array(
                [
                    "accessory_type"=>"Helmet",
                    "quantity"=>2,
                    "accessories_id"=>array("hs1","gf2")
                ],
                [
                    "accessory_type"=>"Jacket",
                    "quantity"=>3,
                    "accessories_id"=>array("hs1","gf2","ty2")
                ],
                [
                    "accessory_type"=>"Saddle Bags",
                    "quantity"=>1,
                    "accessories_id"=>array("sb1")
                ]
            );
            $edit_details = [
                    "start_odo"=>$booking->start_odo,
                    "route_plan"=>$booking->route_plan,
                    "bike_number"=>$booking->getBikeNumber(),
                    "bike_number_id"=>$booking->bike_number_id,
                /*
                    "helmet_name_1"=>$booking->getHelmetName($booking->helmet_id_1),
                    "helmet_id_1"=>$booking->helmet_id_1,
                    "helmet_name_2"=>$booking->getHelmetName($booking->helmet_id_2),
                    "helmet_id_2"=>$booking->helmet_id_2,
                    "jacket_name_1"=>$booking->getJacketName($booking->jacket_id_1),
                    "jacket_id_1"=>$booking->jacket_id_1,
                    "jacket_name_2"=>$booking->getJacketName($booking->jacket_id_2),
                    "jacket_id_2"=>$booking->jacket_id_2,
                
                    "gloves"=>$booking->gloves,
                    "go_pro"=>$booking->go_pro,
                    "knee_gaurd"=>$booking->knee_gaurd,
                    "saddle_bags"=>$booking->saddle_bags,
                    "mobile_holder"=>$booking->mobile_holder,
                    "tank_bag"=>$booking->tank_bag,
                
                    "extra_money"=>$booking->extra_money,
                    "collection_mode"=>$booking->collection_mode,
                    
                */
                    "delivery_notes"=>$booking->delivery_notes,
                    "accessories_rented"=>$accessories_delivered_edit
                    
            ];
            
            
            $booking['delivery_status'] = "delivered";
            $booking['edit_details'] = $edit_details;
        }
        elseif($booking->delivery_cancelled == 1)
        {
            $edit_details = ["delivery_cancelled_notes"=>$booking->delivery_cancelled_notes];
            $booking['delivery_status'] = "cancelled";
            $booking['edit_details'] = $edit_details;
        }
        else
        {
            $edit_details = [];
            $booking['delivery_status'] = "pending";
            $booking['edit_details'] = $edit_details;
        }
        
        /*
        $helmet_options = [];
        $helmets = Helmet::all();
        foreach($helmets as $helmet)
        {
            array_push($helmet_options,["id"=>$helmet->id,"name"=>$helmet->item_id]);
        }
        $booking["helmet_options"] = $helmet_options;
        
        
        $jacket_options = [];
        $jackets = Jacket::all();
        foreach($jackets as $jacket)
        {
            array_push($jacket_options,["id"=>$jacket->id,"name"=>$jacket->item_id]);
        }
        $booking["jacket_options"] = $jacket_options;
        */
        
        
        
        
        $booking["payment_method_list"] = PaymentMethods::where('display',1)->get()->pluck('method')->all();
        $booking["accessories_list"] = AccessoriesList::get()->pluck('accessory')->all();
        $booking["reason_for_renting_list"] = ReasonForRenting::get()->pluck('reason')->all();
        $booking["invoice_type"] = InvoiceType::where('display',1)->get()->pluck('type')->all();
        
        

        return $this->respondWithSuccess($booking);
        
        
    }
    
    
    public function return_details(Request $request)
    {
        $rules = array(
            'id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $booking = self::getErpBookingDetails($request->id);
        
        if(($booking->is_delivered == 1)&&($booking->delivery_cancelled == 0))
        {                    
            $booking['bike_number'] = $booking->getBikeNumber();
            $booking['helmet_name_1'] = $booking->getHelmetName($booking->helmet_id_1);
            $booking['helmet_name_2'] = $booking->getHelmetName($booking->helmet_id_2);
            $booking['jacket_name_1'] = $booking->getJacketName($booking->jacket_id_1);
            $booking['jacket_name_2'] = $booking->getJacketName($booking->jacket_id_2);
            $booking['delivery_status'] = "delivered";
        }
        elseif($booking->delivery_cancelled == 1)
        {
            $booking['delivery_status'] = "cancelled";
            
        }
        else
        {
            $booking['delivery_status'] = "pending";
        }
 
        
        if($booking->is_returned == 1)
        {
            $edit_details = [
                    "end_odo"=>$booking->end_odo,
                    "returned_with_problems"=>$booking->returned_with_problems,
                    "returned_with_problems_accessory"=>$booking->returned_with_problems_accessory,
                    "returned_with_problems_note"=>$booking->returned_with_problems_note,
                    "extra_money_return"=>$booking->extra_money_return,
                    "collection_mode_return"=>$booking->collection_mode_return,
                    "delivery_notes"=>$booking->delivery_notes
            ];
            
            
            $booking['edit_details'] = $edit_details;
            $booking['return_status'] = "returned";
        }
        else
        {
            $edit_details = [];
            $booking['edit_details'] = $edit_details;
            $booking['delivery_status'] = "pending";
        }
        
        
        
        
        $booking["collection_mode_options"] = ["Cash","Card","RazorPay","PayTm","PayUMoney","PayZapp","PayPal","Bank Transfer"];
        
        
        return $this->respondWithSuccess($booking);
        
        
    }
    
    
    public function collidingBookingsIndex($id,Request $request)
    {
        $serviceBlock = ServiceBlock::where('id',$id)->first();
        
        $startDateTimeCarbon = Carbon::parse($serviceBlock->start_datetime);
        $endDateTimeCarbon = Carbon::parse($serviceBlock->end_datetime);
        
        $result = BookingController::getCollidingBookingsForDates($startDateTimeCarbon,$endDateTimeCarbon,$serviceBlock->area_id,$serviceBlock->model_id);
        
        $serviceBlockTimePeriod = $startDateTimeCarbon->format('j M-y ga')." - ".$endDateTimeCarbon->format('j M-y ga');
        
        return view('admin.collidingBookings.index',compact('result','serviceBlockTimePeriod'));
        
    }
    
    
    function base64_to_image($base64_string,$docImageDestinationPath) {
        

        $output_file = base64_decode($base64_string);
         
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
        $docImageHashName = $str.'.'.'jpeg';
        
        $s3 = \Storage::disk('s3');
        $filePath = $docImageDestinationPath.$docImageHashName;
        $s3->put($filePath, $output_file, 'public');
        
        
        if($s3)
            $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
        else
            $image = "";
        
        return ["success"=>true, "image"=>$image];
    }
    
    function base64_with_header_to_image($base64_string,$docImageDestinationPath) {
        
        $str1 = explode(",",$base64_string);
        if(!isset($str1[1]))
        {
            return ["success"=>false, "image"=>""];
        }
        
        $str2 = explode(";",$str1[0]);
        $str3 = explode("/",$str2[0]);
        $output_file = base64_decode($str1[1]);
         
        $str=substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
        $docImageHashName = $str.'.'.$str3[1];
        
        $s3 = \Storage::disk('s3');
        $filePath = $docImageDestinationPath.$docImageHashName;
        $s3->put($filePath, $output_file, 'public');
        
        
        if($s3)
            $image = env('AWSS3URL').$docImageDestinationPath.$docImageHashName;
        else
            $image = "";
        
        return ["success"=>true, "image"=>$image]; 
    }
    
    public function uploadUserDoc(Request $request)
    {
        
        $booking = Booking::where('id',$request->id)->first();
        if(!$booking)
            return redirect()->back();
        
        
        if(isset($request->idImageType))
        {
            
            foreach($request->idImageType as $index=>$idImageType)
            {
                
                $docImageDestinationPath = 'img/user/document/';
                
                    
                $createImage = Self::base64_with_header_to_image($request['imageBase64'.$index],$docImageDestinationPath);

                
                if($createImage['success'])
                {
                    $insert = UserDocImage::create(['user_id' =>$booking->user_id, 'doc_type' => $idImageType,'doc' => $createImage['image'],'doc_reason' => '','status' => 1]);
                }
            }
        }
        
        return redirect()->back();
        
    }
    
    public function updateNotesForServiceBlock($id, Request $request)
    {
        $booking = Booking::where('id',$id)->first();
        if(!$booking)
            return redirect()->back();
        
        Booking::where('id',$id)->update(['backend_notes'=>$request->note]);
        
        $serviceBlock = ServiceBlock::where('booking_id',$id)->first();
        if(!$serviceBlock)
            return redirect()->back();
        
        ServiceBlock::where('id',$serviceBlock->id)->update(['note'=>$serviceBlock->note." - Note Added (".Carbon::now()->toDateTimeString().") - ".$request->note]);
        
        return redirect()->back()->with('message','Note Saved');
    }
    
    
    public function getExtraAmount(Request $request)
    {
        Validator::extend('greater_than', function($attribute, $value, $parameters)
        {
            $other = \Input::get($parameters[0]);
            return isset($other) and intval($value) > intval($other);
        });
        
        $rules = array(
            'booking_ref' => 'required|exists:bookings,reference_id',
            'start_odo'=>'required|numeric',
            'end_odo' => 'required|numeric|greater_than:start_odo'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $booking = Booking::where('reference_id',$request->booking_ref)->first();
        
        $kmDone = $request->end_odo - $request->start_odo;
            
        if($booking->fuel_included == 1)
        {
            $extra_money = $kmDone*$booking->getFuelIncludedPrice();
        }
        else
        {
            if($kmDone > $booking->KMlimit)
            {
                $extra_money = ($kmDone - $booking->KMlimit)*($booking->getExtraKMPrice());
            }
            else
            {
                $extra_money = 0;    
            }
        }
        
        return $this->respondWithSuccess(["extra_amount"=>$extra_money]);
        
            
    }
    
    
    public function reservationDeliveryDetails(Request $request)
    {
        $rules = array(
        'id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $booking = self::getErpReservationDetails($request->id);
        
        $user = $booking->user;
        
        $walletBalanceData = WalletController::checkWalletBalanceErpFunction($user->id);
        
        $userDocs = UserDocImage::where('user_id',$user->id)->get();
        
        $documentsByTypes = $userDocs->groupBy('doc_type');
        
        $userDocsResult = [];
        
        foreach($documentsByTypes as $doc_type => $documentsByTypesEl)
        {
            $docUrls = [];
            foreach($documentsByTypesEl as $doc)
            {
                if(substr($doc->doc,0,4) == "http")
                {
                    $url = $doc->doc;
                }
                else
                {
                    $url = env('HTTPSORHTTP').":".$doc->doc;
                }
                array_push($docUrls,["url"=>$url,'status'=>$doc->status?true:false]);
            }
            array_push($userDocsResult,["doc_type"=>$doc_type,"doc_images"=>$docUrls]);
        }
        
        $booking["user_doc"] = $userDocsResult;
        $booking["user_balance"] = $walletBalanceData['balance'];
        
        $deliveryInfo = $booking->deliveryInfo;
        
        if(!(!$deliveryInfo))
        {
            $accessories = [];
            
            $accessoriesReservationInfo = $booking->activeAccessoryReservations()->groupBy('accessory_model_id');
            foreach($accessoriesReservationInfo as $model_id=>$accessoriesReservationInfoEl)
            {
                $accessory = [
                    "accessory_type"=>$accessoriesReservationInfoEl->first()->accessoryModel->model,
                    "quantity"=>$accessoriesReservationInfoEl->count(),
                    "accessories_id"=>array()
                ];
                
                array_push($accessories,$accessory);
            }
            
            $deliveryImages = $booking->getDeliveryImages();
            
            $edit_details = [
                "start_odo"=>$deliveryInfo->starting_odo,
                "bike_number"=>$deliveryInfo->bike->number,
                "bike_number_id"=>$deliveryInfo->bike_id,
                "reasonForRenting"=>$booking->reasonForRenting?$booking->reasonForRenting->reason:"",
                "accessories_rented"=>$accessories,
                "delivery_images"=>$deliveryImages
            ];
            
            
            $booking['delivery_status'] = "delivered";
            $booking['edit_details'] = $edit_details;
        }
        else
        {
            $edit_details = [
                "start_odo"=>0,
                "bike_number"=>"",
                "bike_number_id"=>0,
                "reasonForRenting"=>"",
                "accessories_rented"=>[],
                "delivery_images"=>[]
            ];
            $booking['delivery_status'] = "pending";
            $booking['edit_details'] = $edit_details;
        }
        
      
        
        
        $booking["payment_method_list"] = PaymentMethods::where('display',1)->get()->pluck('method')->all();
        $booking["accessories_list"] = AccessoriesList::get()->pluck('accessory')->all();
        $booking["reason_for_renting_list"] = ReasonForRenting::get()->pluck('reason')->all();
        $booking["invoice_type"] = InvoiceType::get()->pluck('type')->all();
        
        $bookingObj = collect($booking)->except('user');

        return $this->respondWithSuccess($bookingObj);
        
        
    }
    
    
    public function reservationReturnDetails(Request $request)
    {
        $rules = array(
        'id' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $booking = self::getErpReservationDetails($request->id);
        
        $user = $booking->user;
                
        $walletBalanceData = WalletController::checkWalletBalanceErpFunction($user->id);
        $booking["user_balance"] = $walletBalanceData['balance'];
        
        $userDocs = UserDocImage::where('user_id',$user->id)->get();
        $booking["user_balance"] = $walletBalanceData['balance'];
        
        $deliveryInfo = $booking->deliveryInfo;
        
        if(!(!$deliveryInfo))
        {
            $accessories = [];
            
            $accessoriesReservationInfo = $booking->activeAccessoryReservations()->groupBy('accessory_model_id');
            foreach($accessoriesReservationInfo as $model_id=>$accessoriesReservationInfoEl)
            {
                $accessory = [
                    "accessory_type"=>$accessoriesReservationInfoEl->first()->accessoryModel->model,
                    "quantity"=>$accessoriesReservationInfoEl->count(),
                    "accessories_id"=>array()
                ];
                
                array_push($accessories,$accessory);
            }
            
            $deliveryImages = $booking->getDeliveryImages();
            
            $delivery_details = [
                "start_odo"=>$deliveryInfo->starting_odo,
                "bike_number"=>$deliveryInfo->bike->number,
                "bike_number_id"=>$deliveryInfo->bike_id,
                "accessories_rented"=>$accessories,
                "delivery_images"=>$deliveryImages
            ];
            
            
            $booking['delivery_status'] = "delivered";
            $booking['delivery_details'] = $delivery_details;
        }
        else
        {
            $delivery_details = [
                "start_odo"=>0,
                "bike_number"=>"",
                "bike_number_id"=>0,
                "accessories_rented"=>[],
                "delivery_images"=>[]
            ];
            $booking['delivery_status'] = "pending";
            $booking['delivery_details'] = $delivery_details;
        }
        
        
        $returnInfo = $booking->returnInfo;
        
        if(!(!$returnInfo))
        {
            $deliveryImages = $booking->getReturnImages();
            
            $return_details = [
                "end_odo"=>$returnInfo->ending_odo,
                "return_images"=>$deliveryImages
            ];
            
            
            $booking['return_status'] = "returned";
            $booking['edit_details'] = $return_details;
        }
        else
        {
            $return_details = [
                "end_odo"=>0,
                "return_images"=>[]
            ];
            $booking['return_status'] = "pending";
            $booking['edit_details'] = $return_details;
        }
                
        
        $booking["payment_method_list"] = PaymentMethods::where('display',1)->get()->pluck('method')->all();
        $booking["accessories_list"] = AccessoriesList::get()->pluck('accessory')->all();
        $booking["reason_for_renting_list"] = ReasonForRenting::get()->pluck('reason')->all();
        $booking["invoice_type"] = InvoiceType::get()->pluck('type')->all();
        
        $bookingObj = collect($booking)->except('user');

        return $this->respondWithSuccess($bookingObj);
        
        
    }
    
    
    
    public function deliverReservation($id,Request $request)
    {
        
        $rules = array(
          'start_odo'=>'required',
          'bike_number_id'=>'required',
          'platform'=>'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());
        }
            
        
         
        
        $user = UserController::getLoggedInUser();
        $delivered_by = $user->id;
        
        $reason_for_renting = ReasonForRenting::where('reason',$request->reason_for_renting)->first();
        
        if(!(!$reason_for_renting))
        {
            ReservationInfo::where('id',$id)->update([
                'reason_for_renting'=>$reason_for_renting->id
            ]);
        }
        
        $reservationInfo = ReservationInfo::where('id',$id)->first();
        
        ReservationInfo::where('reference_reservation',$reservationInfo->id)->has('pricingInfo','<',1)->update([
            'deleted'=>1,
            'deleted_by'=>$delivered_by,
            'deleted_at'=>Carbon::now()->toDateTimeString(),
            'current_state'=>"deleted"
        ]);

        $bike_number_id = $request->bike_number_id;
        $start_odo = $request->start_odo;
        
        $delivery_notes = (isset($request->delivery_notes))?$request->delivery_notes:"";
        if($delivery_notes != "")
        {
            NotesInfo::create([
                'booking_id'=>$reservationInfo->booking_id,
                'nature'=>"Delivery notes",
                'text'=>$delivery_notes,
                'created_by'=>$delivered_by
            ]);
        }
        
        $delivery_cancelled = 0;
        $delivered_at = Carbon::now()->toDateTimeString();
        
        
        if(isset($request->user_doc))
        {
            
            foreach($request->user_doc as $docArray)
            {
                foreach($docArray['doc_images'] as $docImageObj)
                {
                    $docImageDestinationPath = 'img/user/document/';
                    
                    $createImage = Self::base64_to_image($docImageObj['image'],$docImageDestinationPath);
                    
                    if($createImage['success'])
                    {
                        $insert = UserDocImage::create([
                            'user_id'=>$reservationInfo->user_id, 
                            'doc_type' => $docArray['doc_type'],
                            'doc' => $createImage['image'],
                            'doc_reason' => '',
                            'status' => 1
                        ]);
                    }
                }
                
            }
        }
        
        
        if(isset($request->bike_images))
        {
            
            foreach($request->bike_images as $bikeImagesArray)
            {
                $docImageDestinationPath = 'img/booking/bikeimages/';

                $createImage = Self::base64_to_image($bikeImagesArray['image'],$docImageDestinationPath);

                if($createImage['success'])
                {
                    $insert  = DB::table('delivery_images')->insert([
                        'reservation_id'=>$reservationInfo->id,
                        'delivery_or_return'=>"D",
                        'url' => $createImage['image'],
                        'created_by' => $delivered_by
                    ]);
                }
            }
        }
        
        $existingDeliveryInfo = $reservationInfo->deliveryInfo;
        if(!$existingDeliveryInfo)
        {
            DeliveryInfo::create([
                "bike_or_accessory"=>"B",
                "reservation_id"=>$reservationInfo->id,
                "delivered_by"=>$delivered_by,
                "delivered_at"=>$delivered_at,
                "bike_id"=>$bike_number_id,
                "starting_odo"=>$start_odo,
                "created_by" => $delivered_by
            ]);
        }
        else
        {
            DeliveryInfo::where('id',$existingDeliveryInfo->id)->update([
                "bike_or_accessory"=>"B",
                "reservation_id"=>$reservationInfo->id,
                "delivered_by"=>$delivered_by,
                "delivered_at"=>$delivered_at,
                "bike_id"=>$bike_number_id,
                "starting_odo"=>$start_odo,
                "updated_by" => $delivered_by
            ]);
        }
        
        $invoices = [];
        
        $customer = User::where('id',$reservationInfo->user_id)->first();
        $balance = round($customer->getBalance(),2);
        if($balance > 0)
        {
            $invoice = [
                "invoice_type"=>"Pending Payment",
                "suggested_amount"=>$balance,
                "description_string"=>"Collect full amount"
            ];

            array_push($invoices,$invoice);
        }
        
        if($request->accessories != [])
        {
            $accessoriesCountArray = Self::createAccessoryReservationForDelivery($request->accessories,$reservationInfo,$delivered_by);
            foreach($request->accessories as $accessory)
            {   
                if($accessoriesCountArray[$accessory['accessory_type']] > 0)
                {
                    $accessoryObj = AccessoriesList::where('accessory',$accessory['accessory_type'])->first();
                    $startDateTimeCarbon = Carbon::parse($reservationInfo->start_datetime);
                    $endDateTimeCarbon = Carbon::parse($reservationInfo->end_datetime);
                    $price = $accessoryObj->price*($endDateTimeCarbon->diffInHours($startDateTimeCarbon))*$accessoriesCountArray[$accessory['accessory_type']];
                    if($price>0){
                    $invoice = [
                        "invoice_type"=>$accessory['accessory_type'],
                        "suggested_amount"=>$price,
                        "description_string"=>"qty: ".$accessoriesCountArray[$accessory['accessory_type']]
                    ];
                    array_push($invoices,$invoice);
                    }
                }
            }
        }
        
        Booking::where('id',$id)->update([
            "bike_number_id"=>$bike_number_id
        ]);
        
        return $this->respondWithSuccess(['status'=>true, 'message'=>'delivered','invoices'=>$invoices]);
        
    }
    
    public function returnReservation($id,Request $request)
    {
        
        $rules = array(
          'end_odo'=>'required',
          'platform'=>'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());
        }
        
        $user = UserController::getLoggedInUser();
        $returned_by = $user->id;
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
                
        $reservationInfo = ReservationInfo::where('id',$id)->first();
        
        $deliveryInfo = $reservationInfo->deliveryInfo;
        if(!$deliveryInfo)
        {
            return $this->respondWithSuccess(['status'=>false,'message'=>'This reservation has not been delivered','invoices'=>[]]);
        }
        
        $end_odo = $request->end_odo;
        
        $return_notes = (isset($request->return_notes))?$request->return_notes:"";
        if($return_notes!="")
        {
            NotesInfo::create([
                'booking_id'=>$reservationInfo->booking_id,
                'nature'=>"Return notes",
                'text'=>$return_notes,
                'created_by'=>$returned_by
            ]);
        }
        
        $returned_at = Carbon::now()->toDateTimeString();
                
        
        if(isset($request->bike_images))
        {
            
            foreach($request->bike_images as $bikeImagesArray)
            {
                $docImageDestinationPath = 'img/booking/bikeimages/';

                $createImage = Self::base64_to_image($bikeImagesArray['image'],$docImageDestinationPath);

                if($createImage['success'])
                {
                    $insert  = DB::table('delivery_images')->insert([
                        'reservation_id'=>$reservationInfo->id,
                        'delivery_or_return'=>"R",
                        'url' => $createImage['image'],
                        'created_by' => $returned_by
                    ]);
                }
            }
        }
        
        $returnInfo = $reservationInfo->returnInfo;
        
        if(!$returnInfo)
        {
            ReturnInfo::create([
                "bike_or_accessory"=>"B",
                "reservation_id"=>$reservationInfo->id,
                "returned_by"=>$returned_by,
                "returned_at"=>$returned_at,
                "ending_odo"=>$end_odo,
                "created_by" => $returned_by
            ]);
        }
        else
        {
            ReturnInfo::where('id',$returnInfo->id)->update([
                "bike_or_accessory"=>"B",
                "reservation_id"=>$reservationInfo->id,
                "returned_by"=>$returned_by,
                "returned_at"=>$returned_at,
                "ending_odo"=>$end_odo,
                "created_by" => $returned_by
            ]);
        }
        
        $invoices = [];
        
        $customer = User::where('id',$reservationInfo->user_id)->first();
        $balance = round($customer->getBalance(),2);
        if($balance > 0)
        {
            $invoice = [
                "invoice_type"=>"Pending Payment",
                "suggested_amount"=>$balance,
                "description_string"=>"Collect full amount"
            ];

            array_push($invoices,$invoice);
        }
        
        $kmDone = $request->end_odo - $deliveryInfo->starting_odo;
        
        if($reservationInfo->fuel_included)
        {
            $extraKMDone = $kmDone;
            $extraKMPrice = $reservationInfo->getFuelIncludedPrice();
            $extraKmCharge = $extraKMPrice*$extraKMDone*(1+($taxDetails->amount/100));
        }
        else
        {
            if($kmDone > $reservationInfo->KMlimit)
            {
                $extraKMDone = ($kmDone - $reservationInfo->KMlimit);
                $extraKMPrice = $reservationInfo->getExtraKMPrice();
                $extraKmCharge = $extraKMPrice*$extraKMDone*(1+($taxDetails->amount/100));
            }
            else
            {
                $extraKmCharge = 0;    
            }
        }
        
        if($extraKmCharge > 0)
        {
            $invoice = [
                "invoice_type"=>"Extra KM",
                "suggested_amount"=>$extraKmCharge,
                "description_string"=>$extraKMDone." Km done. Charged at Rs.".$extraKMPrice." per KM"
            ];

            array_push($invoices,$invoice);
        }
        
        return $this->respondWithSuccess(['status'=>true, 'message'=>'returned','invoices'=>$invoices]);
        
    }
    
    
    public function createAccessoryReservationForDelivery($accessories,$reservationInfo,$delivered_by)
    {
        $accessoriesReservationInfo = $reservationInfo->activeAccessoryReservations()->groupBy('accessory_model_id')->toArray();
        
        $accessoriesCountArray = []; 
        
        foreach($accessories as $accessory)
        {   
            $accessoryObj = AccessoriesList::where('accessory',$accessory['accessory_type'])->first();
            if(array_key_exists($accessoryObj->id,$accessoriesReservationInfo))
            {
                $count = count($accessoriesReservationInfo[$accessoryObj->id]);
            }
            else
            {
                $count = 0;
            }
            
            $accessoriesCountArray[$accessory['accessory_type']] = $accessory['quantity'] - $count;
            
            if($accessory['quantity'] > $count)
            {
                $diffQty = $accessory['quantity'] - $count;
                while($diffQty > 0)
                {
                    ReservationInfo::create([
                        'booking_id'=>$reservationInfo->booking_id,
                        'reference_reservation'=>$reservationInfo->id,
                        'bike_or_accessory'=>"A",
                        'accessory_model_id'=>$accessoryObj->id, 
                        'accessory_size_id'=>1, 
                        'area_id'=>$reservationInfo->area_id, 
                        'start_datetime'=>$reservationInfo->start_datetime, 
                        'end_datetime'=>$reservationInfo->end_datetime,
                        'user_id'=>$reservationInfo->user_id,
                        'creation_mode'=>"S",
                        'creation_reason'=>1,
                        'current_state'=>"Delivered",
                        'created_by'=>$delivered_by
                    ]);

                    $diffQty--;
                }
            } 
            elseif($accessory['quantity'] < $count)
            {
                $diffQty = $count - $accessory['quantity'];
                while($diffQty > 0)
                {
                    $reservation = ReservationInfo::where('reference_reservation',$reservationInfo->id)->where('bike_or_accessory',"A")->where('accessory_model_id',$accessoryObj->id)->where('deleted',0)->orderByRaw(\DB::raw("FIELD(creation_mode, 'S','C','W','A','I')"))->first();

                    ReservationInfo::where('id',$reservation->id)->update([
                        'deleted'=>1,
                        'deleted_by'=>$delivered_by,
                        'deleted_at'=>Carbon::now()->toDateTimeString(),
                        'current_state'=>"deleted"
                    ]);

                    PricingInfo::where('id',$reservation->id)->update([
                        'deleted'=>1,
                        'deleted_by'=>$delivered_by,
                        'deleted_at'=>Carbon::now()->toDateTimeString(),
                    ]);

                    $financialInfoWithoutTax = FinancialInfo::where('reservation_id',$reservation->id)->where('payment_nature',12)->where('deleted',0)->first();
                    if(!(!$financialInfoWithoutTax))
                    {
                        FinancialInfo::create([
                            'booking_id'=>$reservation->booking_id,
                            'user_id'=>$reservation->user_id,
                            'debit_or_credit'=>"C",
                            'new_or_edit'=>"D",
                            'reservation_id'=>$reservation->id,
                            'amount'=>$financialInfoWithoutTax->amount,
                            'payment_method'=>0,
                            'payment_id'=>"na",
                            'payment_nature'=>14,
                            'status'=>"na",
                            'created_by'=>$delivered_by
                        ]);

                    }
                    $financialInfoTax = FinancialInfo::where('reservation_id',$reservation->id)->where('payment_nature',13)->where('deleted',0)->first();
                    if(!(!$financialInfoTax))
                    {
                        FinancialInfo::create([
                            'booking_id'=>$reservation->booking_id,
                            'user_id'=>$reservation->user_id,
                            'debit_or_credit'=>"C",
                            'new_or_edit'=>"D",
                            'reservation_id'=>$reservation->id,
                            'amount'=>$financialInfoTax->amount,
                            'payment_method'=>0,
                            'payment_id'=>"na",
                            'payment_nature'=>15,
                            'status'=>"na",
                            'created_by'=>$delivered_by
                        ]);
                    }

                    $diffQty--;
                }
            }
        }
        
        return $accessoriesCountArray;
        
    }
    
    
    public function submitInvoicePayment(Request $request)
    {
        $rules = array(
            'booking_id'=>'required|exists:reservation_info,id',
            'delivery_or_return'=>'required'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return $this->respondWithValidationErrorStatusCode($validator->errors());
        }
        
        if($request->delivery_or_return == "D")
        {
            $nature = 36;
        }
        else
        {
            $nature = 37;
        }
        
        $createdByUser = UserController::getLoggedInUser();
        
        $accessoriesList = AccessoriesList::get()->pluck('accessory')->all(); 
        $totalAccessoriesInvoice = 0;
        $totalInvoiceAmount = 0;
        $invoiceArray = [];
        foreach($request->invoices as $invoice)
        {
            if(in_array($invoice['invoice_nature'],$accessoriesList))
                $totalAccessoriesInvoice += $invoice['invoice_amount'];
            
            $totalInvoiceAmount += $invoice['invoice_amount'];
            
            $invoiceArray[$invoice['invoice_nature']] = $invoice['invoice_amount'];
        }
        
        $reservationInfo = ReservationInfo::where('id',$request->booking_id)->first();
        $activeAccessoriesGroupsWithoutPricing = $reservationInfo->activeAccessoryReservationsWithoutPricing()->groupBy('accessory_model_id');
        
        $promoWalletAmountRedeemed = 0;
            
        foreach($request->payments as $payment)
        {
            $paymentMethod = PaymentMethods::where('method',$payment['payment_method'])->first();
            
            $payment['payment_method'] = $paymentMethod->id;
            if(!array_key_exists('payment_id',$payment))
            {
                $payment['payment_id'] = "na";
            }
            
            $payment['payment_status'] = "Complete";
            
            $promoWalletAmountRedeemed += ReservationController::parseFinancialInfo($payment, $totalInvoiceAmount, "", $reservationInfo->user, $reservationInfo->booking_id, $createdByUser, "E", $nature, $reservationInfo->id, 0, 0);
        }
        
        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        $taxableTotalInvoiceAmount = ($totalInvoiceAmount - $promoWalletAmountRedeemed)/(1+($taxDetails->amount/100));
        
        $accessoriesTotal = 0;
        $accessoriesTax = 0;
         
        foreach($activeAccessoriesGroupsWithoutPricing as $modelId => $activeAccessoriesWithoutPricing)
        {
            $accessory = AccessoriesList::where('id',$modelId)->first();
            $startDateTimeCarbon = Carbon::parse($reservationInfo->start_datetime);
            $endDateTimeCarbon = Carbon::parse($reservationInfo->end_datetime);
            $price = $accessory->price*($endDateTimeCarbon->diffInHours($startDateTimeCarbon));
            $totalAmountWithTaxForType = $invoiceArray[$accessory->accessory];
            $totalTaxableForType  = ($taxableTotalInvoiceAmount/$totalInvoiceAmount)*$totalAmountWithTaxForType;
            $totalTaxForType = $totalTaxableForType*($taxDetails->amount/100);
            
            $accessoriesTotal += round($totalAmountWithTaxForType,2);
            $accessoriesTax += round($totalTaxForType,2);
            
            $indAmountWithTax = round(($totalAmountWithTaxForType/count($activeAccessoriesWithoutPricing)),2);
            $indTaxableAmount = round(($totalTaxableForType/count($activeAccessoriesWithoutPricing)),2);
            $indTax = round(($totalTaxForType/count($activeAccessoriesWithoutPricing)),2);
            
            foreach($activeAccessoriesWithoutPricing as $accessRes)
            {
                $pricingInfo = PricingInfo::create([
                    'bike_or_accessory'=>"A",
                    'reservation_id'=>$accessRes->id,
                    'pricing_id'=>0,
                    'full_price'=>$price,
                    'promo_discount'=>$price - $indAmountWithTax,
                    'price_after_discount'=>$indAmountWithTax - $indTax,
                    'tax_id'=>1,
                    'taxable_price'=>$indTaxableAmount,
                    'tax_amount'=>$indTax,
                    'price_after_tax'=>$indAmountWithTax,
                    'promo_code'=>""
                ]);
            }
        }
        
        
        
        if($totalAccessoriesInvoice > 0)
        {
            FinancialInfo::create([
                'booking_id'=>$reservationInfo->booking_id,
                'user_id'=>$reservationInfo->user_id,
                'debit_or_credit'=>"D",
                'new_or_edit'=>"E",
                'reservation_id'=>$reservationInfo->id,
                'old_reservation'=>0,
                'edited_reservation'=>0,
                'amount'=>$accessoriesTotal-$accessoriesTax,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>12,
                'status'=>"na",
                'created_by'=>$createdByUser->id
            ]);

            FinancialInfo::create([
                'booking_id'=>$reservationInfo->booking_id,
                'user_id'=>$reservationInfo->user_id,
                'debit_or_credit'=>"D",
                'new_or_edit'=>"E",
                'reservation_id'=>$reservationInfo->id,
                'old_reservation'=>0,
                'edited_reservation'=>0,
                'amount'=>$accessoriesTax,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>13,
                'status'=>"na",
                'created_by'=>$createdByUser->id
            ]);
        }
            
        
        foreach($request->invoices as $invoice)
        {
            
            if(!in_array($invoice['invoice_nature'],$accessoriesList))
            {
                $invoiceTotal = $invoice['invoice_amount'];
                $invoiceTaxable  = round(($taxableTotalInvoiceAmount/$totalInvoiceAmount)*$invoiceTotal,2);
                $invoiceTax = round($invoiceTaxable*($taxDetails->amount/100),2);
                
                $invoiceType = InvoiceType::where('type',$invoice['invoice_nature'])->where('display',1)->first();
                if(!(!$invoiceType))
                {
                    $inNature = PaymentNature::where('invoice_type',$invoiceType->id)->where('tax',0)->where('reversal',0)->first();
                    $taxNature = PaymentNature::where('invoice_type',$invoiceType->id)->where('tax',1)->where('reversal',0)->first();
                
                    FinancialInfo::create([
                        'booking_id'=>$reservationInfo->booking_id,
                        'user_id'=>$reservationInfo->user_id,
                        'debit_or_credit'=>"D",
                        'new_or_edit'=>"E",
                        'reservation_id'=>$reservationInfo->id,
                        'old_reservation'=>0,
                        'edited_reservation'=>0,
                        'amount'=>$invoiceTotal,
                        'payment_method'=>0,
                        'payment_id'=>"na",
                        'payment_nature'=>$inNature->id,
                        'status'=>"na",
                        'created_by'=>$createdByUser->id
                    ]);

                    FinancialInfo::create([
                        'booking_id'=>$reservationInfo->booking_id,
                        'user_id'=>$reservationInfo->user_id,
                        'debit_or_credit'=>"D",
                        'new_or_edit'=>"E",
                        'reservation_id'=>$reservationInfo->id,
                        'old_reservation'=>0,
                        'edited_reservation'=>0,
                        'amount'=>$invoiceTax,
                        'payment_method'=>0,
                        'payment_id'=>"na",
                        'payment_nature'=>$taxNature->id,
                        'status'=>"na",
                        'created_by'=>$createdByUser->id
                    ]);
                }
            }
        }
        
        if($request->delivery_or_return == "D")
            $message = "Delivery successful";
        else
            $message = "Return successful";
        
        return $this->respondWithSuccess(['status'=>true, 'message'=>$message]); 
        
    }
    
    
}
