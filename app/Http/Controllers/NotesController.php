<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class NotesController extends AppController{

  

  public function update($db_table,$column,$id,Request $request){
    $user = Auth::user();  
    DB::table($db_table)->where('id',$id)->update([$column=>$request->note,'updated_by'=>$user->email]);    
    return \Redirect::back()->with('message','Note Saved');
  }

  
}
