<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Partner;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\City;
use App\Models\Area;
use App\Models\User;



class PartnerController extends AppController
{
    public function index(){
    	if(Auth::check())
    		return redirect('partner/dashboard');
    	else
    		return redirect('partner/login');
    }

    public function login(){
    	return view('partner.login');
    }

    public function dashboard(){
    	return view('partner.dashboard');
    }
    
    public function show(){
        
        
        $partners = Partner::all();

        
        $makes = BikeMake::where('status', 1)->get();
        foreach ($makes as $make) {
            $models = BikeModel::where('status', 1)->where('bike_make_id', $make['id'])->get();
            $make['models'] = $models;
        }

        $cities = City::where('status', 1)->get();
        foreach ($cities as $city) {
            $areas = Area::where('status', 1)->where('city_id', $city['id'])->get();
            $city['areas'] = $areas;
        }
        
        


        foreach ($partners as $partner) {
            
            $modelNames = [];
            $areaNames = [];
            
            
            $user = User::where('id',$partner->user_id)->first();
            $partner['fname'] = $user->first_name;
            $partner['lname'] = $user->last_name;
            $partner['email'] = $user->email;
            $partner['mob'] = $user->mobile_num;
            

            

            $partner['modelNames'] = '';
            $partner['areaNames'] = '';
            if (!empty($partner['model_id'])) {
                $modelIds = explode(',', $partner['model_id']);
                foreach ($modelIds as $modelId) {
                    $model = BikeModel::where('id', $modelId)->first();
                    array_push($modelNames, $model->bike_model);
                }
                $partner['modelNames'] = implode(', ', $modelNames);
            }

            if (!empty($partner['area_id'])) {
                $areaIds = explode(',', $partner['area_id']);
                foreach ($areaIds as $areaId) {
                    $area = Area::where('id', $areaId)->first();
                    array_push($areaNames, $area->area);
                }
                $partner['areaNames'] = implode(', ', $areaNames);
            }
            
        
      
        }
                
        
        return view('admin.partners.index', compact('partners', 'makes', 'cities'));
        
    }
    
    public function store(Request $request){
        

        $modelIds = null;
        $areaIds = null;

        


        if ($request->has('model_ids')) {
            $modelIds = implode(',', $request->model_ids);
        }

        if ($request->has('area_ids')) {
            $areaIds = implode(',', $request->area_ids);
        }
          if ($request->has('startDate')) {
            $startDate = $request->startDate;
        }
        
        $user = User::where('email',$request->customer)->first();
        if(!$user)
        {
            return redirect('/admin/partner')->withErrors(['User not found for the entered email  - '.$request->customer]);
        }
        $userId = $user->id;

        
     
        $partner = Partner::create(['area_id' => $areaIds, 'model_id' => $modelIds, 'user_id'=>$userId,'date_added'=>$startDate]);

        if ($partner == true)
            return redirect('/admin/partner')->with(['msg', 'Partner created successfully']);
        else
            return redirect('/admin/partner')->withErrors(['Sorry something went wrong']);
    }
    
    
    public function delete($id){
        
            Partner::destroy($id);
            return redirect('/admin/partner');

    }
    
    
    public function update(Request $request, $id)
    {
        $existing = Partner::where('id', $id)->first();
        
        if (!$existing)
            return redirect('/admin/partner')->withErrors('Partner not found');

       
        
        $areaIds = $request->area_ids;
        $modelIds = $request->model_ids;
        

        if ($request->has('model_ids')) {
            $modelIds = implode(',', $request->model_ids);
        }

        if ($request->has('area_ids')) {
            $areaIds = implode(',', $request->area_ids);
        }
       
       

        $partner = Partner::where('id', $id)->update(['area_id' => $areaIds, 'model_id' => $modelIds]);

        if ($partner == true)
            return redirect('/admin/partner')->with(['msg', 'Partner updated successfully']);
        else
            return redirect('/admin/partner')->withErrors(['Sorry something went wrong']);
    }

    
}
