<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\EmailSubscribe;

class EmailSubscribeController extends AppController
{
    
    public function index(){

    	$subscribeList = EmailSubscribe::all();
    	//$mss="test";
    	return view('admin.emailSubscribe.index',compact('subscribeList'));
    }
}
