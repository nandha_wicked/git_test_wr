<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Dingo\Api\Routing\Helpers;

use Auth;
use DB;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\User;
use App\Models\Booking;
use App\Models\Available;
use App\Models\Capacity;
use App\Models\Occupancy;
use App\Models\Service;
use App\Models\Parameters;
use App\Models\ReservationInfo;
use App\Models\BookingHeader;
use App\Models\DeliveryInfo;
use App\Models\FinancialInfo;
use App\Models\NotesInfo;
use App\Models\PricingInfo;
use App\Models\ReturnInfo;
use App\Models\OneWayRentalBooking;


use Carbon\Carbon;
use Excel;

class ReportController extends AppController
{
    
    public function index(Request $request)
    {
        return view('admin.reportPage.index');
    }
    
    public function export(Request $request)
    {
        ini_set('memory_limit', '-1');
        
        $rules = array(
          'date_mode' => 'required',
          'start_date' => 'required',
          'end_date' => 'required',
           
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/report_page')->withErrors($validator->errors());
        }
        
        $reportArray = [];
        
        $startDateCarbon = Carbon::createFromFormat('m/d/Y H:i:s',$request->start_date." 00:00:00");
        $endDateCarbon = Carbon::createFromFormat('m/d/Y H:i:s',$request->end_date." 00:00:00");
        
        if($startDateCarbon->gt($endDateCarbon)){
          return redirect('/admin/report_page')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
        }
        
        $endDateCarbon->addDay();
        
        $date_mode = $request->date_mode;
        
        $userAreaIds = Auth::user()->getUserZonesForReport();
        
        if($date_mode == "created_at")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('created_at','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('created_at','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('created_at','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        elseif($date_mode == "end_date")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('end_datetime','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('end_datetime','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        elseif($date_mode == "begin_date")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('start_datetime','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('start_datetime','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('start_datetime','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        elseif($date_mode == "revenue_end_date")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('end_datetime','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('end_datetime','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        elseif($date_mode == "revenue_begin_date")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('start_datetime','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('start_datetime','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('start_datetime','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        elseif($date_mode == "end_date_collection")
        {
            $bookings = ReservationInfo::where('bike_or_accessory','B')->where('end_datetime','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDateCarbon->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->whereIn('area_id',$userAreaIds)->where('deleted',0)->orderBy('end_datetime','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }
        if($request->deleted_booking == "yes")
        {
        $bookings = ReservationInfo::where('bike_or_accessory','B')->where('deleted_at','>=',$startDateCarbon->format('Y-m-d H:i:s'))->where('deleted_at','<',$endDateCarbon->format('Y-m-d H:i:s'))->where('deleted',1)->orderBy('created_at','asc')->with(['user','pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        }

        if(!$bookings)
        {
            return redirect('/admin/report_page')->withErrors(["dates"=>"No Bookings found for the dates."]);
        }

        $notesColumnArray = [];
        $notesIndex = -1;
        $notesIndexArray = [];

        $taxDetails = json_decode(Parameters::getParameter('tax_details'))[0];
        
        foreach($bookings as $booking)
        {            
            $amountDetails = $booking->bookingHeader->totalAmountPaid();
            
            $reportArrayEl = [
                'ID'=>$booking->bookingHeader->ref_id,
                'User Name'=>$booking->user->first_name." ".$booking->user->last_name,
                'User_id' => $booking->user_id,
                'Start Date'=>Carbon::parse($booking->start_datetime)->format('m-d-Y H:i'),
                'End Date'=>Carbon::parse($booking->end_datetime)->format('m-d-Y H:i'), 
                'Created At'=>Carbon::parse($booking->created_at)->format('m-d-Y H:i'),
                'Updated At'=>Carbon::parse($booking->updated_at)->format('m-d-Y H:i'),
                'Model'=>$booking->model->bike_model, 
                'City'=> $booking->area->city->city,
                'Area'=>$booking->area->area,
                'Basic Rental Amount'=>$booking->pricingInfo->price_after_discount,
                'Promotional Wallet Used (Addl Discount)'=>$amountDetails["promo"],
                'Net Basic Rental Amount'=>$booking->pricingInfo->taxable_price,
                'GST @ 18%'=>$booking->pricingInfo->tax_amount,
                'Net Amount Payable'=>($booking->pricingInfo->taxable_price+$booking->pricingInfo->tax_amount),
                'Non Promotional Wallet Balance'=>$amountDetails["non_promo"],
                'Cash, Card and Payment Gateway'=>$amountDetails["cash_and_online"],
                'Cash'=>$amountDetails["cash"],
                'Card'=>$amountDetails["card"],
                'Online'=>$amountDetails["online"],
                'Promo Code'=>$booking->pricingInfo->promo_code
            ];

            $legacyBooking = $booking->legacyBooking;
            if($request->deleted_booking == "yes")
            {
                $deleted_Data = ["Deleted_at"=>$booking->deleted_at];
                $reportArrayEl = array_merge($reportArrayEl, $deleted_Data);
                $legacyBooking = DB::table('deleted_bookings')->where('id',$booking->id)->first();
            }
            if(!(!$legacyBooking))
            {
                
                if($request->erp_delivery == "yes")
                { 
                    $erpDeliveryArray = [
                        "Delivered By"=>$legacyBooking->getDeliveredByEmail(),
                        "Delivered Bike"=>$legacyBooking->getBikeNumber(),
                        "Delivery Notes"=>$legacyBooking->delivery_notes, 
                        "Accesories"=>strip_tags($legacyBooking->getAccessoryLabel()),
                        "Delivery Extra Cash"=>$legacyBooking->extra_money, 
                        "Delivery Collection Mode"=>$legacyBooking->collection_mode
                    ];

                    $reportArrayEl = array_merge($reportArrayEl, $erpDeliveryArray);


                }

                if($request->erp_return == "yes")
                {

                    $erpReturnArray = [
                        "Returned By"=>$legacyBooking->getReturnedByEmail(),
                        "Return Notes"=>$legacyBooking->delivery_notes, 
                        "Return Extra Cash"=>$legacyBooking->extra_money_return, 
                        "Return Collection Mode"=>$legacyBooking->collection_mode_return,
                        "KM Limit"=>$legacyBooking->KMlimit,
                        "Start Odo"=>$legacyBooking->start_odo, 
                        "End Odo"=>$legacyBooking->end_odo, 
                        "KM Done"=>($legacyBooking->end_odo - $legacyBooking->start_odo ),
                        "Bike Returned Without Problems"=>$legacyBooking->returned_with_problems?"No":"Yes",
                        "Accesories Returned Without Problems"=>$legacyBooking->returned_with_problems_accessory?"No":"Yes"
                    ];

                    $reportArrayEl = array_merge($reportArrayEl, $erpReturnArray);


                }

                if($request->notes == "yes")
                {
                    $notes = ["Customer Instructions"=>$legacyBooking->note];
                    $reportArrayEl = array_merge($reportArrayEl, $notes);
                }

                if($request->user_details == "yes")
                {
                    $user = $legacyBooking->getUserThis();

                    $userDetails = ["User Email"=>$user->email,"User Phone"=>$user->mobile_num];

                    $reportArrayEl = array_merge($reportArrayEl, $userDetails);
                }

                if($request->weekday_weekend_hours == "yes")
                {
                    $weekdayWeekendHours = ["Week Day Hours"=>$legacyBooking->weekday_hours,"Week End Hours"=>$legacyBooking->weekend_hours];
                    $reportArrayEl = array_merge($reportArrayEl, $weekdayWeekendHours);
                }


                if($request->notes == "yes")
                {
                    $notes = ["Customer Instructions"=>$legacyBooking->note];
                    $reportArrayEl = array_merge($reportArrayEl, $notes);
                }

                if($request->payment_info == "yes")
                {

                    $notes = ["Payment Method"=>$legacyBooking->payment_method,"Payment Gateway ID"=>implode(',', array_unique(explode(',', $legacyBooking->pg_txn_id))),"Order Id"=>$legacyBooking->order_id,"Finance_Calc1"=>number_format($booking->pricingInfo->price_after_discount-$amountDetails["promo"]+$booking->pricingInfo->tax_amount-($booking->pricingInfo->taxable_price+$booking->pricingInfo->tax_amount),2),
                "Finance_Calc2"=>number_format((($booking->pricingInfo->taxable_price+$booking->pricingInfo->tax_amount)-($amountDetails["non_promo"]))-$amountDetails["cash_and_online"],2)];
                    $reportArrayEl = array_merge($reportArrayEl, $notes);
                }
                
            }
            
            array_push($reportArray,$reportArrayEl); 
        }

        if($request->erp_delivery == "yes")
        {

            array_push($notesIndexArray,11);
            $notesIndex = $notesIndex+15;

        }

        if($request->erp_return == "yes")
        {

            array_push($notesIndexArray,$notesIndex+2);
            $notesIndex = $notesIndex+8;

        }

        if($request->notes == "yes")
        {
            array_push($notesIndexArray,$notesIndex+1);
            $notesIndex = $notesIndex+1;
        }


        $columnIndex = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ"];

        foreach($notesIndexArray as $notesIndexArrayEl)
        {
            $notesColumnArray = array_merge($notesColumnArray, [$columnIndex[$notesIndexArrayEl]=>50]);
        }

        

        Excel::create('Bookings', function($excel) use($reportArray,$notesColumnArray) {
                $excel->sheet('All Areas', function($sheet) use($reportArray,$notesColumnArray) {
                    $sheet->fromModel($reportArray);
                    $sheet->setAutoSize(true);
                    $sheet->setWidth($notesColumnArray);
                });
        })->export('xls');
        
        return redirect('/admin/report_page');
       
        
    }
    
    public static function getOccupancyReportForAreaModel($area_id,$model_id,$startDateCarbon,$endDateCarbon)
    {
        for($date = Carbon::parse($startDateCarbon->format('Y-m-d')); $date->lte($endDateCarbon); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
                
        $capacityObjs = Capacity::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        if($capacityObjs->count()==0)
        {
            return "nill_capacity";
        }
        
        $capacityObjsByDate = $capacityObjs->keyBy('date');
                
        $capacityHourArray = [];
        
        foreach($dates as $date)
        {
            $capacityHourStr = $capacityObjsByDate[$date]['hour_string'];
            $capacityHourStrArray = explode(',',$capacityHourStr);
            $capacityHourArray = array_merge($capacityHourArray,$capacityHourStrArray);
        }
        
        $capacitySum = 0;
        
        foreach($capacityHourArray as $capacityHourArrayEl)
        {
            $capacitySum+=$capacityHourArrayEl;
        }
        
        $hourCount = count($capacityHourArray);
        
        $capacityAverage = $capacitySum/$hourCount;
        
        
        if($capacityAverage == 0)
        {
            return "nill_capacity";
        }
        
        
        
        $serviceObjs = Service::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        
        $serviceObjsByDate = $serviceObjs->keyBy('date');
                
        $serviceHourArray = [];
        
        foreach($dates as $date)
        {
            $serviceHourStr = $serviceObjsByDate[$date]['hour_string'];
            $serviceHourStrArray = explode(',',$serviceHourStr);
            $serviceHourArray = array_merge($serviceHourArray,$serviceHourStrArray);
        }
                    
        
        
        $occupancyObjs = Occupancy::where('area_id',$area_id)->where('model_id',$model_id)->whereIn('date',$dates)->orderBy('date','asc')->get();
        
        
        $occupancyObjsByDate = $occupancyObjs->keyBy('date');
                
        $occupancyHourArray = [];
        
        foreach($dates as $date)
        {
            $occupancyHourStr = $occupancyObjsByDate[$date]['hour_string'];
            $occupancyHourStrArray = explode(',',$occupancyHourStr);
            $occupancyHourArray = array_merge($occupancyHourArray,$occupancyHourStrArray);
        }
        
        
        
        $dateIndex = 0;
        $hourIndex = 0;
        
        $occupancyTrue = 0;
        $occupancyGeneral = 0;
        
        $allDaysOccupancyTrue = 0;
        $allDaysOccupancyGeneral = 0;
        
        $weekDayOccupancyTrue = 0;
        $weekDayOccupancyGeneral = 0;
        
        $weekEndOccupancyTrue = 0;
        $weekEndOccupancyGeneral = 0;
        
        $allDaysBucket = ["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $weekdayBucket = ["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $weekendBucket = ["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket = [];
        $dayOfTheWeekBucket[1]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[2]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[3]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[4]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[5]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[6]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        $dayOfTheWeekBucket[0]=["capacity_average_true"=>[],"capacity_average_general"=>[],"occupancy_average"=>[],"nonrevenue_average"=>[],"booking_amount"=>[],"booking_amount_average_true"=>[],"booking_amount_average_general"=>[]];
        
        $nonRevenueIds = explode(',',env('NONREVENUEIDS'));
        
        $bookings = Booking::where('area_id',$area_id)->where('model_id',$model_id)->whereNotIn('user_id',$nonRevenueIds)->where('start_datetime','<',$endDateCarbon->copy()->addDay()->format('Y-m-d 00:00:00'))->where('end_datetime','>',$startDateCarbon->format('Y-m-d 00:00:00'))->get();
        foreach($bookings as $booking)
        {
            $booking['startDateTimeCarbon'] = Carbon::parse($booking->start_datetime);
            $booking['endDateTimeCarbon'] = Carbon::parse($booking->end_datetime);
            $booking['diff_in_hours'] = $booking['startDateTimeCarbon']->diffInHours($booking['endDateTimeCarbon']);
        }
        
        
        foreach($dates as $date)
        {
            $capacityGeneralAverage = 0;
            $capacityTrueAverage = 0;
            $occupancyAverage = 0;
            $nonrevenueAverage = 0;
            
            $dateCarbon = Carbon::parse($date);
            for($i=0;$i<24;$i++)
            {
                $capacityGeneralAverage += $capacityHourArray[$i+$hourIndex];
                $capacityTrueAverage +=($capacityHourArray[$i+$hourIndex] - $serviceHourArray[$i+$hourIndex]);
                $occupancyAverage += $occupancyHourArray[$i+$hourIndex];
                $nonrevenueAverage += $serviceHourArray[$i+$hourIndex];
            }
            
            $hourIndex +=24;
            
            $capacityGeneralAverage = $capacityGeneralAverage/24;
            $capacityTrueAverage = $capacityTrueAverage/24;
            $occupancyAverage = $occupancyAverage/24;
            $nonrevenueAverage = $nonrevenueAverage/24;
            
            if($capacityGeneralAverage>0)
            {
                array_push($allDaysBucket["capacity_average_true"],$capacityTrueAverage);

                array_push($allDaysBucket["capacity_average_general"],$capacityGeneralAverage);

                array_push($allDaysBucket["occupancy_average"],$occupancyAverage);
                
                array_push($allDaysBucket["nonrevenue_average"],$nonrevenueAverage);


                if($dateCarbon->isWeekDay())
                {
                    array_push($weekdayBucket["capacity_average_true"],$capacityTrueAverage);

                    array_push($weekdayBucket["capacity_average_general"],$capacityGeneralAverage);

                    array_push($weekdayBucket["occupancy_average"],$occupancyAverage);
                
                    array_push($weekdayBucket["nonrevenue_average"],$nonrevenueAverage);

                }
                if($dateCarbon->isWeekEnd())
                {
                    array_push($weekendBucket["capacity_average_true"],$capacityTrueAverage);

                    array_push($weekendBucket["capacity_average_general"],$capacityGeneralAverage);

                    array_push($weekendBucket["occupancy_average"],$occupancyAverage);
                
                    array_push($weekendBucket["nonrevenue_average"],$nonrevenueAverage);

                }

                $day = $dateCarbon->dayOfWeek;

                $nextDateCarbon = $dateCarbon->copy()->addDay();

                array_push($dayOfTheWeekBucket[$day]["capacity_average_true"],$capacityTrueAverage);

                array_push($dayOfTheWeekBucket[$day]["capacity_average_general"],$capacityGeneralAverage);

                array_push($dayOfTheWeekBucket[$day]["occupancy_average"],$occupancyAverage);
                
                array_push($dayOfTheWeekBucket[$day]["nonrevenue_average"],$nonrevenueAverage);
                
                

                $totalRevenueShareForTheDay = 0;

                foreach($bookings as $booking)
                {
                    if($booking['startDateTimeCarbon']->lte($dateCarbon)&&$booking['endDateTimeCarbon']->gte($nextDateCarbon))
                    {
                        $hourShare = 24;
                    }
                    elseif($booking['startDateTimeCarbon']->gte($dateCarbon)&&$booking['endDateTimeCarbon']->lte($nextDateCarbon))
                    {
                        $hourShare = $booking['diff_in_hours'];
                    }
                    elseif($booking['endDateTimeCarbon']->gte($dateCarbon)&&$booking['endDateTimeCarbon']->lte($nextDateCarbon))
                    {
                        $hourShare = $booking['endDateTimeCarbon']->hour;
                    }
                    elseif($booking['startDateTimeCarbon']->gte($dateCarbon)&&$booking['startDateTimeCarbon']->lte($nextDateCarbon))
                    {
                        $hourShare = 24 - $booking['startDateTimeCarbon']->hour;
                    }
                    else
                    {
                        continue;
                    }
                    $revenueShare = $booking->total_price/$booking['diff_in_hours']*$hourShare;

                    $totalRevenueShareForTheDay += $revenueShare;
                    

                }
                

                if($capacityTrueAverage == 0)
                {
                    $bookingRevenuePerDayPerBikeTrue = 0;
                }
                else
                {
                    $bookingRevenuePerDayPerBikeTrue = $totalRevenueShareForTheDay/$capacityTrueAverage;
                }

                $bookingRevenuePerDayPerBikeGeneral = $totalRevenueShareForTheDay/$capacityGeneralAverage;

                array_push($allDaysBucket["booking_amount"],$totalRevenueShareForTheDay);
                array_push($allDaysBucket["booking_amount_average_general"],$bookingRevenuePerDayPerBikeGeneral);
                array_push($allDaysBucket["booking_amount_average_true"],$bookingRevenuePerDayPerBikeTrue);

                if($dateCarbon->isWeekDay())
                {

                    array_push($weekdayBucket["booking_amount"],$totalRevenueShareForTheDay); array_push($weekdayBucket["booking_amount_average_general"],$bookingRevenuePerDayPerBikeGeneral);
                    array_push($weekdayBucket["booking_amount_average_true"],$bookingRevenuePerDayPerBikeTrue);


                }
                if($dateCarbon->isWeekEnd())
                {

                    array_push($weekendBucket["booking_amount"],$totalRevenueShareForTheDay); array_push($weekendBucket["booking_amount_average_general"],$bookingRevenuePerDayPerBikeGeneral);
                    array_push($weekendBucket["booking_amount_average_true"],$bookingRevenuePerDayPerBikeTrue);


                }

                array_push($dayOfTheWeekBucket[$day]["booking_amount"],$totalRevenueShareForTheDay);
                array_push($dayOfTheWeekBucket[$day]["booking_amount_average_general"],$bookingRevenuePerDayPerBikeGeneral);
                array_push($dayOfTheWeekBucket[$day]["booking_amount_average_true"],$bookingRevenuePerDayPerBikeTrue);
            }
            else
            {
                
                array_push($allDaysBucket["capacity_average_true"],0);

                array_push($allDaysBucket["capacity_average_general"],0);

                array_push($allDaysBucket["occupancy_average"],0);
                
                array_push($allDaysBucket["nonrevenue_average"],0);


                if($dateCarbon->isWeekDay())
                {
                    array_push($weekdayBucket["capacity_average_true"],0);

                    array_push($weekdayBucket["capacity_average_general"],0);

                    array_push($weekdayBucket["occupancy_average"],0);
                
                    array_push($weekdayBucket["nonrevenue_average"],0);

                }
                if($dateCarbon->isWeekEnd())
                {
                    array_push($weekendBucket["capacity_average_true"],0);

                    array_push($weekendBucket["capacity_average_general"],0);

                    array_push($weekendBucket["occupancy_average"],0);
                
                    array_push($weekendBucket["nonrevenue_average"],0);

                }

                $day = $dateCarbon->dayOfWeek;

                $nextDateCarbon = $dateCarbon->copy()->addDay();

                array_push($dayOfTheWeekBucket[$day]["capacity_average_true"],0);

                array_push($dayOfTheWeekBucket[$day]["capacity_average_general"],0);

                array_push($dayOfTheWeekBucket[$day]["occupancy_average"],0);
                
                array_push($dayOfTheWeekBucket[$day]["nonrevenue_average"],0);
                
                
                array_push($allDaysBucket["booking_amount"],0);
                array_push($allDaysBucket["booking_amount_average_general"],0);
                array_push($allDaysBucket["booking_amount_average_true"],0);

                if($dateCarbon->isWeekDay())
                {

                    array_push($weekdayBucket["booking_amount"],0); 
                    array_push($weekdayBucket["booking_amount_average_general"],0);
                    array_push($weekdayBucket["booking_amount_average_true"],0);


                }
                if($dateCarbon->isWeekEnd())
                {

                    array_push($weekendBucket["booking_amount"],0); 
                    array_push($weekendBucket["booking_amount_average_general"],0);
                    array_push($weekendBucket["booking_amount_average_true"],0);


                }

                array_push($dayOfTheWeekBucket[$day]["booking_amount"],0);
                array_push($dayOfTheWeekBucket[$day]["booking_amount_average_general"],0);
                array_push($dayOfTheWeekBucket[$day]["booking_amount_average_true"],0);
            }
            
        }
        
            
        $allDaysCapacityGeneralAverage = array_sum($allDaysBucket["capacity_average_general"]) / count($allDaysBucket["capacity_average_general"]);
        $allDaysCapacityTrueAverage = array_sum($allDaysBucket["capacity_average_true"]) / count($allDaysBucket["capacity_average_true"]);
        $allDaysOccupancyAverage = array_sum($allDaysBucket["occupancy_average"]) / count($allDaysBucket["occupancy_average"]);
        $allDaysNonRevenueAverage = array_sum($allDaysBucket["nonrevenue_average"]) / count($allDaysBucket["nonrevenue_average"]);
        $allDaysTotalRevenueShareForTheDay = array_sum($allDaysBucket["booking_amount"]);
        $allDaysRevenuePerDayPerBikeGeneral = array_sum($allDaysBucket["booking_amount_average_general"]) / count($allDaysBucket["booking_amount_average_general"]);
        $allDaysRevenuePerDayPerBikeTrue = array_sum($allDaysBucket["booking_amount_average_true"]) / count($allDaysBucket["booking_amount_average_true"]);
        
        if($allDaysCapacityGeneralAverage == 0)
        {
            $allDaysOccupancyGeneral =110;
        }
        else
        {
            $allDaysOccupancyGeneral = $allDaysOccupancyAverage/$allDaysCapacityGeneralAverage*100;
        }
        if($allDaysCapacityTrueAverage == 0)
        {
            $allDaysOccupancyTrue = 110;
        }
        else
        {
            $allDaysOccupancyTrue  = $allDaysOccupancyAverage/$allDaysCapacityTrueAverage*100;
        }
        

        if(count($weekdayBucket["capacity_average_true"]) == 0)
        {
            $weekdayCapacityGeneralAverage = 0;
            $weekdayCapacityTrueAverage = 0;
            $weekdayOccupancyAverage = 0;
            $weekdayNonRevenueAverage = 0;
            $weekdayTotalRevenueShareForTheDay = 0;
            $weekdayRevenuePerDayPerBikeGeneral = 0;
            $weekdayRevenuePerDayPerBikeTrue = 0;
            
            $weekdayOccupancyGeneral = 0;
            $weekdayOccupancyTrue = 0;
            
        }
        else
        {
            $weekdayCapacityGeneralAverage = array_sum($weekdayBucket["capacity_average_general"]) / count($weekdayBucket["capacity_average_general"]);
            $weekdayCapacityTrueAverage = array_sum($weekdayBucket["capacity_average_true"]) / count($weekdayBucket["capacity_average_true"]);
            $weekdayOccupancyAverage = array_sum($weekdayBucket["occupancy_average"]) / count($weekdayBucket["occupancy_average"]);
            $weekdayNonRevenueAverage = array_sum($weekdayBucket["nonrevenue_average"]) / count($weekdayBucket["nonrevenue_average"]);
            $weekdayTotalRevenueShareForTheDay = array_sum($weekdayBucket["booking_amount"]);
            $weekdayRevenuePerDayPerBikeGeneral = array_sum($weekdayBucket["booking_amount_average_general"]) / count($weekdayBucket["booking_amount_average_general"]);
            $weekdayRevenuePerDayPerBikeTrue = array_sum($weekdayBucket["booking_amount_average_true"]) / count($weekdayBucket["booking_amount_average_true"]);
                
            if($weekdayCapacityGeneralAverage == 0)
            {
                $weekdayOccupancyGeneral =110;
            }
            else
            {
                $weekdayOccupancyGeneral = $weekdayOccupancyAverage/$weekdayCapacityGeneralAverage*100;
            }
            if($weekdayCapacityTrueAverage == 0)
            {
                $weekdayOccupancyTrue = 110;
            }
            else
            {
                $weekdayOccupancyTrue  = $weekdayOccupancyAverage/$weekdayCapacityTrueAverage*100;
            }
        
        }

        if(count($weekendBucket["capacity_average_true"]) == 0)
        {
            $weekendCapacityGeneralAverage = 0;
            $weekendCapacityTrueAverage = 0;
            $weekendOccupancyAverage = 0;
            $weekendNonRevenueAverage = 0;
            $weekendTotalRevenueShareForTheDay = 0;
            $weekendRevenuePerDayPerBikeGeneral = 0;
            $weekendRevenuePerDayPerBikeTrue = 0;
            
            $weekendOccupancyGeneral = 0;
            $weekendOccupancyTrue = 0;
            
        }
        else
        {
            $weekendCapacityGeneralAverage = array_sum($weekendBucket["capacity_average_general"]) / count($weekendBucket["capacity_average_general"]);
            $weekendCapacityTrueAverage = array_sum($weekendBucket["capacity_average_true"]) / count($weekendBucket["capacity_average_true"]);
            $weekendOccupancyAverage = array_sum($weekendBucket["occupancy_average"]) / count($weekendBucket["occupancy_average"]);
            $weekendNonRevenueAverage = array_sum($weekendBucket["nonrevenue_average"]) / count($weekendBucket["nonrevenue_average"]);
            $weekendTotalRevenueShareForTheDay = array_sum($weekendBucket["booking_amount"]);
            $weekendRevenuePerDayPerBikeGeneral = array_sum($weekendBucket["booking_amount_average_general"]) / count($weekendBucket["booking_amount_average_general"]);
            $weekendRevenuePerDayPerBikeTrue = array_sum($weekendBucket["booking_amount_average_true"]) / count($weekendBucket["booking_amount_average_true"]); 
                
            if($weekendCapacityGeneralAverage == 0)
            {
                $weekendOccupancyGeneral =110;
            }
            else
            {
                $weekendOccupancyGeneral = $weekendOccupancyAverage/$weekendCapacityGeneralAverage*100;
            }
            if($weekendCapacityTrueAverage == 0)
            {
                $weekendOccupancyTrue = 110;
            }
            else
            {
                $weekendOccupancyTrue  = $weekendOccupancyAverage/$weekendCapacityTrueAverage*100;
            }
        }
        
        
        
        if(count($dayOfTheWeekBucket[1]["capacity_average_true"]) == 0)
        {
            $mondayCapacityGeneralAverage = 0;
            $mondayCapacityTrueAverage = 0;
            $mondayOccupancyAverage = 0;
            $mondayNonRevenueAverage = 0;
            $mondayTotalRevenueShareForTheDay = 0;
            $mondayRevenuePerDayPerBikeGeneral = 0;
            $mondayRevenuePerDayPerBikeTrue = 0;
                
            $mondayOccupancyGeneral = 0;
            $mondayOccupancyTrue = 0;
            
        }
        else
        {
            $mondayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[1]["capacity_average_general"]) / count($dayOfTheWeekBucket[1]["capacity_average_general"]);
            $mondayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[1]["capacity_average_true"]) / count($dayOfTheWeekBucket[1]["capacity_average_true"]);
            $mondayOccupancyAverage = array_sum($dayOfTheWeekBucket[1]["occupancy_average"]) / count($dayOfTheWeekBucket[1]["occupancy_average"]);
            $mondayNonRevenueAverage = array_sum($dayOfTheWeekBucket[1]["nonrevenue_average"]) / count($dayOfTheWeekBucket[1]["nonrevenue_average"]);
            $mondayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[1]["booking_amount"]);
            $mondayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[1]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[1]["booking_amount_average_general"]);
            $mondayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[1]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[1]["booking_amount_average_true"]); 
            
            if($mondayCapacityGeneralAverage == 0)
            {
                $mondayOccupancyGeneral =110;
            }
            else
            {
                $mondayOccupancyGeneral = $mondayOccupancyAverage/$mondayCapacityGeneralAverage*100;
            }
            if($mondayCapacityTrueAverage == 0)
            {
                $mondayOccupancyTrue = 110;
            }
            else
            {
                $mondayOccupancyTrue  = $mondayOccupancyAverage/$mondayCapacityTrueAverage*100;
            }

        }
        
        
        
        if(count($dayOfTheWeekBucket[2]["capacity_average_true"]) == 0)
        {
            $tuesdayCapacityGeneralAverage = 0;
            $tuesdayCapacityTrueAverage = 0;
            $tuesdayOccupancyAverage = 0;
            $tuesdayNonRevenueAverage = 0;
            $tuesdayTotalRevenueShareForTheDay = 0;
            $tuesdayRevenuePerDayPerBikeGeneral = 0;
            $tuesdayRevenuePerDayPerBikeTrue = 0;
                
            $tuesdayOccupancyGeneral = 0;
            $tuesdayOccupancyTrue = 0;
            
        }
        else
        {
            $tuesdayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[2]["capacity_average_general"]) / count($dayOfTheWeekBucket[2]["capacity_average_general"]);
            $tuesdayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[2]["capacity_average_true"]) / count($dayOfTheWeekBucket[2]["capacity_average_true"]);
            $tuesdayOccupancyAverage = array_sum($dayOfTheWeekBucket[2]["occupancy_average"]) / count($dayOfTheWeekBucket[2]["occupancy_average"]);
            $tuesdayNonRevenueAverage = array_sum($dayOfTheWeekBucket[2]["nonrevenue_average"]) / count($dayOfTheWeekBucket[2]["nonrevenue_average"]);
            $tuesdayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[2]["booking_amount"]);
            $tuesdayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[2]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[2]["booking_amount_average_general"]);
            $tuesdayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[2]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[2]["booking_amount_average_true"]); 
            
            if($tuesdayCapacityGeneralAverage == 0)
            {
                $tuesdayOccupancyGeneral =110;
            }
            else
            {
                $tuesdayOccupancyGeneral = $tuesdayOccupancyAverage/$tuesdayCapacityGeneralAverage*100;
            }
            if($tuesdayCapacityTrueAverage == 0)
            {
                $tuesdayOccupancyTrue = 110;
            }
            else
            {
                $tuesdayOccupancyTrue  = $tuesdayOccupancyAverage/$tuesdayCapacityTrueAverage*100;
            }

        }
        
        
        if(count($dayOfTheWeekBucket[3]["capacity_average_true"]) == 0)
        {
            $wednesdayCapacityGeneralAverage = 0;
            $wednesdayCapacityTrueAverage = 0;
            $wednesdayOccupancyAverage = 0;
            $wednesdayNonRevenueAverage = 0;
            $wednesdayTotalRevenueShareForTheDay = 0;
            $wednesdayRevenuePerDayPerBikeGeneral = 0;
            $wednesdayRevenuePerDayPerBikeTrue = 0;
                
            $wednesdayOccupancyGeneral = 0;
            $wednesdayOccupancyTrue = 0;
            
        }
        else
        {
            $wednesdayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[3]["capacity_average_general"]) / count($dayOfTheWeekBucket[3]["capacity_average_general"]);
            $wednesdayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[3]["capacity_average_true"]) / count($dayOfTheWeekBucket[3]["capacity_average_true"]);
            $wednesdayOccupancyAverage = array_sum($dayOfTheWeekBucket[3]["occupancy_average"]) / count($dayOfTheWeekBucket[3]["occupancy_average"]);
            $wednesdayNonRevenueAverage = array_sum($dayOfTheWeekBucket[3]["nonrevenue_average"]) / count($dayOfTheWeekBucket[3]["nonrevenue_average"]);
            $wednesdayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[3]["booking_amount"]);
            $wednesdayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[3]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[3]["booking_amount_average_general"]);
            $wednesdayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[3]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[3]["booking_amount_average_true"]); 
            
            if($wednesdayCapacityGeneralAverage == 0)
            {
                $wednesdayOccupancyGeneral =110;
            }
            else
            {
                $wednesdayOccupancyGeneral = $wednesdayOccupancyAverage/$wednesdayCapacityGeneralAverage*100;
            }
            if($wednesdayCapacityTrueAverage == 0)
            {
                $wednesdayOccupancyTrue = 110;
            }
            else
            {
                $wednesdayOccupancyTrue  = $wednesdayOccupancyAverage/$wednesdayCapacityTrueAverage*100;
            }

        }
        
        if(count($dayOfTheWeekBucket[4]["capacity_average_true"]) == 0)
        {
            $thursdayCapacityGeneralAverage = 0;
            $thursdayCapacityTrueAverage = 0;
            $thursdayOccupancyAverage = 0;
            $thursdayNonRevenueAverage = 0;
            $thursdayTotalRevenueShareForTheDay = 0;
            $thursdayRevenuePerDayPerBikeGeneral = 0;
            $thursdayRevenuePerDayPerBikeTrue = 0;
                
            $thursdayOccupancyGeneral = 0;
            $thursdayOccupancyTrue = 0;
            
        }
        else
        {
            $thursdayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[4]["capacity_average_general"]) / count($dayOfTheWeekBucket[4]["capacity_average_general"]);
            $thursdayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[4]["capacity_average_true"]) / count($dayOfTheWeekBucket[4]["capacity_average_true"]);
            $thursdayOccupancyAverage = array_sum($dayOfTheWeekBucket[4]["occupancy_average"]) / count($dayOfTheWeekBucket[4]["occupancy_average"]);
            $thursdayNonRevenueAverage = array_sum($dayOfTheWeekBucket[4]["nonrevenue_average"]) / count($dayOfTheWeekBucket[4]["nonrevenue_average"]);
            $thursdayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[4]["booking_amount"]);
            $thursdayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[4]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[4]["booking_amount_average_general"]);
            $thursdayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[4]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[4]["booking_amount_average_true"]); 
            
            if($thursdayCapacityGeneralAverage == 0)
            {
                $thursdayOccupancyGeneral =110;
            }
            else
            {
                $thursdayOccupancyGeneral = $thursdayOccupancyAverage/$thursdayCapacityGeneralAverage*100;
            }
            if($thursdayCapacityTrueAverage == 0)
            {
                $thursdayOccupancyTrue = 110;
            }
            else
            {
                $thursdayOccupancyTrue  = $thursdayOccupancyAverage/$thursdayCapacityTrueAverage*100;
            }

        }
        
        if(count($dayOfTheWeekBucket[5]["capacity_average_true"]) == 0)
        {
            $fridayCapacityGeneralAverage = 0;
            $fridayCapacityTrueAverage = 0;
            $fridayOccupancyAverage = 0;
            $fridayNonRevenueAverage = 0;
            $fridayTotalRevenueShareForTheDay = 0;
            $fridayRevenuePerDayPerBikeGeneral = 0;
            $fridayRevenuePerDayPerBikeTrue = 0;
                
            $fridayOccupancyGeneral = 0;
            $fridayOccupancyTrue = 0;
            
        }
        else
        {
            $fridayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[5]["capacity_average_general"]) / count($dayOfTheWeekBucket[5]["capacity_average_general"]);
            $fridayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[5]["capacity_average_true"]) / count($dayOfTheWeekBucket[5]["capacity_average_true"]);
            $fridayOccupancyAverage = array_sum($dayOfTheWeekBucket[5]["occupancy_average"]) / count($dayOfTheWeekBucket[5]["occupancy_average"]);
            $fridayNonRevenueAverage = array_sum($dayOfTheWeekBucket[5]["nonrevenue_average"]) / count($dayOfTheWeekBucket[5]["nonrevenue_average"]);
            $fridayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[5]["booking_amount"]);
            $fridayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[5]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[5]["booking_amount_average_general"]);
            $fridayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[5]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[5]["booking_amount_average_true"]); 
            
            if($fridayCapacityGeneralAverage == 0)
            {
                $fridayOccupancyGeneral =110;
            }
            else
            {
                $fridayOccupancyGeneral = $fridayOccupancyAverage/$fridayCapacityGeneralAverage*100;
            }
            if($fridayCapacityTrueAverage == 0)
            {
                $fridayOccupancyTrue = 110;
            }
            else
            {
                $fridayOccupancyTrue  = $fridayOccupancyAverage/$fridayCapacityTrueAverage*100;
            }

        }
        
        
        if(count($dayOfTheWeekBucket[6]["capacity_average_true"]) == 0)
        {
            $saturdayCapacityGeneralAverage = 0;
            $saturdayCapacityTrueAverage = 0;
            $saturdayOccupancyAverage = 0;
            $saturdayNonRevenueAverage = 0;
            $saturdayTotalRevenueShareForTheDay = 0;
            $saturdayRevenuePerDayPerBikeGeneral = 0;
            $saturdayRevenuePerDayPerBikeTrue = 0;
                
            $saturdayOccupancyGeneral = 0;
            $saturdayOccupancyTrue = 0;
            
        }
        else
        {
            $saturdayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[6]["capacity_average_general"]) / count($dayOfTheWeekBucket[6]["capacity_average_general"]);
            $saturdayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[6]["capacity_average_true"]) / count($dayOfTheWeekBucket[6]["capacity_average_true"]);
            $saturdayOccupancyAverage = array_sum($dayOfTheWeekBucket[6]["occupancy_average"]) / count($dayOfTheWeekBucket[6]["occupancy_average"]);
            $saturdayNonRevenueAverage = array_sum($dayOfTheWeekBucket[6]["nonrevenue_average"]) / count($dayOfTheWeekBucket[6]["nonrevenue_average"]);
            $saturdayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[6]["booking_amount"]);
            $saturdayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[6]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[6]["booking_amount_average_general"]);
            $saturdayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[6]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[6]["booking_amount_average_true"]); 
            
            if($saturdayCapacityGeneralAverage == 0)
            {
                $saturdayOccupancyGeneral =110;
            }
            else
            {
                $saturdayOccupancyGeneral = $saturdayOccupancyAverage/$saturdayCapacityGeneralAverage*100;
            }
            if($saturdayCapacityTrueAverage == 0)
            {
                $saturdayOccupancyTrue = 110;
            }
            else
            {
                $saturdayOccupancyTrue  = $saturdayOccupancyAverage/$saturdayCapacityTrueAverage*100;
            }

        }
        
        
        if(count($dayOfTheWeekBucket[0]["capacity_average_true"]) == 0)
        {
            $sundayCapacityGeneralAverage = 0;
            $sundayCapacityTrueAverage = 0;
            $sundayOccupancyAverage = 0;
            $sundayNonRevenueAverage = 0;
            $sundayTotalRevenueShareForTheDay = 0;
            $sundayRevenuePerDayPerBikeGeneral = 0;
            $sundayRevenuePerDayPerBikeTrue = 0;
                
            $sundayOccupancyGeneral = 0;
            $sundayOccupancyTrue = 0;
            
        }
        else
        {
            $sundayCapacityGeneralAverage = array_sum($dayOfTheWeekBucket[0]["capacity_average_general"]) / count($dayOfTheWeekBucket[0]["capacity_average_general"]);
            $sundayCapacityTrueAverage = array_sum($dayOfTheWeekBucket[0]["capacity_average_true"]) / count($dayOfTheWeekBucket[0]["capacity_average_true"]);
            $sundayOccupancyAverage = array_sum($dayOfTheWeekBucket[0]["occupancy_average"]) / count($dayOfTheWeekBucket[0]["occupancy_average"]);
            $sundayNonRevenueAverage = array_sum($dayOfTheWeekBucket[0]["nonrevenue_average"]) / count($dayOfTheWeekBucket[0]["nonrevenue_average"]);
            $sundayTotalRevenueShareForTheDay = array_sum($dayOfTheWeekBucket[0]["booking_amount"]);
            $sundayRevenuePerDayPerBikeGeneral = array_sum($dayOfTheWeekBucket[0]["booking_amount_average_general"]) / count($dayOfTheWeekBucket[0]["booking_amount_average_general"]);
            $sundayRevenuePerDayPerBikeTrue = array_sum($dayOfTheWeekBucket[0]["booking_amount_average_true"]) / count($dayOfTheWeekBucket[0]["booking_amount_average_true"]); 
            
            if($sundayCapacityGeneralAverage == 0)
            {
                $sundayOccupancyGeneral =110;
            }
            else
            {
                $sundayOccupancyGeneral = $sundayOccupancyAverage/$sundayCapacityGeneralAverage*100;
            }
            if($sundayCapacityTrueAverage == 0)
            {
                $sundayOccupancyTrue = 110;
            }
            else
            {
                $sundayOccupancyTrue  = $sundayOccupancyAverage/$sundayCapacityTrueAverage*100;
            }

        }
        
        
        
    

        $returnObj = 
            
            [
                "allDays"=> [
                                "capacity_average_general"=>($allDaysCapacityGeneralAverage),
                                "capacity_average_true"=>($allDaysCapacityTrueAverage),
                                "occupancy_average"=>($allDaysOccupancyAverage),
                                "nonrevenue_average"=>($allDaysNonRevenueAverage),
                                "booking_amount"=>round($allDaysTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($allDaysRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($allDaysRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($allDaysOccupancyGeneral),
                                "occupancy_percent_true"=>round($allDaysOccupancyTrue)                        
                            ],
                "weekday"=> [
                                "capacity_average_general"=>($weekdayCapacityGeneralAverage),
                                "capacity_average_true"=>($weekdayCapacityTrueAverage),
                                "occupancy_average"=>($weekdayOccupancyAverage),
                                "nonrevenue_average"=>($weekdayNonRevenueAverage),
                                "booking_amount"=>round($weekdayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($weekdayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($weekdayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($weekdayOccupancyGeneral),
                                "occupancy_percent_true"=>round($weekdayOccupancyTrue)                        
                            ],
                "weekend"=> [
                                "capacity_average_general"=>($weekendCapacityGeneralAverage),
                                "capacity_average_true"=>($weekendCapacityTrueAverage),
                                "occupancy_average"=>($weekendOccupancyAverage),
                                "nonrevenue_average"=>($weekendNonRevenueAverage),
                                "booking_amount"=>round($weekendTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($weekendRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($weekendRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($weekendOccupancyGeneral),
                                "occupancy_percent_true"=>round($weekendOccupancyTrue)                        
                            ],
                "monday"=> [
                                "capacity_average_general"=>($mondayCapacityGeneralAverage),
                                "capacity_average_true"=>($mondayCapacityTrueAverage),
                                "occupancy_average"=>($mondayOccupancyAverage),
                                "nonrevenue_average"=>($mondayNonRevenueAverage),
                                "booking_amount"=>round($mondayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($mondayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($mondayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($mondayOccupancyGeneral),
                                "occupancy_percent_true"=>round($mondayOccupancyTrue)                        
                            ],
                "tuesday"=> [
                                "capacity_average_general"=>($tuesdayCapacityGeneralAverage),
                                "capacity_average_true"=>($tuesdayCapacityTrueAverage),
                                "occupancy_average"=>($tuesdayOccupancyAverage),
                                "nonrevenue_average"=>($tuesdayNonRevenueAverage),
                                "booking_amount"=>round($tuesdayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($tuesdayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($tuesdayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($tuesdayOccupancyGeneral),
                                "occupancy_percent_true"=>round($tuesdayOccupancyTrue)                        
                            ],
                "wednesday"=> [
                                "capacity_average_general"=>($wednesdayCapacityGeneralAverage),
                                "capacity_average_true"=>($wednesdayCapacityTrueAverage),
                                "occupancy_average"=>($wednesdayOccupancyAverage),
                                "nonrevenue_average"=>($wednesdayNonRevenueAverage),
                                "booking_amount"=>round($wednesdayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($wednesdayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($wednesdayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($wednesdayOccupancyGeneral),
                                "occupancy_percent_true"=>round($wednesdayOccupancyTrue)                        
                            ],
                "thursday"=> [
                                "capacity_average_general"=>($thursdayCapacityGeneralAverage),
                                "capacity_average_true"=>($thursdayCapacityTrueAverage),
                                "occupancy_average"=>($thursdayOccupancyAverage),
                                "nonrevenue_average"=>($thursdayNonRevenueAverage),
                                "booking_amount"=>round($thursdayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($thursdayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($thursdayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($thursdayOccupancyGeneral),
                                "occupancy_percent_true"=>round($thursdayOccupancyTrue)                        
                            ],
                "friday"=> [
                                "capacity_average_general"=>($fridayCapacityGeneralAverage),
                                "capacity_average_true"=>($fridayCapacityTrueAverage),
                                "occupancy_average"=>($fridayOccupancyAverage),
                                "nonrevenue_average"=>($fridayNonRevenueAverage),
                                "booking_amount"=>round($fridayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($fridayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($fridayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($fridayOccupancyGeneral),
                                "occupancy_percent_true"=>round($fridayOccupancyTrue)                        
                            ],
                "saturday"=> [
                                "capacity_average_general"=>($saturdayCapacityGeneralAverage),
                                "capacity_average_true"=>($saturdayCapacityTrueAverage),
                                "occupancy_average"=>($saturdayOccupancyAverage),
                                "nonrevenue_average"=>($saturdayNonRevenueAverage),
                                "booking_amount"=>round($saturdayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($saturdayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($saturdayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($saturdayOccupancyGeneral),
                                "occupancy_percent_true"=>round($saturdayOccupancyTrue)                        
                            ],
                "sunday"=> [
                                "capacity_average_general"=>($sundayCapacityGeneralAverage),
                                "capacity_average_true"=>($sundayCapacityTrueAverage),
                                "occupancy_average"=>($sundayOccupancyAverage),
                                "nonrevenue_average"=>($sundayNonRevenueAverage),
                                "booking_amount"=>round($sundayTotalRevenueShareForTheDay),
                                "booking_amount_average_general"=>round($sundayRevenuePerDayPerBikeGeneral),
                                "booking_amount_average_true"=>round($sundayRevenuePerDayPerBikeTrue),
                                "occupancy_percent_general"=>round($sundayOccupancyGeneral),
                                "occupancy_percent_true"=>round($sundayOccupancyTrue)                        
                            ]
            
            
            ];
        
        return $returnObj;
            
        
    }
    
    
    public function reportAreaModelIndex(Request $request)
    {
        $charts = [];
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        $results_page = "false";
        return view('admin.salesReportBarChart.index',compact('charts','areas','models','results_page'));  
        
    }
    
    public function reportAreaModelChart(Request $request)
    {
        $rules = array(
          'start_date' => 'required',
          'end_date' => 'required',
           
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/report_chart')->withErrors($validator->errors());
        }
        
        $reportArray = [];
        
        $startDateCarbon = Carbon::createFromFormat('m/d/Y',$request->start_date);
        $endDateCarbon = Carbon::createFromFormat('m/d/Y',$request->end_date);
        
        if($startDateCarbon->gt($endDateCarbon)){
          return redirect('/admin/report_chart')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
        }
        
        if($request->area_id>0 && $request->model_id>0)
            return redirect('/admin/report_chart')->withErrors(["selection"=>"Choose either Area or Model, not both."]);
        
        
        if($request->area_id>0)
        {
            $modelResponseObjsForArea = [];
            
            $models = BikeModel::where('status',1)->get();
            
            $area = Area::where('id',$request->area_id)->first();
            
            $pageTitle = "The statistics for all models in ".$area->area." for the period ".$startDateCarbon->format('j M-y')." to ".$endDateCarbon->format('j M-y');
            
            foreach($models as $model)
            {
                $modelResponseObj = Self::getOccupancyReportForAreaModel($request->area_id,$model->id,$startDateCarbon,$endDateCarbon);
                
                if($modelResponseObj!="nill_capacity")
                {
                    $modelResponseObjsForArea = array_merge($modelResponseObjsForArea,[substr($model->bike_model,0,20)=>$modelResponseObj]);
                }    
            }
            
            $charts = [];
            
            $chartOccupancyTrueGeneralAllDays = [];
            $chartOccupancyTrueGeneralAllDays['id'] = "chart1";
            $chartOccupancyTrueGeneralAllDays['container_id'] = "chart_container1";
            $chartOccupancyTrueGeneralAllDays['unit'] = "percentage";
            $chartOccupancyTrueGeneralAllDays['title'] = "Occupancy of various models in ".$area->area." - True and General - for all days.";
            $chartOccupancyTrueGeneralAllDays['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralAllDays['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralAllDays['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralAllDays['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralAllDays['y_label_pair'] = [];
            $chartOccupancyTrueGeneralAllDays['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralAllDays['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralAllDays['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                
                array_push($chartOccupancyTrueGeneralAllDays['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['allDays']['occupancy_percent_true'],'label'=>$model_name]);
                
                array_push($chartOccupancyTrueGeneralAllDays['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['allDays']['occupancy_percent_general'],'label'=>$model_name]);
                
                
            }
            
            array_push($charts,$chartOccupancyTrueGeneralAllDays);
            
            
            
            $chartBookingAmountAllDays = [];
            $chartBookingAmountAllDays['id'] = "chart2";
            $chartBookingAmountAllDays['container_id'] = "chart_container2";
            $chartBookingAmountAllDays['unit'] = "rupee";
            $chartBookingAmountAllDays['title'] = "Total Accrued Revenue of various models in ".$area->area." - for all days.";
            $chartBookingAmountAllDays['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountAllDays['secondary_y_axis'] = "false";
            
            $chartBookingAmountAllDays['y_label_pair'] = [];
            
            $chartBookingAmountAllDays['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountAllDays['y_axis_min'] = 0;
            $chartBookingAmountAllDays['y_axis_max'] = 0;
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountAllDays['y_axis_max'] = max($chartBookingAmountAllDays['y_axis_max'],$modelResponseObjsForAreaEl['allDays']['booking_amount']);   
                array_push($chartBookingAmountAllDays['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['allDays']['booking_amount'],'label'=>$model_name]);
                
            }
            
            array_push($charts,$chartBookingAmountAllDays);
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['id'] = "chart3";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['container_id'] = "chart_container3";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['title'] = "Accrued Revenue Per Day Per Bike of various models in ".$area->area." - True and General - for all days.";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'],$modelResponseObjsForAreaEl['allDays']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['allDays']['booking_amount_average_true'],'label'=>$model_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['allDays']['booking_amount_average_general'],'label'=>$model_name]);
                
                
            }
            
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralAllDays);
            
            
            
            
            $chartOccupancyTrueGeneralweekday = [];
            $chartOccupancyTrueGeneralweekday['id'] = "chart4";
            $chartOccupancyTrueGeneralweekday['container_id'] = "chart_container4";
            $chartOccupancyTrueGeneralweekday['unit'] = "percentage";
            $chartOccupancyTrueGeneralweekday['title'] = "Occupancy of various models in ".$area->area." - True and General - for weekday.";
            $chartOccupancyTrueGeneralweekday['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralweekday['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralweekday['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralweekday['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralweekday['y_label_pair'] = [];
            $chartOccupancyTrueGeneralweekday['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralweekday['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralweekday['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                
                array_push($chartOccupancyTrueGeneralweekday['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekday']['occupancy_percent_true'],'label'=>$model_name]);
                
                array_push($chartOccupancyTrueGeneralweekday['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['weekday']['occupancy_percent_general'],'label'=>$model_name]);
                
                
            }
            
                        
            $chartBookingAmountweekday = [];
            $chartBookingAmountweekday['id'] = "chart5";
            $chartBookingAmountweekday['container_id'] = "chart_container5";
            $chartBookingAmountweekday['unit'] = "rupee";
            $chartBookingAmountweekday['title'] = "Total Accrued Revenue of various models in ".$area->area." - for weekday.";
            $chartBookingAmountweekday['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountweekday['secondary_y_axis'] = "false";
            
            $chartBookingAmountweekday['y_label_pair'] = [];
            
            $chartBookingAmountweekday['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountweekday['y_axis_min'] = 0;
            $chartBookingAmountweekday['y_axis_max'] = 0;
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountweekday['y_axis_max'] = max($chartBookingAmountweekday['y_axis_max'],$modelResponseObjsForAreaEl['weekday']['booking_amount'],$modelResponseObjsForAreaEl['weekend']['booking_amount']);   
                array_push($chartBookingAmountweekday['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekday']['booking_amount'],'label'=>$model_name]);
                
            }
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['id'] = "chart6";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['container_id'] = "chart_container6";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['title'] = "Accrued Revenue Per Day Per Bike of various models in ".$area->area." - True and General - for weekday.";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'],$modelResponseObjsForAreaEl['weekday']['booking_amount_average_true'],$modelResponseObjsForAreaEl['weekend']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekday']['booking_amount_average_true'],'label'=>$model_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['weekday']['booking_amount_average_general'],'label'=>$model_name]);
                
                
            }
            
            
            
            $chartOccupancyTrueGeneralweekend = [];
            $chartOccupancyTrueGeneralweekend['id'] = "chart7";
            $chartOccupancyTrueGeneralweekend['container_id'] = "chart_container7";
            $chartOccupancyTrueGeneralweekend['unit'] = "percentage";
            $chartOccupancyTrueGeneralweekend['title'] = "Occupancy of various models in ".$area->area." - True and General - for weekend.";
            $chartOccupancyTrueGeneralweekend['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralweekend['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralweekend['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralweekend['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralweekend['y_label_pair'] = [];
            $chartOccupancyTrueGeneralweekend['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralweekend['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralweekend['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                
                array_push($chartOccupancyTrueGeneralweekend['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekend']['occupancy_percent_true'],'label'=>$model_name]);
                
                array_push($chartOccupancyTrueGeneralweekend['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['weekend']['occupancy_percent_general'],'label'=>$model_name]);
                
                
            }
            
            
            
            $chartBookingAmountweekend = [];
            $chartBookingAmountweekend['id'] = "chart8";
            $chartBookingAmountweekend['container_id'] = "chart_container8";
            $chartBookingAmountweekend['unit'] = "rupee";
            $chartBookingAmountweekend['title'] = "Total Accrued Revenue of various models in ".$area->area." - for weekend.";
            $chartBookingAmountweekend['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountweekend['secondary_y_axis'] = "false";
            
            $chartBookingAmountweekend['y_label_pair'] = [];
            
            $chartBookingAmountweekend['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountweekend['y_axis_min'] = 0;
            $chartBookingAmountweekend['y_axis_max'] = 0;
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountweekend['y_axis_max'] = max($chartBookingAmountweekend['y_axis_max'],$modelResponseObjsForAreaEl['weekend']['booking_amount'],$modelResponseObjsForAreaEl['weekday']['booking_amount']);   
                array_push($chartBookingAmountweekend['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekend']['booking_amount'],'label'=>$model_name]);
                
            }
            
            
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['id'] = "chart9";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['container_id'] = "chart_container9";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['title'] = "Accrued Revenue Per Day Per Bike of various models in ".$area->area." - True and General - for weekend.";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($modelResponseObjsForArea as $model_name => $modelResponseObjsForAreaEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'],$modelResponseObjsForAreaEl['weekend']['booking_amount_average_true'],$modelResponseObjsForAreaEl['weekday']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair'],['y'=>$modelResponseObjsForAreaEl['weekend']['booking_amount_average_true'],'label'=>$model_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair_secondary'],['y'=>$modelResponseObjsForAreaEl['weekend']['booking_amount_average_general'],'label'=>$model_name]);
                
                
            }
            
            array_push($charts,$chartOccupancyTrueGeneralweekend);
            array_push($charts,$chartOccupancyTrueGeneralweekday);
             
            array_push($charts,$chartBookingAmountweekend);
            array_push($charts,$chartBookingAmountweekday);
            
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralweekend);
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralweekday);
            
            
            $areas = Area::where('status',1)->get();
            
            $results_page = "true";
            
            return view('admin.salesReportBarChart.index',compact('charts','areas','models','results_page','pageTitle')); 
            
            
        }
        elseif($request->model_id>0)
        {
            $areaResponseObjsForModel = [];
            
            $areas = Area::where('status',1)->get();
            
            $model = BikeModel::where('id',$request->model_id)->first();
            
            $pageTitle = "The statistics for all areas in ".$model->model." for the period ".$startDateCarbon->format('j M-y')." to ".$endDateCarbon->format('j M-y');
            
            foreach($areas as $area)
            {
                $areaResponseObj = Self::getOccupancyReportForAreaModel($area->id,$request->model_id,$startDateCarbon,$endDateCarbon);
                
                if($areaResponseObj!="nill_capacity")
                {
                    $areaResponseObjsForModel = array_merge($areaResponseObjsForModel,[substr($area->area,0,20)=>$areaResponseObj]);
                }    
            }
            
            $charts = [];
            
            $chartOccupancyTrueGeneralAllDays = [];
            $chartOccupancyTrueGeneralAllDays['id'] = "chart1";
            $chartOccupancyTrueGeneralAllDays['container_id'] = "chart_container1";
            $chartOccupancyTrueGeneralAllDays['unit'] = "percentage";
            $chartOccupancyTrueGeneralAllDays['title'] = "Occupancy of various areas in ".$model->model." - True and General - for all days.";
            $chartOccupancyTrueGeneralAllDays['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralAllDays['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralAllDays['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralAllDays['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralAllDays['y_label_pair'] = [];
            $chartOccupancyTrueGeneralAllDays['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralAllDays['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralAllDays['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                
                array_push($chartOccupancyTrueGeneralAllDays['y_label_pair'],['y'=>$areaResponseObjsForModelEl['allDays']['occupancy_percent_true'],'label'=>$area_name]);
                
                array_push($chartOccupancyTrueGeneralAllDays['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['allDays']['occupancy_percent_general'],'label'=>$area_name]);
                
                
            }
            
            array_push($charts,$chartOccupancyTrueGeneralAllDays);
            
            
            
            $chartBookingAmountAllDays = [];
            $chartBookingAmountAllDays['id'] = "chart2";
            $chartBookingAmountAllDays['container_id'] = "chart_container2";
            $chartBookingAmountAllDays['unit'] = "rupee";
            $chartBookingAmountAllDays['title'] = "Total Accrued Revenue of various areas in ".$model->model." - for all days.";
            $chartBookingAmountAllDays['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountAllDays['secondary_y_axis'] = "false";
            
            $chartBookingAmountAllDays['y_label_pair'] = [];
            
            $chartBookingAmountAllDays['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountAllDays['y_axis_min'] = 0;
            $chartBookingAmountAllDays['y_axis_max'] = 0;
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountAllDays['y_axis_max'] = max($chartBookingAmountAllDays['y_axis_max'],$areaResponseObjsForModelEl['allDays']['booking_amount']);   
                array_push($chartBookingAmountAllDays['y_label_pair'],['y'=>$areaResponseObjsForModelEl['allDays']['booking_amount'],'label'=>$area_name]);
                
            }
            
            array_push($charts,$chartBookingAmountAllDays);
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['id'] = "chart3";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['container_id'] = "chart_container3";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['title'] = "Accrued Revenue Per Day Per Bike of various areas in ".$model->model." - True and General - for all days.";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_axis_max'],$areaResponseObjsForModelEl['allDays']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair'],['y'=>$areaResponseObjsForModelEl['allDays']['booking_amount_average_true'],'label'=>$area_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralAllDays['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['allDays']['booking_amount_average_general'],'label'=>$area_name]);
                
                
            }
            
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralAllDays);
            
            
            
            
            $chartOccupancyTrueGeneralweekday = [];
            $chartOccupancyTrueGeneralweekday['id'] = "chart4";
            $chartOccupancyTrueGeneralweekday['container_id'] = "chart_container4";
            $chartOccupancyTrueGeneralweekday['unit'] = "percentage";
            $chartOccupancyTrueGeneralweekday['title'] = "Occupancy of various areas in ".$model->model." - True and General - for weekday.";
            $chartOccupancyTrueGeneralweekday['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralweekday['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralweekday['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralweekday['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralweekday['y_label_pair'] = [];
            $chartOccupancyTrueGeneralweekday['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralweekday['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralweekday['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                
                array_push($chartOccupancyTrueGeneralweekday['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekday']['occupancy_percent_true'],'label'=>$area_name]);
                
                array_push($chartOccupancyTrueGeneralweekday['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['weekday']['occupancy_percent_general'],'label'=>$area_name]);
                
                
            }
            
                        
            $chartBookingAmountweekday = [];
            $chartBookingAmountweekday['id'] = "chart5";
            $chartBookingAmountweekday['container_id'] = "chart_container5";
            $chartBookingAmountweekday['unit'] = "rupee";
            $chartBookingAmountweekday['title'] = "Total Accrued Revenue of various areas in ".$model->model." - for weekday.";
            $chartBookingAmountweekday['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountweekday['secondary_y_axis'] = "false";
            
            $chartBookingAmountweekday['y_label_pair'] = [];
            
            $chartBookingAmountweekday['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountweekday['y_axis_min'] = 0;
            $chartBookingAmountweekday['y_axis_max'] = 0;
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountweekday['y_axis_max'] = max($chartBookingAmountweekday['y_axis_max'],$areaResponseObjsForModelEl['weekday']['booking_amount'],$areaResponseObjsForModelEl['weekend']['booking_amount']);   
                array_push($chartBookingAmountweekday['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekday']['booking_amount'],'label'=>$area_name]);
                
            }
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['id'] = "chart6";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['container_id'] = "chart_container6";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['title'] = "Accrued Revenue Per Day Per Bike of various areas in ".$model->model." - True and General - for weekday.";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_axis_max'],$areaResponseObjsForModelEl['weekday']['booking_amount_average_true'],$areaResponseObjsForModelEl['weekend']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekday']['booking_amount_average_true'],'label'=>$area_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekday['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['weekday']['booking_amount_average_general'],'label'=>$area_name]);
                
                
            }
            
            
            
            $chartOccupancyTrueGeneralweekend = [];
            $chartOccupancyTrueGeneralweekend['id'] = "chart7";
            $chartOccupancyTrueGeneralweekend['container_id'] = "chart_container7";
            $chartOccupancyTrueGeneralweekend['unit'] = "percentage";
            $chartOccupancyTrueGeneralweekend['title'] = "Occupancy of various areas in ".$model->model." - True and General - for weekend.";
            $chartOccupancyTrueGeneralweekend['y_axis_title'] = "Occupancy Percentage";
            $chartOccupancyTrueGeneralweekend['y_axis_min'] = 0;
            $chartOccupancyTrueGeneralweekend['y_axis_max'] = 110;
            
            $chartOccupancyTrueGeneralweekend['secondary_y_axis'] = "true";
            
            $chartOccupancyTrueGeneralweekend['y_label_pair'] = [];
            $chartOccupancyTrueGeneralweekend['y_label_pair_secondary'] = [];
            
            $chartOccupancyTrueGeneralweekend['y_axis_legend'] = "True Occupancy";
            $chartOccupancyTrueGeneralweekend['y_axis_legend_secondary'] = "General Occupancy";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                
                array_push($chartOccupancyTrueGeneralweekend['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekend']['occupancy_percent_true'],'label'=>$area_name]);
                
                array_push($chartOccupancyTrueGeneralweekend['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['weekend']['occupancy_percent_general'],'label'=>$area_name]);
                
                
            }
            
            
            
            $chartBookingAmountweekend = [];
            $chartBookingAmountweekend['id'] = "chart8";
            $chartBookingAmountweekend['container_id'] = "chart_container8";
            $chartBookingAmountweekend['unit'] = "rupee";
            $chartBookingAmountweekend['title'] = "Total Accrued Revenue of various areas in ".$model->model." - for weekend.";
            $chartBookingAmountweekend['y_axis_title'] = "Total Amount in Rs";
            
            $chartBookingAmountweekend['secondary_y_axis'] = "false";
            
            $chartBookingAmountweekend['y_label_pair'] = [];
            
            $chartBookingAmountweekend['y_axis_legend'] = "Total Accrued Revenue";
            
            $chartBookingAmountweekend['y_axis_min'] = 0;
            $chartBookingAmountweekend['y_axis_max'] = 0;
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountweekend['y_axis_max'] = max($chartBookingAmountweekend['y_axis_max'],$areaResponseObjsForModelEl['weekend']['booking_amount'],$areaResponseObjsForModelEl['weekday']['booking_amount']);   
                array_push($chartBookingAmountweekend['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekend']['booking_amount'],'label'=>$area_name]);
                
            }
            
            
            
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['id'] = "chart9";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['container_id'] = "chart_container9";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['unit'] = "rupee";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['title'] = "Accrued Revenue Per Day Per Bike of various areas in ".$model->model." - True and General - for weekend.";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_title'] = "Accrued Revenue Per Day Per Bike in Rs";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_min'] = 0;
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'] = 0;
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['secondary_y_axis'] = "true";
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair'] = [];
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair_secondary'] = [];
            
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_legend'] = "Accrued Revenue Per Day Per Bike - True";
            $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_legend_secondary'] = "Accrued Revenue Per Day Per Bike - General";
            
            foreach($areaResponseObjsForModel as $area_name => $areaResponseObjsForModelEl)
            {
                $chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'] = max($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_axis_max'],$areaResponseObjsForModelEl['weekend']['booking_amount_average_true'],$areaResponseObjsForModelEl['weekday']['booking_amount_average_true']);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair'],['y'=>$areaResponseObjsForModelEl['weekend']['booking_amount_average_true'],'label'=>$area_name]);
                
                array_push($chartBookingAmountPerDayPerBikeTrueGeneralweekend['y_label_pair_secondary'],['y'=>$areaResponseObjsForModelEl['weekend']['booking_amount_average_general'],'label'=>$area_name]);
                
                
            }
            
            array_push($charts,$chartOccupancyTrueGeneralweekend);
            array_push($charts,$chartOccupancyTrueGeneralweekday);
             
            array_push($charts,$chartBookingAmountweekend);
            array_push($charts,$chartBookingAmountweekday);
            
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralweekend);
            array_push($charts,$chartBookingAmountPerDayPerBikeTrueGeneralweekday);
            
            
            $models = BikeModel::where('status',1)->get();
            
            $results_page = "true";
            
            return view('admin.salesReportBarChart.index',compact('charts','models','areas','results_page','pageTitle')); 
        }
        else
        {
            return redirect('/admin/report_chart')->withErrors(["selection"=>"Choose either Area or Model."]);
        }
        
    }
    
    public function reportAreaModelExcelIndex(Request $request)
    {
        $charts = [];
        $results_page = "false";
        return view('admin.salesReportExcel.index',compact('charts','results_page'));  
        
    }
    
    public function reportAreaModelExcel(Request $request)
    {
        $rules = array(
          'start_date' => 'required',
          'end_date' => 'required',
           
        );


        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
          return redirect('/admin/report_excel')->withErrors($validator->errors());
        }
        
        $days_of_week = $request->days_of_week;
        $weekday_weekend = $request->weekday_weekend;
        
        $reportArray = [];
        
        $startDateCarbon = Carbon::createFromFormat('m/d/Y',$request->start_date);
        $endDateCarbon = Carbon::createFromFormat('m/d/Y',$request->end_date);
        
        if($startDateCarbon->gt($endDateCarbon)){
          return redirect('/admin/report_excel')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
        }
        
        
        if($request->area_or_model == "model")
        {
            $modelResponseObjs = [];
            
            $models = BikeModel::where('status',1)->get();
            
            $user = Auth::user();
            $areas = Area::where('status',1)->whereIn('id',$user->getUserZonesForReport())->get();
            if(!$areas)
                return redirect()->back();
            
            foreach($models as $model)
            {
                $modelResponseObjs[$model->bike_model] = [];
                foreach($areas as $area)
                {
                    $modelResponseObj = Self::getOccupancyReportForAreaModel($area->id,$model->id,$startDateCarbon,$endDateCarbon);

                    if($modelResponseObj!="nill_capacity")
                    {
                        $modelResponseObjs[$model->bike_model] = array_merge($modelResponseObjs[$model->bike_model],[$area->area => $modelResponseObj]);
                    }
                }                
            }
        
            Excel::create('Report Chart', function($excel) use($modelResponseObjs,$days_of_week,$weekday_weekend,$areas) {
                foreach($modelResponseObjs as $model => $modelResponseObjsByModels)
                {
                    $modelAreaArrayByDay = [];
                    
                    foreach($modelResponseObjsByModels as $area=>$modelResponseObjsByModelsByArea)
                    {
                        foreach($modelResponseObjsByModelsByArea as $dayLabel => $modelResponseObjsByModelsByAreaByDay)
                        {
                            if(!array_key_exists($dayLabel,$modelAreaArrayByDay))
                            {
                                $modelAreaArrayByDay[$dayLabel] = [];
                            }
                            $currentarea = $areas->where('area', $area)->first();
                            array_push($modelAreaArrayByDay[$dayLabel],array_merge(["area"=>$area,"city"=>$currentarea->city->city],$modelResponseObjsByModelsByAreaByDay));
                        }
                    }
                    foreach($modelAreaArrayByDay as $dayLabel => $modelAreaArrayByDayBylabel)
                    if($days_of_week == "yes"||$weekday_weekend == "yes")
                    {
                        if($days_of_week == "yes")
                        {
                        
                            $excel->sheet(substr($model." - ".$dayLabel,0,30), function($sheet) use($modelAreaArrayByDayBylabel) {
                                $sheet->fromArray($modelAreaArrayByDayBylabel);
                            });
                        }
                        else
                        {
                            if($dayLabel == "allDays" || $dayLabel == "weekday" || $dayLabel == "weekend")                            
                            {
                                $excel->sheet(substr($model." - ".$dayLabel,0,30), function($sheet) use($modelAreaArrayByDayBylabel) {
                                    $sheet->fromArray($modelAreaArrayByDayBylabel);
                                });                     
                            }
                            
                        }
                         
                    }
                    else
                    {
                        if($dayLabel == "allDays")                            
                        {
                            $excel->sheet(substr($model." - ".$dayLabel,0,30), function($sheet) use($modelAreaArrayByDayBylabel) {
                                    $sheet->fromArray($modelAreaArrayByDayBylabel);
                            });                     
                        }
                        
                    }
                        
                    
                }
                
            })->export('xls');
            
        }
        elseif($request->area_or_model == "area")
        {
            $areaResponseObjs = [];
            
            
            $user = Auth::user();
            $areas = Area::where('status',1)->whereIn('id',$user->getUserZonesForReport())->get();
            if(!$areas)
                return redirect()->back();
            
            $models = BikeModel::where('status',1)->get();
            
            
            foreach($areas as $area)
            {
                $areaResponseObjs[$area->area] = [];
                foreach($models as $model)
                {
                    $areaResponseObj = Self::getOccupancyReportForAreaModel($area->id,$model->id,$startDateCarbon,$endDateCarbon);

                    if($areaResponseObj!="nill_capacity")
                    {
                        $areaResponseObjs[$area->area] = array_merge($areaResponseObjs[$area->area],[$model->bike_model => $areaResponseObj]);
                    }
                }                
            }
            
        
            Excel::create('Report Chart', function($excel) use($areaResponseObjs,$days_of_week,$weekday_weekend) {
                foreach($areaResponseObjs as $area => $areaResponseObjsByAreas)
                {
                    $areaModelArrayByDay = [];
                    
                    foreach($areaResponseObjsByAreas as $model=>$areaResponseObjsByAreasByModel)
                    {
                        foreach($areaResponseObjsByAreasByModel as $dayLabel => $areaResponseObjsByAreasByModelByDay)
                        {
                            if(!array_key_exists($dayLabel,$areaModelArrayByDay))
                            {
                                $areaModelArrayByDay[$dayLabel] = [];
                            }
                            array_push($areaModelArrayByDay[$dayLabel],array_merge(["model"=>$model],$areaResponseObjsByAreasByModelByDay));
                        }
                    }
                    
                    foreach($areaModelArrayByDay as $dayLabel => $areaModelArrayByDayBylabel)
                    if($days_of_week == "yes"||$weekday_weekend == "yes")
                    {
                        if($days_of_week == "yes")
                        {
                        
                            $excel->sheet(substr($area." - ".$dayLabel,0,30), function($sheet) use($areaModelArrayByDayBylabel) {
                                $sheet->fromArray($areaModelArrayByDayBylabel);
                            });
                        }
                        else
                        {
                            if($dayLabel == "allDays" || $dayLabel == "weekday" || $dayLabel == "weekend")                            
                            {
                                $excel->sheet(substr($area." - ".$dayLabel,0,30), function($sheet) use($areaModelArrayByDayBylabel) {
                                        $sheet->fromArray($areaModelArrayByDayBylabel);
                                });                     
                            }
                            
                        }
                    }
                    else
                    {
                        if($dayLabel == "allDays")                            
                        {
                            $excel->sheet(substr($area." - ".$dayLabel,0,30), function($sheet) use($areaModelArrayByDayBylabel) {
                                    $sheet->fromArray($areaModelArrayByDayBylabel);
                            });                     
                        }
                        
                    }
                        
                    
                }
                
            })->export('xls');
            
        }
        else
        {
            
            $areaModelResponseObjs = [];
            
            $user = Auth::user();
            $areas = Area::where('status',1)->whereIn('id',$user->getUserZonesForReport())->get();
            if(!$areas)
                return redirect()->back();  
            
            $models = BikeModel::where('status',1)->get();
            
            $areaModelResponseObjs = [];
            
            foreach($areas as $area)
            {
                
                foreach($models as $model)
                {
                    $areaModelResponseObj = Self::getOccupancyReportForAreaModel($area->id,$model->id,$startDateCarbon,$endDateCarbon);

                    if($areaModelResponseObj!="nill_capacity")
                    {
                        foreach($areaModelResponseObj as $dayLabel => $areaModelResponseObjEl)
                        {
                            if(!array_key_exists($dayLabel,$areaModelResponseObjs))
                                $areaModelResponseObjs[$dayLabel]= [];
                            array_push($areaModelResponseObjs[$dayLabel],array_merge(["model"=>$model->bike_model,"area"=>$area->area,"city"=>$area->city->city],$areaModelResponseObjEl));
                        }
                        
                    }
                }                
            }
            
        
            Excel::create('Report Chart', function($excel) use($areaModelResponseObjs,$days_of_week,$weekday_weekend) {
                 
                    
                    if($days_of_week == "yes"||$weekday_weekend == "yes")
                    {
                        if($days_of_week == "yes")
                        {
                            foreach($areaModelResponseObjs as $dayLabel=>$areaModelResponseObj)
                            {
                                $excel->sheet($dayLabel, function($sheet) use($areaModelResponseObj) {
                                    $sheet->fromArray($areaModelResponseObj);
                                });
                            }
                        }
                        else
                        {
                            foreach($areaModelResponseObjs as $dayLabel=>$areaModelResponseObj)
                            {
                                if($dayLabel == "allDays" || $dayLabel == "weekday" || $dayLabel == "weekend")                            
                                {
                                    $excel->sheet($dayLabel, function($sheet) use($areaModelResponseObj) {
                                            $sheet->fromArray($areaModelResponseObj);
                                    });                     
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        foreach($areaModelResponseObjs as $dayLabel=>$areaModelResponseObj)
                        {
                            if($dayLabel == "allDays")                            
                            {
                                $excel->sheet($dayLabel, function($sheet) use($areaModelResponseObj) {
                                        $sheet->fromArray($areaModelResponseObj);
                                });                     
                            }
                        }
                        
                    }
                                        
            })->export('xls');
            
        }
        
    }
    
    public function a2bIndex(Request $request)
    {
        return view('admin.a2breportPage.index');
    }
    
    public function exportA2BBookings(Request $request)
    {
        
        ini_set('memory_limit', '-1');
        
        if($request->start_date!="")
        {
            $startDateCarbon = Carbon::createFromFormat('m/d/Y H:i:s',$request->start_date." 00:00:00");
            $endDateCarbon = Carbon::createFromFormat('m/d/Y H:i:s',$request->end_date." 00:00:00");

            if($startDateCarbon->gt($endDateCarbon))
            {
                return redirect('/admin/report_page')->withErrors(["dates"=>"Starting Date should be before Ending Date"]);
            }
            
            $bookings = OneWayRentalBooking::where('created_at','>',$startDateCarbon->toDateTimeString())->where('created_at','<',$endDateCarbon->toDateTimeString())->orderBy('id','asc')->get();
        
        }
        else
        {
            $bookings = OneWayRentalBooking::orderBy('id','asc')->get();
        }
        
        $reportArray = [];
        
        foreach($bookings as $booking)
        {
            $reportArrayEl = [
                'ID'=>$booking['reference_id'],
                'Status'=>$booking->status,
                'User Name'=>$booking->user->first_name." ".$booking->user->last_name,
                'Created At'=>Carbon::parse($booking->created_at)->format('m-d-Y H:i'),
                'Model'=>$booking->model->bike_model,
                'Bike NUmber'=>$booking->bike->bike_number,
                'From Location'=>$booking->fromArea->name, 
                'To Location'=>$booking->toArea->name,
                'Customer Notes'=>$booking->customer_instructions,
                'Total Price'=>$booking->price,
                'Online Payment'=>$booking->amount_paid_payment_gateway,
                'Wallet Balance'=>$booking->wallet_amount_applied,
                'Payment ID'=>$booking->payment_id,
                'Promo Code'=>$booking->promocode
            ];  
            
            array_push($reportArray,$reportArrayEl);
        }
        
        Excel::create('A2B Bookings', function($excel) use($reportArray) {
                $excel->sheet('A2B Bookings', function($sheet) use($reportArray) {
                    $sheet->fromModel($reportArray);
                });
        })->export('xls');
        
        return redirect('/admin/a2b_report_page');
        
        
    }
    
    
    
    
}



 
            
     
