<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailPromoLink;

class EmailPromoLinkController extends AppController{



  public function index(){
    $emailPromoLinkList = EmailPromoLink::all();
    return view('admin.emailPromoLink.index',compact('emailPromoLinkList'));
  }

  public function add(Request $request){
    $rules = array(
      'url' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/emailPromoLink')->withErrors($validator->errors());        
    }
    else{
        
        $coverImg = $request->file('coverImgUpload');
        $coverImgId = null;
        if($coverImg){
          $coverImgExt = $coverImg->guessClientExtension();
          $coverImgFn = $coverImg->getClientOriginalName();

          $coverImgHashName = time().'.'.$coverImgExt;
          $coverImgDestinationPath = 'img/email-promo/';
          $coverImg->move($coverImgDestinationPath, $coverImgHashName);

          $coverImgUrl =  'http://'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName;

        }
        else
        {
           $coverImgUrl =''; 
        }
          $message="successfully inserted";
          EmailPromoLink::create(['url' => $request->url, 'image'=>$coverImgUrl,'description'=>$request->description, 'status' => $request->publishStatus]);
        
          return redirect('/admin/emailPromoLink');
    }   
  }


  public function edit($emailPromoLink_id,Request $request){
    $rules = array(
      'url' => 'required',
      'publishStatus' => 'required'
    );

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
      return redirect('/admin/emailPromoLink')->withErrors($validator->errors());        
    }

    $coverImg = $request->file('coverImgUpload');

    if($coverImg){
        $coverImgExt = $coverImg->guessClientExtension();
        $coverImgFn = $coverImg->getClientOriginalName();

        $coverImgHashName = time().'.'.$coverImgExt;
        $coverImgDestinationPath = 'img/email-promo/';
        $coverImg->move($coverImgDestinationPath, $coverImgHashName);

        $coverImgUrl =  'http://'.$_SERVER['HTTP_HOST'].'/'.$coverImgDestinationPath.$coverImgHashName;

    }
    else
    {
        $emailPromoLink = EmailPromoLink::where('id',$emailPromoLink_id)->first();
        $coverImgUrl = $emailPromoLink->image; 
    }
      
        EmailPromoLink::where('id',$emailPromoLink_id)->where('status', 1)
        ->update(['url' => $request->url, 'image'=>$coverImgUrl,'description'=>$request->description, 'status' => $request->publishStatus]);
      
        return redirect('/admin/emailPromoLink'); 
  }


  public function delete($emailPromoLink_id){
    EmailPromoLink::destroy($emailPromoLink_id);
    return redirect('/admin/emailPromoLink');
  }


}
