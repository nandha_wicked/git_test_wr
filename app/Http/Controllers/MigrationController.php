<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Dingo\Api\Routing\Helpers;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\BikeMake;
use App\Models\Availability;
use App\Models\Available;
use App\Models\Capacity;
use App\Models\Availabilitycopy;
use App\Models\WorkingSlot;
use App\Models\Booking;
use App\Models\Wallet;
use App\Models\Slot;
use App\Models\Area;
use App\Models\User;
use App\Models\UserCohort;
use App\Models\PromoCode;
use App\Models\Parameters;
use App\Models\ServiceBlock;
use App\Models\Service;
use App\Models\Occupancy;
use App\Models\OpsReport;
use App\Models\ReservationInfo;
use App\Models\BookingHeader;
use App\Models\DeliveryInfo;
use App\Models\FinancialInfo;
use App\Models\NotesInfo;
use App\Models\PricingInfo;
use App\Models\ReturnInfo;
use App\Models\OneWayRentalLocation;
use App\Models\OneWayRentalFinancialInfo;
use App\Models\OneWayRentalBooking;
use App\Models\OneWayRentalPrice;
use App\Models\OneWayRentalPricingInfo;
use App\Models\OneWayRentalBike;
use App\Models\OneWayRentalLocationHubSpoke;


use Razorpay\Api\Api;


use Auth;
use Hash;
use Curl;

use App\Http\Controllers\WorkingSlotController;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use App\Jobs\ModelHourArea;

use Illuminate\Support\Str;

use Faker\Factory as Faker;


use DB;
use Mail;
use Excel;
use Zipper;

class MigrationController extends AppController
{
    
    public function fixWeekendSlots(){
        
        
       $availability = Availability::all();
        foreach ($availability as $availablityRow)
        {
            
            $date = Carbon::parse($availablityRow['date']);
            if($date->isWeekend())
            {
                $availableSlotsStr = $availablityRow['slots'];
                $availableSlotsArray = explode(',', $availableSlotsStr);
                
                $to_remove = array('9','10','11','12','13','14','15','16','17','18','19');
                $availableSlotsfixed = array_diff($availableSlotsArray, $to_remove);
                
                $availableSlots = implode(',',$availableSlotsfixed);
                
                
                        Availabilitycopy::create(['id' => $availablityRow['id'],'city_id' => $availablityRow['city_id'], 'area_id' => $availablityRow['area_id'],
                              'make_id' => $availablityRow['make_id'], 'model_id' => $availablityRow['model_id'],
                              'bike_id' => $availablityRow['bike_id'], 'date' => $availablityRow['date'],
                              'slots' => $availableSlots, 'booking_id' => $availablityRow['booking_id']
                        ]);
                
            }
            else
            {
                 Availabilitycopy::create(['id' => $availablityRow['id'],'city_id' => $availablityRow['city_id'], 'area_id' => $availablityRow['area_id'],
                              'make_id' => $availablityRow['make_id'], 'model_id' => $availablityRow['model_id'],
                              'bike_id' => $availablityRow['bike_id'], 'date' => $availablityRow['date'],
                              'slots' => $availablityRow['slots'], 'booking_id' => $availablityRow['booking_id']
                        ]);
            }
        }
        
        return redirect('/');
        
    }
    
     public function fixPrices(){
        
        
       $prices = Price::all();
        foreach ($prices as $price)
        {
            $make = BikeModel::where('id',$price->model_id)->pluck('bike_make_id');
            if(in_array($make,[1,2,4,5,6]))
               {
                 Price::where('id', $price->id)->update(['minimum_hours' => 10, 'price_per_hour'=>$price->weekend_gt_8]);  
               }
            else
               {
                 Price::where('id', $price->id)->update(['minimum_hours' => 4, 'price_per_hour'=>$price->weekend_gt_8]);  
               }
            
        
        }
        
        return redirect('/');
        
    }
    

    
    public function inactivateIndex(Request $request)
    {   
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        
    	return view('admin.inactivate.index',compact('areas','models'));
    }
    
    public function inactivateBikes(Request $request)
    {    
        
        $area_id = $request->area_id;
        $model_id = $request->model_id;
        
        
        $today = Carbon::today();
        $today->addDays(env('FUTUREDAYSNO'));
        $today->addDay();
        $now =Carbon::now();
        $now->addHour();
        $now->minute = 0;
        $now->second = 0;
        
        
        $dispatcher = app('Dingo\Api\Dispatcher');
        $bikeAvailability = $dispatcher->with(['model_id' => $model_id, 'area_id' => $area_id, 'start_date' => $now->format('Y-m-d'), 'end_date' => $today->format('Y-m-d'), 'start_time' => $now->format('H:i'), 'end_time' => $today->format('H:i')])->get('check-availability');
        
        
        if($bikeAvailability['bike_id']!="none")
        {
            BikeAvailabilityController::addOrDeleteBike($area_id,$model_id,"delete",0,0);
            return "Done";
        }
        else
        {
            $result = BookingController::getCollidingBookingsForDates($now,$today,$area_id,$model_id);
            $serviceBlockTimePeriod = "";
            return view('admin.collidingBookings.index',compact('result','serviceBlockTimePeriod'));
        }
        
        
    }
    
    
    
    
    
    
    public function populateHours()
    {    	
     
        $bookings = Booking::orderBy('created_at', 'desc')->get();
        foreach ($bookings as $booking) {


          $startDateObj = Carbon::parse($booking['start_datetime']);
          $endDateObj = Carbon::parse($booking['end_datetime']);
          // $startTime = Slot::where('id', $booking['start_slot_id'])->first();
          // $endTime = Slot::where('id', $booking['end_slot_id'])->first();
          $startDate = Carbon::parse(date("Y-m-d", strtotime($booking['start_datetime'])));
          $endDate = Carbon::parse(date("Y-m-d", strtotime($booking['end_datetime'])));


          $startTime = date("H:i", strtotime($booking['start_datetime']));
          $endTime = date("H:i", strtotime($booking['end_datetime']));




            $no_of_days = $startDate->diffInDays($endDate);
          if($no_of_days == 0)
            $no_of_days = 1;
          $sd = $startDate->toDateString();
          $ed = $endDate->toDateString();


          $lastHour = '24:00';
          $firstHour = '00:00';



          $weekDayHours = 0;
          $weekEndHours = 0;
          $weekday = 0;
          $weekend = 0;
          $totalHours = 0;

          if ($startDate == $endDate) {
              $start = explode(':', $startTime);
              $end = explode(':', $endTime); 
              if($startDate->isWeekday())
              $weekDayHours =  $end[0] - $start[0]; 
              else
              $weekEndHours =  $end[0] - $start[0]; 


          } else {
              while ($startDate <= $endDate) {
                if($startDate->isWeekday()){
                  $weekday++;
                  $noOfHours = PriceController::getTotalHours($startDate->toDateString(), $sd, $ed, $startTime, $endTime, $firstHour, $lastHour);
                  $weekDayHours = $weekDayHours + $noOfHours;
                }
                if($startDate->isWeekend()){
                  $weekend++;
                  $noOfHours = PriceController::getTotalHours($startDate->toDateString(), $sd, $ed, $startTime, $endTime, $firstHour, $lastHour);
                  $weekEndHours = $weekEndHours + $noOfHours;
                }
                $startDate->addDay();
              }

          }
            
            Booking::where('id',$booking->id)->update(['weekday_hours'=>$weekDayHours, 'weekend_hours'=>$weekEndHours]);
            
        }
            
    	return redirect('/');
    }
    
    

    
    
    public function populateFullPrice()
    {
        
        Booking::chunk(200, function ($bookings){
            foreach($bookings as $booking)
            {
                if($booking->full_price==0)
                {
                    $startDate = substr($booking->start_datetime,0,10);
                    $startTime = substr($booking->start_datetime,11);


                    $endDate = substr($booking->end_datetime,0,10);
                    $endTime = substr($booking->end_datetime,11);

                    $area = Area::where('id',$booking->area_id)->first();

                    $dispatcher = app('Dingo\Api\Dispatcher');

                    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,'start_time' => $startTime, 'end_time' => $endTime,'model_id' => $booking->model_id, 'area_id' => $booking->area_id,'city_id'=>$area->city_id])->get('bookings/price-and-check-availability');

                    Booking::where('id',$booking->id)->update(['full_price'=>round($availability['total_price'])]);
                }
            }
        });
        return "Done";
        
    }
    
    public function userCohort()
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        DB::table('user_cohort')->delete();
        $bookingsAll = Booking::orderBy('end_datetime','asc')->get()->groupBy('user_id');
        $userIdsWithBookings = Booking::get()->pluck('user_id')->all();
        $userCohortArray = [];
        
        $users = User::all();
        foreach($users as $user)
        {
            $total_number_of_bookings=0;
            $total_amount = 0;
            $booking_ids = [];
            if(in_array($user->id,$userIdsWithBookings))
            {
                $bookings = $bookingsAll[$user->id];
            }
            else
            {
                $bookings = [];
            }
            foreach($bookings as $booking)
            {
                array_push($booking_ids,$booking->id);
                $total_number_of_bookings++;
                $total_amount+=$booking->total_price;

            }
            $booking_ids_str = implode(',',$booking_ids);

            DB::table('user_cohort')->insert([
                'user_id'=>$user->id,
                'user_email'=>$user->email,
                'user_phone'=>$user->mobile_num,
                'total_number_of_bookings'=>$total_number_of_bookings,
                'total_amount'=>$total_amount,
                'booking_ids'=>$booking_ids_str
            ]);
        }
        
        return "Done";
        
    }
    
    public function sendSMS()
    {
        
        $numberObjects = DB::table('sms_numbers')->get();
        $message = DB::table('parameter')->where('parameter_name','sms_text')->first();
        
        foreach($numberObjects as $numberObject)
        {
            $ch = curl_init();

            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, "http://sms.variformsolutions.co.in/api/v3/?method=sms&api_key=A6fdc11e12d3847e06bddbc891dac4df8&to=".$numberObject->number."&sender=WCKDRD&message=".$message->parameter_value);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            // grab URL and pass it to the browser
            curl_exec($ch);
            
            error_log(json_encode(curl_getinfo($ch)));
            

            // close cURL resource, and free up system resources
            curl_close($ch);
           
        }
        return "Done";
        
    }
    
    public function isDeliveredForPast()
    {
        $today = Carbon::now();
        error_log($today->toDateTimeString());

        Booking::where('start_datetime', '<=', $today->toDateTimeString())->update(['is_delivered'=>1]);
        return "Done";
    }
    
    public function randomizeData()
    {
       
       ini_set('max_execution_time', 0);  
        if(env('SERVER_STATUS')=="test")
        {
            User::chunk(1000, function ($users){
                foreach($users as $user)
                {
                    if($user->id!=27660&&$user->id!=1613&&$user->id!=1)
                    {
                        $faker = Faker::create();
                        User::where('id',$user->id)->update(['first_name'=>$faker->firstName, 'last_name'=>$faker->lastName, 'email'=>$faker->safeEmail, 'mobile_num'=>$faker->phoneNumber, 'password'=>bcrypt('secret'),'facebook_id'=>"", 'remember_token'=>"",  'dob'=>"",'profile_img_id'=>0, 'isAdmin'=>0, 'gender'=>0, 'location'=>"", 'avatar_original'=>"", 'avatar_thumb'=>"", 'blur_image'=>"", 'user_document'=>"", 'user_document_status'=>"", 'work_email'=>"", 'note'=>"", '_token'=>"",'dl_image'=>"", 'id_image'=>"", 'dl_id_status'=>"", 'sales_manago_id'=>"", 'gcm_token'=>"", 'role'=>0, 'isOperation'=>0, 'note'=>""]);
                    }
                }
            });
        }
        return "done";
    
    }
    
    
    public function sanitizeData()
    {
        if(env('SERVER_STATUS')=="test")
        {
            Booking::where('id','>',0)->update(['note'=>"",'backend_notes'=>"","delivery_notes"=>""]);

            DB::table('aggregated_bikes')->delete();
            DB::table('aggregator_details')->delete();
            DB::table('appRequest')->delete();
            DB::table('bikationvendor_users')->delete();
            DB::table('bikation_packages_user')->delete();
            DB::table('cancelled_enquiry')->delete();
            DB::table('contact_enquiries')->delete();
            DB::table('email_subscribers')->delete();
            DB::table('deleted_bookings')->delete();
            DB::table('enquiry')->delete();
            DB::table('gcm_token')->delete();
            DB::table('occupancy_report')->delete();
            DB::table('ops_report')->delete();
            DB::table('partner')->delete();
            DB::table('sales_report')->delete();
            DB::table('promo_codes')->delete();
            DB::table('user_cohort')->delete();
            DB::table('wpp_enquiry')->delete();
            DB::table('vendor')->delete();
            DB::table('notes_info')->delete();
        
        }
        return "done";
    
    }
    
    
    public function fixDBIssues(Request $request)
    {
        if($request->nature == "add")
        {
            $natureBool = true;
        }
        else
            $natureBool = false;
            

        $startDateTimeCarbon = Carbon::parse($request->start_datetime);
        $endDateTimeCarbon = Carbon::parse($request->end_datetime);
        
        for($date = Carbon::parse($startDateTimeCarbon->format('Y-m-d')); $date->lte($endDateTimeCarbon->copy()->addHour(-1)); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        
        
        
        $availableObjs = DB::table($request->table)->where('area_id',$request->area_id)->whereIn('date',$dates)->where('model_id',$request->model_id)->orderBy('date','asc')->get();
        
        
        
        
        $availableHourStr = "";
        
        
        foreach($availableObjs as $availableObj)
        {
            $availableHourStr.=$availableObj->hour_string.",";
        }
        
        
        
        $availableArray = BikeAvailabilityController::modifyAMHObj($startDateTimeCarbon,$endDateTimeCarbon,$availableHourStr,$natureBool,$request->area_id,$request->model_id,$dates);
        
        
        
        DB::table($request->table)->where('area_id',$request->area_id)->where('model_id',$request->model_id)->orderBy('date','asc')->where('date','>=',$startDateTimeCarbon->format('Y-m-d'))->where('date','<=',$endDateTimeCarbon->format('Y-m-d'))->delete();
        
        
        
        DB::table($request->table)->insert($availableArray); 


        return "done";

    }
    
    
    public function test(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
    
        /*
        $priceArray = []; 
        $finalPriceArray = []; 
        $prices = Price::orderBy('id','desc')->get();
        
        foreach($prices as $price)
        {
            if(array_key_exists($price->model_id,$priceArray))
            {
                if(array_key_exists($price->area_id,$priceArray[$price->model_id]))
                {
                    
                }
                else
                {
                    $priceArray[$price->model_id][$price->area_id]=$price->hour_weekday;
                    $model = BikeModel::where('id',$price->model_id)->first();
                    $area = Area::where('id',$price->area_id)->first();
                    if(!$area)
                        $areaname = "All Other Areas";
                    else
                        $areaname = $area->area;
                    if($model)
                    array_push($finalPriceArray,['Model'=>$model->bike_model,'Area'=>$areaname,'Minimum Hours'=>$price->minimum_hours,'Weekday Hour'=>$price->hour_weekday,'Weekend Hour'=>$price->hour_weekend,"Per Day - Weekday"=>$price->day_weekday,"Per Day - Weekend"=>$price->day_weekend,"5 Weekdays"=>$price->five_weekday,"Week"=>$price->week,"Month"=>$price->month]);
                    
                }
            }
            else
            {
                $priceArray[$price->model_id][$price->area_id]=$price->minimum_hours;
                $model = BikeModel::where('id',$price->model_id)->first();
                $area = Area::where('id',$price->area_id)->first();
                if(!$area)
                        $areaname = "All Other Areas";
                    else
                        $areaname = $area->area;
                if($model)
                array_push($finalPriceArray,['Model'=>$model->bike_model,'Area'=>$areaname,'Minimum Hours'=>$price->minimum_hours,'Weekday Hour'=>$price->hour_weekday,'Weekend Hour'=>$price->hour_weekend,"Per Day - Weekday"=>$price->day_weekday,"Per Day - Weekend"=>$price->day_weekend,"5 Weekdays"=>$price->five_weekday,"Week"=>$price->week,"Month"=>$price->month]);
                
            }
        }
        Excel::create('Prices', function($excel) use($finalPriceArray) {
                $excel->sheet('Prices', function($sheet) use($finalPriceArray) {
                    $sheet->fromModel($finalPriceArray);
                });
            })->export('xls');
        
        
        return json_encode($finalPriceArray);        
        
        
        /*
        
        $areas = Area::all();
        $models = BikeModel::all();
        $array = [];
        foreach($areas as $area)
        {
            foreach($models as $model)
            {
                $countFromBikeTableObj = Bike::where('area_id',$area->id)->where('model_id',$model->id)->where('status',1)->get();
                if($countFromBikeTableObj->isEmpty())
                    $countFromBikeTable = 0;
                else
                    $countFromBikeTable = $countFromBikeTableObj->count();
                $capacity = Capacity::where('date','2017-01-26')->where('area_id',$area->id)->where('model_id',$model->id)->first();
                if(!$capacity)
                {
                    $countFromCapacityTable = 0;
                }
                else
                {
                    $hourArr = explode(',',$capacity);
                    $countFromCapacityTable = $hourArr[22];
                }
                
                if($countFromBikeTable!=$countFromCapacityTable)
                    $notEqual = "True";
                else
                    $notEqual = "";
        
                array_push($array,['area'=>$area->area,'model'=>$model->bike_model,'count_in_bike_table'=>$countFromBikeTable,'count_in_capacity_table'=>$countFromCapacityTable,'Not Equal'=>$notEqual]);
            }
        }
        
        Excel::create('Bike Count', function($excel) use($array) {
                $excel->sheet('count', function($sheet) use($array) {
                    $sheet->fromModel($array);
                });
        })->export('xls');
        
        
        
        */ 
        
        
        /*
        $areas = Area::where('status',1)->get();
        
        Excel::create('Bike Count', function($excel) use($areas) {
            
            foreach($areas as $area)
            {
                $bikesArray = [];
                
                $bookings = Booking::where('area_id',$area->id)->where('start_datetime','<','2017-01-05 10:00:00')->where('end_datetime','>','2017-01-05 11:00:00')->where('weekday_hours','>',480)->get();
                
                $bookingsArray = [];
                
                foreach($bookings as $booking)
                {
                    $bookingsArray[$booking->bike_id] = "Blocked from ".$booking->getReadableDateTime($booking->start_datetime)." for ".$booking->getDuration();
                }
                
                
                
                $bikes = Bike::where('area_id',$area->id)->where('status',1)->get();
                if(!$bikes)
                {
                    continue;
                }
                foreach($bikes as $bike)
                {
                    $longBlock = "";
                    if(array_key_exists($bike->id,$bookingsArray))
                    {
                        $longBlock = $bookingsArray[$bike->id];
                    }
                    array_push($bikesArray,['id'=>$bike->id,'area_id'=>$area->id,'model_id'=>$bike->model_id,'model'=>$bike->getBikeModelName($bike->model_id),'bike_number'=>"",'owner_email'=>"",'long_block'=>$longBlock]);
                }
                
                
                
                $sort = array();
                foreach($bikesArray as $k=>$v) {
                    $sort['model'][$k] = $v['model'];
                }
                # sort by event_type desc and then title asc
                array_multisort($sort['model'], SORT_ASC, $bikesArray);
                
                $excel->sheet(substr($area->area,0,30), function($sheet) use($bikesArray) {
                    $sheet->fromModel($bikesArray);
                });

            }
                
        })->export('xls');
        */
        
        /*
        
        ini_set('max_execution_time', 0);   
        
        $areas = Area::where('status',1)->get();
        $models = BikeModel::where('status',1)->get();
        
        $today = Carbon::today();
        $today->addDay();
        
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        
        Capacity::where('date','>','2017-01-06')->update(["hour_string"=>$zero_str]);
        Available::where('date','>','2017-01-06')->update(["hour_string"=>$zero_str]);
        Occupancy::where('date','>','2017-01-06')->update(["hour_string"=>$zero_str]);
        Service::where('date','>','2017-01-06')->update(["hour_string"=>$zero_str]);
        
        foreach($areas as $area)
        {
            foreach($models as $model)
            {
                $bikes = Bike::where('status',1)->where('area_id',$area->id)->where('model_id',$model->id)->get();
                foreach($bikes as $bike)
                {
                    BikeAvailabilityController::addOrDeleteBike($area->id,$model->id,"add",0,0);
                }
            }
        }
        
        
        $areas = Area::get();
        $models = BikeModel::get();
        $array = [];
        
        
        $bookings = Booking::where('end_datetime','>','2017-01-06 21:00:00')->get();
        error_log(count($bookings));
        foreach($bookings as $booking)
        {
            BikeAvailabilityController::addOrDeleteBooking($booking['id'],"add");
        }
        error_log("done");
        
        */
        
        /*
        
        Capacity::where('date','>','2017-10-16')->delete();
        Available::where('date','>','2017-10-16')->delete();
        Occupancy::where('date','>','2017-10-16')->delete();
        Service::where('date','>','2017-10-16')->delete();
        
        
        $capacityObjs = Capacity::where('date','2017-10-16')->get();
        
        for($date = Carbon::parse('2017-10-17'); $date->lte(Carbon::parse('2018-01-18')); $date->addDay()) 
        {
            $dates[] = $date->format('Y-m-d');
        }
        
        foreach($capacityObjs as $capacityObj)
        {
            $AMHObjArray = [];
            foreach($dates as $date)
            {
                array_push($AMHObjArray,['date'=>$date,'area_id'=>$capacityObj->area_id,'model_id'=>$capacityObj->model_id,'hour_string'=>$capacityObj->hour_string]);
            }
            
            DB::table('capacity')->insert($AMHObjArray);
        }

        $availableObjs = Available::where('date','2017-10-16')->get();
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";        

        foreach($availableObjs as $availableObj)
        {
            $AMHObjArray = [];
            foreach($dates as $date)
            {
                array_push($AMHObjArray,['date'=>$date,'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'hour_string'=>$availableObj->hour_string]);
            }
            
            DB::table('available')->insert($AMHObjArray);
            
            
            
            
            $AMHObjArray = [];
            foreach($dates as $date)
            {
                array_push($AMHObjArray,['date'=>$date,'area_id'=>$availableObj->area_id,'model_id'=>$availableObj->model_id,'hour_string'=>$zero_str]);
            }
            
            DB::table('occupancy')->insert($AMHObjArray);

            DB::table('service')->insert($AMHObjArray);
            
            
        }
        
        */
        
        /*
        if($request->first =="true")
        {
            Capacity::where('model_id',60)->where('area_id',36)->delete();
            Available::where('model_id',60)->where('area_id',36)->delete();
            Service::where('model_id',60)->where('area_id',36)->delete();
            Occupancy::where('model_id',60)->where('area_id',36)->delete();            
        }
        if($request->first =="true2")
        {
            $bookings = Booking::where('area_id',36)->where('model_id',60)->get();
            foreach($bookings as $booking)
            {
                BikeAvailabilityController::addOrDeleteBooking($booking->id,"add");
            }
            
        }
        
      
      
        
        
        User::chunk(200, function ($users){
            $length = 5;
            foreach($users as $user)
            {
                if($user->referral_code == "")
                {
                    $referral_code = substr(str_shuffle(str_repeat($x='23456789abcdefghjklmnpqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
        
                    User::where('id',$user->id)->update(['referral_code'=>$referral_code]);
                }
            }
        });
          */
          
        
    
       /*         
        
        
        foreach($request->request as $key=>$value)
        {
            error_log($key." - ".json_encode($value));
        }
        if(!isset($request->call_name))
        {
            return view('admin.testView.index');
        }
        
        $createdByUser = UserController::getUserByToken();
        
        $callId = DB::table('api_call')->insertGetId([
            'title'=>$request->call_name,
            'description'=>$request->call_description,
            'url'=>$request->call_url,
            'method'=>$request->call_type,
            'created_by'=>$createdByUser->id
        ]);
        
        $parentArguments = [];
        
        $numberOfArguments = count($request->argument_name);
        
        if($numberOfArguments)
        {
            foreach($request->argument_name as $key=>$argument)
            {
                if($key == $request->parent_argument[$key])
                {
                    $argumentObjId = DB::table('api_argument')->insertGetId([
                        'call_id'=>$callId,
                        'parent_argument'=>0,
                        'argument'=>$request->argument_name[$key],
                        'required_or_optional'=>$request->argument_required_or_optional[$key],
                        'description'=>$request->argument_description[$key],
                        'type'=>$request->argument_type[$key],
                        'created_by'=>$createdByUser->id
                    ]);

                    $parentArguments[$key] = $argumentObjId;

                }            
            }

            while(count($parentArguments)<$numberOfArguments)
            {
                foreach($request->argument_name as $key=>$argument)
                {
                    if(($key != $request->parent_argument[$key]) && (array_key_exists($request->parent_argument[$key],$parentArguments)))
                    {
                        $argumentObj = DB::table('api_argument')->insertGetId([
                            'call_id'=>$callId,
                            'parent_argument'=>$parentArguments[$request->parent_argument[$key]],
                            'argument'=>$request->argument_name[$key],
                            'required_or_optional'=>$request->argument_required_or_optional[$key],
                            'description'=>$request->argument_description[$key],
                            'type'=>$request->argument_type[$key],
                            'created_by'=>$createdByUser->id
                        ]);

                        $parentArguments[$key] = $argumentObj;

                    }            
                }
            }
            
        }
        
        
        
        
        return view('admin.testView.index');
        */
        /* 
        
        ServiceBlock::chunk(200, function ($blocks){
            foreach($blocks as $block)
            {
                if($block->booking_status)
                {
                    Booking::where('id',$block->booking_id)->update(['bike_number_id'=>$block->bike_number_id]);
                }
            }
        });
        
        return "done";
        
       
        $startTime = ["10","11","12","13","14","15","16","17","20","21"];
        $bookings = Booking::where('area_id',18)->get();
        
        echo "total booking - ".count($bookings)."<br/>";
        
        $count = 0;
        
        foreach($bookings as $booking)
        {
            $start0 = explode(' ',$booking->start_datetime);
            $start = explode(':',$start0[1]);
            if(in_array($start[0],$startTime)){
                if(($booking->weekday_hours+$booking->weekend_hours)<5)
                {
                    echo $booking->getUserEmailThis()."#---# Hours Weeday Hours Rented #---#".$booking->weekday_hours."#---# Start Time #---#".$start[0]."<br/>";
                    $count++;
                }
            }
            
        }
        
        echo "total booking - ".$count;
        
        
        $bookings = Booking::get(Parameters::getColumns());
                
        foreach($bookings as $booking)
        {
            $startDateTimeCarbon = Carbon::parse($booking->start_datetime);
            $endDateTimeCarbon = Carbon::parse($booking->end_datetime);
            
            $hours = BookingController::weekDayAndWeekEndHours($startDateTimeCarbon,$endDateTimeCarbon);
            
            $weekDayHours = $hours['weekDayHours'];
            $weekEndHours = $hours['weekEndHours'];
            
            Booking::where('id',$booking->id)->update([
                'total_hours'=>($weekDayHours+$weekEndHours)
            ]);
            
            echo $booking->id." - ".$weekDayHours." - ".$weekEndHours."<br/>";
        }
        
        
        
        
        
       
        
        $response = Curl::to("https://maps.googleapis.com/maps/api/directions/json?origin="."12.9907,77.6525"."&destination="."12.978469,77.656595"."&key=".env('GOOGLEMAPSAPI'))->post();
        
        $decoded = json_decode($response,true);
        error_log($decoded['routes'][0]['legs'][0]['duration']['value']."  -  ".$decoded['routes'][0]['legs'][0]['distance']['value']);
        
         
        $response = Curl::to("https://www.googleapis.com/urlshortener/v1/url?key=".env('GOOGLESHORTURLAPI'))->withContentType('application/json')->withData(json_encode(['longUrl'=>'https://docs.google.com/forms/d/e/1FAIpQLSf-uBbd_eVHGeyW8UXLeLz2vFLMMDZcYiH0Ueum3bpRLNt4Mw/viewform?usp=pp_url&entry.1121588755=varunagni@gmail.com&entry.1758923294=BloiiiTest','key'=>env('GOOGLESHORTURLAPI')]))->post();
        error_log(json_decode($response)->id);
        
        $booking = Booking::where('id',29654)->first();
        echo $booking->created_at;
        
        echo Carbon::parse($booking->created_at)->format('m-d-Y H:i');
        
        $bookings = Booking::where('created_at_ts','0000-00-00 00:00:00')->get(Parameters::getColumns());
        $bookingIds = [];
        
        foreach($bookings as $booking)
        {
            array_push($bookingIds,$booking->id);
        }
        Booking::whereIn('id',$bookingIds)->update(['created_at_ts'=>'20001-01-01 00:00:00']);
        
        
        $bookings = Booking::where('total_hours',0)->get(["weekday_hours","weekend_hours","id"]);
        foreach($bookings as $booking)
        {
            Booking::where('id',$booking->id)->update(["total_hours"=>($booking->weekday_hours + $booking->weekend_hours)]);
        }
        return "done";
       
        
        
        echo stripslashes("https:\/\/www.metrobikes.in\/img\/slider\/cashback-17.jpeg");
        
        $revenueIds = explode(',',env('REVENUEIDS'));
        Booking::whereIn('user_id',$revenueIds)->update(["assisted"=>1]);
        return "done";
        
        
        ini_set('max_execution_time', 0);  
        
        //Relationship example
        
        $users = User::whereHas('bookings.model',function ($query) {
            $query->where('id',3);
        })->whereHas('bookings',function ($query) {
            $query->where('end_datetime','>','2017-05-01');
            $query->where('end_datetime','<','2017-06-01');
        })->with(['bookings' => function ($query) {
            $query->where('end_datetime','>','2017-05-01');
            $query->where('end_datetime','<','2017-06-01');
        }])->get();
        
        foreach($users as $user)
        {
            echo $user->id." - ";
            $total = 0;
            foreach($user->bookings as $booking)
            {
                $total += $booking->total_price;
            }
            echo $total."<br>";
        }
       
        
        $startDate = Carbon::parse('2017-08-08 08:00');
        $endDate = Carbon::parse('2017-08-08 20:00');
        $weekendHours = $startDate->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
           return $date->isWeekend();
        }, $endDate);
        $weekdayHours = $startDate->diffFiltered(CarbonInterval::hour(), function(Carbon $date) {
           return $date->isWeekday();
        }, $endDate);
        
        echo $weekendHours." ".$weekdayHours;
        
        
        
        $user = User::where('id',48022)->first();
        echo json_encode($user->getBalance());
        
        
        
        
        $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

        
        $payment = $api->payment->fetch("pay_8S7zVHTqel8Ux3");
        
        foreach($payment as $key=>$value)
        {
            error_log($key." - ".json_encode($value));
        }

        error_log("Response");
        $response = $payment->capture(array('amount' => $payment->amount));
        
        foreach($response as $key=>$value)
        {
            error_log($key." - ".json_encode($value));
        }
        
        
        $currentDateTime = Carbon::now();
        $currentDateTime->minute = 0;
        $currentDateTime->second = 0;
        
        $movements = DB::table('movement_log')->where('end_datetime',$currentDateTime->toDateTimeString())->get();
        foreach($movements as $movement)
        {
            Bike::where('reg_number',$movement->bike_number)->update([
                "area_id"=>$movement->from_area_id
            ]);
        }
        
        return "done";
        
        
        
        
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $bookings = Booking::where('id',"<",50000)->get();
        
        foreach($bookings as $booking)
        {
            BookingHeader::where('id',$booking->id)->update([
                "ref_id"=>$booking->reference_id
            ]);
        }
        
        return "done";
      
        
        FrontViewController::storeHourlyResponses();
        return "done";
        
        
        
        echo Carbon::parse(Carbon::now()->toDateString())->format('d-M-y');
        
        
        
        
        $areas = OneWayRentalLocation::all();
         
        $response = Curl::to("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=cosmopolitan%20club%20bangalore&location=12.917713,77.592728&radius=50000&key=".env('GOOGLEMAPSAPI'))->post();
        
        echo $response;
         
        
        $bookings = ReservationInfo::has('pricingInfo', '<', 1)->get()->pluck('id')->all();
        echo json_encode($bookings);
       
        $bookingIds = array(50001,50002,50003,50004,50005,50006,50007,50008,50009,50010,50011,50012,50013,50014,50015,50016,50017,50018,50019,50020,50021,502643);
        
        foreach($bookingIds as $bookingId)
        {
            $dispatcher = app('Dingo\Api\Dispatcher');
            $booking = ReservationInfo::where('id',$bookingId)->first();
            if(!(!$booking))
            {
                $priceDetails = $dispatcher->with([
                'model_id' => $booking->model_id, 
                'area_id' => $booking->area_id, 
                'start_datetime' => $booking->start_datetime, 
                'end_datetime' => $booking->end_datetime, 
                'coupon'=>""
                ])->get('bookings/total-price');

                PricingInfo::create([
                    'bike_or_accessory'=>"B",
                    'reservation_id'=>$bookingId,
                    'pricing_id'=>$priceDetails['pricing_id'],
                    'full_price'=>$priceDetails['total_price'],
                    'promo_discount'=> 0,
                    'price_after_discount'=>$priceDetails['total_price'],
                    'promo_code'=>"",
                    'tax_id'=>1,
                    'taxable_price'=>$priceDetails['total_price'],
                    'tax_amount'=>$priceDetails['total_price']*0.18,
                    'price_after_tax'=>$priceDetails['total_price']*1.18
                ]);
            }                        
        }
        
        
        
        $bookings = OneWayRentalBooking::has('pricingInfo', '<', 1)->get();
        
        foreach($bookings as $booking)
        {
            $promoCreditsRedeemed = 0;
            $nonpromoRedeemed = 0;
            
            if($booking->wallet_amount_applied > 0)
            {
                $walletTxn = Wallet::where('nature',"Redeemed for a One Way Rental Boooking")->where('booking_id',$booking->id)->first();
                
                if(!(!$walletTxn))
                {
                    if($walletTxn->promotional_amount > 0)
                    {
                        $promoCreditsRedeemed = $walletTxn->promotional_amount;
                         
                        $financialInfo = OneWayRentalFinancialInfo::create([
                            'booking_id'=>$booking->id,
                            'user_id'=>$booking->user_id,
                            'debit_or_credit'=>"C",
                            'new_or_edit'=>"N",
                            'amount'=>$promoCreditsRedeemed,
                            'payment_method'=>12,
                            'payment_id'=>$walletTxn->id,
                            'payment_nature'=>1,
                            'status'=>"Paid",
                            'created_by'=>$booking->user_id
                        ]);
                    }

                    if($walletTxn->non_promotional_amount > 0)
                    {
                        $nonpromoRedeemed = $walletTxn->non_promotional_amount;
                        
                        $financialInfo = OneWayRentalFinancialInfo::create([
                            'booking_id'=>$booking->id,
                            'user_id'=>$booking->user_id,
                            'debit_or_credit'=>"C",
                            'new_or_edit'=>"N",
                            'amount'=>$walletTxn->non_promotional_amount,
                            'payment_method'=>13,
                            'payment_id'=>$walletTxn->id,
                            'payment_nature'=>1,
                            'status'=>"Paid",
                            'created_by'=>$booking->user_id
                        ]);
                    }
                }
                
            }
            
            if($booking->amount_paid_payment_gateway > 0)
            {
                $financialInfo = OneWayRentalFinancialInfo::create([
                    'booking_id'=>$booking->id,
                    'user_id'=>$booking->user_id,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>"N",
                    'amount'=>$booking->amount_paid_payment_gateway,
                    'payment_method'=>3,
                    'payment_id'=>$booking->payment_id,
                    'payment_nature'=>1,
                    'status'=>"COMPLETE",
                    'created_by'=>$booking->user_id
                ]);
            }
                        
            $price = round((($booking->amount_paid_payment_gateway + (1.18*$promoCreditsRedeemed) + $nonpromoRedeemed)/1.18),2);
            $priceObj = OneWayRentalPrice::where('model_id',$booking->model_id)->first();
            $tax = round((($price - $promoCreditsRedeemed)*0.18),2);
            
            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"D",
                'new_or_edit'=>"N",
                'amount'=>$price,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>2,
                'status'=>"na",
                'created_by'=>$booking->user_id
            ]);

            OneWayRentalFinancialInfo::create([
                'booking_id'=>$booking->id,
                'user_id'=>$booking->user_id,
                'debit_or_credit'=>"D",
                'new_or_edit'=>"N",
                'amount'=>$tax,
                'payment_method'=>0,
                'payment_id'=>"na",
                'payment_nature'=>3,
                'status'=>"na",
                'created_by'=>$booking->user_id
            ]);

            OneWayRentalPricingInfo::create([
                'booking_id'=>$booking->id,
                'pricing_id'=>$priceObj->id,
                'distance'=>$booking->estimated_distance,
                'duration'=>$booking->estimated_duration,
                'full_price'=>$price,
                'promo_discount'=> 0,
                'price_after_discount'=>$price,
                'promo_code'=>"",
                'tax_id'=>2,
                'promotional_balance_used'=>$promoCreditsRedeemed,
                'taxable_price'=>($price - $promoCreditsRedeemed),
                'tax_amount'=>$tax,
                'price_after_tax'=>($price + $tax) 
            ]);

        }
        
        return "done";
        
        
        $booking = Booking::where('id',502608)->first();
        echo $booking->reservationInfo->bookingHeader->notes()->first()->text;
        
        
        $bookings = Booking::where('end_datetime','>',"2017-10-24 00:00:00")->get();
        
        foreach($bookings as $booking)
        {
            Booking::where('id',$booking->id)->update([
                "note"=>(count($booking->reservationInfo->bookingHeader->notes)>0)?($booking->reservationInfo->bookingHeader->notes()->first()->text):"" 
            ]);
        }
        
        return "done";
        
        
        
        $bookingHeaders = BookingHeader::where("ref_id","")->get();
        
        $booking = Booking::get(['id','reference_id']);
        
        $bookingById = $booking->keyBy('id')->toArray();
        
        foreach($bookingHeaders as $bookingHeader)
        {
            if(array_key_exists($bookingHeader->id,$bookingById))
            {
                BookingHeader::where('id',$bookingHeader->id)->update([
                    "ref_id"=>$bookingById[$bookingHeader->id]['reference_id']
                ]);
            }
        }
        
        return "done";
        
        
        */
        
        $hubsForTheSpoke = OneWayRentalLocationHubSpoke::where('spoke_area',37)->where('deleted',0)->get()->pluck('hub_area')->all();
            
        $locationsThatCanServeTheGivenLocation = OneWayRentalLocationHubSpoke::whereIn('hub_area',$hubsForTheSpoke)->where('deleted',0)->get()->pluck('spoke_area')->all();

        $allLocationsThatCanServeTheGivenLocation = array_merge($locationsThatCanServeTheGivenLocation,$hubsForTheSpoke);

        echo json_encode($allLocationsThatCanServeTheGivenLocation);
    }
    
    public function create_zip(Request $request)
    {
        $k = $request->k;
        $files = glob(public_path('excel/'.$k.'/*'));
        Zipper::make('excel/'.$k.'/booking.zip')->add($files);
        return "done";
    }
    
    
    public function getBookingFile(Request $request)
    {
        return Response::download(public_path('excel/'.$request->name.'/booking.zip'));

    }

    
    public function createReportData(Request $request)
    {
        ini_set('max_execution_time', 0);   
        
        $begin = new \DateTime( $request->data_start_date );
        $end = new \DateTime($request->data_end_date );
        \date_add($end, \date_interval_create_from_date_string('1 day'));
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);
        
        $dateArray = [];
        
        $result = [];
        
        foreach ($period as $dt)
        {
            $start_date = $dt->format('Y-m-d');
            $dispatcher = app('Dingo\Api\Dispatcher');
            $reportData = $dispatcher->with(['data_start_date' => $start_date,'data_end_date' => $start_date])->get('getReportDataEveryDay');
        }
    }
    
    public function createOpsReportData(Request $request)
    {
        ini_set('max_execution_time', 0);   
        
        $begin = $dt = Carbon::parse($request->data_start_date);
        $end = Carbon::parse($request->data_end_date);
                
        
        while($end > $dt)
        {
            $start_date = $dt->format('Y-m-d');
            $dispatcher = app('Dingo\Api\Dispatcher');
            $reportData = $dispatcher->with(['start_date' => $start_date])->get('getOpsReportDataEveryDay');
            $dt->addDay();
        }
        
    }
    
    public function convertMysqlToPhp(Request $request)
    {
        if(isset($request->mysqlstr))
        {
            $mysqlArray = explode('`',$request->mysqlstr);
            $dataKeys = [];
            foreach($mysqlArray as $key => $mysqlEl)
            {
                if($key%2)
                {
                    array_push($dataKeys,$mysqlEl);
                }
            }
            echo "<br/>";
            echo "[";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."',";
            }
            echo "]";
            echo "<br/>";
            echo "<br/>";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."'=>$".$dataKey.", ";
            }
            echo "<br/>";
            echo "<br/>";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."'=>$".$dataKey.", <br/>";
            }
            echo "<br/>";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."'=>\$request->".$dataKey.", <br/>";
            }
            echo "<br/>";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."'=>\$request->".$dataKey."_add, <br/>";
            }
            echo "<br/>";
            foreach($dataKeys as $dataKey)
            {
                echo "'".$dataKey."'=>\$request->".$dataKey."_edit, <br/>";
            }
        }
        
        $form = "<form method=\"post\" action=\"\">";
        $input = "<input type=\"text\" name=\"mysqlstr\"/><input type=\"hidden\" name=\"_token\" value=\"".csrf_token()."\">";
        $button = "<button>Convert</button></form>";
        echo $form.$input.$button;
        
    }
    
    public function fixAvailableServiceOccupancy(Request $request)
    {
        $area_id = $request->area_id;
        
        Available::where('area_id',$area_id)->delete();
        
        $availableWithoutBookings = Capacity::where('area_id',$area_id)->get();
        $availableWithoutBookings = $availableWithoutBookings->toArray();
        
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        
        $zeroWithoutBookings = Capacity::where('area_id',$area_id)->get();
        
        foreach($zeroWithoutBookings as $zeroWithoutBooking)
        {
            $zeroWithoutBooking->hour_string = $zero_str;
        }
        $zeroWithoutBookings = $zeroWithoutBookings->toArray();
        DB::table('available')->insert($availableWithoutBookings);
        DB::table('occupancy')->insert($zeroWithoutBookings);
        DB::table('service')->insert($zeroWithoutBookings);
        
        $bookings = Booking::where('area_id',$area_id)->get();
        
        foreach($bookings as $booking)
        {
            BikeAvailabilityController::addOrDeleteBooking($booking['id'],"add");
        }
        
    }
    
    public function fixAvailableServiceOccupancyForDatesAreaModel(Request $request)
    {
        $areaId = $request->area_id;
        $modelId = $request->model_id;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        
        Available::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->where('date','<=',$endDate)->delete();
        Service::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->where('date','<=',$endDate)->delete();
        Occupancy::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->where('date','<=',$endDate)->delete();
        
        $availableWithoutBookings = Capacity::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->where('date','<=',$endDate)->get(['area_id','model_id','date','hour_string']);
        $availableWithoutBookings = $availableWithoutBookings->toArray();
        
        $zero_str ="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        
        $zeroWithoutBookings = Capacity::where('area_id',$areaId)->where('model_id',$modelId)->where('date','>=',$startDate)->where('date','<=',$endDate)->get(['area_id','model_id','date','hour_string']);
        
        foreach($zeroWithoutBookings as $zeroWithoutBooking)
        {
            $zeroWithoutBooking->hour_string = $zero_str;
        }
        
        $zeroWithoutBookings = $zeroWithoutBookings->toArray();
        DB::table('available')->insert($availableWithoutBookings);
        DB::table('occupancy')->insert($zeroWithoutBookings);
        DB::table('service')->insert($zeroWithoutBookings);
        
        $bookings = Booking::where('area_id',$areaId)->where('model_id',$modelId)->where('start_datetime','>=',$startDate." 00:00:00")->where('end_datetime','<=',$endDate." 00:00:00")->get();
        
        foreach($bookings as $booking)
        {
            BikeAvailabilityController::addOrDeleteBooking($booking['id'],"add");
        }
        
        return "done";
        
    }
    
    public function deleteCapacityTables(Request $request)
    {
        Capacity::where('area_id',$request->area_id)->where('model_id',$request->model_id)->delete();
        Available::where('area_id',$request->area_id)->where('model_id',$request->model_id)->delete();
        Service::where('area_id',$request->area_id)->where('model_id',$request->model_id)->delete();
        Occupancy::where('area_id',$request->area_id)->where('model_id',$request->model_id)->delete();
        
        
        return "done";
        
    }
    
    public function addBalanceToUser(Request $request)
    {
        ini_set('max_execution_time', 0);   

        $walletUserIds = Wallet::all();
        $walletUserIds = $walletUserIds->pluck('user_id');
        
        User::whereNotIn('id',$walletUserIds)->chunk(200, function ($users){
            foreach($users as $user)
            {
                $dispatcher = app('Dingo\Api\Dispatcher');
                $addWalletBalance = $dispatcher->with([
                    'user_email_add' => $user['email'],
                    'amount_add' => env('SIGNUPBONUS'),
                    'nature_add' => 'Sign up bonus',
                    'notes_add' => 'Sign up bonus for signing up with referral code '.$user->referral_code_used
                ])->post('wallet/add_amount');
            }
        });
        
        return "done";
        
    }
    
    
    public function testApi1(Request $request)
    {
        $data = [
            [
                "title"=>"List 1",
                "component1"=>"Component 1",
                "component2"=>"Component 2",
                
            ],
            [
                "title"=>"List 2",
                "component1"=>"Component 3",
                "component2"=>"Component 4",
                
            ],
            [
                "title"=>"List 3",
                "component1"=>"Component 5",
                "component2"=>"Component 6",
                
            ],
            [
                "title"=>"List 4",
                "component1"=>"Component 7",
                "component2"=>"Component 8",
                
            ]
            
        ];
        
        return $this->respondWithSuccess($data);
        
    }
        
    public function testApi1Details(Request $request)
    {
        $rules = array(
        'list_number' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        return $this->respondWithValidationErrorStatusCode($validator->errors());        
        }
        
        $list = $request->list_number;
        
        $data = [
            "details_para1"=> "list ".$list." details para 1. ahcbdsciu gdscgksucb gusyc bccdsncgn dcdsucbdsuycg sdcugcsc",
            "details_para2"=> "list ".$list." details para 2. ahcbdsciu gdscgksucb gusyc bccdsncgn dcdsucbdsuycg sdcugcsc",
        ];
        return $this->respondWithSuccess($data);
    }
    
    public function testApi2(Request $request)
    {
        $lat = $request->lat;
        $long = $request->long;
        
        $coordinates = [
            [
                "lat" => $lat*1.00009,
                "long" => $long*1.00009,
                "icon" => "http://wickedride.com/img/logo.png"
            ],
            [
                "lat" => $lat*0.99997,
                "long" => $long*0.99997,
                "icon" => "http://www.metrobikes.in/images/metro-bikes-logo.png"
            ],
            [
                "lat" => $lat*1.00005,
                "long" => $long*1.00005,
                "icon" => "http://wickedride.com/img/logo.png"
            ]
            
        ];
        
        return $this->respondWithSuccess($coordinates);
    }
    
    
    public function repeatCustomers(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        if(isset($request->start))
        {
            $startDate = Carbon::parse($request->start);
        }
        else
        {
            $startDate = Carbon::createFromDate(2014,9,1);
        }
        $startDate->hour = 0;
        $startDate->minute = 0;
        $startDate->second = 0;
        $lastMonth = Carbon::parse('first day of last month');
        
        $bookingsAll = Booking::whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->get(['id','end_datetime']);
        $bookingsAllEndDate = $bookingsAll->keyBy('id');
        
        $dataArray = [];
        $result = [];
        
        while($startDate->lt($lastMonth))
        {
            $endDate = $startDate->copy()->addMonth();
            
            $bookings = Booking::where('end_datetime','>=',$startDate->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDate->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->orderBy('end_datetime','asc')->get();
            
            $users = User::where('created_at','>=',$startDate->format('Y-m-d H:i:s'))->where('created_at','<',$endDate->format('Y-m-d H:i:s'))->get();
            
            $bookingsByUsers = $bookings->groupBy('user_id');
            $bookingsById = $bookings->keyBy('id');
            
            $userCohort = UserCohort::all()->keyBy('user_id');                        
            
            
            
            $repeatUsersKnown = 0;
            $revenueFromKnownRepeatUsers = 0;
            $count = 0;
            $numberOfHours = 0;
            $bookingsByUsersArray = $bookingsByUsers->toArray();
            
            $revenueIds = explode(',',env('REVENUEIDS'));
            
            $reservationsIdCount = 0;
            
            foreach($revenueIds as $revenueId)
            {
                if(array_key_exists($revenueId,$bookingsByUsersArray))
                {
                    $reservationsIdCount += $bookingsByUsers[$revenueId]->count();
                }
            }
            
            
            $firstTimeKnownUsers = 0;
            
            foreach($bookingsByUsers as $userId=>$bookingsByUsersEl)
            {
                if($userCohort[$userId]->total_number_of_bookings > 1)
                {
                    $repeatUsersKnown++;
                    foreach($bookingsByUsersEl as $booking)
                    {
                        $revenueFromKnownRepeatUsers += $booking->total_price;
                        $numberOfHours += ($booking->weekday_hours + $booking->weekend_hours);
                        $count++;
                    }
                }
                                
                $userBookingIds = explode(',',$userCohort[$userId]->booking_ids);
                                                
                $endDateTimeCarbon = Carbon::parse($bookingsAllEndDate[$userBookingIds[0]]->end_datetime);
                
                if($endDateTimeCarbon->between($startDate, $endDate))
                {
                    $firstTimeKnownUsers++;
                }
                
            }
            
            if(count($bookingsByUsers)==0)
            {
                $totalRepeatPerc = 0;
                $bookingsFromRepeatUsers = 0;
                $repeatUsers = 0;
                $revenueFromRepeatUsers = 0;
                $totalRepeatPercKnown = 0;
                $totalUsers = 0;
                $firstTimeUsers = 0;
            }
            else
            {   
                $usersKnown = (count($bookingsByUsers)-1);
                $bookingsPerUserForKnown = (count($bookings)-$reservationsIdCount)/$usersKnown;
                $usersUnknown = $reservationsIdCount/$bookingsPerUserForKnown;
                $repeatUsersUnknown = $usersUnknown*$repeatUsersKnown/$usersKnown;
                $totalRepeatPercKnown = $repeatUsersKnown/$usersKnown*100;
                $totalRepeatPerc = ($repeatUsersKnown+$repeatUsersUnknown)/(count($bookingsByUsers)+$usersUnknown-1)*100;
                
                $bookingsFromKnownRepeatUsers = (count($bookings) - $reservationsIdCount) - ($usersKnown - $repeatUsersKnown);
                $bookingsFromUnKnownRepeatUsers = $bookingsFromKnownRepeatUsers/$repeatUsersKnown*$repeatUsersUnknown;
                
                $bookingsFromRepeatUsers = ($bookingsFromKnownRepeatUsers+ceil($bookingsFromUnKnownRepeatUsers));
                
                $repeatUsers = (ceil($repeatUsersUnknown)+$repeatUsersKnown);
                
                $revenueFromRepeatUsers = $revenueFromKnownRepeatUsers + (($revenueFromKnownRepeatUsers/$repeatUsersKnown)*$repeatUsersUnknown);
                
                $totalUsers = (ceil($usersUnknown)+$usersKnown);
                
                $firstTimeUnknownUsers = ceil(($firstTimeKnownUsers/(count($bookings)-$reservationsIdCount))*$reservationsIdCount);
                
                $firstTimeUsers = ($firstTimeUnknownUsers+$firstTimeKnownUsers);
            }
            
            
            $countBelow4 = 0;
            $countBelow10 = 0;
            $countBelow24 = 0;
            $countAbove24 = 0;
            $countAbove7 = 0;
            
            $hoursBelow4 = 0;
            $hoursBelow10 = 0;
            $hoursBelow24 = 0;
            $hoursAbove24 = 0;
            $hoursAbove7 = 0;
            
            $revenueBelow4 = 0;
            $revenueBelow10 = 0;
            $revenueBelow24 = 0;
            $revenueAbove24 = 0;
            $revenueAbove7 = 0;
            
            
            $totalRevenue = 0;
            $totalHoursBooked = 0;
            
            foreach($bookings as $booking)
            {
                if(($booking->weekday_hours + $booking->weekend_hours) < 4)
                {
                    $countBelow4++;
                    $hoursBelow4+=($booking->weekday_hours + $booking->weekend_hours);
                    $revenueBelow4+=$booking->total_price;
                }
                elseif(($booking->weekday_hours + $booking->weekend_hours) < 10)
                {
                    $countBelow10++;
                    $hoursBelow10+=($booking->weekday_hours + $booking->weekend_hours);
                    $revenueBelow10+=$booking->total_price;
                }
                elseif(($booking->weekday_hours + $booking->weekend_hours) < 25)
                {
                    $countBelow24++;
                    $hoursBelow24+=($booking->weekday_hours + $booking->weekend_hours);
                    $revenueBelow24+=$booking->total_price;
                    
                }
                elseif(($booking->weekday_hours + $booking->weekend_hours) > 168)
                {
                    $countAbove7++;
                    $hoursAbove7+=($booking->weekday_hours + $booking->weekend_hours);
                    $revenueAbove7+=$booking->total_price;
                    
                }
                else
                {
                    $countAbove24++;
                    $hoursAbove24+=($booking->weekday_hours + $booking->weekend_hours);
                    $revenueAbove24+=$booking->total_price;
                }
                
                $totalRevenue += $booking->total_price;
                $totalHoursBooked += ($booking->weekday_hours + $booking->weekend_hours);
            }
            
            
            echo $startDate->format('F Y')."<br/>";
            echo "Number of Bookings - ".count($bookings)."<br/>";
            echo "Number of Unique Users - ".$totalUsers."<br/>";
            echo "Number of Repeat Users - ".$repeatUsers."<br/>";
            echo "Percentage of Repeat Users - ".$totalRepeatPerc."%<br/>";
            echo "Percentage of Repeat Users Without Assumption - ".$totalRepeatPercKnown."%<br/>";
            echo "Bookings from Repeat Users - ".$bookingsFromRepeatUsers."<br/>";
            echo "Revenue from Repeat Users - ".$revenueFromRepeatUsers."<br/>";
            echo "First Time Users - ".$firstTimeUsers."<br/>";
            echo "Number of New Registered Users - ".count($users)."<br/>";
            echo "Booking Below 4 Hours - ".$countBelow4."<br/>";
            echo "Booking Below 10 Hours - ".$countBelow10."<br/>";
            echo "Booking Below 24 Hours - ".$countBelow24."<br/>";
            echo "Booking Above 24 Hours - ".$countAbove24."<br/>";
            echo "Booking Above 7 Days - ".$countAbove7."<br/>";
            echo "Booked Hours Below 4 Hours - ".$hoursBelow4."<br/>";
            echo "Booked Hours Below 10 Hours - ".$hoursBelow10."<br/>";
            echo "Booked Hours Below 24 Hours - ".$hoursBelow24."<br/>";
            echo "Booked Revenue Above 24 Hours - ".$hoursAbove24."<br/>";
            echo "Booked Revenue Above 7 Days - ".$hoursAbove7."<br/>";
            echo "Booked Revenue Below 4 Hours - ".$revenueBelow4."<br/>";
            echo "Booked Revenue Below 10 Hours - ".$revenueBelow10."<br/>";
            echo "Booked Revenue Below 24 Hours - ".$revenueBelow24."<br/>";
            echo "Booked Revenue Above 24 Hours - ".$revenueAbove24."<br/>";
            echo "Booked Revenue Above 7 Days - ".$revenueAbove7."<br/>";
            echo "Total Revenue - ".$totalRevenue."<br/>";
            echo "Total Hours Booked - ".$totalHoursBooked."<br/>";
            echo "Total Bookings from Reservation ID - ".$reservationsIdCount."<br/><br/>";
            
            
            
            $dataArray["Month"] = $startDate->format('F Y');
            $dataArray["Number of Bookings"] = count($bookings);
            $dataArray["Number of Unique Users"] = $totalUsers;
            $dataArray["Number of Repeat Users"] = $repeatUsers;
            $dataArray["Percentage of Repeat Users"] = $totalRepeatPerc."%";
            $dataArray["Percentage of Repeat Users Without Assumption"] = $totalRepeatPercKnown."%";
            $dataArray["Bookings from Repeat Users"] = $bookingsFromRepeatUsers;
            $dataArray["Revenue from Repeat Users"] = $revenueFromRepeatUsers;
            $dataArray["First Time Users"] = $firstTimeUsers;
            $dataArray["First Time Known Users"] = $firstTimeKnownUsers;
            $dataArray["Number of New Registered Users"] = count($users);
            $dataArray["Booking Below 4 Hours"] = $countBelow4;
            $dataArray["Booking Below 10 Hours"] = $countBelow10;
            $dataArray["Booking Below 24 Hours"] = $countBelow24;
            $dataArray["Booking Above 24 Hours"] = $countAbove24;
            $dataArray["Booking Above 7 Days"] = $countAbove7;
            $dataArray["Booked Hours Below 4 Hours"] = $hoursBelow4;
            $dataArray["Booked Hours Below 10 Hours"] = $hoursBelow10;
            $dataArray["Booked Hours Below 24 Hours"] = $hoursBelow24;
            $dataArray["Booked Hours Above 24 Hours"] = $hoursAbove24;
            $dataArray["Booked Hours Above 7 Days"] = $hoursAbove7;
            $dataArray["Booked Revenue Below 4 Hours"] = $revenueBelow4;
            $dataArray["Booked Revenue Below 10 Hours"] = $revenueBelow10;
            $dataArray["Booked Revenue Below 24 Hours"] = $revenueBelow24;
            $dataArray["Booked Revenue Above 24 Hours"] = $revenueAbove24;
            $dataArray["Booked Revenue Above 7 Days"] = $revenueAbove7;
            $dataArray["Total Revenue"] = $totalRevenue;
            $dataArray["Total Hours Booked"] = $totalHoursBooked;
            
            
            
            $startDate->addMonth();    
            
            array_push($result,$dataArray);
        }
        
        
        
        echo json_encode($result);
    }
    
    
    public function cohortData(Request $request)
    {
        $areaIds = [2,3,10,13,14,17,30,32,34,37,45,46,51];
            
        $booking = Booking::where('start_datetime','>','2016-12-31 23:59:59')->where('user_id','>',20)->get(Parameters::getColumns());
        
        $bookingBelow4Hours = Booking::where('start_datetime','>','2016-12-31 23:59:59')->where('user_id','>',20)->where('total_hours','<',5)->get(Parameters::getColumns());
        
        $bookingBelow10Hours = Booking::where('start_datetime','>','2016-12-31 23:59:59')->where('user_id','>',20)->where('total_hours','<',11)->get(Parameters::getColumns());
        
        $bookingBelow24Hours = Booking::where('start_datetime','>','2016-12-31 23:59:59')->where('user_id','>',20)->where('total_hours','<',25)->get(Parameters::getColumns());
        
        $bookingsByUser = $booking->groupBy('user_id');
        
        $bookingsBelow4HoursByUser = $bookingBelow4Hours->groupBy('user_id');
        $bookingsBelow10HoursByUser = $bookingBelow10Hours->groupBy('user_id');
        $bookingsBelow24HoursByUser = $bookingBelow24Hours->groupBy('user_id');
        
        
        $userCohort = [];
        $userCohortBelow4Hours = [];
        $userCohortBelow10Hours = [];
        $userCohortBelow24Hours = [];
        
        
        foreach($bookingsByUser as $userId => $bookingsByUserEl)
        {
            $gapBetweenBookings = [];
            $gapBetweenBookingsForMedian = [];
            
            $lastBooking = $bookingsByUserEl[0]->start_datetime;
            if(count($bookingsByUserEl)>1)
            {
                
                $lastBookingStartCarbon = Carbon::parse($lastBooking);
                
                $i = 0;
                
                foreach($bookingsByUserEl as $booking)
                {
                    $startDateCarbon = Carbon::parse($booking->start_datetime);
                    $gap = $startDateCarbon->diffInHours($lastBookingStartCarbon);
                    array_push($gapBetweenBookings,$gap);
                    if($gap>0)
                    array_push($gapBetweenBookingsForMedian,$gap);
                    $i++;
                    $lastBookingStartCarbon = $startDateCarbon;
                }
                
            }
            
            if(count($gapBetweenBookings)>2)
                $averageGap = Self::array_median($gapBetweenBookingsForMedian);
            elseif(count($gapBetweenBookings)==2)
                $averageGap = $gapBetweenBookings[1];
            else
                $averageGap = 0;
            
            if(1)
                $userCohort[$userId] = ["user_id"=>$userId,"gap_between_bookings"=>implode(",",$gapBetweenBookings),"average_gap_between_bookings"=>$averageGap,"count_of_bookings"=>count($bookingsByUserEl)];
            
        }
        
        foreach($bookingsBelow4HoursByUser as $userId => $bookingsByUserEl)
        {
            $gapBetweenBookings = [];
            $gapBetweenBookingsForMedian = [];
            
            $lastBooking = $bookingsByUserEl[0]->start_datetime;
            if(count($bookingsByUserEl)>1)
            {
                
                $lastBookingStartCarbon = Carbon::parse($lastBooking);
                
                foreach($bookingsByUserEl as $booking)
                {
                    $startDateCarbon = Carbon::parse($booking->start_datetime);
                    $gap = $startDateCarbon->diffInHours($lastBookingStartCarbon);
                    array_push($gapBetweenBookings,$gap);
                    if($gap>0)
                    array_push($gapBetweenBookingsForMedian,$gap);
                    $i++;
                    
                    $lastBookingStartCarbon = $startDateCarbon;
                }
                
            }
            
            if(count($gapBetweenBookings)>2)
                $averageGap = Self::array_median($gapBetweenBookingsForMedian);
            elseif(count($gapBetweenBookings)==2)
                $averageGap = $gapBetweenBookings[1];
            else
                $averageGap = 0;
                        
            if(1)
                $userCohortBelow4Hours[$userId] = ["user_id"=>$userId,"gap_between_bookings"=>implode(",",$gapBetweenBookings),"average_gap_between_bookings"=>$averageGap,"count_of_bookings"=>count($bookingsByUserEl)];
            
        }
        
        
        foreach($bookingsBelow10HoursByUser as $userId => $bookingsByUserEl)
        {
            $gapBetweenBookings = [];
            $gapBetweenBookingsForMedian = [];
            
            $lastBooking = $bookingsByUserEl[0]->start_datetime;
            if(count($bookingsByUserEl)>1)
            {
                
                $lastBookingStartCarbon = Carbon::parse($lastBooking);
                
                foreach($bookingsByUserEl as $booking)
                {
                    $startDateCarbon = Carbon::parse($booking->start_datetime);
                    $gap = $startDateCarbon->diffInHours($lastBookingStartCarbon);
                    array_push($gapBetweenBookings,$gap);
                    if($gap>0)
                    array_push($gapBetweenBookingsForMedian,$gap);
                    $i++;
                    
                    $lastBookingStartCarbon = $startDateCarbon;
                }
                
            }
            
            if(count($gapBetweenBookings)>2)
                $averageGap = Self::array_median($gapBetweenBookingsForMedian);
            elseif(count($gapBetweenBookings)==2)
                $averageGap = $gapBetweenBookings[1];
            else
                $averageGap = 0;
            
            
            if(1)
                $userCohortBelow10Hours[$userId] = ["user_id"=>$userId,"gap_between_bookings"=>implode(",",$gapBetweenBookings),"average_gap_between_bookings"=>$averageGap,"count_of_bookings"=>count($bookingsByUserEl)];
            
        }
        
        foreach($bookingsBelow24HoursByUser as $userId => $bookingsByUserEl)
        {
            $gapBetweenBookings = [];
            $gapBetweenBookingsForMedian = [];
            
            $lastBooking = $bookingsByUserEl[0]->start_datetime;
            if(count($bookingsByUserEl)>1)
            {
                
                $lastBookingStartCarbon = Carbon::parse($lastBooking);
                
                foreach($bookingsByUserEl as $booking)
                {
                    $startDateCarbon = Carbon::parse($booking->start_datetime);
                    $gap = $startDateCarbon->diffInHours($lastBookingStartCarbon);
                    array_push($gapBetweenBookings,$gap);
                    if($gap>0)
                    array_push($gapBetweenBookingsForMedian,$gap);
                    $i++;
                    $lastBookingStartCarbon = $startDateCarbon;
                }
                
            }
            
            if(count($gapBetweenBookings)>2)
                $averageGap = Self::array_median($gapBetweenBookingsForMedian);
            elseif(count($gapBetweenBookings)==2)
                $averageGap = $gapBetweenBookings[1];
            else
                $averageGap = 0;
                        
            if(1)
                $userCohortBelow24Hours[$userId] = ["user_id"=>$userId,"gap_between_bookings"=>implode(",",$gapBetweenBookings),"average_gap_between_bookings"=>$averageGap,"count_of_bookings"=>count($bookingsByUserEl)];
            
        }
        
        
        
        
        $result = ["overall"=>$userCohort,"4_hours_and_below"=>$userCohortBelow4Hours,"10_hours_and_below"=>$userCohortBelow10Hours,"24_hours_and_below"=>$userCohortBelow24Hours];
        
        Excel::create('Data', function($excel) use($result) {
                $excel->sheet('overall', function($sheet) use($result) {
                    $sheet->fromModel($result["overall"]);
                });
                $excel->sheet('4_hours_and_below', function($sheet) use($result) {
                    $sheet->fromModel($result["4_hours_and_below"]);
                });
                $excel->sheet('10_hours_and_below', function($sheet) use($result) {
                    $sheet->fromModel($result["10_hours_and_below"]);
                });
                $excel->sheet('24_hours_and_below', function($sheet) use($result) {
                    $sheet->fromModel($result["24_hours_and_below"]);
                });
            })->export('xls');
    }
    
    function array_median($array) 
    {
          // perhaps all non numeric values should filtered out of $array here?
          $iCount = count($array);
          if ($iCount == 0) {
            return 0;
          }
          // if we're down here it must mean $array
          // has at least 1 item in the array.
          $middle_index = floor($iCount / 2);
          sort($array, SORT_NUMERIC);
          $median = $array[$middle_index]; // assume an odd # of items
          // Handle the even case by averaging the middle 2 items
          if ($iCount % 2 == 0) {
            $median = ($median + $array[$middle_index - 1]) / 2;
          }
          return $median;
    }
    
    public function formMakerIndex(Request $request)
    {
        return view('admin.formMaker.index');
    }
    
    public function getElementsForIndexType(Request $request)
    {
        $json = Parameters::getParameter('elements_for_index_form');
        $array = json_decode($json);
        
        $type = $request->type;
        
        foreach($array as $el)
        {
            if($el->type == $type)
                return $this->respondWithSuccess($el);
        }
        
        return "";
        
    }
    
    public function addForm(Request $request)
    {
        foreach($request->request as $key=>$value)
        {
            error_log($key." - ".json_encode($value));
        }
    }
    
    public function matchPaymentId(Request $request)
    {
        ini_set('max_execution_time', 0);   
        
        $paymentIds = explode(',',Parameters::getParameter('paymentIds'));
        
        $result = [];
        
        foreach($paymentIds as $paymentId)
        {
            $booking = Booking::where(function ($query) use($paymentId){
                            $query->where('note','like',"%".$paymentId."%")->first();
                        })->orWhere(function ($query) use($paymentId){
                            $query->where('pg_txn_id',$paymentId);
                        })->orWhere(function ($query) use($paymentId){
                            $query->where('bank_txn_id',$paymentId);
                        })->orWhere(function ($query) use($paymentId){
                            $query->where('order_id',$paymentId);
                        })->first();
            if(!$booking)
            {
                array_push($result,["Payment ID"=>$paymentId,"Booking ID"=>" "]);
            }
            else
            {
                array_push($result,["Payment ID"=>$paymentId,"Booking ID"=>$booking->reference_id]);
            }
        }
        Excel::create('Match', function($excel) use($result) {
            $excel->sheet('Match', function($sheet) use($result) {
                $sheet->fromModel($result);
            });
        })->store('xls');
        
    }
    
    
    public function parseHtml(Request $request)
    {
        $response = Curl::to("http://www.moneycontrol.com/india/stockpricequote/financeinvestments/apisindia/AI65")->get();
        
        $str_split = explode("\"Bse_Prc_tick\"><strong>",$response);
        
        $str_split2 = explode("</strong>",$str_split[1]);
        
        error_log($str_split2[0]);
        
    }
    
    public function disableDebug()
    {
        error_log(Carbon::now()->toDateTimeString());
        
        $env = file_get_contents(base_path() . '/.env');
        str_replace(
            "APP_DEBUG" . '=' . env('APP_DEBUG'),
            "APP_DEBUG=false",$env);
        file_put_contents(base_path() . '/.env', $env);
    }
    
    public function calculatePromotionalBalanceRemaining(Request $request)
    {
        ini_set('memory_limit', '-1');
        $users = User::get(['id']);
        $walletBalance = Wallet::orderBy('id','desc')->get(['id','user_id','promotional_balance'])->groupBy('user_id')->toArray();
        
        $totalBalanceRemaining = 0;
        
        foreach($users as $user)
        {
            if(array_key_exists($user->id,$walletBalance))
            {
                $totalBalanceRemaining += $walletBalance[$user->id][0]['promotional_balance'];
            }
        }
        
        echo $totalBalanceRemaining;
    }
    
    
     public function webAppAssistedCustomers(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $startDate = Carbon::createFromDate(2014,9,1);
        $startDate->hour = 0;
        $startDate->minute = 0;
        $startDate->second = 0;
        $lastMonth = Carbon::parse('first day of this month');
        
        
        $dataArray = [];
        $result = [];
        
        while($startDate->lt($lastMonth))
        {
            $endDate = $startDate->copy()->addMonth();
            
            $appBookings = Booking::where('end_datetime','>=',$startDate->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDate->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->where('assisted',0)->whereIn('payment_method',["paytm","RazorPayApp","RazorPayAppiOS"])->orderBy('end_datetime','asc')->get();
            
            $webBookings = Booking::where('end_datetime','>=',$startDate->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDate->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->where('assisted',0)->whereIn('payment_method',["","RazorPay","PayUMoney"])->orderBy('end_datetime','asc')->get();
            
            $assistedBookings = Booking::where('end_datetime','>=',$startDate->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDate->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->where('assisted',1)->orderBy('end_datetime','asc')->get();
            
            $bookings = Booking::where('end_datetime','>=',$startDate->format('Y-m-d H:i:s'))->where('end_datetime','<',$endDate->format('Y-m-d H:i:s'))->whereNotIn('user_id',explode(',',env('NONREVENUEIDS')))->orderBy('end_datetime','asc')->get();
            
            $dataArray["Month"] = $startDate->format('F Y');
            $dataArray["App Bookings Count"] = count($appBookings);
            $dataArray["App Bookings Revenue"] = $appBookings->sum('total_price');
            $dataArray["Web Bookings Count"] = count($webBookings);
            $dataArray["Web Bookings Revenue"] = $webBookings->sum('total_price');
            $dataArray["Assisted Bookings Count"] = count($assistedBookings);
            $dataArray["Assisted Bookings Revenue"] = $assistedBookings->sum('total_price');
            $dataArray["Edited App and Web Bookings Count"] = count($bookings) - (count($assistedBookings)+count($appBookings)+count($webBookings));
            $dataArray["Edited App and Web Bookings Revenue"] = $bookings->sum('total_price') - ($appBookings->sum('total_price') + $webBookings->sum('total_price') + $assistedBookings->sum('total_price'));
            
            $startDate->addMonth();    
            
            array_push($result,$dataArray);
        }
        
        
        
        echo json_encode($result);
    }
    
    public function walletBalanceMoM()
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $startDate = Carbon::createFromDate(2014,9,1);
        $startDate->hour = 0;
        $startDate->minute = 0;
        $startDate->second = 0;
        $lastMonth = Carbon::parse('first day of this month');
        
        
        $dataArray = [];
        $result = [];
        
        while($startDate->lt($lastMonth))
        {
            $endDate = $startDate->copy()->addMonth();
            
            $walletTxns = Wallet::where('created_at','>=',$startDate->format('Y-m-d H:i:s'))->where('created_at','<',$endDate->format('Y-m-d H:i:s'))->get();
            
            $promoCreditsGiven = 0;
            $promoCreditsRedeemed = 0;
            
            foreach($walletTxns as $walletTxn)
            {
                if($walletTxn->credit_or_debit == "C")
                {
                    $promoCreditsGiven += $walletTxn->promotional_amount;
                }
                else
                {
                    $promoCreditsRedeemed += $walletTxn->promotional_amount;
                }
            }
            
            $dataArray["Month"] = $startDate->format('F Y');
            $dataArray["Promo Credits Given"] = $promoCreditsGiven;
            $dataArray["Promo Credits Redeemed"] = $promoCreditsRedeemed;
            
            $startDate->addMonth();   
            array_push($result,$dataArray);
        }
        
        echo json_encode($result);
    }
    
    public function migrateWalletAmount()
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $walletTxns = Wallet::where('credit_or_debit',"D")->get();
        
        foreach($walletTxns as $walletTxn)
        {
            $lastBalance = Wallet::where('user_id',$walletTxn->user_id)->where('created_at','<',$walletTxn->created_at)->orderBy('id','desc')->first();
            if(!$lastBalance)
            {
            }
            else
            {
                $promoRedeemed = $lastBalance->promotional_balance - $walletTxn->promotional_balance;
                $nonpromoRedeemed = $lastBalance->non_promotional_balance - $walletTxn->non_promotional_balance;
                
                Wallet::where('id',$walletTxn->id)->update([
                    'promotional_amount' => $promoRedeemed,
                    'non_promotional_amount' => $nonpromoRedeemed
                ]);
            }
        }
        
        
        $walletTxns = Wallet::where('credit_or_debit',"C")->get();
        
        foreach($walletTxns as $walletTxn)
        {
            if($walletTxn->nature=="Credit added offline"||$walletTxn->nature=="Credit added as refund")
            {
                Wallet::where('id',$walletTxn->id)->update([
                    'promotional_amount' => 0,
                    'non_promotional_amount' => $walletTxn->amount
                ]);
            }
            else
            {
                
                Wallet::where('id',$walletTxn->id)->update([
                    'promotional_amount' => $walletTxn->amount,
                    'non_promotional_amount' => 0
                ]);
            }
        }
        
        return "done";
    }
    
    
    public function clearNumber(Request $request)
    {
        if(env('SERVER_STATUS')!="live")
        {
            User::where('mobile_num','LIKE',"%".$request->number."%")->update(["mobile_num"=>"000000000"]);
        }
        return "done";
    }
    
    public function showCapacityAvailble(Request $request)
    {
        
        $capacityObj = Capacity::where('date','>=',$request->start_date)->where('date','<=',$request->end_date)->where('model_id',$request->model_id)->where('area_id',$request->area_id)->orderBy('date','asc')->get()->pluck('hour_string');
        
        $capacityStr = implode(',',$capacityObj->all());
        
        $capacity = explode(',',$capacityStr);
        
        $availableObj = Available::where('date','>=',$request->start_date)->where('date','<=',$request->end_date)->where('model_id',$request->model_id)->where('area_id',$request->area_id)->orderBy('date','asc')->get()->pluck('hour_string');
        
        $availableStr = implode(',',$availableObj->all());
        
        $available = explode(',',$availableStr);
        
        $occupancyObj = Occupancy::where('date','>=',$request->start_date)->where('date','<=',$request->end_date)->where('model_id',$request->model_id)->where('area_id',$request->area_id)->orderBy('date','asc')->get()->pluck('hour_string');
        
        $occupancyStr = implode(',',$occupancyObj->all());
        
        $occupancy = explode(',',$occupancyStr);
        
        $serviceObj = Service::where('date','>=',$request->start_date)->where('date','<=',$request->end_date)->where('model_id',$request->model_id)->where('area_id',$request->area_id)->orderBy('date','asc')->get()->pluck('hour_string');
        
        $serviceStr = implode(',',$serviceObj->all());
        
        $service = explode(',',$serviceStr);
        
        
        $start = Carbon::parse($request->start_date);
        $end = Carbon::parse($request->end_date);
        
        $date = $start->copy();
        
        $i = 0;
        
        $res = "";
        
        $res .= "<table><th>Date</th><th></th><th></th><th>Capacity</th><th>Available</th><th>Occupancy</th><th>Service</th>";
        
        while($date->lte($end))
        {
            for($j=0;$j<24;$j++)
            {
                $res .=  "<tr><td>".$date->toDateString()." ".$j."</td><td></td><td></td><td>".$capacity[$i+$j]."</td><td>".$available[$i+$j]."</td><td>".$occupancy[$i+$j]."</td><td>".$service[$i+$j]."</td></tr>";
            }
            $i+=24;
            $date->addDay();
        }
        
        $res .=  "</table>";
        
        echo $res;
        
    }
    
    public function moveBookingstoReservation(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        Booking::chunk(200, function ($bookings){
            
            foreach($bookings as $booking)
            {
                $user = User::where('id',$booking->user_id)->first();
                if($booking->createdBy == 0)
                {
                    $createdByUser = User::where('id',$booking->user_id)->first();
                }
                else
                {
                    $createdByUser = User::where('id',$booking->createdBy)->first();
                }
                $promo_code = $booking->promocode==null?"":$booking->promocode;
                $reservation_detail = [];
                $reservation_detail['id'] = $booking->id;
                $reservation_detail['ref_id'] = $booking->reference_id;
                $reservation_detail['model_id'] = $booking->model_id;
                $reservation_detail['area_id'] = $booking->area_id;
                $reservation_detail['start_datetime'] = $booking->start_datetime;
                $reservation_detail['end_datetime'] = $booking->end_datetime;
                $reservation_detail['wallet_amount_applied'] = $booking->wallet_amount_applied;
                $reservation_detail['total_price'] = $booking->total_price==null?0:$booking->total_price;
                $reservation_detail['pg_txn_amount'] = $booking->pg_txn_amount;
                $reservation_detail['KMlimit'] = $booking->KMlimit;
                $reservation_detail['weekday_hours'] = $booking->weekday_hours;
                $reservation_detail['weekend_hours'] = $booking->weekend_hours;
                $reservation_detail['total_hours'] = $booking->total_hours;
                $reservation_detail['res_lead_time'] = $booking->res_lead_time;
                $reservation_detail['fuel_included']  = $booking->fuel_included;
                $reservation_detail['created_at']  = $booking->created_at;

                $paymentMethods = explode(',',$booking->payment_method);
                if((in_array($paymentMethods[0],array("PayUMoney","RazorPay",null))) && ($booking->assisted == 0))
                    $creation_mode = "W";
                elseif((in_array($paymentMethods[0],array("paytm","RazorPayApp","PayTM"))) && ($booking->assisted == 0))
                    $creation_mode = "A";
                elseif((in_array($paymentMethods[0],array("RazorPayAppiOS"))) && ($booking->assisted == 0))
                    $creation_mode = "I";
                elseif($booking->lead_source == "In-Store")
                    $creation_mode = "S";
                else
                    $creation_mode = "C";
                
                $startDateTimeCarbon = Carbon::parse($booking->start_datetime);
                $endDateTimeCarbon = Carbon::parse($booking->end_datetime);
                $now = Carbon::now();
                if($endDateTimeCarbon->lte($now))
                {
                    $current_state = "Completed";
                }
                elseif($startDateTimeCarbon->lte($now))
                {
                    $current_state = "Reservation Created";
                }
                else
                {
                    $current_state = "Bike delivered";
                }
                
                $financial_info = [];
                if($reservation_detail['wallet_amount_applied'] > 0)
                {
                    $wallet = Wallet::where('booking_id',$booking->id)->where('nature',"Redeemed for a boooking")->first();
                    if(!$wallet)
                    {
                        
                    }
                    else
                    {
                        if($wallet->promotional_amount > 0)
                        {
                            $financialInfo['payment_method'] = 2;
                            $financialInfo['promo'] = $wallet->promotional_amount;
                            $financialInfo['nonpromo'] = 0;
                            $financialInfo['walletid'] = $wallet->id; 
                            $financialInfo['status'] = "NA";
                            
                            array_push($financial_info,$financialInfo);
                        }
                        
                        if($wallet->non_promotional_amount > 0)
                        {
                            $financialInfo['payment_method'] = 2;
                            $financialInfo['nonpromo'] = $wallet->promotional_amount;
                            $financialInfo['promo'] = 0;
                            $financialInfo['walletid'] = $wallet->id; 
                            $financialInfo['status'] = "NA";     
                            
                            array_push($financial_info,$financialInfo);
                        }
                    }
                    
                    if($booking->pg_txn_amount > 0)
                    {
                        if($paymentMethods[0] == "RazorPayApp" || $paymentMethods[0] == "RazorPayAppiOS")
                        {
                            $financialInfo['payment_method'] = 3;
                            $financialInfo['amount'] = $booking->pg_txn_amount;
                            $financialInfo['payment_id'] = $booking->pg_txn_id;
                            $financialInfo['status'] = ($booking->pg_status==null)?"":$booking->pg_status;    
                            
                            array_push($financial_info,$financialInfo);   
                        }
                    }
                }
                else{
                    
                    if(in_array($paymentMethods[0],array("RazorPayApp","RazorPay","RazorPayAppiOS")))
                    {
                        $financialInfo['payment_method'] = 3;
                        $financialInfo['status'] = ($booking->pg_status==null)?"":$booking->pg_status;
                    }
                    elseif(in_array($paymentMethods[0],array("PayTM","paytm")))
                    {
                        $financialInfo['payment_method'] = 4;
                        $financialInfo['status'] = ($booking->pg_status==null)?"":$booking->pg_status;
                    }
                    elseif($booking->payment_method == "PayUMoney")
                    {
                        $financialInfo['payment_method'] = 5;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "PayZapp")
                    {
                        $financialInfo['payment_method'] = 6;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "Card")
                    {
                        $financialInfo['payment_method'] = 7;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "PayPal")
                    {
                        $financialInfo['payment_method'] = 8;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "Cheque")
                    {
                        $financialInfo['payment_method'] = 9;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "Bank Transfer")
                    {
                        $financialInfo['payment_method'] = 10;
                        $financialInfo['status'] = "NA";
                    }
                    elseif($booking->payment_method == "Cash" && $booking->lead_source == "In-Store")
                    {
                        $financialInfo['payment_method'] = 11;
                        $financialInfo['status'] = "NA";
                    }
                    else
                    {
                        $financialInfo['payment_method'] = 1;
                        $financialInfo['status'] = "NA";
                    }
                    
                    
                    
                    $financialInfo['payment_id'] = ($booking->pg_txn_id==null)?"":$booking->pg_txn_id;
                    $financialInfo['amount'] = $booking->total_price;     
                            
                    array_push($financial_info,$financialInfo);   
                    
                }
                
                $notes = [];
                if($booking->note != "")
                {
                    $note['note'] = $booking->note;
                    
                    if($booking->assisted == 1)
                    {
                        if($booking->lead_source == "In-Store")
                        {
                            $note['nature'] = "Store creation notes";
                        }
                        else
                        {
                            $note['nature'] = "Customer Care creation notes";
                        }
                    }
                    else
                    {
                        $note['nature'] = "User creation notes";
                    }
                    
                    array_push($notes,$note);
                }
                if($booking->note != "")
                {
                    $note['note'] = $booking->note;
                    $note['nature'] = "Extra KM notes";
                    array_push($notes,$note);
                }
                if($booking->extra_km_notes != "")
                {
                    $note['note'] = $booking->extra_km_notes;
                    $note['nature'] = "Extra KM notes";
                    array_push($notes,$note);
                }
                if($booking->delivery_cancelled_notes != "")
                {
                    $note['note'] = $booking->delivery_cancelled_notes;
                    $note['nature'] = "Delivery cancellation notes";
                    array_push($notes,$note);
                }
                if($booking->returned_with_problems_note != "")
                {
                    $note['note'] = $booking->returned_with_problems_note;
                    $note['nature'] = "Returned with problem notes";
                    array_push($notes,$note);
                }
                if($booking->delivery_notes != "")
                {
                    $note['note'] = $booking->delivery_notes;
                    $note['nature'] = "Delivery notes";
                    array_push($notes,$note);
                }
                if($booking->backend_notes != "")
                {
                    $note['note'] = $booking->backend_notes;
                    $note['nature'] = "Backend notes";
                    array_push($notes,$note);
                }
                Self::createReservation($user,$createdByUser,$promo_code,$reservation_detail,$creation_mode,$current_state,$financial_info,$notes,$booking);
                
            }
            
        });
        
        return "done";
    }
    
    public function createReservation($user,$createdByUser,$promo_code,$reservation_detail,$creation_mode,$current_state,$financial_info,$notes,$booking)
    {
        
        
        $bookingHeader = BookingHeader::create([
            'id'=>$reservation_detail['id'],
            "user_id"=>$user->id,
            "ref_id"=>$reservation_detail['ref_id']
        ]);
        

        $reservationInfo = ReservationInfo::create([
            'id'=>$reservation_detail['id'],
            'booking_id'=>$bookingHeader->id,
            'bike_or_accessory'=>"B",
            'model_id'=>$reservation_detail['model_id'], 
            'area_id'=>$reservation_detail['area_id'], 
            'start_datetime'=>$reservation_detail['start_datetime'], 
            'end_datetime'=>$reservation_detail['end_datetime'],
            'user_id'=>$user->id,
            'creation_mode'=>$creation_mode,
            'current_state'=>$current_state,
            'created_by'=>$createdByUser->id,
            'created_at'=>$reservation_detail['created_at'],
            'KMlimit'=>$reservation_detail['KMlimit'],
            'weekday_hours'=>$reservation_detail['weekday_hours'],
            'weekend_hours'=>$reservation_detail['weekend_hours'],
            'total_hours'=>$reservation_detail['total_hours'],
            'res_lead_time'=>$reservation_detail['res_lead_time'],
            'fuel_included'=>$reservation_detail['fuel_included']
        ]);

        $startDateTimeCarbon = Carbon::parse($reservation_detail['start_datetime']);
        $endDateTimeCarbon = Carbon::parse($reservation_detail['end_datetime']);
        $aug1 = Carbon::parse("2017-08-01 00:00:00");
        if($endDateTimeCarbon->lte($aug1))
        {
            $taxPlus = 1.055;
            $tax = 0.055;
        }
        else
        {
            $taxPlus = 1.18;
            $tax = 0.18;
        }
        if($reservation_detail['wallet_amount_applied'] > 0)
        {
            $taxable_price = $reservation_detail['pg_txn_amount'];
            $tax_amount = round($taxable_price*$tax,2);
            $price_after_tax = $taxable_price+$tax_amount;
        }
        else
        {
            $taxable_price = $reservation_detail['total_price'];
            $tax_amount = round($taxable_price*$tax,2);
            $price_after_tax = $reservation_detail['total_price']+$tax_amount;            
        }
        
        

        $pricingInfo = PricingInfo::create([
            'bike_or_accessory'=>"B",
            'reservation_id'=>$reservationInfo->id,
            'pricing_id'=>0,
            'full_price'=>$reservation_detail['total_price'],
            'promo_discount'=>0,
            'price_after_discount'=>$reservation_detail['total_price'],
            'promo_code'=>$promo_code,
            'tax_id'=>1,
            'taxable_price'=>$taxable_price,
            'tax_amount'=>$tax_amount,
            'price_after_tax'=>$price_after_tax
        ]);


        foreach($financial_info as $financialInfo)
        {
            if($financialInfo['payment_method']==2)
            {
                if($financialInfo['promo']>0)
                {
                    FinancialInfo::create([
                        'booking_id'=>$bookingHeader->id,
                        'user_id'=>$user->id,
                        'debit_or_credit'=>"C",
                        'new_or_edit'=>"N",
                        'amount'=>$financialInfo['nonpromo'],
                        'payment_method'=>12,
                        'payment_id'=>$financialInfo['walletid'],
                        'payment_nature'=>1,
                        'status'=>"complete",
                        'created_by'=>$createdByUser->id
                    ]);
                }

                if($financialInfo['nonpromo']>0)
                {
                    FinancialInfo::create([
                        'booking_id'=>$bookingHeader->id,
                        'user_id'=>$user->id,
                        'debit_or_credit'=>"C",
                        'new_or_edit'=>"N",
                        'amount'=>$financialInfo['nonpromo'],
                        'payment_method'=>13,
                        'payment_id'=>$financialInfo['walletid'],
                        'payment_nature'=>1,
                        'status'=>"complete",
                        'created_by'=>$createdByUser->id
                    ]);
                }
            }
            else
            {
                $amountFinancialInfo = $financialInfo['amount'];
                FinancialInfo::create([
                    'booking_id'=>$bookingHeader->id,
                    'user_id'=>$user->id,
                    'debit_or_credit'=>"C",
                    'new_or_edit'=>"N",
                    'amount'=>$amountFinancialInfo,
                    'payment_method'=>$financialInfo['payment_method'],
                    'payment_id'=>$financialInfo['payment_id'],
                    'payment_nature'=>1,
                    'status'=>$financialInfo['status'],
                    'created_by'=>$createdByUser->id
                ]); 
            }
            
        }
        
        
      
        FinancialInfo::create([
            'booking_id'=>$bookingHeader->id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$reservation_detail['total_price']-$tax_amount,
            'payment_method'=>"na",
            'payment_id'=>"na",
            'payment_nature'=>2,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        FinancialInfo::create([
            'booking_id'=>$bookingHeader->id,
            'user_id'=>$user->id,
            'debit_or_credit'=>"D",
            'new_or_edit'=>"N",
            'amount'=>$tax_amount,
            'payment_method'=>"na",
            'payment_id'=>"na",
            'payment_nature'=>3,
            'status'=>"na",
            'created_by'=>$createdByUser->id
        ]);
        
        foreach($notes as $note)
        {
            NotesInfo::create([
                'booking_id'=>$bookingHeader->id,
                'nature'=>$note['nature'],
                'text'=>$note['note'],
                'created_by'=>$createdByUser->id
            ]);
        }
        
        if($booking->is_delivered == 1)
        {
            DeliveryInfo::create([
                'bike_or_accessory'=>"B",
                'reservation_id'=>$reservationInfo->id,
                'delivered_by'=>$booking->delivered_by,
                'delivered_at'=>$booking->delivered_at,
                'bike_id'=>$booking->bike_number_id,
                'starting_odo'=>$booking->start_odo,
                'created_by'=>$booking->delivered_by,
                'created_at'=>$booking->delivered_at
            ]);
        }
        
        if($booking->is_returned == 1)
        {
            ReturnInfo::create([
                'bike_or_accessory'=>"B",
                'reservation_id'=>$reservationInfo->id,
                'returned_by'=>$booking->returned_by,
                'returned_at'=>$booking->returned_at,
                'ending_odo'=>$booking->end_odo,
                'created_by'=>$booking->returned_by,
                'created_at'=>$booking->returned_at
            ]);
        }
        
    }
    
    
    public function cleanseDeletedEntriesInA2b()
    {
        $bookings = OneWayRentalBooking::orderBy('id','asc')->get();
        foreach($bookings as $booking)
        {
            $delete = false;
            
            if(!isset($booking->bike))
            {
                $delete =true;
            }
            if(!isset($booking->fromArea))
            {
                $delete =true;
            }
            if(!isset($booking->toArea))
            {
                $delete =true;
            }
            
            if($delete == true)
            {
                echo $booking->id."  -  ";
                OneWayRentalBooking::destroy($booking->id);
            }
        }
        return "done";
    }
    
    
    public function getUserBalance(Request $request)
    {
        $user = User::find($request->id);
        return "A2A - ".$user->getBalance()." - A2B - ".$user->getA2BBalance();
    }
    
    public function showMissingPricingInfo(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $bookings = ReservationInfo::where('created_at','>',$request->start_date)->where('created_at','<',$request->end_date)->with(['pricingInfo'])->get();
        
        foreach($bookings as $booking)
        {
            if(!$booking->pricingInfo)
                echo $booking->id."<br>";
        }
        
        return "done";
    }
    
    
    public function fixPricing(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        
        $bookings = ReservationInfo::where('created_at','<','2017-09-19 00:00:00')->with(['pricingInfo','bookingHeader','bookingHeader.financialInfos'])->get();
        
        $walletTxns = Wallet::where('nature',"Redeemed for a booking")->get()->keyBy('booking_id');
        
        foreach($bookings as $booking)
        {
            $totalWithTax = 0;
            $totalWithOutTax = 0;
            $totalTax = 0;
            $walletTxnPresent = false;
            
            foreach($booking->bookingHeader->financialInfos as $financialInfo)
            {
                if($financialInfo->payment_nature == 1)
                {
                    $totalWithTax += $financialInfo->amount;
                }
                
                
                if($financialInfo->payment_method == 12||$financialInfo->payment_method == 13||$financialInfo->payment_method == 2)
                {
                    if($financialInfo->payment_method == 12)
                        $promoCreditsGiven += $financialInfo->amount; 
                    $walletTxnPresent = true;
                }
            }
            
            $promoCreditsGiven = 0;
            $nonPromoCreditsGiven = 0;
            
            if(isset($walletTxns[$booking->id]) && !$walletTxnPresent)
            {
                if($walletTxns[$booking->id]->promotional_amount > 0)
                {
                    FinancialInfo::create([
                        'booking_id'=>$booking->id,
                        'user_id'=>$booking->user_id,
                        'debit_or_credit'=>"C",
                        'new_or_edit'=>"N",
                        'amount'=>$walletTxns[$booking->id]->promotional_amount,
                        'payment_method'=>12,
                        'payment_id'=>$walletTxns[$booking->id]->id,
                        'payment_nature'=>1,
                        'status'=>"paid",
                        'created_by'=>1
                    ]);
                    $promoCreditsGiven = $walletTxns[$booking->id]->promotional_amount;
                }
                
                if($walletTxns[$booking->id]->non_promotional_amount > 0)
                {
                    FinancialInfo::create([
                        'booking_id'=>$booking->id,
                        'user_id'=>$booking->user_id,
                        'debit_or_credit'=>"C",
                        'new_or_edit'=>"N",
                        'amount'=>$walletTxns[$booking->id]->non_promotional_amount,
                        'payment_method'=>13,
                        'payment_id'=>$walletTxns[$booking->id]->id,
                        'payment_nature'=>1,
                        'status'=>"paid",
                        'created_by'=>1
                    ]);
                    $nonPromoCreditsGiven = $walletTxns[$booking->id]->non_promotional_amount;
                }                
            }
            
            $endDateTimeCarbon = Carbon::parse($booking['created_at']);
            $aug1 = Carbon::parse("2017-08-01 00:00:00");
            if($endDateTimeCarbon->lte($aug1))
            {
                $taxPlus = 1.055;
                $tax = 0.055;
            }
            else
            {
                $taxPlus = 1.18;
                $tax = 0.18;
            }
            
            $totalWithTax = $totalWithTax + $promoCreditsGiven + $nonPromoCreditsGiven;
            
            if($promoCreditsGiven == 0)
            {
                $totalWithOutTax = round($totalWithTax/$taxPlus,2);
                $taxablePrice = $totalWithOutTax;
                $taxAmount = ($totalWithTax - $totalWithOutTax);
            }
            else
            {
                $totalWithOutTax = round((($totalWithTax - ($promoCreditsGiven*$tax))/(1+$tax)),2);
                $taxablePrice = $totalWithOutTax - $promoCreditsGiven;
                $taxAmount = ($totalWithTax - $totalWithOutTax);
            }
                        
            PricingInfo::where('reservation_id',$booking->id)->update([
                "full_price"=>$totalWithOutTax,
                "price_after_discount" => $totalWithOutTax,
                "taxable_price"=>$taxablePrice,
                "tax_amount" =>$taxAmount,
                "price_after_tax"=>($totalWithOutTax+$taxAmount)
            ]);
            
        }
        
        return "done";
                
    }

     public function fixMovement(Request $request)
    {
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');
        $currentDateTime = Carbon::now();
        $currentDateTime->minute = 0;
        $currentDateTime->second = 0;
        
        $movements = DB::table('movement_log')->where('end_datetime','>',$currentDateTime->toDateTimeString())->get();
        foreach($movements as $movement)
        {
            $endDateTimeCarbon = Carbon::parse($movement->end_datetime);
            $startDateTimeCarbon = Carbon::parse($movement->start_datetime);

           if($startDateTimeCarbon->diffInDays($endDateTimeCarbon)==env('FUTUREDAYS'))
            {
               $endDateTimeCarbon->addYears(31);
                DB::table('movement_log')->where('bike_number',$movement->bike_number)->where('created_at',$movement->created_at)->where('start_datetime',$movement->start_datetime)->update([
                "end_datetime"=>$endDateTimeCarbon
            ]);

            } 
        }
        
        return "done";

    }


    public function duke390bikefix()
    {  
        ini_set('max_execution_time', 0);  
        ini_set('memory_limit', '-1');  
        //BikeAvailabilityController::addOrDeleteBike($activebike->area_id,$activebike->model_id,"add",0,0);
        $bikelocation=Bike::where('id',626)->first();

        if($bikelocation->area_id!=43){
        //jayanager remove
        BikeAvailabilityController::addOrDeleteBike(1,21,"delete",0,0);
        //add in hebbal
         BikeAvailabilityController::addOrDeleteBike(43,21,"add",0,0);
        //Move bike to hebbal
         Bike::where('id',626)->update([
            'area_id'=>43
        ]);

     }

     echo "Done";
 }
    
    
    
}
