<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class VendorController extends AppController
{
    public function index(){
    	if(Auth::check())
    		return redirect('vendor/dashboard');
    	else
    		return redirect('vendor/login');
    }

    public function login(){
    	return view('vendor.login');
    }

    public function dashboard(){
    	return view('vendor.dashboard');
    }
}
