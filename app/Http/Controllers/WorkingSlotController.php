<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\WorkingSlot;
use App\Models\FromWorkingSlot;
use App\Models\ToWorkingSlot;
use App\Models\Slot;
use App\Models\Area;
use App\Models\Availability;
use Carbon\Carbon;
use DB;


class WorkingSlotController extends AppController
{
    
    public function getWorkingSlots(Request $request){
        $slotNo = date('N', strtotime($request->date));
        $workingSlot = WorkingSlot::where('day_of_week', $slotNo)->get();
        $slotIdsArr = explode(',', $workingSlot[0]['slots']);
        $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
        return $availableSlots;
    }
    public function dayOffs(Request $request){
        $city_id = $request->city_id;
        $day_offs = DB::table('day_offs')
                              ->where('city_id', $city_id)
                              ->first();
        if(!$day_offs)
        {
            $day_offs_ids = explode(',',$day_offs->day_offs);
        }
        else
        {
            $day_offs_ids = [];
        }
        return $day_offs_ids;
    }

    public function getFromWorkingSlots(Request $request){

        $currentDateObj = Carbon::today();
        $cityId = $request->city_id;
        
        $dateArray = explode('-',$request->date);

        $dateCarbon = Carbon::create($dateArray[0], (int)$dateArray[1], $dateArray[2]);

        
        if(!$cityId)
        {
          $cityId = 1;
        }

        if($dateCarbon->toDateString() != $currentDateObj->toDateString())
        {
            $slotNo = date('N', strtotime($dateCarbon->toDateString()));  

            $holiday = DB::table('holiday')->where('date',$dateCarbon->toDateString())->first();

            if(!$holiday)
            {

            }
            else
            {
                $slotNo = 7;
            }

            $dateCarbon = Carbon::parse($dateCarbon->toDateString());

            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $slotNo = 1;
            }

            $workingSlot = FromWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->get();


            if(empty((array) $workingSlot))
            {
                $slotIdsArr = [];
            }
            else
            {
                $slotIdsArr = explode(',', $workingSlot[0]['slots']);
            }
            $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = array('slots'=>[]);

                return $availableSlotsEmpty;
            }
        }
        else
        {
            $slotNo = date('N', strtotime($dateCarbon->toDateString()));  

            $holiday = DB::table('holiday')->where('date',$dateCarbon->toDateString())->first();

            if(!$holiday)
            {

            }
            else
            {
                $slotNo = 7;
            }

            $dateCarbon = Carbon::parse($dateCarbon->toDateString());

            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $slotNo = 1;
            }

            $workingSlot = FromWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->first();
            if(empty((array) $workingSlot))
            {
                $slotIdsArr = [];
            }
            else
            {


                $slotIdsArr = explode(',', $workingSlot['slots']);
            }
            //$slotIdsArrCopy = $slotIdsArr->getArrayCopy()        
            foreach ($slotIdsArr as $slotId)
            {
              $slot = Slot::where('id', $slotId)->first(); 
              $date = Carbon::parse("today " . $slot['start_time']);
              if($date < Carbon::now())
              {
                  if (($key = array_search($slotId, $slotIdsArr)) !== false) {
                        unset($slotIdsArr[$key]);
                    }
              }

            }
            $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();  
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = array('slots'=>[]);
                return $availableSlotsEmpty;
            }        
        }
    }
    public function getToWorkingSlots(Request $request){
        
        $dateArray = explode('-',$request->date);

        $dateCarbon = Carbon::create($dateArray[0], (int)$dateArray[1], $dateArray[2]);

        $slotNo = date('N', strtotime($dateCarbon->toDateString()));

        $holiday = DB::table('holiday')->where('date',Carbon::parse(($dateCarbon->toDateString()))->format('Y-m-d'))->first();


        if(!$holiday)
        {

        }
        else
        {
            $slotNo = 7;
        }


        $diff = $dateCarbon->diffInHours(Carbon::now());

        if($diff<24)
        {
            $slotNo = 1;
        }

        $cityId = $request->city_id;

          if(!$cityId)
          {
              $cityId = 1;
          }
        $workingSlot = ToWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->first();


        if(empty((array) $workingSlot))
        {
            $slotIdsArr = [];
        }
        else
        {
            $slotIdsArr = explode(',', $workingSlot['slots']);

            if($request->start_date!=null && $request->start_date!="")
            {
                if($request->start_date == $dateCarbon->toDateString())
                {
                    $slotIdsArrForSameDate = [];
                    foreach($slotIdsArr as $slot)
                    {
                        if(substr($request->start_time,0,2)<$slot)
                        {
                            array_push($slotIdsArrForSameDate,$slot);
                        }
                    }
                    $slotIdsArr = $slotIdsArrForSameDate;
                }

            }

        }
        $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
        $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

        if ($availableSlotsCount!=0)
            return $availableSlots;
        else
        {
            $availableSlotsEmpty = array('slots'=>[]);
            return $availableSlotsEmpty;
        }
    }
    
    
    public function getFromWorkingSlotsWeb(Request $request){
        $currentDateObj = Carbon::today();
        $cityId = $request->city_id;
          if(!$cityId)
          {
              $cityId = 1;
          }

        if($request->date != $currentDateObj->toDateString())
        {
            $slotNo = date('N', strtotime($request->date));  
            $holiday = DB::table('holiday')->where('date',$request->date)->first();

            if(!$holiday)
            {

            }
            else
            {
                $slotNo = 7;
            }

            $dateCarbon = Carbon::parse($request->date);

            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $slotNo = 1;
            }

            $workingSlot = FromWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->get();


            if(empty((array) $workingSlot))
            {
                $slotIdsArr = [];
            }
            else
            {
                $slotIdsArr = explode(',', $workingSlot[0]['slots']);
            }
            $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = [];

                return $availableSlotsEmpty;
            }
        }
        else
        {
            $slotNo = date('N', strtotime($request->date));  

            $holiday = DB::table('holiday')->where('date',$request->date)->first();

            if(!$holiday)
            {

            }
            else
            {
                $slotNo = 7;
            }
            $dateCarbon = Carbon::parse($request->date);

            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $slotNo = 1;
            }

            $workingSlot = FromWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->first();
            if(empty((array) $workingSlot))
            {
                $slotIdsArr = [];
            }
            else
            {
                $slotIdsArr = explode(',', $workingSlot['slots']);
            }
            //$slotIdsArrCopy = $slotIdsArr->getArrayCopy();
            foreach ($slotIdsArr as $slotId)
            {
              $slot = Slot::where('id', $slotId)->first(); 
              $date = Carbon::parse("today " . $slot['start_time']);
              if($date < Carbon::now())
              {
                  if (($key = array_search($slotId, $slotIdsArr)) !== false) {
                        unset($slotIdsArr[$key]);
                    }
              }

            }
            $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();  
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = [];
                return $availableSlotsEmpty;
            }        
        }
    }
    
    public function getToWorkingSlotsWeb(Request $request)
    {
        $slotNo = date('N', strtotime($request->date));

        $holiday = DB::table('holiday')->where('date',Carbon::parse(($request->date))->format('Y-m-d'))->first();

        if(!$holiday)
        {

        }
        else
        {
            $slotNo = 7;
        }

        $dateCarbon = Carbon::parse($request->date);

        $diff = $dateCarbon->diffInHours(Carbon::now());

        if($diff<24)
        {
            $slotNo = 1;
        }

        $cityId = $request->city_id;

          if(!$cityId)
          {
              $cityId = 1;
          }
        $workingSlot = ToWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->first();


        if(empty((array) $workingSlot))
        {
            $slotIdsArr = [];
        }
        else
        {
            $slotIdsArr = explode(',', $workingSlot['slots']);

        }
        $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
        $availableSlotsCount = Slot::whereIn('id', $slotIdsArr)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = [];
                return $availableSlotsEmpty;
            }
      }

    
    
    public function getFromWorkingSlotsWebForArea(Request $request)
    {
        
        $currentDateObj = Carbon::today();
        $cityId = $request->city_id;
        if(!$cityId)
        {
            $cityId = 1;
        }
        
        $dateArray = explode('-',$request->date);

        $dateCarbon = Carbon::create($dateArray[0], (int)$dateArray[1], $dateArray[2]);

        if($dateCarbon->toDateString() != $currentDateObj->toDateString())
        {
            
            $slotNo = date('N', strtotime($dateCarbon->toDateString()));  
            $holiday = DB::table('holiday')->where('date',$dateCarbon->toDateString())->first();

            if(!$holiday)
            {

            }
            else
            {
                $slotNo = 7;
            }
            
            
            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $slotNo = 1;
            }

            $workingSlot = FromWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->get();
            

            if(empty((array) $workingSlot))
            {
                $slotIdsArr = [];
            }
            else
            {
                $slotIdsArr = explode(',', $workingSlot[0]['slots']);
            }
            
            $area = Area::where('id',$request->area_id)->first();
            $hoursOpen = json_decode($area->hours_open);
            
            $slotIdsArrForArea = [];
            
            foreach($hoursOpen[$dateCarbon->dayOfWeek] as $hour)
            {
                array_push($slotIdsArrForArea,$hour+1);
            }
            
            $availableSlots = Slot::whereIn('id', $slotIdsArrForArea)->get();
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArrForArea)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = [];

                return $availableSlotsEmpty;
            }
        }
        else
        {
            $slotNo = date('N', strtotime($dateCarbon->toDateString())); 
            
            $now = Carbon::now();

            $holiday = DB::table('holiday')->where('date',$dateCarbon->toDateString())->first();

            if(!$holiday)
            {
                $dayOfWeek = $dateCarbon->dayOfWeek;
            }
            else
            {
                $dayOfWeek = 0;
            }
            
            $dateCarbon = Carbon::parse($dateCarbon->toDateString());

            $diff = $dateCarbon->diffInHours(Carbon::now());

            if($diff<24)
            {
                $dayOfWeek = 1;
            }
            else
            {
                $dayOfWeek = $dateCarbon->dayOfWeek;
            }
            
            $area = Area::where('id',$request->area_id)->first();
            $hoursOpen = json_decode($area->hours_open);
            
            $slotIdsArrForArea = [];
            
            foreach($hoursOpen[$dayOfWeek] as $hour)
            {
                if($hour>=$now->hour)
                    array_push($slotIdsArrForArea,$hour+1);
            }
            
            $availableSlots = Slot::whereIn('id', $slotIdsArrForArea)->get();  
            $availableSlotsCount = Slot::whereIn('id', $slotIdsArrForArea)->count();

            if ($availableSlotsCount!=0)
                return $availableSlots;
            else
            {
                $availableSlotsEmpty = [];
                return $availableSlotsEmpty;
            }        
        }
    }
    public function getToWorkingSlotsWebForArea(Request $request)
    {
        $dateArray = explode('-',$request->date);
        
        if(!(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2])))
        {
            $availableSlotsEmpty = [];
            return $availableSlotsEmpty;
        }

        $dateCarbon = Carbon::create($dateArray[0], (int)$dateArray[1], $dateArray[2]);

        $slotNo = date('N', strtotime($dateCarbon->toDateString()));

        $holiday = DB::table('holiday')->where('date',Carbon::parse(($dateCarbon->toDateString()))->format('Y-m-d'))->first();

        if(!$holiday)
        {
            $dayOfWeek = $dateCarbon->dayOfWeek;
        }
        else
        {
            $dayOfWeek = 0;
        }


        $diff = $dateCarbon->diffInHours(Carbon::now());

        if($diff<24)
        {
            $dayOfWeek = 1;
        }
        else
        {
            $dayOfWeek = $dateCarbon->dayOfWeek;
        }

        $cityId = $request->city_id;

        if(!$cityId)
        {
            $cityId = 1;
        }
        
        $workingSlot = ToWorkingSlot::where('day_of_week', $slotNo)->where('city_id',$cityId)->first();


        if(empty((array) $workingSlot))
        {
            $slotIdsArr = [];
        }
        else
        {
            $slotIdsArr = explode(',', $workingSlot['slots']);

        }
        
        $area = Area::where('id',$request->area_id)->first();
        $hoursOpen = json_decode($area->hours_open);

        $slotIdsArrForArea = [];

        foreach($hoursOpen[$dayOfWeek] as $hour)
        {
            array_push($slotIdsArrForArea,$hour+1);
        }
        
        $availableSlots = Slot::whereIn('id', $slotIdsArrForArea)->get();
        $availableSlotsCount = Slot::whereIn('id', $slotIdsArrForArea)->count();

        if ($availableSlotsCount!=0)
            return $availableSlots;
        else
        {
            $availableSlotsEmpty = [];
            return $availableSlotsEmpty;
        }
        
    }

  public static function getAvailableSlotsIdsStart($date, $booked_slot_id){
    $slotNo = date('N', strtotime($date));
    $workingSlot = WorkingSlot::where('day_of_week', $slotNo)->get();
    $slotIdsArr = explode(',', $workingSlot[0]['slots']);
    $availableSlots['slots'] = Slot::whereIn('id', $slotIdsArr)->get()->lists('id');
    $key = array_search($booked_slot_id, $availableSlots['slots']->toArray());
    // NO BUFFER, BIKE CAN BE RETURNED AT THE BOOKING TIME
    $noAvailableSlotsIds = array_slice($availableSlots['slots']->toArray(), $key); 
    $availableSlotsIds = array_diff($availableSlots['slots']->toArray(), $noAvailableSlotsIds);
    
    $availableSlotsIdsStr = implode(',', $availableSlotsIds);
    return $availableSlotsIdsStr;
  }

  public static function getAvailableSlotsIdsEnd($date, $booked_slot_id){
    $slotNo = date('N', strtotime($date));
    $workingSlot = WorkingSlot::where('day_of_week', $slotNo)->get();
    $slotIdsArr = explode(',', $workingSlot[0]['slots']);
    $availableSlots['slots'] = Slot::whereIn('id', $slotIdsArr)->get()->lists('id');
    $key = array_search($booked_slot_id, $availableSlots['slots']->toArray());
    $availableSlotsIds = array_slice($availableSlots['slots']->toArray(), $key); // removed + 1
    //$availableSlotsIds = array_diff($noAvailableSlotsIds, $availableSlots['slots']->toArray());
    $availableSlotsIdsStr = implode(',', $availableSlotsIds);
    return $availableSlotsIdsStr;
  }

  public static function getAvailableSlotBookedDate($date, $bike_id){
    $booking = Availability::where('bike_id', $bike_id)->where('date', $date)->first();
      if(!$booking){
        $dispatcher = app('Dingo\Api\Dispatcher');
        $dateSlots = $dispatcher->with(['date' => $date])->get('working-hours');
        return reset($dateSlots);
      }
      $slotIdsArr = explode(',', $booking['slots']);
      $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
      return reset($availableSlots);
  }

  // public function getAvailableSlots(Request $request){
  //   $date  = $request->date;
  //   $models = $dispatcher->with(['start_date' => $startDate->toDateString(), 'end_date' => $endDate->toDateString(), 
  //                           'start_time' => $startTime, 'end_time' => $endTime])
  //                           ->get('from-availability-calendar');
  // }

}
