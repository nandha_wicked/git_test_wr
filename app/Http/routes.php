<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('test', function(){
	//$booking = \App\Models\Booking::find(2175);
	//return $booking->validateBooking();
});
//$dispatcher = app('Dingo\Api\Dispatcher');



$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['middleware' => 'api.log'],function($api){
        
        $api->get('cities', '\App\Http\Controllers\CityController@cities');
        $api->get('cities/{id}', '\App\Http\Controllers\CityController@show');
        $api->get('cities/{id}/models', 'App\Http\Controllers\FrontViewController@getModelsAMHForApp');
        
        
        //User Apis
        
        $api->post('signin', 'App\Http\Controllers\UserController@signin');
        $api->post('signup', 'App\Http\Controllers\UserController@signup');
        
        $api->post('verify-otp', 'App\Http\Controllers\UserController@verifyOTP');
        $api->post('resend-otp', 'App\Http\Controllers\UserController@resendOTP');
        
        $api->post('signupFromAdmin', 'App\Http\Controllers\UserController@signupFromAdmin');
        $api->get('forgot-password', 'App\Http\Controllers\UserController@forgotPassword');
        $api->post('reset-password', 'App\Http\Controllers\UserController@resetPassword');
        $api->group(['middleware' => 'api.auth' ], function($api){
            // $api->get('user', 'App\Http\Controllers\UserController@getUserByToken');
            $api->get('me/bookings', '\App\Http\Controllers\UserController@getUpcomingBookingDetails');
            $api->get('me/subscription', '\App\Http\Controllers\UserController@getSubscriptionDetails');
            $api->get('me/events', '\App\Http\Controllers\UserController@getUpcomingEventsDetails');
            $api->get('me/profile', '\App\Http\Controllers\UserController@getProfile');
            $api->post('me/profile', '\App\Http\Controllers\UserController@update');
            $api->post('me/profile-image', '\App\Http\Controllers\UserController@uploadImage');
            $api->post('bikations/{id}/reviews', '\App\Http\Controllers\BikationController@postReview');
            $api->get('me/referral_code', '\App\Http\Controllers\UserController@getReferralCode');
            $api->get('me/booking-history', '\App\Http\Controllers\UserController@getBookingHistory');
            
        });
        
        $api->get('wallet/check_balance','App\Http\Controllers\WalletController@checkWalletBalance');
        $api->post('wallet/apply_amount','App\Http\Controllers\WalletController@applyWalletAmount');
        $api->get('wallet/function_apply_amount','App\Http\Controllers\WalletController@applyWalletAmountFunction');
        $api->post('wallet/add_amount','App\Http\Controllers\WalletController@addWalletApi');
        $api->get('wallet/add_amount_int','App\Http\Controllers\WalletController@addWalletApi');
        $api->get('wallet/add_amount_int_new','App\Http\Controllers\WalletController@addWalletApiNew');
        
        
        
        
        
        $api->get('working-hours', '\App\Http\Controllers\WorkingSlotController@getWorkingSlots');
        $api->get('from-working-hours', '\App\Http\Controllers\WorkingSlotController@getFromWorkingSlots');	
        $api->get('to-working-hours', '\App\Http\Controllers\WorkingSlotController@getToWorkingSlots');
        $api->get('from-working-hours-web', '\App\Http\Controllers\WorkingSlotController@getFromWorkingSlotsWeb');	
        $api->get('to-working-hours-web', '\App\Http\Controllers\WorkingSlotController@getToWorkingSlotsWeb');
        
        
        
        $api->get('from-working-hours-for-area', '\App\Http\Controllers\WorkingSlotController@getFromWorkingSlotsWebForArea');	
        $api->get('to-working-hours-for-area', '\App\Http\Controllers\WorkingSlotController@getToWorkingSlotsWebForArea');
        
        $api->get('get-store-open-status', '\App\Http\Controllers\AreaController@isAreaOpen');

        $api->get('day-offs', '\App\Http\Controllers\WorkingSlotController@dayOffs');


        $api->get('from-availability-calendar', '\App\Http\Controllers\AMHAvailabilityController@fromAvailabilityCalendarApp');
        $api->get('from-availability-calendar-web', '\App\Http\Controllers\AMHAvailabilityController@fromAvailabilityCalendarApp');
        $api->get('to-availability-calendar', '\App\Http\Controllers\AMHAvailabilityController@toAvailabilityCalendarApp');
        $api->get('check-availability', '\App\Http\Controllers\AMHAvailabilityController@checkAvailability');
        $api->get('check-capacity', '\App\Http\Controllers\AMHAvailabilityController@checkCapacity');
        $api->get('check-availabilityMobile', '\App\Http\Controllers\AvailabilityController@getAvailableBikeMobile');
        $api->group(['middleware' => 'auth' ], function($api){ 
            $api->get('bookingsweb', '\App\Http\Controllers\BookingCreationController@createServiceBlock');
            $api->put('bookingsweb/{id}', '\App\Http\Controllers\BookingController@update');
            $api->post('unblock327e5gbach5FFD','\App\Http\Controllers\BookingController@unblock');

        });
        

        $api->group(['middleware' => 'api.auth' ], function($api){ 
            $api->post('bookings', '\App\Http\Controllers\BookingCreationController@paymentApp');
            $api->put('bookings/{id}', '\App\Http\Controllers\BookingController@update');
            $api->post('app/cities/{id}/models', 'App\Http\Controllers\FrontViewController@getModelsAMHForApp');
            $api->post('sent-transaction','\App\Http\Controllers\ApiController@sentTransaction');
            $api->post('user_document/upload', '\App\Http\Controllers\EdituserController@upload_document_app');
            $api->post('user_document/delete', '\App\Http\Controllers\EdituserController@delete_document_app');
            $api->post('get_user_document_status', '\App\Http\Controllers\EdituserController@get_user_document_status');
            
            //Push Notification
            $api->post('push_notification_token', '\App\Http\Controllers\ApiController@pushNotification');

            
        });
        $api->group(['middleware' => 'auth' ], function($api){ 
            $api->post('bookings/test', '\App\Http\Controllers\BookingController@createTest');
            $api->put('bookings/{id}/test', '\App\Http\Controllers\BookingController@updateTest');
            $api->post('getBookingDetailsFromId', '\App\Http\Controllers\ApiController@getBookingDetailsFromId');
        });

        $api->get('cancelledEnquiries/add', '\App\Http\Controllers\EnquiryController@cancelledAdd');

        $api->get('bookings/total-price', '\App\Http\Controllers\PriceController@getTotalPrice');

        $api->post('bikerPlanSubscription', '\App\Http\Controllers\BikerPlanController@create');

        $api->get('updateParseFromLastId', '\App\Http\Controllers\BookingController@updateParse');
        $api->get('bikations', '\App\Http\Controllers\BikationController@getBikationList');
        $api->get('bikations/{id}', '\App\Http\Controllers\BikationController@show');
        $api->post('bikation-bookings', '\App\Http\Controllers\BikationController@create');

        $api->get('bookings/price-and-check-availability', '\App\Http\Controllers\PriceController@getPriceAndcheckAvailability');

        $api->get('filter/bike-count-vendor', '\App\Http\Controllers\BikeFilterController@getBikeCountVendor');
        $api->get('filter/bookings-vendor', '\App\Http\Controllers\BikeFilterController@getBookingsVendor');

        $api->get('bookingswebBglojd76', '\App\Http\Controllers\BookingCreationController@createServiceBlock');



        $api->get('filter/bookings-partner', '\App\Http\Controllers\BikeFilterController@getBookingsPartner');

        //$api->get('filter/bike-count', '\App\Http\Controllers\BikeFilterController@getBikeCount');
        $api->get('filter/bookings', '\App\Http\Controllers\BikeFilterController@getBookings');

        $api->get('filter/bike-count', '\App\Http\Controllers\AMHAvailabilityController@checkAvailabilityByArea');

        $api->get('urls', '\App\Http\Controllers\AppController@getUrls');
        $api->get('models/gallery-images/{id}', '\App\Http\Controllers\BikeModelController@getGalleryImages');

        $api->get('accessories/find', '\App\Http\Controllers\AccessoryController@index');
        $api->get('accessories/{id}', '\App\Http\Controllers\AccessoryController@show');

        // PromoCode Apis
        $api->get('promocodes', '\App\Http\Controllers\PromoCodeController@getAll');
        $api->get('promocodes/apply', '\App\Http\Controllers\PromoCodeController@applyPromoCode');
        $api->post('promocodes/delete/{id}', '\App\Http\Controllers\PromoCodeController@destroy');
        $api->get('promocodes/{id}', '\App\Http\Controllers\PromoCodeController@show');
        $api->get('is_available', '\App\Http\Controllers\PriceController@isBikeAvailable');

        //ERP APIs
        $api->get('unblock327e5gbach5FFD','\App\Http\Controllers\BookingController@unblock');
        $api->get('getBikeNumbersForModel', '\App\Http\Controllers\ApiController@getBikeNumbersForModel');
        $api->get('get-bike-numbers-for-model', '\App\Http\Controllers\ApiController@getBikeNumbersForAreaModel');
        $api->get('getBikeNumbersForModelAdd', '\App\Http\Controllers\ApiController@getBikeNumbersForModelAdd');
        $api->get('get-bike-numbers-for-booking', '\App\Http\Controllers\ApiController@getBikeNumbersForBooking');
        $api->post('apply-bike-numbers-for-booking', '\App\Http\Controllers\ApiController@applyBikeNumbersForBooking');
        $api->get('get-bike-numbers-html-for-booking', '\App\Http\Controllers\ApiController@getBikeNumbersHTMLForBooking');
        
        $api->get('get-extra-amount-for-return', '\App\Http\Controllers\ErpController@getExtraAmount');
        
        $api->get('getBikeNumbersForModelEdit', '\App\Http\Controllers\ApiController@getBikeNumbersForModelEdit');

        //Report APIs

        $api->get('getReportDataJson', '\App\Http\Controllers\ApiController@getReportData');
        $api->get('getReportChart', '\App\Http\Controllers\ApiController@getReportChart');
        $api->get('getReportDataFromDB', '\App\Http\Controllers\ApiController@getReportDataFromDB');
        $api->get('getReportDataEveryDay', '\App\Http\Controllers\ApiController@getReportDataEveryDay');

        //Ops Report APIs
        $api->get('getOpsReportDataEveryDay', '\App\Http\Controllers\ErpController@getOpsReportDataEveryDay');


        //Ops Email
        $api->get('sendOpsEmail', '\App\Http\Controllers\ErpController@sendOpsEmail');
        
        //Documents Email and SMS
        $api->get('send-document-sms', '\App\Http\Controllers\BookingController@sendDocumentSMSToCustomers');

        //Create Zip
        $api->get('create_zip', '\App\Http\Controllers\MigrationController@create_zip');

        //ERP APIs
        $api->get('getModelPricesForDates', '\App\Http\Controllers\ErpController@getModelPricesForDates');
        $api->get('getAreaPricesForDates', '\App\Http\Controllers\ErpController@getAreaPricesForDates');

        //OPs team APP APIs

        $api->group(['middleware' => 'auth.erpapp' ], function($api){ 

            $api->get('erp/get_area_dropdown', '\App\Http\Controllers\ErpController@get_area_dropdown');
            $api->get('erp/delivery_today', '\App\Http\Controllers\ErpController@delivery_today');
            $api->get('erp/delivery_tomorrow', '\App\Http\Controllers\ErpController@delivery_tomorrow');
            $api->get('erp/return_today', '\App\Http\Controllers\ErpController@return_today');
            $api->get('erp/return_tomorrow', '\App\Http\Controllers\ErpController@return_tomorrow');
            $api->get('erp/delivery_details', '\App\Http\Controllers\ErpController@reservationDeliveryDetails');
            $api->get('erp/return_details', '\App\Http\Controllers\ErpController@reservationReturnDetails');
            $api->get('erp/movement', '\App\Http\Controllers\ErpController@movement');
            $api->post('erp/deliver/{id}','\App\Http\Controllers\ErpController@deliverReservation');
            $api->post('erp/deliver-cancel/{id}','\App\Http\Controllers\ErpController@deliver_cancel');
            $api->post('erp/return/{id}','\App\Http\Controllers\ErpController@returnReservation');
            $api->post('erp/walkin','\App\Http\Controllers\BookingCreationController@paymentErpAdmin');
            $api->post('erp/apply-bike-numbers-for-booking-app', '\App\Http\Controllers\ApiController@applyBikeNumbersForBookingApp');
            $api->get('erp/get-bike-numbers-for-booking-app', '\App\Http\Controllers\ApiController@getBikeNumbersForBookingApp');
            $api->post('erp/apply-bike-numbers-for-booking', '\App\Http\Controllers\ApiController@applyBikeNumbersForBooking');
            $api->get('erp/get-extra-amount', '\App\Http\Controllers\ErpController@getExtraAmount');
            $api->post('erp/submit-invoice-payment', '\App\Http\Controllers\ErpController@submitInvoicePayment');
            $api->get('erp/wallet/check-balance','App\Http\Controllers\WalletController@checkWalletBalanceErp');
            
        });


        //Front Page API
        $api->get('checkAvailabilityByCityForFrontPage', '\App\Http\Controllers\AMHAvailabilityController@checkAvailabilityByCityForFrontPage');
        
        //SMS Apis
        $api->get('receive_sms', '\App\Http\Controllers\ApiController@receiveSMS');
        $api->get('receive_sms_api', '\App\Http\Controllers\ApiController@receiveSMSAPI');
        
        //Areas with Model
        
        $api->get('get-areas-with-models', '\App\Http\Controllers\ApiController@getAreasWithModels');
        
        
        
        //Accessories Apis
        
        $api->get('getAccessoriesTextIdForModelSize', '\App\Http\Controllers\ApiController@getAccessoriesTextIdForModelSize');
        $api->get('check-accessories-availability', '\App\Http\Controllers\AccessoriesAvailabilityController@checkAvailability');
		$api->get('check-accessories-availability-for-area-with-dates', '\App\Http\Controllers\AccessoriesAvailabilityController@checkAvailabilityForAreaWithDates');
        $api->get('accessories-total-price', '\App\Http\Controllers\AccessoriesPriceController@getTotalPrice');
                
        
        
        $api->get('test-api-1', '\App\Http\Controllers\MigrationController@testApi1');
        $api->post('test-api-1-details', '\App\Http\Controllers\MigrationController@testApi1Details');
        $api->get('test-api-2', '\App\Http\Controllers\MigrationController@testApi2');



        // One Way Rentals APIs
        $api->get('get-list-of-locations', '\App\Http\Controllers\OneWayRentalController@getOneWayRentalLocations');
        
        $api->get('a2b-from-location', '\App\Http\Controllers\OneWayRentalController@fromLocationForLatLong');
        $api->get('a2b-to-location', '\App\Http\Controllers\OneWayRentalController@toLocationForLatLong');
        
        $api->get('get-models-prices-one-way-trip', '\App\Http\Controllers\OneWayRentalController@getOneWayRentalModelsAndPrices');
        $api->get('get-models-prices-one-way-trip-with-latlong', '\App\Http\Controllers\OneWayRentalController@getOneWayRentalModelsAndPricesForLatLong');

        //OTp for mahamastaka
        $api->post('a2b/maha/signin_generateotp','\App\Http\Controllers\UserController@generateotp_signin');
        $api->post('a2b/maha/signin_verify-otp','App\Http\Controllers\UserController@signin_verifyOTP');


        $api->group(['middleware' => 'auth' ], function($api){ 
            $api->post('one-way-rentals-booking', '\App\Http\Controllers\OneWayRentalController@createOneWayRentalBooking');
            $api->post('one-way-rentals-booking-with-latlong', '\App\Http\Controllers\OneWayRentalController@createOneWayRentalBookingForLatLong');
            $api->post('one-way-rentals-promocode/apply', '\App\Http\Controllers\OneWayRentalController@applyOneWayRentalPromoCode');

        });
        
        //Tutorial Images
        
        $api->get('get-tutorial-images-for-a2b', '\App\Http\Controllers\AppLinksController@getTutorialImagesForA2b');
        
        //Questionnaire Apis
        $api->get('get-purpose-list-for-questionnaire', '\App\Http\Controllers\OneWayRentalController@getPurposeListForQuestionnaire');
        $api->post('questionnaire-response', '\App\Http\Controllers\OneWayRentalController@questionnaireResponse');
        $api->get('get-elements-for-questionnaire-share', '\App\Http\Controllers\OneWayRentalController@questionnaireShareElements');
        $api->get('get-text-for-questionnaire-share', '\App\Http\Controllers\OneWayRentalController@questionnaireShareText');

		
    });
    
    //Get Notification Elements
    $api->get('getNotificationElements', '\App\Http\Controllers\AppLinksController@getNotificationElements');
    $api->get('getTPOffers', '\App\Http\Controllers\AppLinksController@getTPOffers');
    
    $api->get('getElementsForIndexType', '\App\Http\Controllers\MigrationController@getElementsForIndexType');
    $api->post('tp/booking','\App\Http\Controllers\BookingController@tpBooking');
    
    //FOP APIs
    $api->get('get-earn-info-elements', '\App\Http\Controllers\FopController@getEarnInfoElements');
    $api->post('post-fop-enquiry-form', '\App\Http\Controllers\FopController@postEnquiryForm');
    
    //App Version Check
    $api->get('check-app-version','\App\Http\Controllers\AppLinksController@appVersionCheck');
   
    //Feedback 
    $api->post('feedback/submit','\App\Http\Controllers\AppLinksController@feedbackSubmit');
   
    
    //Reservation APIs
    $api->group(['middleware' => 'api.auth' ], function($api){ 
        $api->post('booking/new', '\App\Http\Controllers\ReservationController@createBookingApi');
        $api->post('booking/edit', '\App\Http\Controllers\ReservationController@editReservation');
        $api->post('booking/delete', '\App\Http\Controllers\ReservationController@deleteReservation');
    });
    
    
    
    //A2B Ops APP APIs
    $api->group(['middleware' => 'auth.erpapp' ], function($api){ 
        $api->get('a2b/home_screen_list', '\App\Http\Controllers\OneWayRentalOpsController@getA2BHomeScreenList');

        $api->get('a2b/details', '\App\Http\Controllers\OneWayRentalOpsController@getA2Bdetail');

        $api->get('a2b/assign_myself','\App\Http\Controllers\OneWayRentalOpsController@assignSupervisor');

        $api->post('a2b/cancelbooking','\App\Http\Controllers\OneWayRentalOpsController@cancelOneWayRentalBooking');

        $api->post('a2b/deliver','\App\Http\Controllers\OneWayRentalOpsController@deliverBike');
        $api->post('a2b/return','\App\Http\Controllers\OneWayRentalOpsController@returnBike');


    });
    
    
});

//wpp
Route::get('wpp','FrontPagesController@wpp');
Route::get('wpp-more','FrontPagesController@wppMore');
Route::post('wpp-add', 'WppController@addWppEnquiry');

// Front Site
Route::get('/','FrontPagesController@home');
Route::get('about-us','FrontPagesController@aboutUs');
Route::get('contact-us','FrontPagesController@contactUs');
Route::get('faqs','FrontPagesController@faq');
Route::get('reviews','FrontPagesController@reviews');
Route::get('bike/{bike_name}','FrontPagesController@bikes');
Route::get('booking','FrontPagesController@booking');
Route::get('list-my-bike','FrontPagesController@listMyBike');
Route::post('fetch-city','FrontPagesController@getSelectedCity');

// City Urls
Route::get('bangalore','FrontPagesController@getSelectedCityBangalore');
Route::get('mysore','FrontPagesController@getSelectedCityMysore');
Route::get('jaipur','FrontPagesController@getSelectedCityJaipur');
Route::get('udaipur','FrontPagesController@getSelectedCityUdaipur');
Route::get('hospet','FrontPagesController@getSelectedCityHampi');
Route::get('hampi','FrontPagesController@getSelectedCityHampi');
Route::get('belagavi','FrontPagesController@getSelectedCityBelagavi');
Route::get('bhuj','FrontPagesController@getSelectedCityBhuj');
Route::get('ahmedabad','FrontPagesController@getSelectedCityAhmedabad');
Route::get('jaisalmer','FrontPagesController@getSelectedCityJaisalmer');
Route::get('delhi','FrontPagesController@getSelectedCityDelhi');


Route::get('login','FrontPagesController@login');
Route::post('contact-enquiry/add', 'FrontPagesController@addContactEnquiry');
Route::post('faq-enquiry/add', 'FrontPagesController@addContactEnquiry');
//Route::post('faq-home-enquiry/add', 'FrontPagesController@addContactEnquiry');
Route::post('email-subscribe/add', 'FrontPagesController@addEmailSubscribers');
Route::post('booking/front-login', 'FrontPagesController@doLogin');
Route::post('booking/front-signup', 'FrontPagesController@doSignup');
Route::get('front-logout', 'FrontPagesController@doLogout');
Route::post('get-date', 'FrontPagesController@getDate');
Route::post('get-priceid', 'FrontPagesController@getPid');
Route::post('paymentRedirect', 'PaymentController@paymentRedirect');
Route::post('paymentBikerPlanRedirect', 'PaymentController@paymentBikerPlanRedirect');
Route::post('getModelBookedDates', 'FrontPagesController@getModelBookedDates');
Route::post('getSelectedMonthModelBookedDates', 'FrontPagesController@getSelectedMonthModelBookedDates');
Route::post('booking/accessories', 'FrontPagesController@bookingShowAccessories');
Route::get('get-totalprice/{from_date}/{to_date}/{price_id}', 'FrontPagesController@getPriceByDate');
Route::get('tariff','TariffController@index');
Route::get('termsandconditions','TandCController@index');
Route::get('termsandconditionsDelhi','TandCController@indexDelhi');
Route::get('termsandconditionsNZ','TandCController@indexNZ');

Route::get('privacy','FrontPagesController@privacy');





//Occupancy Report
Route::get('idqJk38Loccupancy','OccupancyReportController@update');
Route::get('phpinfo','OccupancyReportController@phpinfodisplay');



//Migrations
Route::get('fixWeekendSlots327e5gbach5FFD','MigrationController@fixWeekendSlots');
Route::get('fixPrices327e5gbach5FFD','MigrationController@fixPrices');
Route::get('populateHours327e5gbach5FFD','MigrationController@populateHours');
Route::get('populateFullPrice327e5gbach5FFD','MigrationController@populateFullPrice');
Route::get('userCohort327e5gbach5FFD','MigrationController@userCohort');
Route::get('sendSMS327e5gbach5FFD','MigrationController@sendSMS');
Route::get('isDeliveredForPast327e5gbach5FFD','MigrationController@isDeliveredForPast');
Route::get('test327e5gbach5FFD','MigrationController@test');
Route::get('fixDBIssues327e5gbach5FFD','MigrationController@fixDBIssues');
Route::get('randomize327e5gbach5FFD','MigrationController@randomizeData');
Route::get('sanitizeData327e5gbach5FFD','MigrationController@sanitizeData');
Route::get('createReportData327e5gbach5FFD','MigrationController@createReportData');
Route::get('createOpsReportData327e5gbach5FFD','MigrationController@createOpsReportData');
Route::get('convertmysqltophp','MigrationController@convertMysqlToPhp');
Route::post('convertmysqltophp','MigrationController@convertMysqlToPhp');
Route::get('fixAvailableServiceOccupancy327e5gbach5FFD','MigrationController@fixAvailableServiceOccupancy');
Route::get('fixAvailableServiceOccupancyForDatesAreaModel327e5gbach5FFD','MigrationController@fixAvailableServiceOccupancyForDatesAreaModel');
Route::get('deleteCapacityTables327e5gbach5FFD','MigrationController@deleteCapacityTables');
Route::get('addBalanceToUser327e5gbach5FFD','MigrationController@addBalanceToUser');
Route::get('repeatCustomers327e5gbach5FFD','MigrationController@repeatCustomers');
Route::get('cohortData327e5gbach5FFD','MigrationController@cohortData');
Route::get('form-maker327e5gbach5FFD','MigrationController@formMakerIndex');
Route::post('form-maker327e5gbach5FFD/add','MigrationController@addForm');
Route::get('matchPaymentId327e5gbach5FFD','MigrationController@matchPaymentId');
Route::get('parseHtml327e5gbach5FFD','MigrationController@parseHtml');
Route::get('calculatePromotionalBalanceRemaining327e5gbach5FFD','MigrationController@calculatePromotionalBalanceRemaining');
Route::get('webAppAssistedCustomers327e5gbach5FFD','MigrationController@webAppAssistedCustomers');
Route::get('migrateWalletAmount327e5gbach5FFD','MigrationController@migrateWalletAmount');
Route::get('walletBalanceMoM327e5gbach5FFD','MigrationController@walletBalanceMoM');
Route::get('clearNumber27eFFD','MigrationController@clearNumber');
Route::get('showCapacityAvailble327e5gbach5FFD','MigrationController@showCapacityAvailble');
Route::get('moveBookingstoReservation327e5gbach5FFD','MigrationController@moveBookingstoReservation');
Route::get('cleanseDeletedEntriesInA2b327e5gbach5FFD','MigrationController@cleanseDeletedEntriesInA2b');
Route::get('getUserBalance327e5gbach5FFD','MigrationController@getUserBalance');
Route::get('showMissingPricingInfo327e5gbach5FFD','MigrationController@showMissingPricingInfo');
Route::get('fixPricing327e5gbach5FFD','MigrationController@fixPricing');
Route::get('Movementfix327e5gbach5FFD','MigrationController@fixMovement');

//duke390 bike fix 
Route::get('duke390bike327e5gbach5FFD','MigrationController@duke390bikefix');


Route::post('enquiry-form/add','EnquiryController@store');


// App Links
Route::get('app/faqs','AppLinksController@faq');
Route::get('app/tariff','AppLinksController@tariff');
Route::get('app/about-us','AppLinksController@aboutUs');
Route::get('app/contact-us','AppLinksController@contactUs');
Route::get('app/reviews','AppLinksController@reviews');
Route::get('app/docs-req','AppLinksController@docsReq');
Route::get('app/notification/{url_path}','AppLinksController@notification');



//App Redirection
Route::get('app','FrontPagesController@appRedirect');
Route::get('return-to-webstie','FrontPagesController@appRedirectToWeb');


//Offers
Route::get('offers','FrontPagesController@offers');




//Test Booking Routes
Route::get('booking/select-date', 'FrontPagesController@bookingSelectDate');
Route::get('booking/choose-models', '\App\Http\Controllers\FrontViewController@getModelsAMH');

Route::post('booking/checkUserLoggedin', 'FrontPagesController@bookingCheckUserLoggedin');
Route::get('booking/login', 'FrontPagesController@bookingLogin');
Route::get('booking/confirm', 'FrontPagesController@bookingConfirm');
Route::get('booking/redirect/paytm', 'FrontPagesController@redirectForPaytm');
Route::post('booking/paytm/callback','BookingCreationController@paymentWebPaytm');
Route::get('booking/confirmWithPromo', 'FrontPagesController@bookingConfirmZero');


// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('forgot-password', 'Auth\PasswordController@getEmail');
Route::post('forgot-password', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//Annual Plan
Route::get('biker-plan','BikerPlanController@index');
Route::get('biker-plan/{planName}','BikerPlanController@detail');
Route::get('buy-biker-plan','BikerPlanController@checkUserLoggedin');
Route::get('biker-plan/login', 'BikerPlanController@planLogin');
Route::get('confirm-plan', 'BikerPlanController@planConfirm');

Route::get('plan-login', 'BikerPlanController@planLogin');


//User Profile

Route::get('profile', 'UserController@userProfileIndex');
Route::post('user-upload', 'UserController@updateProfile');





//Excluding URIs From CSRF Protection : Start 
Route::get('payment/success', 'BookingCreationController@paymentWeb');
Route::post('payment/success', 'BookingCreationController@paymentWeb');
Route::post('payment/successPromo', 'BookingCreationController@paymentWebZero');
Route::post('payment/failed', 'PaymentController@paymentFailed');
Route::post('payment/cancel', 'PaymentController@paymentCanceled');

Route::get('biker-plan/payment/success', 'PaymentController@paymentBikerPlanSuccess');
Route::post('biker-plan/payment/success', 'PaymentController@paymentBikerPlanSuccess');
Route::post('biker-plan/payment/failed', 'PaymentController@paymentBikerPlanFailed');
Route::post('biker-plan/payment/cancel', 'PaymentController@paymentBikerPlanCanceled');
//Excluding URIs From CSRF Protection : End 


// Login
Route::post('login','LoginController@dologin');
Route::post('erpLogin','LoginController@doErplogin');
Route::post('a2bLogin','LoginController@doA2blogin');
Route::post('randmLogin','LoginController@doRandMlogin');


Route::post('vendor/login','LoginController@doVendorlogin');
Route::post('partner/login','LoginController@doPartnerlogin');
Route::get('logout','LoginController@dologout');
Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');


// Date-Hour Availablity

Route::get('migrateExistingOccupancy327e5gbach5FFD','BikeAvailabilityController@migrateExistingBookingsOccupancy');
Route::get('migrateExistingService327e5gbach5FFD','BikeAvailabilityController@migrateExistingBookingsService');
Route::get('addOneDayCapacity327e5gbach5FFD','BikeAvailabilityController@addOneDayCapacity');
Route::get('fixAreaModel327e5gbach5FFD/{area_id}/{model_id}','BikeAvailabilityController@fixAreaModel');




// Admin
Route::get('admin','AdminController@index');
Route::get('admin/login','AdminController@login');


//API Docs
Route::get('docs/api','ApiDocController@indexApiDoc');
Route::get('docs/api-test','ApiDocController@indexApiDocTest');

Route::group(['middleware' => 'auth.admin'], function()
{

	Route::get('admin/dashboard','AdminController@dashboard');

    
    //Master Data
    
    Route::group(['middleware' => 'permission:master data'],function(){
        
        //News
        Route::get('admin/news','NewsController@index');
        Route::post('admin/news/add','NewsController@add');
        Route::post('admin/news/edit/{news_id}','NewsController@edit');
        Route::post('admin/news/delete/{news_id}','NewsController@delete');

        //Reviews
        Route::get('admin/reviews','ReviewsController@index');
        Route::post('admin/reviews/add','ReviewsController@add');
        Route::post('admin/reviews/edit/{reviews_id}','ReviewsController@edit');
        Route::post('admin/reviews/delete/{reviews_id}','ReviewsController@delete');

        //City
        Route::get('admin/city','CityController@index');
        Route::post('admin/city/add','CityController@add');
        Route::post('admin/city/edit/{city_id}','CityController@edit');

        //Area
        Route::get('admin/area','AreaController@index');
        Route::post('admin/area/add','AreaController@add');
        Route::post('admin/area/edit/{area_id}','AreaController@edit');

        //BikeMake
        Route::get('admin/bikeMake','BikeMakeController@index');
        Route::post('admin/bikeMake/add','BikeMakeController@add');
        Route::post('admin/bikeMake/edit/{bike_make_id}','BikeMakeController@edit');

        //BikeModel
        Route::get('admin/bikeModel','BikeModelController@index');
        Route::post('admin/bikeModel/add','BikeModelController@add');
        Route::post('admin/bikeModel/edit/{bikeModel_id}','BikeModelController@edit');
        Route::post('admin/bikeModel/spec/{bikeModel_id}','BikeModelController@spec');
        
        //BikeModel 
        Route::get('admin/bike-model','BikeModelController@indexBikeModel');
        Route::post('admin/bike-model/add','BikeModelController@addBikeModel');
        Route::get('admin/bike-model/details','BikeModelController@detailsBikeModel');
        Route::post('admin/bike-model/edit/{id}','BikeModelController@editBikeModel');
        Route::post('admin/bike-model/delete/{id}','BikeModelController@deleteBikeModel');

        //Bike
        //Route::get('admin/bike','BikeController@index');
        //Route::post('admin/bike/add','BikeController@add');
        //Route::post('admin/bike/edit/{bike_id}','BikeController@edit');

        
        //Bike 
        Route::get('admin/bike','BikeController@indexBike');
        Route::post('admin/bike/add','BikeController@addBike');
        Route::get('admin/bike/details','BikeController@detailsBike');
        Route::post('admin/bike/edit/{id}','BikeController@editBike');
        Route::post('admin/bike/delete/{id}','BikeController@deleteBike');
        
        //Price
        Route::get('admin/price','PriceController@index');
        Route::post('admin/price/add','PriceController@add');
        Route::post('admin/price/edit/{price_id}','PriceController@edit');
        
        
        //Move a bike
    
        Route::get('admin/moveBike','BikeAvailabilityController@moveBikeIndex');
        Route::post('admin/moveBike/add','BikeAvailabilityController@moveBike');


        //Faq 
        Route::get('admin/faq','FaqController@indexFaq');
        Route::post('admin/faq/add','FaqController@addFaq');
        Route::get('admin/faq/details','FaqController@detailsFaq');
        Route::post('admin/faq/edit/{id}','FaqController@editFaq');
        Route::post('admin/faq/delete/{id}','FaqController@deleteFaq');



        //Accessories Type
        Route::get('admin/accessories-type','AccessoriesController@indexAccessoriesType');
        Route::post('admin/accessories-type/add','AccessoriesController@addAccessoriesType');
        Route::get('admin/accessories-type/details','AccessoriesController@detailsAccessoriesType');
        Route::post('admin/accessories-type/edit/{id}','AccessoriesController@editAccessoriesType');
        Route::post('admin/accessories-type/delete/{id}','AccessoriesController@deleteAccessoriesType');

        //Accessories Size
        Route::get('admin/accessories-size','AccessoriesController@indexAccessoriesSize');
        Route::post('admin/accessories-size/add','AccessoriesController@addAccessoriesSize');
        Route::get('admin/accessories-size/details','AccessoriesController@detailsAccessoriesSize');
        Route::post('admin/accessories-size/edit/{id}','AccessoriesController@editAccessoriesSize');
        Route::post('admin/accessories-size/delete/{id}','AccessoriesController@deleteAccessoriesSize');

        //Accessories Color
        Route::get('admin/accessories-color','AccessoriesController@indexAccessoriesColor');
        Route::post('admin/accessories-color/add','AccessoriesController@addAccessoriesColor');
        Route::get('admin/accessories-color/details','AccessoriesController@detailsAccessoriesColor');
        Route::post('admin/accessories-color/edit/{id}','AccessoriesController@editAccessoriesColor');
        Route::post('admin/accessories-color/delete/{id}','AccessoriesController@deleteAccessoriesColor');


        //Accessories Model
        Route::get('admin/accessories-brand','AccessoriesController@indexAccessoriesBrand');
        Route::post('admin/accessories-brand/add','AccessoriesController@addAccessoriesBrand');
        Route::get('admin/accessories-brand/details','AccessoriesController@detailsAccessoriesBrand');
        Route::post('admin/accessories-brand/edit/{id}','AccessoriesController@editAccessoriesBrand');
        Route::post('admin/accessories-brand/delete/{id}','AccessoriesController@deleteAccessoriesBrand');

        //Accessories Image
        Route::get('admin/accessories-image','AccessoriesController@indexAccessoriesImage');
        Route::post('admin/accessories-image/add','AccessoriesController@addAccessoriesImage');
        Route::get('admin/accessories-image/details','AccessoriesController@detailsAccessoriesImage');
        Route::post('admin/accessories-image/edit/{id}','AccessoriesController@editAccessoriesImage');
        Route::post('admin/accessories-image/delete/{id}','AccessoriesController@deleteAccessoriesImage');

        //Accessories Model
        Route::get('admin/accessories-model','AccessoriesController@indexAccessoriesModel');
        Route::post('admin/accessories-model/add','AccessoriesController@addAccessoriesModel');
        Route::get('admin/accessories-model/details','AccessoriesController@detailsAccessoriesModel');
        Route::post('admin/accessories-model/edit/{id}','AccessoriesController@editAccessoriesModel');
        Route::post('admin/accessories-model/delete/{id}','AccessoriesController@deleteAccessoriesModel');

        //Accessories 
        Route::get('admin/accessories','AccessoriesController@indexAccessories');
        Route::post('admin/accessories/add','AccessoriesController@addAccessories');
        Route::get('admin/accessories/details','AccessoriesController@detailsAccessories');
        Route::post('admin/accessories/edit/{id}','AccessoriesController@editAccessories');
        Route::post('admin/accessories/delete/{id}','AccessoriesController@deleteAccessories');

        //Move an accessory

        Route::get('admin/move_accessories','AccessoriesAvailabilityController@moveAccessoriesIndex');
        Route::post('admin/move_accessories/add','AccessoriesAvailabilityController@moveAccessories');


        // Accessories Price
        Route::get('admin/accessories_price','AccessoriesPriceController@index');
        Route::post('admin/accessories_price/add','AccessoriesPriceController@add');
        Route::post('admin/accessories_price/edit/{price_id}','AccessoriesPriceController@edit');


        //Email Promo Link
        Route::get('admin/emailPromoLink','EmailPromoLinkController@index');
        Route::post('admin/emailPromoLink/add','EmailPromoLinkController@add');
        Route::post('admin/emailPromoLink/edit/{news_id}','EmailPromoLinkController@edit');
        Route::post('admin/emailPromoLink/delete/{news_id}','EmailPromoLinkController@delete');

        //Slider Images
        Route::get('admin/sliderImage','SliderImageController@index');
        Route::post('admin/sliderImage/add','SliderImageController@add');
        Route::post('admin/sliderImage/edit/{news_id}','SliderImageController@edit');
        Route::post('admin/sliderImage/delete/{news_id}','SliderImageController@delete');
        
        //Make bike model inactive
        Route::get('admin/inactivate','MigrationController@inactivateIndex');
        Route::post('admin/inactivateBikes','MigrationController@inactivateBikes');
        
        
        //App Notification Page
        
        Route::get('admin/app-notification','AppLinksController@indexAppNotification');
        Route::post('admin/app-notification/add','AppLinksController@addAppNotification');
        Route::get('admin/app-notification/details','AppLinksController@detailsAppNotification');
        Route::post('admin/app-notification/edit/{id}','AppLinksController@editAppNotification');
        Route::post('admin/app-notification/delete/{id}','AppLinksController@deleteAppNotification');
        
        //FrontEndText 
        Route::get('admin/front-end-text','FrontEndTextController@indexFrontEndText');
        Route::post('admin/front-end-text/add','FrontEndTextController@addFrontEndText');
        Route::get('admin/front-end-text/details','FrontEndTextController@detailsFrontEndText');
        Route::post('admin/front-end-text/edit/{id}','FrontEndTextController@editFrontEndText');
        Route::post('admin/front-end-text/delete/{id}','FrontEndTextController@deleteFrontEndText');
        
        //Global Variable
        Route::get('admin/global_variable','GlobalConfigController@indexGlobalconfig');
        Route::post('admin/global_variable/edit/{id}','GlobalConfigController@editGlobalconfig');
        
        
        
    });
    
    Route::group(['middleware' => 'permission:inside sales'],function(){
        
        //User
        Route::get('admin/user','UserController@index');
        Route::post('admin/user','UserController@index');
        Route::get('admin/userSearch','UserController@index');
        Route::post('admin/user/add','UserController@add');
        Route::get('admin/user/Change_doc_status/{user_id}','UserController@Change_doc_status');
        Route::post('admin/user/document_status/','UserController@document_status');
        Route::post('admin/user/document_delete/{user_id}','UserController@document_delete');
        Route::post('admin/user/edit/{user_id}','UserController@edit');

        //Enquiry
        Route::get('admin/enquiry','EnquiryController@index');

        //ContactEnquiry
        Route::get('admin/contactEnquiry','ContactEnquiryController@index');

        //EmailSubscribe
        Route::get('admin/emailSubscribeidqJk38L','EmailSubscribeController@index');



        //Booking
        Route::get('admin/booking','BookingController@index');
        Route::get('admin/booking/search','BookingController@search');
        Route::post('admin/booking/add','BookingCreationController@paymentErpAdmin');
        Route::post('admin/booking/edit/{id}','BookingCreationController@editReservationApiLegacy');
        Route::post('admin/booking/delete/{booking_id}','BookingCreationController@deleteReservationApiLegacy');
        Route::post('admin/booking/bikeAvailability','BookingController@checkAdminBikeAvailability');

        //Booking
        Route::get('admin/bookingParseidqJk38L','BookingController@indexParse');



        //Bike Filter
        Route::get('admin/bike-filter','BikeFilterController@index');

        // Promo Codes
        Route::get('admin/promocode','PromoCodeController@index');
        Route::get('admin/promocode/search','PromoCodeController@search');
        Route::post('admin/promocode/add','PromoCodeController@store');
        Route::post('admin/promocode/{id}','PromoCodeController@update');

        

        //Cancelled Enquiry 

        Route::get('admin/cancelledEnquiry','EnquiryController@cancelledEnquiry');
        Route::post('admin/cancelledEnquiryNote/edit/{id}','EnquiryController@cancelledEnquiryNoteEdit');


        //Add User to ERP

        Route::get('admin/addUserToErp','UserController@addUserToErpIndex');
        Route::post('admin/addUserToErp/add','UserController@addUserToErpAdd');
        Route::post('admin/addUserToErp/delete/{id}','UserController@addUserToErpDelete');


        //Search User
        Route::get('admin/searchUser','AdminController@searchUser');


        //Collections
        Route::get('admin/collection','BookingController@collection');

        //Service Block

        Route::get('admin/serviceBlock','ErpController@serviceBlockCreateBlockIndex');
        Route::post('admin/serviceBlock/block/{id}','ErpController@serviceBlockCreateBlock');
        Route::post('admin/serviceBlock/delete/{id}','ErpController@serviceBlockAdminDelete');
        Route::get('admin/collidingBookings/{id}','ErpController@collidingBookingsIndex');


        //Hour By Hour Availability
        Route::get('admin/check-occupancy-HBH','BikeFilterController@hourByHourIndex');
        Route::post('admin/check-occupancy-HBH-filter','BikeFilterController@hourByHourFilterOccupancyAMH');

        //Hour By Hour Availability
        Route::get('admin/check-availability-HBH','BikeFilterController@hourByHourAvailableIndex');
        Route::post('admin/check-availability-HBH-filter','BikeFilterController@hourByHourFilterAvailable');

        //Hour By Hour Capacity
        Route::get('admin/check-capacity-HBH','BikeFilterController@hourByHourCapacityIndex');
        Route::post('admin/check-capacity-HBH-filter','BikeFilterController@hourByHourFilterCapacityAMH');


        //Accessories Booking
        Route::get('admin/accessories-booking','AccessoriesController@indexAccessoriesBooking');
        Route::post('admin/accessories-booking/add','AccessoriesController@addAccessoriesBooking');
        Route::get('admin/accessories-booking/details','AccessoriesController@detailsAccessoriesBooking');
        Route::post('admin/accessories-booking/edit/{id}','AccessoriesController@editAccessoriesBooking');
        Route::post('admin/accessories-booking/delete/{id}','AccessoriesController@deleteAccessoriesBooking');

        //Accessories Booking Item
        Route::get('admin/accessories-booking-item','AccessoriesController@indexAccessoriesBookingItem');
        Route::post('admin/accessories-booking-item/add','AccessoriesController@addAccessoriesBookingItem');
        Route::get('admin/accessories-booking-item/details','AccessoriesController@detailsAccessoriesBookingItem');
        Route::post('admin/accessories-booking-item/edit/{id}','AccessoriesController@editAccessoriesBookingItem');
        Route::post('admin/accessories-booking-item/delete/{id}','AccessoriesController@deleteAccessoriesBookingItem');

        //Users With Document Uploaded Index
        Route::get('admin/users_with_doc','UserController@usersWithDocumentUploadedIndex');
        Route::get('admin/users_with_doc/search','UserController@usersWithDocumentUploadedSearch');
        Route::post('admin/users_with_doc/update','UserController@usersWithDocumentUploadedUpdate');
        Route::post('admin/users_with_doc/update_work_email_notes/{id}','UserController@usersWithDocumentUploadedUpdateWorkEmailNotes');
        Route::post('admin/users_with_doc/delete/{id}','UserController@usersWithDocumentUploadedDelete');
        
        
        //Referred Users
        Route::get('admin/referred-users','UserController@indexTopReferers');


        
        
    });
    
    Route::group(['middleware' => 'permission:master data*a2b admin'],function(){
        
        //OneWayRental Locations
        Route::get('admin/one-way-rental-location','OneWayRentalController@indexOneWayRentalLocation');
        Route::post('admin/one-way-rental-location/add','OneWayRentalController@addOneWayRentalLocation');
        Route::get('admin/one-way-rental-location/details','OneWayRentalController@detailsOneWayRentalLocation');
        Route::post('admin/one-way-rental-location/edit/{id}','OneWayRentalController@editOneWayRentalLocation');
        Route::post('admin/one-way-rental-location/delete/{id}','OneWayRentalController@deleteOneWayRentalLocation');
        
         //OneWayRental Locations
        Route::get('admin/one-way-rental-location-spoke','OneWayRentalController@indexOneWayRentalLocationSpoke');
        Route::post('admin/one-way-rental-location-spoke/add','OneWayRentalController@addOneWayRentalLocationSpoke');
        Route::get('admin/one-way-rental-location-spoke/details','OneWayRentalController@detailsOneWayRentalLocationSpoke');
        Route::post('admin/one-way-rental-location-spoke/edit/{id}','OneWayRentalController@editOneWayRentalLocationSpoke');
        Route::post('admin/one-way-rental-location-spoke/delete/{id}','OneWayRentalController@deleteOneWayRentalLocationSpoke');

        //OneWayRental Bikes
        Route::get('admin/one-way-rental-bike','OneWayRentalController@indexOneWayRentalBikeAdmin');
        Route::post('admin/one-way-rental-bike/add','OneWayRentalController@addOneWayRentalBike');
        Route::get('admin/one-way-rental-bike/details','OneWayRentalController@detailsOneWayRentalBike');
        Route::post('admin/one-way-rental-bike/edit/{id}','OneWayRentalController@editOneWayRentalBike');
        Route::post('admin/one-way-rental-bike/delete/{id}','OneWayRentalController@deleteOneWayRentalBike');


        //OneWayRental Routes
        Route::get('admin/one-way-rental-route','OneWayRentalController@indexOneWayRentalRoute');
        Route::post('admin/one-way-rental-route/add','OneWayRentalController@addOneWayRentalRoute');
        Route::get('admin/one-way-rental-route/details','OneWayRentalController@detailsOneWayRentalRoute');
        Route::post('admin/one-way-rental-route/edit/{id}','OneWayRentalController@editOneWayRentalRoute');
        Route::post('admin/one-way-rental-route/delete/{id}','OneWayRentalController@deleteOneWayRentalRoute');



        //OneWayRental Prices
        Route::get('admin/one-way-rental-price','OneWayRentalController@indexOneWayRentalPrice');
        Route::post('admin/one-way-rental-price/add','OneWayRentalController@addOneWayRentalPrice');
        Route::get('admin/one-way-rental-price/details','OneWayRentalController@detailsOneWayRentalPrice');
        Route::post('admin/one-way-rental-price/edit/{id}','OneWayRentalController@editOneWayRentalPrice');
        Route::post('admin/one-way-rental-price/delete/{id}','OneWayRentalController@deleteOneWayRentalPrice');
        
    });
	
    
    Route::group(['middleware' => 'permission:inside sales*a2b admin'],function(){
        

        //OneWayRental Bookings
        Route::get('admin/one-way-rental-booking','OneWayRentalController@indexOneWayRentalBooking');
        Route::post('admin/one-way-rental-booking/delete/{id}','OneWayRentalController@deleteOneWayRentalBooking');
        
    });
    
    
    Route::group(['middleware' => 'permission:inside sales*a2b admin'],function(){
        
    });
    
    
    Route::group(['middleware' => 'permission:sales'],function(){
        
        //Sales Report
        Route::get('admin/salesReport','FrontPagesController@salesReport');
        Route::get('admin/salesReportChart','FrontPagesController@salesReportChart');

        //Ops Report
        Route::get('admin/opsReport','ErpController@opsReportIndex');
        Route::get('admin/opsReportExport','ErpController@opsReportExport');
        
    });
    
    
    Route::group(['middleware' => 'permission:bikation'],function(){
        
         ////  bikation-vendor-admin /////
        Route::get('admin/bikation-vendor','BikationvendorAdminController@getuser');
        Route::post('admin/bikation-vendor/get_packages','BikationvendorAdminController@get_packages');
        Route::get('admin/bikation-vendor/{id}','BikationvendorAdminController@activeBikation_vendor');
        Route::get('admin/bikation-vendor-review','BikationvendorAdminController@getreview');
        Route::get('admin/bikation-banner','BikationvendorAdminController@get_banner');
        Route::post('admin/bikation-add-banner','BikationvendorAdminController@add_banner');
        Route::get('admin/bikation-footer','BikationvendorAdminController@get_footer');
        Route::post('admin/bikation-add-footer','BikationvendorAdminController@add_footer');
        Route::get('admin/bikation-reviews/change-status/{id}','BikationvendorAdminController@activeReviews');
        Route::get('admin/bikation-vendor-bookings','BikationvendorAdminController@getbookings');
        Route::get('admin/bikation-vendor-people_on_trip','BikationvendorAdminController@getpeople_on_trip');
        Route::get('admin/bikation-vendor-trip','BikationvendorAdminController@get_trip');
        Route::get('admin/bikation-vendor-trip/change-status/{id}','BikationvendorAdminController@change_trip');
        Route::get('/admin/bikation-vendor-refund','BikationvendorAdminController@get_refundList');
        Route::get('/admin/bikation-vendor-show-refund','BikationvendorAdminController@show_refund_list');
        Route::post('/admin/bikation-vendor-refund/{id}','BikationvendorAdminController@payment_refund');
        Route::get('admin/bikation-people_on_trip','Bikation_People_On_Trip_AdminController@index');
        Route::post('admin/bikation-people_on_trip','Bikation_People_On_Trip_AdminController@index');
        Route::post('admin/bikation-people_on_trip/add','Bikation_People_On_Trip_AdminController@addPeople_On_Trip');
        Route::post('admin/bikation-people_on_trip/edit/{id}','Bikation_People_On_Trip_AdminController@edit');
        Route::post('admin/bikation-people_on_trip/delete/{id}','Bikation_People_On_Trip_AdminController@delete');
        Route::get('admin/bikation-bookings','Bikation_Bookings_AdminController@index');
        Route::post('admin/bikation-bookings','Bikation_Bookings_AdminController@index');
        Route::post('admin/bikation-bookings/add','Bikation_Bookings_AdminController@addBookings');
        Route::post('admin/bikation-bookings/edit/{id}','Bikation_Bookings_AdminController@edit');
        Route::post('admin/bikation-bookings/delete/{id}','Bikation_Bookings_AdminController@delete');
        Route::post('admin/bikation-bookings/cancel/{id}','Bikation_Bookings_AdminController@cancel');
        
    });
    
    
    
    Route::group(['middleware' => 'permission:aggregation'],function(){
        
        //Aggregation
    
        Route::get('admin/aggregators','AggregationController@aggregatorIndex');
        Route::post('admin/aggregators/add','AggregationController@addAggregator');
        Route::post('admin/aggregators/delete/{id}','AggregationController@deleteAggregator');
        Route::post('admin/aggregators/edit/{id}','AggregationController@editAggregator');

        Route::get('admin/aggregatedBikes','AggregationController@aggregatedBikeIndex');
        Route::post('admin/aggregatedBikes/add','AggregationController@addaggregatedBike'); Route::post('admin/aggregatedBikes/delete/{id}','AggregationController@deleteaggregatedBike');

        Route::get('admin/calculatePayoutIndex','AggregationController@calculatePayoutIndex');
        Route::post('admin/calculatePayout','AggregationController@calculatePayout');
        
        
        //WPP Enquiry
        Route::get('admin/wppEnquiry','WppController@wppEnquiryIndex');
        
    });
    
    Route::group(['middleware' => 'permission:super admin'],function(){
        
        //Repeat User List
        Route::get('admin/repeatUsersBh6fddu', 'UserController@repeatUsersBh6fddu');
        Route::get('admin/repeatUsersCsvBh6fddu', 'UserController@repeatUsersCsvBh6fddu');
        Route::post('admin/repeatUserNote/edit/{id}','UserController@repeatUserNoteEdit');
        
        
        //API log requests
        Route::get('admin/app-payments','AdminController@indexAppRequest');
        Route::get('admin/web-payments','AdminController@indexAppRequestLog');
        Route::get('admin/app-payments/search','AdminController@searchAppRequest');
        Route::get('admin/web-payments/search','AdminController@searchAppLogRequest');
        Route::get('admin/api-logs','ApiController@indexLog');
        
        
        //Roles
        Route::get('admin/user-permission','UserController@indexUserPermission');
        Route::post('admin/user-permission/add','UserController@addUserPermission');
        Route::get('admin/user-permission/details','UserController@detailsUserPermission');
        Route::post('admin/user-permission/edit/{id}','UserController@editUserPermission');
        Route::post('admin/user-permission/delete/{id}','UserController@deleteUserPermission');
        
        //Zones
        Route::get('admin/zone-assign','UserController@indexZoneAssignment');
        Route::post('admin/zone-assign/add','UserController@addZoneAssignment');
        Route::get('admin/zone-assign/details','UserController@detailsZoneAssignment');
        Route::post('admin/zone-assign/edit/{id}','UserController@editZoneAssignment');
        Route::post('admin/zone-assign/delete/{id}','UserController@deleteZoneAssignment');
        
        Route::get('admin/add-user-to-dont-send-list','UserController@indexAddUserToDontSendList');
        Route::post('admin/add-user-to-dont-send-list/add','UserController@addUserToDontSendList');
        
    });
    
    Route::group(['middleware' => 'permission:sales manager'],function(){
        
        
        //Report Page
        Route::get('admin/report_page','ReportController@index');
        Route::get('admin/a2b_report_page','ReportController@a2bIndex');
        Route::post('admin/report_page/download','ReportController@export');
        Route::post('admin/a2b_report_page/download','ReportController@exportA2BBookings');
        Route::get('admin/report_chart','ReportController@reportAreaModelIndex');
        Route::get('admin/report_excel','ReportController@reportAreaModelExcelIndex');
        Route::post('admin/report_chart/chart','ReportController@reportAreaModelChart');
        Route::post('admin/report_excel/chart','ReportController@reportAreaModelExcel');
        
        
        //GCM 
        Route::get('admin/gcm','GCMController@index');
        Route::post('admin/gcm/push','GCMController@push');
        
        
        //Download Bookings
        Route::get('admin/bookings327e5gbach5FFD','BookingController@createFiles');
        Route::get('admin/getBookingFile','BookingController@getBookingFile');
        
    });
    
    
    
    // Update Notes
    Route::post('admin/updateNotes/{db_table}/{column}/{id}','NotesController@update');
    
    
    //Add Images
    Route::get('admin/image','SliderImageController@indexImage');
    Route::post('admin/image/add','SliderImageController@addImage');
    
    
    Route::group(['middleware' => 'permission:inside sales*aggregation'],function(){
        //Aggregation Owner Block
        Route::get('admin/ownerBlock','AggregationController@indexOwnerBlock');
        Route::post('admin/ownerBlock/add','AggregationController@addOwnerBlock');	Route::post('admin/ownerBlock/delete/{id}','AggregationController@deleteOwnerBlock');

        //Partners

        Route::get('admin/partner','PartnerController@show');
        Route::post('admin/partner/add','PartnerController@store');
        Route::post('admin/partner/delete/{id}','PartnerController@delete');
        Route::post('admin/partner/edit/{id}','PartnerController@update');
    });
    
    
    Route::group(['middleware' => 'permission:apidocs'],function(){
    
        //API Docs 

        Route::get('admin/api-category','ApiDocController@indexApiCategory');
        Route::post('admin/api-category/add','ApiDocController@addApiCategory');
        Route::get('admin/api-category/details','ApiDocController@detailsApiCategory');
        Route::post('admin/api-category/edit/{id}','ApiDocController@editApiCategory');
        Route::post('admin/api-category/delete/{id}','ApiDocController@deleteApiCategory');


        Route::get('admin/api-language','ApiDocController@indexApiLanguage');
        Route::post('admin/api-language/add','ApiDocController@addApiLanguage');
        Route::get('admin/api-language/details','ApiDocController@detailsApiLanguage');
        Route::post('admin/api-language/edit/{id}','ApiDocController@editApiLanguage');
        Route::post('admin/api-language/delete/{id}','ApiDocController@deleteApiLanguage');


        Route::get('admin/api-call','ApiDocController@indexApiCall');
        Route::get('admin/api-call/add','ApiDocController@addApiCallIndex');
        Route::post('admin/api-call/add','ApiDocController@addApiCall');
        Route::get('admin/api-call/details','ApiDocController@detailsApiCall');
        Route::get('admin/api-call/edit/{id}','ApiDocController@editApiCallIndex');
        Route::post('admin/api-call/edit/{id}','ApiDocController@editApiCall');
        Route::post('admin/api-call/delete/{id}','ApiDocController@deleteApiCall');

        Route::post('admin/rearrange-api','ApiDocController@rearrangeApiCall');
        Route::post('admin/rearrange-api/{id}','ApiDocController@rearrangeApiElements');


    });

    //Wallet Transcations

    Route::group(['middleware' => 'permission:wallet'],function(){    
        Route::get('admin/add_wallet_to_user','WalletController@indexWallet');
        Route::post('admin/add_wallet_to_user/add','WalletController@addWallet');
        Route::post('admin/add_wallet_to_user/delete/{id}','WalletController@deleteWallet');
    });

});

 
// Vendor
Route::get('vendor','VendorController@index');
Route::get('vendor/login','VendorController@login');

Route::group(['middleware' => 'auth.vendor'], function()
{

	Route::get('vendor/dashboard','VendorController@dashboard');
    
    
    //Price
	Route::get('vendor/price','PriceController@indexVendor');
	Route::post('vendor/price/add','PriceController@addVendor');
	Route::post('vendor/price/edit/{price_id}','PriceController@editVendor');

	


	//Bike
	Route::get('vendor/bike','BikeController@indexVendor');
	Route::post('vendor/bike/add','BikeController@addVendor');
	Route::post('vendor/bike/edit/{bike_id}','BikeController@editVendor');

	


	//Booking
	Route::get('vendor/booking','BookingController@indexVendor');
	Route::post('vendor/booking/add','BookingController@addVendor');
	Route::post('vendor/booking/delete/{booking_id}','BookingController@deleteVendor');
	Route::post('vendor/booking/bikeAvailability','BookingController@checkBikeAvailability');

 
    
	//Bike Filter
	Route::get('vendor/bike-filter','BikeFilterController@indexVendor');
    

});


Route::post('user/document_delete/{user_id}','UserController@document_user_delete');

// Partner
Route::get('partner','PartnerController@index');
Route::get('partner/login','PartnerController@login');

Route::group(['middleware' => 'auth.partner'], function()
{

	Route::get('partner/dashboard','PartnerController@dashboard');
    
    


	//Booking
	Route::get('partner/booking','BookingController@indexPartner');
    
    
    //Bike Filter
    Route::get('partner/bike-filter','BikeFilterController@indexPartner');


	

});


//Operations

Route::get('erp','ErpController@index');
Route::get('erp/login','ErpController@login');

Route::get('a2b','OneWayRentalOpsController@opsIndex');
Route::get('a2b/login','OneWayRentalOpsController@opsLogin');

Route::get('randm','RandMController@index');
Route::get('randm/login','RandMController@login');

Route::get('one-way-rental-booking-message/{result}/{id}','OneWayRentalController@oneWayRentalBookingMessageUrl');



Route::get('/edituser','EdituserController@index');
Route::post('/sendmail','EdituserController@sendmail');
// Route::get('/tasks/{id?}','EdituserController@edit');
// Route::put('/tasks/{id?}','EdituserController@update');
Route::post('/upload','EdituserController@upload');
Route::post('/upload_document','EdituserController@upload_document');
Route::post('/cancel_booking_request','EdituserController@cancel_booking_request');
//Route::get('admin/packages','PackagesController@index');
//Route::post('admin/packages/add','PackagesController@add');
//Route::post('admin/packages/edit/{bikeModel_id}','PackagesController@edit');
//Route::post('admin/packages/delete/{bikeModel_id}','PackagesController@delete');

/*
Route::get('/packages','PackagesdashboardController@index');
Route::get('packages/{id}','PackagesdashboardController@detail');
Route::post('packages/addreviews','PackagesdashboardController@addreviews');
Route::post('packages/addpackage','PackagesdashboardController@addpackage');*/




//  bikation-vendor /////
Route::get('bikation-vendor','BikationvendorController@getlogin');
Route::post('bikation-vendor','BikationvendorController@postlogin');
Route::get('vendor_logout','BikationvendorController@doLogout');

Route::get('bikation-vendor/register','BikationvendorController@getregister');
Route::post('bikation-vendor/register','BikationvendorController@postregister');
Route::get('bikation-vendor/dashboard','BikationvendorController@dashboard');
Route::post('bikation-vendor/get_packages','BikationvendorController@get_packages');

Route::get('bikation-user-profile','Bikation_User_ProfileController@index');
Route::post('bikation-user-profile/add','Bikation_User_ProfileController@addProfile');

Route::get('bikation-addon','Bikation_AddonController@index');
Route::post('bikation-addon','Bikation_AddonController@index');
Route::post('bikation-addon/add','Bikation_AddonController@addAddon');
Route::post('bikation-addon/edit/{id}','Bikation_AddonController@edit');
Route::post('bikation-addon/delete/{id}','Bikation_AddonController@delete');

Route::get('bikation-address','Bikation_AddressController@index');
Route::post('bikation-address/add','Bikation_AddressController@addAddress');
Route::get('bikation-address/edit/{id}','Bikation_AddressController@edit');

Route::get('bikation-bookings','Bikation_BookingsController@index');
Route::post('bikation-bookings','Bikation_BookingsController@index');
Route::post('bikation-bookings/add','Bikation_BookingsController@addBookings');
Route::post('bikation-bookings/edit/{id}','Bikation_BookingsController@edit');
Route::post('bikation-bookings/delete/{id}','Bikation_BookingsController@delete');

Route::get('bikation-itinerary','Bikation_ItineraryController@index');
Route::post('bikation-itinerary','Bikation_ItineraryController@index');
Route::post('bikation-itinerary/add','Bikation_ItineraryController@addItinerary');
Route::post('bikation-itinerary/edit/{id}','Bikation_ItineraryController@edit');
Route::post('bikation-itinerary/delete/{id}','Bikation_ItineraryController@delete');

Route::get('bikation-package','Bikation_PackageController@index');
Route::post('bikation-package','Bikation_PackageController@index');
Route::post('bikation-package/add','Bikation_PackageController@addPackage');
Route::post('bikation-package/edit/{id}','Bikation_PackageController@edit');
Route::post('bikation-package/delete/{id}','Bikation_PackageController@delete');

Route::get('bikation-people_on_trip','Bikation_People_On_TripController@index');
Route::post('bikation-people_on_trip','Bikation_People_On_TripController@index');
Route::post('bikation-people_on_trip/add','Bikation_People_On_TripController@addPeople_On_Trip');
Route::post('bikation-people_on_trip/edit/{id}','Bikation_People_On_TripController@edit');
Route::post('bikation-people_on_trip/delete/{id}','Bikation_People_On_TripController@delete');

Route::get('bikation-refund','Bikation_RefundController@index');
Route::post('bikation-refund/add','Bikation_RefundController@addRefund');
Route::post('bikation-refund/edit/{id}','Bikation_RefundController@edit');

Route::get('bikation-reviews','Bikation_ReviewsController@index');
Route::post('bikation-reviews','Bikation_ReviewsController@index');
Route::post('bikation-reviews/add','Bikation_ReviewsController@addReviews');

Route::get('bikation-trip-photo','Bikation_Trip_PhotoController@index');
Route::post('bikation-trip-photo','Bikation_Trip_PhotoController@index');
Route::post('bikation-trip-photo/add','Bikation_Trip_PhotoController@addTrip_Photo');
Route::post('bikation-trip-photo/edit/{id}','Bikation_Trip_PhotoController@edit');
Route::post('bikation-trip-photo/delete/{id}','Bikation_Trip_PhotoController@delete');

Route::get('bikation-trip','Bikation_TripController@index');
Route::get('bikation-trip/send-approve-request/{id}','Bikation_TripController@send_approve_request');
Route::post('bikation-trip/add','Bikation_TripController@addTrip');

Route::post('bikation-trip/edit/{id}','Bikation_TripController@edit');
Route::post('bikation-trip/delete/{id}','Bikation_TripController@delete');


//front trip
Route::get('/trip','Bikation_PackagesdashboardController@index');
Route::get('/trip/{id}','Bikation_PackagesdashboardController@detail');
Route::post('/trip/addreviews','Bikation_PackagesdashboardController@addreviews');
Route::post('/trip-signup','Bikation_PackagesdashboardController@trip_signup');
Route::post('/triplogin','Bikation_PackagesdashboardController@post_triplogin');
Route::post('/checkemail','Bikation_PackagesdashboardController@checkemail');
Route::post('/add_no_seats','Bikation_PackagesdashboardController@add_no_seats');
Route::post('/cancel_payment','Bikation_PackagesdashboardController@cancel_payment');

Route::get('/filter_price','Bikation_PackagesdashboardController@filter_price');
Route::get('/filter_location','Bikation_PackagesdashboardController@filter_location');
Route::get('/filter_popularity','Bikation_PackagesdashboardController@filter_popularity');
Route::post('/search_trip','Bikation_PackagesdashboardController@search_trip');
Route::post('/price_calculation','Bikation_PackagesdashboardController@price_calculation');
Route::post('/update_payment','Bikation_PackagesdashboardController@update_payment');

Route::post('/update_payment_trip','Bikation_PackagesdashboardController@update_payment_trip');
Route::post('/update_payment_demo_post','Bikation_PackagesdashboardController@update_payment_demo_post');

Route::get('/thankyou','Bikation_PackagesdashboardController@update_payment_thankyou');




Route::group(['middleware' => 'auth.erp'], function()
{

	Route::get('erp/dashboard','ErpController@dashboard');
    

    Route::group(['middleware' => 'permission:operations manager'],function(){
        
        //Approve Service Block
        Route::get('erp/serviceBlockApprove','ErpController@serviceBlockApprovalIndex');
        Route::post('erp/serviceBlockApprove/approve/{id}','ErpController@serviceBlockApprove');
        Route::post('erp/serviceBlockApprove/reject/{id}','ErpController@serviceBlockReject');
        
        //Add Bike Numbers to ERP
    
        Route::get('erp/addBikeNumbers','ErpController@addBikeNumbersIndex');
        Route::post('erp/addBikeNumbers/add','ErpController@addBikeNumbersAdd');
        Route::post('erp/addBikeNumbers/delete/{id}','ErpController@addBikeNumbersDelete');
        
        
        
    });
    

    //Booking
    Route::get('erp/todaybooking','BookingController@indexErpToday');
    Route::get('erp/tomorrowbooking','BookingController@indexErpTomorrow');
    Route::get('erp/todaybookingdrop','BookingController@indexErpTodayDrop');
    Route::get('erp/tomorrowbookingdrop','BookingController@indexErpTomorrowDrop');

    Route::post('erp/deliver/{id}','ErpController@deliver');
    Route::post('erp/deliver/edit/{id}','ErpController@deliverEdit');
    Route::post('erp/cancel-delivery/{id}','ErpController@cancelDelivery');

    Route::post('erp/return/{id}','ErpController@returnF');
    Route::post('erp/return/edit/{id}','ErpController@returnEdit');



    //Bike Filter
    Route::get('erp/check-availability','BikeFilterController@indexErp');
    Route::get('erp/check-availability-AMH','BikeFilterController@indexAMH');

    
    // Service Block
    Route::get('erp/service-block','BookingController@indexErpServiceBlock');
    Route::post('admin/cancelledEnquiryNote/edit/{id}','BookingController@cancelledEnquiryNoteEdit');
    Route::post('erp/update-notes-for-service-block/{id}','ErpController@updateNotesForServiceBlock');
    
    // Movements
    Route::get('erp/movement','BookingController@indexErpMovement');
    
    // Update Notes
    Route::post('erp/updateNotes/{db_table}/{column}/{id}','NotesController@update');
    
    //Add Service Block
    
    Route::get('erp/serviceBlock','ErpController@serviceBlockIndex');
    Route::post('erp/serviceBlock/add','ErpController@serviceBlockAdd');
    Route::post('erp/serviceBlock/delete/{id}','ErpController@serviceBlockDelete');
    Route::post('erp/serviceBlock/unblock/{id}','ErpController@serviceBlockUnblock');
    
    
    
    //Add Helmet Jacket to ERP
    
    Route::get('erp/addHelmetJacket','ErpController@addHelmetJacketIndex');
    Route::post('erp/addHelmetJacket/add','ErpController@addHelmetJacketAdd');
    Route::post('erp/addHelmetJacket/delete/{id}','ErpController@addHelmetJacketDelete');
    
    //Move a bike
    
    Route::get('erp/moveBike','BikeAvailabilityController@moveBikeIndex');
    Route::post('erp/moveBike/add','BikeAvailabilityController@moveBike');
    
    
    //Move an accessory
    
    Route::get('erp/move_accessories','AccessoriesAvailabilityController@moveAccessoriesIndex');
    Route::post('erp/move_accessories/add','AccessoriesAvailabilityController@moveAccessories');
    
    Route::get('erp/testFunctions','BikeAvailabilityController@testFunctions');
    
    
    //Backend Notes
    Route::get('erp/bookingNotes','ErpController@bookingNotesIndex');
    Route::post('erp/bookingNotes/ack/{id}','ErpController@bookingNotesAck');
    
    
    //ERP creating bookings
    Route::get('erp/booking','ErpController@bookingIndex');
    Route::get('erp/booking/search','ErpController@bookingSearch');
	
    
    //ERP Booking
    Route::post('erp/booking/add','BookingCreationController@paymentErpAdmin');
    
    //User Docs
    Route::post('erp/users_with_doc/update','UserController@usersWithDocumentUploadedUpdate');
    Route::post('erp/users_with_doc/delete','UserController@usersWithDocumentUploadedDeleteErp');
    Route::post('erp/upload_user_docs','ErpController@uploadUserDoc');

    
    
});


Route::group(['middleware' => 'auth.erp'], function()
{

	Route::get('a2b/dashboard','OneWayRentalOpsController@opsDashboard');
    Route::get('a2b/get-deliveries','OneWayRentalOpsController@getDeliveriesForOps');
    Route::get('a2b/get-returns','OneWayRentalOpsController@getReturnsForOps');
    Route::post('a2b/deliver','OneWayRentalOpsController@deliverBike');
    Route::post('a2b/return','OneWayRentalOpsController@returnBike');
    Route::get('a2b/get-movement-deliveries','OneWayRentalOpsController@getMovementDeliveriesForOps');
    Route::get('a2b/get-movement-returns','OneWayRentalOpsController@getMovementReturnsForOps');
    Route::post('a2b/movement-deliver','OneWayRentalOpsController@movementDeliverBike');
    Route::post('a2b/movement-return','OneWayRentalOpsController@movementReturnBike');
    
    //Movement
    Route::get('a2b/a2b-movement','OneWayRentalController@movementIndex');
    Route::post('a2b/a2b-movement/add','OneWayRentalController@createMovementOrder');
    Route::post('a2b/a2b-movement/delete/{id}','OneWayRentalController@deleteMovementOrder');
    
    Route::get('a2b/one-way-rental-bike','OneWayRentalController@indexOneWayRentalBikeA2BList');
    
    Route::group(['middleware' => 'permission:operations manager'],function(){
        Route::get('a2b/report','OneWayRentalOpsController@reportIndex');
    });
    
    
});

Route::group(['middleware' => 'auth.erp'], function()
{

	Route::get('randm/dashboard','RandMController@dashboard');
    

    Route::group(['middleware' => 'permission:operations manager'],function(){
        
       
        
        //Repairs
        Route::get('randm/randm-inventory','RandMController@indexRandMInventory');
        Route::post('randm/randm-inventory/add','RandMController@addRandMInventory');
        Route::get('randm/randm-inventory/details','RandMController@detailsRandMInventory');
        Route::post('randm/randm-inventory/edit/{id}','RandMController@editRandMInventory');
        Route::post('randm/randm-inventory/delete/{id}','RandMController@deleteRandMInventory');
        
        Route::get('randm/randm-inventory-register','RandMController@indexRandMInventoryRegister');
        Route::post('randm/randm-inventory-register/add','RandMController@addRandMInventoryRegister');
        Route::get('randm/randm-inventory-register/details','RandMController@detailsRandMInventoryRegister');
        Route::post('randm/randm-inventory-register/edit/{id}','RandMController@editRandMInventoryRegister');
        Route::post('randm/randm-inventory-register/delete/{id}','RandMController@deleteRandMInventoryRegister');
        
        Route::get('randm/randm-service-dealer','RandMController@indexRandMServiceDealer');
        Route::post('randm/randm-service-dealer/add','RandMController@addRandMServiceDealer');
        Route::get('randm/randm-service-dealer/details','RandMController@detailsRandMServiceDealer');
        Route::post('randm/randm-service-dealer/edit/{id}','RandMController@editRandMServiceDealer');
        Route::post('randm/randm-service-dealer/delete/{id}','RandMController@deleteRandMServiceDealer');
        
        Route::get('randm/randm-service','RandMController@indexRandMService');
        Route::post('randm/randm-service/add','RandMController@addRandMService');
        Route::get('randm/randm-service/details','RandMController@detailsRandMService');
        Route::post('randm/randm-service/edit/{id}','RandMController@editRandMService');
        Route::post('randm/randm-service/delete/{id}','RandMController@deleteRandMService');
        
        Route::get('randm/randm-service-schedule','RandMController@indexRandMServiceSchedule');
        Route::post('randm/randm-service-schedule/add','RandMController@addRandMServiceSchedule');
        Route::get('randm/randm-service-schedule/details','RandMController@detailsRandMServiceSchedule');
        Route::post('randm/randm-service-schedule/edit/{id}','RandMController@editRandMServiceSchedule');
        Route::post('randm/randm-service-schedule/delete/{id}','RandMController@deleteRandMServiceSchedule');
        
        
                
    });
    
    
    Route::get('randm/randm-service-record','RandMController@indexRandMServiceRecord');
    Route::post('randm/randm-service-record/add','RandMController@addRandMServiceRecord');
    Route::get('randm/randm-service-record/details','RandMController@detailsRandMServiceRecord');
    Route::post('randm/randm-service-record/edit/{id}','RandMController@editRandMServiceRecord');
    Route::post('randm/randm-service-record/delete/{id}','RandMController@deleteRandMServiceRecord');
    
});

Route::get('rent-a-{make_name}-{model_name}','FrontPagesController@bookingSelectDateByName');

Route::get('/edituser','EdituserController@index');
Route::post('/sendmail','EdituserController@sendmail');
// Route::get('/tasks/{id?}','EdituserController@edit');
// Route::put('/tasks/{id?}','EdituserController@update');
Route::post('/upload','EdituserController@upload');
Route::post('/uploadDL','EdituserController@uploadDL');
Route::post('/uploadID','EdituserController@uploadID');

//Health check for website
Route::get('health','ApiController@healthcheck');

//Route::post('admin/upload', ['as' => 'admin.upload', 'uses' =>'EdituserController@upload']);


include 'Route/mobileRoute.php';


