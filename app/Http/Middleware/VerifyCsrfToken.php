<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //'payment/*',
    	'payment/success',
    	'payment/failed',
    	'payment/cancel',
        'api/v1/userRegistration',
        'api/v1/getUserAuthenticationDetails',
        'api/v1/resetPassword',
        'api/*',
        'biker-plan/payment/success',
        'biker-plan/payment/failed',
        'biker-plan/payment/cancel',
        '/enquiry-form/add'

    ];
}
