<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Http\Requests;
use Response;
use App\Http\AppController;

class AuthenticateErpApi
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \App\Http\Controllers\UserController::getUserByToken();
        
        if (!$user) 
        {   
            $controller =  new \App\Http\Controllers\AppController;
            return $controller->respondWithSuccess(['authorization'=>"user not authorized"]);
        } 
        else
        {
            if ($user->isOperation == 1)
            {
                return $next($request);
            }
            else
            {
                $controller =  new \App\Http\Controllers\AppController;
                return $controller->respondWithSuccess(['authorization'=>"user not authorized"]);
            }
        }
    }
}
