<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use DB;

class LogRequests
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     
        $log  = DB::table('parameter')->where('parameter_name','log_request')->first();
        
        if ($log->parameter_value == "true") { 
            $requestArray = [];
            foreach($request->request as $key=>$value)
            {
                array_push($requestArray,[$key=>$value]);
            }
            foreach($request->query as $key=>$value)
            {
                array_push($requestArray,[$key=>$value]);
            }
            DB::table('log_requests')->insert(['request'=>json_encode($requestArray),'url'=>$request->url()]);
           
        } 
       return $next($request);
    }
}
