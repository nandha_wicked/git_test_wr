<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;


use App\Models\FinancialInfo;

use App\Http\Requests;
use Session;
use Mail;
use Config;
use Razorpay\Api\Api;

class RazorPayCapture extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
    */

    
    protected $parameters;
    
    public function __construct($parameters)
    {
        //
        
        $this->parameters = $parameters;
        
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
        $parameterDE = $this->parameters;
        
        $parameter = json_decode($parameterDE);
        
        $txnid = $parameter->txnid;
        $amount = $parameter->amount;
        $booking_id = $parameter->booking_id;
         
        $problem = false;
       
        //Change to capture
        
        $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

        
        if(env('TESTPAYMENT')==true)
        {
            $str = "captured";
        }
        else
        {
            $payment = $api->payment->fetch($txnid);
            $str = strtolower($payment->status);            
        }
        
        if($str=='authorized')
        {
            if($payment->amount/100 > ($amount-1))
            {
                $response = $payment->capture(array('amount' => $payment->amount));
                $str = strtolower($response->status);   
            }
            else
            {
                $data = [
                    'bookingId'=>$booking_id,
                    'txnAmount'=>$payment->amount/100,
                    'totalPrice'=>$amount,
                    'reason'=>"RazorPay Transaction Problem"
                ];

                $problem = true;
            }
            
        }
        elseif($str!='captured')
        {
            $data = [
                'bookingId'=>$booking_id,
                'reason'=>"RazorPay Transaction Problem"
            ];
            
            $problem = true;
        }
        
        if($problem)
        {
            Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject('RazorPay App Problem - '.$data['bookingId']);
            });
            
        }
                
        FinancialInfo::where('payment_id',$txnid)->update([
            'status'=>$str
        ]);
        
        return "done";
        
    }
}
