<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Models\Booking;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\Image;
use App\Models\Slot;
use App\Models\PromoCode;
use App\Models\BikerPlanSubscription;
use App\Models\SentTransaction;
use Session;
use Mail;
use Config;
use Carbon\Carbon;
use Razorpay\Api\Api;

class RazorPay extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
    */

    
    protected $parameters;
    
    public function __construct($parameters)
    {
        //
        
        $this->parameters = $parameters;
        
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        
        $parameterDE = $this->parameters;
        
        $parameter = json_decode($parameterDE);
        
               
        
        $txnid = $parameter->txnid;
        
        $booking_id = $parameter->booking_id;
        
        $modelId= $parameter->modelId;
        $areaId= $parameter->areaId;
        $startDate= $parameter->startDate;
        $endDate= $parameter->endDate;
        $startTime= $parameter->startTime;
        $endTime= $parameter->endTime;
        $coupon= $parameter->coupon;
        
        
        $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

        
        $payment = $api->payment->fetch($txnid);
        
        
        $str = strtolower($payment->status);
        
        if($str=='authorized')
        {
            $payment->capture(array('amount' => $payment->amount));
            
            $amount = $payment->amount/100;
            //Change to capture



            $dispatcher = app('Dingo\Api\Dispatcher');

            $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $modelId, 'area_id' => $areaId, 'start_date' => $startDate, 'end_date' => $endDate, 'start_time' => $startTime, 'end_time' => $endTime, 'coupon' => $coupon,'device'=>"WEB"])->get('bookings/price-and-check-availability');


            $data = ['bookingId'=>$booking_id,'txnAmount'=>$amount,'totalPrice'=>$bikeAvailabilityWithPrice['total_price']];
            


            if($amount<($bikeAvailabilityWithPrice['total_price']-2))
            {
                Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
                  $message->to(env('ADMIN_EMAIL'));
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject('RazorPay Web Problem - '.$data['bookingId']);
                });
            }
            
        }
        elseif($str!='captured')
        {
            $data = ['bookingId'=>$booking_id];
           
            Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject('RazorPay Web Problem - '.$data['bookingId']);
            });
           
        }
        return "done";
        
    }
}
