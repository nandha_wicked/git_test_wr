<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Models\BikeModel;
use Session;
use Mail;
use Config;
use Carbon\Carbon;
use Razorpay\Api\Api;

class RazorPayOneWay extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
    */

    
    protected $parameters;
    
    public function __construct($parameters)
    {
        //
        
        $this->parameters = $parameters;
        
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
        $parameterDE = $this->parameters;
        
        $parameter = json_decode($parameterDE);
        
        
        $pgTxnAmount = $parameter->pg_txn_amount;
        $pgTxnId = $parameter->pg_txn_id;
        $amountDue = $parameter->amount_due;
        $bookingId =  $parameter->bookingId;       
        
        //Change to capture
        
        $api = new Api(env('RAZOR_PAY_PUBLIC'), env('RAZOR_PAY_SECRET'));

        if(env('TESTPAYMENT')==true)
        {
            $str = "captured";
        }
        else
        {
            if($pgTxnId == "")
                return "done";
            $payment = $api->payment->fetch($pgTxnId);
            $str = strtolower($payment->status);            
        }
        
        
        if($str=='authorized')
        {
            $payment->capture(array('amount' => $payment->amount));
            
            $amount = $payment->amount/100;
            //Change to capture
            

            $data = ['bookingId'=>$bookingId,'txnAmount'=>$amount,'totalPrice'=>$amountDue,'reason'=>"RazorPay Transaction Problem"];


            if($amount<($amountDue-2))
            {
                Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
                  $message->to(env('ADMIN_EMAIL'));
                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                  $message->subject('One Way Rentals Payment Mismatch - '.$data['bookingId']);
                });
            }
            
        }
        elseif($str!='captured')
        {
            $data = ['bookingId'=>$bookingId,'reason'=>"RazorPay Transaction Problem"];
           
            Mail::send('emails.admin_booking_problem_mail',$data,function($message) use ($data) {
              $message->to(env('ADMIN_EMAIL'));
              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
              $message->subject('One Way Rentals Payment Problem - '.$data['bookingId']);
            });
           
        }
        return "done";
        
    }
}
