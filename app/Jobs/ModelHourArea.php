<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Models\Booking;
use App\Http\Controllers\BikeAvailabilityController;
use App\Models\Availability;



use App\Http\Requests;

use Session;
use Mail;
use Config;


class ModelHourArea extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
    */

    
    protected $parameters;
    
    public function __construct($parameters)
    {
        //
        
        $this->parameters = $parameters;
        
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
        $parameterDE = $this->parameters;
        
        $parameter = json_decode($parameterDE);
        
        
        $booking_id= $parameter->booking_id;
        $nature=$parameter->nature;
        
        $booking = Booking::where('id',$booking_id)->first();
        
        if(!$booking)
        {
            
        }
        else
        {
        
            BikeAvailabilityController::newBookingsAvailable($booking_id,$nature);
            $serviceIds = array(1,2,3,5,6,7,8,9,10,15);
            if(in_array($booking['user_id'],$serviceIds))
            {
                BikeAvailabilityController::newBookingsService($booking_id,$nature);
            }
            else
            {
                BikeAvailabilityController::newBookingsOccupancy($booking_id,$nature);
            }

            if($nature=="delete")
            {
                Booking::destroy($booking_id);
                $availability = Availability::where('booking_id', '=', $booking_id)->delete();
            }
        }

        
        return "done";
        
    }
}
