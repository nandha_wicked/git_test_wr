<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Models\Booking;
use App\Models\Bike;
use App\Models\BikeModel;
use App\Models\BikeMake;
use App\Models\Price;
use App\Models\Area;
use App\Models\User;
use App\Models\Image;
use App\Models\Slot;
use App\Models\PromoCode;
use App\Models\BikerPlanSubscription;
use App\Models\SentTransaction;
use Session;
use Mail;
use DB;
use Config;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\CancelledEnquiry;


class PayTM extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
    */

    
    protected $parameters;
    
    public function __construct($parameters)
    {
        //
        
        $this->parameters = $parameters;
        
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        
        
        $parameterDe = $this->parameters; 
        $parameter = json_decode($parameterDe);
        
       
       
        $order_id= $parameter->order_id;
        $user_id= $parameter->user_id;
        $modelId= $parameter->model_id;
        $area_id= $parameter->area_id;
        $start_date_time= $parameter->start_date_time; 
        $end_date_time= $parameter->end_date_time;
        $user = User::where('id',$user_id)->first();
        $userFname = $user->first_name;
        $userLname = $user->last_name;
        $userEmail = $user->email;
        $userMob = $user->mobile_num;
        $amount= $parameter->amount;
        $model = BikeModel::where('id', $modelId)->where('status', 1)->first();
        $modelName = $model['bike_model'];
        $area = Area::where('id',$area_id)->first();
        $areaName = $area['area'];
        $areaMapLink = $area->maps_link;
        $startDate = $parameter->start_date;
        $endDate = $parameter->end_date;
        $startTime = $parameter->start_time;
        $endTime = $parameter->end_time;
        $startDateTime = Carbon::parse($startDate.' '.$startTime);
        $endDateTime = Carbon::parse($endDate.' '.$endTime);
        $dateStr = $startDateTime->format('D j My ha').' to '.$endDateTime->format('D j My ha');
        $enquiry = $model->bike_model." - ".$area->area."  From ".$dateStr;

                
        
        $existingBooking = Booking::where('order_id',$order_id)->count();
        if($existingBooking==0)
        {
            

            $payment_gateway= $parameter->payment_gateway;

            $coupon= $parameter->promocode;

            
            $cityId = $area->city_id;
            $startDate = $parameter->start_date;
            $endDate = $parameter->end_date;
            $startTime = $parameter->start_time;
            $endTime = $parameter->end_time;
            
            
            header("Pragma: no-cache");
            header("Cache-Control: no-cache");
            header("Expires: 0");
            // following files need to be included
            require_once("/var/www/html/public/lib/config_paytm.php");
            require_once("/var/www/html/public/lib/encdec_paytm.php");
            $requestParamList = array();
            $responseParamList = array();

            $requestParamList = array("MID" => "Wicked31644082492327" , "ORDERID" => $order_id);  
            $responseParamList = getTxnStatus($requestParamList);
            
            DB::table('paytm_txn')->insert(['TXNID' => $responseParamList['TXNID'],
            'BANKTXNID' => $responseParamList['BANKTXNID'],
            'ORDERID' => $responseParamList['ORDERID'],
            'TXNAMOUNT' => $responseParamList['TXNAMOUNT'],
            'STATUS' => $responseParamList['STATUS'],
            'TXNTYPE' => $responseParamList['TXNTYPE'],
            'GATEWAYNAME' => $responseParamList['GATEWAYNAME'],
            'RESPCODE' => $responseParamList['RESPCODE'],
            'RESPMSG' => $responseParamList['RESPMSG'],
            'BANKNAME' => $responseParamList['BANKNAME'],
            'MID' => $responseParamList['MID'],
            'PAYMENTMODE' => $responseParamList['PAYMENTMODE'],
            'REFUNDAMT' => $responseParamList['REFUNDAMT'],
            'TXNDATE' => $responseParamList['TXNDATE']]);
            
            

            

            if($responseParamList['STATUS']=="TXN_SUCCESS")
            {
                
                
                $dispatcher = app('Dingo\Api\Dispatcher');

                $bikeAvailabilityWithPrice = $dispatcher->with(['model_id' => $modelId, 'area_id' => $area_id, 'city_id' => $cityId, 'start_date' => $startDate, 'end_date' => $endDate, 'start_time' => $startTime, 'end_time' => $endTime, 'coupon' => $coupon])->get('bookings/price-and-check-availability');

                $number_of_hours = $bikeAvailabilityWithPrice['number_of_hours'];
                $minimum_hours = $bikeAvailabilityWithPrice['minimum_hours'];

                $max_hours = max($number_of_hours,$minimum_hours);

                $make = BikeModel::where('id',$modelId)->pluck('bike_make_id');
                    if(in_array($make,[1,2,4,5,6]))
                       {
                         $extrakmprice = env('EXTRAKMPRE');  
                       }
                    else
                       {
                         $extrakmprice = env('EXTRAKMRE');  
                       }
                $freekm = $max_hours*env('KMLIMIT');

                $kmLimitMessage = $freekm." KMs are free with this reservation. After that every KM will be charged at Rs ".$extrakmprice.".";



                $bikeId = $bikeAvailabilityWithPrice['bike_id'];
                $status = 'COMPLETE';
                $dispatcher = app('Dingo\Api\Dispatcher');

                if($bikeId == "none")
                {
                  //SendEmailToAdmin 
                    $data = ['fromDate' => $startDate, 'toDate' => $endDate, 'fromTime' => $startTime, 'toTime' => $endTime, 
                                'areaName' => $areaName, 'amount' => $amount,
                                'userFname' => $userFname, 'modelName' => $modelName, 
                                'userEmail' => $userEmail, 'userMob' => $userMob, 'reason' => 'Bike blocked while payment was being made' ];
                    Mail::send('emails.admin_paytm_failure_mail',$data,function($message) use ($data) {
                          $message->to(env('ADMIN_EMAIL'));
                          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                          $message->subject('PayTM Problem '.$data['userFname']);
                    });
                }
                else
                { 
                    $totalPrice = $bikeAvailabilityWithPrice['total_price'];
                    
                    //5.5% VAT
                    $totalPrice = $totalPrice + ($totalPrice * 0.055);
                    // 2 decimal points round up
                    $totalPrice = floor($totalPrice * 100) / 100;

                    //if($responseParamList['TXNAMOUNT'] > $totalPrice-2)
                    if(1)
                    {

                        $booking = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate, 
                                    'start_time' => $startTime, 'end_time' => $endTime, 
                                    'user_id' =>$user_id, 'actual_user_id'=>$user_id, 'model_id' => $modelId, 'area_id' => $area_id, 'status' => 'COMPLETE',
                                    'total_price' => $totalPrice,'pg_status' => 'success', 
                                    'pg_txn_id' => $responseParamList['TXNID'], 'bank_txn_id' => $responseParamList['BANKTXNID'], 
                                    'order_id' => $order_id, 'pg_mode' => $responseParamList['PAYMENTMODE'], 'note' => '', 'pg_txn_amount' => $responseParamList['TXNAMOUNT'], 'coupon'=>$coupon,'payment_method'=>'PayTM','kmlimit'=>$freekm])->get('bookingswebBglojd76');
                        
                        if(isset($booking['error'])){
                            $message = $booking['error'];

                            //SendEmailToAdmin     
                            $data = ['fromDate' => $startDate, 'toDate' => $endDate, 'fromTime' => $startTime, 'toTime' => $endTime, 
                                'areaName' => $areaName, 'amount' => $amount,
                                'userFname' => $userFname, 'modelName' => $modelName, 
                                'userEmail' => $userEmail, 'userMob' => $userMob, 'reason' => $message ];
                            Mail::send('emails.admin_paytm_failure_mail',$data,function($message) use ($data) {
                                  $message->to(env('ADMIN_EMAIL'));
                                  $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                                  $message->subject('PayTM Problem '.$data['userFname']);
                            });
                        }
                        
                        CancelledEnquiry::where('enquiry',$enquiry)->where('email',$userEmail)->delete();



                        $refId = $booking['reference_id'];

                        
                        $modelImage = Image::where('id', $model['thumbnail_img_id'])->first();
                        $modelName = $model['bike_model'];
                        $makeFromId = BikeMake::where('id',$model->bike_make_id)->first();
                        $makeName = $makeFromId['bike_make'];
                        $modelThumbnail = $modelImage['full'];
                        
                        $areaAddress = $area['address'];
                        
                        $sdCarbonObj = Carbon::parse($start_date_time);
                        $edCarbonObj = Carbon::parse($end_date_time);
                        $hours = $edCarbonObj->diffInHours($sdCarbonObj);
                        
                        $ss = $hours*60*60;
                        $s = $ss%60;
                        $m = floor(($ss%3600)/60);
                        $h = floor(($ss%86400)/3600);
                        $d = floor(($ss%2592000)/86400);
                        $M = floor($ss/2592000);

                        $duration = $d. " days, .".$h." hours";
                        $unit = "Days";
                        
                        $now = Carbon::now();
                        
                        DB::table('paytm_failure')->insert(['booking_id' => $refId,'created_at' =>$now,'order_id'=>$order_id]);


                        $data = ['fromDate' => $startDate, 'toDate' => $endDate, 'fromTime' => $startTime, 'toTime' => $endTime, 
                                'areaName' => $areaName, 'amount' => $amount, 'duration' => $duration, 'areaAddress' => $areaAddress, 
                                'userFname' => $userFname, 'modelName' => $modelName, 'unit' => $unit, 'modelThumbnail' => $modelThumbnail, 
                                'userEmail' => $userEmail, 'userMob' => $userMob, 'note' => '', 'bookingId' => $refId, 
                                'kmLimitMessage'=>$kmLimitMessage, 'areaMapLink'=>$areaMapLink ];

                        Mail::send('emails.user_booking_mail',$data,function($message) use ($data) {
                          $message->to($data['userEmail']);
                          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                          $message->subject('WickedRide Booking - '.$data['bookingId']);
                        });

                        Mail::send('emails.admin_booking_mail',$data,function($message) use ($data) {
                        //var_dump($data);
                          $message->to(env('ADMIN_EMAIL'));
                          $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                          $message->subject('WickedRide '.$data['userFname'].' Booking - '.$data['bookingId']);
                        });

                        // PROMOCODE CHANGE AVAILABLE COUNT
                        $promocode = PromoCode::where('code', $coupon)->first();
                        if ($promocode) {
                            $promocode['available_count'] = $promocode['available_count'] - 1;
                            $promocode->save();
                        }
                    }
                    else
                    {
                        //SendEmailToAdmin 
                        
                        $data = ['fromDate' => $startDate, 'toDate' => $endDate, 'fromTime' => $startTime, 'toTime' => $endTime, 
                                'areaName' => $areaName, 'amount' => $amount,
                                'userFname' => $userFname, 'modelName' => $modelName, 
                                'userEmail' => $userEmail, 'userMob' => $userMob, 'reason' => 'Total Amount not right - User paid'.$responseParamList['TXNAMOUNT'] ];
                        Mail::send('emails.admin_paytm_failure_mail',$data,function($message) use ($data) {
                              $message->to(env('ADMIN_EMAIL'));
                              $message->from(env('ADMIN_EMAIL'), env('ADMIN_NAME'));
                              $message->subject('PayTM Problem '.$data['userFname']);
                        });
                    }

                }
            }
            else
            {
                
                $count = CancelledEnquiry::where('email',$userEmail)->where('enquiry',$enquiry)->count();
                if($count == 0)
                {
                    CancelledEnquiry::create(['name'=>$userFname." ".$userLname, 'phone'=>$userMob, 'email'=>$userEmail, 'enquiry'=>$enquiry, 'amount'=>$amount,'mode'=>'paytm']);
                
                }   
                
            }
        }
        return "done";
        
    }
}
