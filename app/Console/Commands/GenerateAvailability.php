<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Booking;
use App\Http\Controllers\WorkingSlotController;
use App\Models\Slot;
use App\Models\Bike;
use App\Models\Availability;
use DB;

class GenerateAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'availability:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->comment(PHP_EOL."".PHP_EOL);
        ini_set('max_execution_time', 600);
        DB::table('availability')->delete();
        $today = Carbon::today();
        $bookings = Booking::where('end_datetime', '>=', $today->toDateTimeString())->get();
        foreach ($bookings as $booking) {
            $startDateObj = Carbon::parse($booking['start_datetime']);
            $endDateObj = Carbon::parse($booking['end_datetime']);

            $st = $startDateObj->copy();
            $ed = $endDateObj->copy();

            $startSlot = Slot::where('start_time', $startDateObj->toTimeString())->first();
            $endSlot = Slot::where('start_time', $endDateObj->toTimeString())->first();
            $startSlot = $startSlot->toArray();
            $endSlot = $endSlot->toArray();

            $booking['start_slot_id'] = $startSlot['id'];
            $booking['end_slot_id'] = $endSlot['id'];
            $booking->save();

            while ($startDateObj <= $endDateObj) {
                $availableSlots = null;
                if ($st->toDateString() == $ed->toDateString()) {
                    $availableSlotsSt = WorkingSlotController::getAvailableSlotsIdsStart($startDateObj->toDateString(), $startSlot['id']);
                    $availableSlotsEd = WorkingSlotController::getAvailableSlotsIdsEnd($endDateObj->toDateString(), $endSlot['id']);
                    $availableSlotsSt = is_array($availableSlotsSt)? $availableSlotsSt : [$availableSlotsSt];
                    $availableSlotsEd = is_array($availableSlotsEd)? $availableSlotsEd : [$availableSlotsEd];
                    $availableSlots = array_merge($availableSlotsSt, $availableSlotsEd);
                    $availableSlots = implode(',', $availableSlots);
                } else {
                    if ($startDateObj->toDateString() == $st->toDateString()) {
                        $availableSlots = WorkingSlotController::getAvailableSlotsIdsStart($startDateObj->toDateString(), $startSlot['id']);
                    }
                    elseif ($startDateObj->toDateString() == $ed->toDateString()) {
                        $availableSlots = WorkingSlotController::getAvailableSlotsIdsEnd($endDateObj->toDateString(), $endSlot['id']);
                    }
                    else
                        $availableSlots = null;
                }
                $bike = Bike::where('id', $booking['bike_id'])->first();
                if($bike){
                    $model = $bike->bikeModel;
                    $area = $bike->area;
                    $make = $bike->make();
                    $city = $bike->city();
                }

                $availability = Availability::create(['city_id' => $city['id'], 'area_id' => $area['id'],
                  'make_id' => $make['id'], 'model_id' => $model['id'],
                  'bike_id' => $bike['id'], 'date' => $startDateObj->toDateString(),
                  'slots' => $availableSlots, 'booking_id' => $booking['id']
                ]);

                $startDateObj->addDay();
            }
        }

    }
}
