<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\GenerateAvailability::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call('App\Http\Controllers\BookingController@adminEmail')->daily();
        $schedule->call('App\Http\Controllers\BikeAvailabilityController@addOneDayCapacity')->dailyAt('00:01');
        
        $schedule->call('App\Http\Controllers\BookingController@sendBookingSMSToCustomers')->hourly();
        $schedule->call('App\Http\Controllers\OneWayRentalController@toggleAllActiveAreaBikeStatus')->hourly();
        $schedule->call('App\Http\Controllers\BikeAvailabilityController@returnBikesToLocation')->hourly();
        $schedule->call('App\Http\Controllers\FrontViewController@storeHourlyResponses')->hourly();
        
    }
}
