<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BikeModelImage extends Model
{

    protected $table = 'bikeModel_images';

    protected $fillable = ['bikeModel_id','handler'];

    public $timestamps = false;

}
