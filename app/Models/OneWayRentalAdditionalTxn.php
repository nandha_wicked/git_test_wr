<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalAdditionalTxn extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_additional_txn';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','booking_id','amount','credit_or_debit','payment_method','amount_paid_payment_gateway','payment_id','wallet_amount_applied','category','notes','created_at','created_by','updated_at','updated_by','status'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
   
}
