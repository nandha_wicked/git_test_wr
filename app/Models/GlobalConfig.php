<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GlobalConfig extends Model{

    protected $table = 'global_config';

    protected $fillable = ['id','parameter','value','active_status','created_at','created_by','updated_at','updated_by'];

    public $timestamps = false;

}//END OF CLASS