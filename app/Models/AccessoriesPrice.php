<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Bike;
use App\Models\City;


  class AccessoriesPrice extends Model
  {

      protected $table = 'accessories_prices';

      protected $fillable = ['id','model_id','area_id','price_per_hour','minimum_hours','hour_weekday','hour_weekend','day_weekday','day_weekend','five_weekday','week','month','created_at','created_by','updated_at','updated_by'];
      
      
      public function getModel()
      {
          $model = AccessoriesModel::where('id',$this->model_id)->first();
          return $model->model;
      }
      
      public function getArea()
      {
          if($this->area_id == 0)
              return "All";
          $area = Area::where('id',$this->area_id)->first();
          return $area->area;
      }


                 
  }
