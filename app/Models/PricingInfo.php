<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class PricingInfo extends Model{

    protected $table = 'pricing_info';

    protected $fillable = ['id','bike_or_accessory','reservation_id','pricing_id','full_price','promo_discount','price_after_discount','promo_code','tax_id','taxable_price','tax_amount','price_after_tax','deleted_at','deleted_by','deleted'];


    public $timestamps = false;

  }//END OF CLASS