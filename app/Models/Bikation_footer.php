<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Bikation_footer extends Model
{

  protected $table = 'bikation_footer';

  protected $fillable = ['desc'];

  
  function get_Bikationfooter(){
    $bikeModels = DB::table('bikation_footer')->get();

    foreach($bikeModels as $bikeModel){
      $bikeModel->desc = $bikeModel->desc;
    }  
    return $bikeModels;  
  }
  
  function getBikeModel_thumb($model_id){
    $bikeModel = BikeModel::where('id', $model_id)->first();
    return $bikeModel->thumbnail_img_id;
  }
}
