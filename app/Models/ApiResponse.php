<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiResponse extends Model{

    protected $table = 'api_response';

    protected $fillable = ['id','call_id','language','description','response','created_at','created_by','updated_at','updated_by','status'];





    public $timestamps = false;


    

}//END OF CLASS
