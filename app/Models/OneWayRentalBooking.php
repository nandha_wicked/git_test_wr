<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;


class OneWayRentalBooking extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_booking';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','reference_id','user_id','from_location','to_location','model_id','bike_id','status','price','amount_paid_payment_gateway','payment_method','payment_id','wallet_amount_applied','customer_instructions','promocode','estimated_duration','estimated_distance','start_odo','delivery_ending_odo','return_ending_odo','end_odo','created_at','created_by','updated_at','updated_by','delivered_at','delivered_by','returned_at','returned_by','from_location_longitude','from_location_latitude','to_location_longitude','to_location_latitude','delivery_assigned_at','delivery_assigned_to','return_assigned_to','return_assigned_at'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function fromArea()
    {
        return $this->belongsTo('App\Models\OneWayRentalLocation','from_location');
    }
    
    public function toArea()
    {
        return $this->belongsTo('App\Models\OneWayRentalLocation','to_location');
    }
    
    
    public function bike()
    {
        return $this->belongsTo('App\Models\OneWayRentalBike','bike_id');
    }
    
    
    public function model(){
        return $this->belongsTo('App\Models\BikeModel','model_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function pricingInfo()
    {
        return $this->hasOne('App\Models\OneWayRentalPricingInfo','booking_id')->orderBy('id', 'desc');
    }
    
    public function financialInfo()
    {
        return $this->hasMany('App\Models\OneWayRentalFinancialInfo','booking_id');
    } 
    
    public function deliveryInfo()
    {
        return $this->hasOne('App\Models\OneWayRentalDeliveryInfo','booking_id');
    }
    
    public function returnInfo()
    {
        return $this->hasOne('App\Models\OneWayRentalReturnInfo','booking_id');
    }
    
    public function bookingImages()
    {
        return $this->hasMany('App\Models\OneWayRentalBookingImage','booking_id');
    }

    public function deliveredBy()
    {
        return $this->belongsTo('App\Models\User','delivered_by');
    }

    public function returnedBy()
    {
        return $this->belongsTo('App\Models\User','returned_by');
    }

    
    function getUser(){
        $user = User::where('id', $this->user_id)->first();
        return $user;
    }

    
    
    function getBikeModelName(){
        $bikeModel = BikeModel::where('id', $this->model_id)->first();
        return $bikeModel->bike_model;
    }
    
    
    function getCityName(){
        $area = Area::where('id', $this->area_id)->first();
        $city_id = $area->city_id;
        $city = City::where('id', $city_id)->first();
        return $city->city;
    }
    
    
    function getBikeMakeName(){
        $bikeModel = BikeModel::where('id', $this->model_id)->first();
        $bike_make_id = $bikeModel->bike_make_id;
        $bikeMake = BikeMake::where('id', $bike_make_id)->first();
        return $bikeMake->bike_make;    
    }
    
    public function getFromLocation()
    {
        if($this->from_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->from_location)->first();
        return $location->name;
    }
    
    
    public function getToLocation()
    {
        if($this->to_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->to_location)->first();
        return $location->name;
    }
    
    
    public function getBikeNumber()
    {
        $bike = OneWayRentalBike::where('id',$this->bike_id)->first();
        if(!$bike)
            return "Bike Deleted.";
        return $bike->bike_number;
    }
    
    function getReadableDateTime($dateTime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($dateTime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($dateTime)->format('j M-y ga');
        }
        return Carbon::parse($dateTime)->format('j M ga');
    }
    
    function getReadableDateTimeWithMinutes($dateTime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($dateTime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($dateTime)->format('j M-y g:i a');
        }
        return Carbon::parse($dateTime)->format('j M g:i a');
    }
    
   
}
