<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiArgument extends Model{

    protected $table = 'api_argument';

    protected $fillable = ['id','call_id','parent_argument','argument','required_or_optional','description','type','priority','created_at','created_by','updated_at','updated_by','status'];



    public $timestamps = false;


    

}//END OF CLASS
