<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneWayRentalBookingImage extends Model{

    protected $table = 'a2b_booking_images';

    protected $fillable = ['id','booking_id','user_id','type','image_url','created_by','created_at','updated_by','updated_at'];

    public $timestamps = false;

}//END OF CLASS
