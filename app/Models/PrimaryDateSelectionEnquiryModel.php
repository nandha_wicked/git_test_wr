<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrimaryDateSelectionEnquiryModel extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'primary_date_selection_enquiry_model';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','date_selection_enquiry_id', 'model_id', 'is_sold_out','area_id','created_at','updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}
