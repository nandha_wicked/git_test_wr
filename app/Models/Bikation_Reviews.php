<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Reviews extends Model
{

  protected $table = 'bikation_reviews';

  protected $fillable = ['Trip_ID','Customer_ID', 'Vendor_ID','Overall','Planning','On_Schedule', 'Food', 'Stay', 'Activities', 'Remarks'];

}
