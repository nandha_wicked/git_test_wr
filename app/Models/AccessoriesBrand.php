<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Image;


class AccessoriesBrand extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories_brand';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','brand','logo','country_of_origin','description','created_at','updated_at','created_by','updated_by','status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function getLogo()
    {
        $image = Image::where('id',$this->logo)->first();
        if(!$image)
            $imageurl = "";
        else
            $imageurl = $image->full;
        return $imageurl;
    }
   
}
