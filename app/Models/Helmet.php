<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Helmet extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'helmet';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','item_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}
