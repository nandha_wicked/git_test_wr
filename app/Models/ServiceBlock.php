<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Booking;
use Carbon\Carbon;
 

class ServiceBlock extends Model
{
    protected $table = 'service_block';
    protected $fillable = ['id','model_id','bike_number_id','area_id','created_by','created_at','approved_by','approval_status','note','booking_id','start_datetime','end_datetime','booking_status','booked_by','unblocked_by','status','unblocked_at'];
    
    public $timestamps = false;
    
    function getBikeNumber(){
        $bike_number = BikeNumbers::where('id', $this->bike_number_id)->first();
        if(!$bike_number)
            return "Bike Number Deleted";
        else
            return $bike_number->number;
    }
    function getAreaName(){
        $area = Area::where('id', $this->area_id)->first();
        return $area->area;
    }
    function getModelName(){
        $model = BikeModel::where('id', $this->model_id)->first();
        return $model->bike_model;
    }
    function getCreatedByEmail(){
        $user = User::where('id', $this->created_by)->first();
        return $user->email;
    }
    function getApprovedByEmail(){
        $user = User::where('id', $this->approved_by)->first();
        if(!$user)
        {
            return "NA";
        }
        else
        {
            return $user->email;
        }
        
    }
    function getBookedByEmail(){
        $user = User::where('id', $this->booked_by)->first();
        if(!$user)
        {
            return "NA";
        }
        else
        {
            return $user->email;
        }
        
    }
    function getBookedAt(){
        $booking = Booking::where('id', $this->booking_id)->first();
        if(!$booking)
        {
            return "";
        }
        else
        {
            return $booking->created_at;
        }
        
    }
    function getUnblockedByEmail(){
        $user = User::where('id', $this->unblocked_by)->first();
        if(!$user)
        {
            return "";
        }
        else
        {
            return $user->email;
        }
        
    }
    function getUnblockedAt(){
        if($this->unblocked_at == '0000-00-00 00:00:00')
        {
            return "";
        }
        else
        {
            return $this->unblocked_at;
        }
        
    }
    function getDuration(){
        $start =  Carbon::parse($this->start_datetime);
        $end =  Carbon::parse($this->end_datetime);
        
        return $start->diffForHumans($end,true);
        
    }
    
    function getReadableDateTime($dateTime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($dateTime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($dateTime)->format('j M-y ga');
        }
        return Carbon::parse($dateTime)->format('j M ga');
    }
    
}


