<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class NotesInfo extends Model{

    protected $table = 'notes_info';

    protected $fillable = ['id','booking_id','reservation_id','nature','text','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted'];

    public $timestamps = false;
    
    public function createdByUser()
    {
        return $this->belongsTo('App\Models\User','created_by');
    }

  }//END OF CLASS
