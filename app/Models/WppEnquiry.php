<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WppEnquiry extends Model
{

    protected $table = 'wpp_enquiry';

    protected $fillable = ['fullName','bikeinterested','email','mobileNumber','bikes_count', 'loan','notes','updated_by'];

    public $timestamps = false;
    
    function loanNeeded(){
        if($this->loan=='true')
            return "Yes";
        else
            return "No";
    }


}

