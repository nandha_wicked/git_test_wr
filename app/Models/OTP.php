<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class OTP extends Model{

    protected $table = 'a2b_otp';

    protected $fillable = ['id','mobile_otp','otp_verification_tries','mobile_num','otp_for','created_at','updated_at'];


  }//END OF CLASS
