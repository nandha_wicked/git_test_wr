<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class ReturnInfo extends Model{

    protected $table = 'return_info';

    protected $fillable = ['id','bike_or_accessory','reservation_id','returned_by','returned_at','ending_odo','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted'];

    public $timestamps = false;

  }//END OF CLASS
