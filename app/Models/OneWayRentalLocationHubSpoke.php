<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalLocationHubSpoke extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_hub_spoke';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','hub_area','spoke_area','created_at','created_by','updated_at','deleted_at','deleted_by','deleted'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    
   
}
