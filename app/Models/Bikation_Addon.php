<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Addon extends Model
{

  protected $table = 'bikation_addon';

  protected $fillable = ['Trip_ID','Package_ID', 'Activity_ID','Vendor_ID','Exclusion_Inclusion','Std_Opt', 'Cost','Status'];


}
