<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RandMServiceDealer extends Model{

    protected $table = 'randm_service_dealer';

    protected $fillable = ['id','name','city','state','country','address','postal_code','gst','contact_person_name','contact_person_phone','contact_person_email','created_by','created_at','updated_by','updated_at','status','deleted'];





    public $timestamps = false;


    

}//END OF CLASS