<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class PaymentNature extends Model{

    protected $table = 'payment_nature';

    protected $fillable = ['id','nature','tax','reversal','invoice_type'];

    public $timestamps = false;

  }//END OF CLASS
