<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class UserCohort extends Model{

    protected $table = 'user_cohort';

    protected $fillable = ['user_id','user_email','user_phone','total_number_of_bookings','total_amount','booking_ids'];

    public $timestamps = false;

  }//END OF CLASS
