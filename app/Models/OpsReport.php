<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class OpsReport extends Model{
    protected $table = 'ops_report';

    protected $fillable = ['date','report'];

    public $timestamps = false;

    

  }
