<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Bike;
use App\Models\Helmet;
use App\Models\Jacket;
use App\Models\BikeNumbers;
use Carbon\Carbon;
use Mail;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Models\Booking;
use App\Models\BikeModel;
use App\Models\Price;
use App\Models\Area;
use App\Models\Image;
use App\Models\Slot;
use App\Models\PromoCode;
use App\Models\BikerPlanSubscription;
use App\Models\SentTransaction;
use Session;
use Config;
use Razorpay\Api\Api;

class BookingEnquiry extends Model
{
  protected $table = 'bookings_enquiry';

  protected $fillable = ['id','start_datetime','end_datetime', 'user_id','area_id','model_id','promocode'];

  //public $timestamps = false;


  

  function getUserName(){
    $user = User::where('id', $this->user_id)->first();
    return $user->first_name;
  }
    
  function getUserLastName(){
    $user = User::where('id', $this->user_id)->first();
    return $user->last_name;
  }

  function getUserEmail(){
    $user = User::where('id', $this->user_id)->first();
    return $user->email;
  }
    
  function getCreatedByEmail(){
      if($this->createdBy==0)
      {
            return "Not Present"; 
      }
      else
      {
            $user = User::where('id', $this->createdBy)->first();
            return $user->email;
      }

  }
    
  function getDeliveredByEmail(){
     $user = User::where('id',$this->delivered_by)->first();
      if(!$user)
      {
            return "Not Delivered"; 
      }
      else
      {
            return $user->email;
      }

  }
    
  function getUserMobileNum(){
    $user = User::where('id', $this->user_id)->first();
    return $user->mobile_num;
  }

  function getBikeMakeName(){
    $bikeModel = BikeModel::where('id', $this->model_id)->first();
    $bike_make_id = $bikeModel->bike_make_id;
    $bikeMake = BikeMake::where('id', $bike_make_id)->first();
    return $bikeMake->bike_make;    
  }

  function getBikeModelName(){
    $bikeModel = BikeModel::where('id', $this->model_id)->first();
    return $bikeModel->bike_model;
  }

  function getCityName(){
    
    $area = Area::where('id', $this->area_id)->first();
    $city_id = $area->city_id;
    $city = City::where('id', $city_id)->first();
    return $city->city;
  }

  function getCityId(){
    
    $area = Area::where('id', $this->area_id)->first();
    
    return $area->city_id;
  }

  function getAreaName(){
    $area = Area::where('id', $this->area_id)->first();
    return $area->area;
  }

  function getExtraKMPrice(){
      
    $make = BikeModel::where('id',$this->model_id)->pluck('bike_make_id');
    if(in_array($make,[1,2,4,5,6]))
    {
        $extrakmprice = env('EXTRAKMPRE');  
    }
    else
    {
        $extrakmprice = env('EXTRAKMRE');  
    }
    return $extrakmprice;
      
  }

  function getExtraKMPriceMessage(){
    
    $startDate = substr($this->start_datetime,0,10);
    $startTime = substr($this->start_datetime,11);


    $endDate = substr($this->end_datetime,0,10);
    $endTime = substr($this->end_datetime,11);
   
    $dispatcher = app('Dingo\Api\Dispatcher');
    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,
                                        'start_time' => $startTime, 'end_time' => $endTime,
                                        'model_id' => $this->model_id, 'area_id' => $this->area_id, 'city_id' => $this->getCityId()
                                      ])->get('bookings/price-and-check-availability');
      
    $number_of_hours = $availability['number_of_hours'];
    $minimum_hours = $availability['minimum_hours'];

    $max_hours = max($number_of_hours,$minimum_hours);
      
    $make = BikeModel::where('id',$this->model_id)->pluck('bike_make_id');
        if(in_array($make,[1,2,4,5,6]))
           {
             $extrakmprice = env('EXTRAKMPRE');  
           }
        else
           {
             $extrakmprice = env('EXTRAKMRE');  
           }
    $freekm = $max_hours*env('KMLIMIT');

    $kmLimitMessage = $freekm." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";
    
    return $kmLimitMessage;  
  }

  function getAvailability(){
    
    $startDate = substr($this->start_datetime,0,10);
    $startTime = substr($this->start_datetime,11);


    $endDate = substr($this->end_datetime,0,10);
    $endTime = substr($this->end_datetime,11);
   
    $dispatcher = app('Dingo\Api\Dispatcher');
    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,
                                        'start_time' => $startTime, 'end_time' => $endTime,
                                        'model_id' => $this->model_id, 'area_id' => $this->area_id, 'city_id' => $this->getCityId()
                                      ])->get('bookings/price-and-check-availability');
      
    return $availability;
      
  }

  function createBooking(){
    
    $startDate = substr($this->start_datetime,0,10);
    $startTime = substr($this->start_datetime,11);


    $endDate = substr($this->end_datetime,0,10);
    $endTime = substr($this->end_datetime,11);
   
    $dispatcher = app('Dingo\Api\Dispatcher');
    $availability = $dispatcher->with(['start_date' => $startDate, 'end_date' => $endDate,
                                        'start_time' => $startTime, 'end_time' => $endTime,
                                        'model_id' => $this->model_id, 'area_id' => $this->area_id, 'city_id' => $this->getCityId()
                                      ])->get('bookings/price-and-check-availability');
      
    return $availability;
      
  }
    

  
    
    
    
    


  


  
    
   
    
    
}
