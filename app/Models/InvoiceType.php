<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class InvoiceType extends Model{

    protected $table = 'invoice_type';

    protected $fillable = ['id','type','display'];

    public $timestamps = false;

  }//END OF CLASS