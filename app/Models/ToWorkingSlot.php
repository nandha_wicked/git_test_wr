<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\Slot;

  class ToWorkingSlot extends Model{
    protected $table = 'to_working_slots';

    protected $fillable = ['day_of_week','slots','city_id'];

    public $timestamps = false;

    public static function getSlots($date){
    	$slotNo = date('N', strtotime($date));
      $workingSlot = WorkingSlot::where('day_of_week', $slotNo)->get();
      $slotIdsArr = explode(',', $workingSlot[0]['slots']);
      $availableSlots = Slot::whereIn('id', $slotIdsArr)->get();
      // $data = ['slots' => $availableSlots];
      // return $this->respondWithSuccess($data);
      return $availableSlots;
    }

 }
