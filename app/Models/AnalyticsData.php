<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class AnalyticsData extends Model{

    protected $table = 'analysis_data';

    protected $fillable = ['area_name','model_name','reservation_hours','service_hours','marketing_hours','movement_hours','admin_hours','reservation_hours_weekday','service_hours_weekday','marketing_hours_weekday','movement_hours_weekday','admin_hours_weekday','reservation_hours_weekend','service_hours_weekend','marketing_hours_weekend','movement_hours_weekend','admin_hours_weekend'];

    public $timestamps = false;

  }//END OF CLASS