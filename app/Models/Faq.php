<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model{

    protected $table = 'faq';

    protected $fillable = ['id','question','answer','created_at','created_by','updated_at','updated_by','status'];

    public $timestamps = false;


}//END OF CLASS