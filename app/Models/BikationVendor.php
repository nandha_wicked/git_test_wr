<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BikationVendor extends Model
{

    protected $table = 'bikationvendor_users';

    protected $fillable = ['first_name', 'last_name', 'email', 'mobile_num', 'password','status','Address_ID','Bank','Bank_Account_Number','IFSC', 'PAN', 'TAN', 'SRN', 'CIN','TIN','created_on','updated_on','Inactive_on','Inactive_since', 'NDA_ID', 'Agreement_ID', '	Created_By', 'Overall_Rating','Vendor_Image','Contact_person','Contact_Email','Contact_Phone_Number','Started_On'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];
  /* Relationships */

}
