<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BikeDocument extends Model{

    protected $table = 'bike_document';

    protected $fillable = ['id','bike_id','doc_type','url','created_by','created_at','updated_by','updated_at','deleted','deleted_by','deleted_at'];
    public $timestamps = false;

    /* Relationships*/
    public function bike() {
        return $this->belongsTo('App\Models\Bike', 'bike_id'); 
    }


}
