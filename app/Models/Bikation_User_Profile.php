<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_User_Profile extends Model
{

  protected $table = 'bikationvendor_users';

  protected $fillable = ['first_name','last_name','email', 'mobile_num','password','Bank', 'Bank_Account_Number', 'IFSC', 'PAN', 'TAN','SRN','CIN','TIN', 'NDA_ID','Agreement_ID', 'Vendor_Image', 'Contact_person', 'Contact_Email','Contact_Phone_Number'];

}
