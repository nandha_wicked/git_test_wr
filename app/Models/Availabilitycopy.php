<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Availabilitycopy extends Model{

    protected $table = 'availabilitycopy';

    protected $fillable = ['id','city_id','area_id','make_id','model_id', 'bike_id', 'date', 'slots', 'status', 'booking_id'];

    public $timestamps = false;

  }//END OF CLASS