<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialInfo extends Model{

    protected $table = 'financial_info';

    protected $fillable = ['id','booking_id','debit_or_credit','new_or_edit','reservation_id','old_reservation','edited_reservation','amount','old_amount','edited_amount','payment_method','payment_id','payment_nature','status','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted','user_id'];

    public $timestamps = false;
    
    
    public function paymentMethodName()
    {
        return $this->belongsTo('App\Models\PaymentMethods','payment_method');
    }

    public function paymentNature()
    {
        return $this->belongsTo('App\Models\PaymentNature','payment_nature');
    }

}//END OF CLASS
