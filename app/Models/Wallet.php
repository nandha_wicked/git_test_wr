<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model{

    protected $table = 'wallet';

    protected $fillable = ['user_id','nature','notes','booking_id','amount','promotional_amount','non_promotional_amount','balance','credit_or_debit','created_at','created_by','updated_at','updated_by','cross_reference_wallet_id','promotional','promotional_balance','non_promotional_balance'];

    public $timestamps = false;
    
    public function getUserEmail()
    {
        $user = User::where('id',$this->user_id)->first();
        return $user->email;
    }
    
    public function getBookingId()
    {
        if($this->booking_id == 0)
        {
            return "NA";
        }
        else
        {
            if($this->nature == "Redeemed for a One Way Rental Boooking")
            {
                $booking = OneWayRentalBooking::where('id',$this->booking_id)->first();
            }
            else
            {
                $booking = Booking::where('id',$this->booking_id)->first();
            }
            
            if(!$booking)
                return "Deleted";
            
            return $booking->reference_id;
        }
    }
    
    public function getCreatedByUserEmail()
    {
        $user = User::where('id',$this->created_by)->first();
        return $user->email;
    }


}//END OF CLASS
