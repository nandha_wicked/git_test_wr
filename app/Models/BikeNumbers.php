<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BikeNumbers extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bike_numbers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','number', 'make', 'model'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    function getModelName(){
        $model = BikeModel::where('id', $this->model)->first();
        return $model->bike_model;
    }
    
}
