<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AggregatedBikes extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'aggregated_bikes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','model_id', 'bike_number_id', 'date_of_addition','cities','aggregator_id','commission_perc'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    function getAggregatorName(){
        $user = Aggregator::where('id', $this->aggregator_id)->first();
        return $user->name;
    }
    
    function getModelName(){
        $model = BikeModel::where('id', $this->model_id)->first();
        return $model->bike_model;
    }
    
    function getBikeNumber(){
        $bike_number = BikeNumbers::where('id', $this->bike_number_id)->first();
        return $bike_number->number;
    }
    
    function getCityNames(){
        $city_ids = explode(',',$this->cities);
        $cities =[];
        foreach($city_ids as $city_id)
        {
            array_push($cities,City::where('id',$city_id)->pluck('city'));
        }

        return implode(',',$cities);
    }
   
    
}
