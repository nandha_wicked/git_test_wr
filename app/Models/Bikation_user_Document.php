<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bikation_user_Document extends Model
{

  protected $table = 'bikation_user_document';

  protected $fillable = ['user_id','doc_type','doc','doc_reason','status'];

}
