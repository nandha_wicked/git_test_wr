<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalMovementOrder extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_movement_order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','from_location','to_location','model_id','bike_id','priority','status','notes','created_at','created_by','updated_at','updated_by','delivered_at','delivered_by','returned_at','returned_by','from_location_longitude','from_location_latitude','to_location_longitude','to_location_latitude','start_odo','end_odo','delivery_assigned_at','delivery_assigned_to','return_assigned_to','return_assigned_at'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public function fromArea()
    {
        return $this->belongsTo('App\Models\OneWayRentalLocation','from_location');
    }

    public function toArea()
    {
        return $this->belongsTo('App\Models\OneWayRentalLocation','to_location');
    }

    public function deliveredBy()
    {
        return $this->belongsTo('App\Models\User','delivered_by');
    }

    public function returnedBy()
    {
        return $this->belongsTo('App\Models\User','returned_by');
    }

    public function bike()
    {
        return $this->belongsTo('App\Models\OneWayRentalBike','bike_id');
    }
    
    function getBikeModelName(){
        $bikeModel = BikeModel::where('id', $this->model_id)->first();
        return $bikeModel->bike_model;
    }
    
    
    function getCityName(){
        $area = Area::where('id', $this->area_id)->first();
        $city_id = $area->city_id;
        $city = City::where('id', $city_id)->first();
        return $city->city;
    }
    
    
    function getBikeMakeName(){
        $bikeModel = BikeModel::where('id', $this->model_id)->first();
        $bike_make_id = $bikeModel->bike_make_id;
        $bikeMake = BikeMake::where('id', $bike_make_id)->first();
        return $bikeMake->bike_make;    
    }
    
    public function getFromLocation()
    {
        if($this->from_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->from_location)->first();
        return $location->name;
    }
    
    
    public function getToLocation()
    {
        if($this->to_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->to_location)->first();
        return $location->name;
    }
    
    
    public function getBikeNumber()
    {
        $bike = OneWayRentalBike::where('id',$this->bike_id)->first();
        if(!$bike)
            return "";
        return $bike->bike_number;
    }
    
    public function getUser()
    {
        return "";
    }
    
   
}
