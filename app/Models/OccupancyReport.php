<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OccupancyReport extends Model
{
   	protected $table = 'occupancy_report';

    protected $fillable = ['date_time','area_name', 'model_name', 'service', 'marketing', 'reservations', 'movement','admin'];
 
}
