<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiTable extends Model{

    protected $table = 'api_table';

    protected $fillable = ['id','call_id','title','description','language','left_title','right_title','created_at','created_by','updated_at','updated_by','status'];




    public $timestamps = false;


    

}//END OF CLASS