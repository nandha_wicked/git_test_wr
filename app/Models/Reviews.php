<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reviews extends Model
{
    protected $table = 'reviews';

    protected $fillable = ['name','review', 'image_id','status','reviewed_on'];

    public $timestamps = false;

   
    function getAllActiveReviews(){

    	$activeReviews = Reviews::where('status', 1)->get();
    	return $activeReviews;
    }
     function getAllReviews(){

    	$activeReviews = Reviews::all();
    	return $activeReviews;
    }

    

}

