<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class SentTransaction extends Model{
    protected $table = 'sent_transaction';

    protected $fillable = ['user_id','model_id','area_id','start_date_time','end_date_time','payment_gateway','order_id','amount','promocode','status','response'];

    public $timestamps = false;

    

  }
