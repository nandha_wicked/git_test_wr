<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\Gallery;
  use App\Transformers\ImageTransformer;

  class Image extends Model{
    protected $table = 'images';
    protected $fillable = ['full','small', 'medium', 'large'];
    protected $hidden = ['created_at', 'updated_at', 'raw_data'];
    public $timestamps = true;

    /* Relationships*/
    public function gallery() {
    	return $this->belongsToMany('App\Models\Gallery', 'gallery_image'); 
    }

    public function transform(){
      $it = new ImageTransformer();
      return $it->transform($this);
    }

  }
