<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalMovementLog extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_movement_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','bike_id','model_id','from_location','to_location','reason','ref_id','start_datetime','end_datetime','status'];





    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    
   
}
