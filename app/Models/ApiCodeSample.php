<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCodeSample extends Model{

    protected $table = 'api_code_sample';

    protected $fillable = ['id','call_id','language','description','code','created_at','created_by','updated_at','updated_by','status'];


    public $timestamps = false;


    

}//END OF CLASS