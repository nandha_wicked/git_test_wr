<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

  class Sendmail extends Model{
    protected $table = 'sendmail';

    protected $fillable = ['email_send','_token'];

    public $timestamps = false;

   
  }
