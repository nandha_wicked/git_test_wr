<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
   	protected $table = 'promo_codes';

    protected $fillable = ['code','title', 'description', 'value_type', 'value', 'total_count', 'available_count', 'start_set', 'end_set', 'new_user', 'min_order', 'area_ids', 'model_ids', 'weekday', 'weekend', 'dates', 'status','minimum_hours','max_value','weekday_exempt'];
 
}
