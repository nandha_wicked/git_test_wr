<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalExcludedRoutes extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_excluded_routes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','from_location','to_location','created_at','created_by','updated_at','updated_by','status'];
    
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
   
}
