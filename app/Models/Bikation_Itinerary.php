<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Itinerary extends Model
{

  protected $table = 'bikation_itinerary';

  protected $fillable = ['Trip_ID','Day','Text_ID','Vendor_ID', 'Stay_at','Image_ID','Video_ID','Status'];

}
