<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RandMInventoryRegister extends Model{

    protected $table = 'randm_inventory_register';

    protected $fillable = ['id','inventory','dealer','qty','cost','nature','reference','created_at','created_by','updated_at','updated_by','status','deleted'];

    public $timestamps = false;

    public function getInventory()
    {
        $inventory = RandMInventory::where('id',$this->inventory)->first();
        return $inventory;
    }
    
    public function getDealerName()
    {
        if($this->dealer==0)
            return "NA";
        $dealer = RandMServiceDealer::where('id',$this->dealer)->first();
        return $dealer->name;
    }
    

}//END OF CLASS
