<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RandMServiceRecord extends Model{

    protected $table = 'randm_service_record';

    protected $fillable = ['id','bike','service','service_dealer','start_datetime','end_datetime','current_odo','cost','created_at','created_by','updated_at','updated_by','status','reference','deleted'];





    public $timestamps = false;


    public function getService()
    {
        $service = RandMService::where('id',$this->service)->first();
        return $service;
    }
    
    public function getDealerName()
    {
        $dealer = RandMServiceDealer::where('id',$this->service_dealer)->first();
        return $dealer->name;
    }

}//END OF CLASS
