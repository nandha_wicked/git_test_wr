<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Occupancy extends Model{

    protected $table = 'occupancy';

    protected $fillable = ['date','area_id','model_id','hour_string'];

    public $timestamps = false;

  }//END OF CLASS