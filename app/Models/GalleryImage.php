<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\Gallery;
  use App\Models\Image;

  class GalleryImage extends Model{
    protected $table = 'gallery_image';
    protected $fillable = ['gallery_id','image_id'];
    public $timestamps = false;

    /* Relationships*/
    public function gallery(){
      return $this->belongsTo('App\Models\Gallery');
    }

    public function image(){
      return $this->belongsTo('App\Models\Image');
    }
  }
