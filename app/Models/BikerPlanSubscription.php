<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Bike;
use Carbon\Carbon;
use Mail;

class BikerPlanSubscription extends Model
{
  protected $table = 'biker_plan_subscription';

  protected $fillable = ['id', 'user_id','biker_plan_id', 'total_price', 'status', 'pg_status', 'pg_txn_id', 'bank_txn_id', 'order_id', 'pg_mode', 'pg_txn_amount', 'note', 'raw_data', 'reference_id','promocode','createdBy'];

  //public $timestamps = false;

  /* Relationships */


  function getUserName($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->first_name;
  }
  function getUserLastName($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->last_name;
  }

  function getUserEmail($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->email;
  }
  function getCreatedByEmail($user_id){
      if($user_id==0)
      {
            return "Not Present"; 
      }
      else
      {
            $user = User::where('id', $user_id)->first();
            return $user->email;
      }

  }
    
  function getUserMobileNum($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->mobile_num;
  }




}
