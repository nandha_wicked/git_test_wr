<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class AccessoriesCapacity extends Model{

    protected $table = 'accessories_capacity';

    protected $fillable = ['date','area_id','model_id','size_id','hour_string'];

    public $timestamps = false;

  }//END OF CLASS