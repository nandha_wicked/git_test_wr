<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RandMServiceSchedule extends Model{

    protected $table = 'randm_service_schedule';

    protected $fillable =['id','service','model','odo_soft_threshold','odo_hard_threshold','duration','cost','created_at','created_by','updated_at','updated_by','status','deleted'];





    public $timestamps = false;
    
    public function getService()
    {
        $service = RandMService::where('id',$this->service)->first();
        return $service;
    }
    
    public function getModelName()
    {
        $bikeModel = BikeModel::where('id', $this->model)->first();
        return $bikeModel->bike_model;
    }


    

}//END OF CLASS
