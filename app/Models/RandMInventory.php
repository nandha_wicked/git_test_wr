<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RandMInventory extends Model{

    protected $table = 'randm_inventory';

    protected $fillable = ['id','name','code','description','total_qty_added','total_qty_deleted','available','created_at','created_by','updated_at','updated_by','status','deleted'];




    public $timestamps = false;


    

}//END OF CLASS