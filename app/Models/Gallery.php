<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\BikeModel;
  use App\Models\Image;
  use App\Models\GalleryImage;

  class Gallery extends Model{

    protected $table = 'gallery';

    protected $fillable = ['name','description','raw_data'];

    public $timestamps = true;

  /* Relationships*/

    public function bikeModel(){
      return $this->belongsTo('App\Models\BikeModel');
    }

    // public function images() {
    //   return $this->belongsToMany('App\Models\Image', 'gallery_image');
    // }

    public function getImages()
    {
      $imageIds = GalleryImage::where('gallery_id', $this['id'])->get()->lists('image_id');
      return $images = Image::whereIn('id', $imageIds)->get();
    }
  }//END OF CLASS