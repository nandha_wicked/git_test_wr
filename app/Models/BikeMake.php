<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BikeMake extends Model
{
    protected $table = 'bike_makes';

    protected $fillable = ['bike_make','status', 'logo_id'];

    public $timestamps = false;

    /* Relationships */
    public function bikes()
    {
        return $this->hasManyThrough('App\Models\Bike', 'App\Models\BikeModel', 'bike_make_id', 'model_id');
    }

    function models(){
        return $this->hasMany('App\Models\BikeModel');
    }

    function getAllActiveBikeMake(){

    	$activeBikeMakes = $this->where('status', 1)->get();
    	return $activeBikeMakes;
    }

    function getBikeMakeName($bike_make_id){

    	$bikeMake=$this->where('id',$bike_make_id)->first();
    	return $bikeMake->bike_make;
    }

    function getUniqueBikeMakes($city_id){

    	$bikeMakes = DB::table('bikes')
            ->join('areas', 'areas.id', '=', 'bikes.area_id')
            ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
            ->select('areas.city_id', 'areas.area', 'bikes.model_id', 'bike_models.bike_make_id', 'bike_models.bike_model', 'bike_models.thumbnail_img_id')
            ->where('areas.city_id', $city_id)
            ->where('bikes.status', 1)
            ->where('areas.status', 1)
            ->where('bike_models.status', 1)
            ->groupBy('bike_models.bike_make_id')
            ->get();

        foreach($bikeMakes as $bikeMake){
            $bikeMake->bikeMake_name = $this->getBikeMakeName($bikeMake->bike_make_id);
        }  

        return $bikeMakes;  
    }

}
