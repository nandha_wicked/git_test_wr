<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class AccessoriesModel extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories_model';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','model','description','type','brand','created_at','updated_at','created_by','updated_by','status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function getType()
    {
        $type = AccessoriesType::where('id',$this->type)->first();
        return $type->type;
    }
    
    public function getBrand()
    {
        $brand = AccessoriesBrand::where('id',$this->brand)->first();
        return $brand->brand;
    }
    
    public function getImage()
    {
        $image = AccessoriesImage::where('model',$this->id)->first();
        if(!$image)
            return "";
        return $image->full;
    }
    
   
}
