<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCall extends Model{

    protected $table = 'api_call';

    protected $fillable = ['id','title','description','category','url','method','priority','created_at','created_by','updated_at','updated_by'];


    public $timestamps = false;
    
    public function getCategory()
    {
        $apiCategory = ApiCategory::where('id',$this->category)->first();
        return $apiCategory->category;
    }


    

}//END OF CLASS
