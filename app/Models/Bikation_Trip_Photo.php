<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bikation_Trip_Photo extends Model
{

  protected $table = 'bikation_trip_photo';

  protected $fillable = ['Trip_ID','Vendor_ID','Media_URL','Media_Select', 'Media_Size','created_on','updated_on', 'Created_By', 'Updated_by','Status','Media_URL_trip1','Media_URL_trip2','Media_URL_trip3','Media_URL_details'];

}
