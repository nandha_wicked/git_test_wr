<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Service extends Model{

    protected $table = 'service';

    protected $fillable = ['date','area_id','model_id','hour_string'];

    public $timestamps = false;

  }//END OF CLASS