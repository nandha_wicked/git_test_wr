<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReservationInfo extends Model{

    protected $table = 'reservation_info';

    protected $fillable = ['id','booking_id','bike_or_accessory','area_id','model_id','accessory_model_id','accessory_size_id','start_datetime','end_datetime','user_id','creation_mode','current_state','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted','reference_reservation','creation_reason','reason_for_renting','KMlimit','weekday_hours','weekend_hours','total_hours','res_lead_time','fuel_included','booking_platform'];


    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function pricingInfo()
    {
        return $this->hasOne('App\Models\PricingInfo','reservation_id');
    }
    
    public function financialInfo()
    {
        return $this->hasMany('App\Models\FinancialInfo','reservation_id');
    }    
    
    public function deliveryImages()
    {
        return $this->hasMany('App\Models\DeliveryImages','reservation_id');
    }
    
    public function area()
    {
        return $this->belongsTo('App\Models\Area','area_id');
    }
    
    public function model()
    {
        return $this->belongsTo('App\Models\BikeModel','model_id');
    }
    
    public function accessoryModel()
    {
        return $this->belongsTo('App\Models\AccessoriesModel','accessory_model_id');
    }
    
    public function bookingHeader()
    {
        return $this->belongsTo('App\Models\BookingHeader','booking_id');
    }
    
    public function deliveryInfo()
    {
        return $this->hasOne('App\Models\DeliveryInfo','reservation_id');
    }
    
    public function returnInfo()
    {
        return $this->hasOne('App\Models\ReturnInfo','reservation_id');
    }
    
    public function relatedReservations()
    {
        return $this->hasMany('App\Models\ReservationInfo','reference_reservation');
    }
    
    public function activeAccessoryReservations()
    {
        return $this->relatedReservations()->where('bike_or_accessory',"A")->where('deleted',0)->get();
    }
    
    public function activeAccessoryReservationsWithPricing()
    {
        return $this->relatedReservations()->where('bike_or_accessory',"A")->where('deleted',0)->has('pricingInfo')->get();
    }
    
    public function activeAccessoryReservationsWithoutPricing()
    {
        return $this->relatedReservations()->where('bike_or_accessory',"A")->where('deleted',0)->has('pricingInfo','<',1)->get();
    }
    
    public function notes()
    {
        return $this->hasMany('App\Models\NotesInfo','reservation_id');
    }
    
    public function reasonForRenting()
    {
        return $this->belongsTo('App\Models\ReasonForRenting','reason_for_renting');
    }
    
    public function legacyBooking()
    {
        return $this->hasOne('App\Models\Booking','id');
    }
    
    function readableStartDateTime(){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($this->start_datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($this->start_datetime)->format('jS M-y ga');
        }
        return Carbon::parse($this->start_datetime)->format('jS M ga');
    }
    
    function readableEndDateTime(){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($this->end_datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($this->end_datetime)->format('jS M-y ga');
        }
        return Carbon::parse($this->end_datetime)->format('jS M ga');
    }
    
    function getDuration(){
        $start =  Carbon::parse($this->start_datetime);
        $end =  Carbon::parse($this->end_datetime);

        return $start->diffForHumans($end,true);

    }
    
    function activeNotes()
    {
        $notes = NotesInfo::where(function ($query) {
                            $query->where('booking_id',$this->bookingHeader->id);
                        })->orWhere(function ($query) {
                            $query->where('reservation_id',$this->id);
                        })->orderBy('id','asc')->where('deleted',0)->get();
        $notesWithoutUser = [];
        
        foreach($notes as $note)
        {
            $note['created_by_email'] = $note->createdByUser->email;
            $noteWithoutUser = collect($note)->except('created_by_user');
            array_push($notesWithoutUser,$noteWithoutUser);
        }
        
        return $notesWithoutUser;
        
    }
    
    function getFuelIncludedPrice()
    {
        $price = Price::where('model_id',$this->model_id)->where('area_id',$this->area_id)->orderBy('id','desc')->first();
        if(!$price)
        {
            $price = Price::where('model_id',$this->model_id)->where('area_id',0)->orderBy('id','desc')->first();
        }
        
        return $price->per_km_fuel_included;
    }
    
    function getExtraKMPrice(){

        $make = BikeModel::where('id',$this->model_id)->pluck('bike_make_id');
        if(in_array($make,explode(',',env('PREMIUMMAKES'))))
        {
            $extrakmprice = env('EXTRAKMPRE');  
        }
        else
        {
            //check if model is premium
            if(in_array($this->model_id,explode(',',env('PREMIUMMODELS')))) {
                $extrakmprice = env('EXTRAKMPRE');
            }else {
                $extrakmprice = env('EXTRAKMRE');  
            }
        }
        return $extrakmprice;

    }
    
    function KMmessage()
    {
        if(!$this->fuel_included)
        {
            $make = BikeModel::where('id',$this->model_id)->pluck('bike_make_id');
            if(in_array($make,explode(',',env('PREMIUMMAKES'))))
            {
                $extrakmprice = env('EXTRAKMPRE');  
            }
            else
            {
                //check if model is premium
                if(in_array($this->model_id,explode(',',env('PREMIUMMODELS')))) {
                    $extrakmprice = env('EXTRAKMPRE');
                }else {
                    $extrakmprice = env('EXTRAKMRE');  
                }                
            }

            $kmLimitMessage = $this->KMlimit." kilometres are included in the rent charged for the duration of the booking. Any extra kilometres will be charged at Rs. ".$extrakmprice." per K.M. Please note that fuel is not included in the rental.";
        }
        else
        {
        
            $kmLimitMessage = "Please note that the fuel is included with the rental and each KM is charged Rs ".$this->getFuelIncludedPrice()." per KM";
        }
        
        return $kmLimitMessage;
    }
    
    function getDeliveryImages()
    {
        return $this->deliveryImages()->where('delivery_or_return',"D")->where('deleted',0)->get()->pluck('url')->all();
    }
    
    function getReturnImages()
    {
        return $this->deliveryImages()->where('delivery_or_return',"R")->where('deleted',0)->get()->pluck('url')->all();
    }
    
    
    function getCreditNonReversedFinancialInfo()
    {
        $financialInfos = FinancialInfo::where('booking_id',$this->booking_id)->where("debit_or_credit","C")->where('deleted',0)->whereHas('paymentNature', function ($query) {
            $query->where('reversal',0);
        })->get();
        return $financialInfos;
    }

    function getAppliedWalletAmount()
    {
        $financialInfos = FinancialInfo::where('booking_id',$this->booking_id)->where("debit_or_credit","C")->where('deleted',0)->whereIn('payment_method',array(12,13))->get();
        $walletAmount = 0;
        foreach($financialInfos as $financialInfo)
        {
            $walletAmount+=$financialInfo->amount;
        }
        return $walletAmount;
    }


}//END OF CLASS
