<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiMessage extends Model{

    protected $table = 'api_message';

    protected $fillable = ['id','call_id','type','message','created_at','created_by','updated_at','updated_by','status'];




    public $timestamps = false;


    

}//END OF CLASS