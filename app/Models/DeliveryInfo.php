<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryInfo extends Model
{

    protected $table = 'delivery_info';

    protected $fillable = ['id','bike_or_accessory','reservation_id','delivered_by','delivered_at','bike_id','accessory_id','starting_odo','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted'];


    public $timestamps = false;
    
    public function bike()
    {
        return $this->belongsTo('App\Models\BikeNumbers','bike_id');
    }


}//END OF CLASS
