<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class DeliveryImages extends Model{

    protected $table = 'delivery_images';

    protected $fillable = ['id','reservation_id','delivery_or_return','url','file_location','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','deleted'];


    public $timestamps = false;

  }//END OF CLASS