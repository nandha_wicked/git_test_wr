<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Available extends Model{

    protected $table = 'available';

    protected $fillable = ['date','area_id','model_id','hour_string'];

    public $timestamps = false;

  }//END OF CLASS