<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class ReasonForRenting extends Model{

    protected $table = 'reason_for_renting';

    protected $fillable = ['id','reason'];

    public $timestamps = false;

  }//END OF CLASS