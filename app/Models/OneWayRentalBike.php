<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalBike extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_bike';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','model_id','bike_number','owner_id','current_location','current_location_latitude','current_location_longitude','current_status','created_at','created_by','updated_at','updated_by','status','deleted','deleted_at','deleted_by'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function getModel()
    {
        $model = BikeModel::where('id',$this->model_id)->first();
        return $model->bike_model;
    }
    
    public function getOwner()
    {
        $owner = User::where('id',$this->owner_id)->first();
        return $owner->email;
        
    }
    
    public function getCurrentLocation()
    {
        if($this->current_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->current_location)->first();
        return $location->name;
    }
    
    public function area()
    {
        if($this->current_location == 0)
            return (object)["name"=>"In Transit"];
        else
            return $this->belongsTo('App\Models\OneWayRentalLocation','current_location');
    }
    
    
   
}
