<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Trip extends Model
{

  protected $table = 'bikation_trip';

  protected $fillable = ['Vendor_ID','Trip_type', 'Trip_Name','Trip_url','Created_On','Created_by', 'Status', 'Approve_requested_on', 'Approved_By', 'Approved_on','Cancelled_Request_on','Cancel_approved_by','Cancelled_on', 'Total_No_of_Tickets','No_of_tickets_available','Trip_Start_Date', 'Trip_End_Date', 'Start_Location', 'End_Location', 'Total_Distance','Total_riding_hours','No_of_stops','ticket_denomination', 'Experience_Level_required', 'Preffered_Bikes', 'General_Instructions', 'Trip_Description','Approve_By_Admin'];


}
