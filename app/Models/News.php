<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class News extends Model{
    protected $table = 'news';

    protected $fillable = ['news','status'];

    public $timestamps = false;

    function getActiveNews(){
      $news = News::where('status',1)->get();
      return $news;
    }

    function getAllNews(){
      $news = News::all();
      return $news;
    }
  }
