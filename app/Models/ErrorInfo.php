<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class ErrorInfo extends Model{

    protected $table = 'error_handling';

    protected $fillable = ['id','booking_id','subject','reason','data','type','resolved_by','created_at','updated_at'];


  }//END OF CLASS
