<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FrontEndText extends Model
{

    protected $table = 'front_end_text';

    protected $fillable = ['id','text_key','text_value','created_by','created_at','updated_by','updated_at'];

    public $timestamps = false;
    
    public static function getText($name)
    {
        $text = DB::table('front_end_text')->where('text_key',$name)->first();
        if(!$text)
            return "";
        else
            return $text->text_value;
    }
}

