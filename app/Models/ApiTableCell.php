<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiTableCell extends Model{

    protected $table = 'api_table_cell';

    protected $fillable = ['id','table_id','title','description','priority','created_at','created_by','updated_at','updated_by','status'];





    public $timestamps = false;


    

}//END OF CLASS
