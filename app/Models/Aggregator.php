<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Aggregator extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'aggregator_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'phone_number','bank_account_number','ifsc_code','bank_account_notes', 'address', 'pan','user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    function getEmail(){
    $user = User::where('id', $this->user_id)->first();
    return $user->email;
    }
    
    function getAddress(){
    
    return nl2br($this->address);
    }
   
}
