<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_People_On_Trip extends Model
{

  protected $table = 'bikation_people_on_trip';

  protected $fillable = ['Book_ID','Trip_ID','Vendor_ID','Booked_Type','Name_of_Co_Px', 'Contact_No','Email_ID','Sex', 'DoB', 'Rider', 'DL', 'Cancelled_On','Cancelled_By','Status'];

}
