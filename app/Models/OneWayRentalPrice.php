<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalPrice extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','model_id','price_per_km','price_per_min','created_at','created_by','updated_at','updated_by','status'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function getModel()
    {
        $model = BikeModel::where('id',$this->model_id)->first();
        return $model->bike_model;
    }
    
    
    
   
}
