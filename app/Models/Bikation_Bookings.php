<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Bookings extends Model
{

  protected $table = 'bikation_bookings';

  protected $fillable = ['Trip_ID','Customer_ID','Vendor_ID','Booked_Type','Package_ID','Booked_On','Booked_By', 'No_of_People','No_of_Tickets_Cost_2px','No_of_Tickets_Cost_1px', 'Status', 'Package_Cost', 'Addon_cost','Total_Cost','Cancelled_On','Cancelled_By', 'Vehicle','Additional_Remarks','Payment_status','Razorpay_id','Razorpay_entity','Razorpay_amount','Razorpay_currency','Razorpay_status','Razorpay_order_id','Razorpay_method','Razorpay_amount_refunded','Razorpay_refund_status','Razorpay_captured','Razorpay_description','Razorpay_card_id','Razorpay_bank','Razorpay_wallet','Razorpay_email','Razorpay_contact','Razorpay_fee','Razorpay_service_tax','Razorpay_error_code','Razorpay_error_description','Razorpay_created_at','Razorpay_refund_id'];

}
