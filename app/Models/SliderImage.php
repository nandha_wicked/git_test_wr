<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model
{

    protected $table = 'slider_image';

    protected $fillable = ['url','image','description','status','mobile_image','button_description'];

    public $timestamps = false;
    
    function getActiveLinks(){
        $link = SliderImage::where('status',1)->get();
        return $link;
    }


}

