<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\Bike;
  use App\Models\BikeModel;
  use App\Models\BikeMake;
  use App\Models\User; 
  use App\Models\DateSelectionEnquiry;
  use App\Models\DateSelectionEnquiryModelsArea;
  use App\Models\PrimaryDateSelectionEnquiryModel;
  use App\Http\Controllers\Controller;
  use App\Http\Controllers\UserController;
  use Auth;
  use Session;






  class City extends Model{
    protected $table = 'cities';
    protected $fillable = ['city','status'];
    public $timestamps = false;

    /* Relationships*/
    public function areas(){
      return $this->hasMany('App\Models\Area');
    }

    function getActiveCities(){
      $cities = City::where('status', 1)->get();
      return $cities;
    }

    function getAllCities(){
      $cities = City::where('status', 1)->get();
      return $cities;
    }
    
    function getCityById($id){
      $city = City::where('id', $id)->first();
      return $city;
    }

    public function brandsList(){
      $areaIds = $this->areas->lists('id');
      $bike_model_ids = Bike::whereIn('area_id', $areaIds)->groupBy('model_id')->where('status', 1)->get()->lists('model_id');
      $make_ids = BikeModel::whereIn('id', $bike_model_ids)->groupBy('bike_make_id')->where('status', 1)->get()->lists('bike_make_id');
      $makes = BikeMake::whereIn('id', $make_ids)->where('status', 1)->get();
      return $makes;
    }

    /* All bikes model */
  


  }
