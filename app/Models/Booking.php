<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Bike;
use App\Models\Helmet;
use App\Models\Jacket;
use App\Models\BikeNumbers;
use App\Models\Price;
use Carbon\Carbon;
use Mail;
use Nicolaslopezj\Searchable\SearchableTrait;
use Color;


class Booking extends Model
{
    protected $table = 'bookings';


    protected $fillable = ['id','start_datetime','start_slot_id','end_datetime','end_slot_id','operation_end_datetime', 'user_id','bike_id', 'total_price', 'status', 'pg_status', 'pg_txn_id', 'bank_txn_id', 'order_id', 'pg_mode', 'pg_txn_amount', 'note', 'raw_data', 'reference_id','promocode','createdBy','created_at','payment_method','active','KMlimit','model_id','area_id','weekday_hours','weekend_hours','total_hours','backend_notes','jackets','gloves','helmets','camera','knee_guard','saddle_bags','tank_bags','updated_by','area_id','model_id','booking_status','is_delivered','bike_number_id','helmet_id_1','helmet_id_2','jacket_id_1','jacket_id_2','full_price','res_lead_time','created_at_ts','route_plan','start_odo','end_odo','extra_km_charge','extra_km_charge_paid','extra_km_notes','delivered_at_area','delivered_by','delivered_at','mobile_holder','delivered_model_id','backend_notes_ack_by','backend_notes_ack_at','delivery_cancelled','delivery_cancelled_notes','is_retuned','returned_with_problems','returned_with_problems_note','assisted','collection_mode','extra_money','delivery_notes','returned_with_problems_accessory','collection_mode_return','extra_money_return','lead_source','wallet_amount_applied','fuel_included'];



    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'bookings.id' => 10,
            'bookings.reference_id' => 10,
            'bookings.order_id' => 10,
            'users.email' => 10,
            'users.first_name' => 2,
            'users.mobile_num' => 10
        ],
        'joins' => [
            'users' => ['bookings.user_id','users.id'],
        ]
    ];


    //public $timestamps = false;

    /* Relationships */
    

    public function bike(){
        return $this->belongsTo('App\Models\Bike');
    }
    
    public function model(){
        return $this->belongsTo('App\Models\BikeModel','model_id');
    }
    
    public function reservationInfo(){
        return $this->belongsTo('App\Models\ReservationInfo','id');
    }

    function getUserName($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->first_name;
    }

    function getUserNameThis(){
    $user = User::where('id', $this->user_id)->first();
    return $user->first_name;
    }

    function getUserFullNameThis(){
    $user = User::where('id', $this->user_id)->first();
    return $user->first_name." ".$user->last_name;
    }   

    function getUserThis(){
    $user = User::where('id', $this->user_id)->first();
    return $user;
    }


    function total_price_without_vat(){
    return $this->total_price/1.055;
    }

    function getUserLastName($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->last_name;
    }

    function getUserEmail($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->email;
    }

    function getUserEmailThis(){
    $user = User::where('id', $this->user_id)->first();
    return $user->email;
    }
    function getCreatedByEmail($user_id){
      if($user_id==0)
      {
            return "Not Present"; 
      }
      else
      {
            $user = User::where('id', $user_id)->first();
            return $user->email;
      }

    }
    function getDeliveredByEmail(){
     $user = User::where('id',$this->delivered_by)->first();
      if(!$user)
      {
            return "Not Delivered"; 
      }
      else
      {
            return $user->email;
      }

    }
    function getReturnedByEmail(){
     $user = User::where('id',$this->returned_by)->first();
      if(!$user)
      {
            return "Not Returned"; 
      }
      else
      {
            return $user->email;
      }

    }

    function getUserMobileNum($user_id){
    $user = User::where('id', $user_id)->first();
    return $user->mobile_num;
    }

    function getBikeMakeName($model_id){
    $bikeModel = BikeModel::where('id', $model_id)->first();
    $bike_make_id = $bikeModel->bike_make_id;
    $bikeMake = BikeMake::where('id', $bike_make_id)->first();
    return $bikeMake->bike_make;    
    }

    function getBikeModelName($model_id){
    $bikeModel = BikeModel::where('id', $model_id)->first();
    return $bikeModel->bike_model;
    }
    function getBikeModelNameThis(){
    $bikeModel = BikeModel::where('id', $this->model_id)->first();
    return $bikeModel->bike_model;
    }

    function getCityName($area_id){
    $area = Area::where('id', $area_id)->first();
    $city_id = $area->city_id;
    $city = City::where('id', $city_id)->first();
    return $city->city;
    }

    function getCityNameThis(){
    $area = Area::where('id', $this->area_id)->first();
    $city_id = $area->city_id;
    $city = City::where('id', $city_id)->first();
    return $city->city;
    }    

    function getCityId(){
    $area = Area::where('id', $this->area_id)->first();
    $city_id = $area->city_id;
    $city = City::where('id', $city_id)->first();
    return $city->id;
    }

    function getAreaName($area_id){
    $area = Area::where('id', $area_id)->first();
    return $area->area;
    }

    function getAreaNameThis(){
    $area = Area::where('id', $this->area_id)->first();
    return $area->area;
    }

    function getDeliveredAt(){
    $area = Area::where('id', $this->delivered_at_area)->first();
    return $area->area;
    }


    function getHelmetName($id){
    $helmet = Helmet::where('id', $id)->first();
    if(!$helmet)
        return "None";
    return $helmet->item_id;
    }

    function getJacketName($id){
    $jacket = Jacket::where('id', $id)->first();
    if(!$jacket)
        return "None";
    return $jacket->item_id;
    }



    function getBookingDetails($from_date, $to_date){
    $bookings = DB::select('SELECT * FROM bookings WHERE  ("'.$from_date.'" >= from_datetime && "'.$from_date.'"  < to_datetime OR to_datetime < "'.$to_date.'" && from_datetime  > "'.$from_date.'" OR "'.$to_date.'" > from_datetime && "'.$to_date.'" <= to_datetime)');
    return $bookings;                                            
    }
    function getBackEndNotesAckByEmail()
    {
      $user = User::where('id',$this->backend_notes_ack_by)->first();
      if(!$user)
      {
          return "";
      }
      else
      {
        return $user->email;    
      }

    }
    function getBackEndNotesAckAt(){

      if($this->backend_notes_ack_at == '0000-00-00 00:00:00')
        {
            return "";
        }
        else
        {
            return $this->backend_notes_ack_at;
        }

    }
    function collectedModeText(){

      if($this->extra_money == 0)
        {
            return "NA";
        }
        else
        {
            return $this->collection_mode;
        }

    }



    function getBikeNumber(){
     $bikeNumber = BikeNumbers::where('id',$this->bike_number_id)->first();
      if(!$bikeNumber)
        {
            return "";
        }
        else
        {
            return $bikeNumber->number;
        }

    }

    function getExtraKMPrice(){

        $make = BikeModel::where('id',$this->model_id)->pluck('bike_make_id');
        if(in_array($make,explode(',',env('PREMIUMMAKES'))))
        {
            $extrakmprice = env('EXTRAKMPRE');  
        }
        else
        {
            $extrakmprice = env('EXTRAKMRE');  
        }
        return $extrakmprice;

    }

    function PricePerHour(){

        return Price::where('model_id',$this->model_id)->where('area_id',$this->area_id)->pluck('price_per_hour');;

    }

    function getAccessoryLabel(){

        $accessories_label = "";

        if($this->helmet_id_1>0)
        {
            $accessories_label = $accessories_label."Helmet 1 : ".$this->getHelmetName($this->helmet_id_1)."<br/>";
        }
        if($this->helmet_id_2>0)
        {
            $accessories_label = $accessories_label." Helmet 2 : ".$this->getHelmetName($this->helmet_id_2)."<br/>";
        }
        if($this->jacket_id_1>0)
        {
            $accessories_label = $accessories_label." Jacket 1 : ".$this->getHelmetName($this->jacket_id_1)."<br/>";
        }
        if($this->jacket_id_2>0)
        {
            $accessories_label = $accessories_label." Jacket 2 : ".$this->getHelmetName($this->jacket_id_2)."<br/>";
        }
        if($this->gloves>0)
        {
            $accessories_label = $accessories_label." Gloves : ".$this->gloves."<br/>";
        }
        if($this->camera>0)
        {
            $accessories_label = $accessories_label." Go Pro Camera : ".$this->camera."<br/>";
        }
        if($this->knee_guard>0)
        {
            $accessories_label = $accessories_label." Knee Guard : ".$this->knee_guard."<br/>";
        }
        if($this->saddle_bags>0)
        {
            $accessories_label = $accessories_label." Saddle Bags : ".$this->saddle_bags."<br/>";
        }
        if($this->tank_bags>0)
        {
            $accessories_label = $accessories_label." Tank Bags : ".$this->tank_bags."<br/>";
        }
        if($this->mobile_holder>0)
        {
            $accessories_label = $accessories_label." Mobile Holder : ".$this->mobile_holder."<br/>";
        }

        if($accessories_label=="")
            $accessories_label = "None";

      return $accessories_label;
    }

    function getDuration(){
        $start =  Carbon::parse($this->start_datetime);
        $end =  Carbon::parse($this->end_datetime);

        return $start->diffForHumans($end,true);

    }


    function getStartTime(){
        $start =  Carbon::parse($this->start_datetime);

        return $start->format('h:i');

    }

    function getCountOfWRCancelledForUser(){
        if($this->user_id > 25)
        {
            $count = DB::table('deleted_bookings')->where('user_id',$this->user_id)->where('reason_for_delete','wicked_ride_cancelled')->count();
        }
        else
        {
            $count = "NA";
        }
        return $count;
    }

    function getReadableDateTime($dateTime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($dateTime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($dateTime)->format('j M-y ga');
        }
        return Carbon::parse($dateTime)->format('j M ga');
    }
    function getReadableDateTimeWithMinutes($dateTime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($dateTime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($dateTime)->format('j M-y g:i a');
        }
        return Carbon::parse($dateTime)->format('j M g:i a');
    }

    function md5Hash(){
        $color = substr(md5($this->id),0,6);
        $colorMex = new Color($color);

        return $colorMex->getCssGradient();
    }

    function md5HashText(){
        $color = substr(md5($this->id),0,6);
        $colorMex = new Color($color);
        if($colorMex->isDark())
            return $colorMex->lighten(50);
        else
        {
            return $colorMex->darken(30);
        }
    }


    function getUser(){
        $user = User::where('id', $this->user_id)->first();
        return $user;
    } 
    
    function getFuelIncludedPrice()
    {
        $price = Price::where('model_id',$this->model_id)->where('area_id',$this->area_id)->orderBy('id','desc')->first();
        if(!$price)
        {
            $price = Price::where('model_id',$this->model_id)->where('area_id',0)->orderBy('id','desc')->first();
        }
        
        return $price->per_km_fuel_included;
    }
    
    function getPayLater()
    {
        if(($this->payment_method == "PayLaterAndroid")||($this->payment_method == "PayLateriOS"))
        {
            return $this->total_price;
        }
        else
        {
            return 0;
        }
    }
    
    function amountPaid()
    {
        if(($this->payment_method == "PayLaterAndroid")||($this->payment_method == "PayLateriOS"))
        {
            return 0;
        }
        else
        {
            return $this->total_price;
        }
    }
    
    function revenueAmount()
    {
        $financialInfo = $this->reservationInfo->bookingHeader->totalAmountPaid();
        return $financialInfo['amount_paid'];
    }

}
