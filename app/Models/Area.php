<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\City;
  use App\Transformers\AreaTransformer;
  use DB; 

  class Area extends Model{

    protected $table = 'areas';

    protected $fillable = ['city_id','area','address','status','vendor_id','code','gmapLink','longitude','latitude','hours_open','mapsLink'];

    public $timestamps = false;

  /* Relationships*/

    public function city(){
      return $this->belongsTo('App\Models\City');
    }
      
    public function reservations(){
        return $this->hasMany('App\Models\ReservationInfo', 'area_id');
    }
      
    public function bikes(){
      return $this->hasMany('App\Models\Bike');
    }

    public function price(){
      return $this->hasOne('App\Models\Price');
    }

    function getCityName($city_id){
      $city=City::where('id',$city_id)->first();
      return $city->city;
    }

    function getAllAreas($city_id){
      $areas = $this->where('city_id', $city_id)->get();
      return $areas;
    }
      
    function getAllActiveAreasForCity($city_id){
      $areas = $this->where('city_id', $city_id)->where('status',1)->get();
      return $areas;
    }

    function getAreaName($area_id){
      $area = $this->where('id', $area_id)->first();
      return $area->area;
    }

    function getAreaAddress($area_id){
      $area = $this->where('id', $area_id)->first();
      return $area->address;
    }

    function getAllActiveAreas(){
      $areas = $this->where('status', 1)->get();
      return $areas;
    }

    public function transform(){
      $at = new AreaTransformer();
      return $at->transform($this);
    }

    public static function transformCollection($areas){
      $at = new AreaTransformer();
      return $at->transformCollection($areas);
    }
    
    public function getHoursOpen()
    {
        if($this->hours_open == "")
            return "";
        
        $dayOfWeekArray = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
        
        $openHoursForArea = json_decode($this->hours_open);
            
            
        $openHoursString = "";

        foreach($openHoursForArea as $day=>$openHoursForAreaByDay) 
        {
            $dayOfWeekArray[$day]." - ";

            $openHours = [];
            $closeHours = [];
            foreach($openHoursForAreaByDay as $hour)
            {
                if(!in_array($hour-1,$openHoursForAreaByDay))
                {
                    array_push($openHours,$hour);
                }
                if(!in_array($hour+1,$openHoursForAreaByDay))
                {
                    array_push($closeHours,$hour);
                }
            }

            $openHoursStringByDayArray = [];
            
            
            foreach($openHours as $key => $openHour)
            {
                $openHoursStringByDay = $openHour==0?"12 am":($openHour==12?'12 pm':($openHour>12?($openHour-12).' pm':$openHour.' am'));
                $openHoursStringByDay .= " to ";
                $openHoursStringByDay .= $closeHours[$key]==0?"12 am":($closeHours[$key]==12?'12 pm':($closeHours[$key]>12?($closeHours[$key]-12).' pm':$closeHours[$key].' am'));
                array_push($openHoursStringByDayArray,$openHoursStringByDay);
            }

            $openHoursString .= $dayOfWeekArray[$day]." - ".implode(", ",$openHoursStringByDayArray)."<br/>";
        }
        
        return $openHoursString;
        
        
    }
      
    public function getHoursForEdit()
    {
        if($this->hours_open == "")
            return "";
        
        $dayOfWeekArray = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
        
        $openHoursForWeek = json_decode($this->hours_open);
        
        $hoursForEdit = [];
        
        foreach($openHoursForWeek as $day=>$openHoursForDay)
        {
            if(!reset($openHoursForDay))
                $first = "";
            else
                $first = reset($openHoursForDay);
            
            if(!end($openHoursForDay))
                $last = "";
            else
                $last = end($openHoursForDay);
            
            if(!reset($openHoursForDay)&&!end($openHoursForDay))
            {
                $closedHoursArray = [];
            }
            else
            {
                $closedHoursArray = [];

                foreach(range($first,$last) as $number) 
                {
                    if(!in_array($number,$openHoursForDay))
                    {
                        array_push($closedHoursArray,$number);
                    }
                }
            }
            
            array_push($hoursForEdit,["first"=>$first,"last"=>$last,"closed"=>implode(",",$closedHoursArray)]);
        }
        
        return json_encode($hoursForEdit);
    }
    
    public function getKiranaUsers()
    {
        $kiranaUsers = DB::table('kirana_user_area_relation')->where('area',$this->id)->get();
        if(!$kiranaUsers)
        {
            return "";
        }
        else
        {
            $emailsArray = [];
            foreach($kiranaUsers as $kiranaUser)
            {
                $kiranaUserObj = User::where('id',$kiranaUser->user)->first();
                array_push($emailsArray,$kiranaUserObj->email);
            }
            $emailStr = implode(',',$emailsArray);
            return $emailStr;
        }
        
    }

  }//END OF CLASS
