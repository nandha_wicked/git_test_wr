<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;




class PackagesReview extends Model
{

 protected $table = 'bikation_packages_reviews';

 protected $fillable = ['product_id','product_name','user_id', 'rating', 'comment','url','created_at'];

  
public $timestamps = true;


}