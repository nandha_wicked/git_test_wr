<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Package extends Model
{

  protected $table = 'bikation_package';

  protected $fillable = ['Trip_ID','Vendor_ID','Description','name','No_of_Tickets_Cost_1px','Tickes_Remaining_Cost_1px','Cost_1px','No_of_Tickets_Cost_2px','Tickes_Remaining_Cost_2px','Cost_2px','rental_bike_cost','Status'];

}
