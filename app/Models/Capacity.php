<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Capacity extends Model{

    protected $table = 'capacity';

    protected $fillable = ['date','area_id','model_id','hour_string'];

    public $timestamps = false;

  }//END OF CLASS