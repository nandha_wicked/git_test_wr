<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCategory extends Model{

    protected $table = 'api_category';

    protected $fillable = ['id','category','description','created_at','created_by','updated_at','updated_by'];


    public $timestamps = false;

}//END OF CLASS