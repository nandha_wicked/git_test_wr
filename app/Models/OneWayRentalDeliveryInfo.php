<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneWayRentalDeliveryInfo extends Model{

    protected $table = 'a2b_booking_delivery_info';

    protected $fillable = ['id','booking_id','starting_fuel','helmets_given'];

    public $timestamps = false;

}//END OF CLASS
