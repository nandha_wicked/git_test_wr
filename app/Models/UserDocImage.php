<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDocImage extends Model
{

  protected $table = 'user_doc_image';

  protected $fillable = ['user_id','doc_type','doc','doc_reason','status'];

}
