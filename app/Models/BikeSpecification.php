<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class BikeSpecification extends Model{
    protected $table = 'bike_specifications';
    protected $fillable = ['bike_model_id','engine', 'power', 'seats', 'fuel_capacity'];
    public $timestamps = false;

    /* Relationships*/
 }
