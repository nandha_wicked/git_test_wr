<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class OneWayRentalPricingInfo extends Model{

    protected $table = 'a2b_pricing_info';

    protected $fillable = ['id','bike_or_accessory','booking_id','pricing_id','distance','duration','full_price','promo_discount','price_after_discount','promo_code','tax_id','promotional_balance_used','taxable_price','tax_amount','price_after_tax','deleted_at','deleted_by','deleted'];


    public $timestamps = false;

  }//END OF CLASS
