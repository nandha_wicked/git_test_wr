<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneWayRentalReturnInfo extends Model{

    protected $table = 'a2b_booking_return_info';

    protected $fillable = ['id','booking_id','ending_fuel','helmets_returned'];

    public $timestamps = false;
    

}//END OF CLASS
