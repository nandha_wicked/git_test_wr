<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PackagesUser extends Model
{

  protected $table = 'bikation_packages_user';

  protected $fillable = ['user_id','package_id', 'package_name','no_seats','firstname', 'lastname', 'gender', 'email', 'mobileNo','url'];

}
