<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class BikerPlan extends Model{
    protected $table = 'biker_plan';

    protected $fillable = ['name','description','cost','status','image','longDescription'];

    public $timestamps = false;

    function getBikerPlans(){
      $bikerPlans = BikerPlan::where('status',1)->get();
      return $bikerPlans;
    }


  }
