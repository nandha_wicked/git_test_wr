<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  use App\Models\BikeModel;
  use App\Models\Area;
  use App\Models\BikeMake;
  use App\Models\City;
  use App\Models\Price;
  use App\Models\Booking;
  use App\Models\Image;
  use DB;
    use Carbon\Carbon;

  class Bike extends Model{

    protected $table = 'bikes';

    protected $fillable = ['model_id','area_id','status','vendor_id','date_of_addition','reg_number','created_at','created_by','updated_at','updated_by'];

    public $timestamps = false;

    /* Relationships */

    public function area(){
      return $this->belongsTo('App\Models\Area');
    }

    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }
      
    
    public function documents()
    {
        return $this->hasMany('App\Models\BikeDocument', 'bike_id');
    }

    public function bikeModel(){
      return $this->belongsTo('App\Models\BikeModel', 'model_id');
    }
    
    public function owner(){
      return $this->belongsTo('App\Models\User', 'vendor_id');
    }

      

    public function make(){
      $model = $this->bikeModel;
      $make = $model->make;
      return $make;
    }

    public function city(){
      $area = $this->area;
      $city = $area->city;
      return $city;
    }
    


    function getBikeModelName($model_id){
      $bikeModel = BikeModel::where('id', $model_id)->where('status', 1)->first();
      return $bikeModel->bike_model;
    }

    function getBikeModelThumbnail($model_id){
      $bikeModel = BikeModel::where('id', $model_id)->where('status', 1)->first();
      return Image::find('id', $bikeModel->thumbnail_img_id);
    }

    function getBikeId($model_id,$area_id){
      $bike = $this->where('model_id', $model_id)
               ->where('area_id', $area_id)
                ->first();
      return $bike->id;             
    }

    function getAreaName($area_id){
      $area = Area::where('id', $area_id)->where('status', 1)->first();
      return $area->area;
    }

    function getBikeMakeName($model_id){
      $bikeModel = BikeModel::where('id',$model_id)->where('status', 1)->first();
      $bikeMakeId = $bikeModel->bike_make_id;
      $bikeMake = BikeMake::where('id',$bikeMakeId)->where('status', 1)->first();
      return $bikeMake->bike_make;    
    }

    function getCityName($area_id){
      $area = Area::where('id',$area_id)->where('status', 1)->first();
      $cityId = $area->city_id;
      $city = City::where('id',$cityId)->where('status', 1)->first();
      return $city->city; 
    }

    function getPriceName($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->name;
    }

    function getWeekDayPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->week_day_price;   
    }

    function getWeekEndPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->week_end_price;   
    }

    function get_3_6_DaysPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->days_price_3_6;   
    }

    function get_7_14_DaysPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->days_price_7_14;   
    }

    function get_15_30_DaysPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->days_price_15_30;   

    }

    function get_30_plus_DaysPrice($price_id){
      $price = Price::where('id', $price_id)->first();
      return $price->days_price_30_plus;   
    }

    function getBikeModelsByCity($city_id){
      $area = Area::where('id', $city_id)->where('status', 1)->get();
    }

    /*Method to get all areas in a City*/
    public function getAreas($city_id, $area_ids){
        return Area::whereIn('id', $area_ids)->where("city_id", $city_id)->where('status', 1)->get();
    }


    public function getPriceByDate($fromDate, $toDate, $priceId){
      //$fromDate = "15-aug-2015";
      //$toDate = "18-aug-2015";
      $fromTs = strtotime($fromDate);
      $toTs = strtotime($toDate);
      $diffTs = $toTs - $fromTs;
      $no_of_days = floor($diffTs/(60*60*24));
      $price=0;

      $bike = new Bike();
    if($no_of_days == "1" || $no_of_days == "2")
    {
    $price = 0;

    if(date('N', strtotime($from)) >= 6)
    $price += $bike->getWeekEndPrice($priceId);
    else
    $price += $bike->getWeekDayPrice($priceId);

    if($no_of_days == 2)
    if(date('N', strtotime($to)) >= 6)
    $price += $bike->getWeekEndPrice($priceId);
    else
    $price += $bike->getWeekDayPrice($priceId);

    /*if($no_of_days == "1"){
    if(date('N', $fromTs) >= 6)
    $price += $bike->getWeekEndPrice($priceId);
    else
    $price += $bike->getWeekDayPrice($priceId);
    }
    else {
    if(date('N', $fromTs) < 6  &&  date('N', $toTs) < 6)
    $price += $bike->getWeekDayPrice($priceId);
    else if(date('N', $fromTs) >= 6  &&  date('N', $toTs) >= 6)
    $price += $bike->getWeekEndPrice($priceId);
    else               
    }  */  

    return $price;

    }else if((3 <= $no_of_days) && ($no_of_days <= 6)){
    return $price = $bike->get_3_6_DaysPrice($priceId);

    }else if((7 <= $no_of_days) && ($no_of_days <= 14)){
    return $price = $bike->get_7_14_DaysPrice($priceId);

    }else if((15 <= $no_of_days) && ($no_of_days <= 30)){
    return $price = $bike->get_15_30_DaysPrice($priceId);
    }else{
    return $price = $bike->get_30_plus_DaysPrice($priceId);
    }

    }




      public function getBikeCount($model_id, $area_id){

          return $this::where('model_id', $model_id)
                  ->where('area_id', $area_id)
                  ->where('status', 1)
                  ->count();
      }

      /*
      * Master function: To featch available bike between a time range
      */
      public function checkBikeAvailability($from_date, $to_date, $model_id, $area_id)
      {
         $bikes = $this->where('model_id', $model_id)
                  ->where('area_id', $area_id)
                  ->get();

         
          foreach($bikes as $bike){
          $bikes_availability =  DB::select('SELECT * 
                              FROM bookings
                              WHERE bike_id = "'.$bike->id.'"
                                  && (
                                          "'.$from_date.'" >= from_datetime    &&    "'.$from_date.'"  < to_datetime
                                      OR   to_datetime  < "'.$to_date.'"       &&     from_datetime  > "'.$from_date.'"
                                      OR   "'.$to_date.'" > from_datetime      &&     "'.$to_date.'" <= to_datetime

                                      )
                              ');

          if(count($bikes_availability) == 0 )
              return $bike->id;

          }
          return false;

      }


      /*public function bikesNotBooked($from_date, $to_date, $model_id, $area_id){
         
         $notbooked = DB::select(
          'SELECT * FROM bikes 
          INNER JOIN bookings ON bikes.id = bookings.bike_id 
          WHERE 
              (bookings.from_datetime < "'.$from_date.'" && bookings.to_datetime > "'.$to_date.'")
              &&
              (bikes.model_id = '.$model_id.'  && bikes.area_id = '.$area_id.')
          ');

          
          return $notbooked;
      }*/



  /*********************************************Admin Bike Filter Starts*****************************************************/

      public function getBookedUniqueModelsCount($from_date, $to_date, $model_id, $area_id)
      {
          $bikes = $this->where('model_id', $model_id)
                  ->where('area_id', $area_id)
                  ->get();

          $count = 0;
          
          foreach($bikes as $bike){
              
              $bikes_booked =  DB::select('SELECT * 
                              FROM bookings
                              WHERE bike_id = "'.$bike->id.'"
                                  && (
                                          "'.$from_date.'" >= from_datetime    &&    "'.$from_date.'"  < to_datetime
                                      OR   to_datetime  < "'.$to_date.'"       &&     from_datetime  > "'.$from_date.'"
                                      OR   "'.$to_date.'" > from_datetime      &&     "'.$to_date.'" <= to_datetime

                                      )
                              ');

              if($bikes_booked)
                  $count = $count + 1;
          }
              return $count;
      }

      public function getTotalUniqueModelsCount($model_id, $area_id)
      {
          $bikes = $this->where('model_id', $model_id)
                  ->where('area_id', $area_id)
                  ->get();
          
          return count($bikes);        
      }    

      public function bikeAvailabilityFilter($from_date, $to_date, $area_id)
      {
          $uniqueModels = DB::table('bikes')
                          ->join('bike_models', 'bikes.model_id', '=', 'bike_models.id')
                          ->select('bikes.model_id', 'bike_models.bike_model')
                          ->where('area_id', $area_id)
                          ->groupBy('model_id')
                          ->get();

          foreach ($uniqueModels as $model) 
          {
              $model->total_model_count = $this->getTotalUniqueModelsCount($model->model_id, $area_id);
              $model->booked_model_count = $this->getBookedUniqueModelsCount($from_date, $to_date, $model->model_id, $area_id);
              $model->available_model_count = $model->total_model_count - $model->booked_model_count;
          }

          return $uniqueModels;          
      }

      public function getAvailableMatrix($st, $en){
        $availability =  Availability::whereIn('bike_id', [$this->id])->whereBetween('date',[$st, $en])->get();
    
        $availabilityCalendar= [];
        foreach ($availability as $booking) {
          if(array_key_exists($booking['bike_id'], $availabilityCalendar) && array_key_exists($booking['date'], $availabilityCalendar[$booking['bike_id']]))
            $availabilityCalendar[$booking['bike_id']][$booking['date']] = array_intersect($availabilityCalendar[$booking['bike_id']][$booking['date']], (empty($booking['slots']))? []: explode(',', $booking['slots']));
          else{
            $slots = (empty($booking['slots']))? []: explode(',', $booking['slots']);
            $availabilityCalendar[$booking['bike_id']][$booking['date']] = $slots;
          }
        }

        return $availabilityCalendar;
      }

  /*********************************************Admin Bike Filter Ends*****************************************************/    
    function readableDateTime($datetime){
        $year = Carbon::now()->year;
        $dateTimeCarbon = Carbon::parse($datetime);
        if($dateTimeCarbon->year!=$year)
        {
            return Carbon::parse($datetime)->format('jS M-y ga');
        }
        return Carbon::parse($datetime)->format('jS M ga');
    }
    
}
