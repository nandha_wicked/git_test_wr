<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;



class TempUser extends Model 
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  ['first_name','last_name','email','mobile_num','password','remember_token','created_at','updated_at','dob','profile_img_id','isAdmin','facebook_id','avatar_original','location','gender','_token','note','isOperation','role','dl_image','id_image','dl_id_status','sales_manago_id','avatar_thumb','blur_image','user_document','user_document_status','work_email','gcm_token','role_string','user_doc_notes','referral_code','referral_code_used','mobile_otp','otp_verification_tries'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}
