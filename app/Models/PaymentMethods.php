<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class PaymentMethods extends Model{

    protected $table = 'payment_method';

    protected $fillable = ['id','method','display'];

    public $timestamps = false;

  }//END OF CLASS
