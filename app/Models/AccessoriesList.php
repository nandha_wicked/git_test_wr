<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class AccessoriesList extends Model{

    protected $table = 'accessories_list';

    protected $fillable = ['id','accessory'];

    public $timestamps = false;

  }//END OF CLASS