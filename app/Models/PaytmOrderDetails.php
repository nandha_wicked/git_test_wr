<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class PaytmOrderDetails extends Model{

    protected $table = 'paytm_order_details';

    protected $fillable = ['id','area_id','model_id','start_datetime','end_datetime','promocode','user_id','note','created_at'];

    public $timestamps = false;

  }//END OF CLASS
