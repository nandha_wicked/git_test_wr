<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AccessoriesType extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','type','description','created_at','updated_at','created_by','updated_by','status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
   
}
