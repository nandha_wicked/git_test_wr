<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bikation_Banner extends Model
{

  protected $table = 'bikation_banner';

  protected $fillable = ['banner_title','image','status'];

}
