<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Address extends Model
{

  protected $table = 'bikation_address';

  protected $fillable = ['Vendor_ID','Valid_From','Valid_To', 'Created_By','Updated_By','Created_On', 'Updated_On', 'Door_No', 'Street_1', 'Street_2','Area','City','State', 'Country','ZipCode','Name', 'LandLine_Number', 'Phone_Number', 'Alternate_Number','Website','about_me'];

}
