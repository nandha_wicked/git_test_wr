<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Accessories extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','text_id','model','type','brand','color','size','area','owner','created_at','updated_at','created_by','updated_by','status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function getModel()
    {
        $model = AccessoriesModel::where('id',$this->model)->first();
        return $model->model;
    }
    
    public function getSize()
    {
        $size = AccessoriesSize::where('id',$this->size)->first();
        return $size->size;
    }
    
    public function getColor()
    {
        $color = AccessoriesColor::where('id',$this->color)->first();
        return $color->color;
    }
    
    public function getType()
    {
        $type = AccessoriesType::where('id',$this->type)->first();
        return $type->type;
    }
    
    public function getBrand()
    {
        $brand = AccessoriesBrand::where('id',$this->brand)->first();
        return $brand->brand;
    }
    
    public function getArea()
    {
        if($this->area<1000000)
        {  
            $area = Area::where('id',$this->area)->first();

            return $area->area;
        }
        else
        {
            $city = City::where('id',($this->area-1000000))->first();
            return $city->city;
        }
    }
    
    
    public function getOwner()
    {
        $owner = User::where('id',$this->owner)->first();
        return $owner->email;
    }
    
   
}
