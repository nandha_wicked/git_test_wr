<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Vendor extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vendor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'email', 'phone','user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}
