<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Bike;
use App\Models\City;


  class Price extends Model
  {

      protected $table = 'prices';

      protected $fillable = [
            'model_id', 
            'area_id',
            'weekday_gt_8', 
            'weekday_lt_8', 
            'weekend_gt_8', 
            'weekend_lt_8',
            'price_per_hour',
            'minimum_hours',
            'hour_weekday',
            'hour_weekend',
            'day_weekday',
            'day_weekend',
            'five_weekday',
            'week',
            'month',
            'status',
            'per_hour_fuel_included_weekday',
            'per_km_fuel_included',
            'updated_by'
        ];

      public $timestamps = false;


      /* Relationships */
      public function model(){
        return $this->belongsTo('App\Models\BikeModel', 'model_id');
      }

      public function area(){
        return $this->belongsTo('App\Models\Area', 'area_id');
      }

      public function getPriceId($model_id, $area_id){
        $price = Price::where('model_id', $model_id)
                ->where('area_id',$area_id)
                ->first();
        
        if(!$price)
        {
            $price = Price::where('model_id',$model_id)->orderBy('id','asc')->first();
        }

        return $price->id;      
      }
      
      public function getPriceByDate($fromDate, $toDate, $priceId){
        //$fromDate = "15-aug-2015";
        //$toDate = "18-aug-2015";
        $fromTs = strtotime($fromDate);
        $toTs = strtotime($toDate);
        $diffTs = $toTs - $fromTs;
        $no_of_days = floor($diffTs/(60*60*24));
        $price=0;

        $bike = new Bike();
      
        if($no_of_days == "1" || $no_of_days == "2")
        {
          $price = 0;

          if(date('N', strtotime($fromDate)) == 7 && date('N', strtotime($toDate)) == 1)
             $price += $bike->getWeekDayPrice($priceId);
          else if(date('N', strtotime($fromDate)) == 7 && $no_of_days == 2)
             $price += $bike->getWeekDayPrice($priceId);    
          else if(date('N', strtotime($fromDate)) >= 5)
             $price += $bike->getWeekEndPrice($priceId);
          else
             $price += $bike->getWeekDayPrice($priceId);
        }

      if($no_of_days == 2)
        if(date('N', strtotime($toDate)) >= 6)
          $price += $bike->getWeekEndPrice($priceId);
        else
          $price += $bike->getWeekDayPrice($priceId);

      /*if($no_of_days == "1"){
          if(date('N', $fromTs) >= 6)
              $price += $bike->getWeekEndPrice($priceId);
          else
              $price += $bike->getWeekDayPrice($priceId);
      }
      else {
          if(date('N', $fromTs) < 6  &&  date('N', $toTs) < 6)
              $price += $bike->getWeekDayPrice($priceId);
          else if(date('N', $fromTs) >= 6  &&  date('N', $toTs) >= 6)
              $price += $bike->getWeekEndPrice($priceId);
          else               
      }  */  

      return $price;

          
    }



      public function getTotalPriceOfBikeSelected($model_id, $area_id, $from_date, $to_date){
      	$price_id = $this->getPriceId($model_id, $area_id);
      	$total_price = $this->getPriceByDate($from_date, $to_date, $price_id);
      	return $total_price;
      }

      public function perDayPrice($from_date, $to_date, $total_price){
        $from_ts = strtotime($from_date);
        $to_ts = strtotime($to_date);
        $diff_ts = $to_ts - $from_ts;
        $no_of_days = floor($diff_ts/(60*60*24));

        return $total_price/$no_of_days;
      }

      public function getPrice($noOfHours, $weekday){
        if($weekday)
        {
            $total = 0;
            if($noOfHours < $this['minimum_hours']){
                $total = $this['minimum_hours'] * $this['price_per_hour'];
              }
            else{
                $total = $noOfHours * $this['price_per_hour'];
              }
        }
        else
        {
            $total = 0;
            if($noOfHours < 24){
                $total = 24 * $this['price_per_hour'];
              }
            else{
                $total = $noOfHours * $this['price_per_hour'];
              }
        }
        
        return $total;
      }

      public function getWeekendWeekdayPrice($weekDayHours, $weekEndHours){
        $totalHours = $weekDayHours + $weekEndHours;
        $total = 0;
     
            $price1 = $weekDayHours * $this['price_per_hour'];
            $price2 = $weekEndHours * $this['price_per_hour'];
            $total = $price1 + $price2;
        
        return $total;
      }

      public static function getFinalPriceAfterDiscount($price, $noOfHours, $areaisvendorBool) {

        return $price;
      }

      public static function getExtraKMPriceformodel($model_id){
        $make = BikeModel::where('id',$model_id)->pluck('bike_make_id');
        if(in_array($make,explode(',',env('PREMIUMMAKES'))))
        {
            $extrakmprice = env('EXTRAKMPRE');  
        }
        else
        {
            //check if model is premium
            if(in_array($model_id,explode(',',env('PREMIUMMODELS')))) {
                $extrakmprice = env('EXTRAKMPRE');
            }else {
                $extrakmprice = env('EXTRAKMRE');  
            }
        }
        return $extrakmprice;
    }
                 
  }
