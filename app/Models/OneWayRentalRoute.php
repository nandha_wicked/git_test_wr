<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalRoute extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_routes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','model_id','from_location','to_location','price','created_at','created_by','updated_at','updated_by','status'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function getModel()
    {
        $model = BikeModel::where('id',$this->model_id)->first();
        return $model->bike_model;
    }
    
    
    public function getFromLocation()
    {
        if($this->from_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->from_location)->first();
        return $location->name;
    }
    
    public function getToLocation()
    {
        if($this->to_location == 0)
            return "In Transit";
        $location = OneWayRentalLocation::where('id',$this->to_location)->first();
        return $location->name;
    }
   
}
