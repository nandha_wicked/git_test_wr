<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Enquiry extends Model
{
    protected $table = 'enquiry';
    public $timestamps = false;    

    public function getCreatedAt(){
        
        return Carbon::parse($this->created_at)->format('j M-y ga');
    }
    public function getStartDate(){
        $startDate = new Carbon;
        if($this->startdate == null)
        {
            $dateStr = "";
        }
        else
        {
            $startDate = $startDate->createFromFormat('d M Y H:i', $this->startdate." ".$this->starttime);
            $dateStr = $startDate->format('j M-y ga');
        }
        return $dateStr;
    }
    public function getEndDate(){
        
        $endDate = new Carbon;
        if($this->enddate == null)
        {
            $dateStr = "";
        }
        else
        {
            $endDate = $endDate->createFromFormat('d M Y H:i', $this->enddate." ".$this->endtime);
            $dateStr = $endDate->format('j M-y ga');
        }
        return $dateStr;
    }
    
}
