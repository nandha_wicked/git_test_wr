<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Image;


class AppNotification extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'app_notification_page';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','url_path','image_url','title','description','button_text','button_link','is_offer'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    
}
