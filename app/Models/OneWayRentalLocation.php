<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OneWayRentalLocation extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'on_demand_a2b_locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','address','latitude','longitude','contact_person','contact_number','created_at','created_by','updated_at','updated_by','status','open_time','close_time','is_spoke','radius_delivery','radius_return','deleted','deleted_at','deleted_by'];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public function bikes()
    {
        return $this->hasMany('App\Models\OneWayRentalBike','current_location');
    }
    
    
    public function getHubAreas()
    {
        if($this->is_spoke == 0)
            return "";
        else
        {
            $hubSpokes = DB::table('on_demand_a2b_hub_spoke')->where('spoke_area',$this->id)->where('deleted',0)->get();
            $hubsArray = [];
            foreach($hubSpokes as $hubSpoke)
            {
                $hub = OneWayRentalLocation::where('id',$hubSpoke->hub_area)->where('status',1)->first();
                if(!(!$hub))
                {
                    array_push($hubsArray,$hub->name);
                }
            }
            return implode(",",$hubsArray);
        }
    }
    
    public function getHubAreaIds()
    {
        if($this->is_spoke == 0)
            return "";
        else
        {
            $hubSpokes = DB::table('on_demand_a2b_hub_spoke')->where('spoke_area',$this->id)->where('deleted',0)->get();
            $hubsArray = [];
            foreach($hubSpokes as $hubSpoke)
            {
                $hub = OneWayRentalLocation::where('id',$hubSpoke->hub_area)->where('status',1)->first();
                if(!(!$hub))
                {
                    array_push($hubsArray,$hub->id);
                }
            }
            return implode(",",$hubsArray);
        }
    }
   
}
