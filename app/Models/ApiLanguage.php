<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiLanguage extends Model{

    protected $table = 'api_language';

    protected $fillable = ['id','language','description','created_at','created_by','updated_at','updated_by'];



    public $timestamps = false;


    

}//END OF CLASS