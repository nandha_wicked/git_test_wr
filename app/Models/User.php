<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;



class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =  ['first_name','last_name','email','mobile_num','password','facebook_id','remember_token', 'dob','profile_img_id','isAdmin','gender','location','avatar_original','avatar_thumb','blur_image','user_document','user_document_status','work_email','note','_token','dl_image','id_image','dl_id_status','sales_manago_id','gcm_token','role_string','user_doc_notes','referral_code_used','referral_code','zones_assigned'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    use SearchableTrait;
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            
            'users.email' => 10,
            'users.first_name' => 2,
            'users.mobile_num' => 10,
            'bookings.reference_id' => 10,
            'bookings.id' => 10,
        ],
        'joins' => [
            'bookings' => ['users.id','bookings.user_id'],
        ]
    ];
    
    protected $hidden = ['password', 'remember_token'];
    
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking','user_id');
    }
    
    public function userDocImages()
    {
        return $this->hasMany('App\Models\UserDocImage','user_id');
    }
    
    
    public function reservations(){
        return $this->hasMany('App\Models\ReservationInfo', 'user_id');
    }
    
    public function zonesAssigned()
    {
        $areas = Area::all();
        $cities = City::all();
        $areasById = $areas->keyBy('id');
        $citiesById = $cities->keyBy('city');
        $zones = explode(',',$this->zones_assigned);
        if($this->zones_assigned == "")
        {
            $zoneStr = "";
        }
        elseif($this->zones_assigned == "0")
        {
            $zoneStr = "All Areas";
        }
        else
        {
            $str = [];
            $areasById = $areasById->all();
            $citiesById = $citiesById->all();
            foreach($zones as $zone)
            {
                if(array_key_exists($zone,$areasById))
                {
                    array_push($str,$areasById[$zone]->area);
                }
                elseif(array_key_exists($zone,$citiesById))
                {
                    array_push($str,$citiesById[$zone]->city);
                }
            }
            
            $zoneStr = implode(',',$str);
        }
        
        
        return $zoneStr;
    }
    
    public function getUserZonesForOps()
    {
        $areas = Area::all();
        $cities = City::all();
        $areasById = $areas->keyBy('id');
        $citiesById = $cities->keyBy('city');
        $zones = explode(',',$this->zones_assigned);
        $zoneArray = [];
        if($this->zones_assigned == "0" || $this->zones_assigned == "")
        {
            $zoneArray = Area::where('status',1)->get()->pluck('id')->all();
        }
        else
        {
            $areasById = $areasById->all();
            $citiesById = $citiesById->all();
            foreach($zones as $zone)
            {
                if(array_key_exists($zone,$areasById))
                {
                    array_push($zoneArray,$areasById[$zone]->id);
                }
                elseif(array_key_exists($zone,$citiesById))
                {
                    $areaIds = $citiesById[$zone]->areas()->get()->pluck('id')->all();
                    foreach($areaIds as $areaId)
                    {
                        array_push($zoneArray,$areaId);
                    }
                }
            }
        }
        
        return $zoneArray;
    }
    
    public function getUserZonesForReport()
    {
        $areas = Area::all();
        $cities = City::all();
        $areasById = $areas->keyBy('id');
        $citiesById = $cities->keyBy('city');
        $zones = explode(',',$this->zones_assigned);
        $zoneArray = [];
        if($this->zones_assigned == "")
        {
            $zoneArray = array("0");
        }
        elseif($this->zones_assigned == "0")
        {
            $zoneArray = Area::get()->pluck('id')->all();
        }
        else
        {
            $areasById = $areasById->all();
            $citiesById = $citiesById->all();
            foreach($zones as $zone)
            {
                if(array_key_exists($zone,$areasById))
                {
                    array_push($zoneArray,$areasById[$zone]->id);
                }
                elseif(array_key_exists($zone,$citiesById))
                {
                    $areaIds = $citiesById[$zone]->areas()->get()->pluck('id')->all();
                    foreach($areaIds as $areaId)
                    {
                        array_push($zoneArray,$areaId);
                    }
                }
            }
        }
        
        return $zoneArray;
    }
    
    public function getBalance()
    {
        $financials = FinancialInfo::where('user_id',$this->id)->where('deleted',0)->get();
        $balance = 0;
        foreach($financials as $financial)
        {
            if($financial->debit_or_credit == "D")
            {
                $balance += $financial->amount;
            }
            else
            {
                $balance -= $financial->amount;
            }
        }
        return round($balance);
    }
    
    public function getA2BBalance()
    {
        $financials = OneWayRentalFinancialInfo::where('user_id',$this->id)->where('deleted',0)->get();
        $balance = 0;
        foreach($financials as $financial)
        {
            if($financial->debit_or_credit == "D")
            {
                $balance += $financial->amount;
            }
            else
            {
                $balance -= $financial->amount;
            }
        }
        return round($balance);
    }
    
    public function getDocuments()
    {
        $documentsByTypes = $this->userDocImages->groupBy('doc_type');
        
        $userDocsResult = [];
        
        foreach($documentsByTypes as $doc_type => $documentsByTypesEl)
        {
            $docUrls = [];
            foreach($documentsByTypesEl as $doc)
            {
                if(substr($doc->doc,0,4) == "http")
                {
                    $url = $doc->doc;
                }
                else
                {
                    $url = env('HTTPSORHTTP').":".$doc->doc;
                }
                array_push($docUrls,["url"=>$url,'status'=>$doc->status?true:false]);
            }
            array_push($userDocsResult,["doc_type"=>$doc_type,"doc_images"=>$docUrls]);
        }
        
        return $userDocsResult;
        
    }

    public function name()
    {
        if ($this->first_name) {
            return $this->first_name;
        } else {
            return $this->last_name;
        }
    }
    
}
