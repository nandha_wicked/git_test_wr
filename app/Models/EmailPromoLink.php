<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailPromoLink extends Model
{

    protected $table = 'email_promo_link';

    protected $fillable = ['url','image','description','status'];

    public $timestamps = false;
    
    function getActiveLinks(){
        $link = EmailPromoLink::where('status',1)->get();
        return $link;
    }


}

