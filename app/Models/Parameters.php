<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Parameters extends Model
{
   	protected $table = 'parameter';

    protected $fillable = ['id','parameter_name','parameter_value'];
    
    public static function getColumns()
    {
        $booking_table_columns_for_data = DB::table('parameter')->where('parameter_name','booking_table_columns_for_data')->first();
        
        $columns =  explode(',',$booking_table_columns_for_data->parameter_value);
        
        return $columns;
    }
    
    public static function getParameter($name)
    {
        $parameter = DB::table('parameter')->where('parameter_name',$name)->first();        
        return $parameter->parameter_value;
    }
 
}
