<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class FopEnquiry extends Model
{
   	protected $table = 'fop_enquiry_form';

    protected $fillable = ['id','email','phone','name','bike_branding','rent_bike','location','model','odo','registration_year','created_at','created_by','updated_at','updated_by'];

    
   
 
}
