<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BikeMake;
use App\Models\Area;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\Price;
use App\Models\PackagesReview;
use App\Transformers\BikeModelTransformer;
use DB;
use Illuminate\Support\Str;

class Packages extends Model
{

  protected $table = 'bikation_packages';

  protected $fillable = ['name', 'desc', 'destination', 'date_time', 'price', 'cover_img_id', 'available_seat','total_seats','ride_include','ride_exclude', 'ride_rule', 'meeting_point', 'recommanded_bike', 'total_km', 'total_days','status'];

  public $timestamps = false;

  
  /* Relationships */

  public function bikes(){
    return $this->hasMany('App\Models\Bike', 'model_id');
  }

  public function make(){
    return $this->belongsTo('App\Models\BikeMake', 'bike_make_id');
  }

  public function gallery(){
    return $this->belongsTo('App\Models\Gallery');
  }

  public function price(){
    return $this->hasOne('App\Models\Price');
  }

  function getThumbnailImg(){
    $image = Image::find($this->thumbnail_img_id);
    //$image = $image->transform();
    return $image;
  }

  function getAreas($cityId = 0){
    $area_ids = Bike::where('model_id', $this['id'])->groupBy('area_id')->where('status', 1)->get()->lists('area_id');
    $areas = Area::whereIn('id', $area_ids)->where('status', 1)->get();
    if($cityId != 0){
      $areas = Area::whereIn('id', $area_ids)->where('city_id', $cityId)->where('status', 1)->get();  
    }
    return $areas;
  }

  function getActiveBikeModels(){
    $activeBikeModelList=$this->where('status',1)->get();
    return $activeBikeModelList;
  }

  function getBikeMakeName($bike_make_id){
    $bikeMake=BikeMake::where('id',$bike_make_id)->first();
    return $bikeMake->bike_make;
  }

  function getBikeModelImages($bikeModel_id){
    $bikeModel_images = BikeModelImage::where('bikeModel_id', $bikeModel_id)->get();
    return $bikeModel_images;
  }

  function getBikeModelSpecKey($spec){
    $specArray = unserialize($spec);
    return $specArray[0];
  }

  function getBikeModelSpecValue($spec){
    $specArray = unserialize($spec);
    return $specArray[1];
  }

  public function transform(){
    $it = new BikeModelTransformer();
    return $it->transform($this);
  }

  function getUniqueBikeModels($city_id){
    $bikeModels = DB::table('bikes')
                  ->join('areas', 'areas.id', '=', 'bikes.area_id')
                  ->join('bike_models', 'bike_models.id', '=', 'bikes.model_id')
                  ->select('areas.city_id', 'areas.area', 'bikes.model_id', 'bike_models.bike_make_id', 'bike_models.bike_model', 'bike_models.thumbnail_img_id')
                  ->where('areas.city_id', $city_id)
                  ->where('bikes.status', 1)
                  ->where('areas.status', 1)
                  ->where('bike_models.status', 1)
                  ->groupBy('bikes.model_id')
                  ->get();

    foreach($bikeModels as $bikeModel){
      $bikeModel->bikeMake_name = $this->getBikeMakeName($bikeModel->bike_make_id);
    }  
    return $bikeModels;  
  }

  public function getModelDetailsById($dates = [], $cityId = 0){
    $image = $this->getThumbnailImg();
    $image = $image->transform();
    $make = $this->make;
    $makeLogo = Image::find($make['logo_id']);
    $makeLogo = $makeLogo->transform();
    $areas = $this->getAreas($cityId);
    foreach ($areas as $area) {
      $price = $this->getModelPrice($area['id']);
      $area["price"] = $price;
      $area->latitude = "0.0001234";
      $area->longitude = "0.0003456";
      
      if(!empty($dates)){
        $dispatcher = app('Dingo\Api\Dispatcher');
        $totalPrice = $dispatcher->with(['model_id' => $this->id, 
                                  'area_id' => $area['id'], 
                                  'start_date' => $dates['start_date'], 'end_date' => $dates['end_date'],
                                  'start_time' => $dates['start_time'], 'end_time' => $dates['end_time']
                                  ])->get('bookings/total-price');
        $area->price_details = $totalPrice;
      }
    }

    $data = [
      "id" => $this['id'],
      "name" => $this['bike_model'],
      "description" => $this['description'],
      "make_id" => $this['bike_make_id'],
      "logo_id" => $make['logo_id'],
      "logo" => $makeLogo,
      "available_locations" => $areas,
      "not_available_locations" => [],
      "image_id" => $image['id'],
      "image" => $image
    ];

    return $data;
  }

  public function getModelPrice($areaId){
      // "Rs 60/hour Weekdays"
      // "Rs 75/hour Weekends and holidays"
      // "Rs 100/hour for rentals more than 8hours",
      // "Rs 200/hour for rentals less than 8hour"
    $price = Price::where('model_id', $this['id'])->where('area_id', $areaId)->first();
    if($price['weekday_lt_8'] == $price['weekday_gt_8']){
      $line1 = 'Rs '.$price['weekday_lt_8'].'/hour on Weekdays';
      $line2 = 'Rs '.$price['weekend_lt_8'].'/hour on Weekends';
      $data = [$line1, $line2];
    }else{
      $line1 = 'Rs '.$price['weekday_lt_8'].'/hour for rentals less than 14 hour';
      $line2 = 'Rs '.$price['weekday_gt_8'].'/hour for rentals more than 14 hours';
      $data = [$line1, $line2];
    }
    return $data;

  }

  /*End Of Class*/
}
