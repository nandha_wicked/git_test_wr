<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactEnquiry extends Model
{

    protected $table = 'contact_enquiries';

    protected $fillable = ['first_name','last_name','email','mobile_num','enquiry'];

    public $timestamps = false;

}
