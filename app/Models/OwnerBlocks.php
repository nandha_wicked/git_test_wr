<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OwnerBlocks extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'owner_block';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','aggregator_id', 'model_id', 'start_datetime','end_datetime','booking_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    function getAggregatorName(){
        $user = Aggregator::where('id', $this->aggregator_id)->first();
        return $user->name;
    }
    
    function getModelName(){
        $model = BikeModel::where('id', $this->model_id)->first();
        return $model->bike_model;
    }
    
}
