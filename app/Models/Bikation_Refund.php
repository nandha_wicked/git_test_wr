<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Bikation_Refund extends Model
{

  protected $table = 'bikation_refund';

  protected $fillable = ['reference_ID','Vendor_ID','bikation_bike_rental', 'user','amount','refund_reason', 'status', 'transaction_ID_from_PG', 'created_on', 'updated_on','Created_By','Updated_by','Approved_By', 'Approved_on','refund_id_Razorpay','Booking_ID','Trip_ID'];

}
