<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Partner extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'partner';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','user_id', 'model_id', 'area_id','date_added'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}
