<?php

namespace App\Models;
use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
	protected $table = 'slots';
    protected $fillable = ['start_time','end_time'];
    public $timestamps = false;

    public static function getSlotsForTime($st, $et){
    	$start = Carbon::parse("today" . " " . $st);
    	$end = Carbon::parse("today" . " " . $et);

    	$ids = [];

    	while ($start <= $end){
    		array_push($ids, Slot::getSlotIdForStartTime($start->toTimeString()));
    		$start->addHour();
    	}

    	$ids_str = array_map(function($id){
    		return strval($id);
    	}, $ids);

    	return $ids_str;
    }

    public static function getEODTime(){
    	return "23:00:00";
    }

    public static function getSlotIdForStartTime($st){
    	return Slot::where("start_time", $st)->first()->id;
    }

    public static function fixEndSlots($ids){
       $to_add = [];
       foreach ($ids as $id) {
               $id = intval($id);
               $next_id = strval($id + 1);
               if(! in_array($next_id, $ids))
                       $to_add[] = $next_id;
       }
       return array_merge($ids, $to_add);
    }

}
