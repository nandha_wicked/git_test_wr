<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class SalesReport extends Model{
    protected $table = 'sales_report';

    protected $fillable = ['date','report'];

    public $timestamps = false;

    

  }
