<?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;

  class Availability extends Model{

    protected $table = 'availability';

    protected $fillable = ['city_id','area_id','make_id','model_id', 'bike_id', 'date', 'slots', 'status', 'booking_id'];

    public $timestamps = false;

  }//END OF CLASS