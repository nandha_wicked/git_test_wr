<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancelledEnquiry extends Model
{
    protected $table = 'cancelled_enquiry';
    protected $fillable = ['id','email','name','phone','enquiry','amount','created_at','updated_at','updated_by','notes','mode','customer_notes','coupon'];
}
