<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class AccessoriesImage extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories_image';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'model', 'color', 'full', 'small', 'medium', 'large', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function getModel()
    {
        $model = AccessoriesModel::where('id',$this->model)->first();
        return $model->model;
    }
    
    public function getColor()
    {
        $color = AccessoriesColor::where('id',$this->color)->first();
        return $color->color;
    }
    
   
}
