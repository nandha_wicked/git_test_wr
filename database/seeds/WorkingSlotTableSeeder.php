<?php
	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;
	use App\Models\WorkingSlot;

	class WorkingSlotTableSeeder extends Seeder {

    public function run()
    {
        DB::table('working_slots')->delete();
        $slots = array();
        for ($j=11; $j < 22; $j++) { 
            array_push($slots, $j);
        }
        
        $im = implode(",",$slots);

        for ($i=1; $i < 8; $i++) { 
        	WorkingSlot::create(['day_of_week' => $i, 'slots' => $im]);
    	}
    }

}
?>