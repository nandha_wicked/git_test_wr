<?php
	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;
	use App\Models\Slot;

	class SlotsTableSeeder extends Seeder {

    public function run()
    {   
        DB::table('slots')->delete();
    	for ($i=0; $i < 24; $i++) { 
        	Slot::create(['start_time' => $i.':00:00', 'end_time' => ($i+1).':00:00']);
    	}
    }

}
?>