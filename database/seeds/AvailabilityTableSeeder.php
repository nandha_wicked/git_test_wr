<?php
	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;
	use App\Models\Availabilty;

	class AvailabilityTableSeeder extends Seeder {

    public function run()
    {
    	for ($i=1; $i < 4; $i++) { 
            $j=10; 
        	Availabilty::create(['city_id' => 1, 'area_id' => 1, 'make_id' => 1, 'model_id' => 1, 'bike_id' => 1,
                'date' => '2015-'.$j.'-'.$j+$i, 'slots' => "11,12,13,14", 'status' => 'P']);
    	}
        for ($i=1; $i < 4; $i++) { 
            $j=20; 
            Availabilty::create(['city_id' => 1, 'area_id' => 1, 'make_id' => 1, 'model_id' => 3, 'bike_id' => $i,
                'date' => '2015-'.$j.'-'.$j+$i, 'slots' => "11,12,13,14", 'status' => 'P']);
        }
        for ($i=1; $i < 4; $i++) { 
            $j=15; 
            Availabilty::create(['city_id' => 1, 'area_id' => 1, 'make_id' => $i, 'model_id' => 1, 'bike_id' => $i,
                'date' => '2015-'.$j.'-'.$j+$i, 'slots' => "11,12,13,14", 'status' => 'P']);
        }
        for ($i=1; $i < 2; $i++) { 
            $j=5; 
            Availabilty::create(['city_id' => 1, 'area_id' => $i, 'make_id' => $i, 'model_id' => 1, 'bike_id' => 1,
                'date' => '2015-'.$j.'-'.$j+$i, 'slots' => "11,12,13,14", 'status' => 'P']);
        }
    }

}
?>