<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(SlotsTableSeeder::class);
        $this->command->info('slots table seeded!');
        
        $this->call(WorkingSlotTableSeeder::class);
        $this->command->info('working_slots table seeded!');
        
        $this->call(PriceTableSeeder::class);
        $this->command->info('prices table seeded!');
        
        Model::reguard();
    }
}
