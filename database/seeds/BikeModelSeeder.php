<?php
	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;
	use App\Models\BikeModel;

	class BikeModelSeeder extends Seeder {

    public function run()
    {
        BikeModel::create(['id' => 1, 'bike_make_id' => 1, 'bike_model' => 'Iron 883', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 2, 'bike_make_id' => 1, 'bike_model' => 'SuperLow', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 3, 'bike_make_id' => 1, 'bike_model' => 'Street 750', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 4, 'bike_make_id' => 2, 'bike_model' => 'Bonneville', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 5, 'bike_make_id' => 3, 'bike_model' => 'Desert Storm - 500', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 6, 'bike_make_id' => 3, 'bike_model' => 'Thunderbird - 500', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 7, 'bike_make_id' => 3, 'bike_model' => 'Continental GT', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 8, 'bike_make_id' => 3, 'bike_model' => 'Bullet - 500', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 9, 'bike_make_id' => 1, 'bike_model' => 'Test', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 10, 'bike_make_id' => 3, 'bike_model' => 'Classic Chrome - 500', 'cover_img_id' =>   8, 'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 11, 'bike_make_id' => 3, 'bike_model' => 'Despatch Limited Edition', 'cover_img_id'       => 8, 'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 12, 'bike_make_id' => 3, 'bike_model' => 'Classic - 350', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 13, 'bike_make_id' => 3, 'bike_model' => 'Bullet 350', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 14, 'bike_make_id' => 3, 'bike_model' => 'Thunderbird - 350', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 15, 'bike_make_id' => 3, 'bike_model' => 'Classic - 500', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 16, 'bike_make_id' => 4, 'bike_model' => 'Ninja 650', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 17, 'bike_make_id' => 2, 'bike_model' => 'Tiger 800 XCx', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 18, 'bike_make_id' => 5, 'bike_model' => 'TNT 600 GT', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);
        BikeModel::create(['id' => 19, 'bike_make_id' => 6, 'bike_model' => 'Scrambler', 'cover_img_id' => 8, 
                'thumbnail_img_id' => 9, 'gallery_id' => 4, 'status' => 1]);

    }

}
?>