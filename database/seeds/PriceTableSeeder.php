<?php
	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;
	use App\Models\Price;

	class PriceTableSeeder extends Seeder {

    public function run()
    {
    	$models = [
    		[
    			'model_id' => 1,
    			'weekday_gt_8' => 242,
    			'weekday_lt_8' => 580,
    			'weekend_gt_8' => 242,
    			'weekend_lt_8' => 580
    		],
    		[
    			'model_id' => 2,
    			'weekday_gt_8' => 200,
    			'weekday_lt_8' => 480,
    			'weekend_gt_8' => 200,
    			'weekend_lt_8' => 480
    		],
    		[
    			'model_id' => 3,
    			'weekday_gt_8' => 159,
    			'weekday_lt_8' => 380,
    			'weekend_gt_8' => 159,
    			'weekend_lt_8' => 380
    		],
    		[
    			'model_id' => 4,
    			'weekday_gt_8' => 242,
    			'weekday_lt_8' => 580,
    			'weekend_gt_8' => 242,
    			'weekend_lt_8' => 580
    		],
    		[
    			'model_id' => 5,
    			'weekday_gt_8' => 50,
    			'weekday_lt_8' => 50,
    			'weekend_gt_8' => 63,
    			'weekend_lt_8' => 63
    		],
    		[
    			'model_id' => 6,
    			'weekday_gt_8' => 50,
    			'weekday_lt_8' => 50,
    			'weekend_gt_8' => 63,
    			'weekend_lt_8' => 63
    		],
    		[
    			'model_id' => 7,
    			'weekday_gt_8' => 55,
    			'weekday_lt_8' => 55,
    			'weekend_gt_8' => 70,
    			'weekend_lt_8' => 70
    		],
    		[
    			'model_id' => 8,
    			'weekday_gt_8' => 50,
    			'weekday_lt_8' => 50,
    			'weekend_gt_8' => 63,
    			'weekend_lt_8' => 63
    		],
    		[
    			'model_id' => 9, // TEST
    			'weekday_gt_8' => 000,
    			'weekday_lt_8' => 000,
    			'weekend_gt_8' => 000,
    			'weekend_lt_8' => 000
    		],
    		[
    			'model_id' => 10,
    			'weekday_gt_8' => 50,
    			'weekday_lt_8' => 50,
    			'weekend_gt_8' => 63,
    			'weekend_lt_8' => 63
    		],
    		[
    			'model_id' => 11,
    			'weekday_gt_8' => 55,
    			'weekday_lt_8' => 55,
    			'weekend_gt_8' => 70,
    			'weekend_lt_8' => 70
    		],
    		[
    			'model_id' => 12,
    			'weekday_gt_8' => 42,
    			'weekday_lt_8' => 42,
    			'weekend_gt_8' => 50,
    			'weekend_lt_8' => 50
    		],
    		[
    			'model_id' => 13,
    			'weekday_gt_8' => 42,
    			'weekday_lt_8' => 42,
    			'weekend_gt_8' => 50,
    			'weekend_lt_8' => 50
    		],
    		[
    			'model_id' => 14,
    			'weekday_gt_8' => 42,
    			'weekday_lt_8' => 42,
    			'weekend_gt_8' => 50,
    			'weekend_lt_8' => 50
    		],
    		[
    			'model_id' => 15,
    			'weekday_gt_8' => 50,
    			'weekday_lt_8' => 50,
    			'weekend_gt_8' => 63,
    			'weekend_lt_8' => 63
    		],
    		[
    			'model_id' => 16,
    			'weekday_gt_8' => 230,
    			'weekday_lt_8' => 550,
    			'weekend_gt_8' => 230,
    			'weekend_lt_8' => 550
    		],
    		[
    			'model_id' => 17,
    			'weekday_gt_8' => 313,
    			'weekday_lt_8' => 750,
    			'weekend_gt_8' => 313,
    			'weekend_lt_8' => 750
    		],
    		[
    			'model_id' => 18,
    			'weekday_gt_8' => 230,
    			'weekday_lt_8' => 550,
    			'weekend_gt_8' => 230,
    			'weekend_lt_8' => 550
    		],
    		[
    			'model_id' => 19,
    			'weekday_gt_8' => 270,
    			'weekday_lt_8' => 650,
    			'weekend_gt_8' => 270,
    			'weekend_lt_8' => 650
    		],
            [
                'model_id' => 20,
                'weekday_gt_8' => 42,
                'weekday_lt_8' => 50,
                'weekend_gt_8' => 42,
                'weekend_lt_8' => 50
            ],
    	];
        DB::table('prices')->delete();
    	for ($i=1; $i <= 9 ; $i++) { 
    		foreach ($models as $model) {
    			Price::create(['model_id' => $model['model_id'], 'area_id' => $i, 
    				'weekday_gt_8' => $model['weekday_gt_8'], 'weekday_lt_8' => $model['weekday_lt_8'], 
    				'weekend_gt_8' => $model['weekend_gt_8'], 'weekend_lt_8' => $model['weekend_lt_8'] ]);
    		}
    	}
    }

}
