<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('value_type');
            $table->integer('value');
            $table->integer('total_count');
            $table->integer('available_count')->default(0);
            $table->dateTime('start_set');
            $table->dateTime('end_set');
            $table->boolean('new_user')->default(false);
            $table->integer('min_order')->nullable();
            $table->string('area_ids')->nullable();
            $table->string('model_ids')->nullable();
            $table->boolean('weekday')->default(false);
            $table->boolean('weekend')->default(false);
            $table->string('dates')->nullable();
            $table->string('device')->nullable()->default('BOTH');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}
