<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SingleMigrationFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full');
            $table->string('small')->nullable();
            $table->string('medium')->nullable();
            $table->string('large')->nullable();
            $table->string('raw_data')->nullable();
            $table->timestamps();
        });

        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('raw_data')->nullable();
            $table->timestamps();
        });

        Schema::create('bike_specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bike_model_id');
            $table->string('engine')->nullable();
            $table->string('power')->nullable();
            $table->string('seats')->nullable();
            $table->string('fuel_capacity')->nullable();
        });

        Schema::create('availability', function($table){
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('area_id');
            $table->integer('make_id');
            $table->integer('model_id');
            $table->integer('bike_id');
            $table->date('date');
            $table->string('slots')->nullable();
            $table->integer('booking_id');
           
        });
        
        Schema::create('gallery_image', function (Blueprint $table) {
            $table->integer('gallery_id');
            $table->integer('image_id');
        });

        Schema::create('slots', function (Blueprint $table) {
            $table->increments('id');
            $table->time('start_time');
            $table->time('end_time');
        });

        Schema::create('working_slots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('day_of_week');
            $table->string('slots');
        });
        
        Schema::table('bike_models', function($table)
        {
            $table->renameColumn('bikeMake_id', 'bike_make_id');
            $table->dropColumn(['gallery1_img', 'gallery2_img', 'gallery3_img', 'gallery4_img', 
                'gallery5_img', 'gallery6_img', 'spec1', 'spec2', 'spec3', 'spec4', 'cover_img', 'thumbnail_img']);
            $table->integer('cover_img_id')->nullable();
            $table->integer('thumbnail_img_id')->nullable();
            $table->integer('gallery_id')->nullable();
            $table->integer('spec_id')->nullable();
            $table->longText('description')->nullable();
        });

        Schema::table('bike_makes', function($table)
        {
            $table->integer('logo_id')->nullable();
        });

        Schema::table('users', function($table)
        {
            $table->date('dob')->nullable();
            $table->integer('profile_img_id')->nullable();
        }); 

        DB::statement('ALTER TABLE bookings MODIFY COLUMN total_paid double(10,2)');
        Schema::table('bookings', function($table){
            $table->renameColumn('from_datetime', 'start_datetime');
            $table->renameColumn('to_datetime', 'end_datetime');
            $table->renameColumn('booking_note', 'note');
            $table->renameColumn('total_paid', 'total_price');
            $table->dateTime('operation_end_datetime');
            $table->integer('start_slot_id');
            $table->integer('end_slot_id');
            $table->string('status');
            $table->string('pg_status')->nullable();
            $table->string('pg_txn_id')->nullable();
            $table->string('bank_txn_id')->nullable();
            $table->string('order_id')->nullable();
            $table->string('pg_mode')->nullable();
            $table->double('pg_txn_amount', 10, 2)->nullable();
            $table->text('raw_data')->nullable();
            $table->string('reference_id')->nullable();;
        });


        Schema::table('prices', function($table){
            $table->dropColumn(['name', 'week_day_price', 'week_end_price', 'days_price_3_6', 'days_price_7_14', 'days_price_15_30', 'days_price_30_plus']);
            $table->integer('model_id');
            $table->integer('area_id');
            $table->integer('weekday_gt_8');
            $table->integer('weekday_lt_8');
            $table->integer('weekend_gt_8');
            $table->integer('weekend_lt_8');
        });

        Schema::table('areas', function($table)
        {
            $table->string('code');
        });
       
        Schema::table('bikes', function($table)
        {
            $table->dropColumn(['price_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
        Schema::drop('gallery');
        Schema::drop('bike_specifications');
        Schema::drop('availability');
        Schema::drop('gallery_image');
        Schema::drop('slots');
        Schema::drop('working_slots');
        Schema::table('bike_models', function($table)
        {
            $table->renameColumn('bike_make_id', 'bikeMake_id');
            $table->string('gallery1_img');
            $table->string('gallery2_img');
            $table->string('gallery3_img');
            $table->string('gallery4_img');
            $table->string('gallery5_img');
            $table->string('gallery6_img');
            $table->text('spec1');
            $table->text('spec2');
            $table->text('spec3');
            $table->text('spec4');
            $table->string('cover_img');
            $table->string('thumbnail_img');
            $table->dropColumn(['gallery_id', 'spec_id', 'cover_img_id', 'thumbnail_img_id', 'description'] );
        });
        Schema::table('bike_makes', function($table)
        {
            $table->dropColumn('logo_id');
        });
        Schema::table('users', function($table)
        {
            $table->dropColumn(['dob', 'profile_img_id'] );
        });

        DB::statement('ALTER TABLE bookings MODIFY COLUMN total_price integer');
        Schema::table('bookings', function($table){
            $table->dropColumn(['operation_end_datetime', 'start_slot_id', 'end_slot_id', 'status',
            'pg_status', 'pg_txn_id', 'bank_txn_id', 'order_id', 'pg_mode', 'pg_txn_amount', 
            'raw_data', 'reference_id']);
            $table->renameColumn('start_datetime', 'from_datetime');
            $table->renameColumn('end_datetime', 'to_datetime');
            $table->renameColumn('total_price', 'total_paid');
            $table->renameColumn('note', 'booking_note');
        });

        Schema::table('prices', function($table){
            $table->dropColumn(['model_id', 'area_id', 'weekday_gt_8', 'weekday_lt_8', 'weekend_gt_8', 'weekend_lt_8']);
            $table->string('name');
            $table->integer('week_day_price');
            $table->integer('week_end_price');
            $table->integer('days_price_3_6');
            $table->integer('days_price_7_14');
            $table->integer('days_price_15_30');
            $table->integer('days_price_30_plus');
        });
        Schema::table('areas', function($table)
        {
            $table->dropColumn('code');
        });
        Schema::table('bikes', function($table)
        {
            $table->integer('price_id');
        });
    }
}
