@extends('a2b_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Returns</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/a2b/get-returns">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Location: </label>
		  		<select name="locationId" class="form-control">
                    @foreach($activeLocations as $location)
                        <option value="{{ $location->id }}" <?php if($location->id == $selected) echo "selected"?>>{{ $location->name }}</option>
                    @endforeach
                    <option value="0" <?php if($selected==0) echo "selected"?>>All Locations</option>
		  		</select>
		  	</div>
            

            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
            
		</form>
        
        &nbsp;
        
        
        <form class="form-inline" method="get" action="/a2b/get-returns">
			
		  	<div class="form-group">
                <input type="text" name="booking_id" placeholder="Search by booking" class="form-control"/>
            </div>
            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
		</form>
        
       
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        <div>&nbsp;</div>&nbsp;
        
        <label>{{$tableTitle}}</label>
        
        <div class="hidden-xs">
            
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1380px;" >
                <thead>
                    <tr>
                        <th style="width: 110px !important;">Action</th>
                        <th style="width: 90px !important;">Id</th>
                        <th style="width: 140px !important;">Customer</th>
                        <th style="width: 110px !important;">Model</th>
                        <th style="width: 150px !important;">Bike Assigned</th>
                        <th style="width: 120px !important;">From Location</th>
                        <th style="width: 100px !important;">To Location</th>
                        <th style="width: 300px !important;">Customer Instruction</th>
                        <th style="width: 180px !important;">User Document</th>
                        <th style="width: 100px !important;">Work Email Verified</th>

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookingsByLocation as $booking)
                        <form class="form-inline" method="post" action="/a2b/return">
                            <tr>
                                <td>
                                    <button class="btn btn-primary booking_return_btn" data-id="{{ $booking->id }}"><i class="fa fa-plus"></i> Return</button>
                                </td>
                                <td>{{ $booking['reference_id'] }}</td>
                                <?php $user = $booking->getUser(); ?>
                                <td>{{ $user->first_name }} - {{ $user->mobile_num }}</td>
                                <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                                <td>{{ $booking->getBikeNumber() }}</td>
                                <td>{{ $booking->getFromLocation() }}</td>
                                <td>{{ $booking->getToLocation() }}</td>
                                <td>{{ $booking->customer_instructions }}</td>
                                
                            </tr>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="visible-xs">
            @foreach($bookingsByLocation as $booking)
                <div class="card">
                  <div class="card-block">
                      <form class="form-inline" id="form{{$booking->id}}" method="post" action="/a2b/return">
                        <h4 class="card-title">{{ $booking['reference_id'] }}</h4>

                        <?php $user = $booking->getUser(); ?>  
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">User: {{ $user->first_name }} - {{ $user->mobile_num }}</li>
                            <li class="list-group-item">Model: {{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</li>
                            <li class="list-group-item">Bike: {{ $booking->getBikeNumber() }}</li>
                            <li class="list-group-item">{{ $booking->getFromLocation() }} - to - {{ $booking->getToLocation() }}</li>
                            <li class="list-group-item">Notes: {{ $booking->customer_instructions }}</li>
                        </ul>
                        <a href="javascript:{}" onclick="document.getElementById('form{{$booking->id}}').submit();" class="btn btn-primary">Return</a>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form>
                  </div>
                </div>
            @endforeach
        </div>
        
    </div>



    

@stop


