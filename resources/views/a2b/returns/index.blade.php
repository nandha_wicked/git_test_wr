@extends('a2b_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Returns</h2>
	
        
        
                <p>&nbsp;</p>
        @if($booking_or_movement == "booking")
        <form class="form-inline" method="get" action="/a2b/get-returns">
		@else
        <form class="form-inline" method="get" action="/a2b/get-movement-returns">
        @endif	
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Location: </label>
		  		<select name="locationId" class="form-control">
                    <option value="0" <?php if($selected==0) echo "selected"?>>All Locations</option>
                    @foreach($activeLocations as $location)
                        <option value="{{ $location->id }}" <?php if($location->id == $selected) echo "selected"?>>{{ $location->name }}</option>
                    @endforeach
		  		</select>
		  	</div>
            

            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
            
		</form>
        
        &nbsp;
        
        
        <form class="form-inline" method="get" action="/a2b/get-returns">
			
		  	<div class="form-group">
                <input type="text" name="booking_id" placeholder="Search by booking" class="form-control"/>
            </div>
            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
		</form>
        
       
		@if(count($errors) > 0)    
        
            @if($errors->all()[0]!="Booking returned with money")
                <script type="text/javascript"> 
                    alert("@foreach($errors->all() as $error) {{ $error }} @endforeach"); 
                </script>
            @else
                <script type="text/javascript"> 
                    alert("{{ $errors->all()[1] }}"); 
                </script>
			@endif
            
		@endif
        <div>&nbsp;</div>&nbsp;
        
        <label>{{$tableTitle}}</label>
        
        <div class="hidden-xs">
            
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1380px;" >
                <thead>
                    <tr>
                        <th style="width: 110px !important;">Action</th>
                        <th style="width: 90px !important;">Id</th>
                        @if($booking_or_movement == "booking")
                        <th style="width: 140px !important;">Customer</th>
                        @endif
                        <th style="width: 110px !important;">Model</th>
                        <th style="width: 150px !important;">Bike Assigned</th>
                        <th style="width: 120px !important;">From Location</th>
                        <th style="width: 100px !important;">To Location</th>
                        @if($booking_or_movement == "booking")
                        <th style="width: 300px !important;">Customer Instruction</th>
                        @else
                        <th style="width: 300px !important;">Notes</th>
                        @endif

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookingsByLocation as $booking)
                            <tr>
                                <td>
                                    <button class="btn btn-primary booking_return_btn" data-id="{{ $booking->id }}" data-bikeimages="{{ $booking['bike_images'] }}" data-fuelimages="{{ $booking['fuel_images'] }}"><i class="fa fa-plus"></i> Return</button>
                                </td>
                                @if($booking_or_movement == "booking")
                                <td>{{ $booking['reference_id'] }}</td>
                                <?php $user = $booking->getUser(); ?>
                                <td>{{ $user->first_name }} - {{ $user->mobile_num }}</td>
                                @else
                                <td>{{ $booking->id }}</td>
                                @endif
                                <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                                <td>{{ $booking->getBikeNumber() }}</td>
                                <td>{{ $booking->getFromLocation() }}</td>
                                <td>{{ $booking->getToLocation() }}</td>
                                @if($booking_or_movement == "booking")
                                <td>{{ $booking->customer_instructions }}</td>
                                @else
                                <td>{{ $booking->notes }}</td>
                                @endif
                                
                            </tr>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="visible-xs">
            @foreach($bookingsByLocation as $booking)
                <div class="card">
                  <div class="card-block">
                        @if($booking_or_movement == "booking")
                        <h4 class="card-title">{{ $booking['reference_id'] }}</h4>
                        @else
                        <h4 class="card-title">{{ $booking['id'] }}</h4>
                        @endif

                        <?php $user = $booking->getUser(); ?>  
                        <ul class="list-group list-group-flush">
                            @if($booking_or_movement == "booking")
                            <li class="list-group-item">User: {{ $user->first_name }} - {{ $user->mobile_num }}</li>
                            @endif
                            <li class="list-group-item">Model: {{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</li>
                            <li class="list-group-item">Bike: {{ $booking->getBikeNumber() }}</li>
                            <li class="list-group-item">{{ $booking->getFromLocation() }} - to - {{ $booking->getToLocation() }}</li>
                            @if($booking_or_movement == "booking")
                            <li class="list-group-item">Notes: {{ $booking->customer_instructions }}</li>
                            @else
                            <li class="list-group-item">Notes: {{ $booking->notes }}</li>
                            @endif
                        </ul>
                        <a href="javascript:{}" class="btn btn-primary booking_return_btn" data-id="{{ $booking->id }}" data-bikeimages="{{ $booking['bike_images'] }}" data-fuelimages="{{ $booking['fuel_images'] }}">Return</a>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
                </div>
            @endforeach
        </div>
        <img hidden id="imgID">
    </div>

<script>
    $('.booking_return_btn').click(function(){
        var id	= $(this).data('id');
        $('#returnModalIdField').val(id);
        
        var bikeImagesStr = $(this).data('bikeimages');
        var fuelImagesStr = $(this).data('fuelimages');
        
        if(bikeImagesStr != "")
        {
            $('#returnModalCarouselBike').html("");
            $('#returnModalCarouselBikeInd').html("");
            
            var bikeImages = bikeImagesStr.split("@");
            bikeImages.forEach(function(url, indexForUrl){
                if(indexForUrl==0)
                {
                    $('#returnModalCarouselBike').prepend("<div class=\"item active\"><img class=\"img-responsive\" src=\""+url+"\"  alt=\"...\"></div>");
                    
                    $('#returnModalCarouselBikeInd').append("<li data-target=\"#myCarousel\" data-slide-to=\""+indexForUrl+"\" class=\"active\"></li>");
                }
                else
                {
                    $('#returnModalCarouselBike').prepend("<div class=\"item\"><img class=\"img-responsive\" src=\""+url+"\"  alt=\"...\"></div>");
                    $('#returnModalCarouselBikeInd').append("<li data-target=\"#myCarousel\" data-slide-to=\""+indexForUrl+"\"></li>");
                }
                
                $('#returnModalCarouselBikeDiv').removeClass("hidden");
            });
        }
        else
        {
            $('#returnModalCarouselBikeDiv').addClass("hidden");
        }
        
        
        if(fuelImagesStr != "")
        {
            $('#returnModalCarouselFuel').html("");
            $('#returnModalCarouselFuelInd').html("");
            
            var fuelImages = fuelImagesStr.split("@");
            fuelImages.forEach(function(url, indexForUrl){
                if(indexForUrl==0)
                {
                    $('#returnModalCarouselFuel').prepend("<div class=\"item active\"><img class=\"img-responsive\" src=\""+url+"\"  alt=\"...\"></div>");
                    
                    $('#returnModalCarouselFuelInd').append("<li data-target=\"#myCarousel\" data-slide-to=\""+indexForUrl+"\" class=\"active\"></li>");
                }
                else
                {
                    $('#returnModalCarouselFuel').prepend("<div class=\"item\"><img class=\"img-responsive\" src=\""+url+"\"  alt=\"...\"></div>");
                    $('#returnModalCarouselFuelInd').append("<li data-target=\"#myCarousel\" data-slide-to=\""+indexForUrl+"\"></li>");
                }
                
                $('#returnModalCarouselFuelDiv').removeClass("hidden");
            });
        }
        else
        {
            $('#returnModalCarouselFuelDiv').addClass("hidden");
        }
        
        
        $('#addEndOdo-modal').modal('show');
        
    });
</script>

    

@stop

@section('model')

    <div class="modal fade" id="addEndOdo-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-inline" id="returnForm" method="post" action="/a2b/return">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirm Return</h4>
                    </div>
                    
                    <div class="modal-header hidden" id="returnModalCarouselBikeDiv">
                      <!-- Wrapper for slides -->
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators" id="returnModalCarouselBikeInd">
                            
                          </ol>
                          <div class="carousel-inner" id="returnModalCarouselBike">

                          </div>
                        </div>

                      
                    </div>
                    
                    <div class="modal-header hidden" id="returnModalCarouselFuelDiv">
                      <!-- Wrapper for slides -->
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators" id="returnModalCarouselFuelInd">
                            
                          </ol>
                          <div class="carousel-inner" id="returnModalCarouselFuel">

                          </div>
                        </div>

                      
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-3">Ending Odo:</label>
                            <div class="col-md-9">
                                <input type="text" id="end_odo_in_modal" name="end_odo" class="form-control" />
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <fieldset style="margin: 5px;border: 1px solid grey">
                        <div id="addReturnImage"></div>
                        <div style="text-align:center; margin:5px;">
                            <button type="button" class="btn btn-primary return_img_add_btn" data-countReturnAdd="0" id="addReturnImageBtn"><i class="fa fa-plus"></i> Add Bike Image</button>
                        </div>
                    </fieldset>
                    <fieldset style="margin: 5px;border: 1px solid grey">
                        <div id="addFuelImage"></div>
                        <div style="text-align:center; margin:5px;">
                            <button type="button" class="btn btn-primary fuel_img_add_btn" data-countFuelAdd="0" id="addFuelImageBtn"><i class="fa fa-plus"></i> Add Fuel Image</button>
                        </div>
                    </fieldset>
                    <input type="hidden" id="returnModalIdField" name="id">
                    <input type="hidden" name="booking_or_movement" value="{{$booking_or_movement}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary booking_return_submit_form_btn"><i class="fa fa-check-circle"></i> Return</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


<script>
    
    var countReturnAddObj = [];
    var countFuelAddObj = [];
    
    $('.booking_return_submit_form_btn').click(function(){
            $('#returnForm').submit();
    });
    
    $('.return_img_add_btn').click(function(){
        var  id	= $(this).data('id');

        if(countReturnAddObj[id] == null)
        {
            countReturnAddObj[id] = {"count":0};
            var  countReturnAdd	= 0;
        }
        else
        {
            countReturnAddObj[id].count++;
            var countReturnAdd = countReturnAddObj[id].count;
        }


        $('#addReturnImage').append("<fieldset style=\"border:1px solid silver; margin:10px;\"><div class=\"col-md-9\"><input name=\"idImageFile"+countReturnAdd+"\" type=\"file\" data-idsuffixreturnadd=\""+countReturnAdd+"\"  id=\"fileUploadReturnAdd"+countReturnAdd+"\"><input name=\"imageBase64"+countReturnAdd+"\" type=\"hidden\" id=\"inputAddReturnID"+countReturnAdd+"\"/><img hidden id=\"imgAddReturnID"+countReturnAdd+"\"/><input name=\"image[]\" type=\"hidden\" value=\""+countReturnAdd+"\"/></div></fieldset>");

        el('fileUploadReturnAdd'+countReturnAdd).addEventListener("change", readImageReturnAdd, false);
        countReturnAdd++;
        $(this).attr('data-countReturnAdd', countReturnAdd);
        $(this).hide();


        resizedReturnAdd = 0;


    });
    
    $('.fuel_img_add_btn').click(function(){
        var  id	= $(this).data('id');

        if(countFuelAddObj[id] == null)
        {
            countFuelAddObj[id] = {"count":0};
            var  countFuelAdd	= 0;
        }
        else
        {
            countFuelAddObj[id].count++;
            var countFuelAdd = countFuelAddObj[id].count;
        }


        $('#addFuelImage').append("<fieldset style=\"border:1px solid silver; margin:10px;\"><div class=\"col-md-9\"><input name=\"fuelImageFile"+countFuelAdd+"\" type=\"file\" data-idsuffixfueladd=\""+countFuelAdd+"\"  id=\"fileUploadFuelAdd"+countFuelAdd+"\"><input name=\"fuelimageBase64"+countFuelAdd+"\" type=\"hidden\" id=\"inputAddFuelID"+countFuelAdd+"\"/><img hidden id=\"imgAddFuelID"+countFuelAdd+"\"/><input name=\"fuelimage[]\" type=\"hidden\" value=\""+countFuelAdd+"\"/></div></fieldset>");

        el('fileUploadFuelAdd'+countFuelAdd).addEventListener("change", readImageFuelAdd, false);
        countFuelAdd++;
        $(this).attr('data-countFuelAdd', countFuelAdd);
        $(this).hide();


        resizedFuelAdd = 0;


    });
    
</script>
        
<script>
    
    function el(id) {
      return document.getElementById(id);
    } // Get elem by ID

    var imageObj = el("imgID");
    var idsuffix;
    var idbooking;
    var resized = 0;
    
    var idsuffixReturnAdd;
    var resizedReturnAdd = 0;
    
    var idsuffixFuelAdd;
    var resizedFuelAdd = 0;
    
    
    function readImageReturnAdd() {
      if (this.files && this.files[0]) {
        idsuffixReturnAdd = $(this).data('idsuffixreturnadd');
        var FR = new FileReader();
        FR.onload = function(e) {
            var imageObjReturnAdd = document.getElementById('imgAddReturnID'+idsuffixReturnAdd);
            imageObjReturnAdd.src = e.target.result;  
            imageObjReturnAdd.onload = function()
            {
                if(resizedReturnAdd == 0)
                {
                    resizedReturnAdd = 1;
                    var HERMITE = new Hermite_class();
                    //default resize
                    HERMITE.resize_image('imgAddReturnID'+idsuffixReturnAdd, 300, 300*imageObjReturnAdd.height/imageObjReturnAdd.width, 100, true, function(){
                        $('#inputAddReturnID'+idsuffixReturnAdd).val($('#imgAddReturnID'+idsuffixReturnAdd).attr('src'));   
                        $('#addReturnImageBtn').show();
                    });
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    function readImageFuelAdd() {
      if (this.files && this.files[0]) {
        idsuffixFuelAdd = $(this).data('idsuffixfueladd');
        var FR = new FileReader();
        FR.onload = function(e) {
            var imageObjFuelAdd = document.getElementById('imgAddFuelID'+idsuffixFuelAdd);
            imageObjFuelAdd.src = e.target.result;  
            imageObjFuelAdd.onload = function()
            {
                if(resizedFuelAdd == 0)
                {
                    resizedFuelAdd = 1;
                    
                    var HERMITE = new Hermite_class();
                    //default resize
                    HERMITE.resize_image('imgAddFuelID'+idsuffixFuelAdd, 300, 300*imageObjFuelAdd.height/imageObjFuelAdd.width, 100, true, function(){
                        $('#inputAddFuelID'+idsuffixFuelAdd).val($('#imgAddFuelID'+idsuffixFuelAdd).attr('src'));                        
                        $('#addFuelImageBtn').show();
                    });
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    

</script>

@stop
