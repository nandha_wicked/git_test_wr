@extends('a2b_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Deliveries</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/a2b/get-movement-deliveries">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Location: </label>
		  		<select name="locationId" class="form-control">
                    @foreach($activeLocations as $location)
                        <option value="{{ $location->id }}" <?php if($location->id == $selected) echo "selected"?>>{{ $location->name }}</option>
                    @endforeach
                    <option value="0" <?php if($selected==0) echo "selected"?>>All Locations</option>
		  		</select>
		  	</div>
            

            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
            
		</form>
        
        &nbsp;
        
        
        <form class="form-inline" method="get" action="/a2b/get-movement-deliveries">
			
		  	<div class="form-group">
                <input type="text" name="booking_id" placeholder="Search by booking" class="form-control"/>
            </div>
            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
		</form>
        
       
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        <div>&nbsp;</div>&nbsp;
        
        <label>{{$tableTitle}}</label>
        
        <div class="hidden-xs">
            
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1380px;" >
                <thead>
                    <tr>
                        <th style="width: 110px !important;">Action</th>
                        <th style="width: 90px !important;">Id</th>
                        <th style="width: 110px !important;">Model</th>
                        <th style="width: 150px !important;">Bike Number</th>
                        <th style="width: 120px !important;">From Location</th>
                        <th style="width: 100px !important;">To Location</th>
                        <th style="width: 300px !important;">Notes</th>

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookingsByLocation as $booking)
                        <form class="form-inline" method="post" action="/a2b/movement-deliver">
                            <tr>
                                <td>
                                    @if($booking->status == "Movement Cancelled")
                                        <p style="color: red; font:bold 15px arial, sans-serif;">Movement Cancelled</p>
                                        <button class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}"><i class="fa fa-plus"></i> Start Movement</button>

                                    @else

                                        <button class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}"><i class="fa fa-plus"></i> Start Movement</button>
                                    
                                    @endif
                                </td>
                                <td>{{ $booking['id'] }}</td>
                                <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                                <td>{{ $booking->getBikeNumber() }}</td>
                                <td>{{ $booking->getFromLocation() }}</td>
                                <td>{{ $booking->getToLocation() }}</td>
                                <td>{{ $booking->notes }}</td>
                                


                        </tr>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="visible-xs">
            @foreach($bookingsByLocation as $booking)
                <div class="card">
                  <div class="card-block">
                    <form class="form-inline" id="form{{$booking->id}}" method="post" action="/a2b/movement-deliver">
                        <h4 class="card-title">{{ $booking['reference_id'] }}</h4>

                        <?php $user = $booking->getUser(); ?>  
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">User: {{ $user->first_name }} - {{ $user->mobile_num }}</li>
                            <li class="list-group-item">Model: {{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</li>
                            <li class="list-group-item">Bike: {{ $booking->getBikeNumber() }}</li>
                            <li class="list-group-item">{{ $booking->getFromLocation() }} - to - {{ $booking->getToLocation() }}</li>
                            <li class="list-group-item">Notes: {{ $booking->customer_instructions }}</li>
                            <li class="list-group-item">
                                @if(count($booking['user_document'])==0)
                                -- Not Availabe --
                                @else

                                    @foreach($booking['user_document'] as $user_doc)
                                        @if($user_doc->status == 0)
                                            <button class="btn btn-primary user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @elseif($user_doc->status == 1)
                                            <button class="btn btn-success user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @else
                                            <button class="btn btn-danger user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @endif	
                                    @endforeach
                                @endif
                            </li>
                        </ul>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="javascript:{}" onclick="document.getElementById('form{{$booking->id}}').submit();" class="btn btn-primary">Deliver</a>
                      </form>
                  </div>
                </div>
            @endforeach
        </div>
        
    </div>


<script type="text/javascript">


        $('.user_doc_show_btn').click(function(){
            var  user_document	= $(this).data('userdocument');
            var  status	= $(this).data('status');
            var  doctype	= $(this).data('doctype');
            var  reason	= $(this).data('reason');
            var  docid	= $(this).data('docid');
            var  userid	= $(this).data('userid');

            var  search_user_status	=  $("#seach-modal input[name='user_status']:checked").val();
            var  search_seach_by	= $('#seach-modal select[name=seach_by]').val();
            var  search_txtname	= $('#seach-modal input[name=tx_search]').val();


            //user_document = user_document.replace("//", "");

            //var coverImgHolder = '<img src="http:'+user_document+'" />';
            var coverImgHolder = '<img src="http:'+user_document+'" width=100%""></img>';
            $('#old_coverImage_holder').html(coverImgHolder);
            $('#user_document_reason').html(reason);
            $('#doc_id').val(docid);
            $('#user_id').val(userid);
            $('#user_doc').val(doctype);

            $('#search_user_status').val(search_user_status);
            $('#search_seach_by').val(search_seach_by);
            $('#search_txtname').val(search_txtname);
            if(status == '1')
                $('#bmose1').prop("checked",true);	
            else
                $('#bmose2').prop("checked",true);		


            $('#specBikeModel-modal').modal('show');
        });
</script>
    

@stop

@section('model')

<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/users_with_doc/update" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View Document</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="col-md-12">
						<div class="form-group">
		        			<div class="row">
									<div class="col-md-12">
									<div class="col-md-1"></div>
		        				
				        			<div id="old_coverImage_holder"></div>
				        			</div>
		        			</div>
		        		</div>
						
						<div class="form-group">
							<div class="col-md-12">
		        			<div class="row">
		        			Reason: 
							<textarea type="text" name="user_document_reason" id="user_document_reason" rows="3" style="width:40%"></textarea>
							
		        			</div>
							</div>
		        		</div>
						<div class="form-group">
		        			<div class="row">
							<div class="col-md-12">
		        			Status: 
							<label class="radio-inline">
								  <input type="radio" name="status" id="bmose1" value="1"> Approve
								</label>
								<label class="radio-inline">
								  <input type="radio" name="status" id="bmose2" value="2"> Not Approve
								</label>
								<label id="requestorStatus">
        						</label>
								</div>
		        			</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="doc_id" id="doc_id" value="">
						<input type="hidden" name="user_id" id="user_id" value="">
						<input type="hidden" name="user_doc" id="user_doc" value="">
						<input type="hidden" name="source" value="a2b">
						<input type="hidden" name="selected" value="{{ $selected }}">
						<input type="hidden" name="search_user_status" id="search_user_status" value="">
						<input type="hidden" name="search_seach_by" id="search_seach_by" value="">
						<input type="hidden" name="search_txtname" id="search_txtname" value="">
						</div>
		      		</div>
		      		<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop


