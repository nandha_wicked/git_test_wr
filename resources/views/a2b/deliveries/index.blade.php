@extends('a2b_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">{{ucfirst($booking_or_movement)}} Deliveries</h2>
	
        
        
                <p>&nbsp;</p>
        @if($booking_or_movement == "booking")
        <form class="form-inline" method="get" action="">
		@else
        <form class="form-inline" method="get" action="/a2b/get-movement-deliveries">
        @endif
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Location: </label>
		  		<select name="locationId" class="form-control">
                    <option value="0" <?php if($selected==0) echo "selected"?>>All Locations</option>
                    @foreach($activeLocations as $location)
                        <option value="{{ $location->id }}" <?php if($location->id == $selected) echo "selected"?>>{{ $location->name }}</option>
                    @endforeach
		  		</select>
		  	</div>
            

            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
            
		</form>
        
        &nbsp;
        
        
        <form class="form-inline" method="get" action="/a2b/get-deliveries">
			
		  	<div class="form-group">
                <input type="text" name="booking_id" placeholder="Search by booking" class="form-control"/>
            </div>
            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
		</form>
        
       
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        <div>&nbsp;</div>&nbsp;
        
        <label>{{$tableTitle}}</label>
        
        <div class="hidden-xs">
            
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1380px;" >
                <thead>
                    <tr>
                        <th style="width: 110px !important;">Action</th>
                        <th style="width: 90px !important;">Id</th>
                        @if($booking_or_movement == "booking")
                        <th style="width: 140px !important;">Customer</th>
                        @endif
                        <th style="width: 110px !important;">Model</th>
                        <th style="width: 150px !important;">Bike Assigned</th>
                        <th style="width: 120px !important;">From Location</th>
                        <th style="width: 100px !important;">To Location</th>
                        @if($booking_or_movement == "booking")
                        <th style="width: 300px !important;">Customer Instruction</th>
                        <th style="width: 180px !important;">User Document</th>
                        @else
                        <th style="width: 300px !important;">Notes</th>
                        @endif

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookingsByLocation as $booking)
                            <tr>
                                <td>
                                    @if($booking->status == "Booking Cancelled")
                                        <p style="color: red; font:bold 15px arial, sans-serif;">Booking Cancelled</p>
                                        <button type="button" class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}"  data-useramountpending="{{ $booking['user_amount_pending'] }}"><i class="fa fa-plus"></i> Deliver</button>

                                    @else

                                        <button type="button" class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-useramountpending="{{ $booking['user_amount_pending'] }}"><i class="fa fa-plus"></i> Deliver</button>
                                    
                                    @endif
                                </td>
                                @if($booking_or_movement == "booking")
                                <td>{{ $booking['reference_id'] }}</td>
                                <?php $user = $booking->getUser(); ?>
                                <td>{{ $user->first_name }} - {{ $user->mobile_num }}</td>
                                @else
                                <td>{{ $booking->id }}</td>
                                @endif
                                <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                                <td>{{ $booking->getBikeNumber() }}</td>
                                <td>{{ $booking->getFromLocation() }}</td>
                                <td>{{ $booking->getToLocation() }}</td>
                                @if($booking_or_movement == "booking")
                                <td>{{ $booking->customer_instructions }}</td>
                                <td>
                                    @if(count($booking['user_document'])==0)
                                    -- Not Availabe --
                                    @else

                                        @foreach($booking['user_document'] as $user_doc)
                                        @if($user_doc->status == 0)
                                        <button type="button" class="btn btn-primary user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>

                                        @elseif($user_doc->status == 1)
                                        <button type="button" class="btn btn-success user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @else
                                        <button type="button" class="btn btn-danger user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>


                                        @endif	
                                        @endforeach
                                    @endif
                                </td>
                                @else
                                <td>{{ $booking->notes }}</td>
                                @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="visible-xs">
            @foreach($bookingsByLocation as $booking)
                <div class="card">
                  <div class="card-block">
                        @if($booking_or_movement == "booking")
                        <h4 class="card-title">{{ $booking['reference_id'] }}</h4>
                        @else
                        <h4 class="card-title">{{ $booking['id'] }}</h4>
                        @endif
                        <?php $user = $booking->getUser(); ?>  
                        <ul class="list-group list-group-flush">
                            @if($booking_or_movement == "booking")
                            <li class="list-group-item">User: {{ $user->first_name }} - {{ $user->mobile_num }}</li>
                            @endif
                            <li class="list-group-item">Model: {{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</li>
                            <li class="list-group-item">Bike: {{ $booking->getBikeNumber() }}</li>
                            <li class="list-group-item">{{ $booking->getFromLocation() }} - to - {{ $booking->getToLocation() }}</li>
                            
                            @if($booking_or_movement == "booking")
                            <li class="list-group-item">Notes: {{ $booking->customer_instructions }}</li>
                            <li class="list-group-item">
                                @if(count($booking['user_document'])==0)
                                -- Not Availabe --
                                @else

                                    @foreach($booking['user_document'] as $user_doc)
                                        @if($user_doc->status == 0)
                                            <button type="button" class="btn btn-primary user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @elseif($user_doc->status == 1)
                                            <button type="button" class="btn btn-success user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @else
                                            <button type="button" class="btn btn-danger user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @endif	
                                    @endforeach
                                @endif
                            </li>
                            @else
                            <li class="list-group-item">Notes: {{ $booking->notes }}</li>
                            @endif
                        </ul>
                        <a href="javascript:{}" class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-useramountpending="{{ $booking['user_amount_pending'] }}">Deliver</a>
                  </div>
                </div>
            @endforeach
        </div>
            
            <img hidden id="imgID">
        
    </div>


<script type="text/javascript">


        $('.user_doc_show_btn').click(function(){
            var  user_document	= $(this).data('userdocument');
            var  status	= $(this).data('status');
            var  doctype	= $(this).data('doctype');
            var  reason	= $(this).data('reason');
            var  docid	= $(this).data('docid');
            var  userid	= $(this).data('userid');

            var  search_user_status	=  $("#seach-modal input[name='user_status']:checked").val();
            var  search_seach_by	= $('#seach-modal select[name=seach_by]').val();
            var  search_txtname	= $('#seach-modal input[name=tx_search]').val();


            //user_document = user_document.replace("//", "");

            //var coverImgHolder = '<img src="http:'+user_document+'" />';
            var coverImgHolder = '<img src="http:'+user_document+'" width=100%""></img>';
            $('#old_coverImage_holder').html(coverImgHolder);
            $('#user_document_reason').html(reason);
            $('#doc_id').val(docid);
            $('#user_id').val(userid);
            $('#user_doc').val(doctype);

            $('#search_user_status').val(search_user_status);
            $('#search_seach_by').val(search_seach_by);
            $('#search_txtname').val(search_txtname);
            if(status == '1')
                $('#bmose1').prop("checked",true);	
            else
                $('#bmose2').prop("checked",true);		


            $('#specBikeModel-modal').modal('show');
        });
    
    
        $('.booking_deliver_btn').click(function(){
            var id	= $(this).data('id');
            var useramountpending = $(this).data('useramountpending');
            if(useramountpending > 0)
            {
                $('#addAmountCollectionDetails').removeClass("hidden");
                $('#addAmountCollectionDetailsLabel').html("Collect Rs. "+useramountpending+" from the customer before delivering the bike.");
                alert("Collect Rs. "+useramountpending+" from the customer before delivering the bike.");
            }
            else
            {
                $('#addAmountCollectionDetailsLabel').html("");
                $('#addAmountCollectionDetails').addClass("hidden");
            }
            $('#deliverModalIdField').val(id);
            $('#addStartOdo-modal').modal('show');
        });
    
</script>
    

@stop

@section('model')

<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/users_with_doc/update" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View Document</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="col-md-12">
						<div class="form-group">
		        			<div class="row">
									<div class="col-md-12">
									<div class="col-md-1"></div>
		        				
				        			<div id="old_coverImage_holder"></div>
				        			</div>
		        			</div>
		        		</div>
						
						<div class="form-group">
							<div class="col-md-12">
		        			<div class="row">
		        			Reason: 
							<textarea type="text" name="user_document_reason" id="user_document_reason" rows="3" style="width:40%"></textarea>
							
		        			</div>
							</div>
		        		</div>
						<div class="form-group">
		        			<div class="row">
							<div class="col-md-12">
		        			Status: 
							<label class="radio-inline">
								  <input type="radio" name="status" id="bmose1" value="1"> Approve
								</label>
								<label class="radio-inline">
								  <input type="radio" name="status" id="bmose2" value="2"> Not Approve
								</label>
								<label id="requestorStatus">
        						</label>
								</div>
		        			</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="doc_id" id="doc_id" value="">
						<input type="hidden" name="user_id" id="user_id" value="">
						<input type="hidden" name="user_doc" id="user_doc" value="">
						<input type="hidden" name="source" value="a2b">
						<input type="hidden" name="selected" value="{{ $selected }}">
						<input type="hidden" name="search_user_status" id="search_user_status" value="">
						<input type="hidden" name="search_seach_by" id="search_seach_by" value="">
						<input type="hidden" name="search_txtname" id="search_txtname" value="">
						</div>
		      		</div>
		      		<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


<div class="modal fade" id="addStartOdo-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-inline" id="deliverForm" method="post" action="/a2b/deliver">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delivery</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3">Starting Odo:</label>
                        <div class="col-md-9">
                            <input type="text" id="start_odo_in_modal" name="start_odo" class="form-control" />
                        </div>
                    </div>
                    <div id="addAmountCollectionDetails" class="hidden">
                        <label id="addAmountCollectionDetailsLabel"></label>
                        <div class="form-group">
                            <label class="col-md-3">Payment Method:</label>
                            <div class="col-md-9">
                                <select name="payment_method" class="form-control">
                                    <option value="1">Cash</option>
                                    <option value="4">PayTM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Payment ID:</label>
                            <div class="col-md-9">
                                <input type="text" name="payment_id" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <fieldset style="margin: 5px;border: 1px solid grey">
                        <div id="addDeliveryImage"></div>
                        <div style="text-align:center; margin:5px;">
                            <button type="button" class="btn btn-primary delivery_img_add_btn" data-countDeliveryAdd="0" id="addDeliveryImageBtn"><i class="fa fa-plus"></i> Add Bike Image</button>
                        </div>
                    </fieldset>
                    <fieldset style="margin: 5px;border: 1px solid grey">
                        <div id="addFuelImage"></div>
                        <div style="text-align:center; margin:5px;">
                            <button type="button" class="btn btn-primary fuel_img_add_btn" data-countFuelAdd="0" id="addFuelImageBtn"><i class="fa fa-plus"></i> Add Fuel Image</button>
                        </div>
                    </fieldset>
                    
                    
                    
                </div>
                <input type="hidden" id="deliverModalIdField" name="id">
                <input type="hidden" name="booking_or_movement" value="{{$booking_or_movement}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary booking_delivery_submit_form_btn"><i class="fa fa-check-circle"></i> Deliver</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    
    var countDeliveryAddObj = [];
    var countFuelAddObj = [];
    
    $('.booking_delivery_submit_form_btn').click(function(){
            $('#deliverForm').submit();
    });
    
    $('.delivery_img_add_btn').click(function(){
        var  id	= $(this).data('id');

        if(countDeliveryAddObj[id] == null)
        {
            countDeliveryAddObj[id] = {"count":0};
            var  countDeliveryAdd	= 0;
        }
        else
        {
            countDeliveryAddObj[id].count++;
            var countDeliveryAdd = countDeliveryAddObj[id].count;
        }


        $('#addDeliveryImage').append("<fieldset style=\"border:1px solid silver; margin:10px;\"><div class=\"col-md-9\"><input name=\"idImageFile"+countDeliveryAdd+"\" type=\"file\" data-idsuffixdeliveryadd=\""+countDeliveryAdd+"\"  id=\"fileUploadDeliveryAdd"+countDeliveryAdd+"\"><input name=\"imageBase64"+countDeliveryAdd+"\" type=\"hidden\" id=\"inputAddDeliveryID"+countDeliveryAdd+"\"/><img hidden id=\"imgAddDeliveryID"+countDeliveryAdd+"\"/><input name=\"image[]\" type=\"hidden\" value=\""+countDeliveryAdd+"\"/></div></fieldset>");

        el('fileUploadDeliveryAdd'+countDeliveryAdd).addEventListener("change", readImageDeliveryAdd, false);
        countDeliveryAdd++;
        $(this).attr('data-countDeliveryAdd', countDeliveryAdd);
        $(this).hide();


        resizedDeliveryAdd = 0;


    });
    
    $('.fuel_img_add_btn').click(function(){
        var  id	= $(this).data('id');

        if(countFuelAddObj[id] == null)
        {
            countFuelAddObj[id] = {"count":0};
            var  countFuelAdd	= 0;
        }
        else
        {
            countFuelAddObj[id].count++;
            var countFuelAdd = countFuelAddObj[id].count;
        }


        $('#addFuelImage').append("<fieldset style=\"border:1px solid silver; margin:10px;\"><div class=\"col-md-9\"><input name=\"fuelImageFile"+countFuelAdd+"\" type=\"file\" data-idsuffixfueladd=\""+countFuelAdd+"\"  id=\"fileUploadFuelAdd"+countFuelAdd+"\"><input name=\"fuelimageBase64"+countFuelAdd+"\" type=\"hidden\" id=\"inputAddFuelID"+countFuelAdd+"\"/><img hidden id=\"imgAddFuelID"+countFuelAdd+"\"/><input name=\"fuelimage[]\" type=\"hidden\" value=\""+countFuelAdd+"\"/></div></fieldset>");

        el('fileUploadFuelAdd'+countFuelAdd).addEventListener("change", readImageFuelAdd, false);
        countFuelAdd++;
        $(this).attr('data-countFuelAdd', countFuelAdd);
        $(this).hide();


        resizedFuelAdd = 0;


    });
    
</script>
        
<script>
    
    function el(id) {
      return document.getElementById(id);
    } // Get elem by ID

    var imageObj = el("imgID");
    var idsuffix;
    var idbooking;
    var resized = 0;
    
    var idsuffixDeliveryAdd;
    var resizedDeliveryAdd = 0;
    
    var idsuffixFuelAdd;
    var resizedFuelAdd = 0;
    
    
    function readImageDeliveryAdd() {
      if (this.files && this.files[0]) {
        idsuffixDeliveryAdd = $(this).data('idsuffixdeliveryadd');
        var FR = new FileReader();
        FR.onload = function(e) {
            var imageObjDeliveryAdd = document.getElementById('imgAddDeliveryID'+idsuffixDeliveryAdd);
            imageObjDeliveryAdd.src = e.target.result;  
            imageObjDeliveryAdd.onload = function()
            {
                if(resizedDeliveryAdd == 0)
                {
                    resizedDeliveryAdd = 1;
                    var HERMITE = new Hermite_class();
                    //default resize
                    HERMITE.resize_image('imgAddDeliveryID'+idsuffixDeliveryAdd, 300, 300*imageObjDeliveryAdd.height/imageObjDeliveryAdd.width, 100, true, function(){
                        $('#inputAddDeliveryID'+idsuffixDeliveryAdd).val($('#imgAddDeliveryID'+idsuffixDeliveryAdd).attr('src'));   
                        $('#addDeliveryImageBtn').show();
                    });
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    function readImageFuelAdd() {
      if (this.files && this.files[0]) {
        idsuffixFuelAdd = $(this).data('idsuffixfueladd');
        var FR = new FileReader();
        FR.onload = function(e) {
            var imageObjFuelAdd = document.getElementById('imgAddFuelID'+idsuffixFuelAdd);
            imageObjFuelAdd.src = e.target.result;  
            imageObjFuelAdd.onload = function()
            {
                if(resizedFuelAdd == 0)
                {
                    resizedFuelAdd = 1;
                    
                    var HERMITE = new Hermite_class();
                    //default resize
                    HERMITE.resize_image('imgAddFuelID'+idsuffixFuelAdd, 300, 300*imageObjFuelAdd.height/imageObjFuelAdd.width, 100, true, function(){
                        $('#inputAddFuelID'+idsuffixFuelAdd).val($('#imgAddFuelID'+idsuffixFuelAdd).attr('src'));                        
                        $('#addFuelImageBtn').show();
                    });
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    

</script>
@stop


