@extends('a2b_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Reports Homepage</h2>                   
           
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        
        
        
        <form method="get" action="/a2b/report" class="form-horizontal" name="report-inPage">
		      		
		      	<div>	
                        <div class="inPageForm">
                            <label class="col-md-5">Start Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker1" name="start_date" class="form-control datepicker" id="startDate" placeholder="Start Date"/>
                            </div>

                        </div>
                        <div class="inPageForm">
                            <label class="col-md-5">End Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker2" name="end_date" class="form-control datepicker" id="endDate" placeholder="End Date"/>
                            </div>

                        </div>
                                        
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div align="center" >
                            <input style="width:200px; margin-top:10px" type="submit" class="btn btn-primary" value="Show Data for the Dates" />

                        </div>
                </div>
        </form>
        
        <table class="wk-table wk-table2" id="all-bookings">
			<thead>
				<tr>
					<th>Bike Number</th>
					<th>Total Bookings</th>
					<th>Total Revenue</th>
                    <th>KM done</th>
                    <th>Hours Rented</th>
                    <th>Current Area</th>
				</tr>
			</thead>
			<tbody>
                @foreach($reportData['bikes'] as $bike)
                    <tr>
                        <td>{{ $bike['number'] }}</td>
                        <td>{{ $bike['bookings_count'] }}</td>
                        <td>{{ $bike['total_revenue'] }}</td>
                        <td>{{ $bike['km_done'] }}</td>
                        <td>{{ $bike['hours_rented'] }}</td>
                        <td>{{ $bike['area'] }}</td>
                    </tr>
                @endforeach
			</tbody>
		</table>
        
        
	</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
		  dateFormat:'yy-mm-dd'
        });
        $('#datetimepicker2').datepicker({
		  dateFormat:'yy-mm-dd'
        });
    });
    
    $('#all-bookings').DataTable({});
</script>

	
				
	    	
@stop

