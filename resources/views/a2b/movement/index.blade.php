@extends('admin_layout')

@section('content')
<script>
    document.getElementById("textareaWysihtml").disabled = true;
</script>

	<div class="booking-pg">
		<h2 class="title">Returns</h2>
	
        
        
                <p>&nbsp;</p>
                
        
       
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        <div>&nbsp;</div>&nbsp;
        
          <form class="form-inline" id="form" name="form" method="post" action="/a2b/movement/add">
              
                <div>
                    <label class="col-lg-3">Bike Number:</label>
                    <div class="col-lg-9">
                        <select id="bikeNumbers" name="bike_id" onChange="getFromLocation()">
                            <option disabled selected></option>
                            @foreach($bikeNumbers as $bikeNumber)
                                <option value="{{ $bikeNumber->id }}">{{$bikeNumber->bike_number}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div style = "margin:10px; display:inline;"></div>
                <div>
                    <label class="col-lg-3">From Location:</label>
                    <div class="col-lg-9">
                        <input disabled name="from_location" id="from_location_input">
                    </div>
                </div>
                <div style = "margin:10px; display:inline;"></div>
                <div>
                    <label class="col-md-3">To Location:</label>
                    <div class="col-md-9">
                        <select id="locations" name="to_location">
                            <option disabled selected></option>
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div style = "margin:10px; display:inline;"></div>

                    
                    


            <div><a href="javascript:{}" onclick="document.getElementById('form').submit();" class="btn btn-primary">Create Movement</a></div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
    </div>
        
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#bikeNumbers" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( "#bikeNumbers" ).toggle();
    });
      
    $( "#locations" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( "#locations" ).toggle();
    });
      
  } );
</script>

        
@stop


