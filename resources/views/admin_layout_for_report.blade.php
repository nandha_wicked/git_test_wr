<!DOCTYPE html>
<html>
	<head>
		<meta name="_token" content="{{ Session::token() }}">
		<link rel="stylesheet" href="/css/admin-style.css?a=1" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
		<link rel="stylesheet" href="/css/jquery.dataTables.min.css" />
        	<link rel="stylesheet" href="/css/font-awesome.min.css"/>
		<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<script src="/js/jquery-ui.js"></script>
     	 <link rel="stylesheet" type="text/css" href="/editer/lib/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="/editer/lib/css/prettify.css"/>
		<link rel="stylesheet" type="text/css" href="/editer/src/bootstrap-wysihtml5.css"/>
        
	
        
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="robots" content="index,follow" />

        <meta name="description"
            content="Simple, easy method to visually inspect a JSON-encoded string." />

        <meta name="keywords"
            content="json,visualize,pretty print,developer tool" />

        <link rel="canonical"
            href="http://chris.photobooks.com/json/default.htm" />

        <link rel="stylesheet" type="text/css"
            href="/glibrary/yui_2.8.0r4/build/reset-fonts/reset-fonts.css" />

        <link rel="stylesheet" type="text/css"
            href="/glibrary/yui_2.8.0r4/build/base/base.css" />

        <link rel="stylesheet" type="text/css" href="/default.css" />

        <script type="text/javascript"
            src="/glibrary/yui_2.8.0r4/build/yuiloader-dom-event/yuiloader-dom-event.js"></script>

        <script type="text/javascript"
            src="/glibrary/yui_2.8.0r4/build/json/json-min.js"></script>

        <script type="text/javascript" src="/default-min.js"></script>


        
	</head>
   
    	
	<body class="admin-body">
        
    <nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Wicked Ride</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
                    <li><a href="/logout">Logout<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-sign-out"></span></a></li>
                    
                    @if(Auth::user()->role == 2||Auth::user()->role == 3)
                    
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
					    <ul class="dropdown-menu forAnimate" role="menu">
                        
                            <li><a href="/admin/news">News<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/city">City<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/area"> Area<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikeMake"> Make<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikeModel"> Model<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/price"> Price<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bike"> Bike<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/check-availability-HBH"> Check Occupancy<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/salesReportChart"> Sales Report<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/emailPromoLink"> Email Promotion<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/sliderImage"> Slider Images<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/moveBike"> Move Bike<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                               
                        </ul>
				        </li>
                    
                    @endif
                
                    @if(Auth::user()->role == 2||Auth::user()->role == 3)
                    
                        <li class="treeview">
                            <a href="#"><span>Accessories Master Data</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="/admin/accessories-type">Type<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories-size">Size<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories-color">Color<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories-brand">Brand<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories-image">Images<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories-model">Model<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories">Accessory Item<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/accessories_price">Accessory Pricing<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/move_accessories">Move Accessory<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                
                            </ul>
				        </li>
                    
                    @endif
                    
                    @if(Auth::user()->role == 2||Auth::user()->role == 3)
                    
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bikation <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
					    <ul class="dropdown-menu forAnimate" role="menu">
                        
                            <li><a href="/admin/bikation-vendor">Vendors<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikation-vendor-review">Vendor Reviews<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            
			    <li><a href="/admin/bikation-bookings"> Bookings<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikation-vendor-people_on_trip"> Users<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikation-vendor-trip"> Trips<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                            <li><a href="/admin/bikation-vendor-refund"> Payment Refund<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
							<li><a href="/admin/bikation-vendor-people_on_trip">People On Trip <span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
							<li><a href="/admin/bikation-banner"> Banner <span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
							<li><a href="/admin/bikation-footer"> Footer <span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
							
							
							
                               
                        </ul>
				        </li>
                
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Aggregation <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
                            <ul class="dropdown-menu forAnimate" role="menu">

                                <li><a href="/admin/aggregators">Aggregator List<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/aggregatedBikes">Aggregated Bikes<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/ownerBlock">Owner Block<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/calculatePayoutIndex">Aggregation Calculation<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                

                            </ul>
				        </li>
                    
                    @endif
                
                    @if(Auth::user()->role == 2||Auth::user()->role == 3)
                    
                        <li class="treeview">
                            <a href="#"><span>OneWayRentals Master Data</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="/admin/one-way-rental-location">Location<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/one-way-rental-bike">Bike<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                <li><a href="/admin/one-way-rental-route">Routes<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
                                
                            </ul>
				        </li>
                    
                    @endif
                    
                    @if(Auth::user()->role == 1 || Auth::user()->role == 2||Auth::user()->role == 3)
					<li><a href="/admin/user"> User<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-user"></span></a></li>
                    <li><a href="/admin/users_with_doc"> Verify User Documents<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
					<li><a href="/admin/booking"> Booking<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
					<li><a href="/admin/bike-filter"> Bike Filter<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-filter"></span></a></li>
					<li><a href="/admin/contactEnquiry"> Contact Enquiries<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-phone"></span></a></li>
					<li><a href="/admin/promocode"> Promo Code<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-gift"></span></a></li>
                    <li><a href="/admin/reviews"> Reviews<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
                    <li><a href="/admin/enquiry"> Booking Enquiry<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-user"></span></a></li>
                    <li><a href="/admin/partner"> Partners<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-user"></span></a></li>
                    <li><a href="cancelledEnquiry"> Cancelled Enquiries<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
                    <!--<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
					<ul class="dropdown-menu forAnimate" role="menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li class="divider"></li>
						<li><a href="#">Separated link</a></li>
						<li class="divider"></li>
						<li><a href="#">One more separated link</a></li>
					</ul>
				    </li>-->
                    <li><a href="/admin/addUserToErp"> Add User to Erp<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
                    <li><a href="/admin/serviceBlock"> Service Block<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
				
				    @endif
				
			</ul>
		</div>
	</div>
</nav>
        
		<div class="admin-wrapper">
			<div class="header">
				
				
					@if (isset(Auth::user()->first_name))
		                                       
		            @else 
                        <div class="block2">
                            <a href="/" target="_blank">View Site</a>
		                <a href="#" data-toggle="modal" data-target="#loginModal">LOGIN</a>
                            </div>
		            @endif 							
				
                
            
                
			</div>
            <div class="content">
				@yield('content')
				@yield('model')
			</div>
			
			
		</div>
        <script type="text/javascript" src="/js/moment.js"></script>
		<script type="text/javascript" src="/js/back.js?a=1"></script>
		<script type="text/javascript" src="/js/backend.js?a=1"></script>
	
		<script type="text/javascript" src="/js/packages.js?a=1"></script>
		<script type="text/javascript" src="/js/promocode.js?a=1"></script>
        <script type="text/javascript" src="/js/partner.js?a=1"></script>
        <script type="text/javascript" src="/js/adminvendor.js?a=1"></script>        
		<script type="text/javascript" src="/js/bikationpeopleontrip_admin.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationrefund_admin.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationtrip_admin.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationbookings_admin.js?a=1"></script>
		<link rel="stylesheet" href="/css/spinner.css" />
		<script type="text/javascript" src="/editer/lib/js/wysihtml5-0.3.0.js"></script>
		<script type="text/javascript" src="/editer/lib/js/prettify.js"></script>
		<script type="text/javascript" src="/editer/src/bootstrap-wysihtml5.js"></script>
		<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
  		
		
		<div id="loader" class="backdrop">
			<div class="loader-div">
				<div class="gauge-loader">
				  Loading…
				</div>
			</div>
		</div>
	</body>
</html>
