@inject('bikeObj','App\Models\Bike')
@inject('bookingObj','App\Models\Booking')

@extends('admin_layout')

@section('content')

	

		<h4>The following are the repeat users :</h4>
		<table class="wk-table wk-table3" id="all-users">
           
			<thead>
                
                
				<tr>
                    <th>Customer ID</th>
					<th>Customer Name</th>
                    <th>Customer Email</th>
                    <th>Customer Phone</th>
					<th>Count</th>
					<th>Total Amount</th>
					<th class="widetable">Bike Models</th>
                    <th>Areas Bikes Taken from</th>
                    <th>Dates</th>
                    <th>Boking IDs</th>
					<th>Weekday Hours</th>
                    <th>Weekend Hours</th>
                    <th>Note</th>
                    <th>Edit Note</th>
				</tr>
			</thead>
			<tbody>
                
                @foreach($userList as $user)
					<tr>
                        <td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->mobile_num }}</td>
						<td>{{ $user->counts }}</td>
						<td>{{ $user->total_amount }}</td>
                        <td>
                        <?php
                        $models = $user->models;
                        foreach($models as $model)
                        {
                            echo $model;
                            echo "<br/>";
                        }
                        ?></td>
                        <td>
                        <?php
                        $areas = $user->areas;
                        foreach($areas as $area)
                        {
                            echo $area;
                            echo "<br/>";
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        $datesList = $user->dates;
                        foreach($datesList as $dates)
                        {
                            echo $dates;
                            echo "<br/>";
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        $idsList = $user->ids;
                        foreach($idsList as $ids)
                        {
                            echo $ids;
                            echo "<br/>";
                        }
                        ?>
                        </td>
                        <td>{{ $user->weekend_hours }}</td>
                        <td>{{ $user->weekday_hours }}</td>
                        
                        <td>{{ $user->note }}</td>
						<td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $user->id }}"
							data-note="{{ $user->note }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
                
			</tbody>
		</table>


<script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/admin/repeatUserNote/edit/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>

@stop	


@section('model')
	
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
