@extends('admin_layout')

@section('content')
	
	<div class="city-pg">
		<h2 class="title">City Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addCity-modal"><i class="fa fa-plus"></i> Add City</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>City</th>
					<th>City Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($cityList as $city)
					<tr>
						<td>{{ $city->id }}</td>
						<td>{{ $city->city }}</td>
						<td>
							@if( $city->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn city_edit_btn" data-id="{{ $city->id }}" data-city="{{ $city->city }}" data-status="{{ $city->status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.city_edit_btn').click(function(){
			var id = $(this).data('id');
			var city = $(this).data('city');
			var status = $(this).data('status');
				
			$('#editCity-modal form').attr("action","/admin/city/edit/"+id);
			$('#editCity-modal form input[type=text]').val(city);
			if(status == '1')
				$('#editCity-modal form #cse1').prop("checked",true);	
			else
				$('#editCity-modal form #cse2').prop("checked",true);
			$('#editCity-modal').modal('show');
		});
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addCity-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/city/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD CITY</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">City Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="cityName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">City Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="cityStatus" id="csa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="cityStatus" id="csa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editCity-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT CITY</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">City Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="cityName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">City Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="cityStatus" id="cse1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="cityStatus" id="cse2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	