@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">News Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addNews-modal"><i class="fa fa-plus"></i> Add News</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>News</th>
					<th>Publish Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($newsList as $news)
					<tr>
						<td>{{ $news->news }}</td>
						<td>
							@if( $news->status == 1 )
								Published
							@else
								Unpublished
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn news_edit_btn" data-nid="{{ $news->id }}" data-news="{{ $news->news }}" data-status="{{ $news->status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-danger wk-btn news_delete_btn" data-nid="{{ $news->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.news_edit_btn').click(function(){
			var nid = $(this).data('nid');
			var news = $(this).data('news');
			var status = $(this).data('status');
				
			$('#editNews-modal form').attr("action","/admin/news/edit/"+nid);
			$('#editNews-modal form textarea').val(news);
			if(status == '1')
				$('#editNews-modal form #ps1').prop("checked",true);	
			else
				$('#editNews-modal form #ps2').prop("checked",true);
			$('#editNews-modal').modal('show');
		});

		$('.news_delete_btn').click(function(){
			var nid = $(this).data('nid');
			$('#deleteNews-modal form').attr("action","/admin/news/delete/"+nid);
			$('#deleteNews-modal').modal('show');
		});
	</script>

@stop


@section('model')
	
	<div class="modal fade" id="addNews-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/news/add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Latest News</h4>
		      		</div>
		      		<div class="modal-body">
		        		<textarea class="form-control" name="news" rows="6"></textarea>
		        		News status : 
		        		<label class="radio-inline">
						  <input type="radio" name="publishStatus" id="radio1" value="1"> publish
						</label>
						<label class="radio-inline">
						  <input type="radio" name="publishStatus" id="radio2" value="0"> unpublish
						</label>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editNews-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Latest News</h4>
		      		</div>
		      		<div class="modal-body">
		        		<textarea class="form-control" name="news" rows="6"></textarea>
		        		News status : 
		        		<label class="radio-inline">
						  <input type="radio" name="publishStatus" id="ps1" value="1"> publish
						</label>
						<label class="radio-inline">
						  <input type="radio" name="publishStatus" id="ps2" value="0"> unpublish
						</label>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="deleteNews-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Latest News</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this news?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop