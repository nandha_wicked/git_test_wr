@extends('admin_layout')

@section('content')
	
	<div class="user-pg">
		<h2 class="title">User Homepage</h2>
	<div class="col-xs-5 text-left">
		<button class="btn btn-primary" data-toggle="modal" data-target="#addUser-modal"><i class="fa fa-plus"></i> Add User</button>
		</div>
		<form method="post" action="/admin/user" class="form-horizontal" id="seach-modal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-5 text-right text-left-xs">
				<div class="row">
				<div class="col-xs-12">
				<div class="col-xs-4 text-left" style="padding-left:5px; padding-right:5px;">
				<div class="radio">
				@if($user_sta == 'Approve') 	<?php $chk = 'checked';?> @else	<?php $chk= ''; ?>	@endif		
				@if($user_sta == 'Pending') 	<?php $chk1 = 'checked';?> @else	<?php $chk1= ''; ?>	@endif		
				
				  <label><input type="radio" <?php echo $chk; ?> name="user_status"  value="Approve" >Verified</label>
				</div>
				<div class="radio">
				  <label><input type="radio" <?php echo $chk1; ?> name="user_status" value="Pending" >Unverified</label>
				</div>
					
				</div>
				
				<div class="col-xs-4" style="padding-left:5px; padding-right:5px;">
					<input type="text" name="tx_search" required id="tx_search" value="{{$search_val}}" class="form-control" >
				</div>
				</div>
				
				</div>
				
				
			</div>
			<div class="col-xs-2 text-left text-left-xs" style="padding-left:5px; padding-right:5px;">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i> Select</button>	
			<button type="reset" onClick="reloadfun();" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>	
			</div>
		
		</form>
		
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Sl No.</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Mobile Number</th>
					<!--th>Created At</th-->
					<th>User Document</th>
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php $i=1; ?>
				@foreach($userList as $user)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->mobile_num }}</td>
						<!--td>{{ $user->created_at }}</td-->
						<td>
							@if(empty($user->user_doc_List))
							-- Not Availabe --
						    @else
								<?php $cls = ''; ?>
								<!--@if($user->user_document_status == 'Pending')
									<a href="{{ url('admin/user/Change_doc_status/'.$user->id) }}"><button class="btn btn-warning"><i class="fa fa-close" aria-hidden="true"></i> {{$user->user_document_status}}</button></a>	
								@else	
									<a href="{{ url('admin/user/Change_doc_status/'.$user->id) }}"><button class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> {{$user->user_document_status}}</button></a>	
								@endif	-->
								@foreach($user->user_doc_List as $user_doc)
								@if($user_doc->status == 0)
								<button class="btn btn-danger bikeModel_delete_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button>
								<button class="btn bikeModel_delete" data-docid="{{ $user_doc->id }}"  ><i class="fa fa-close" aria-hidden="true"></i></button><br>
								@else
								<button class="btn btn-success bikeModel_delete_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
								<!--button class="btn btn-success bikeModel_delete" data-docid="{{ $user_doc->id }}"  ><i class="fa fa-close" aria-hidden="true"></i></button-->
								@endif	
								@endforeach
						    @endif
						</td>
						<td>
							<button class="btn btn-info wk-btn user_edit_btn" style="margin-bottom:5px;" data-id="{{ $user->id }}" data-fname="{{ $user->first_name }}" 
							data-lname="{{ $user->last_name }}" data-email="{{ $user->email }}" data-mob="{{ $user->mobile_num }}" data-gender="{{ $user->gender }}" data-work_email="{{ $user->work_email }}" data-location="{{ $user->location }}" data-dob="{{ $user->dob }}" 
							data-password="{{ $user->password }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br>
							
								
						<!--@if(!empty($user->user_doc_List)) --->		
						<!--	@endif -->
						</td>
						
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>	

	<script type="text/javascript">
		$('.user_edit_btn').click(function(){
			var id = $(this).data('id');
			var fname = $(this).data('fname');
			var lname = $(this).data('lname');
			var email = $(this).data('email');
			var mob = $(this).data('mob');
			var gender = $(this).data('gender');
			var work_email = $(this).data('work_email');
			var location = $(this).data('location');
			var dob = $(this).data('dob');
			var password = $(this).data('password');
				
			$('#editUser-modal form').attr("action","/admin/user/edit/"+id);
			$('#editUser-modal form input[name=firstName]').val(fname);
			$('#editUser-modal form input[name=lastName]').val(lname);
			$('#editUser-modal form input[name=userEmail]').val(email);
			$('#editUser-modal form input[name=mobileNumber]').val(mob);
			$('#editUser-modal form input[name=gender]').val(gender);
			$('#editUser-modal form input[name=work_email]').val(work_email);
			$('#editUser-modal form input[name=location]').val(location);
			$('#editUser-modal form input[name=dob]').val(dob);
			//$('#editUser-modal form input[name=userPassword]').val(password);
			//$('#editUser-modal form input[name=confirmPassword]').val(password);
			$('#editUser-modal').modal('show');
		});
	function reloadfun()
	{
		window.location = '/admin/user';
	}
	</script>

	<script type="text/javascript">
	
		$('.bikeModel_delete').click(function(){
		var id = $(this).data('docid');
		$('#specBikeModel-modal-data form').attr("action","/admin/user/document_delete/"+id);
		$('#specBikeModel-modal-data').modal('show');
	});
	
		$('.bikeModel_delete_btn').click(function(){
		var  user_document	= $(this).data('userdocument');
		var  status	= $(this).data('status');
		var  doctype	= $(this).data('doctype');
		var  reason	= $(this).data('reason');
		var  docid	= $(this).data('docid');
		var  userid	= $(this).data('userid');
		
		var  search_user_status	=  $("#seach-modal input[name='user_status']:checked").val();
		var  search_seach_by	= $('#seach-modal select[name=seach_by]').val();
		var  search_txtname	= $('#seach-modal input[name=tx_search]').val();
		
			
		//user_document = user_document.replace("//", "");
		
		//var coverImgHolder = '<img src="http:'+user_document+'" />';
            var coverImgHolder = '<img src="'+user_document+'" height="500" width="95%">';
		$('#old_coverImage_holder').html(coverImgHolder);
		//$('#old_coverImage_holder').html(coverImgHolder);
		$('#user_document_reason').html(reason);
		$('#doc_id').val(docid);
		$('#user_id').val(userid);
		$('#user_doc').val(doctype);
		
		$('#search_user_status').val(search_user_status);
		$('#search_seach_by').val(search_seach_by);
		$('#search_txtname').val(search_txtname);
		if(status == '1')
			$('#bmose1').prop("checked",true);	
		else
			$('#bmose2').prop("checked",true);		
		

		$('#specBikeModel-modal').modal('show');
	});
	</script>

	<script type="text/javascript">
		$(function(){
			$('#addUserSubmit').click(function(event){
				var pass1 = $('#addUser-modal form input[name=userPassword]').val();
				var pass2 = $('#addUser-modal form input[name=confirmPassword]').val();
				if(pass1 != pass2){
					$('#addUser-modal form #password-mismatch-error').text("* Confirm Password Mismatch");
					$('#addUser-modal').modal('show');
					event.preventDefault();
				}
			});
		});
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addUser-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/user/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD USER</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">First Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="firstName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Last Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="lastName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="userEmail" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Mobile Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="mobileNumber" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Password:</label>
							<div class="col-md-9">
		        				<input type="password" name="userPassword" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Confirm Password:</label>
							<div class="col-md-9">
		        				<input type="password" name="confirmPassword" class="form-control" />
		        			</div>
		        		</div>
		        		<div id="password-mismatch-error" class="wk-err-msg"></div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary" id="addUserSubmit"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editUser-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT USER</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">First Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="firstName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Last Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="lastName" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="userEmail" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Mobile Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="mobileNumber" class="form-control" />
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Gender:</label>
							<div class="col-md-9">
		        				<input type="text" name="gender" class="form-control" />
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Work Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="work_email" class="form-control" />
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Location:</label>
							<div class="col-md-9">
		        				<input type="text" name="location" class="form-control" />
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">DoB:</label>
							<div class="col-md-9">
		        				<input type="text" name="dob" class="form-control" />
		        			</div>
		        		</div>
		        		<!-- <div class="form-group">
							<label class="col-md-3">Password:</label>
							<div class="col-md-9">
		        				<input type="password" name="userPassword" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Confirm Password:</label>
							<div class="col-md-9">
		        				<input type="password" name="confirmPassword" class="form-control" />
		        			</div>
		        		</div> -->
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/users_with_doc/update" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View Document</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="col-md-12">
						<div class="form-group">
		        			<div class="row">
									<div class="col-md-12">
									<div class="col-md-1"></div>
		        				
				        			<div id="old_coverImage_holder"></div>
				        			</div>
		        			</div>
		        		</div>
						
						<div class="form-group">
							<div class="col-md-12">
		        			<div class="row">
		        			Reason: 
							<textarea type="text" name="user_document_reason" id="user_document_reason" rows="3" style="width:40%" /></textarea>
							
		        			</div>
							</div>
		        		</div>
						<div class="form-group">
		        			<div class="row">
							<div class="col-md-12">
		        			Status: 
							<label class="radio-inline">
								  <input type="radio" name="status" id="bmose1" value="1"> Approve
								</label>
								<label class="radio-inline">
								  <input type="radio" name="status" id="bmose2" value="0"> Not Approve
								</label>
								<label id="requestorStatus">
        						</label>
								</div>
		        			</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="doc_id" id="doc_id" value="">
						<input type="hidden" name="user_id" id="user_id" value="">
						<input type="hidden" name="user_doc" id="user_doc" value="">
						
						<input type="hidden" name="search_user_status" id="search_user_status" value="">
						<input type="hidden" name="search_seach_by" id="search_seach_by" value="">
						<input type="hidden" name="search_txtname" id="search_txtname" value="">
						</div>
		      		</div>
		      		<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
	
	<div class="modal fade" id="specBikeModel-modal-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete this File</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
@stop	
