@extends('admin_layout')

@section('content')

    
    <form class="form-inline" method="get" action="/admin/opsReportExport">
            
			<div class="form-group">
		    	<input type="text" name="start_date" class="form-control" id="startDateFilter" <?php if($start_date!="") echo "value = ".$start_date; else echo "placeholder=\"Start Date\""; ?> autocomplete="off">
		  	</div>
		  	
		  	<div class="form-group">
		    	<input type="text" name="end_date" class="form-control" id="endDateFilter" <?php if($end_date!="") echo "value = ".$end_date; else echo "placeholder=\"End Date\""; ?> autocomplete="off">
		  	</div>
		  	
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <button type="submit" class="btn btn-success">Filter</button>
    </form>


<script type="text/javascript">
    today = new Date();
    yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
	$('#startDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
            maxDate:yesterday,
            minDate:new Date('2016-10-01'),
            onSelectDate: function(selectedDateTime, $el){
                document.getElementById("endDateFilter").value = null;
                var minDate = selectedDateTime.dateFormat('Y/m/d');
                $('#endDateFilter').datetimepicker({
                        timepicker: false,
                        scrollInput: false,
                        format:'Y-m-d',
                        minDate:minDate,
                        maxDate:yesterday

                }).datetimepicker('show').focus();
            }
    });
    
</script>

@stop

