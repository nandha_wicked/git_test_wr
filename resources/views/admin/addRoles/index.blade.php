@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">ERP Users Homepage</h2>
		 <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/admin/searchUser">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Email: </label>
		  		<input type="text" name="searchStr" class="form-control"/>
		  	</div>
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Search</button>
		  	</div>
		</form>

		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        
        @if(!isset($userResult))
            @if(isset($searchStr))
                <label class="" for="exampleInputPassword3">No Users found for the email address - {{$searchStr}}</label>
            @endif
        @else
            <table class="wk-table">
                <thead>
                    <tr>

                        <th>Email</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $userResult['email'] }}</td>
                        <td>
                            @if($userResult['isAdmin'])
                            <button class="btn btn-danger wk-btn erpUser_delete_btn" data-id="{{ $userResult->id }}"><i class="fa fa-trash-o"></i> Delete </button>
                            @else
                            <form method="post" action="/admin/addUserToErp/add" class="form-horizontal" style="margin: auto;">
                                <input type="hidden" name="user" value="{{$userResult->id}}" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="text" name="user_role" class="form-control" value="{{$userResult->role_string}}"/>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add To Admin</button>
                                
                            </form>
                            @endif
                        </td>
                    </tr>
                   
                </tbody>
            </table>
        @endif
        
        @if(isset($erpUsers))
            <table class="wk-table">
                <thead>
                    <tr>

                        <th>Email</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($erpUsers as $erpUser)
                        <tr>

                            <td>{{ $erpUser->email }}</td>


                            <td>
                                <button class="btn btn-danger wk-btn erpUser_delete_btn" data-id="{{ $erpUser->id }}"><i class="fa fa-trash-o"></i> Delete </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
        
	</div>

<script type="text/javascript">
		$('.erpUser_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteErpUser-modal form').attr("action","/admin/addUserToErp/delete/"+id);
			$('#deleteErpUser-modal').modal('show');
		});
	</script>

	<script type="text/javascript">
		$(function(){
			erpUser.init();
	    });
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="deleteErpUser-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Erp User</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Erp User?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	



@stop
