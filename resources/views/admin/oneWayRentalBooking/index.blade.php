@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>
		
        
        
             <p>&nbsp;</p>
     
                    <form id="custom-search-form" method="get" action="/admin/one-way-rental-booking/search" class="form-search form-horizontal pull-left">
                        <div class="input-append span12">
                            <input type="text" name = "id" class="search-query mac-style" placeholder="Search">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                    </form>
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:2240px">
			<thead>
				<tr>
					<th style="width: 80px !important;">Id</th>
					<th style="width: 100px !important;">Status</th>
					<th style="width: 140px !important;">Customer</th>
					<th style="width: 110px !important;">Model</th>
                    <th style="width: 110px !important;">Bike Number</th>
					<th style="width: 100px !important;">From Location</th>
					<th style="width: 100px !important;">To Location</th>
					<th style="width: 300px !important;">Customer Instruction</th>
					<th style="width: 80px !important;">Total Price</th>
                    <th style="width: 80px !important;">Amount Paid at Payment Gateway</th>
                    <th style="width: 80px !important;">Payment ID</th>
                    <th style="width: 80px !important;">Promocode</th>
                    <th style="width: 100px !important;">Created At</th>
                	<th style="width: 180px !important;">Action</th>
                    <th style="width: 80px !important;">Add Notes</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['reference_id'] }}</td>
						<td>{{ $booking->status }}</td>
                        <?php $user = $booking->getUser(); ?>
						<td>{{ $user->first_name  }} {{ $user->last_name }} - {{ $user->email }}</td>
						<td>{{ $booking->getBikeMakeName() }} - {{  $booking->getBikeModelName() }}</td>
                        <td>{{ $booking->getBikeNumber() }}</td>
						<td>{{ $booking->getFromLocation() }}</td>
						<td>{{ $booking->getToLocation() }}</td>
						<td>{{ $booking->customer_instructions }}</td>
                        <td>{{ $booking->price }}</td>
                        @if(($booking->price > $booking->amount_paid_payment_gateway))
                            <td>{{ $booking->amount_paid_payment_gateway }} (Wallet payment - {{$booking->wallet_amount_applied }})</td>
                        @else
                            <td>{{ $booking->amount_paid_payment_gateway }}</td>
                        @endif
                        <td>{{ $booking->payment_id }}</td>
                        <td>{{ $booking->promocode }}</td>
                        <td>{{ $booking->getReadableDateTimeWithMinutes($booking['created_at']) }}</td>
                        @if($booking->status=="Bike returned"||$booking->status=="Booking Cancelled")
                            <td>
                            </td>
                        @else
                            <td>
                                <button class="btn btn-danger wk-btn booking_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
                            </td>
                        @endif
                        <td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $booking->id }}"
							data-note="{{ $booking->customer_instructions }}"
							><i class="fa fa-pencil-square-o"></i> Add</button>
						</td>            
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.booking_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBooking-modal form').attr("action","/admin/one-way-rental-booking/delete/"+id);
			$('#deleteBooking-modal').modal('show');
		});
        
	</script>

    
    
    <script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/admin/updateNotes/on_demand_a2b_booking/customer_instructions/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>
    

@stop

@section('model')
	
	


	<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_delete_form" id="booking_delete_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		Are you sure, you want to delete this booking?<br>
		        		If "Yes" press "Delete" else "Close". Also select reason for deleting.
                        
                        <select name="reason" class="form-control" id="booking_delete_form_select">
                            <option value="not_selected" disabled selected>-- Select Reason --</option>
                            <option value="error">Error in Booking</option>
                            <option value="user_cancelled">User Cancelled</option>
                            <option value="wicked_ride_cancelled">Wicked Ride Cancelled</option>
                        </select>
                        <p id="booking_delete_form_error" style="color:red;"></p>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_delete_form_btn"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


    
		
    <script type="text/javascript">
     $('.booking_delete_form_btn').click(function(){

                e = document.getElementById("booking_delete_form_select");
                var reason = e.options[e.selectedIndex].text;
            
                if(reason == "-- Select Reason --")
                {
                    $("#booking_delete_form_error").html("Select a reason");        
                }
                else
                {
                    $('#booking_delete_form').submit();
                    $('#cancel-delivery').submit();
                }

        });


    </script>

    
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


    


@stop	
