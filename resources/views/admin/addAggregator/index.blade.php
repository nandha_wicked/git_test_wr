@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Aggregators Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addAggregatorModal"><i class="fa fa-plus"></i> Add Aggregator</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
			<thead>
				<tr>
					
					<th style="width: 100px !important;">Name</th>
                    <th style="width: 100px !important;">Phone Number</th>
                    <th style="width: 110px !important;">Bank Account</th>
                    <th style="width: 80px !important;">IFSC code</th>
                    <th style="width: 200px !important;">Bank Account Notes</th>
                    <th style="width: 200px !important;">Address</th>
                    <th style="width: 80px !important;">PAN</th>
                    <th style="width: 140px !important;">Email</th>
                    <th style="width: 100px !important;">Edit</th>
                    <th style="width: 100px !important;">Delete</th>
					
				</tr>
			</thead>
			<tbody>
				@foreach($aggregatorList as $aggregator)
					<tr>
						
                        <td>{{$aggregator->name}}</td>
                        <td>{{$aggregator->phone_number}}</td>
                        <td>{{$aggregator->bank_account_number}}</td>
                        <td>{{$aggregator->ifsc_code}}</td>
                        <td><pre>{{$aggregator->bank_account_notes}}</pre></td>
                        <td><pre>{{ $aggregator->address }}</pre></td>
                        <td>{{$aggregator->pan}}</td>
                        <td>{{$aggregator->getEmail()}}</td>
						
						
						<td>
							<button class="btn btn-primary wk-btn aggregator_edit_btn" data-id="{{ $aggregator->id}}" data-name="{{ $aggregator->name}}" data-phone_number="{{ $aggregator->phone_number}}" data-bank_account_number="{{ $aggregator->bank_account_number}}" data-ifsc_code="{{ $aggregator->ifsc_code}}" data-bank_account_notes="{{ $aggregator->bank_account_notes}}" data-address="{{ $aggregator->address}}" data-pan="{{ $aggregator->pan}}" data-email="{{ $aggregator->getEmail()}}" ><i class="fa fa-pencil"></i> Edit </button>
						</td>
                        <td>
							<button class="btn btn-danger wk-btn aggregator_delete_btn" data-id="{{ $aggregator->id }}"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.aggregator_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteAggregator-modal form').attr("action","/admin/aggregators/delete/"+id);
			$('#deleteAggregator-modal').modal('show');
		});
    
        $('.aggregator_edit_btn').click(function(){
			var id = $(this).data('id');
            var name = $(this).data('name');
            var phone_number = $(this).data('phone_number');
            var bank_account_number = $(this).data('bank_account_number');
            var ifsc_code = $(this).data('ifsc_code');
            var bank_account_notes = $(this).data('bank_account_notes');
            var address = $(this).data('address');
            var pan = $(this).data('pan');
            var email = $(this).data('email');
            
            var formEl = $('form[name="edit_aggregator_form"]');


            formEl.find('input[name="id"]').val(id);
            formEl.find('input[name="name"]').val(name);
            formEl.find('input[name="phone_number"]').val(phone_number);
            formEl.find('input[name="bank_account_number"]').val(bank_account_number);
            formEl.find('input[name="ifsc_code"]').val(ifsc_code);
            formEl.find('textarea[name="bank_account_notes"]').val(bank_account_notes);
            formEl.find('textarea[name="address"]').val(address);
            formEl.find('input[name="pan"]').val(pan);
            formEl.find('input[name="email"]').val(email);
            
			$('#editAggregatorModal form').attr("action","/admin/aggregators/edit/"+id);
            
			$('#editAggregatorModal').modal('show');
		});
    
    
</script>

	

@stop

@section('model')
	
	<div class="modal fade" id="deleteAggregator-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Aggregator</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Aggregator?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addAggregatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/aggregators/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Aggregator</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						
		        		
		        		<div class="form-group">
							<label class="col-md-3">Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Phone Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="phone_number" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="email" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bank Account Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="bank_account_number" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">IFSC Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="ifsc_code" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bank Account Notes:</label>
							<div class="col-md-9">
		        				<textarea name="bank_account_notes" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Address:</label>
							<div class="col-md-9">
		        				<textarea name="address" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">PAN:</label>
							<div class="col-md-9">
		        				<input type="text" name="pan" class="form-control" />
		        			</div>
		        		</div>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

    <div class="modal fade" id="editAggregatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="edit_aggregator_form" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Aggregator</h4>
		      		</div>
		      		<div class="modal-body">
		      			
		        		
		        		<div class="form-group">
							<label class="col-md-3">Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Phone Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="phone_number" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="email" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bank Account Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="bank_account_number" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">IFSC Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="ifsc_code" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bank Account Notes:</label>
							<div class="col-md-9">
		        				<textarea name="bank_account_notes" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Address:</label>
							<div class="col-md-9">
		        				<textarea name="address" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">PAN:</label>
							<div class="col-md-9">
		        				<input type="text" name="pan" class="form-control" />
		        			</div>
		        		</div>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



@stop
