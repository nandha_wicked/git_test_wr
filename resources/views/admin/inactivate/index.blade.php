@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">Inactivate Bikes of the following Area - Model combination</h2>
        
        
        
		
				<form method="post" action="/admin/inactivateBikes">
		      		
		        		
		        		
		      		<div class="inPageForm">
		        		<label class="col-md-3">Bike Model :</label>
                        <div class="col-md-9">
                            <select name="model_id" class="form-control">
                                @foreach($models as $model)
                                    <option value="{{ $model->id }}">{{ $model->bike_model }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="inPageForm">   
                        <label class="col-md-3">Area :</label>
                        <div class="col-md-9">
                            <select name="area_id" class="form-control">
                                @foreach($areas as $area)
                                    <option value="{{ $area->id }}">{{ $area->area }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		
		      		<div align="center">
		      			<input style="width:200px" type="submit" class="btn btn-primary" value="Inactivate" />
		        		
		      		</div>
		    	</form>
	    	</div>
	  	



@stop


