@extends('admin_layout_for_report')

@section('content')

	
	<form class="form-inline" method="get" action="/admin/salesReport">
            
			<div class="form-group">
		    	<input type="text" name="start_date" class="form-control" id="startDateFilter" <?php if($start_date!="") echo "value = ".$start_date; else echo "placeholder=\"Start Date\""; ?> >
		  	</div>
		  	
		  	<div class="form-group">
		    	<input type="text" name="end_date" class="form-control" id="endDateFilter" <?php if($end_date!="") echo "value = ".$end_date; else echo "placeholder=\"End Date\""; ?> autocomplete="off">
		  	</div>
		  	
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <button type="submit" class="btn btn-success">Filter</button>
    </form>
<div style = "font-size:15px; font-weight: 700;">Type Start and End Dates in yyyy-mm-dd Format</div>


	<menu style="
		margin: 0px 0px 1em 0px;
		padding: 0px;
        height: 40px;
		width: 12em;">

		<h3 style="margin-top: 0px; display:none;">Input</h3>

		<p style="display:none;"  >
			<label>
				<input type="radio" name="jsonParser"
					id="jsonStrict" value="1" checked="checked" /> Strict JSON
			</label><br />

			<label>
				<input type="radio" name="jsonParser"
					id="jsonEval" value="0" /> Eval
			</label>
		</p>

		<h3 style="display:none;"  >Output</h3>

		<p id="jsonOutputSet" style="display:none;"  >
			<label>
				<input type="radio" name="jsonOutStyle"
					id="json2HTML" value="1" checked="checked" /> HTML
			</label><br />

			<label>
				<input type="radio" name="jsonOutStyle"
					id="json2JSON" value="0" /> JSON
			</label>

		</p>

		<h3 style="display:none;"  >Options:</h3>

		<p id="jsonOptionSet" class="HTML" style="display:none;"  >

			<label id="jsonTrunc_label" style="display:none;"  >
				<input type="checkbox" id="jsonTrunc"
					value="1" checked="checked" /> Truncate long strings
			</label><br />

			<label id="jsonDate_label" style="display:none;"  >
				<input type="checkbox" id="jsonDate"
					value="1" checked="checked" /> Detect encoded dates
			</label><br />

			<label id="jsonData_label" style="display:none;"  >
				<input type="checkbox" id="jsonData"
					value="1" checked="checked" /> Detect data structures
			</label><br />

			<label id="jsonSpace_label" style="display:none;"  >
				<input type="checkbox" id="jsonSpace" value="1" />
				Preserve whitespace
			</label>
		</p>

		<h3 style="display:none;">Actions:</h3>

		<p>
			<button id="cmdRender" style="display:none;">Render</button><br />
              
			<span id="loadCommands" style="display:none;"  >
				<a id="cmdLoad">Load</a>
				<span id="reloadCommand">
					/ <a id="cmdReload">Reload</a>
				</span>
			</span>
			<span id="loadMessage" style="display: none;">
				Loading...
			</span><br />
			<a id="cmdValidate" style="display:none;"  >Validate</a><br />
			<a id="cmdClear" style="display:none;"  >Clear</a><br />
			<a id="cmdEncode" style="display:none;"  >Re-encode</a><br />
			<a id="cmdRemoveCRLF" style="display:none;"  >Remove Line Breaks</a><br />
			<a id="cmdDecodeURI" style="display:none;"  >Decode URI</a><br />
			<a id="cmdTrim2JSON" style="display:none;"  >Trim non-JSON</a><br />
			<span id="htmlCommands" style="display:none;"  >
				<a id="cmdCollapse">Collapse</a> /
				<a id="cmdExpand">Expand</a> All<br />
			</span>
			<a id="cmdHelp" href="help.htm" target="help" style="display:none;"  >Help</a><br />
			<a id="cmdBeer" href="beer.htm" target="beer" style="display:none;"  >Beer Fund</a>
		</p>

		<code id="jsonLocation" style="display:none;"></code>
        
        

	</menu>

    <div style="padding-left: 0px;">
		<h2 style="margin-top: 0px; font-size: 108%;">
			<label for="jsonInput" style="display:none;">Input:</label>
			<small id="jsonSize" style="display:none;"></small>
		</h2>

		<textarea id="jsonInput"
			rows="10" cols="50"
			placeholder="Paste your JSON here"
			spellcheck="false"
			autofocus="autofocus" style="display:none;">{{ json_encode($reportData) }}</textarea>
    

		

		<h2 style="font-size: 108%; display:none;">Output:</h2>
		<output
			style="display: block;"
			for="jsonInput cmdValidate"
			id="jsonValidateOutput"></output>
		<output
			style="display: block;"
			for="jsonInput jsonStrict jsonEval json2HTML json2JSON jsonTrunc jsonDate jsonData jsonSpace"
			id="jsonOutput"></output>
	</div>

<script>
    window.onload = function() {
        
            document.getElementById("cmdRender").click();

        
        };
             $(function() {
                $('#start_date').datetimepicker({
                    timepicker: false,
                    scrollInput: false,
                    format:'Y-m-d'
                });
             });
           
        
            $(function() {
                $('#endDateFilter').datetimepicker({
                    timepicker: false,
                    scrollInput: false,
                    format:'Y-m-d'
                });
            });
    
</script>





@stop
