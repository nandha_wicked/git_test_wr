@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Packages Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Description</th>
					<th>Destination</th>
					<th>Date_time</th>
					<th>Price</th>
					<th>Cover Image</th>
					<th>Available seat</th>
					<th>Total Seats</th>
					<th>Ride Iinclude</th>
					<th>Ride Exclude</th>
					<th>Ride Rule</th>
					<th>Meeting Point</th>
					<th>Recommanded Bike</th>
					<th>Total Km</th>
					<th>Total Days</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($packagesModelList as $bikeModel)
					<tr>
						<td>{{ $bikeModel->id }}</td>
						<td>{{ $bikeModel->name }}</td>
						<td><div class="model-description">{{ $bikeModel->desc }}</div></td>
						<td>{{ $bikeModel->destination }}</td>
						<td>{{ $bikeModel->date_time }}</td>
						<td>{{ $bikeModel->price }}</td>
						<td><img src="{{ $bikeModel['cover_img'] }}" width ="50px" /></td>
						<td>{{ $bikeModel->available_seat }}</td>
						<td>{{ $bikeModel->total_seats }}</td>
						<td><div class="model-description">{{ $bikeModel->ride_include }}</div></td>
						<td><div class="model-description">{{ $bikeModel->ride_exclude }}</div></td>
						<td><div class="model-description">{{ $bikeModel->ride_rule }}</div></td>
						<td><div class="model-description">{{ $bikeModel->meeting_point }}</div></td>
						<td><div class="model-description">{{ $bikeModel->recommanded_bike }}</div></td>
						<td>{{ $bikeModel->total_km }}</td>
						<td>{{ $bikeModel->total_days }}</td>
						<td>
							@if( $bikeModel->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn" data-cover-img="{{ $bikeModel['cover_img'] }}" data-id="{{ $bikeModel->id }}" data-name="{{ $bikeModel->name }}" data-desc="{{ $bikeModel->desc }}" data-destination="{{ $bikeModel->destination }}" data-datetime="{{ $bikeModel->date_time }}" data-price="{{ $bikeModel->price }}" data-availableseat="{{ $bikeModel->available_seat }}" data-totalseats="{{ $bikeModel->total_seats }}" data-rideinclude="{{ $bikeModel->ride_include }}" data-rideexclude="{{ $bikeModel->ride_exclude }}" data-riderule="{{ $bikeModel->ride_rule }}" data-meetingpoint="{{ $bikeModel->meeting_point }}" data-recommandedbike="{{ $bikeModel->recommanded_bike }}" data-totalkm="{{ $bikeModel->total_km }}" data-totaldays="{{ $bikeModel->total_days }}" data-status="{{ $bikeModel->status }}" ><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikeModel->id }}"><i class="fa fa-pencil-square-o"></i> DELETE</button>

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Admin.PackagesModel.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<form method="post" action="/admin/packages/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="desc"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Destination :</label>
							<div class="col-md-9">
		        				<input type="text" name="destination" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Date</label>
							<div class="col-md-9">
		        				<input type="text" name="date_time" id="date_time" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Price</label>
							<div class="col-md-9">
		        				<input type="text" name="price" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Add Image:</label>
							<div class="col-md-3">
		        				<input type="file" name="coverImgUpload"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Available Seat</label>
							<div class="col-md-9">
		        				<input type="text" name="available_seat" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Seat</label>
							<div class="col-md-9">
		        				<input type="text" name="total_seats" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Include</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_include"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Exclude</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_exclude"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Rule</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_rule"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Meeting Point</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="meeting_point"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Recommanded Bike</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="recommanded_bike"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Km</label>
							<div class="col-md-9">
		        				<input type="text" name="total_km" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Total Days</label>
							<div class="col-md-9">
		        				<input type="text" name="total_days" class="form-control"/>
		        			</div>
		        		</div>
		        				        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="packagesStatus" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="packagesStatus" id="bmose2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
		        				<textarea class="form-control" rows="3" name="desc"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Destination :</label>
							<div class="col-md-9">
		        				<input type="text" name="destination" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Date</label>
							<div class="col-md-9">
		        				<input type="text" name="date_time" id="date_time" class="form-control date_time" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Price</label>
							<div class="col-md-9">
		        				<input type="text" name="price" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverImgUpload" />
				        			</div>
		        				</div>
		        				<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			<label class="col-md-6">Old Image :</label>
									<div class="col-md-6">
				        				<div id="old_coverImage_holder"></div>
				        			</div>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Available Seat</label>
							<div class="col-md-9">
		        				<input type="text" name="available_seat" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Seat</label>
							<div class="col-md-9">
		        				<input type="text" name="total_seats" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Include</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_include"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Exclude</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_exclude"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ride Rule</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="ride_rule"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Meeting Point</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="meeting_point"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Recommanded Bike</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="recommanded_bike"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Km</label>
							<div class="col-md-9">
		        				<input type="text" name="total_km" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Total Days</label>
							<div class="col-md-9">
		        				<input type="text" name="total_days" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="packagesStatus" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="packagesStatus" id="bmose2" value="0"> Inactive
								</label>
							</div>
						</div>

						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="view-gallery-modal" tabindex="-1" role="dialog" aria-labelledby="galleryModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Gallery Images</h4>
				</div>
				<div class="modal-body">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgdmlld0JveD0iMCAwIDkwMCA1MDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzkwMHg1MDAvYXV0by8jNzc3OiM1NTUvdGV4dDpGaXJzdCBzbGlkZQpDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48IVtDREFUQVsjaG9sZGVyXzE1MGIyYWEyNmI4IHRleHQgeyBmaWxsOiM1NTU7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6NDVwdCB9IF1dPjwvc3R5bGU+PC9kZWZzPjxnIGlkPSJob2xkZXJfMTUwYjJhYTI2YjgiPjxyZWN0IHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBmaWxsPSIjNzc3Ii8+PGc+PHRleHQgeD0iMzA4LjI4OTA2MjUiIHk9IjI3MC4xIj5GaXJzdCBzbGlkZTwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" alt="...">
						    </div>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <!-- <i class="fa fa-chevron-left"></i> -->
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						    <!-- <i class="fa fa-chevron-right"></i> -->
						  </a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<input type="hidden" name="id" value="">
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#view-gallery-modal').on('show.bs.modal', function(e) {
		    var modelId = $(e.relatedTarget).data('model-id');
		    $(e.currentTarget).find('input[name="id"]').val(modelId);
		});
	</script>	

@stop	