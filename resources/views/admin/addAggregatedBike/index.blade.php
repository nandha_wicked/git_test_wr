@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Aggregated Bikes Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addAggregatedBikeModal"><i class="fa fa-plus"></i> Add Aggregated Bike</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
			<thead>
				<tr>
					<th style="width: 100px !important;">ID</th>
                    <th style="width: 100px !important;">Aggregator</th>
                    <th style="width: 110px !important;">Model</th>
                    <th style="width: 110px !important;">Bike Number</th>
                    <th style="width: 140px !important;">Date of Addition</th>
                    <th style="width: 250px !important;">Cities</th>
                    <th style="width: 80px !important;">Commission</th>
                    <th style="width: 100px !important;">Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($aggregatedBikeList as $aggregatedBike)
					<tr>
						
                        <td>{{$aggregatedBike->id}}</td>
                        <td>{{$aggregatedBike->getAggregatorName()}}</td>
                        <td>{{$aggregatedBike->getModelName()}}</td>
                        <td>{{ $aggregatedBike->getBikeNumber() }}</td>
                        <td>{{$aggregatedBike->date_of_addition}}</td>
                        <td>{{$aggregatedBike->getCityNames()}}</td>
                        <td>{{$aggregatedBike->commission_perc}}%</td>
                        <td>
							<button class="btn btn-danger wk-btn aggregatedBike_delete_btn" data-id="{{ $aggregatedBike->id }}"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.aggregatedBike_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteAggregatedBike-modal form').attr("action","/admin/aggregatedBikes/delete/"+id);
			$('#deleteAggregatedBike-modal').modal('show');
		});
    
</script>
<script type="text/javascript">
    $(function() {
		$('#startDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d'
        });
    });
    
    
</script>
	

@stop

@section('model')
	
	<div class="modal fade" id="deleteAggregatedBike-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Aggregated Bike</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Aggregated Bike?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addAggregatedBikeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/aggregatedBikes/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Aggregated Bike</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						
		        		
		        		<div class="form-group">
							<label class="col-md-3">Aggregator:</label>
							<div class="col-md-9">
		        				<select name="aggregator_id" id="aggregator_id" class="form-control">
                                    <option disabled selected> --Select-- </option>
		        					@foreach($aggregatorList as $aggregator)
		        						<option value="{{ $aggregator->id }}">{{ $aggregator->name }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bike Model:</label>
							<div class="col-md-9">
		        				<select name="model_id" id="model_id" class="form-control">
                                    <option disabled selected> --Select-- </option>
		        					@foreach($modelList as $model)
		        						<option value="{{ $model->id }}">{{ $model->bike_model }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Bike Number:</label>
							<div class="col-md-9">
		        				<select name="bike_number_id" id="bike_number_id" class="form-control">
                                    <option disabled selected> --Select-- </option>
		        					@foreach($bikeNumberList as $bikeNumber)
		        						<option value="{{ $bikeNumber->id }}">{{ $bikeNumber->number }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Date of Addition:</label>
							<div class="col-md-9">
		        				<input type="text" name="date_of_addition" class="form-control" id="startDate" placeholder="Start Date"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Cities:</label>
							<div class="col-md-9">
		        				
				        			@foreach ($cityList as $city)
                                    <li>
                                        <input type="checkbox" name="ch{{$city->id}}" value="{{$city->id}}"/>&nbsp;{{ $city->city }}
								    </li>
				        			@endforeach
				        		
				        	</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Commission:</label>
							<div class="col-md-9">
		        				<input type="number" name="commission_perc" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
   



@stop
