@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBooking-modal"><i class="fa fa-plus"></i> Add Booking</button>
        
        
             <p>&nbsp;</p>

     
                    <form id="custom-search-form" method="get" action="/admin/booking/search" class="form-search form-horizontal pull-left">
                        <div class="input-append span12">
                            <input type="text" name = "id" class="search-query mac-style" placeholder="Search">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                    </form>
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:2240px">
			<thead>
				<tr>
					<th style="width: 50px !important;">Id</th>
					<th style="width: 100px !important;">Start Date</th>
					<th style="width: 100px !important;">End Date</th>
					<th style="width: 100px !important;">Created At</th>
					<th style="width: 140px !important;">Customer</th>
					<th style="width: 110px !important;">Bike</th>
					<th style="width: 100px !important;">Area</th>
					<th style="width: 300px !important;">Customer Instruction</th>
					<th style="width: 80px !important;">Total Price</th>
                    <th style="width: 100px !important;">CreatedBy</th>
                    <th style="width: 80px !important;">Payment ID</th>
                    <th style="width: 80px !important;">Promocode</th>
                	<th style="width: 180px !important;">Action</th>
                    <th style="width: 300px !important;">Notes for Operations</th>
                    <th style="width: 80px !important;">Add Notes</th>
                    <th style="width: 100px !important;">Notes Acknowledged By</th>
                    <th style="width: 100px !important;">Notes Acknowledged At</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['reference_id'] }}</td>
						<td>{{ $booking->getReadableDateTime($booking['start_datetime']) }}</td>
						<td>{{ $booking->getReadableDateTime($booking['end_datetime']) }}</td>
						<td>{{ $booking->getReadableDateTimeWithMinutes($booking['created_at']) }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserEmail($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->revenueAmount() }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
                        <td>{{ $booking->pg_txn_id }}</td>
                        <td>{{ $booking->promocode }}</td>
						<td>
                            <button class="btn btn-info wk-btn booking_edit_btn" data-id="{{ $booking->id }}"><i class="fa fa-pencil"></i> Edit</button>
							<button class="btn btn-danger wk-btn booking_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
                        <td>{{$booking->backend_notes}}</td>
                        <td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $booking->id }}"
							data-note="{{ $booking->backend_notes }}"
							><i class="fa fa-pencil-square-o"></i> Add</button>
						</td>
                        
                        <td>{{$booking->getBackEndNotesAckByEmail()}}</td>
                        <td>{{$booking->getBackEndNotesAckAt()}}</td>
                        
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.booking_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBooking-modal form').attr("action","/admin/booking/delete/"+id);
			$('#deleteBooking-modal').modal('show');
		});
        
	</script>

    <script type="text/javascript">
		$('.booking_edit_btn').click(function(){
			var id = $(this).data('id');
			$('#editBooking-modal form').attr("action","/admin/booking/edit/"+id);
			$('#editBooking-modal').modal('show');
		});
	</script>
    
    <script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/admin/updateNotes/bookings/backend_notes/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>
    <script type="text/javascript">
		$(document).ready(function(){
            $("select#customer").change(function(){
			var formEl = $('form[name="add-booking"]');
            var customer = formEl.find('select[name="customer"]').val();
            if(customer == <?php echo env('RESERVATIONSID');?>)
            {
                $('#addBooking-modal form div[name=customer_email_div]').attr("style","");
                $('#addBooking-modal form div[name=customer_fname_div]').attr("style","");
                $('#addBooking-modal form div[name=customer_lname_div]').attr("style","");
                $('#addBooking-modal form div[name=customer_mobile_div]').attr("style","");
                
            }
            else
            {
                $('#addBooking-modal form div[name=customer_email_div]').attr("style","display:none;");
                $('#addBooking-modal form div[name=customer_fname_div]').attr("style","display:none;");
                $('#addBooking-modal form div[name=customer_lname_div]').attr("style","display:none;");
                $('#addBooking-modal form div[name=customer_mobile_div]').attr("style","display:none;");
            }

			

		  });
        });
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/booking/add" onsubmit="Backend.Booking.validateBookingForm();" class="form-horizontal" name="add-booking" id="add-booking">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD Booking</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Start Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="startDate" class="form-control" id="startDate" placeholder="Start Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="startTime" class="form-control" id="startTime" placeholder="Start Time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">End Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="endDate" class="form-control" id="endDate" placeholder="End Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="endTime" class="form-control" id="endTime" placeholder="End Time"/>
		        			</div>
		        		</div>
		        		<div id="date-error"></div>
		        		<div class="form-group">
							<label class="col-md-3">Customer:</label>
							<div class="col-md-9">
		        				<select name="customer" id ="customer" class="form-control">
		        					@foreach($userList as $user)
		        						<option value="{{ $user->id }}">{{ $user->first_name }} - {{ $user->email }}</option>
		        					@endforeach
		        				</select>
		        			</div>
                        </div>
                        <div class="form-group" name="customer_email_div"  style="display:none;">
							<label class="col-md-3">Customer Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_email" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group" name="customer_mobile_div"  style="display:none;">
							<label class="col-md-3">Customer Mobile:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_mobile" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group" name="customer_fname_div"  style="display:none;">
							<label class="col-md-3">Customer First Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_fname" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group" name="customer_lname_div"  style="display:none;">
							<label class="col-md-3">Customer Last Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_lname" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Model & Area:</label>
							<div class="col-md-4">
		        				<select name="model" class="form-control">
		        					@foreach($models as $model)
		        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        			<div class="col-md-4">
		        				<select name="area" class="form-control">
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Delivery Area:</label>
							<div class="col-md-9">
		        				<select name="delivery_area" class="form-control">
                                    <option value="same" selected> --- Same as above ---</option>
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer Instruction:</label>
							<div class="col-md-9">
		        				<textarea name="customer_note" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Promo Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="coupon" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Price With Tax:</label>
							<div class="col-md-9">
		        				<input type="text" name="amount" class="form-control" disabled/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Override Price:</label>
							<div class="col-md-9">
		        				<input type="text" name="override_price" class="form-control"/>
		        			</div>
		        		</div>
                        <div id="payment_box_edit">
                        </div>
                        <button type="button" id="add_payment_box_edit" style="margin-left: 30%;margin-bottom: 10px;">Add Payment Info</button>		        		
                    </div>
		      		<div id="date-error"></div>
                    
                    <input type="hidden" name="creation_mode" value="C" class="form-control" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<div class="modal-footer">
		      			<input type="submit" class="btn btn-primary" value="Add" />
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_delete_form" id="booking_delete_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		Are you sure, you want to delete this booking?<br>
		        		If "Yes" press "Delete" else "Close". Also select reason for deleting.
                        
                        <select name="reason" class="form-control" id="booking_delete_form_select">
                            <option value="not_selected" disabled selected>-- Select Reason --</option>
                            <option value="error">Error in Booking</option>
                            <option value="user_cancelled">User Cancelled</option>
                            <option value="wicked_ride_cancelled">Wicked Ride Cancelled</option>
                        </select>
                        <p id="booking_delete_form_error" style="color:red;"></p>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_delete_form_btn"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


    <div class="modal fade" id="editBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_edit_form" id="booking_edit_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabelEdit">Edit Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		
                        <div class="form-group">
							<label class="col-md-3">Start Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="startDate" class="form-control" id="editstartDate" placeholder="Start Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="startTime" class="form-control" id="editstartTime" placeholder="Start Time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">End Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="endDate" class="form-control" id="editendDate" placeholder="End Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="endTime" class="form-control" id="editendTime" placeholder="End Time"/>
		        			</div>
		        		</div>
		        		<div id="editdate-error"></div>
		        		
		        		<div class="form-group">
							<label class="col-md-3">Model & Area:</label>
							<div class="col-md-4">
		        				<select name="model" class="form-control">
		        					@foreach($models as $model)
		        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        			<div class="col-md-4">
		        				<select name="area" class="form-control">
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Delivery Area:</label>
							<div class="col-md-9">
		        				<select name="delivery_area" class="form-control">
                                    <option value="same" selected> --- Same as above ---</option>
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Reason for editing:</label>
							<div class="col-md-9">
		        				<select name="reason_for_editing" class="form-control">
                                    <option disabled>Select reason for editing</option>
		        					@foreach($editReasons as $editReason)
		        						<option value="{{ $editReason->id }}">{{ $editReason->reason }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Edit Notes:</label>
							<div class="col-md-9">
		        				<textarea name="edit_note" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Promo Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="coupon" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Price With Tax:</label>
							<div class="col-md-9">
		        				<input type="text" name="amount" class="form-control" disabled/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Override Price:</label>
							<div class="col-md-9">
		        				<input type="text" name="override_price" class="form-control"/>
		        			</div>
		        		</div>
                        <div id="payment_box">
                        </div>
                        <button type="button" id="add_payment_box" style="margin-left: 30%;margin-bottom: 10px;">Add Payment Info</button>
                        <div class="form-group">
                                <label class="col-md-3">Notify changes to user by email:</label>
                                <div class="col-md-9">
                                    <select name="notify_user" id ="editnotify_user" class="form-control">
                                            <option value="no">No</option>
                                            <option value="yes">Yes</option>
                                    </select>
                                </div>
                        </div>
                        <div id="editdate-error"></div>
                        
				        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_edit_form_btn"><i class="fa fa-pencil"></i> Save Changes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<script type="text/javascript">
		$(function(){
			Backend.Booking.init();
			
		});
	</script>
		
    <script type="text/javascript">
     $('.booking_delete_form_btn').click(function(){

                e = document.getElementById("booking_delete_form_select");
                var reason = e.options[e.selectedIndex].text;
            
                if(reason == "-- Select Reason --")
                {
                    $("#booking_delete_form_error").html("Select a reason");        
                }
                else
                {
                    $('#booking_delete_form').submit();
                    $('#cancel-delivery').submit();
                }

        });


    </script>

    <script type="text/javascript">
        $('.booking_edit_form_btn').click(function(){
            $('#booking_edit_form').submit();
        });
    </script>
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


    <script type="text/javascript">
        $('#add_payment_box').click(function(){
            $('#payment_box').append("<fieldset style=\"padding: 15px;margin: 10px;border: 2px solid lightgrey;\"><div class=\"form-group\"><label class=\"col-md-3\">Amount Paid:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"amount[]\" class=\"form-control\" /> </div></div> <div class=\"form-group\"><label class=\"col-md-3\">Payment Mode:</label><div class=\"col-md-9\">	<select name=\"payment_method[]\" id =\"payment_method\" class=\"form-control\"> @foreach($paymentMethods as $paymentMethod)<option value=\"{{$paymentMethod->id}}\">{{$paymentMethod->method}}</option>@endforeach </select>	  </div>	</div> <div class=\"form-group\"><label class=\"col-md-3\">Payment ID:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"payment_id[]\" class=\"form-control\" /></div> </div></fieldset>");
        });
        
        $('#add_payment_box_edit').click(function(){
            $('#payment_box_edit').append("<fieldset style=\"padding: 15px;margin: 10px;border: 2px solid lightgrey;\"><div class=\"form-group\"><label class=\"col-md-3\">Amount Paid:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"amount[]\" class=\"form-control\" /> </div></div> <div class=\"form-group\"><label class=\"col-md-3\">Payment Mode:</label><div class=\"col-md-9\">	<select name=\"payment_method[]\" id =\"payment_method\" class=\"form-control\"> @foreach($paymentMethods as $paymentMethod)<option value=\"{{$paymentMethod->id}}\">{{$paymentMethod->method}}</option>@endforeach </select>	  </div>	</div> <div class=\"form-group\"><label class=\"col-md-3\">Payment ID:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"payment_id[]\" class=\"form-control\" /></div> </div></fieldset>");
        });
        
    </script>


@stop	
