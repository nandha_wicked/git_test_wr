@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
	
	
        
             <p>&nbsp;</p>

     
                    
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
                        @if($error == "The bike has been successfully moved!!")
                            <script>
                                alert("{{ $error }}");
                            </script>
                        @endif
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
        <form method="post" action="/admin/moveBike/add" class="form-vertical" name="add-booking" id="add-booking">
            
        <h4 class="modal-title" >MOVE BIKE</h4>
            
            
            <p>&nbsp;</p>
            <p>&nbsp;</p>
                <div class="inPageForm">
                    <label class="col-md-5">Start Date & Time:</label>
                    <div class="col-md-7">
                        <input type="text" id="datetimepicker1" name="start_datetime" class="form-control"  placeholder="Leave Blank for Immediate transfer"/>
                    </div>

                </div>
                <div class="inPageForm">
                    <label class="col-md-5">End Date & Time:</label>
                    <div class="col-md-7">
                        <input type="text" id="datetimepicker2" name="end_datetime" class="form-control" placeholder="Leave Blank for Permanent transfers"/>
                    </div>

                </div>
                <div id="date-error"></div>
                <p>&nbsp;</p>
                <div class="inPageForm">
                    <label class="col-md-5">From Location:</label>
                    <div class="col-md-7">
                        <select name="from_area_id" class="form-control" id="areaPick">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inPageForm">
                    <label class="col-md-5">To Location:</label>
                    <div class="col-md-7">
                        <select name="to_area_id" class="form-control" id="areaDrop">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inPageForm">
                    <label class="col-md-5">Model & Bike Number:</label>
                    <div class="col-md-7">
                        <select name="model_id" class="form-control" id="modelDrop">
                            <option selected disabled> Select Model</option>
                            @foreach($modelList as $model)
                                <option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-9 inPageForm">
                        <div class="cascade" id="bikeNumbers"></div>
                </div>
            
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            

                
            
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="col-md-9 inPageForm">
                    <input type="submit" class="btn btn-primary inPageForm" style="width:150px;" value="Confirm Movement" />
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload();"><i class="fa fa-refresh"></i> Refresh</button>
                </div>
           
        </form>
    </div>
	    	

    <script>
        $(document).ready(function(){
            $("select#modelDrop").change(function(){

                var model_id = $("select#modelDrop option:selected").attr('value');
                var area_id = $("select#areaPick option:selected").attr('value');

                    $.ajax({
                        type: "GET",
                        url: "/api/get-bike-numbers-for-model",
                        data: { 
                        'model_id': model_id,
                            'area_id': area_id
                           },
                        cache: false,
                        beforeSend: function () {
                        $('#bikeNumbers').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                        },
                        success: function(html) {                
                            $("#bikeNumbers").html( html );
                        }
                    });

            });
        });
    </script>

	
    <script type="text/javascript">
                jQuery('#datetimepicker1').datetimepicker({
                  format:'Y-m-d H:00',
                  minDate:'2015/09/01'
                });
                jQuery('#datetimepicker2').datetimepicker({
                  format:'Y-m-d H:00',
                  minDate:'2015/09/01'
                });
                
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });        
    </script>


@stop	
