@extends('admin_layout')

@section('content')

 <title> Chart Data - <?php echo env('SITENAMECAP');?> </title>

   @if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
    @endif
    @if($results_page!="true")
    <form method="post" action="/admin/report_excel/chart" class="form-horizontal" name="report-inPage">
		      		
		      	<div>	
                        <div class="inPageForm">
                            <label class="col-md-5">Start Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker1" name="start_date" class="form-control datepicker" id="startDate" placeholder="Start Date"/>
                            </div>

                        </div>
                        <div class="inPageForm">
                            <label class="col-md-5">End Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker2" name="end_date" class="form-control datepicker" id="endDate" placeholder="End Date"/>
                            </div>

                        </div>
                    
                        <div class="inPageForm">
                            <label class="col-md-3">Bike Model :</label>
                            <div class="col-md-9">
                                <select name="area_or_model" class="form-control">
                                    <option value="model" selected>-- Report By Model -- </option>
                                    <option value="area" selected>-- Report By Area -- </option>
                                    <option value="areaAndModel" selected>-- Report By Area and Model-- </option>
                                    
                                </select>
                            </div>
                        </div>
                    
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="weekday_weekend" value="yes" checked/><label class="col-md-8">Break up by Weekends and Weekdays</label>
                            </div>
                        </div>
                    
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="days_of_week" value="yes"/><label class="col-md-8">Break up by days of the Week</label>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div align="center" >
                            <input style="width:200px; margin-top:10px" type="submit" class="btn btn-primary" value="Chart" />
                        </div>
                </div>
                
    </form>
    @else
    <h2>{{$pageTitle}}</h2>
    @endif
<script type="text/javascript">
    window.onload = function () {

    @foreach($charts as $chart)  
        var {{$chart['id']}} = new CanvasJS.Chart("{{$chart['container_id']}}",
        {
            title:{
                text: "{{$chart['title']}}",
                fontSize:18
            },
            animationEnabled: true,
            axisY: {
                title: "{{$chart['y_axis_title']}}",
                minimum: {{$chart['y_axis_min']}},
                maximum: {{$chart['y_axis_max']}}
            },
            
            @if($chart['secondary_y_axis'] == "true")
                toolTip: {
                    shared: true,
                    contentFormatter: function(e){
                      var str = "";
                      for (var i = 0; i < e.entries.length; i++){
                        if(str.substring(str.length-4,str.length-2)!="br")
                        {
                            var header = e.entries[i].dataPoint.label+"<hr style=\"margin:4px;border-top: 1px solid #a29a9a;\"/>";
                        }
                        else
                        {
                            var header = "";    
                        }
                        if("{{$chart['unit']}}" == "rupee")
                        {
                            var  temp = header + e.entries[i].dataSeries.name + " - ₹<strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
                        }
                        else
                        {
                            var  temp = header + e.entries[i].dataSeries.name + " - <strong>"+  e.entries[i].dataPoint.y + "%</strong> <br/>" ;
                        }
                         
                        str = str.concat(temp);
                      }
                      return (str);
                    }
                },
                axisY2: {
                    title: "{{$chart['y_axis_title']}}",
                    minimum: {{$chart['y_axis_min']}},
                    maximum: {{$chart['y_axis_max']}}
                },
            @endif
            legend: {
                verticalAlign: "bottom",
                horizontalAlign: "center"
            },
            theme: "theme2",
            data: [

                    {        
                        type: "column",
                        name:"{{$chart['y_axis_legend']}}",
                        showInLegend: true,
                        legendText:"{{$chart['y_axis_legend']}}",
                        dataPoints: [ 
                            @foreach($chart['y_label_pair'] as $y_label_pair)
                                {y: {{$y_label_pair['y']}}, label: "{{$y_label_pair['label']}}"},
                            @endforeach
                        ]
                    }
                    @if($chart['secondary_y_axis'] == "true")
                       ,{        
                            type: "column",  
                            name:"{{$chart['y_axis_legend_secondary']}}",
                            showInLegend: true,
                            legendText:"{{$chart['y_axis_legend_secondary']}}", 
                            axisYType: "secondary",
                            dataPoints: [ 
                                @foreach($chart['y_label_pair_secondary'] as $y_label_pair)
                                    {y: {{$y_label_pair['y']}}, label: "{{$y_label_pair['label']}}"},
                                @endforeach
                            ]
                        }
                    @endif

            ],
          legend:{
            cursor:"pointer",
            itemclick: function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              	e.dataSeries.visible = false;
              }
              else {
                e.dataSeries.visible = true;
              }
            	{{$chart['id']}}.render();
            }
          },
        });

        {{$chart['id']}}.render();

    @endforeach  

}
</script>


    @foreach($charts as $chart)
        <div id="{{$chart['container_id']}}" style="height: 300px; width: 100%;"></div>
        <p></p>
    @endforeach  

    <script type="text/javascript">
                $(function () {
                    $('#datetimepicker1').datepicker();
                    $('#datetimepicker2').datepicker();
                });
    </script>
  
  
@stop

@section('libraries')
        <script type="text/javascript" src="/js/canvasjs.min.js"></script>
@stop
