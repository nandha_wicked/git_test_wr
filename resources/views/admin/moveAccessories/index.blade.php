@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
	
	
        
             <p>&nbsp;</p>

     
                    
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
                        @if($error == "The accessories has been successfully moved!!")
                            <script>
                                alert("{{ $error }}");
                            </script>
                        @endif
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
        <form method="post" action="/admin/move_accessories/add" class="form-vertical" name="add-booking" id="add-booking">
            
        <h4 class="modal-title" >Move Accessories</h4>
            
            
            <p>&nbsp;</p>
            <p>&nbsp;</p>
                <div class="inPageForm">
                    <label class="col-md-5">Start Date & Time:</label>
                    <div class="col-md-7">
                        <input type="text" id="datetimepicker1" name="start_datetime" class="form-control"  placeholder="Leave Blank for Immediate transfer"/>
                    </div>

                </div>
                <div class="inPageForm">
                    <label class="col-md-5">End Date & Time:</label>
                    <div class="col-md-7">
                        <input type="text" id="datetimepicker2" name="end_datetime" class="form-control" placeholder="Leave Blank for Permanent transfers"/>
                    </div>

                </div>
                <div id="date-error"></div>
                <p>&nbsp;</p>
                <div class="inPageForm">
                    <label class="col-md-5">From Location:</label>
                    <div class="col-md-7">
                        <select name="from_area_id" class="form-control" id="fromAreaDrop">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inPageForm">
                    <label class="col-md-5">To Location:</label>
                    <div class="col-md-7">
                        <select name="to_area_id" class="form-control">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inPageForm">
                    <label class="col-md-5">Model:</label>
                    <div class="col-md-7">
                        <select name="model_id" class="form-control" id="modelDrop">
                            <option selected disabled> Select Model</option>
                            @foreach($modelList as $model)
                                <option value="{{ $model['id'] }}">{{ $model['model'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="inPageForm hidden" id="sizeForm">
                    <label class="col-md-5">Size:</label>
                    <div class="col-md-7">
                        <select name="size_id" class="form-control" id="sizeDrop">
                            <option selected disabled> Select Size</option>
                            @foreach($sizeList as $size)
                                <option value="{{ $size['id'] }}">{{ $size['size'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-9 inPageForm">
                        <div class="cascade" id="accessoriesTextIds"></div>
                </div>
            
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            

                
            
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="col-md-9 inPageForm">
                    <input type="submit" class="btn btn-primary inPageForm" style="width:150px;" value="Confirm Movement" />
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload();"><i class="fa fa-refresh"></i> Refresh</button>
                </div>
           
        </form>
    </div>
	    	

    <script>
        $(document).ready(function(){
            $("select#modelDrop").change(function(){
                
                $("#sizeForm").removeClass("hidden");
                
            });
        });
        $(document).ready(function(){
            $("select#sizeDrop").change(function(){

                var model_id = $("select#modelDrop option:selected").attr('value');
                var size_id = $("select#sizeDrop option:selected").attr('value');
                var from_area_id = $("select#fromAreaDrop option:selected").attr('value');

                    $.ajax({
                        type: "GET",
                        url: "/api/getAccessoriesTextIdForModelSize",
                        data: { 
                            'model_id': model_id,
                            'size_id': size_id,
                            'from_area_id': from_area_id
                        },
                        cache: false,
                        beforeSend: function () {
                        $('#accessoriesTextIds').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                        },
                        success: function(html) {                
                            $("#accessoriesTextIds").html( html );
                        }
                    });

            });
        });
    </script>

	
    <script type="text/javascript">
                jQuery('#datetimepicker1').datetimepicker({
                  format:'Y-m-d H:00',
                  minDate:'2015/09/01'
                });
                jQuery('#datetimepicker2').datetimepicker({
                  format:'Y-m-d H:00',
                  minDate:'2015/09/01'
                });
                
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });        
    </script>


@stop	
