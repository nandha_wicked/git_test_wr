@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Service Block Add </h2>
		
        
        
             <p>&nbsp;</p>
 
		
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
                    <th>Bike Number</th>
                    <th>Area</th>
                    <th>Model</th>
					<th>Start Date Time</th>
					<th>End Date Time</th>
					<th>Notes</th>
                    <th>CreatedBy</th>
                    <th>Approval Status</th>
                    <th>Approved By</th>
                	<th width="170px">Action</th>
               </tr>
			</thead>
			<tbody>
				@foreach($serviceBlocks as $serviceBlock)
					<tr>
						<td>{{ $serviceBlock['id'] }}</td>
                        <td>{{ $serviceBlock->getBikeNumber() }}</td>
                        <td>{{ $serviceBlock->getAreaName() }}</td>
                        <td>{{ $serviceBlock->getModelName() }}</td>
                        
                        <td>{{ $serviceBlock->getReadableDateTime($serviceBlock->start_datetime) }}</td>
                        <td>{{ $serviceBlock->getReadableDateTime($serviceBlock->end_datetime) }}</td>

                        <td>{{ $serviceBlock['note'] }}</td>
						<td>{{ $serviceBlock->getCreatedByEmail() }}</td>
						<td>{{ $serviceBlock['approval_status'] }}</td>
                        <td>{{ $serviceBlock->getApprovedByEmail() }}</td>
						<td>
                                
                                @if($serviceBlock['available'])
                                <button class="btn btn-success wk-btn serviceBlock_approve_btn" data-id="{{$serviceBlock->id}}">Block</button>
                                @else
                                <button style="width:150px;" class="btn btn-success wk-btn serviceBlock_available_check_btn" data-id="{{$serviceBlock->id}}">Show Bookings</button>
                                @endif
                                <p></p>

                                <button  class = "btn btn-danger wk-btn serviceBlock_reject_btn" data-id="{{$serviceBlock->id}}">Delete</button>
                            
						</td>
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.serviceBlock_approve_btn').click(function(){
			var id = $(this).data('id');
			$('#approveServiceBlock-modal form').attr("action","/admin/serviceBlock/block/"+id);
			$('#approveServiceBlock-modal').modal('show');
		});
	</script>

    <script type="text/javascript">
		$('.serviceBlock_available_check_btn').click(function(){
			var id = $(this).data('id');
            window.location.href = "/admin/collidingBookings/"+id;
		});
	</script>
    
    <script type="text/javascript">
		$('.serviceBlock_reject_btn').click(function(){
			var id = $(this).data('id');
			$('#rejectServiceBlock-modal form').attr("action","/admin/serviceBlock/delete/"+id);
			$('#rejectServiceBlock-modal').modal('show');
		});
	</script>

    <?php
    if(isset($errorStr))
    {
        echo "<script type=\"text/javascript\"> alert(\"".$errorStr."\"); </script>";
    }
    ?>

@stop

@section('model')

	<div class="modal fade" id="approveServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Create Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to create this Service Block?<br>
		        		If "Yes" press "Create" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"> Create</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

    <div class="modal fade" id="rejectServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Reject Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to reject this Service Block?<br>
		        		If "Yes" press "Reject" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary">Reject</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
    
    
	
@stop	
