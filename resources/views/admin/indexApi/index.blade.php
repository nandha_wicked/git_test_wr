@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">API Homepage</h2>
        <div class="inline-elements">
            <a href="/admin/api-call/add" class="btn btn-primary"><i class="fa fa-plus"></i> API Calls</a>
            <button class="btn btn-primary rearrange_list_btn">Rearrange APIs</button>
        </div>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif        
		<table class="wk-table">
			<thead>
				<tr>
					<th>API</th>
					<th>Category</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($apiCategories as $apiCategory)
                    @foreach($apiCategory['apis'] as $api)
                        <tr>
                            <td>{{ $api->title }}</td>
                            <td>{{ $api->getCategory() }}</td>
                            <td>
                                <button class="btn btn-info wk-btn" onclick="window.location.href='api-call/edit/{{$api->id}}'"><i class="fa fa-pencil-square-o"></i> Edit</button>
                                <button class="btn btn-danger wk-btn deleteApi" data-id="{{$api->id}}'"><i class="fa fa-trash-o"></i> Delete</button>
                            </td>
                        </tr>
                    @endforeach
				@endforeach
			</tbody>
		</table>
	</div>


<script>
    $('.deleteApi').click(function(){
			var id = $(this).data('id');
			$('#delete-api-modal form').attr("action","api-call/delete/"+id);
			$('#delete-api-modal').modal('show');
		});
    
    $('.rearrange_list_btn').click(function(){
			$('#rearrange-api-modal').modal('show');
    });
    
    var el = document.getElementById('apis_to_reaarange');
    var sortable = Sortable.create(el);
    
</script>
@stop

@section('model')
<div class="modal fade" id="delete-api-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete API call</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure you want to delete the api call?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

    <div class="modal fade" id="rearrange-api-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="background-color: linen;">
				<form method="post" action="rearrange-api">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Rearrange APIs</h4>
		      		</div>
                    @foreach($apiCategories as $apiCategory)
                        @if(count($apiCategory['apis']) >1)
                            <div style="text-align:center;"><h4>{{$apiCategory->category}}</h4></div>
                            <ul style="list-style: none;" class="rearrage-list" id="apis_to_reaarange{{$apiCategory->id}}">
                                @foreach($apiCategory['apis'] as $api)
                                    <li>{{ $api->title }}  -  {{ $api->getCategory() }}<input type="hidden" value="{{$api->id}}" name="api_category[{{$apiCategory->id}}][]"></li>
                                @endforeach

                            </ul>
                        @endif
                    @endforeach
		      		
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Save Changes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

<script>  
    
    @foreach($apiCategories as $apiCategory)
        @if(count($apiCategory['apis']) >1)
            var el = document.getElementById('apis_to_reaarange{{$apiCategory->id}}');
            Sortable.create(el,{
                group: {
                    name:"category{{$apiCategory->id}}",
                    pull:false,
                    put:true
                },
                animation: 150,
                ghostClass: 'ghost',
                store: {
                    get: function (sortable) {
                        var order = localStorage.getItem(sortable.options.group);
                        return order ? order.split('|') : [];
                    },
                    set: function (sortable) {
                        var order = sortable.toArray();
                        localStorage.setItem(sortable.options.group, order.join('|'));
                    }
                },
                
            });
        @endif
    @endforeach
</script>

@stop
