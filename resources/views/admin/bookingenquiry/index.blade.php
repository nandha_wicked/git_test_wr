@extends('admin_layout')

@section('content')

    <div class="contactEnquiry-pg">
        <h2 class="title">ContactEnquiry Homepage</h2>
        <table class="wk-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Mobile Number</th>
                <th>Message</th>
                <th>Bike Model</th>
                <th>StartDate</th>
                <th>EndDate</th>
                <th>Created At</th>

            </tr>
            </thead>
            <tbody>
            @foreach($enquirylist as $enq)
                <tr>
                    <td>{{ $enq->id }}</td>
                    <td>{{ $enq->firstname }}</td>
                    <td>{{ $enq->lastname }}</td>
                    <td>{{ $enq->email }}</td>
                    <td>{{ $enq->phone }}</td>
                    <td>{{ $enq->message }}</td>
                    <td>{{$enq->bikemodel}}</td>
                    <td>{{$enq->getStartDate() }}</td>
                    <td>{{$enq->getEndDate() }}</td>
                    <td>{{ $enq->getCreatedAt() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@stop
