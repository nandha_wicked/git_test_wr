@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Colliding Booking for the service block for {{$serviceBlockTimePeriod}}</h2>		
                   
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        @foreach($result as $resultElement)
        
        <h3>Colliding bookings for the period - {{$resultElement['dates']}}</h3>
        <p/>
        
        <h3>Bookings that are an exact match for the date</h3>
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1980px; margin-top: 2px;">
                <thead>
                    <tr>
                        <th style="width: 50px !important;">Id</th>
                        <th style="width: 80px !important;">Amount Paid</th>
                        <th style="width: 80px !important;">Count of Trouble Cases</th>
                        <th style="width: 80px !important;">Start Date and Time</th>
                        <th style="width: 80px !important;">End Date and Time</th>
                        <th style="width: 140px !important;">Customer</th>
                        <th style="width: 110px !important;">Bike</th>
                        <th style="width: 100px !important;">Area</th>
                        <th style="width: 300px !important;">Customer Instruction</th>
                        <th style="width: 80px !important;">Action</th>
                   </tr>
                </thead>
                <tbody>
                    @foreach($resultElement['exactMatchBookings'] as $booking)
                        <tr>
                            <td style="{{ $booking->md5Hash() }} color:#{{$booking->md5HashText()}}">{{ $booking['id'] }}</td>
                            <td>{{ $booking->total_price }}</td>
                            <td>{{ $booking->getCountOfWRCancelledForUser() }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->start_datetime) }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->end_datetime) }}</td>
                            <td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserEmail($booking->user_id) }}</td>
                            <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                            <td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
                            <td>{{ $booking->note }}</td>
                            <td>
                                <button class="btn btn-danger wk-btn booking_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
                            </td>


                    </tr>
                    @endforeach
                </tbody>
            </table>
        
            <br/>
            <h3>Other bookings that are in the date</h3>
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1980px; margin-top: 2px;">
                <thead>
                    <tr>
                        <th style="width: 50px !important;">Id</th>
                        <th style="width: 80px !important;">Amount Paid</th>
                        <th style="width: 80px !important;">Count of Trouble Cases</th>
                        <th style="width: 80px !important;">Start Date and Time</th>
                        <th style="width: 80px !important;">End Date and Time</th>
                        <th style="width: 140px !important;">Customer</th>
                        <th style="width: 110px !important;">Bike</th>
                        <th style="width: 100px !important;">Area</th>
                        <th style="width: 300px !important;">Customer Instruction</th>
                        <th style="width: 80px !important;">Action</th>
                   </tr>
                </thead>
                <tbody>
                    @foreach($resultElement['matchedBookings'] as $booking)
                        <tr>
                            <td style="{{ $booking->md5Hash() }} color:#{{$booking->md5HashText()}}">{{ $booking['id'] }}</td>
                            <td>{{ $booking->total_price }}</td>
                            <td>{{ $booking->getCountOfWRCancelledForUser() }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->start_datetime) }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->end_datetime) }}</td>
                            <td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserEmail($booking->user_id) }}</td>
                            <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                            <td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
                            <td>{{ $booking->note }}</td>
                            <td>
                                <button class="btn btn-danger wk-btn booking_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
                            </td>


                    </tr>
                    @endforeach
                </tbody>
            </table>
        
        <p/>
        <br/>
        <br/>
        <br/>
        @endforeach
	</div>

	<script type="text/javascript">
		$('.booking_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBooking-modal form').attr("action","/admin/booking/delete/"+id);
			$('#deleteBooking-modal').modal('show');
		});
        
	</script>
    
   

@stop

@section('model')
	


	<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_delete_form" id="booking_delete_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		Are you sure, you want to delete this booking?<br>
		        		If "Yes" press "Delete" else "Close". Also select reason for deleting.
                        
                        <select name="reason" class="form-control" id="booking_delete_form_select">
                            <option value="not_selected" disabled selected>-- Select Reason --</option>
                            <option value="error">Error in Booking</option>
                            <option value="user_cancelled">User Cancelled</option>
                            <option value="wicked_ride_cancelled">Wicked Ride Cancelled</option>
                        </select>
                        <p id="booking_delete_form_error" style="color:red;"></p>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_delete_form_btn"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<script type="text/javascript">
		$(function(){
			Backend.Booking.init();
			
		});
	</script>
		
    <script type="text/javascript">
     $('.booking_delete_form_btn').click(function(){

                e = document.getElementById("booking_delete_form_select");
                var reason = e.options[e.selectedIndex].text;
            
                if(reason == "-- Select Reason --")
                {
                    $("#booking_delete_form_error").html("Select a reason");        
                }
                else
                {
                    $('#booking_delete_form').submit();
                    $('#cancel-delivery').submit();
                }

        });


    </script>


@stop	
