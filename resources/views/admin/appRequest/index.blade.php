@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">App Payments</h2>
        
        
             <p>&nbsp;</p>

     
                    <form id="custom-search-form" method="get" action="/admin/app-payments/search" class="form-search form-horizontal pull-left">
                        <div class="input-append span12">
                            <input type="text" name = "searchStr" class="search-query mac-style" placeholder="Search">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                    </form>
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        @if(isset($appRequests) > 0)
		<table class="fixedCol wk-table" style="table-layout: fixed; width:1000px">
			<thead>
				<tr>
					<th style="width: 600px !important;">App Request</th>
                    <th style="width: 400px !important;">App Request</th>
               </tr>
			</thead>
			<tbody>
				@foreach($appRequests as $appRequest)
					<tr>
                        <td>{{ $appRequest['string'] }}</td>
                        <td>{{ $appRequest['enquiry'] }}</td>
                    </tr>
				@endforeach
			</tbody>
		</table>
        @endif
	</div>

	
@stop
