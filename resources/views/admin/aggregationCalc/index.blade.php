@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Aggregated Bikes Homepage</h2>
        
        
        <form method="post" action="/admin/calculatePayout">

                        <select name="month" class="form-control">
                            <option value="not_selected" disabled selected>-- Select Month --</option>
                            <option value="August 2016">August 2016</option>
                            <option value="September 2016">September 2016</option>
                            <option value="October 2016">October 2016</option>
                            <option value="November 2016">November 2016</option>
                            <option value="December 2016">December 2016</option>
                            <option value="January 2017">January 2017</option>
                            <option value="February 2017">February 2017</option>
                            <option value="March 2017">March 2017</option>
                            <option value="April 2017">April 2017</option>
                            <option value="May 2017">May 2017</option>
                            <option value="June 2017">June 2017</option>
                            <option value="July 2017">July 2017</option>
                            
                        </select>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		
		      			<button class="btn btn-primary "> Show</button>
		        		
        </form>
        
        
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        @foreach($aggregators as $aggregator)
            
        <h3>Aggregation Calculation for {{$aggregator->name}}</h3>
            <table class="fixedCol wk-table" style="table-layout: fixed; width:400px">
                <thead>
                    <td style="width: 100px !important;height: 0px !important;padding-bottom: 0px;padding-top: 0px;border-top-width: 0px;border-bottom-width: 0px;padding-right: 0px;padding-left: 0px;border-right-width: 0px;border-left-width: 0px;"></td>
                    <td style="height: 0px !important;padding-bottom: 0px;padding-top: 0px;border-top-width: 0px;border-bottom-width: 0px;padding-right: 0px;padding-left: 0px;border-right-width: 0px;border-left-width: 0px;"></td>
                </thead>
                <tbody>
                    <tr>
                        <td >Phone Number</td>
                        <td>{{$aggregator->phone_number}}</td>
                    </tr>
                
                    <tr>                         
                        <td >Bank Account</td>
                        <td>{{$aggregator->bank_account_number}}</td>  
                    </tr>
               
                    <tr>                         
                        <td >IFSC code</td>
                        <td>{{$aggregator->ifsc_code}}</td>  
                    </tr>
                               
                    <tr>                         
                        <td >Address</td>
                        <td><pre>{{$aggregator->address}}</pre></td>  
                    </tr>
                
                    <tr>                         
                        <td >PAN</td>
                        <td>{{$aggregator->pan}}</td>  
                    </tr>
                
                    <tr>                         
                        <td >Email</td>
                        <td>{{$aggregator->getEmail()}}</td>  
                    </tr>
                </tbody> 
                    
            </table>
        
        
            <table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
                <thead>
                    <tr>
                        <th style="width: 100px !important;">Reference ID</th>
                        <th style="width: 140px !important;">From Date</th>
                        <th style="width: 140px !important;">To Date</th>
                        <th style="width: 120px !important;">Customer Name</th>
                        <th style="width: 110px !important;">Bike Model</th>
                        <th style="width: 100px !important;">Amount Paid</th>
                        <th style="width: 100px !important;">Without GST</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($aggregator['booking_bucket'] as $booking)
                        <tr>

                            <td>{{$booking->reference_id}}</td>
                            <td>{{$booking->start_datetime}}</td>
                            <td>{{$booking->end_datetime}}</td>
                            <td>{{$booking->getUserNameThis() }}</td>
                            <td>{{$booking->getBikeModelNameThis() }}</td>
                            <td>{{$booking->total_price}}</td>
                            <td>{{round($booking->total_price_without_vat(),2)}}</td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        
        <p><br/></p>
        
            <h4> Summary of Revenue Share</h4>
        
            <table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
                <thead>
                    <tr>
                        <th style="width: 100px !important;">Vehicle No.</th>
                        <th style="width: 130px !important;">Bike Model</th>
                        <th style="width: 100px !important;">Revenue Share</th>
                        <th style="width: 110px !important;">Gross Amount</th>
                        <th style="width: 110px !important;">Gross Rev Share</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($aggregator['aggregatedBike_bucket'] as $agg_bike)
                        <tr>

                            <td>{{$agg_bike->getBikeNumber()}}</td>
                            <td>{{$agg_bike->getModelName()}}</td>
                            <td>{{$agg_bike->commission_perc}}</td>
                            <td>{{$agg_bike['totalCollection']}}</td>
                            <td>{{$agg_bike['totalCollection']*$agg_bike->commission_perc/100 }}</td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        
        <p><br/><br/><br/></p>
            
        @endforeach
        
        
        
	</div>



	

@stop

