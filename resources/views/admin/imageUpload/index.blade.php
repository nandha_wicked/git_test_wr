@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">Upload Image Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addSliderImage-modal"><i class="fa fa-plus"></i> Add Image</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
			
	</div>
    


@stop


@section('model')
	
	<div class="modal fade" id="addSliderImage-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/image/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Image</h4>
		      		</div>
                    
		      		<div class="modal-body">
                       
                        <div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverImgUpload" />
				        			</div>
		        				</div>
		        			</div>
		        		</div>
                        
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


@stop
