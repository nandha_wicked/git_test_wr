<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/css/admin-style.css" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
	</head>
	<body class="login-pg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="login-container">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif

						<div class="img"><img src="/img/admin3.png" /></div>
						<form action="/login" method="post">
							<input type="text" name="email" placeholder="Email address" class="form-control" value="{{ Input::old('email') }}"/>
							<input type="password" name="password" placeholder="Password" class="form-control" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type="submit" class="btn btn-primary">Sign in</button>
						</form>
						<div class="float_right"><a href="../password/email" class="forgot-btn"><span class="indicator">forgot your password?</span></a></div>
						<div class="float_right"><a href="../login" ><span class="small_caps">Register as new user</span></a></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
