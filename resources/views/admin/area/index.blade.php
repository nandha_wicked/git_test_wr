@extends('admin_layout')

@section('content')
	
	<div class="area-pg">
		<h2 class="title">Area Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addArea-modal"><i class="fa fa-plus"></i> Add Area</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
			<thead>
				<tr>
					<th style="width: 100px !important;">Id</th>
					<th style="width: 130px !important;">City</th>
					<th style="width: 140px !important;">Area</th>
					<th style="width: 150px !important;">Address</th>
					<th style="width: 80px !important;">Status</th>
                    <th style="width: 130px !important;">Google Map Position</th>
                    <th style="width: 130px !important;">Google Map Link</th>
                    <th style="width: 250px !important;">Hours Open</th>
                    <th style="width: 250px !important;">Kirana Users</th>
					<th style="width: 120px !important;">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($areaList as $area)
					<tr>
						<td>{{ $area->id }}</td>
						<td><?= $area->getCityName($area->city_id); ?></td>
						<td>{{ $area->area }}</td>
						<td>{{ $area->address }}</td>
						<td>
							@if( $area->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
                        <td>{{$area->latitude.",".$area->longitude}}</td>
                        <td>{{$area->maps_link}}</td>
                        <td>{!! $area->getHoursOpen() !!}</td>
                        <?php $kiranaUsers = $area->getKiranaUsers(); ?>
                        <td><?php echo $kiranaUsers; ?></td>
						<td>
							<button class="btn btn-info wk-btn area_edit_btn" data-id="{{ $area->id }}" 
								data-cityid="{{ $area->city_id }}" data-area="{{ $area->area }}" 
								data-address="{{ $area->address }}" data-status="{{ $area->status }}" 
								data-code="{{ $area->code }}" data-lat="{{$area->latitude}}" data-long="{{$area->longitude}}" data-hoursopen="{{$area->getHoursForEdit()}}" data-kiranausers="<?php echo $kiranaUsers;?>"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.area_edit_btn').click(function(){
			var id = $(this).data('id');
			var cityId = $(this).data('cityid');
			var area = $(this).data('area');
			var status = $(this).data('status');
			var addr = $(this).data('address');
			var code = $(this).data('code');
            var lat = $(this).data('lat');
            var long = $(this).data('long');
            var hoursopen = $(this).data('hoursopen');
            var kiranausers = $(this).data('kiranausers');
            if(hoursopen != "")
            {
                var hourJSON = jQuery.parseJSON(JSON.stringify(hoursopen));
                for (i = 0; i < 7; i++)
                {
                    $("#editArea-modal form input[name='from_open_hours["+i+"]\']").val(hourJSON[i].first);
                    $("#editArea-modal form input[name='to_open_hours["+i+"]\']").val(hourJSON[i].last);
                    $("#editArea-modal form input[name='closed_hours["+i+"]\']").val(hourJSON[i].closed);
                }
            }
            else
            {
                for (i = 0; i < 7; i++)
                {
                    $("#editArea-modal form input[name='from_open_hours["+i+"]\']").val('');
                    $("#editArea-modal form input[name='to_open_hours["+i+"]\']").val('');
                    $("#editArea-modal form input[name='closed_hours["+i+"]\']").val('');
                }
            }
			
			$('#editArea-modal form').attr("action","/admin/area/edit/"+id);
			$('#editArea-modal form select[name=cityName] option[value='+cityId+']').prop('selected',true);
			//$('#editArea-modal form input[name=cityId]').val(city);
			$('#editArea-modal form input[name=areaName]').val(area);
			$('#editArea-modal form textarea[name=areaAddress]').val(addr);
			$('#editArea-modal form input[name=areaCode]').val(code);
            $('#editArea-modal form input[name=lat]').val(lat);
            $('#editArea-modal form input[name=long]').val(long);
            $('#editArea-modal form input[name=kirana_users]').val(kiranausers);
            
            
            
            
			if(status == '1')
				$('#editArea-modal form #ase1').prop("checked",true);	
			else
				$('#editArea-modal form #ase2').prop("checked",true);
			$('#editArea-modal').modal('show');
		});
	</script>	

@stop

@section('model')
	
	<div class="modal fade" id="addArea-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/area/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD AREA</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">City Name:</label>
							<div class="col-md-9">
		        				<select name="cityName" class="form-control">
		        					@foreach($cityList as $city)
		        						<option value="{{ $city->id }}">{{ $city->city }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaName" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Address:</label>
							<div class="col-md-9">
		        				<textarea name="areaAddress" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaCode" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Map Co-ordinates:</label>
							<div class="col-md-4">
		        				<input type="text" name="lat" placeholder="Latitude" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="long" placeholder="Longitude" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Sunday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Sunday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Monday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Monday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Tuesday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Tuesday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Wednesday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Wednesday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Thursday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Thursday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Friday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Friday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Saturday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[]" placeholder="Closed Hours Saturday" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Area Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="areaStatus" id="asa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="areaStatus" id="asa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="editArea-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT AREA</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">City Name:</label>
							<div class="col-md-9">
		        				<select name="cityName" class="form-control">
		        					@foreach($cityList as $city)
		        						<option value="{{ $city->id }}">{{ $city->city }}</option>
		        					@endforeach
		        				</select>
		        				<!-- <input type="hidden" name="cityId" /> -->
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaName" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Address:</label>
							<div class="col-md-9">
		        				<textarea name="areaAddress" class="form-control" rows="4" /></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaCode" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Map Co-ordinates:</label>
							<div class="col-md-4">
		        				<input type="text" name="lat" placeholder="Latitude" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="long" placeholder="Longitude" class="form-control"/>
		        			</div>
		        		</div>
                    
                        <div class="form-group">
							<label class="col-md-3">Sunday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[0]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[0]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[0]" placeholder="Closed Hours Sunday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Monday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[1]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[1]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[1]" placeholder="Closed Hours Monday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Tuesday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[2]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[2]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[2]" placeholder="Closed Hours Tuesday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Wednesday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[3]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[3]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[3]" placeholder="Closed Hours Wednesday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Thursday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[4]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[4]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[4]" placeholder="Closed Hours Thursday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Friday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[5]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[5]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[5]" placeholder="Closed Hours Friday" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Saturday Hours:</label>
							<div class="col-md-2">
		        				<input type="text" name="from_open_hours[6]" placeholder="Start Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-2">
		        				<input type="text" name="to_open_hours[6]" placeholder="End Hour" class="form-control"/>
		        			</div>
                            <div class="col-md-4">
		        				<input type="text" name="closed_hours[6]" placeholder="Closed Hours Saturday" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Kirana Users:</label>
							<div class="col-md-9">
		        				<input type="text" name="kirana_users" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Area Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="areaStatus" id="ase1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="areaStatus" id="ase2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
