@extends('admin_layout')

@section('content')
	
	<div class="city-pg">
		<h2 class="title">Promotional Balance</h2>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:700px">
			<thead>
				<tr>
					<th style="width: 150px !important;">User Email</th>
					<th style="width: 100 !important;">Balance</th>
					<th style="width: 450px !important;">Referred Users</th>
				</tr>
			</thead>
			<tbody>
				@foreach($userDetails as $userDetail)
					<tr>
						<td>{{ $userDetail['email'] }}</td>
						<td>{{ $userDetail['balance'] }}</td>
						<td>{!! $userDetail['referred_emails'] !!}</td>
						
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

@stop

@section('model')
	
	

@stop	