@extends('admin_layout')

@section('content')
	
	<div class="city-pg">
		<h2 class="title">Global Config Homepage</h2>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Parameter</th>
					<th>Value</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($configdatas as $configdata)
					<tr>
						<td>{{ $configdata->id }}</td>
						<td>{{ $configdata->parameter }}</td>
						<td>{{ $configdata->value }}</td>
						<td>
							<button class="btn btn-info wk-btn city_edit_btn" data-id="{{ $configdata->id }}" data-parameter="{{ $configdata->parameter }}" data-value="{{ $configdata->value }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.city_edit_btn').click(function(){
			var id = $(this).data('id');
			var parameter = $(this).data('parameter');
			var value = $(this).data('value');
				
			$('#editGlobalConfig-modal form').attr("action","/admin/global_variable/edit/"+id);
			$('#editGlobalConfig-modal form input[name=parameter]').val(parameter);
			$('#editGlobalConfig-modal form input[name=value]').val(value);
			$('#editGlobalConfig-modal').modal('show');
		});
	</script>

@stop

@section('model')

	<div class="modal fade" id="editGlobalConfig-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Global Variables</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Parameter:</label>
							<div class="col-md-9">
		        				<input type="text" name="parameter" class="form-control" readonly="" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Value :</label>
			        		<div class="col-md-9"> 
		        				<input type="text" name="value" class="form-control" />
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	