@extends('admin_layout')

@section('content')

    
    <form class="form-inline" method="get" action="/admin/salesReportChart">
            
			<div class="form-group">
		    	<input type="text" name="start_date" class="form-control" id="startDateFilter" <?php if($start_date!="") echo "value = ".$start_date; else echo "placeholder=\"Start Date\""; ?> autocomplete="off">
		  	</div>
		  	
		  	<div class="form-group">
		    	<input type="text" name="end_date" class="form-control" id="endDateFilter" <?php if($end_date!="") echo "value = ".$end_date; else echo "placeholder=\"End Date\""; ?> autocomplete="off">
		  	</div>
		  	
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <button type="submit" class="btn btn-success">Filter</button>
		</form>

<div id="container" style="min-width: 310px; max-width: 600px; height: 500px; margin: 0 auto"></div>
<div id="container2" style="min-width: 310px; max-width: 600px; height: 500px; margin: 0 auto"></div>
    

	<script type="text/javascript">
	$(function () {
    // Create the chart
   
        
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Break up of transactions by Categories'
        },
        subtitle: {
            text: 'Total Number of Transactions between {{$start_date}} and {{$end_date}}'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
       "series": {!!$mainSeriesCount!!},
        "drilldown": {
            "series": {!!$drillDownSeriesCount!!}
        }
    });
        
        
    $('#container2').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Break up of Amount Collected by Categories'
        },
        subtitle: {
            text: 'Total Amount Collected between {{$start_date}} and {{$end_date}}'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
       "series": {!!$mainSeriesAmount!!},
        "drilldown": {
            "series": {!!$drillDownSeriesAmount!!}
        }
    });
});
</script>

<script type="text/javascript">
    today = new Date();
    yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
	$('#startDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
            maxDate:yesterday,
            minDate:new Date('2016-07-01'),
            onSelectDate: function(selectedDateTime, $el){
                document.getElementById("endDateFilter").value = null;
                var minDate = selectedDateTime.dateFormat('Y/m/d');
                $('#endDateFilter').datetimepicker({
                        timepicker: false,
                        scrollInput: false,
                        format:'Y-m-d',
                        minDate:minDate,
                        maxDate:yesterday

                }).datetimepicker('show').focus();
            }
    });
    
</script>

@stop

@section('libraries')
        <script src="/js/highcharts.js"></script>
        <script src="/js/modules/data.js"></script>
        <script src="/js/modules/drilldown.js"></script>
@stop
