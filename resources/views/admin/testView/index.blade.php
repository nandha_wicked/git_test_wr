@extends('admin_layout')
@section('content')


<form method="get" action="/test327e5gbach5FFD" class="form-horizontal" name="add-call" id="add-call"> 
        <div class="form-group">
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Name : </label>
                    <div class="col-md-5">
                        <input type="text" name="call_name" class="form-control" />
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call URL : </label>
                    <div class="col-md-5">
                        <input type="text" name="call_url" class="form-control" />
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Type : </label>
                    <div class="col-md-5">
                        <select name="call_type" id="call_type" class="form-control">
                            <option value="GET">GET</option>
                            <option value="POST">POST</option>
                            <option value="PUT">PUT</option>
                            <option value="DELETE">DELETE</option>
                            <option value="PATCH">PATCH</option>
                            <option value="HEAD">HEAD</option>
                            <option value="OPTIONS">OPTIONS</option>
                            <option value="CONNECT">CONNECT</option>
                        </select>
                        
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Description : </label>
                    <div class="col-md-7">
                        <textarea type="text" name="call_description" rows="3" class="form-control textarea"></textarea>
                    </div>
                </div>
            </div>
                        
        </div>
        <div class="form-group">
            <fieldset>
                <div id= "argument-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_argument_btn" type = "button">Add Argument</button>
                </div>
            </fieldset>
        </div>
        <div class="form-group">
            <fieldset>
                <div id= "box-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_box_btn" type = "button">Add Message Box</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "table-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_table_btn" type = "button">Add Table</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "response-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_response_btn" type = "button">Add Example Response</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "code-sample-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_code_sample_btn" type = "button">Add Code Sample</button>
                </div>
            </fieldset>
        </div>
        
        <input type="submit" class="btn btn-primary" value="Add Call" />
    
</form>
       



    
    
<script>
    var i = 0;
    
    var j = 0;
    
    var k = 0;
    
    var l = 0;
    
    var m = 0;
    
    var n = 0;
    
    $(document).ready(function(){

                

        $("#add_argument_btn").click(function(){

            
            $("#argument-field-set").append(
                "<br/>"+
                "<fieldset id=\"argument"+window.i+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Name : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Argument Description : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"argument_description["+window.i+"]\" class=\"form-control argument-textarea"+window.i+"\" rows=\"3\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_type["+window.i+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" class=\"form-control\">"+
                                "<option value=\"Required\">Required</option>"+
                                "<option value=\"Optional\">Optional</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<br/>"+
                    "<div class=\"form-group\">"+
                    "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                        "<br/>"+ 
                    "</div>"+
                    "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+window.i+"\">"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.i+"\" onClick='removeElement(\"argument"+window.i+"\")'>Delete Argument</button>"+
                    "</div>"+
                "</fieldset>"
            
            );
            
            $('.argument-textarea'+window.i).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]}); 
            
            window.i++;
                        
            
        });
        
        
        $("#add_box_btn").click(function(){

            
            $("#box-field-set").append(
                "<br/>"+
                "<fieldset id=\"message-box-"+window.j+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Message Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"message_type["+window.j+"]\" class=\"form-control\">"+
                                "<option value=\"blue\">Info</option>"+
                                "<option value=\"red\">Alert</option>"+
                                "<option value=\"green\">Success</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Message : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<textarea type=\"text\" name=\"message["+window.j+"]\" class=\"form-control box-textarea"+window.j+"\" rows=\"3\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.j+"\" onClick='removeElement(\"message-box-"+window.j+"\")'>Delete Message Box</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.box-textarea'+window.j).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]});
            
            window.j++;
        });
        
        
        
        $("#add_table_btn").click(function(){

            
            $("#table-field-set").append(
                "<br/>"+
                "<fieldset id=\"table"+window.k+"\">"+
                   
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Left Title : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"table_left_title["+window.k+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Left Title : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"table_right_title["+window.k+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    
                    "<br/>"+
                    "<div class=\"form-group\">"+
                    "<div id = \"table-cells-fieldset-"+window.k+"\"></div>"+
                        "<br/>"+ 
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"button-"+window.k+"\" onClick='addTableCell(\""+window.k+"\")'>Add Cells</button>"+
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.k+"\" onClick='removeElement(\"table"+window.k+"\")'>Delete Table</button>"+
                    "</div>"+
                "</fieldset>"
            
            );
             
            window.k++;
                        
            
        });
        
        
        $("#add_response_btn").click(function(){

            
            $("#response-field-set").append(
                "<br/>"+
                "<fieldset id=\"response-"+window.m+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Response Introduction : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"response_intro["+window.m+"]\" class=\"form-control response-textarea"+window.m+"\" rows=\"2\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Response : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"response["+window.m+"]\" class=\"form-control response-textarea"+window.m+"\" rows=\"9\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.m+"\" onClick='removeElement(\"response-"+window.m+"\")'>Delete Response</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.response-textarea'+window.m).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]});
            
            window.m++;
        });
        
        
        $("#add_code_sample_btn").click(function(){

            
            $("#code-sample-field-set").append(
                "<br/>"+
                "<fieldset id=\"code-sample-"+window.n+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Programming Language : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"language["+window.n+"]\" class=\"form-control\">"+
                                "<option value=\"php\">PHP</option>"+
                                "<option value=\"ruby\">RUBY</option>"+
                                "<option value=\"c++\">C++</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Code Introduction : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"code_intro["+window.n+"]\" class=\"form-control code-textarea"+window.n+"\" rows=\"2\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Code : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"code["+window.n+"]\" class=\"form-control code-textarea"+window.n+"\" rows=\"9\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.n+"\" onClick='removeElement(\"code-sample-"+window.n+"\")'>Delete Code Sample</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.code-textarea'+window.n).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]});
            
            window.n++;
        });
     
        
        
        
        
        

    });

    function addChildArgument(argumentId){

                
        $("#child-argument-fieldset-"+argumentId).append(
            "<br/>"+
            "<fieldset id=\"argument"+window.i+"\">"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Name : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Argument Description : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<textarea type=\"text\" name=\"argument_description["+window.i+"]\" class=\"form-control child-argument-textarea"+window.i+"\" rows=\"3\"></textarea>"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Type : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_type["+window.i+"]\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" class=\"form-control\">"+
                            "<option value=\"Required\">Required</option>"+
                            "<option value=\"Optional\">Optional</option>"+
                        "</select>"+
                    "</div>"+
                "</div>"+
                "<br/>"+
                "<div class=\"form-group\">"+
                "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                    "<br/>"+ 
                "</div>"+
                "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+argumentId+"\">"+
                "<div class=\"col-md-6\">"+
                    "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
                "</div>"+
                "<div class=\"col-md-6\">"+
                    "<button type=\"button\" id=\"delete-button-"+window.i+"\" onClick='removeElement(\"argument"+window.i+"\")'>Delete Child Argument</button>"+
                "</div>"+
            "</fieldset>"

        );

        $('.child-argument-textarea'+window.i).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]});
        
        window.i++;
        
        
    }
    
    function addTableCell(tableId){

                
        $("#table-cells-fieldset-"+tableId).append(
            "<br/>"+
            "<fieldset id = \"table-cells-"+window.l+"\">"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Left Cell : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<input type=\"text\" name=\"table_left_cell["+tableId+"][]\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Right Cell : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<textarea type=\"text\" name=\"table_right_cell["+tableId+"][]\" class=\"form-control table-cell-textarea"+window.l+"\" rows=\"3\"></textarea>"+
                    "</div>"+
                "</div>"+
                "<div class=\"col-md-12\">"+
                    "<button type=\"button\" id=\"delete-table-cell-"+window.l+"\" onClick='removeElement(\"table-cells-"+window.l+"\")'>Delete Table Cell</button>"+
                "</div>"+
            "</fieldset>"+
            "<br/>"
            

        );

        $('.table-cell-textarea'+window.l).wysihtml5({"color": true,"stylesheets": ["editer/lib/css/wysiwyg-color.css"]});
        
        window.l++;
        
        
    }
    
    function changeBtnText(buttonId){
        
        var btn_text = "Add Child Argument for ";
        btn_text += $("#argument-input-"+buttonId).val();
        
        $("#button-"+buttonId).text(btn_text);
        
    }
    
    function removeElement(el)
    {
        $("#"+el).remove();

    }
    
    
</script>





@stop
