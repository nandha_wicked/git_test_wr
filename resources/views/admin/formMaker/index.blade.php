@extends('admin_layout')
@section('content')

@if(count($errors) > 0)
    <div style="margin:20px"></div>
    @if(count($errors) > 0)
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
    @endif
@endif

<form method="post" action="/form-maker327e5gbach5FFD/add" class="form-horizontal" name="add-form" id="add-form"> 
        <div class="form-group">
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Page Title : </label>
                    <div class="col-md-5">
                        <input type="text" name="page_title" class="form-control" />
                    </div>
                </div>
            </div>                        
        </div>
    
        <div class="form-group">
            <label class="col-md-3">Components for Index form : </label>
            <fieldset>
                <div id= "index-form-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_indexform_btn" type = "button">Add Index Components</button>
                </div>
            </fieldset>
        </div>
        
        <div class="form-group">
            <label class="col-md-3">Components for Add form : </label>
            <fieldset>
                <div id= "add-form-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_addform_btn" type = "button">Add Components</button>
                </div>
            </fieldset>
        </div>
        <div class="form-group">
            <label class="col-md-3">Components for Edit form : </label>
            <fieldset>
                <div id= "edit-form-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_editform_btn" type = "button">Edit Components</button>
                </div>
            </fieldset>
        </div>
        
        
        <input type="submit" class="btn btn-primary" value="Add Form" />
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
</form>
       



    
    
<script>
    var i = 0;
    
    var j = 0;
    
    $(document).ready(function(){

        
        
        $("#add_indexform_btn").click(function(){

            
            $("#index-form-set").append(
                "<br/>"+
                "<fieldset id=\"index-form-fieldset-"+window.i+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Column Name : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"index_column_name["+window.i+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Column Width : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"index_column_width["+window.i+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Column Value Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"index_column_value_type["+window.i+"]\"  onChange='callForElements(\"index_column_value_type"+window.i+"\")' data-fieldset=\"index_column_value_type_fieldset"+window.i+"\" data-nameprefix=\"index_column_value_elements_"+window.i+"\" id=\"index_column_value_type"+window.i+"\" class=\"form-control\">"+
                                "<option disabled selected>-- Select Type --</option>"+
                                "<option value=\"text\">Text</option>"+
                                "<option value=\"image\">Image</option>"+
                                "<option value=\"button\">Button</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<fieldset id=\"index_column_value_type_fieldset"+window.i+"\">"+
                    "</fieldset>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.i+"\" onClick='removeElement(\"index-form-fieldset-"+window.i+"\")'>Delete Index Component</button>"+
                    "</div>"+
                    "<br/>"+                    
                "</fieldset>"
            
            );
            
            window.i++;
        });
    });
        
        
    function callForElements(selectId)
    {
        
        var type = $('#'+selectId).val();
        var divId = $('#'+selectId).data('fieldset');
        var nameprefix = $('#'+selectId).data('nameprefix');
        
        $("#"+divId).html(""); 
        
        $.ajax({
            type: "GET",
            url: "/api/getElementsForIndexType",
            data: 
            { 
                'type': type
            },
            cache: false,
            success: function(data) {
                
                var elements = data.result.data.elements;
                
                for (var x = 0, len = elements.length; x < len; x++) 
                {
                    if(elements[x].type == "input")
                    {
                        addElementToDiv(divId,"input",elements[x].label,nameprefix+"["+elements[x].label+"]",nameprefix+"_id["+elements[x].label+"]",[]);
                    }
                    else if(elements[x].type == "select") 
                    {
                        addElementToDiv(divId,"select",elements[x].label,nameprefix+"["+elements[x].label+"]",nameprefix+"_id["+elements[x].label+"]",elements[x].values);
                    }
                    
                }
            }
        });
    }    

    function changeBtnText(buttonId){
        
        var btn_text = "Add Child Argument for ";
        btn_text += $("#argument-input-"+buttonId).val();
        
        $("#button-"+buttonId).text(btn_text);
        
    }
    
    function removeElement(el)
    {
        $("#"+el).remove();

    }
    
    function addElementToDiv(divId, type, label, name, id, valueItems)
    {
        if(type == "input")
        {
            var htmlToBeAppended = 
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">"+label+":</label>"+
                    "<div class=\"col-md-9\">"+
                        "<input type=\"text\" name=\""+name+"\" id=\""+id+"\" class=\"form-control\" />"+
                    "</div>"+
                "</div>";
        }
        else if(type == "select")
        {
            var htmlToBeAppended = 
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">"+label+": </label>"+
                    "<div class=\"col-md-7\">"+
                        "<select type=\"text\" name=\""+name+"\" id=\""+id+"\" class=\"form-control\">";
            for (var y = 0, len = valueItems.length; y < len; y++) 
            {
                htmlToBeAppended += "<option value=\""+valueItems[y].value+"\">"+valueItems[y].text+"</option>";
            }
                            
            htmlToBeAppended += 
                        "</select>"+
                    "</div>"+
                "</div>";
        }
        
        $("#"+divId).append(htmlToBeAppended); 
    }
    
    
</script>





@stop