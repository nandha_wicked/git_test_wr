@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Reports Homepage</h2>
		
        
        
             <p>&nbsp;</p>

     
                   
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        
        
        
        <form method="post" action="/admin/a2b_report_page/download" class="form-horizontal" name="report-inPage">
		      		
		      	<div>	
                        <div class="inPageForm">
                            <label class="col-md-5">Start Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker1" name="start_date" class="form-control datepicker" id="startDate" placeholder="Start Date"/>
                            </div>

                        </div>
                        <div class="inPageForm">
                            <label class="col-md-5">End Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker2" name="end_date" class="form-control datepicker" id="endDate" placeholder="End Date"/>
                            </div>

                        </div>
                    
                        
                        
                        
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div align="center" >
                            <input style="width:200px; margin-top:10px" type="submit" class="btn btn-primary" value="Download" />

                        </div>
                </div>
		  </form>
        
        
	</div>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datepicker();
                $('#datetimepicker2').datepicker();
            });
</script>

	
				
	    	
@stop

