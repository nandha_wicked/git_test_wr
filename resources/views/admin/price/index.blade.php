@extends('admin_layout')

@section('content')
	
	<div class="price-pg">
		<h2 class="title">Price Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addPrice-modal"><i class="fa fa-plus"></i> Add Price</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Model</th>
                    <th>Area</th>
					<th>Minimum Hours</th>
                    <th>Weekday Hour</th>
                    <th>Weekend Hour</th>
                    <th>Per Day - Weekday</th>
                    <th>Per Day - Weekend</th>
                    <th>5 Weekdays</th>
                    <th>Week</th>
                    <th>Month</th>
                    <th>Fuel Inc. Per Hour</th>
                    <th>Fuel Inc. Per KM</th>
                    <th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($priceList as $price)
					<tr>
						<td>{{ $price->id }}</td>
						<td>{{ $price->model_name }}</td>
                        <td>{{ $price->area_name }}</td>
						<td>{{ $price->minimum_hours }}</td>
						<td>{{ $price->hour_weekday }}</td>
						<td>{{ $price->hour_weekend }}</td>
						<td>{{ $price->day_weekday }}</td>
						<td>{{ $price->day_weekend }}</td>
						<td>{{ $price->five_weekday }}</td>
						<td>{{ $price->week }}</td>
						<td>{{ $price->month }}</td>
						<td>{{ $price->per_hour_fuel_included_weekday }}</td>
						<td>{{ $price->per_km_fuel_included }}</td>
                        <td>
							@if( $price->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn price_edit_btn" 
							data-id="{{ $price->id }}"
							data-model-id="{{ $price->model_id }}"
							data-model-name="{{ $price->model_name }}"
							data-area-id="{{ $price->area_id }}"
							data-area-name="{{ $price->area_name }}"
							data-wd-gt-eight="{{ $price->price_per_hour }}"
							data-wd-lt-eight="{{ $price->minimum_hours }}"
							data-hour-weekday="{{ $price->hour_weekday }}" 
							data-hour-weekend="{{ $price->hour_weekend }}" 
							data-day-weekday="{{ $price->day_weekday }}" 
							data-day-weekend="{{ $price->day_weekend }}" 
							data-five-weekday="{{ $price->five_weekday }}" 
							data-week="{{ $price->week }}" 
							data-month="{{ $price->month }}"  
							data-perhourfuel="{{ $price->per_hour_fuel_included_weekday }}"  
							data-perkmfuel="{{ $price->per_km_fuel_included }}" 
                            data-status="{{ $price->status }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>



	<script type="text/javascript">
		$('.price_edit_btn').click(function(){
			var id = $(this).data('id');
			var modelId = $(this).data('model-id');
			var modelName = $(this).data('model-name');
			var areaId = $(this).data('area-id');
			var areaName = $(this).data('area-name');
			var minimum_hours = $(this).data('wd-lt-eight');
            
            var hour_weekday = $(this).data('hour-weekday');
            var hour_weekend = $(this).data('hour-weekend');
            var day_weekday = $(this).data('day-weekday');
            var day_weekend = $(this).data('day-weekend');
            var five_weekday = $(this).data('five-weekday');
            var week = $(this).data('week');
            var month = $(this).data('month');
            var perhourfuel = $(this).data('perhourfuel');
            var perkmfuel = $(this).data('perkmfuel');
			var status = $(this).data('status');

			$('#editPrice-modal form').attr("action","/admin/price/edit/"+id);
			$('#editPrice-modal form input[name=modelName]').val(modelName);
			$('#editPrice-modal form input[name=modelId]').val(modelId);
			$('#editPrice-modal form input[name=areaName]').val(areaName);
			$('#editPrice-modal form input[name=areaId]').val(areaId);
			$('#editPrice-modal form input[name=minimum_hours]').val(minimum_hours);
            
            $('#editPrice-modal form input[name=hour_weekday]').val(hour_weekday);
            $('#editPrice-modal form input[name=hour_weekend]').val(hour_weekend);
            $('#editPrice-modal form input[name=day_weekday]').val(day_weekday);
            $('#editPrice-modal form input[name=day_weekend]').val(day_weekend);
            $('#editPrice-modal form input[name=five_weekday]').val(five_weekday);
            $('#editPrice-modal form input[name=week]').val(week);
            $('#editPrice-modal form input[name=month]').val(month);
            $('#editPrice-modal form input[name=perhourfuel]').val(perhourfuel);
            $('#editPrice-modal form input[name=perkmfuel]').val(perkmfuel);
	
            if(status == '1')
				$('#editPrice-modal form #ase1').prop("checked",true);	
			else
				$('#editPrice-modal form #ase2').prop("checked",true);
            
			$('#editPrice-modal').modal('show');
		});
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addPrice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/price/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PRICE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Model ID:</label>
							<div class="col-md-9">
			        			<select name="modelId" class="form-control">
	        					@foreach($modelList as $model)
	        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
	        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area ID:</label>
							<div class="col-md-9">
			        			<select name="areaId" class="form-control">
                                    <option value="0">All</option>
	        					@foreach($areaList as $area)
	        						<option value="{{ $area['id'] }}">{{ $area['area'] }}</option>
	        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Minimum Hours: </label>
							<div class="col-md-9">
		        				<input type="text" name="minimum_hours" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Weekday Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="hour_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Weekend Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="hour_weekend" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Per Day Weekday:</label>
							<div class="col-md-9">
		        				<input type="text" name="day_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Per Day Weekend:</label>
							<div class="col-md-9">
		        				<input type="text" name="day_weekend" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">5 weekdays:</label>
							<div class="col-md-9">
		        				<input type="text" name="five_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Week:</label>
							<div class="col-md-9">
		        				<input type="text" name="week" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Month:</label>
							<div class="col-md-9">
		        				<input type="text" name="month" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Fuel Inc. Per Hour:</label>
							<div class="col-md-9">
		        				<input type="text" name="perhourfuel" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Fuel Inc. Per KM:</label>
							<div class="col-md-9">
		        				<input type="text" name="perkmfuel" class="form-control" />
		        			</div>
		        		</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editPrice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT PRICE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Model:</label>
							<div class="col-md-9">
		        				<input type="text" name="modelName" class="form-control" disabled="disabled" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaName" class="form-control" disabled="disabled" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Minimum Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="minimum_hours" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Weekday Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="hour_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Weekend Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="hour_weekend" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Per Day Weekday:</label>
							<div class="col-md-9">
		        				<input type="text" name="day_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Per Day Weekend:</label>
							<div class="col-md-9">
		        				<input type="text" name="day_weekend" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">5 weekdays:</label>
							<div class="col-md-9">
		        				<input type="text" name="five_weekday" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Week:</label>
							<div class="col-md-9">
		        				<input type="text" name="week" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Month:</label>
							<div class="col-md-9">
		        				<input type="text" name="month" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Fuel Inc. Per Hour:</label>
							<div class="col-md-9">
		        				<input type="text" name="perhourfuel" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Fuel Inc. Per KM:</label>
							<div class="col-md-9">
		        				<input type="text" name="perkmfuel" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
			        		<label class="col-md-3">Price Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="priceStatus" id="ase1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="priceStatus" id="ase2" value="0"> Inactive
								</label>
							</div>
						</div>
		        		
		        		<input type="hidden" name="modelId" class="form-control" />
		        		<input type="hidden" name="areaId" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
