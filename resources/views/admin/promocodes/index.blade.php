@extends('admin_layout')

@section('content')
	
	<div class="cancelled-table">
		<h2 class="title">Promo Codes Homepage</h2>
        
		<button class="btn btn-primary" data-toggle="modal" data-target="#addPromoCodeModal"><i class="fa fa-plus"></i> Add Promo Code</button>
        
        <p>&nbsp;</p>
        
        <form id="custom-search-form" method="get" action="/admin/promocode/search" class="form-search form-horizontal pull-left">
                        <div class="input-append span12">
                            <input type="text" name = "code" class="search-query mac-style" placeholder="Search">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                    </form>
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table" id="all-promocodes" style="table-layout: fixed; word-wrap:break-word;">
			<thead>
				<tr>
					<th>Id</th>
					<th>Code</th>
					<th>Title</th>
					<th>Description</th>
					<th>Value Type</th>
					<th>Value</th>
					<th>Total Count</th>
					<th>Available Count</th>
					<th>Start Set</th>
					<th>End Set</th>
					<th>New User</th>
					<th>Min Order</th>
					<th>Areas</th>
					<th>Bike Models</th>
					<th>WeekDay</th>
					<th>WeekEnd</th>
					<th>Dates</th>
					<th>Device</th>
                    <th>Max Discount</th>
                    <th>Weekday Exempt</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($promoCodes as $promoCode)
					<tr>
						<td>{{ $promoCode->id }}</td>
						<td>{{ $promoCode->code }}</td>
						<td>{{ $promoCode->title }}</td>
						<td>{{ $promoCode->description }}</td>
						<td>{{ $promoCode->value_type }}</td>
						<td>{{ $promoCode->value }}</td>
						<td>{{ $promoCode->total_count }}</td>
						<td>{{ $promoCode->available_count }}</td>
						<td>{{ $promoCode->start_set }}</td>
						<td>{{ $promoCode->end_set }}</td>
						<td>{{ $promoCode->newUserTxt }}</td>
						<td>{{ $promoCode->min_order }}</td>
						<td>{{ $promoCode->areaNames }}</td>
						<td>{{ $promoCode->modelNames }}</td>
						<td>{{ $promoCode->weekdayTxt }}</td>
						<td>{{ $promoCode->weekendTxt }}</td>
						<td>{{ $promoCode->dates }}</td>
						<td>{{ $promoCode->device }}</td>
                        <td>{{ $promoCode->max_value }}</td>
                        <td>{{ $promoCode->exemptTxt }}</td>
						<td>{{ $promoCode->statusTxt }}</td>
						<td>
							<button class="btn btn-info wk-btn promocode_edit_btn" data-id="{{ $promoCode->id }}" 
								data-code="{{ $promoCode->code }}" data-title="{{ $promoCode->title }}" 
								data-description="{{ $promoCode->description }}" data-value-type="{{ $promoCode->value_type }}" data-value="{{ $promoCode->value }}" data-total-count="{{ $promoCode->total_count }}" data-available-count="{{ $promoCode->available_count }}" data-start="{{ $promoCode->start_set }}" data-end="{{ $promoCode->end_set }}" data-new-user="{{ $promoCode->new_user }}" data-min-order="{{ $promoCode->min_order }}" data-area-ids="{{ $promoCode->area_ids }}" data-model-ids="{{ $promoCode->model_ids }}" data-weekday="{{ $promoCode->weekday }}" data-weekend="{{ $promoCode->weekend }}" data-dates="{{ $promoCode->dates }}" data-device="{{ $promoCode->device }}" data-status="{{ $promoCode->status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$(function(){
			PromoCode.init();
	    });
	</script>
@stop

@section('model')
	<div class="modal fade" id="editPromoCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="edit-promocode">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit PromoCode</h4>
		      		</div>
		      		<div class="modal-body">
		      			@if (count($errors->edit) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
		      			<div class="form-group">
							<label class="col-md-3">Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="code" class="form-control" placeholder="Alpanumeric code, no special character" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Title:</label>
							<div class="col-md-9">
		        				<input type="text" name="title" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description:</label>
							<div class="col-md-9">
		        				<textarea name="description" class="form-control" rows="4" ></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
			        			<div class="col-md-12">
			        				<div class="col-md-3 col-md-offset-3">
			        					<label> Date Range:</label>
			        					<input type="radio" name="range-or-specific" value="1" checked="checked">
			        				</div>
			        				<div class="col-md-3">
			        					<label> Specific Dates:</label>
			        					<input type="radio" name="range-or-specific" value="0">
			        				</div>
			        			</div>
		        			</div>
		        		</div>	
		        		<div class="dates-range">
		        			<div class="form-group">
								<label class="col-md-3">Start Date & Time:</label>
								<div class="col-md-5">
			        				<input type="text" name="start_date" class="form-control" id="startDate" placeholder="Start Date"/>
			        			</div>
								<div class="col-md-3">
			        				<input type="text" name="start_time" class="form-control" id="startTime" placeholder="Start Time"/>
			        			</div>
			        		</div>
			        		<div class="form-group">
								<label class="col-md-3">End Date & Time:</label>
								<div class="col-md-5">
			        				<input type="text" name="end_date" class="form-control" id="endDate" placeholder="End Date"/>
			        			</div>
								<div class="col-md-3">
			        				<input type="text" name="end_time" class="form-control" id="endTime" placeholder="End Time"/>
			        			</div>
			        		</div>
			        		<div id="date-error-edit"></div>
			        		<div class="form-group">
								<label class="col-md-3">Weekday :</label>
				        		<div class="col-md-3"> 
					        		<label class="radio-inline">
										<input type="radio" name="weekday" value="1"> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="weekday" value="0"> No
									</label>
								</div>
								<label class="col-md-3">Weekend :</label>
				        		<div class="col-md-3"> 
					        		<label class="radio-inline">
										<input type="radio" name="weekend" value="1"> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="weekend" value="0"> No
									</label>
								</div>
			        		</div>
		        		</div>	        		
		        		<div class="form-group specific-date">
							<label class="col-md-3">Dates:</label>
							<div class="col-md-9">
								<input type="text" name="dates" class="form-control" placeholder="Comma separated dates e.g 2016-01-01, 2016-01-05"/>
							</div>
						</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Count:</label>
							<div class="col-md-3">
		        				<input type="text" name="total_count" class="form-control" placeholder="e.g 10" readonly />
		        			</div>
		        			<label class="col-md-3">Available Count:</label>
							<div class="col-md-3">
		        				<input type="text" name="available_count" class="form-control" placeholder="e.g 2" readonly />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Value Type:</label>
							<div class="col-md-9">
								<select name="value_type" class="form-control">
		        					<option value="RUPEES">RUPEES</option>
		        					<option value="PERCENTAGE">PERCENTAGE</option>
                                    <option value="DAYS">DAYS</option>
                                    <option value="HOURS">HOURS</option>
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="col-md-3">Value:</label>
							<div class="col-md-3">
		        				<input type="text" name="value" class="form-control" placeholder="e.g 25"/>
		        			</div>
		        			<label class="col-md-3">Min Order:</label>
							<div class="col-md-3">
		        				<input type="text" name="min_order" class="form-control" placeholder="e.g 1000"/>
		        			</div>
						</div>
                        <div class="form-group">
		        			<label class="col-md-3">Max Discount:</label>
							<div class="col-md-3">
		        				<input type="text" name="max_value" class="form-control" placeholder="e.g 1000"/>
		        			</div>
		        			
						</div>
                        <div class="form-group">
			        		<label class="col-md-3">Exempt from Weekday Low Prices :</label>
			        		<div class="col-md-3"> 
				        		<label class="radio-inline">
									<input type="radio" name="weekday_exempt" value="1"> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" name="weekday_exempt" value="0"> No
								</label>
							</div>
						</div>
						<div class="form-group">
			        		<label class="col-md-3">New User :</label>
			        		<div class="col-md-3"> 
				        		<label class="radio-inline">
									<input type="radio" name="new_user" value="1"> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" name="new_user" value="0"> No
								</label>
							</div>
						</div>
						
		        		<div class="form-group">
		        			<label class="col-md-3">Bike Models:</label>
		        			<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 model-dropdown">
				        			@foreach ($makes as $make)
										<li><a href="#" class="small make-name" data-value="{{ $make->id }}" tabIndex="-1"><input type="checkbox" class="" name="make-name" value=""/>&nbsp;{{ $make->bike_make }}</a>
											<ul>
												<?php foreach ($make['models'] as $model) { ?>
													<li><a href="#" class="small model-name" data-value="{{ $model->id }}" tabIndex="-1"><input type="checkbox" class="model-ids" name="model_ids[]" value="{{ $model->id }}" />&nbsp;{{ $model->bike_model }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Areas:</label>
							<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 area-dropdown">
				        			@foreach ($cities as $city)
										<li><a href="#" class="small city-name" data-value="{{ $city->id }}" tabIndex="-1"><input type="checkbox" name="city-name" value=""/>&nbsp;{{ $city->city }}</a>
											<ul>
												<?php foreach ($city['areas'] as $area) { ?>
													<li><a href="#" class="small" tabIndex="-1"><input type="checkbox" name="area_ids[]" class="area-ids" value="{{ $area->id }}"/>&nbsp;{{ $area->area }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>		
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Device:</label>
							<div class="col-md-9">
		        				<select name="device" class="form-control">
		        					<option value="WEB">WEB</option>
		        					<option value="MOBILE">MOBILE</option>
		        					<option value="BOTH">BOTH</option>
		        				</select>
		        			</div>
						</div>
		        		<div class="form-group">
		        			<label class="col-md-3">Status :</label>
		        			<div class="col-md-9"> 
				        		<label class="radio-inline">
									<input type="radio" name="status" value="1"> Active
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" value="0"> Inactive
								</label>
							</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="addPromoCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/promocode/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add PromoCode</h4>
		      		</div>
		      		<div class="modal-body">
		      			<div class="form-group">
							<label class="col-md-3">Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="code" class="form-control" placeholder="Alpanumeric code, no special character"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Title:</label>
							<div class="col-md-9">
		        				<input type="text" name="title" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description:</label>
							<div class="col-md-9">
		        				<textarea name="description" class="form-control" rows="4" /></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
			        			<div class="col-md-12">
			        				<div class="col-md-3 col-md-offset-3">
			        					<label> Date Range:</label>
			        					<input type="radio" name="range-or-specific" value="1" checked="checked" >
			        				</div>
			        				<div class="col-md-3">
			        					<label> Specific Dates:</label>
			        					<input type="radio" name="range-or-specific" value="0">
			        				</div>
			        			</div>
		        			</div>
		        		</div>
		        		<div class="dates-range">
		        			<div class="form-group">
								<label class="col-md-3">Start Date & Time:</label>
								<div class="col-md-5">
			        				<input type="text" name="start_date" class="form-control" id="addPcStartDate" placeholder="Start Date"/>
			        			</div>
								<div class="col-md-3">
			        				<input type="text" name="start_time" class="form-control" id="addPcStartTime" placeholder="Start Time"/>
			        			</div>
			        		</div>
			        		<div class="form-group">
								<label class="col-md-3">End Date & Time:</label>
								<div class="col-md-5">
			        				<input type="text" name="end_date" class="form-control" id="addPcEndDate" placeholder="End Date"/>
			        			</div>
								<div class="col-md-3">
			        				<input type="text" name="end_time" class="form-control" id="addPcEndTime" placeholder="End Time"/>
			        			</div>
			        		</div>
			        		<div id="date-error-add"></div>
			        		<div class="form-group">
								<label class="col-md-3">Weekday :</label>
				        		<div class="col-md-3"> 
					        		<label class="radio-inline">
										<input type="radio" name="weekday" value="1"> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="weekday" value="0"> No
									</label>
								</div>
								<label class="col-md-3">Weekend :</label>
				        		<div class="col-md-3"> 
					        		<label class="radio-inline">
										<input type="radio" name="weekend" value="1"> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="weekend" value="0"> No
									</label>
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group specific-date">
		        			<label class="col-md-3">Dates:</label>
							<div class="col-md-9">
		        				<input type="text" name="dates" class="form-control" placeholder="Comma separated dates e.g 2016-01-01, 2016-01-05"/>
		        			</div>
						</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Count:</label>
							<div class="col-md-3">
		        				<input type="text" name="total_count" class="form-control" placeholder="e.g 10"/>
		        			</div>
		        			<label class="col-md-3">Available Count:</label>
							<div class="col-md-3">
		        				<input type="text" name="available_count" class="form-control"  readonly/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Value Type:</label>
							<div class="col-md-9">
		        				<select name="value_type" class="form-control">
		        					<option value="RUPEES">RUPEES</option>
		        					<option value="PERCENTAGE">PERCENTAGE</option>
                                    <option value="DAYS">DAYS</option>
                                    <option value="HOURS">HOURS</option>
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="col-md-3">Value:</label>
							<div class="col-md-3">
		        				<input type="text" name="value" class="form-control" placeholder="e.g 25"/>
		        			</div>
		        			<label class="col-md-3">Min Order:</label>
							<div class="col-md-3">
		        				<input type="text" name="min_order" class="form-control" placeholder="e.g 1000"/>
		        			</div>
						</div>
                        <div class="form-group">
		        			<label class="col-md-3">Max Discount:</label>
							<div class="col-md-3">
		        				<input type="text" name="max_value" class="form-control" placeholder="e.g 1000"/>
		        			</div>
		        			
						</div>
                        <div class="form-group">
			        		<label class="col-md-3">Exempt from Weekday Low Prices :</label>
			        		<div class="col-md-3"> 
				        		<label class="radio-inline">
									<input type="radio" name="weekday_exempt" value="1"> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" name="weekday_exempt" value="0"> No
								</label>
							</div>
						</div>
						<div class="form-group">
			        		<label class="col-md-3">New User :</label>
			        		<div class="col-md-3"> 
				        		<label class="radio-inline">
									<input type="radio" name="new_user" value="1"> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" name="new_user" value="0"> No
								</label>
							</div>
						</div>
						
		        		<div class="form-group">
		        			<label class="col-md-3">Bike Models:</label>
		        			<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 model-dropdown">
				        			@foreach ($makes as $make)
										<li><a href="#" class="small make-name" data-value="{{ $make->id }}" tabIndex="-1"><input type="checkbox" class="" name="make-name" value=""/>&nbsp;{{ $make->bike_make }}</a>
											<ul>
												<?php foreach ($make['models'] as $model) { ?>
													<li><a href="#" class="small model-name" data-value="{{ $model->id }}" tabIndex="-1"><input class="model-ids" type="checkbox" name="model_ids[]" value="{{ $model->id }}" />&nbsp;{{ $model->bike_model }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Area Ids:</label>
							<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9">
				        			@foreach ($cities as $city)
										<li><a href="#" class="small city-name" data-value="{{ $city->id }}" tabIndex="-1"><input type="checkbox" class="" name="city-name" value=""/>&nbsp;{{ $city->city }}</a>
											<ul>
												<?php foreach ($city['areas'] as $area) { ?>
													<li><a href="#" class="small" tabIndex="-1"><input type="checkbox" class="area-ids" name="area_ids[]" value="{{ $area->id }}"/>&nbsp;{{ $area->area }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>		
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Device:</label>
							<div class="col-md-9">
		        				<select name="device" class="form-control">
		        					<option value="BOTH">BOTH</option>
		        					<option value="WEB">WEB</option>
		        					<option value="MOBILE">MOBILE</option>
		        				</select>
		        			</div>
						</div>
		        		<div class="form-group">
		        			<label class="col-md-3">Status :</label>
		        			<div class="col-md-9"> 
				        		<label class="radio-inline">
									<input type="radio" name="status" value="1"> Active
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" value="0"> Inactive
								</label>
							</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
@stop
