@extends('admin_layout')
@section('content')

@if(count($errors) > 0)
    @if ($errors->first() == "API Call has been added successfully")
        <script>
            alert("{{$errors->first()}}");
        </script>
    @else
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
    @endif
@endif


<script>
var apiCallJson = JSON.parse('{!! $json !!}');
</script>


<form method="post" action="/admin/api-call/edit/{{$id}}" class="form-horizontal" name="add-call" id="add-call"> 
        <div class="form-group">
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Name : </label>
                    <div class="col-md-5">
                        <input type="text" name="call_name" class="form-control" id="call_name"/>
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Category : </label>
                    <div class="col-md-5">
                        <select name="call_category" id="call_category" class="form-control" id="call_category">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category }}</option>
                            @endforeach
                        </select>
                        
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call URL : </label>
                    <div class="col-md-5">
                        <input type="text" name="call_url" class="form-control" id="call_url"/>
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Type : </label>
                    <div class="col-md-5">
                        <select name="call_type" id="call_type" class="form-control" id="call_type">
                            <option value="GET">GET</option>
                            <option value="POST">POST</option>
                            <option value="PUT">PUT</option>
                            <option value="DELETE">DELETE</option>
                            <option value="PATCH">PATCH</option>
                            <option value="HEAD">HEAD</option>
                            <option value="OPTIONS">OPTIONS</option>
                            <option value="CONNECT">CONNECT</option>
                        </select>
                        
                    </div>
                </div>
            </div>
            
            <div style="display:block; text-align: center;">
                <div class="form-group">
                    <label class="col-md-3">Call Description : </label>
                    <div class="col-md-7">
                        <textarea type="text" name="call_description" rows="3" class="form-control textarea" id="call_description"></textarea>
                    </div>
                </div>
            </div>
                        
        </div>
    
        <button type="button" class="btn btn-primary rearrange_list_btn">Rearrange API</button>
        
        
        <div class="form-group">
            <fieldset>
                <div id= "argument-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_argument_btn" type = "button">Add Argument</button>
                </div>
            </fieldset>
        </div>
        <div class="form-group">
            <fieldset>
                <div id= "box-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_box_btn" type = "button">Add Message Box</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "table-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_table_btn" type = "button">Add Table</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "response-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_response_btn" type = "button">Add Example Response</button>
                </div>
            </fieldset>
        </div>
    
        <div class="form-group">
            <fieldset>
                <div id= "code-sample-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_code_sample_btn" type = "button">Add Code Sample</button>
                </div>
            </fieldset>
        </div>
        
        <input type="submit" class="btn btn-primary" value="Save Changes" />
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
</form>
       



    
    
<script>
    var i = 0;
    
    var j = 0;
    
    var k = 0;
    
    var l = 0;
    
    var m = 0;
    
    var n = 0;
    
    var parent = [];
    
    $(document).ready(function(){

                

        $("#add_argument_btn").click(function(){

            
            $("#argument-field-set").append(
                "<br/>"+
                "<fieldset id=\"argument"+window.i+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Name : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Argument Description : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"argument_description["+window.i+"]\" id = \"argument-description-"+window.i+"\" class=\"form-control argument-textarea"+window.i+"\" rows=\"3\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_type["+window.i+"]\" id = \"argument-type-"+window.i+"\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" id = \"argument-required_or_optional-"+window.i+"\" class=\"form-control\">"+
                                "<option value=\"Required\">Required</option>"+
                                "<option value=\"Optional\">Optional</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<br/>"+
                    "<div class=\"form-group\">"+
                    "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                        "<br/>"+ 
                    "</div>"+
                    "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+window.i+"\">"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.i+"\" onClick='removeElement(\"argument"+window.i+"\")'>Delete Argument</button>"+
                    "</div>"+
                "</fieldset>"
            
            );
            
            $('.argument-textarea'+window.i).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]}); 
            
            window.i++;
                        
            
        });
        
        
        $("#add_box_btn").click(function(){

            
            $("#box-field-set").append(
                "<br/>"+
                "<fieldset id=\"message-box-"+window.j+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Message Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"message_type["+window.j+"]\" id = \"message-type-"+window.j+"\" class=\"form-control\">"+
                                "<option value=\"blue\">Info</option>"+
                                "<option value=\"red\">Alert</option>"+
                                "<option value=\"green\">Success</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Message : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<textarea type=\"text\" name=\"message["+window.j+"]\" id = \"message-message-"+window.j+"\" class=\"form-control box-textarea"+window.j+"\" rows=\"3\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.j+"\" onClick='removeElement(\"message-box-"+window.j+"\")'>Delete Message Box</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.box-textarea'+window.j).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
            
            window.j++;
        });
        
        
        
        $("#add_table_btn").click(function(){

            
            $("#table-field-set").append(
                "<br/>"+
                "<fieldset id=\"table"+window.k+"\">"+
                   
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Table Title : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"table_title["+window.k+"]\" id = \"table-input-"+window.k+"\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Table Description : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<textarea type=\"text\" name=\"table_description["+window.k+"]\" id = \"table-description-"+window.k+"\" class=\"form-control table-textarea"+window.k+"\" rows=\"2\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Left Title : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"table_left_title["+window.k+"]\" id = \"table-left-title-"+window.k+"\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Right Title : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"table_right_title["+window.k+"]\" id = \"table-right-title-"+window.k+"\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    
                    "<br/>"+
                    "<div class=\"form-group\">"+
                    "<div id = \"table-cells-fieldset-"+window.k+"\"></div>"+
                        "<br/>"+ 
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"button-"+window.k+"\" onClick='addTableCell(\""+window.k+"\")'>Add Cells</button>"+
                    "</div>"+
                    "<div class=\"col-md-6\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.k+"\" onClick='removeElement(\"table"+window.k+"\")'>Delete Table</button>"+
                    "</div>"+
                "</fieldset>"
            
            );
            
            $('.table-textarea'+window.k).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
            
            window.k++;
                        
            
        });
        
        
        $("#add_response_btn").click(function(){

            
            $("#response-field-set").append(
                "<br/>"+
                "<fieldset id=\"response-"+window.m+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Response Introduction : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"response_intro["+window.m+"]\" id = \"response-description-"+window.m+"\"  class=\"form-control response-textarea"+window.m+"\" rows=\"2\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Response : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"response["+window.m+"]\"  id = \"response-response-"+window.m+"\" class=\"form-control response-textarea"+window.m+"\" rows=\"9\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.m+"\" onClick='removeElement(\"response-"+window.m+"\")'>Delete Response</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.response-textarea'+window.m).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
            
            window.m++;
        });
        
        
        $("#add_code_sample_btn").click(function(){

            
            $("#code-sample-field-set").append(
                "<br/>"+
                "<fieldset id=\"code-sample-"+window.n+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Programming Language : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"language["+window.n+"]\" id = \"code-sample-input-"+window.n+"\" class=\"form-control\">"+
                                "<option value=\"php\">PHP</option>"+
                                "<option value=\"ruby\">RUBY</option>"+
                                "<option value=\"c++\">C++</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Code Introduction : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"code_intro["+window.n+"]\" id = \"code-sample-intro-"+window.n+"\" class=\"form-control code-textarea"+window.n+"\" rows=\"2\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-3\">Code : </label>"+
                        "<div class=\"col-md-9\">"+
                            "<textarea type=\"text\" name=\"code["+window.n+"]\" id = \"code-sample-code-"+window.n+"\" class=\"form-control code-textarea"+window.n+"\" rows=\"9\"></textarea>"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"col-md-12\">"+
                        "<button type=\"button\" id=\"delete-button-"+window.n+"\" onClick='removeElement(\"code-sample-"+window.n+"\")'>Delete Code Sample</button>"+
                    "</div>"+
                    "<br/>"+                    
                    
                "</fieldset>"
            
            );
            
            $('.code-textarea'+window.n).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
            
            window.n++;
        });
     
        
        
        
        
        

    });

    function addChildArgument(argumentId){

                
        $("#child-argument-fieldset-"+argumentId).append(
            "<br/>"+
            "<fieldset id=\"argument"+window.i+"\">"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Name : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Argument Description : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<textarea type=\"text\" name=\"argument_description["+window.i+"]\" id = \"argument-description-"+window.i+"\" class=\"form-control child-argument-textarea"+window.i+"\" rows=\"3\"></textarea>"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Type : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_type["+window.i+"]\" id = \"argument-type-"+window.i+"\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" id = \"argument-required_or_optional-"+window.i+"\" class=\"form-control\">"+
                            "<option value=\"Required\">Required</option>"+
                            "<option value=\"Optional\">Optional</option>"+
                        "</select>"+
                    "</div>"+
                "</div>"+
                "<br/>"+
                "<div class=\"form-group\">"+
                "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                    "<br/>"+ 
                "</div>"+
                "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+argumentId+"\">"+
                "<div class=\"col-md-6\">"+
                    "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
                "</div>"+
                "<div class=\"col-md-6\">"+
                    "<button type=\"button\" id=\"delete-button-"+window.i+"\" onClick='removeElement(\"argument"+window.i+"\")'>Delete Child Argument</button>"+
                "</div>"+
            "</fieldset>"

        );

        $('.child-argument-textarea'+window.i).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
        
        window.i++;
        
        
    }
    
    function addTableCell(tableId){

                
        $("#table-cells-fieldset-"+tableId).append(
            "<br/>"+
            "<fieldset id = \"table-cells-"+window.l+"\">"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Left Cell : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<input type=\"text\" name=\"table_left_cell["+tableId+"][]\" id = \"table-cell-left-title-"+window.l+"\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-3\">Right Cell : </label>"+
                    "<div class=\"col-md-9\">"+
                        "<textarea type=\"text\" name=\"table_right_cell["+tableId+"][]\" id = \"table-cell-right-title-"+window.l+"\" class=\"form-control table-cell-textarea"+window.l+"\" rows=\"3\"></textarea>"+
                    "</div>"+
                "</div>"+
                "<div class=\"col-md-12\">"+
                    "<button type=\"button\" id=\"delete-table-cell-"+window.l+"\" onClick='removeElement(\"table-cells-"+window.l+"\")'>Delete Table Cell</button>"+
                "</div>"+
            "</fieldset>"+
            "<br/>"
            

        );

        $('.table-cell-textarea'+window.l).wysihtml5({"color": true,"stylesheets": ["/editer/lib/css/wysiwyg-color.css"]});
        
        window.l++;
        
        
    }
    
    function changeBtnText(buttonId){
        
        var btn_text = "Add Child Argument for ";
        btn_text += $("#argument-input-"+buttonId).val();
        
        $("#button-"+buttonId).text(btn_text);
        
    }
    
    function removeElement(el)
    {
        $("#"+el).remove();

    }
    
    
</script>

<script>
    $(document).ready(function(){
        $("#call_name").val(apiCallJson.call_title);
        $("#call_category").val(apiCallJson.call_category);
        $("#call_description").val(apiCallJson.call_description);
        $("#call_url").val(apiCallJson.call_url);
        $("#call_type").val(apiCallJson.call_method);
        
        
        var arguments = apiCallJson.arguments;
        
        for(arg in arguments)
        {
            if(!arguments[arg].child_argument)
            {
                $("#add_argument_btn").click();
                
                var index = window.i;
                index = index-1;
                
                
                $("#argument-input-"+index).val(arguments[arg].argument);
                $("#argument-description-"+index).val(arguments[arg].description);
                $("#argument-required_or_optional-"+index).val(arguments[arg].required_or_optional);
                $("#argument-type-"+index).val(arguments[arg].type);
                
                window.parent[arguments[arg].id] = index;
                   
            }
            else
            {
                addChildArgument(window.parent[arguments[arg].parent_argument]);
                var index = window.i;
                index = index-1;
                
                
                $("#argument-input-"+index).val(arguments[arg].argument);
                $("#argument-description-"+index).val(arguments[arg].description);
                $("#argument-required_or_optional-"+index).val(arguments[arg].required_or_optional);
                $("#argument-type-"+index).val(arguments[arg].type);
                
                window.parent[arguments[arg].id] = index;
            }
        }
        
        var tables = apiCallJson.tables;
        
        for(tableIndex in tables)
        {
            $("#add_table_btn").click();
            var index = window.k;
            index = index-1;


            $("#table-input-"+index).val(tables[tableIndex].title);
            $("#table-description-"+index).val(tables[tableIndex].description);
            $("#table-left-title-"+index).val(tables[tableIndex].left_title);
            $("#table-right-title-"+index).val(tables[tableIndex].right_title);
            
            for(tableCellIndex in tables[tableIndex].table_cells)
            {
                addTableCell(index);
                
                cellIndex = window.l-1;
                
                $("#table-cell-left-title-"+cellIndex).val(tables[tableIndex].table_cells[tableCellIndex].title);
                $("#table-cell-right-title-"+cellIndex).val(tables[tableIndex].table_cells[tableCellIndex].description);
            }
            
        }
        
        var responses = apiCallJson.responses;
        
        for(responseIndex in responses)
        {
            $("#add_response_btn").click();
            var index = window.m;
            index = index-1;


            $("#response-response-"+index).val(responses[responseIndex].response);
            $("#response-description-"+index).val(responses[responseIndex].description);
            
            
        }
        
        var messages = apiCallJson.messages;
        
        for(messageIndex in messages)
        {
            $("#add_box_btn").click();
            var index = window.j;
            index = index-1;


            $("#message-message-"+index).val(messages[messageIndex].message);
            $("#message-type-"+index).val(messages[messageIndex].type);
            
            
        }
        
        
        
        
    });

</script>

<script>
    $('.rearrange_list_btn').click(function(){
			$('#rearrange-api-modal').modal('show');
    });
    
    var el = document.getElementById('apis_to_reaarange');
    var sortable = Sortable.create(el);
    
</script>



@stop

@section('model')

    <div class="modal fade" id="rearrange-api-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="background-color: linen;">
				<form method="post" action="/admin/rearrange-api/{{ $apiCallDetails['id'] }}">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Rearrange APIs</h4>
		      		</div>
                    @if(count($apiCallDetails['arguments']) > 0)
                        <div style="text-align:center;"><h4>Arguments</h4></div>
                    
                        <ul style="list-style: none;" class="rearrage-list" id="argumentList">
                            @foreach($apiCallDetails['arguments'] as $argument)
                                @if($argument['child_argument_offset'] == 0)
                                    <li>{{ $argument['argument'] }}<input type="hidden" value="{{ $argument['id'] }}" name="argument[]"><input type="hidden" value="{{$argument['id']}}" name="api_arg[]">
                                        <?php $reverseApiArguments = array_reverse($apiCallDetails['arguments']); ?>
                                        @foreach($apiCallDetails['arguments'] as $argumentChild)
                                            @if($argumentChild['id'] != $argumentChild['parent_argument'])
                                                @if($argumentChild['parent_argument'] == $argument['id'])
                                                    <ul style="list-style: none;" class="rearrage-child-list" id="argumentList{{ $argumentChild['id'] }}">
                                                        <li style="margin-left:20px;">{{ $argumentChild['argument'] }}<input type="hidden" value="{{ $argumentChild['id'] }}" name="argumentChild[{{ $argumentChild['parent_argument'] }}][]"><input type="hidden" value="{{$argumentChild['id']}}" name="api_arg[]">
                                                            @foreach($apiCallDetails['arguments'] as $argument2Child)
                                                                @if($argument2Child['id'] != $argument2Child['parent_argument'])
                                                                    @if($argument2Child['parent_argument'] == $argumentChild['id'])
                                                                        <ul style="list-style: none;" class="rearrage-child2-list" id="argumentList{{ $argument2Child['id'] }}">
                                                                            <li style="margin-left:40px;">{{ $argument2Child['argument'] }}<input type="hidden" value="{{ $argument2Child['id'] }}" name="argument2Child[{{ $argument2Child['parent_argument'] }}][]">
                                                                            <input type="hidden" value="{{$argument2Child['id']}}" name="api_arg[]"></li>
                                                                        </ul>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </li>
                                                    </ul>
                                                @endif
                                            @endif
                                        @endforeach
                            
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    
                    @endif
                    
                    
                    @if(count($apiCallDetails['tables']) > 0)
                        
                    
                        @foreach($apiCallDetails['tables'] as $table)
                            <div style="text-align:center;"><h4>Table - {{ $table->title }} </h4></div>
                            <ul style="list-style: none;" class="rearrage-list" id="tableList{{ $table->id }}">        
                               @foreach($table['table_cells'] as $cell)
                                    <li style="margin-left:40px; margin-right:40px;">{{ $cell['title'] }}
                                        <input type="hidden" value="{{ $cell['id'] }}" name="cell[{{ $table['id'] }}][]">
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    @endif
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Save Changes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

<script>  
    
    @if(count($apiCallDetails['arguments']) > 0)
        var el = document.getElementById('argumentList');
        Sortable.create(el,{
            group: {
                name:"argumentList",
                pull:true,
                put:true
            },
            animation: 150,
            ghostClass: 'ghost',

        });
    @endif
    
    @if(count($apiCallDetails['arguments']) > 0)
        @foreach($apiCallDetails['arguments'] as $argument)
            @if($argument['child_argument_offset'] == 20 || $argument['child_argument_offset'] == 40)
                var el = document.getElementById('argumentList{{ $argument['id'] }}');
                Sortable.create(el,{
                    group: {
                        name:"argumentList{{ $argument['parent_argument'] }}",
                        pull:true,
                        put:true
                    },
                    animation: 150,
                    ghostClass: 'ghost',

                });
            @endif
        @endforeach
    @endif
    
    @if(count($apiCallDetails['tables']) > 0)
        @foreach($apiCallDetails['tables'] as $table)
            var el = document.getElementById('tableList{{ $table->id }}');
            Sortable.create(el,{
                group: {
                    name:"tableList{{ $table->id }}",
                    pull:true,
                    put:true
                },
                animation: 150,
                ghostClass: 'ghost',

            });
        @endforeach
    @endif
       
</script>

@stop
