@extends('admin_layout')

@section('content')
	
	<div class="bikeMake-pg">
		<h2 class="title">BikeMake Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeMake-modal"><i class="fa fa-plus"></i> Add BikeMake</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>BikeMake</th>
					<th>Logo</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikeMakeList as $bikeMake)
					<tr>
						<td>{{ $bikeMake->id }}</td>
						<td>{{ $bikeMake->bike_make }}</td>
						<td>
							<?php if($bikeMake->logo_id){ 
								$image = App\Models\Image::find($bikeMake->logo_id);
							?>
								<img src="http:{{ $image['full'] }}" width ="50px" />
							<?php } else { 
								echo "No Image";
								}
							?>		
						</td>
						<td>
							@if( $bikeMake->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bikeMake_edit_btn" data-id="{{ $bikeMake->id }}" 
								data-bikemake="{{ $bikeMake->bike_make }}" data-status="{{ $bikeMake->status }}">
								<i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>	

	<script type="text/javascript">
		$('.bikeMake_edit_btn').click(function(){
			var id = $(this).data('id');
			var bikeMake = $(this).data('bikemake');
			//var logo_url = $(this).data('logo-url');
			var status = $(this).data('status');
				
			$('#editBikeMake-modal form').attr("action","/admin/bikeMake/edit/"+id);
			$('#editBikeMake-modal form input[type=text]').val(bikeMake);
			if(status == '1')
				$('#editBikeMake-modal form #bmse1').prop("checked",true);	
			else
				$('#editBikeMake-modal form #bmse2').prop("checked",true);
			$('#editBikeMake-modal').modal('show');
		});
	</script>

@stop	

@section('model')
	
	<div class="modal fade" id="addBikeMake-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/bikeMake/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD BIKE MAKE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">BikeMake :</label>
							<div class="col-md-9">
		        				<input type="text" name="bikeMake" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">New Logo :</label>
							<div class="col-md-3">
		        				<input type="file" name="logoUpload">
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeMakeStatus" id="bmsa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeMakeStatus" id="bmsa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeMake-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT BIKEMAKE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">BikeMake :</label>
							<div class="col-md-9">
		        				<input type="text" name="bikeMake" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Add New Logo :</label>
							<div class="col-md-3">
								<input type="file" name="logoUpload" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeMakeStatus" id="bmse1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeMakeStatus" id="bmse2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	