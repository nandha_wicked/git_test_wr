@extends('admin_layout')

@section('content')
	
	<div class="contactEnquiry-pg">
		<h2 class="title">ContactEnquiry Homepage</h2>
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Mobile Number</th>
					<th>Enquiry</th>
				</tr>
			</thead>
			<tbody>
				@foreach($contactEnquiryList as $enq)
					<tr>
						<td>{{ $enq->id }}</td>
						<td>{{ $enq->first_name }}</td>
						<td>{{ $enq->last_name }}</td>
						<td>{{ $enq->email }}</td>
						<td>{{ $enq->mobile_num }}</td>
						<td>{{ $enq->enquiry }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@stop		