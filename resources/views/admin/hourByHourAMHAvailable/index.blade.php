@inject('bikeObj','App\Models\Bike')
@inject('bookingObj','App\Models\Booking')

@extends('admin_layout')

@section('content')

	<div class="bikeFilter-pg">
		<h2 class="title">Bike Availability Filter Homepage</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeMake-modal"><i class="fa fa-plus"></i> Add BikeMake</button> -->

		<h3>Select:</h3>
		<form class="form-inline" method="post" action="/admin/check-availability-HBH-filter">
            
			<div class="form-group">
		    	<input type="text" name="start_date" class="form-control" id="startDateFilter" <?php if($start_date!="") echo "value = ".$start_date; else echo "placeholder=\"Start Date\""; ?> autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="start_time" class="form-control" id="startTimeFilter" <?php if($start_time!="") echo "value = ".$start_time; else echo "placeholder=\"Start Time\""; ?> autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="end_date" class="form-control" id="endDateFilter" <?php if($end_date!="") echo "value = ".$end_date; else echo "placeholder=\"End Date\""; ?> autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="end_time" class="form-control" id="endTimeFilter" <?php if($end_time!="") echo "value = ".$end_time; else echo "placeholder=\"End Time\""; ?> autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="area_id" class="form-control">
		  		@foreach($activeAreas as $area)
		  			<option value="{{ $area->id }}" <?php if($area_id == $area->id) echo "selected"; ?>>{{ $area->area }}</option>
		  		@endforeach
		  		</select>
		  	</div>
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <button type="submit" class="btn btn-success">Filter</button>
		</form>
        <br/>

		

		<table class="fixedCol wk-table" style="table-layout: fixed; width:{{ $width }}px">
            
			<thead>
				<tr>
                        <th rowspan="2" style="width: 200px !important;">Bike Model</th>
                        <th rowspan="2" style="width: 110px !important;">Availability</th>
					@foreach($date_hour_read_format as $date_hour_read_format_element)
		  			     <th colspan="{{ $date_hour_read_format_element['count'] }}">{{ $date_hour_read_format_element['date'] }}</th>
		  		    @endforeach
                    
				</tr>
                
                <tr>
                    @foreach($date_hour_read_format as $date_hour_read_format_element)
                         @foreach($date_hour_read_format_element['hours'] as $date_hour_read_format_element_hour)
                            <th style="text-align:center; ">{{ $date_hour_read_format_element_hour }}</th>
                         @endforeach
		  		    @endforeach
                    
                </tr>
			</thead>
			<tbody>
                @foreach($model_data_array as $model_data_array_element)
                <tr>
                        <td style="width: 200px !important;">{{ $model_data_array_element['model_name'] }}</td>
                        <td style="width: 110px !important;">{{ $model_data_array_element['percentage'] }}%</td>
                    
                         @foreach($model_data_array_element['aorc'] as $model_data_array_element_aorc)
                            <td style="text-align:center; background: {{ $model_data_array_element_aorc['color'] }};">{{ $model_data_array_element_aorc['count'] }}</td>
                         @endforeach
                </tr>
		        @endforeach
			</tbody>
		</table>

	
	</div>	

<script type="text/javascript">
	$(function() {
		Backend.BikeFilterAMH.init();
	});
</script>

<script>
	// $('#filter-form button').click(function(){
	// 	var areaName = $('#filter-form select[name=areaId] option:selected').text();
	// 	$('#filter-form input[name=areaName]').val(areaName);
	// 	console.log($('#filter-form input[name=areaName]').val());
	// });
</script>	

@stop	
