@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">BikeModel Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add BikeModel</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>BikeMake</th>
					<th>BikeModel</th>
					<th>Description</th>
					<th>Cover Image</th>
					<th>Thumbnail Image</th>
					<th>Gallery Images</th>
					<th>Spec1 Key</th>
					<th>Spec1 Value</th>
					<th>Spec2 Key</th>
					<th>Spec2 Value</th>
					<th>Spec3 Key</th>
					<th>Spec3 Value</th>
					<th>Spec4 Key</th>
					<th>Spec4 Value</th>
                    <th>Priority</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikeModelList as $bikeModel)
					<tr>
						<td>{{ $bikeModel->id }}</td>
						<td>{{ $bikeModel->getBikeMakeName($bikeModel->bike_make_id) }}</td>
						<td>{{ $bikeModel->bike_model }}</td>
						<td><div class="model-description">{{ $bikeModel->description }}</div></td>
						<td><img src="http:{{ $bikeModel['cover_img'] }}" width ="50px" /></td>
						<td><img src="http:{{ $bikeModel['thumb_img'] }}" width ="50px" /></td>
						<td>
							<button class="btn btn-default view-model-gallery" data-toggle="modal"  data-model-id="{{ $bikeModel['id'] }}">View Gallery</button>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecKey($bikeModel->spec1)) 
									echo $bikeModel->getBikeModelSpecKey($bikeModel->spec1);
								else
									echo "No Spec Key";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecValue($bikeModel->spec1)) 
									echo $bikeModel->getBikeModelSpecValue($bikeModel->spec1);
								else
									echo "No Spec Value";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecKey($bikeModel->spec2)) 
									echo $bikeModel->getBikeModelSpecKey($bikeModel->spec2);
								else
									echo "No Spec Key";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecValue($bikeModel->spec2)) 
									echo $bikeModel->getBikeModelSpecValue($bikeModel->spec2);
								else
									echo "No Spec Value";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecKey($bikeModel->spec3)) 
									echo $bikeModel->getBikeModelSpecKey($bikeModel->spec3);
								else
									echo "No Spec Key";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecValue($bikeModel->spec3)) 
									echo $bikeModel->getBikeModelSpecValue($bikeModel->spec3);
								else
									echo "No Spec Value";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecKey($bikeModel->spec4)) 
									echo $bikeModel->getBikeModelSpecKey($bikeModel->spec4);
								else
									echo "No Spec Key";
							?>
						</td>
						<td>
							<?php 
								if($bikeModel->getBikeModelSpecValue($bikeModel->spec4)) 
									echo $bikeModel->getBikeModelSpecValue($bikeModel->spec4);
								else
									echo "No Spec Value";
							?>
						</td>
                        <td>
                            {{ $bikeModel->priority }}
                        </td>
						<td>
							@if( $bikeModel->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn" data-thumbnail-img="http:{{ $bikeModel['thumb_img'] }}" data-cover-img="http:{{ $bikeModel['cover_img'] }}"	data-id="{{ $bikeModel->id }}" data-bikemakeid="{{ $bikeModel->bike_make_id }}" data-bikemodel="{{ $bikeModel->bike_model }}" data-status="{{ $bikeModel->status }}" data-description="{{ $bikeModel->description }}" data-priority="{{ $bikeModel->priority }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-info wk-btn bikeModel_spec_btn" data-id="{{ $bikeModel->id }}" 
							data-spec1key="{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec1) }}"
							data-spec1value="{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec1) }}"
							data-spec2key="{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec2) }}"
							data-spec2value="{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec2) }}"
							data-spec3key="{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec3) }}"
							data-spec3value="{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec3) }}"
							data-spec4key="{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec4) }}"
							data-spec4value="{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec4) }}"   
							> Spec</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      BikeModel.init();
	    });
        
        
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/bikeModel/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD BIKEMODEL</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">BikeMake :</label>
							<div class="col-md-9">
		        				<select name="bikeMake" class="form-control">
		        					@foreach($bikeMakeList as $bikeMake)
		        						<option value="{{ $bikeMake->id }}">{{ $bikeMake->bike_make }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">BikeModel :</label>
							<div class="col-md-9">
		        				<input type="text" name="bikeModel" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Add New CoverImage :</label>
							<div class="col-md-3">
		        				<input type="file" name="coverImgUpload" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Add New Thumbnail Image :</label>
							<div class="col-md-3">
		        				<input type="file" name="thumbnailImgUpload" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Add New Image(s) in Gallery:</label>
							<div class="col-md-3">
		        				<input type="file" name="galleryImgUpload[]" multiple="" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeModelStatus" id="bmosa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeModelStatus" id="bmosa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT BIKEMODEL</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">BikeMake :</label>
							<div class="col-md-9">
		        				<select name="bikeMake" class="form-control" disabled>
		        					@foreach($bikeMakeList as $bikeMake)
		        						<option value="{{ $bikeMake->id }}">{{ $bikeMake->bike_make }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">BikeModel :</label>
							<div class="col-md-9">
		        				<input type="text" name="bikeModel" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" name="modelDesc"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New CoverImage :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverImgUpload" />
				        			</div>
		        				</div>
		        				<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			<label class="col-md-6">Old CoverImage :</label>
									<div class="col-md-6">
				        				<div id="old_coverImage_holder"></div>
				        			</div>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add Thumbanil Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="thumbnailImgUpload" />
				        			</div>
				        		</div>
				        		<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			<label class="col-md-6">Old Thumbnail Image :</label>
									<div class="col-md-6">
				        				<div id="old_thumbnailImage_holder"></div>
				        			</div>
				        		</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add Image(s) in Gallery:</label>
									<div class="col-md-6">
				        				<input type="file" name="galleryImgUpload[]" multiple="" />
				        			</div>
				        		</div>
				        		<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			<label class="col-md-6"></label>
									<div class="col-md-6">
				        				<div id="old_gallery1Image_holder"></div>
				        			</div>
				        		</div>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Priority :</label>
							<div class="col-md-9">
		        				<input type="text" name="priority" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeModelStatus" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeModelStatus" id="bmose2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">BIKEMODEL TECHNICAL SPECS</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="text" name="spec1Key" class="form-control" placeholder="Spec1 Key" />
							</div>
							<div class="col-md-1">:</div>
							<div class="col-md-7">
								<textarea name="spec1Value" class="form-control" rows="4" placeholder="Spec1 Value"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<div class="col-md-4">
								<input type="text" name="spec2Key" class="form-control" placeholder="Spec2 Key" />
							</div>
							<div class="col-md-1">:</div>
							<div class="col-md-7">
								<textarea name="spec2Value" class="form-control" rows="4" placeholder="Spec2 Value"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<div class="col-md-4">
								<input type="text" name="spec3Key" class="form-control" placeholder="Spec3 Key" /> 
							</div>
							<div class="col-md-1">:</div>
							<div class="col-md-7">
								<textarea name="spec3Value" class="form-control" rows="4" placeholder="Spec3 Value"></textarea>
		        			</div>
						</div>
						<div class="form-group">
			        		<div class="col-md-4">
								<input type="text" name="spec4Key" class="form-control" placeholder="Spec4 Key" />
							</div>
							<div class="col-md-1">:</div>
							<div class="col-md-7">
								<textarea name="spec4Value" class="form-control" rows="4" placeholder="Spec4 Value"></textarea>
		        			</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="view-gallery-modal" tabindex="-1" role="dialog" aria-labelledby="galleryModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Gallery Images</h4>
				</div>
				<div class="modal-body">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgdmlld0JveD0iMCAwIDkwMCA1MDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzkwMHg1MDAvYXV0by8jNzc3OiM1NTUvdGV4dDpGaXJzdCBzbGlkZQpDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48IVtDREFUQVsjaG9sZGVyXzE1MGIyYWEyNmI4IHRleHQgeyBmaWxsOiM1NTU7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6NDVwdCB9IF1dPjwvc3R5bGU+PC9kZWZzPjxnIGlkPSJob2xkZXJfMTUwYjJhYTI2YjgiPjxyZWN0IHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBmaWxsPSIjNzc3Ii8+PGc+PHRleHQgeD0iMzA4LjI4OTA2MjUiIHk9IjI3MC4xIj5GaXJzdCBzbGlkZTwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" alt="...">
						    </div>
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <!-- <i class="fa fa-chevron-left"></i> -->
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						    <!-- <i class="fa fa-chevron-right"></i> -->
						  </a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<input type="hidden" name="model_id" value="">
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#view-gallery-modal').on('show.bs.modal', function(e) {
		    var modelId = $(e.relatedTarget).data('model-id');
		    $(e.currentTarget).find('input[name="model_id"]').val(modelId);
		});
	</script>	

@stop	
