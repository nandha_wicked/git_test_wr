@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">OwnerBlocks Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addOwnerBlockModal"><i class="fa fa-plus"></i> Add OwnerBlock</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:80px">
			<thead>
				<tr>
					
					<th style="width: 100px !important;">ID</th>
                    <th style="width: 100px !important;">Aggregator</th>
                    <th style="width: 110px !important;">Model</th>
                    <th style="width: 140px !important;">Start Date</th>
                    <th style="width: 140px !important;">End Date</th>
                    <th style="width: 80px !important;">Booking ID</th>    
                    <th style="width: 100px !important;">Delete</th>
				
				</tr>
			</thead>
			<tbody>
				@foreach($ownerBlockList as $ownerBlock)
					<tr>
						
                        <td>{{$ownerBlock->id}}</td>
                        <td>{{$ownerBlock->getAggregatorName()}}</td>
                        <td>{{$ownerBlock->getModelName()}}</td>
                        <td>{{$ownerBlock->start_datetime}}</td>
                        <td>{{$ownerBlock->end_datetime}}</td>
                        <td>{{ $ownerBlock->booking_id }}</td>
                        
                        <td>
							<button class="btn btn-danger wk-btn ownerBlock_delete_btn" data-id="{{ $ownerBlock->id }}"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.ownerBlock_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteOwnerBlock-modal form').attr("action","/admin/ownerBlock/delete/"+id);
			$('#deleteOwnerBlock-modal').modal('show');
		});
    
       
    
</script>

	

@stop

@section('model')
	
	<div class="modal fade" id="deleteOwnerBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete OwnerBlock</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this OwnerBlock?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addOwnerBlockModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/ownerBlock/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add OwnerBlock</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						
		        		
		        		<div class="form-group">
							<label class="col-md-3">Aggregator:</label>
							<div class="col-md-9">
		        				<select name="aggregator_id" id="aggregator_id" class="form-control">
                                    <option disabled selected> --Select-- </option>
		        					@foreach($aggregatorList as $aggregator)
		        						<option value="{{ $aggregator->id }}">{{ $aggregator->name }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Booking ID:</label>
							<div class="col-md-9">
		        				<input type="text" name="booking_id" class="form-control" />
		        			</div>
		        		</div>
                        
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

   


@stop
