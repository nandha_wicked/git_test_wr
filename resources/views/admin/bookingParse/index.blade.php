@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBooking-modal"><i class="fa fa-plus"></i> Add Booking</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>					
					<th>reference_id</th>    
                    <th>area</th>
					<th>bike_model</th>
					<th>total_price</th>
					<th>user_first_name</th>
					<th>user_last_name</th>
					<th>start_datetime</th>
					<th>phone_number</th>
					<th>pg_txn_id</th>
					<th>pg_mode</th>
					<th>pg_status</th>
					<th>status</th>
					<th>note</th>
					<th>pg_txn_amount</th>
                    <th>order_id</th>
                    <th>email</th>
                    <th>end_datetime</th>
                    <th>promocode</th>
                    <th>Created At</th>
                    <th>Weekday Hours</th>
                    <th>Weekend Hours</th>
                    <th>Full Price</th>
              
				</tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
                       
                        
                        <td>{{ $booking->reference_id }}</td>
                        <td>{{ $booking->getAreaName($booking->area_id) }}</td>
                        <td>{{ $booking->getBikeModelName($booking->model_id) }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getUserName($booking->user_id) }}</td>
                        <td>{{ $booking->getUserLastName($booking->user_id) }}</td>
                        <td>{{ $booking->start_datetime }}</td>
                        <td>{{ $booking->getUserMobileNum($booking->user_id) }}</td>
                        <td>{{ $booking->pg_txn_id }}</td>
                        <td>{{ $booking->pg_mode }}</td>
                        <td>{{ $booking->pg_status }}</td>
                        <td>{{ $booking->status }}</td>
                        <td>{{ $booking->note }}</td>
                        <td>{{ $booking->pg_txn_amount }}</td>
                        <td>{{ $booking->order_id }}</td>
                        <td>{{ $booking->getUserEmail($booking->user_id) }}</td>
                        <td>{{ $booking->end_datetime }}</td>
                        <td>{{ $booking->promocode }}</td>
                        <td>{{ $booking->created_at }}</td>
                        <td>{{ $booking->weekday_hours }}</td>
                        <td>{{ $booking->weekend_hours }}</td>
                        <td>{{ $booking->full_price }}</td>
             
 
                        
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

@stop


