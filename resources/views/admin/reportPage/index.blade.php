@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Reports Homepage</h2>
		
        
        
             <p>&nbsp;</p>

     
                   
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        
        
        
        <form method="post" action="/admin/report_page/download" class="form-horizontal" name="report-inPage">
		      		
		      	<div>	
                        <div class="inPageForm">
                            <label class="col-md-5">Start Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker1" name="start_date" class="form-control datepicker" id="startDate" placeholder="Start Date"/>
                            </div>

                        </div>
                        <div class="inPageForm">
                            <label class="col-md-5">End Date & Time:</label>
                            <div class="col-md-5">
                                <input type="text" id="datetimepicker2" name="end_date" class="form-control datepicker" id="endDate" placeholder="End Date"/>
                            </div>

                        </div>
                    
                        <div class="inPageForm">
							<label class="col-md-3"></label>
							<div class="col-md-9">
		        				<select name="date_mode" id ="date_mode" class="form-control">
		        						<option value="created_at">Bookings that were created during the selected period</option>
                                        <option value="end_date">Bookings that ended during the selected period</option>
                                        <option value="begin_date">Bookings that began during the selected period</option>
                                        <option value="revenue_end_date">Revenue Bookings that ended during the selected period</option>
                                        <option value="revenue_begin_date">Revenue Bookings that began during the selected period</option>
		        				</select>
		        			</div>
		        		</div>
                        <h5 class="col-md-12" style="margin:10px;">Default fields are Start Date, End Date, Created At, Model, Area, Total Price, User Name, Promocode</h5>


                        <h5 class="col-md-12" style="margin:10px;">Select additional fields</h5>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="user_details" value="yes"/><label class="col-md-8">User Details</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="erp_delivery" value="yes"/><label class="col-md-8">ERP delivery Status</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="erp_return" value="yes"/><label class="col-md-8">ERP return Status</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="weekday_weekend_hours" value="yes"/><label class="col-md-8">Weekend and Weeday Hours</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="notes" value="yes"/><label class="col-md-8">Customer Instructions</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="payment_info" value="yes"/><label class="col-md-8">Payment Informatoin</label>
                            </div>
                        </div>
                        <div class="inPageForm">
                            <div class="col-md-12">
                                <input type="checkbox" class="col-md-5" name="deleted_booking" value="yes"/><label class="col-md-8">Deleted Bookings</label>
                            </div>
                        </div>

                        
                        
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div align="center" >
                            <input style="width:200px; margin-top:10px" type="submit" class="btn btn-primary" value="Download" />

                        </div>
                </div>
		  </form>
        
        
	</div>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datepicker();
                $('#datetimepicker2').datepicker();
            });
</script>

	
				
	    	
@stop

