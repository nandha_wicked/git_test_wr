@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Aggregated Bikes Homepage</h2>
        
        
        <form method="post" action="/admin/calculatePayout">

                        <select name="month" class="form-control">
                            <option value="not_selected" disabled selected>-- Select Month --</option>
                            <option value="August 2016">August 2016</option>
                            <option value="September 2016">September 2016</option>
                            <option value="October 2016">October 2016</option>
                            <option value="November 2016">November 2016</option>
                            <option value="December 2016">December 2016</option>
                            <option value="January 2017">January 2017</option>
                            <option value="February 2017">February 2017</option>
                            <option value="March 2017">March 2017</option>
                            <option value="April 2017">April 2017</option>
                            <option value="May 2017">May 2017</option>
                            <option value="June 2017">June 2017</option>
                            <option value="July 2017">July 2017</option>
                            
                        </select>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		
		      			<button class="btn btn-primary "> Show</button>
		        		
        </form>
        
        
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
        
        
        
        
        
	</div>



	

@stop

