@extends('admin_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBooking-modal"><i class="fa fa-plus"></i> Add Booking</button>
        
                <p>&nbsp;</p>

        
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <form id="custom-search-form" method="get" action="/admin/booking/search" class="form-search form-horizontal pull-right">
                        <div class="input-append span12">
                            <input type="text" name = "code" class="search-query mac-style" placeholder="Search">
                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Start Date</th>
					<th>Start Time</th>
					<th>End Date</th>
					<th>End Time</th>
					<th>Customer</th>
					<th>Bike</th>
					<th>Area</th>
					<th>Customer Instruction</th>
					<th>Amount Paid</th>
                    <th>CreatedBy</th>
                	<th>Action</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['id'] }}</td>
						<td>{{ $booking['start_date'] }}</td>
						<td>{{ $booking['start_time'] }}</td>
						<td>{{ $booking['end_date'] }}</td>
						<td>{{ $booking['end_time'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserEmail($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
						<td>
							<button class="btn btn-danger wk-btn booking_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.booking_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBooking-modal form').attr("action","/admin/booking/delete/"+id);
			$('#deleteBooking-modal').modal('show');
		});
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/booking/add" onsubmit="Backend.Booking.validateBookingForm();" class="form-horizontal" name="add-booking" id="add-booking">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD Booking</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Start Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="startDate" class="form-control" id="startDate" placeholder="Start Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="startTime" class="form-control" id="startTime" placeholder="Start Time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">End Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="endDate" class="form-control" id="endDate" placeholder="End Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="endTime" class="form-control" id="endTime" placeholder="End Time"/>
		        			</div>
		        		</div>
		        		<div id="date-error"></div>
		        		<div class="form-group">
							<label class="col-md-3">Customer:</label>
							<div class="col-md-9">
		        				<select name="customer" class="form-control">
		        					@foreach($userList as $user)
		        						<option value="{{ $user->id }}">{{ $user->first_name }} - {{ $user->email }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Model & Area:</label>
							<div class="col-md-4">
		        				<select name="model" class="form-control">
		        					@foreach($models as $model)
		        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        			<div class="col-md-4">
		        				<select name="area" class="form-control">
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer Instruction:</label>
							<div class="col-md-9">
		        				<textarea name="note" class="form-control" rows="4" /></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Amount Paid:</label>
							<div class="col-md-9">
		        				<input type="text" name="amount" class="form-control" />
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div id="date-error"></div>
		      		<div class="modal-footer">
		      			<input type="submit" class="btn btn-primary" value="Add" />
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this booking?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<script type="text/javascript">
		$(function(){
			Backend.Booking.init();
			
		});
	</script>
@stop	
