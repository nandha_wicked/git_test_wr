@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">GCM Homepage</h2>
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<br/>
        <br/>
        
                
        <form method="post" action="/admin/gcm/push" class="form-inline">
            
                
                    <label class="col-md-3">User Type:</label>
                    <div class="col-md-9">
                        <select name="user_type" id="user_type" class="form-control">
                            <option value=0>All Users</option>
                            <option value=1>All Registered Users</option>
                            <option value=2>All Unregistered Users</option>
                            <option value=3>Test Users</option>
                        </select>
                    </div>
            
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param1"  class="form-control" placeholder="Key 1">
                            <input type ="text" name="value1" class="form-control" placeholder="Value 1">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param2"  class="form-control" placeholder="Key 2">
                            <input type ="text" name="value2" class="form-control" placeholder="Value 2">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param3"  class="form-control" placeholder="Key 3">
                            <input type ="text" name="value3" class="form-control" placeholder="Value 3">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param4"  class="form-control" placeholder="Key 4">
                            <input type ="text" name="value4" class="form-control" placeholder="Value 4">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param5"  class="form-control" placeholder="Key 5">
                            <input type ="text" name="value5" class="form-control" placeholder="Value 5">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param6"  class="form-control" placeholder="Key 6">
                            <input type ="text" name="value6" class="form-control" placeholder="Value 6">
                    </div>
                    <div class="row" style="height: 40px; margin-left: 20px">
                            <input type ="text" name="param7"  class="form-control" placeholder="Key 7">
                            <input type ="text" name="value7" class="form-control" placeholder="Value 7">
                    </div>
            
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
        </form>
               

	

@stop
