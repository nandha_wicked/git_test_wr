@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">EmailPromoLink Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addEmailPromoLink-modal"><i class="fa fa-plus"></i> Add EmailPromoLink</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Link</th>
                    <th>Image</th>
                    <th>Description</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($emailPromoLinkList as $emailPromoLink)
					<tr>
						<td>{{ $emailPromoLink->url }}</td>
                        <td><img src="{{ $emailPromoLink->image }}" width ="200px" /></td>
                        <td>{{ $emailPromoLink->description }}</td>
						<td>
							@if( $emailPromoLink->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn emailPromoLink_edit_btn" data-nid="{{ $emailPromoLink->id }}" data-url="{{ $emailPromoLink->url }}" data-image="{{ $emailPromoLink->image }}" data-description="{{ $emailPromoLink->description }}" data-status="{{ $emailPromoLink->status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-danger wk-btn emailPromoLink_delete_btn" data-nid="{{ $emailPromoLink->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
    

	<script type="text/javascript">
		$('.emailPromoLink_edit_btn').click(function(){
			var nid = $(this).data('nid');
			var url = $(this).data('url');
            var description = $(this).data('description');
			var status = $(this).data('status');
            var image = $(this).data('image');
            var ImgHolder = '<img src="'+image+'" width="50px" />';
				
			$('#editEmailPromoLink-modal form').attr("action","/admin/emailPromoLink/edit/"+nid);
            $('#editEmailPromoLink-modal form textarea[name=description]').data("wysihtml5").editor.setValue(description);
            $('#editEmailPromoLink-modal form input[name="url"]').val(url);
            $('#editEmailPromoLink-modal form #old_coverImage_holder').html(ImgHolder);
			if(status == '1')
				$('#editEmailPromoLink-modal form #ps1').prop("checked",true);	
			else
				$('#editEmailPromoLink-modal form #ps2').prop("checked",true);
			$('#editEmailPromoLink-modal').modal('show');
		});

		$('.emailPromoLink_delete_btn').click(function(){
			var nid = $(this).data('nid');
			$('#deleteEmailPromoLink-modal form').attr("action","/admin/emailPromoLink/delete/"+nid);
			$('#deleteEmailPromoLink-modal').modal('show');
		});
	</script>

@stop


@section('model')
	
	<div class="modal fade" id="addEmailPromoLink-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/emailPromoLink/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Latest EmailPromoLink</h4>
		      		</div>
                    
		      		<div class="modal-body">
                        <div class="form-group">
							<label class="col-md-3">Link :</label>
							<div class="col-md-9">
		        				<input type="text" name="url" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverImgUpload" />
				        			</div>
		        				</div>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="description" id="description"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
                            EmailPromoLink status : 
                            <label class="radio-inline">
                              <input type="radio" name="publishStatus" id="radio1" value="1"> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="publishStatus" id="radio2" value="0"> Inactive
                            </label>
                        </div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editEmailPromoLink-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Latest EmailPromoLink</h4>
		      		</div>
                        <div class="modal-body">
		        		    <div class="form-group">
							 <label class="col-md-3">Link :</label>
							 <div class="col-md-9">
		        				  <input type="text" name="url" class="form-control"/>
                                </div>
		        		    </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="col-md-6">Add New Image :</label>
                                        <div class="col-md-6">
                                            <input type="file" name="coverImgUpload" />
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-6">
                                        <label class="col-md-6">Old CoverImage :</label>
                                        <div class="col-md-6">
                                            <div id="old_coverImage_holder"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">Description :</label>
                                <div class="col-md-9">
                                    <textarea class="form-control textarea" rows="3" name="description"></textarea>
                                </div>
		        		    </div>
                            <div class="form-group">
                                EmailPromoLink status : 
                                <label class="radio-inline">
                                  <input type="radio" name="publishStatus" id="ps1" value="1"> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="publishStatus" id="ps2" value="0"> Inactive
                                </label>
                            </div>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
                        
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="deleteEmailPromoLink-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Latest EmailPromoLink</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this emailPromoLink?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop
