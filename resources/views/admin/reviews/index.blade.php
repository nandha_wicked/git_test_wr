@extends('admin_layout')

@section('content')
	
	<div class="reviews-pg">
		<h2 class="title">Reviews Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addReviews-modal"><i class="fa fa-plus"></i> Add Reviews</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
                    <th>Name</th>
					<th>Reviews</th>
                    <th>Reviewed On</th>
					<th>Image</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($reviewsList as $reviews)
					<tr>
						<td>{{ $reviews->id }}</td>
                        <td>{{ $reviews->name }}</td>
						<td>{{ $reviews->review }}</td>
                        <td>{{ $reviews->reviewed_on }}</td>
						<td>
							<?php if($reviews->image_id){ 
								$image = App\Models\Image::find($reviews->image_id);
							?>
								<img src="http:{{ $image['full'] }}" width ="50px" />
							<?php } else { 
								echo "No Image";
								}
							?>		
						</td>
						<td>
							@if( $reviews->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn reviews_edit_btn" data-id="{{ $reviews->id }}" 
								data-name="{{ $reviews->name }}" data-review="{{ $reviews->review }}" data-reviewedon="{{ $reviews->reviewed_on }}" data-status="{{ $reviews->status }}">
								<i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>	

	<script type="text/javascript">
		$('.reviews_edit_btn').click(function(){
			var id = $(this).data('id');
            var name = $(this).data('name');
			var review = $(this).data('review');
			var reviewedon = $(this).data('reviewedon');
            
            
			//var image_url = $(this).data('image-url');
			var status = $(this).data('status');
				
			$('#editReviews-modal form').attr("action","/admin/reviews/edit/"+id);
			$('#editReviews-modal form input[name=name]').val(name);
			$('#editReviews-modal form input[name=reviews]').val(review);
			$('#editReviews-modal form input[name=reviewedon]').val(reviewedon);	
            
            if(status == '1')
                $('#editReviews-modal form #bmse1').prop("checked",true);	
			else
				$('#editReviews-modal form #bmse2').prop("checked",true);
			$('#editReviews-modal').modal('show');
		});
	</script>

@stop	

@section('model')
	
	<div class="modal fade" id="addReviews-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/reviews/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD REVIEWS</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Reviews :</label>
							<div class="col-md-9">
		        				<input type="text" name="reviews" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Reviewed On :</label>
							<div class="col-md-9">
		        				<input type="text" name="reviewedon" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">New Image :</label>
							<div class="col-md-3">
		        				<input type="file" name="imageUpload">
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="publishStatus" id="bmsa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="publishStatus" id="bmsa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editReviews-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Review</h4>
		      		</div>
		      		<div class="modal-body">
                        <div class="form-group">
							<label class="col-md-3">Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" class="form-control" />
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Reviews :</label>
							<div class="col-md-9">
		        				<input type="text" name="reviews" class="form-control" />
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Reviewed On :</label>
							<div class="col-md-9">
		        				<input type="text" name="reviewedon" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Add New Image :</label>
							<div class="col-md-3">
								<input type="file" name="imageUpload" />
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="publishStatus" id="bmse1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="publishStatus" id="bmse2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	