@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">EmailSubscriber Homepage</h2>
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				@foreach($subscribeList as $subscribe)
					<tr>
						<td>{{ $subscribe->id }}</td>
						<td>{{ $subscribe->email }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@stop

