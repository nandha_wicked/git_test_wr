
@extends('admin_layout')

@section('content')

	<div class="cancelled-table">

		<h4>The following are the WPP enquiries :</h4>
		<table class="fixedCol wk-table  wk-table3" id="sort-table" style="table-layout: fixed; width:1610px">
           
			<thead>
                
                
				<tr>
                    <th  style="width: 50px !important;">ID</th>
					<th  style="width: 120px !important;">Name</th>
                    <th  style="width: 150px !important;">Email</th>
                    <th  style="width: 70px !important;">Phone</th>
					<th  style="width: 50px !important;">No. of Bikes</th>
					<th  style="width: 40px !important;">Loan Needed</th>
                    <th  style="width: 200px !important;">Bikes Interested</th>
					<th  style="width: 200px !important; ">Notes</th>
                    <th  style="width: 130px !important;">Updated By</th>
                    <th  style="width: 50px !important;">Edit Note</th>
                    <th  style="width: 110px !important;">Created At</th>
				</tr>
			</thead>
			<tbody>
                
                @foreach($enquiryList as $enquiry)
					<tr>
                        <td>{{ $enquiry->id }}</td>
						<td>{{ $enquiry->fullName }}</td>
						<td>{{ $enquiry->email }}</td>
						<td>{{ $enquiry->mobileNumber }}</td>
						<td>{{ $enquiry->bikes_count }}</td>
						<td>{{ $enquiry->loanNeeded() }}</td>
                        <td>{{ $enquiry->bikeinterested }}</td>
                        <td style= "padding:2px;"><pre style="margin-bottom:0px;">{{ $enquiry->notes }}</pre></td>
                        <td>{{ $enquiry->updated_by }}</td>
						<td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $enquiry->id }}"
							data-note="{{ $enquiry->notes }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
                        <td>{{ $enquiry->created_at }}</td>
					</tr>
				@endforeach
                
			</tbody>
		</table>
    </div>

<script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/admin/updateNotes/wpp_enquiry/notes/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>

@stop	


@section('model')
	
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
