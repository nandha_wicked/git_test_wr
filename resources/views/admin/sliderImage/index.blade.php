@extends('admin_layout')

@section('content')
	
	<div class="news-pg">
		<h2 class="title">Slider Image Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addSliderImage-modal"><i class="fa fa-plus"></i> Add Slider Image</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Link</th>
                    <th>Image</th>
                    <th>Mobile Image</th>
                    <th>Description</th>
                    <th>Button Description</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($sliderImageList as $sliderImage)
					<tr>
						<td>{{ $sliderImage->url }}</td>
                        <td><img src="{{ $sliderImage->image }}" width ="200px" /></td>
                        <td><img src="{{ $sliderImage->mobile_image }}" width ="200px" /></td>
                        <td>{{ $sliderImage->description }}</td>
                        <td>{{ $sliderImage->button_description }}</td>
						<td>
							@if( $sliderImage->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn sliderImage_edit_btn" data-nid="{{ $sliderImage->id }}" data-url="{{ $sliderImage->url }}" data-image="{{ $sliderImage->image }}" data-mobileimage="{{ $sliderImage->mobile_image }}" data-description="{{ $sliderImage->description }}" data-buttondescription="{{ $sliderImage->button_description }}" data-status="{{ $sliderImage->status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-danger wk-btn sliderImage_delete_btn" data-nid="{{ $sliderImage->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
    

	<script type="text/javascript">
		$('.sliderImage_edit_btn').click(function(){
			var nid = $(this).data('nid');
			var url = $(this).data('url');
            var description = $(this).data('description');
            var buttondescription = $(this).data('buttondescription');
			var status = $(this).data('status');
            var image = $(this).data('image');
            var mobileimage = $(this).data('mobileimage');
            var ImgHolder = '<img src="'+image+'" width="50px" />';
            var MobileImgHolder = '<img src="'+mobileimage+'" width="50px" />';
				
			$('#editSliderImage-modal form').attr("action","/admin/sliderImage/edit/"+nid);
            $('#editSliderImage-modal form textarea[name=description]').data("wysihtml5").editor.setValue(description);
            $('#editSliderImage-modal form textarea[name=button_description]').data("wysihtml5").editor.setValue(buttondescription);
            $('#editSliderImage-modal form input[name="url"]').val(url);
            $('#editSliderImage-modal form #old_coverImage_holder').html(ImgHolder);
            $('#editSliderImage-modal form #old_coverMobileImage_holder').html(MobileImgHolder);
			if(status == '1')
				$('#editSliderImage-modal form #ps1').prop("checked",true);	
			else
				$('#editSliderImage-modal form #ps2').prop("checked",true);
			$('#editSliderImage-modal').modal('show');
		});

		$('.sliderImage_delete_btn').click(function(){
			var nid = $(this).data('nid');
			$('#deleteSliderImage-modal form').attr("action","/admin/sliderImage/delete/"+nid);
			$('#deleteSliderImage-modal').modal('show');
		});
	</script>

@stop


@section('model')
	
	<div class="modal fade" id="addSliderImage-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/sliderImage/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Latest Slider Image</h4>
		      		</div>
                    
		      		<div class="modal-body">
                        <div class="form-group">
							<label class="col-md-3">Link :</label>
							<div class="col-md-9">
		        				<input type="text" name="url" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverImgUpload" />
				        			</div>
		        				</div>
		        			</div>
		        		</div>
                        <div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Mobile Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="coverMobileImgUpload" />
				        			</div>
		        				</div>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="description" id="description"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Button Description :</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="button_description" id="description"></textarea>
		        			</div>
		        		</div>
                        <div class="form-group">
                            Slider Image status : 
                            <label class="radio-inline">
                              <input type="radio" name="publishStatus" id="radio1" value="1"> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="publishStatus" id="radio2" value="0"> Inactive
                            </label>
                        </div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editSliderImage-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Latest Slider Image</h4>
		      		</div>
                        <div class="modal-body">
		        		    <div class="form-group">
							 <label class="col-md-3">Link :</label>
							 <div class="col-md-9">
		        				  <input type="text" name="url" class="form-control"/>
                                </div>
		        		    </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="col-md-6">Add New Image :</label>
                                        <div class="col-md-6">
                                            <input type="file" name="coverImgUpload" />
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-6">
                                        <label class="col-md-6">Old Image :</label>
                                        <div class="col-md-6">
                                            <div id="old_coverImage_holder"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="col-md-6">Add New Mobile Image :</label>
                                        <div class="col-md-6">
                                            <input type="file" name="coverMobileImgUpload" />
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-6">
                                        <label class="col-md-6">Old MobileImage :</label>
                                        <div class="col-md-6">
                                            <div id="old_coverMobileImage_holder"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">Description :</label>
                                <div class="col-md-9">
                                    <textarea class="form-control textarea" rows="3" name="description"></textarea>
                                </div>
		        		    </div>
                            <div class="form-group">
                                <label class="col-md-3">Button Description :</label>
                                <div class="col-md-9">
                                    <textarea class="form-control textarea" rows="3" name="button_description"></textarea>
                                </div>
		        		    </div>
                            <div class="form-group">
                                Slider Image status : 
                                <label class="radio-inline">
                                  <input type="radio" name="publishStatus" id="ps1" value="1"> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="publishStatus" id="ps2" value="0"> Inactive
                                </label>
                            </div>
                        
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
                        
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="deleteSliderImage-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Latest Slider Image</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this slider image?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop
