@extends('a2b_layout')

@section('content')
<script>
    document.getElementById("textareaWysihtml").disabled = true;
</script>

	<div class="booking-pg">
		<h2 class="title">Movement</h2>
	
        
                        
        
       
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        
          <form class="form-inline" id="form" name="form" method="post" action="/a2b/a2b-movement/add">
              
            <div>
                <label class="col-lg-3">Bike Number:</label>
                <div class="col-lg-9">
                    <select id="bikeNumbers" name="bike_id" onChange="getFromLocation()">
                        <option disabled selected></option>
                        @foreach($bikeNumbers as $bikeNumber)
                            <option value="{{ $bikeNumber->id }}">{{$bikeNumber['number_with_location']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
              
            <script>
                $("#bikeNumbers").combobox();
                $( "#toggle" ).on( "click", function() {
                    $( "#bikeNumbers" ).toggle();
                });
            </script>
              
            <div style = "margin:10px; display:inline;"></div>
            <div>
                <label class="col-md-3">To Location:</label>
                <div class="col-md-9">
                    <select id="locations" name="to_location">
                        <option disabled selected></option>
                        @foreach($locations as $location)
                            <option value="{{ $location->id }}">{{$location->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <script>
                $( "#locations" ).combobox();
                $( "#toggle" ).on( "click", function() {
                    $( "#locations" ).toggle();
                });
            </script>
              
            <div style = "margin:10px; display:inline;"></div>
            <div>
                <label class="col-md-3">Notes: </label>
                <div class="col-md-9">
                    <textarea type="text" name="notes" class="form-control" value =""></textarea>
                </div>
            </div>
            <div style = "margin:10px; display:inline;"></div>
            <div>
                <label class="col-md-3">Block the bike for the move:</label>
                <div class="col-md-9">
                    <select id="priority" name="priority">
                        <option value="0" selected>No</option>
                        <option value="1">Yes</option>
                        
                    </select>
                </div>
            </div>
            <div style = "margin:10px; display:inline;"></div>
            <div>
                <label class="col-md-3">Blocking the bike for Fuelling? :</label>
                <div class="col-md-9">
                    <select id="fuel" name="fuel">
                        <option value="0" selected>No</option>
                        <option value="1">Yes</option>
                        
                    </select>
                </div>
            </div>
            <div style = "margin:10px; display:inline;"></div>

            <div><a href="javascript:{}" onclick="document.getElementById('form').submit();" class="btn btn-primary">Create Movement</a></div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        
        <p>&nbsp;&nbsp;</p>
        
            <table class="fixedCol wk-table" style="table-layout: fixed; width:1380px;" >
                <thead>
                    <tr>
                        <th style="width: 90px !important;">Id</th>
                        <th style="width: 150px !important;">Bike</th>
                        <th style="width: 110px !important;">Status</th>
                        <th style="width: 110px !important;">Model</th>
                        <th style="width: 120px !important;">From Location</th>
                        <th style="width: 100px !important;">To Location</th>
                        <th style="width: 80px !important;">Start Odo</th>
                        <th style="width: 80px !important;">End Odo</th>
                        <th style="width: 300px !important;">Notes</th>
                        <th style="width: 80px !important;">Delete</th>

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookings as $booking)
                            <tr>
                                <td>{{ $booking->id }}</td>
                                <td>{{ $booking->getBikeNumber() }}</td>
                                <td>{{ $booking->status }}</td>
                                <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                                <td>{{ $booking->getFromLocation() }}</td>
                                <td>{{ $booking->getToLocation() }}</td>
                                <td>{{ $booking->start_odo }}</td>
                                <td>{{ $booking->end_odo }}</td>
                                <td>{{ $booking->notes }}</td>
                                <td>
                                    @if($booking->status == "Movement Order Created")
                                        <button class="btn btn-danger wk-btn movement_delete_btn" data-id="{{ $booking->id }}"><i class="fa fa-trash-o"></i> Delete</button>
                                    @endif
                                </td>
                                
                        </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
        
                    
        
@stop

@section('model')

<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_delete_form" id="booking_delete_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Movement Order?<br>
		        		If "Yes" press "Delete" else "Close". Also select reason for deleting.
                        
                        <p id="booking_delete_form_error" style="color:red;"></p>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_delete_form_btn"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

<script type="text/javascript">
    $('.movement_delete_btn').click(function(){
        var id = $(this).data('id');
        $('#deleteBooking-modal form').attr("action","/a2b/a2b-movement/delete/"+id);
        $('#deleteBooking-modal').modal('show');
    }); 
    
    $('.booking_delete_form_btn').click(function(){
      $('#deleteBooking-modal form').submit();  
    });
</script>

@stop


