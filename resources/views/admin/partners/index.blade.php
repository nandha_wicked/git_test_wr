@extends('admin_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Partners Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addPartnerModal"><i class="fa fa-plus"></i> Add Partner</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Partner Name</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Areas</th>
					<th>Bike Models</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($partners as $partner)
					<tr>
						<td>{{ $partner->id }}</td>
						<td>{{ $partner->fname }} {{ $partner->lname }}</td>
						<td>{{ $partner->mob }}</td>
						<td>{{ $partner->email }}</td>
						<td>{{ $partner->areaNames }}</td>
						<td>{{ $partner->modelNames }}</td>
						
						<td>
                            <button class="btn btn-info wk-btn partner_edit_btn" data-id="{{ $partner->id }}" data-fname="{{$partner->fname }}" data-area-ids="{{ $partner->area_id }}" data-model-ids="{{ $partner->model_id }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
                            <p></p>
							<button class="btn btn-danger wk-btn partner_delete_btn" data-id="{{ $partner->id }}"><i class="fa fa-trash-o"></i> Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.partner_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deletePartner-modal form').attr("action","/admin/partner/delete/"+id);
			$('#deletePartner-modal').modal('show');
		});
	</script>

	<script type="text/javascript">
		$(function(){
			Partner.init();
	    });
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="deletePartner-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Partner</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this partner?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addPartnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/partner/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Partner</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						
		        		<div class="form-group">
		        			<label class="col-md-3">Bike Models:</label>
		        			<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 model-dropdown">
				        			@foreach ($makes as $make)
										<li><a href="#" class="small make-name" data-value="{{ $make->id }}" tabIndex="-1"><input type="checkbox" class="" name="make-name" value=""/>&nbsp;{{ $make->bike_make }}</a>
											<ul>
												<?php foreach ($make['models'] as $model) { ?>
													<li><a href="#" class="small model-name" data-value="{{ $model->id }}" tabIndex="-1"><input class="model-ids" type="checkbox" name="model_ids[]" value="{{ $model->id }}" />&nbsp;{{ $model->bike_model }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Area Ids:</label>
							<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9">
				        			@foreach ($cities as $city)
										<li><a href="#" class="small city-name" data-value="{{ $city->id }}" tabIndex="-1"><input type="checkbox" class="" name="city-name" value=""/>&nbsp;{{ $city->city }}</a>
											<ul>
												<?php foreach ($city['areas'] as $area) { ?>
													<li><a href="#" class="small" tabIndex="-1"><input type="checkbox" class="area-ids" name="area_ids[]" value="{{ $area->id }}"/>&nbsp;{{ $area->area }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>		
						</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer:</label>
							<div class="col-md-9">
		        				<input name="customer" class="form-control" placeholder="Partner Email">
		        			</div>
		        		</div>	
                        
                        <div class="form-group">
							<label class="col-md-3">Start Date</label>
							<div class="col-md-5">
		        				<input type="text" name="startDate" class="form-control" id="startDate" placeholder="Start Date"/>
		        			</div>
							
		        		</div>

		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>




<div class="modal fade" id="editPartnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="edit-partner">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Partner</h4>
		      		</div>
		      		<div class="modal-body">
		      			@if (count($errors->edit) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
		      			<div class="form-group">
							<label class="col-md-3">Partner:</label>
							<div class="col-md-9">
		        				<input type="text" name="fname" class="form-control"  disabled/>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<label class="col-md-3">Bike Models:</label>
		        			<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 model-dropdown">
				        			@foreach ($makes as $make)
										<li><a href="#" class="small make-name" data-value="{{ $make->id }}" tabIndex="-1"><input type="checkbox" class="" name="make-name" value=""/>&nbsp;{{ $make->bike_make }}</a>
											<ul>
												<?php foreach ($make['models'] as $model) { ?>
													<li><a href="#" class="small model-name" data-value="{{ $model->id }}" tabIndex="-1"><input type="checkbox" class="model-ids" name="model_ids[]" value="{{ $model->id }}" />&nbsp;{{ $model->bike_model }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>
						</div>
						<div class="form-group">
		        			<label class="col-md-3">Areas:</label>
							<div class="button-group col-md-9">
		        				<button type="button" class="btn btn-default btn-sm dropdown-toggle col-md-9" data-toggle="dropdown">Select &nbsp;<span class="caret"></span></button>
			        			<ul class="dropdown-menu col-md-9 area-dropdown">
				        			@foreach ($cities as $city)
										<li><a href="#" class="small city-name" data-value="{{ $city->id }}" tabIndex="-1"><input type="checkbox" name="city-name" value=""/>&nbsp;{{ $city->city }}</a>
											<ul>
												<?php foreach ($city['areas'] as $area) { ?>
													<li><a href="#" class="small" tabIndex="-1"><input type="checkbox" name="area_ids[]" class="area-ids" value="{{ $area->id }}"/>&nbsp;{{ $area->area }}</a></li>
												<?php } ?>
											</ul>
										</li>
				        			@endforeach
				        		</ul>
				        	</div>		
						</div>
						
		        		
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



@stop
