@extends('partner_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>This data is with respect to last two months.For more data please contact customer care.
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Start Date</th>
					<th>Start Time</th>
					<th>End Date</th>
					<th>End Time</th>
					<th>Customer</th>
					<th>Bike</th>
					<th>City</th>
					<th>Amount Paid</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['id'] }}</td>
						<td>{{ $booking['start_date'] }}</td>
						<td>{{ $booking['start_time'] }}</td>
						<td>{{ $booking['end_date'] }}</td>
						<td>{{ $booking['end_time'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }}</td>
                        <td>{{ $booking->revenueAmount() }}</td>
						
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

@stop
