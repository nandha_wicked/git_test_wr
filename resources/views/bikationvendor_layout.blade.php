<!DOCTYPE html>
<html>
	<head>
		<meta name="_token" content="{{ Session::token() }}">
		<link rel="stylesheet" href="/css/admin-style-bikation.css?a=1" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
  		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
		<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
        <script src="/js/jquery-ui.js"></script>
     	 <link rel="stylesheet" type="text/css" href="/editer/lib/css/bootstrap.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/editer/lib/css/prettify.css"></link>
		<link rel="stylesheet" type="text/css" href="/editer/src/bootstrap-wysihtml5.css"></link>
		
        <script>
	    $(function() {
	      $( ".date_time" ).datepicker({dateFormat: 'dd-mm-yy' });
		  var today = new Date();
			//$( ".date_time" ).datepicker({dateFormat: 'dd-mm-yy', minDate: 1 });
	    });
	    </script>

	</head>
	<body class="admin-body">
		<div class="admin-wrapper" id="bikethonvendor-wrapper">
			<div class="header">
				<div class="block1">
					<div><i class="fa fa-tachometer"></i> Dashboard</div>
				</div>
				<div class="block2">
					@if (Session::has('email'))
		                <span class="name">Hello, {!! Session::get('email') !!}</span>                        
		                <a href="/vendor_logout"><i class="fa fa-sign-out"></i> Logout</a>		                        
		            @else                       
		                <a href="#" data-toggle="modal" data-target="#loginModal">LOGIN</a>
		            @endif 							
				</div>
			</div>
			<div class="sidebar">
				<div class="logo">
					<a href="/admin"><img src="/img/logo.png"></a>
				</div>
				<div class="title">MAIN NAVIGATION</div>
							<div id="vertical-menu">
				<ul class="main-nav">
			   <li><a href="{{ url('bikation-user-profile') }}"><i class="fa fa-edit"></i> Edit Profile</a></li>
        <li>
            	<h3><span class="plus">+</span>Trip</h3>
            <ul>
					<li><a href="{{ url('bikation-trip') }}"><i class="fa fa-motorcycle"></i> Trip</a></li>
					<li><a href="{{ url('bikation-package') }}"><i class="fa fa-user"></i> Package</a></li>
					<li><a href="{{ url('bikation-addon') }}"><i class="fa fa-user"></i> Addon</a></li>
					<li><a href="{{ url('bikation-itinerary') }}"><i class="fa fa-bed"></i> Itinerary</a></li>
					<li><a href="{{ url('bikation-trip-photo') }}"><i class="fa fa-picture-o"></i> Trip-photo</a></li>
            </ul>
        </li>
		<li>
            	<h3><span class="plus">+</span>Bookings</h3>
				<ul>
     			   <!--li><a href="{{ url('bikation-refund') }}"><i class="fa fa-user-times"></i> Refund</a></li-->
				   <li><a href="{{ url('bikation-reviews') }}"><i class="fa fa-edit"></i> Reviews</a></li>
				   <li><a href="{{ url('bikation-people_on_trip') }}"><i class="fa fa-user-plus"></i> People-on-trip</a></li>
				   <li><a href="{{ url('bikation-bookings') }}"><i class="fa fa-users"></i> Bookings</a></li>
				</ul>
		</li>
        <!-- we will keep this LI open by default -->
       
	   <li><a href="{{ url('bikation-address') }}"><i class="fa fa-edit"></i> Address</a></li>
        <!--li>
            	<h3><span class="plus">+</span>Favorites</h3>

            <ul>
                <li><a href="#">Global favs</a>
                </li>
                <li><a href="#">My favs</a>
                </li>
                <li><a href="#">Team favs</a>
                </li>
                <li><a href="#">Settings</a>
                </li>
            </ul>
        </li-->
    </ul>
</div>
			</div>
			<div class="content">
				@yield('content')
				@yield('model')
			</div>
		</div>	
		<script type="text/javascript" src="/js/moment.js"></script>
		<script type="text/javascript" src="/js/bikationaddon.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationpackage.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationtrip.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationitinerary.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationtripphoto.js?a=1"></script>
		<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
  		<script type="text/javascript" src="/js/custom.js"></script>
		<script type="text/javascript" src="/js/bikationbookings.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationpeopleontrip.js?a=1"></script>
		<script type="text/javascript" src="/js/bikationreviews.js?a=1"></script>
		<link rel="stylesheet" href="/css/spinner.css" />
		<script type="text/javascript" src="/editer/lib/js/wysihtml5-0.3.0.js"></script>
		<script type="text/javascript" src="/editer/lib/js/prettify.js"></script>
		<script type="text/javascript" src="/editer/src/bootstrap-wysihtml5.js"></script>
		<script>
			$('.textarea').wysihtml5();
		</script>
		<link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">
    	<script src="/js/moment-with-locales.js"></script>
		<script src="/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
            $(function () {
               /* $('#Trip_Start_Date').datetimepicker({
                    format: "DD-MM-YYYY HH:mm A" ,
                    disabledDates: [
                        moment("12/25/2016"),
                        new Date(2016, 11 - 1, 21),
                        "11/22/2016 00:53"
                    ]
                });*/
					$('#Trip_Start_Date').datetimepicker({format: "DD-MM-YYYY HH:mm"});
					$('#Trip_End_Date').datetimepicker({
						format: "DD-MM-YYYY HH:mm" ,
						useCurrent: false //Important! See issue #1075
					});
					$("#Trip_Start_Date").on("dp.change", function (e) {
						$('#Trip_End_Date').data("DateTimePicker").minDate(e.date);
					});
					$("#Trip_End_Date").on("dp.change", function (e) {
						$('#Trip_Start_Date').data("DateTimePicker").maxDate(e.date);
					});
					
					$('#TripStartDate').datetimepicker({format: "DD-MM-YYYY HH:mm"});
					$('#TripEndDate').datetimepicker({
						format: "DD-MM-YYYY HH:mm" ,
						useCurrent: false //Important! See issue #1075
					});
					$("#TripStartDate").on("dp.change", function (e) {
						$('#TripEndDate').data("DateTimePicker").minDate(e.date);
					});
					$("#TripEndDate").on("dp.change", function (e) {
						$('#TripStartDate').data("DateTimePicker").maxDate(e.date);
					});
            });
        </script>
		<div id="loader" class="backdrop">
			<div class="loader-div">
				<div class="gauge-loader">
				  Loading…
				</div>
			</div>
		</div>
	</body>
</html>
