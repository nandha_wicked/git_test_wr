@extends('vendor_layout')

@section('content')
	
	<div class="bike-pg">
		<h2 class="title">Bike Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBike-modal"><i class="fa fa-plus"></i> Add Bike</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>BikeMake</th>
					<th>BikeModel</th>
					<th>City</th>
					<th>Area</th>
					<th>Price Per Hour</th>
					<th>Minimum Hours</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikes as $bike)
					<tr>
						<td>{{ $bike['id'] }}</td>
						<td>{{ $bike['make_name'] }}  </td>
						<td>{{ $bike['model_name'] }}</td>
						<td>{{ $bike['city_name'] }}</td>
						<td>{{ $bike['area_name'] }}</td>
						
						<td>{{ $bike['price_per_hour'] }}</td>
						<td>{{ $bike['minimum_hours'] }}</td>
						<td>
							@if( $bike['status'] == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bike_edit_btn" data-id="{{ $bike['id'] }}" data-modelid="{{ $bike['model_id'] }}" data-areaid="{{ $bike['area_id'] }}" data-status="{{ $bike['status'] }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>	

	<script type="text/javascript">
		$('.bike_edit_btn').click(function(){
			var id = $(this).data('id');
			var modelId = $(this).data('modelid');
			var areaId = $(this).data('areaid');
			var priceId = $(this).data('priceid');
			var status = $(this).data('status');
			
			$('#editBike-modal form').attr("action","/vendor/bike/edit/"+id);
			$('#editBike-modal form select[name=modelName] option[value='+modelId+']').attr('selected', 'selected');
			$('#editBike-modal form select[name=areaName] option[value='+areaId+']').attr('selected', 'selected');
			$('#editBike-modal input[name=modelid]').val(modelId);
			if(status == '1')
				$('#editBike-modal form #bse1').prop("checked",true);	
			else
				$('#editBike-modal form #bse2').prop("checked",true);
			$('#editBike-modal').modal('show');
			console.log($('#editBike-modal form select[name=modelName]').val());
		});
	</script>
		
@stop

@section('model')
	
	<div class="modal fade" id="addBike-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/vendor/bike/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD BIKE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Bike Model :</label>
							<div class="col-md-9">
		        				<select name="modelName" class="form-control">
		        					@foreach($bikeModelList as $bikeModel)
		        						<option value="{{ $bikeModel->id }}">{{ $bikeModel->bike_model }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area :</label>
							<div class="col-md-9">
		        				<select name="areaName" class="form-control">
		        					@foreach($areaList as $area)
		        						<option value="{{ $area->id }}">{{ $area->area }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Bike Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeStatus" id="bsa1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeStatus" id="bsa2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="editBike-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT BIKE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Bike Model :</label>
							<div class="col-md-9">
		        				<select name="modelName" class="form-control" disabled>
		        					@foreach($bikeModelList as $bikeModel)
		        						<option value="{{ $bikeModel->id }}">{{ $bikeModel->bike_model }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area :</label>
							<div class="col-md-9">
		        				<select name="areaName" class="form-control">
		        					@foreach($areaList as $area)
		        						<option value="{{ $area->id }}">{{ $area->area }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Bike Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="bikeStatus" id="bse1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="bikeStatus" id="bse2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="modelid" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
