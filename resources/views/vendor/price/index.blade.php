@extends('vendor_layout')

@section('content')
	
	<div class="price-pg">
		<h2 class="title">Price Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addPrice-modal"><i class="fa fa-plus"></i> Add Price</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Model</th>
					<th>Area</th>
					<th>Price Per Hour</th>
					<th>Minimum Hours</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($priceList as $price)
					<tr>
						<td>{{ $price->id }}</td>
						<td>{{ $price->model_name }}</td>
						<td>{{ $price->area_name }}</td>
						<td>{{ $price->price_per_hour }}</td>
						<td>{{ $price->minimum_hours }}</td>
						<td>
							<button class="btn btn-info wk-btn price_edit_btn" 
							data-id="{{ $price->id }}"
							data-model-id="{{ $price->model_id }}"
							data-model-name="{{ $price->model_name }}"
							data-area-id="{{ $price->area_id }}"
							data-area-name="{{ $price->area_name }}"
							data-wd-gt-eight="{{ $price->price_per_hour }}"
							data-wd-lt-eight="{{ $price->minimum_hours }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.price_edit_btn').click(function(){
			var id = $(this).data('id');
			var modelId = $(this).data('model-id');
			var modelName = $(this).data('model-name');
			var areaId = $(this).data('area-id');
			var areaName = $(this).data('area-name');
			var price_per_hour = $(this).data('wd-gt-eight');
			var minimum_hours = $(this).data('wd-lt-eight');

			$('#editPrice-modal form').attr("action","/vendor/price/edit/"+id);
			$('#editPrice-modal form input[name=modelName]').val(modelName);
			$('#editPrice-modal form input[name=modelId]').val(modelId);
			$('#editPrice-modal form input[name=areaName]').val(areaName);
			$('#editPrice-modal form input[name=areaId]').val(areaId);
			$('#editPrice-modal form input[name=price_per_hour]').val(price_per_hour);
			$('#editPrice-modal form input[name=minimum_hours]').val(minimum_hours);
	
			$('#editPrice-modal').modal('show');
		});
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="addPrice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/vendor/price/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PRICE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Model ID:</label>
							<div class="col-md-9">
			        			<select name="modelId" class="form-control">
	        					@foreach($modelList as $model)
	        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
	        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area ID:</label>
							<div class="col-md-9">
			        			<select name="areaId" class="form-control">
	        					@foreach($areaList as $area)
	        						<option value="{{ $area['id'] }}">{{ $area['area'] }}</option>
	        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Price Per Hour:</label>
							<div class="col-md-9">
		        				<input type="text" name="price_per_hour" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Minimum Hours: </label>
							<div class="col-md-9">
		        				<input type="text" name="minimum_hours" class="form-control" />
		        			</div>
		        		</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editPrice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT PRICE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Model:</label>
							<div class="col-md-9">
		        				<input type="text" name="modelName" class="form-control" disabled="disabled" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaName" class="form-control" disabled="disabled" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Price Per Hour:</label>
							<div class="col-md-9">
		        				<input type="text" name="price_per_hour" class="form-control" />
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Minimum Hours:</label>
							<div class="col-md-9">
		        				<input type="text" name="minimum_hours" class="form-control" />
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="modelId" class="form-control" />
		        		<input type="hidden" name="areaId" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
