<!DOCTYPE html>
    <!--
    This is a starter template page. Use this page to start your new project from
    scratch. This page gets rid of all links and provides the needed markup only.
    -->
    <html>
    <head>
        <meta charset="UTF-8">
        <title>{{env('SITENAMECAP')}} Admin</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="/bower_components/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link href="/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        
        <![endif]-->
        
        <meta name="_token" content="{{ Session::token() }}">
		<link rel="stylesheet" href="/css/admin-style.css?a=7" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
		<link rel="stylesheet" href="/css/jquery.dataTables.min.css" />
        	<link rel="stylesheet" href="/css/font-awesome.min.css"/>
        
        @if(!isset($jqueryOverride))
		  <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
		@else
            {!! $jqueryOverride !!}
        @endif
        
        
        
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/combobox.js"></script>
		<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
		<script src="/js/jquery-ui.js"></script>
     	 <link rel="stylesheet" type="text/css" href="/editer/lib/css/bootstrap.min.css" id="textareaWysihtml"/>
		<link rel="stylesheet" type="text/css" href="/editer/lib/css/prettify.css" id="textareaWysihtml"/>
		<link rel="stylesheet" type="text/css" href="/editer/src/bootstrap-wysihtml5.css" id="textareaWysihtml"/>
        <link href="/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
        
        
        <script type="text/javascript" src="/js/Sortable.js"></script>
        <script src="/js/jquery.multi-select.js" type="text/javascript"></script>
        
        @yield('libraries')
        
    </head>
    <body class="skin-blue">
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="" class="logo"><b>{{env('SITENAMECAP')}}</b></a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                       
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="/images/user1.png" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>{{Auth::user()->first_name}}</p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                
                <?php $userRoles = explode(',',Auth::user()->role_string); ?>


                <!-- Sidebar Menu -->
                <ul class="sidebar-menu">
                    <li><a href="/logout">Logout<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-sign-out"></span></a></li>
                    <li><a href="get-deliveries">Deliveries<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-share"></span></a></li>
                    <li><a href="get-returns">Returns<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-reply"></span></a></li>
                    <li><a href="get-movement-deliveries">Movement Deliveries<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-truck"></span></a></li>
                    <li><a href="get-movement-returns">Movement Returns<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-truck"></span></a></li>
                    
                    <li><a href="a2b-movement">Create Movement Order<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-truck"></span></a></li>
                    
                    <li><a href="one-way-rental-bike">Bike List<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-list"></span></a></li>
                    
                    @if(!empty(array_intersect(["operations manager"], $userRoles)))
                        <li><a href="report"> Report<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-table"></span></a></li>
                        
                    @endif
                    
                </ul><!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                

            <!-- Main content -->
                <section class="content" style="overflow-x: scroll; padding-bottom: 500px;">
                
				@yield('content')
				@yield('model')
                </section>
			</div>

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                
            </div>
            <!-- Default to the left -->
            <strong>Copyright © 2017 <a href="http://www.wickedride.com">WickedRide Adventure Services Pvt. Ltd.</a></strong> All rights reserved.
        </footer>

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <script type="text/javascript" src="/js/moment.js"></script>
    <script type="text/javascript" src="/js/back.js?a=1"></script>
    <script type="text/javascript" src="/js/backend.js?a=9"></script>
    <script type="text/javascript" src="/js/admin.js?a=1"></script>
    <script type="text/javascript" src="/js/promocode.js?a=1"></script>
    <script type="text/javascript" src="/js/delivery.js?a=6"></script>
    <script type="text/javascript" src="/js/return.js?a=26"></script>
    <script type="text/javascript" src="/js/hermite.js"></script>
    <script type="text/javascript" src="/js/erp.js?a=1"></script>
    <link rel="stylesheet" href="/css/spinner.css" />
    <script type="text/javascript" src="/editer/lib/js/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="/editer/lib/js/prettify.js"></script>
    <script type="text/javascript" src="/editer/src/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
    <script type="text/javascript" src="/js/Sortable.js"></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/js/froala_editor.min.js'></script>
    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/js/plugins/link.min.js"></script>
    

    <script>
        $('.textarea').wysihtml5();
    </script>
    <link rel="stylesheet" href="/css/spinner.css" />

    <div id="loader" class="backdrop">
        <div class="loader-div">
            <div class="gauge-loader">
              Loading…
            </div>
        </div>
    </div>
        
    
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
    
    </body>
</html>
