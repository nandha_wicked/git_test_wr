@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">User Profile</h2>
		<!--button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button-->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)	
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	
		<form method="post" action="/bikation-user-profile/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-user-profile-add">
		      		<div class="modal-header">
		        		<!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button-->
		        		<!--h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4-->
		      		</div>
		      		<div class="modal-body1">
						<div class="form-group">
							<label class="col-md-3">First Name</label>
							<div class="col-md-9">
		        				<input type="text" name="first_name" value="{{ isset($bikation_user_profile->first_name) ? $bikation_user_profile->first_name : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Last Name</label>
							<div class="col-md-9">
		        				<input type="text" name="last_name" value="{{ isset($bikation_user_profile->last_name) ? $bikation_user_profile->last_name : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Mobile Num</label>
							<div class="col-md-9">
		        				<input type="text" name="mobile_num" value="{{ isset($bikation_user_profile->mobile_num) ? $bikation_user_profile->mobile_num : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Bank</label>
							<div class="col-md-9">
		        				<input type="text" name="Bank" value="{{ isset($bikation_user_profile->Bank) ? $bikation_user_profile->Bank : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Bank Account Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Bank_Account_Number" value="{{ isset($bikation_user_profile->Bank_Account_Number) ? $bikation_user_profile->Bank_Account_Number : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">IFSC</label>
							<div class="col-md-9">
		        				<input type="text" name="IFSC" value="{{ isset($bikation_user_profile->IFSC) ? $bikation_user_profile->IFSC : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">PAN</label>
							<div class="col-md-9">
		        				<input type="text" name="PAN" value="{{ isset($bikation_user_profile->PAN) ? $bikation_user_profile->PAN : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">TAN</label>
							<div class="col-md-9">
		        				<input type="text" name="TAN" value="{{ isset($bikation_user_profile->TAN) ? $bikation_user_profile->TAN : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">SRN</label>
							<div class="col-md-9">
		        				<input type="text" name="SRN" value="{{ isset($bikation_user_profile->SRN) ? $bikation_user_profile->SRN : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">CIN</label>
							<div class="col-md-9">
		        				<input type="text" name="CIN"  value="{{ isset($bikation_user_profile->CIN) ? $bikation_user_profile->CIN : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">TIN</label>
							<div class="col-md-9">
		        				<input type="text" name="TIN" value="{{ isset($bikation_user_profile->TIN) ? $bikation_user_profile->TIN : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">NDA ID</label>
							<div class="col-md-9">
		        				<input type="text" name="NDA_ID" value="{{ isset($bikation_user_profile->NDA_ID) ? $bikation_user_profile->NDA_ID : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		
						<div class="form-group">
							<label class="col-md-3">Agreement ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Agreement_ID" value="{{ isset($bikation_user_profile->Agreement_ID) ? $bikation_user_profile->Agreement_ID : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Vendor Image</label>
							<div class="col-md-9">
		        				<input type="text" name="Vendor_Image" value="{{ isset($bikation_user_profile->Vendor_Image) ? $bikation_user_profile->Vendor_Image : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Contact Person</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_person" value="{{ isset($bikation_user_profile->Contact_person) ? $bikation_user_profile->Contact_person : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Contact Email</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_Email" value="{{ isset($bikation_user_profile->Contact_Email) ? $bikation_user_profile->Contact_Email : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Contact Phone Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_Phone_Number" value="{{ isset($bikation_user_profile->Contact_Phone_Number) ? $bikation_user_profile->Contact_Phone_Number : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<!--button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button-->
		      		</div>
		    	</form>
	</div>

	

@stop		

