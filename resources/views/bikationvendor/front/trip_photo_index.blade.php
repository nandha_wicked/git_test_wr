@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Trip Media</h2>
		<div class="row">
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Media</button>
		</div>
		
		<form method="post" action="/bikation-trip-photo" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-right-xs" style="padding: 0;">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		</div>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Trip Name</th>
					<th>Media Type</th>
					<th>Media URL</th>
					<!-- <th>Media_URL</th>
					<th>created on</th>
					<th>updated on</th>
					<th>Created By</th>
					<th>Updated by</th> -->
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_trip_photoList as $bikation_trip_photo)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_trip_photo['TripName'] }}</td>
						<td>{{ $bikation_trip_photo->Media_Select }}</td>

						<td>@if( $bikation_trip_photo->Media_Select == "Video" )
								{{ $bikation_trip_photo->Media_URL }}
							@else
								<img src="{{ $bikation_trip_photo->Media_URL }}" width ="50px" />
							@endif
							</td>
						<!-- <td>{{ $bikation_trip_photo->Media_Size }}</td>
						<td>{{ $bikation_trip_photo->created_on }}</td>
						<td>{{ $bikation_trip_photo->updated_on }}</td>
						<td>{{ $bikation_trip_photo->Created_By }}</td>
						<td>{{ $bikation_trip_photo->Updated_by }}</td> -->
						<td>
							@if( $bikation_trip_photo->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-danger wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_trip_photo->id }}" data-tripid="{{ $bikation_trip_photo->Trip_ID }}" data-mediaselect="{{ $bikation_trip_photo->Media_Select }}" data-mediaurl="{{ $bikation_trip_photo->Media_URL }}" data-mediasize="{{ $bikation_trip_photo->Media_Size }}" data-createdon="{{ $bikation_trip_photo->created_on }}" data-updatedon="{{ $bikation_trip_photo->updated_on }}" data-createdby="{{ $bikation_trip_photo->Created_By }}" data-updatedby="{{ $bikation_trip_photo->Updated_by }}" data-status="{{ $bikation_trip_photo->Status }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_trip_photo->id }}"><i class="fa fa-close"></i> Delete</button>

						</td>
					</tr>
					<?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      TripPhoto.BikationTripPhoto.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-trip-photo/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-trip-photo-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD Media</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name:</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">Media Size</label>
							<div class="col-md-9">
		        				<input type="text" name="Media_Size" id="Media_Size" /> 
								
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Media Option</label>
							<div class="col-md-9">
		        				<input type="radio" name="Media_Select" id="Media_Select1" checked value="Image" /> Image
        						<input type="radio" name="Media_Select" id="Media_Select2" value="Video" />Video
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Media</label>
							<div class="col-md-9">
		        				<input type="text" name="Media_URL" id="media_video" class="form-control media_file hide"/>
								<input type="file" name="Media_URL" id="media_image" class="form-control media_file"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">created on</label>
							<div class="col-md-9">
		        				<input type="text" name="created_on" id="created_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">updated on</label>
							<div class="col-md-9">
		        				<input type="text" name="updated_on" id="updated_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_By" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Updated by</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_by" id="Updated_by" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data" id="bikation-trip-photo-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit Media</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">Media Size</label>
							<div class="col-md-9">
		        				<input type="text" name="Media_Size" id="Media_Size" /> 
								
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Media Option</label>
							<div class="col-md-9">
		        				<input type="radio" name="Media_Select" id="Media_Select1" checked value="Image" /> Image
        						<input type="radio" name="Media_Select" id="Media_Select2" value="Video" />Video
		        			</div>
		        			<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			<label class="col-md-6">Old Image :</label>
									<div class="col-md-6">
				        				<div id="old_coverImage_holder"></div>
				        			</div>
		        				</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Media</label>
							<div class="col-md-9">
		        				<input type="text" name="Media_URL" id="media_video" class="form-control media_file hide"/>
								<input type="file" name="Media_URL" id="media_image" class="form-control media_file"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">created on</label>
							<div class="col-md-9">
		        				<input type="text" name="created_on" id="created_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">updated on</label>
							<div class="col-md-9">
		        				<input type="text" name="updated_on" id="updated_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_By" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Updated by</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_by" id="Updated_by" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
	
@stop	