<!DOCTYPE html>
<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/css/admin-style.css" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<style type="text/css">
    /* Credit to bootsnipp.com for the css for the color graph */
	.colorgraph {
	  height: 5px;
	  border-top: 0;
	  background: #c4e17f;
	  border-radius: 5px;
	  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	}
    </style>
	</head>
	<body class="login-pg">
		<div class="container">
			<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-5 col-sm-offset-2 col-md-offset-3">
				<div class="col-md-12">
					<div class="login-container register-vendor">
						

						<div class="img"><img src="/img/admin3.png" /></div>
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<hr class="colorgraph">
						<form action="/bikation-vendor" method="post">
							<div class="form-group">
							<input type="text" name="email" placeholder="Email address" class="form-control" value="{{ Input::old('email') }}" required/>
							</div>
							<div class="form-group">
							<input type="password" name="password" placeholder="Password" class="form-control" required/>
							</div>
							<hr class="colorgraph">
							<div class="row">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="col-xs-12 col-md-6"><button type="submit" class="btn btn-primary btn-block btn-lg">Login</button></div>
							<div class="col-xs-12 col-md-6"><a class="btn btn-success btn-block btn-lg" href="{{ url('bikation-vendor/register') }}">Register</a></div>
							</div>
						</form>
					</div>
				</div>
				</div>
			</div>
		</div>
	</body>
</html>