@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Refund Homepage</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Refund</button> -->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Refund_ID</th>
					<th>reference_ID</th>
					<th>bikation_bike_rental</th>
					<th>user</th>
					<th>amount</th>
					<th>refund_reason</th>
					<th>status</th>
					<th>transaction_ID_from_PG</th>
					<th>created_on</th>
					<th>updated_on</th>
					<th>Created_By</th>
					<th>Updated_by</th>
					<th>Approved_By</th>
					<th>Approved_on</th>
					<th>Additional_Remarks</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikation_refundList as $bikation_refund)
					<tr>
						<td>{{ $bikation_refund->Refund_ID }}</td>
						<td>{{ $bikation_refund->reference_ID }}</td>
						<td>{{ $bikation_refund->bikation_bike_rental }}</td>
						<td>{{ $bikation_refund->user }}</td>
						<td>{{ $bikation_refund->amount }}</td>
						<td>{{ $bikation_refund->refund_reason }}</td>
						<td>{{ $bikation_refund->status }}</td>
						<td>{{ $bikation_refund->transaction_ID_from_PG }}</td>
						<td>{{ $bikation_refund->created_on }}</td>
						<td>{{ $bikation_refund->updated_on }}</td>
						<td>{{ $bikation_refund->Created_By }}</td>
						<td>{{ $bikation_refund->Updated_by }}</td>
						<td>{{ $bikation_refund->Approved_By }}</td>
						<td>{{ $bikation_refund->Approved_on }}</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_refund->Refund_ID }}" data-referenceID="{{ $bikation_refund->reference_ID }}" data-bikationbikerental="{{ $bikation_refund->bikation_bike_rental }}" data-user="{{ $bikation_refund->user }}" data-amount="{{ $bikation_refund->amount }}" data-refundreason="{{ $bikation_refund->refund_reason }}" data-status="{{ $bikation_refund->status }}" data-transactionIDfromPG="{{ $bikation_refund->transaction_ID_from_PG }}" data-createdon="{{ $bikation_refund->created_on }}" data-updatedon="{{ $bikation_refund->updated_on }}" data-CreatedBy="{{ $bikation_refund->Created_By }}" data-Updatedby="{{ $bikation_refund->Updated_by }}" data-ApprovedBy="{{ $bikation_refund->Approved_By }}" data-Approvedon="{{ $bikation_refund->Approved_on }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_refund->Refund_ID }}"><i class="fa fa-pencil-square-o"></i> Delete</button>

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Admin.BikationRefund.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/packages/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" id="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Package_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Booked_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Booked_By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No_of_People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-9">
		        				<input type="text" name="Status" id="Status" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon_cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total_Cost :</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Additional_Remarks:</label>
							<div class="col-md-3">
		        				<input type="text" name="Additional_Remarks" id="Additional_Remarks" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Address</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" id="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Package_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Booked_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Booked_By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No_of_People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-9">
		        				<input type="text" name="Status" id="Status" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon_cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total_Cost :</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Additional_Remarks:</label>
							<div class="col-md-3">
		        				<input type="text" name="Additional_Remarks" id="Additional_Remarks" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	