@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Addon Homepage</h2>
		<div class="row">
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Add-On</button>
		</div>
		<form method="post" action="/bikation-addon" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		</div>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Trip Name</th>
					<th>Package Name</th>
					<th>Activity Name</th>
					<!-- <th>Exclusion Inclusion</th> -->
					<!--th>Std_Opt</th-->
					<th>Cost</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_addonList as $bikation_addon)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_addon['TripName'] }}</td>
						<td>{{ $bikation_addon['PackageName'] }}</td>
						<td>@if( $bikation_addon->Exclusion_Inclusion == 1 )
								<strong>Include :</strong> {{$bikation_addon->Activity_ID}}
							@else
								<strong>Exclude :</strong> {{$bikation_addon->Activity_ID}}
							@endif</td>
						<!-- <td>{{ $bikation_addon->Exclusion_Inclusion }}</td> -->
						<!--td>{{ $bikation_addon->Std_Opt }}</td-->
						<td>{{ $bikation_addon->Cost }}</td>
						<td>
							@if( $bikation_addon->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_addon->id }}" data-tripid="{{ $bikation_addon->Trip_ID }}" data-packageid="{{ $bikation_addon->Package_ID }}" data-activityid="{{ $bikation_addon->Activity_ID }}" data-stdopt="{{ $bikation_addon->Std_Opt }}" data-exclusioninclusion="{{ $bikation_addon->Exclusion_Inclusion }}" data-cost="{{ $bikation_addon->Cost }}" data-status="{{ $bikation_addon->Status }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-danger wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_addon->id }}"><i class="fa fa-close"></i> Delete</button>
						</td>
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	
	<script type="text/javascript">
		$(function(){
	      Addon.BikationAddon.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-addon/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-addon-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD Add-On</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" id="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">Valid To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name</label>
							<div class="col-md-9">
								<select name="Package_ID" id="Package_ID" class="form-control">
									<option value="">Select</option>
		        					<!--@foreach($bikation_packageList as $bikation_package)
								  			<option value="{{ $bikation_package->Package_ID }}">{{ $bikation_package->name }}</option>
								  	@endforeach -->
								</select>
							</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Exclusion Inclusion :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Exclusion_Inclusion" id="bmose1" value="1"> RIDE INCLUDES
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Exclusion_Inclusion" id="bmose2" value="0"> RIDE EXCLUDES
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Activity Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Activity_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Std Opt</label>
							<div class="col-md-9">
		        				<!--input type="text" name="Std_Opt" class="form-control"/-->
								<label class="radio-inline">
								  <input type="radio" name="Std_Opt" value="Standard"> Standard
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Std_Opt" value="Optional"> Optional
								</label>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data" id="bikation-addon-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Addon</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name:</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" id="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group  hide">
							<label class="col-md-3">Valid To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name</label>
							<div class="col-md-9">
								<select name="Package_ID" id="Package_ID" class="form-control">
		        					<!--@foreach($bikation_packageList as $bikation_package)
								  			<option value="{{ $bikation_package->Package_ID }}">{{ $bikation_package->name }}</option>
								  	@endforeach-->
								</select>
							</div>
		        		</div>
		        		
		        		<div class="form-group">
			        		<label class="col-md-3">Exclusion Inclusion :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Exclusion_Inclusion" id="Exclusion_Inclusion1" value="1"> RIDE INCLUDES
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Exclusion_Inclusion" id="Exclusion_Inclusion2" value="0"> RIDE EXCLUDES
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Activity Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Activity_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Std Opt</label>
							<div class="col-md-9">
		        				<label class="radio-inline">
								  <input type="radio" name="Std_Opt" id="Standard" value="Standard"> Standard
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Std_Opt" id="Optional" value="Optional"> Optional
								</label>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
