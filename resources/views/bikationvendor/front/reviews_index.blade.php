@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Reviews Homepage</h2>
		<div class="row">
		<div class="col-xs-7 text-left"><!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Reviews</button> -->
		</div>
		
		<form method="post" action="/bikation-reviews" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs" style="padding:0;">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		</div>
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Trip Name</th>
				<!-- 	<th>Customer_ID</th> -->
					<th>Overall</th>
				    <th>Remarks</th>
					<!--th>Planning</th>
					<th>On_Schedule</th>
					<th>Food</th>
					<th>Stay</th>
					<th>Activities</th-->
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_reviewsList as $bikation_review)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_review['TripName'] }}</td>
						<!-- <td>{{ $bikation_review->Customer_ID }}</td> -->
						<td>{{ $bikation_review->Overall }}</td>
						<td>{{ $bikation_review->Remarks }}</td>
						<!--td>{{ $bikation_review->Planning }}</td>
						<td>{{ $bikation_review->On_Schedule }}</td>
						<td>{{ $bikation_review->Food }}</td>
						<td>{{ $bikation_review->Stay }}</td>
						<td>{{ $bikation_review->Activities }}</td-->
						
						<td>
							@if( $bikation_review->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td><!--<a href="{{ url('bikation-reviews/change-status/'.$bikation_review->id) }}"><button class="btn btn-info wk-btn">Change Status</button></a>-->
						<!--button class="btn btn-primary bikeModel_edit_btn"  data-id="{{ $bikation_review->id }}" data-tripid="{{ $bikation_review->Trip_ID }}" data-customerid="{{ $bikation_review->Customer_ID }}" data-overall="{{ $bikation_review->Overall }}" data-planning="{{ $bikation_review->Planning }}" data-onschedule="{{ $bikation_review->On_Schedule }}" data-food="{{ $bikation_review->Food }}" data-stay="{{ $bikation_review->Stay }}" data-activities="{{ $bikation_review->Activities }}" data-remarks="{{ $bikation_review->Remarks }}" data-status="{{ $bikation_review->Status }}"><i class="fa fa-pencil-square-o"></i>View Review</button--></td>

					</tr>
					<?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<script type="text/javascript">
		$(function(){
	      Reviews.BikationReviews.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-reviews/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD Reviews</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Trip Name</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Overall</label>
							<div class="col-md-9">
		        				<input type="text" name="Overall" id="Overall" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Planning</label>
							<div class="col-md-9">
		        				<input type="text" name="Planning" id="Planning" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">On_Schedule</label>
							<div class="col-md-9">
		        				<input type="text" name="On_Schedule" id="On_Schedule" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Food</label>
							<div class="col-md-9">
		        				<input type="text" name="Food" id="Food" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Stay</label>
							<div class="col-md-9">
		        				<input type="text" name="Stay" id="Stay" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Activities</label>
							<div class="col-md-9">
		        				<input type="text" name="Activities" id="Activities" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Remarks</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="Remarks" id="Remarks"></textarea>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data" id="bikation-addon-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Show Review</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name:</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" id="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Overall</label>
							<div class="col-md-9">
		        				<input type="text" name="Overall" id="Overall" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Planning</label>
							<div class="col-md-9">
		        				<input type="text" name="Planning" id="Planning" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">On_Schedule</label>
							<div class="col-md-9">
		        				<input type="text" name="On_Schedule" id="On_Schedule" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Food</label>
							<div class="col-md-9">
		        				<input type="text" name="Food" id="Food" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Stay</label>
							<div class="col-md-9">
		        				<input type="text" name="Stay" id="Stay" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Activities</label>
							<div class="col-md-9">
		        				<input type="text" name="Activities" id="Activities" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Remarks</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="Remarks" id="Remarks"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<!--<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>-->
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>	

	
@stop	