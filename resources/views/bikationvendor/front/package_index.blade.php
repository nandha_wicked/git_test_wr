@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Packages Homepage</h2>
		<div class="row">
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button>
		</div>
		<form method="post" action="/bikation-package" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		</div>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Package No</th>
					<th>Trip Name</th>
					<th>Package Name</th>
					<!-- <th>Description</th> -->
					<th>No of Tickets 1px</th>
					<th>Remaining Tickes 1px</th>
					<th>Cost 1px</th>
					<th>No of Tickets 2px</th>
					<th>Remaining Tickes 2px</th>
					<th>Cost 2px</th>
					<th>Rental Bike Cost</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_packageList as $bikation_package)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_package['TripName'] }}</td>
						<td>{{ $bikation_package->name }}</td>
						<!-- <td>{{ $bikation_package->Description }}</td> -->
						<td>{{ $bikation_package->No_of_Tickets_Cost_1px }}</td>
						<td>{{ $bikation_package->Tickes_Remaining_Cost_1px }}</td>
						<td>{{ $bikation_package->Cost_1px }}</td>
						<td>{{ $bikation_package->No_of_Tickets_Cost_2px }}</td>
						<td>{{ $bikation_package->Tickes_Remaining_Cost_2px }}</td>
						<td>{{ $bikation_package->Cost_2px }}</td>
						<td>{{ $bikation_package->rental_bike_cost }}</td>
						<td>
							@if( $bikation_package->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_package->Package_ID }}" data-name="{{ $bikation_package->name }}" data-tripid="{{ $bikation_package->Trip_ID }}" data-description="{{ $bikation_package->Description }}" data-noofticketscost1px="{{ $bikation_package->No_of_Tickets_Cost_1px }}" data-tickesremainingcost1px="{{ $bikation_package->Tickes_Remaining_Cost_1px }}" data-cost1px="{{ $bikation_package->Cost_1px }}" data-noofticketscost2px="{{ $bikation_package->No_of_Tickets_Cost_2px }}" data-tickesremainingcost2px="{{ $bikation_package->Tickes_Remaining_Cost_2px }}" data-cost2px="{{ $bikation_package->Cost_2px }}" data-rental_bike_cost="{{ $bikation_package->rental_bike_cost }}" data-status="{{ $bikation_package->Status }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-danger wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_package->Package_ID }}"><i class="fa fa-close"></i> Delete</button>
						</td>
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		$(function(){
	      Package.BikationPackage.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-package/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-package-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name:</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" id="name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="Description" id="Description"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets 1px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Tickes Remaining 1px</label>
							<div class="col-md-9">
		        				<input type="text" name="Tickes_Remaining_Cost_1px" id="Tickes_Remaining_Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost of 1px ticket</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost_1px" id="Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets 2px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Tickes Remaining 2px</label>
							<div class="col-md-9">
		        				<input type="text" name="Tickes_Remaining_Cost_2px" id="Tickes_Remaining_Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost of 2px ticket</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost_2px" id="Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Rental Bike Cost (Only for subtraction)</label>
							<div class="col-md-9">
		        				<input type="text" name="rental_bike_cost" id="rental_bike_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data" id="bikation-package-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Edit PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name :</label>
							<div class="col-md-9">
		        				<input type="text" name="name" id="name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Description :</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="Description" id="Description"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets Cost 1px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Tickes Remaining Cost 1px</label>
							<div class="col-md-9">
		        				<input type="text" name="Tickes_Remaining_Cost_1px" id="Tickes_Remaining_Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost of 1px ticket</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost_1px" id="Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets Cost 2px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Tickes Remaining Cost 2px</label>
							<div class="col-md-9">
		        				<input type="text" name="Tickes_Remaining_Cost_2px" id="Tickes_Remaining_Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost of 2px ticket</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost_2px" id="Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Rental Bike Cost (Only for subtraction)</label>
							<div class="col-md-9">
		        				<input type="text" name="rental_bike_cost" id="rental_bike_cost" class="form-control"/>
		        			</div>
		        		</div>

		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
	
@stop	
