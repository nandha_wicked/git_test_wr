@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">TRIP Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Trip</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<!-- <th>Vendor</th> -->
					<th>Trip Type</th>
					<th>Trip Name</th>
					<!-- <th>Created On</th>
					<th>Created by</th>
					<th>Approve Requested on</th>
					<th>Approved By</th>
					<th>Approved  on</th>
					<th>Cancelled Request on</th>
					<th>Cancel approved by</th>
					<th>Cancelled on</th> -->
					<!--th>Total No of Tickets</th>
					<th>No Of Tickets Available</th-->
					<!-- <th>Trip Start Date</th>
					<th>Trip End Date</th> -->
					<th>Start Location</th>
					<th>End Location</th>
					<th>Total Distance</th>
					<th>Total riding hours</th>
					<th>No of stops</th>
					<!-- <th>Ticket Denomination</th> -->
					<!-- <th>Experience Level Required</th> -->
					<th>Preffered Bikes</th>
					<!-- <th>General Instructions</th> -->
					<!-- <th>Trip Description</th> -->
					<!--th>Approve By Admin</th-->
					<th>Status</th>
					<th>Send Request Status Action</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				 <?php  $i=1; ?>
				@foreach($bikation_tripList as $bikation_trip)
					<tr>
						<td><?php echo $i ?></td>
						<!-- <td>{{ $bikation_trip->Vendor_ID }}</td> -->
						<td>{{ $bikation_trip->Trip_type }}</td>
						<td>{{ $bikation_trip->Trip_Name }}</td>
						<!-- <td>{{ $bikation_trip->Created_On }}</td>
						<td>{{ $bikation_trip->Created_by }}</td>
						<td>{{ $bikation_trip->Approve_requested_on }}</td>
						<td>{{ $bikation_trip->Approved_By }}</td>
						<td>{{ $bikation_trip->Approved_on }}</td>
						<td>{{ $bikation_trip->Cancelled_Request_on }}</td>
						<td>{{ $bikation_trip->Cancel_approved_by }}</td>
						<td>{{ $bikation_trip->Cancelled_on }}</td> -->
						<!--td>{{ $bikation_trip->Total_No_of_Tickets }}</td>
						<td>{{ $bikation_trip->No_of_tickets_available }}</td-->
						<!-- <td>{{ $bikation_trip->Trip_Start_Date }}</td>
						<td>{{ $bikation_trip->Trip_End_Date }}</td> -->
						<td>{{ $bikation_trip->Start_Location }}</td>
						<td>{{ $bikation_trip->End_Location }}</td>
						<td>{{ $bikation_trip->Total_Distance }}</td>
						<td>{{ $bikation_trip->Total_riding_hours }}</td>
						<td>{{ $bikation_trip->No_of_stops }}</td>
						<!-- <td>{{ $bikation_trip->ticket_denomination }}</td> -->
						<!-- <td>{{ $bikation_trip->Experience_Level_required }}</td> -->
						<td>{{ $bikation_trip->Preffered_Bikes }}</td>
						<!-- <td>{{ $bikation_trip->General_Instructions }}</td> -->
						<!-- <td>{{ $bikation_trip->Trip_Description }}</td> -->
						<!--td>@if( $bikation_trip->Approve_By_Admin == 2 )
								Send Request For Approve but Not Approve By Admin
							@elseif( $bikation_trip->Approve_By_Admin == 0 )
								Not Send Approve Request For Approve
							@else
							   Approve By Admin
							@endif
						</td-->
						<td>
							@if( $bikation_trip->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							@if($bikation_trip->Approve_By_Admin == 0 )
								<a href="{{ url('bikation-trip/send-approve-request/'.$bikation_trip->Trip_ID) }}"><button class="btn btn-primary">Send Request For Approve</button></a>
							@elseif($bikation_trip->Approve_By_Admin == 1 )
								<button class="btn btn-success"> Approved by Admin</button>
							@elseif($bikation_trip->Approve_By_Admin == 3 )
								<button class="btn btn-danger"> Cancel By Admin</button>
							@else
							<button class="btn btn-primary"> Pending</button>
							@endif
						</td>
						<td>
						<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_trip->Trip_ID }}" data-vendorid="{{ $bikation_trip->Vendor_ID }}" data-triptype="{{ $bikation_trip->Trip_type }}" data-tripname="{{ $bikation_trip->Trip_Name }}" data-createdon="{{ $bikation_trip->Created_On }}" data-createdby="{{ $bikation_trip->Created_by }}" data-status="{{ $bikation_trip->Status }}" data-approverequestedon="{{ $bikation_trip->Approve_requested_on }}" data-approvedby="{{ $bikation_trip->Approved_By }}" data-approvedon="{{ $bikation_trip->Approved_on }}" data-cancelledrequeston="{{ $bikation_trip->Cancelled_Request_on }}" data-cancelapprovedby="{{ $bikation_trip->Cancel_approved_by }}" data-cancelledon="{{ $bikation_trip->Cancelled_on }}" data-totalnooftickets="{{ $bikation_trip->Total_No_of_Tickets }}" data-noofticketsavailable="{{ $bikation_trip->No_of_tickets_available }}" data-tripstartdate="{{  date('d-m-Y H:i', strtotime($bikation_trip->Trip_Start_Date)) }}" data-tripenddate="{{ date('d-m-Y H:i', strtotime($bikation_trip->Trip_End_Date)) }}" data-startlocation="{{ $bikation_trip->Start_Location }}" data-endlocation="{{ $bikation_trip->End_Location }}" data-totaldistance="{{ $bikation_trip->Total_Distance }}" data-totalridinghours="{{ $bikation_trip->Total_riding_hours }}" data-noofstops="{{ $bikation_trip->No_of_stops }}" data-ticketdenomination="{{ $bikation_trip->ticket_denomination }}" data-experiencelevelrequired="{{ $bikation_trip->Experience_Level_required }}" data-prefferedbikes="{{ $bikation_trip->Preffered_Bikes }}" data-generalinstructions="{{ $bikation_trip->General_Instructions }}" data-tripdescription="{{ $bikation_trip->Trip_Description }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
						<button class="btn btn-danger wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_trip->Trip_ID }}"><i class="fa fa-close"></i> Delete</button>
						</td>
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		$(function(){
	      Trip.BikationTrip.init();
	    });
	</script>
@stop		
@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-trip/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-trip-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD TRIP</h4>
		      		</div>
		      		<div class="modal-body">
		        		<input type="hidden" name="Vendor_ID" id="Vendor_ID" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        		<div class="form-group">
							<label class="col-md-3">Trip Type</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_type" id="Trip_type" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_Name" id="Trip_Name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created On</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_On" id="Created_On" class="form-control date_time "/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_by" id="Created_by" class="form-control"/>
		        			</div>
		        		</div> 
		        		<div class="form-group" hidden>
							<label class="col-md-3">Approve Requested On</label>
							<div class="col-md-9">
		        				<input type="text" name="Approve_requested_on" id="Approve_requested_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Approved By</label>
							<div class="col-md-9">
		        				<input type="text" name="Approved_By" id="Approved_By" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group" hidden>
							<label class="col-md-3">Approved On</label>
							<div class="col-md-9">
		        				<input type="text" name="Approved_on" id="Approved_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled Request On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_Request_on" id="	Cancelled_Request_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancel Approved By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancel_approved_by" id="Cancel_approved_by" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_on" id="Cancelled_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">Total No Of Tickets</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_No_of_Tickets" id="Total_No_of_Tickets" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">No Of Tickets Available</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_tickets_available" id="No_of_tickets_available" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Start Date</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_Start_Date" id="Trip_Start_Date" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip End Date</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_End_Date" id="Trip_End_Date" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Start Location</label>
							<div class="col-md-9">
		        				<input type="text" name="Start_Location" id="Start_Location" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">End Location</label>
							<div class="col-md-9">
		        				<input type="text" name="End_Location" id="End_Location" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Distance</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Distance" id="Total_Distance" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Riding Hours </label>
							<div class="col-md-9">
		        				<input type="text" name="Total_riding_hours" id="	Total_riding_hours" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No Of Stops</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_stops" id="No_of_stops" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ticket Denomination</label>
							<div class="col-md-9">
		        				<input type="text" name="ticket_denomination" id="ticket_denomination" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Experience Level Required</label>
							<div class="col-md-9">
		        				<input type="text" name="Experience_Level_required" id="Experience_Level_required" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Preffered Bikes</label>
							<div class="col-md-9">
		        				<input type="text" name="Preffered_Bikes" id="Preffered_Bikes" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">General Instructions</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="General_Instructions" id="General_Instructions"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Description</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="Trip_Description" id="Trip_Description"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" class="form-horizontal" enctype="multipart/form-data" id="bikation-trip-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Trip</h4>
		      		</div>
		      		<div class="modal-body">
		        		<input type="hidden" name="Vendor_ID" id="Vendor_ID" value="" class="form-control"/>
		        		<div class="form-group">
							<label class="col-md-3">Trip Type</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_type" id="Trip_type" class="form-control"/>
		        			</div> 
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_Name" id="Trip_Name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created On</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_On" id="Created_On" class="form-control date_time "/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_by" id="Created_by" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Approve Requested On</label>
							<div class="col-md-9">
		        				<input type="text" name="Approve_requested_on" id="Approve_requested_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Approved By</label>
							<div class="col-md-9">
		        				<input type="text" name="Approved_By" id="Approved_By" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group" hidden>
							<label class="col-md-3">Approved On</label>
							<div class="col-md-9">
		        				<input type="text" name="Approved_on" id="Approved_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled Request On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_Request_on" id="	Cancelled_Request_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancel Approved By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancel_approved_by" id="Cancel_approved_by" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_on" id="Cancelled_on" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">Total No Of Tickets</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_No_of_Tickets" id="Total_No_of_Tickets" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group hide">
							<label class="col-md-3">No Of Tickets Available</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_tickets_available" id="No_of_tickets_available" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Start Date</label>
							<div class="col-md-9">
		        				<input type="text" name="TripStartDate" id="TripStartDate" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip End Date</label>
							<div class="col-md-9">
		        				<input type="text" name="TripEndDate" id="TripEndDate" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Start Location</label>
							<div class="col-md-9">
		        				<input type="text" name="Start_Location" id="Start_Location" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">End Location</label>
							<div class="col-md-9">
		        				<input type="text" name="End_Location" id="End_Location" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Distance</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Distance" id="Total_Distance" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Riding Hours </label>
							<div class="col-md-9">
		        				<input type="text" name="Total_riding_hours" id="	Total_riding_hours" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No Of Stops</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_stops" id="No_of_stops" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Ticket Denomination</label>
							<div class="col-md-9">
		        				<input type="text" name="ticket_denomination" id="ticket_denomination" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Experience Level Required</label>
							<div class="col-md-9">
		        				<input type="text" name="Experience_Level_required" id="Experience_Level_required" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Preffered Bikes</label>
							<div class="col-md-9">
		        				<input type="text" name="Preffered_Bikes" id="Preffered_Bikes" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">General Instructions</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="General_Instructions" id="General_Instructions"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Description</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="Trip_Description" id="Trip_Description"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
	
@stop	
