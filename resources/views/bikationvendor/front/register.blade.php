<!--!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/css/admin-style.css" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>

		<script type="text/javascript">
		$(document).ready(function(){
			  $("#submit").click(function (e) {

	        var password =$('#password').val();
	        var confirmPassword = $('#confirmPassword').val();
	        if (password != confirmPassword) {
	            alert("Passwords do not match.");
	            return false;
	        }
		});
		});
		</script>
	</head-->
	<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">

    <title>Register Vendor | Bikation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/css/admin-style.css" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <style type="text/css">
    /* Credit to bootsnipp.com for the css for the color graph */
	.colorgraph {
	  height: 5px;
	  border-top: 0;
	  background: #c4e17f;
	  border-radius: 5px;
	  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
	}
    </style>
   
    
</head>
	<body class="login-pg">
		<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form class="form" method="post" action="/bikation-vendor/register" name="vendor_signup_form" id="vendor_signup_form">
		
		<div class="login-container register-vendor">
		@if(Session::has('message'))
		<div class="alert alert-{{ Session::get('message-type') }} alert-dismissable">
			<i class="fa fa-check"></i> {{ Session::get('message') }}
		</div>
		@endif
			<h2>Please Sign Up </h2>
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="first_name" id="firstName" placeholder="First Name" tabindex="1">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="last_name" id="lastName" class="form-control input-lg" placeholder="Last Name" tabindex="2">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="email" id="email" placeholder="Email" tabindex="3">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="mobile_num" id="mobileNumber" class="form-control input-lg" placeholder="Mobile Number" tabindex="4">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Door_No" id="Door_No" placeholder="Door No" tabindex="7">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="Street_1" id="Street_1" class="form-control input-lg" placeholder="Street 1" tabindex="8">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Street_2" id="Street_2" placeholder="Street 2" tabindex="9">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="Area" id="Area" class="form-control input-lg" placeholder="Area" tabindex="10">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="City" id="City" placeholder="City" tabindex="11">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text"  name="State" id="State" class="form-control input-lg" placeholder="State" tabindex="12">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Country" id="Country" placeholder="Country" tabindex="13">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text"  name="ZipCode" id="ZipCode" class="form-control input-lg" placeholder="ZipCode" tabindex="14">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Name" id="Name" placeholder="Contact Name" tabindex="15">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
					<input type="text"  name="Alternate_Number" id="Alternate_Number"  class="form-control input-lg" placeholder="Alternate Number" tabindex="18">
						<input type="hidden" name="LandLine_Number" id="LandLine_Number" class="form-control input-lg" placeholder="LandLine Number" tabindex="16">
						<input type="hidden" name="Phone_Number" id="Phone_Number" class="form-control input-lg" placeholder="Phone Number" tabindex="16">
					</div>
				</div>
			</div>
			<!--div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Phone_Number" id="Phone_Number" placeholder="Phone Number" tabindex="17">	
                  </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text"  name="Alternate_Number" id="Alternate_Number"  class="form-control input-lg" placeholder="Alternate Number" tabindex="18">
					</div>
				</div>
			</div-->
			<div class="form-group">
				<input class="form-control input-lg" type="text" name="Website" id="Website" placeholder="Website" tabindex="19">	
						
			</div>
			
			<div class="form-group">
				<textarea class="form-control input-lg" name="about_me" id="about_me" placeholder="About Me"></textarea>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</div>
			
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				<input  class="btn btn-primary btn-block btn-lg" type="submit" id="submit" value="Register" tabindex="20">
				<!--input type="submit" value="Register" tabindex="20"--></div>
				<div class="col-xs-12 col-md-6"><a href="/bikation-vendor" class="btn btn-success btn-block btn-lg">Sign In</a></div>
			</div>
		</div>	
		</form>
		

	</div>
</div>

</div>
	
	<!--div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="form-group">
						<input class="form-control input-lg" type="text" name="Website" id="Website" placeholder="First Name" required tabindex="1">	
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
				</div>
			</div-->
			<!--div class="form-group">
				<input type="text" name="display_name" id="display_name" class="form-control input-lg" placeholder="Display Name" tabindex="3">
			</div>
			<div class="form-group">
				<input type="email"  name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="4">
			</div-->
	</body>
	<!--body class="login-pg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="login-container">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif

						<div class="img"><img src="/img/admin3.png" /></div>
						<form class="form" method="post" action="/bikation-vendor/register" id="signup-form">
							
								<div class="login_for_booking">
									<h4>New Vendor Register</h4>
									<div id="signup-msg"></div>
									<div class="clear_both"></div>
									<input class="form-control" type="text" name="first_name" id="firstName" placeholder="first name" required>
									<input class="form-control" type="text" name="last_name" id="lastName" placeholder="last name">
									<input class="form-control" type="text" name="email" id="email" placeholder="email id" required>
									<input class="form-control" type="text" name="mobile_num" id="mobileNumber" placeholder="mobile number" required>
									<input class="form-control" type="password" name="password" id="password" placeholder="password" required>
									<input class="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="confirm password" required>
									<input class="form-control" type="text" name="Door_No" id="Door_No" placeholder="Door No">
									<input class="form-control" type="text" name="Street_1" id="Street_1" placeholder="Street 1">
									<input class="form-control" type="text" name="Street_2" id="Street_2" placeholder="Street 2">
									<input class="form-control" type="text" name="Area" id="Area" placeholder="Area" required>
									<input class="form-control" type="text" name="City" id="City" placeholder="City" required>
									<input class="form-control" type="text" name="State" id="State" placeholder="State" required>
									<input class="form-control" type="text" name="Country" id="Country" placeholder="Country" required>
									<input class="form-control" type="text" name="ZipCode" id="ZipCode" placeholder="ZipCode" required>
									<input class="form-control" type="text" name="Name" id="Name" placeholder="Name">
									<input class="form-control" type="text" name="LandLine_Number" id="LandLine_Number" placeholder="LandLine Number">
									<input class="form-control" type="text" name="Phone_Number" id="Phone_Number" placeholder="Phone Number">
									<input class="form-control" type="text" name="Alternate_Number" id="Alternate_Number" placeholder="Alternate Number">
									<input class="form-control" type="text" name="Website" id="Website" placeholder="Website" required>
									
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="common_btn_big"><span></span><input class="submit" type="submit" id="submit" value="create an account"></div>
								</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</body-->
	  <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
	 <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
</html>
