@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Itinerary</h2>
		<div class="row">
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Itinerary</button>
		</div>
		
		<form method="post" action="/bikation-itinerary" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }} </option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		</div>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Trip Name</th>
					<th>Day</th>
					<!-- <th>Text ID</th> -->
					<!-- <th>Stay at</th> -->
					<th>Image</th>
					<th>Video</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_itineraryList as $bikation_itinerary)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_itinerary['TripName'] }}</td>
						<td>{{ $bikation_itinerary->Day }}</td>
						<!-- <td>{{ $bikation_itinerary->Text_ID }}</td> -->
						<!-- <td>{{ $bikation_itinerary->Stay_at }}</td> -->
						<td>
						@if(!empty($bikation_itinerary->Image_ID)) 
						<img src="{{ $bikation_itinerary->Image_ID }}" width ="50px" />
						@endif
						</td>
						<td>{{ $bikation_itinerary->Video_ID }}</td>
						<td>
							@if( $bikation_itinerary->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							<button class="btn btn-info  bikeModel_edit_btn"  data-id="{{ $bikation_itinerary->id }}" data-tripid="{{ $bikation_itinerary->Trip_ID }}" data-day="{{ $bikation_itinerary->Day }}" data-textid="{{ $bikation_itinerary->Text_ID }}" data-stayat="{{ $bikation_itinerary->Stay_at }}" data-image="{{ $bikation_itinerary->Image_ID }}" data-videoid="{{ $bikation_itinerary->Video_ID }}" data-status="{{ $bikation_itinerary->Status }}" ><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-danger bikeModel_delete_btn"  data-id="{{ $bikation_itinerary->id }}"><i class="fa fa-close	"></i> Delete</button>
						</td>
					</tr>
					<?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Itinerary.BikationItinerary.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/bikation-itinerary/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-itinerary-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Itinerary</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
											<?php $datetime1 = date_create($bikation_trip->Trip_Start_Date);
                                           $datetime2 = date_create($bikation_trip->Trip_End_Date);
                                           $interval = date_diff($datetime1, $datetime2);
                                           ?>
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }} (<?php  echo $interval->format('%a Days'); ?>)</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Day</label>
							<div class="col-md-9">
		        				<input type="text" name="Day" id="Day" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Text ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Text_ID" id="Text_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Stay at</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="Stay_at" id="Stay_at"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Image</label>
							<div class="col-md-9">
								<input type="file" id="Image_ID" name="Image_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Video ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Video_ID" id="Video_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data" id="bikation-itinerary-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Itineray</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name:</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" id="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
											<?php $datetime1 = date_create($bikation_trip->Trip_Start_Date);
                                           $datetime2 = date_create($bikation_trip->Trip_End_Date);
                                           $interval = date_diff($datetime1, $datetime2);
                                           ?>
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }} (<?php  echo $interval->format('%a Days'); ?>)</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Day</label>
							<div class="col-md-9">
		        				<input type="text" name="Day" id="Day" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Text ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Text_ID" id="Text_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Stay at</label>
							<div class="col-md-9">
		        				<textarea class="form-control textarea" rows="3" name="Stay_at" id="Stay_at"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="row">
		        				<div class="col-md-5">
									<label class="col-md-6">Add New Image :</label>
									<div class="col-md-6">
				        				<input type="file" name="Image_ID" id="Image_ID"/>
				        			</div>
		        				</div>
		        				<div class="col-md-1"></div>
		        				<div class="col-md-6">
				        			
									<div class="col-md-6">
				        				<div id="old_coverImage_holder"></div>
				        			</div>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Video ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Video_ID" id="Video_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
@stop	