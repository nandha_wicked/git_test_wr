@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Address Details</h2>
		<!--button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button-->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)	
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	
		<form method="post" action="/bikation-address/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-address-add">
		      		<div class="modal-header">
		        		<!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button-->
		        		<!--h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4-->
		      		</div>
		      		<div class="modal-body">
						<div class="form-group" hidden>
							<label class="col-md-3">Valid From :</label>
							<div class="col-md-9">
		        				<input type="hidden" name="Vendor_ID" value="{{ isset($bikation_address->Vendor_ID) ? $bikation_address->Vendor_ID : ''  }}" class="form-control date_time" />
		        				<input type="text" name="Valid_From" value="{{ isset($bikation_address->Valid_From) ? $bikation_address->Valid_From : '' }}" class="form-control date_time" />
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Valid To :</label>
							<div class="col-md-9" >
		        				<input type="text" name="Valid_To" value="{{ isset($bikation_address->Valid_To) ? $bikation_address->Valid_To : '' }}" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_By" value="{{ isset($bikation_address->Created_By) ? $bikation_address->Created_By : '' }}" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Updated By</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_By" value="{{ isset($bikation_address->Updated_By) ? $bikation_address->Updated_By : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Created On:</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_On" value="{{ isset($bikation_address->Created_On) ? $bikation_address->Created_On : '' }}" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Updated On</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_On" value="{{ isset($bikation_address->Updated_On) ? $bikation_address->Updated_On : '' }}" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Door No</label>
							<div class="col-md-9">
		        				<input type="text" name="Door_No" value="{{ isset($bikation_address->Door_No) ? $bikation_address->Door_No : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Street 1</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_1" value="{{ isset($bikation_address->Street_1) ? $bikation_address->Street_1 : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Street 2</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_2" value="{{ isset($bikation_address->Street_2) ? $bikation_address->Street_2 : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area :</label>
							<div class="col-md-9">
		        				<input type="text" name="Area" value="{{ isset($bikation_address->Area) ? $bikation_address->Area : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">City</label>
							<div class="col-md-9">
		        				<input type="text" name="City" value="{{ isset($bikation_address->City) ? $bikation_address->City : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">State</label>
							<div class="col-md-9">
		        				<input type="text" name="State" value="{{ isset($bikation_address->State) ? $bikation_address->State : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Country</label>
							<div class="col-md-9">
		        				<input type="text" name="Country" value="{{ isset($bikation_address->Country) ? $bikation_address->Country : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">ZipCode:</label>
							<div class="col-md-9">
		        				<input type="text" name="ZipCode" value="{{ isset($bikation_address->ZipCode) ? $bikation_address->ZipCode : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Name" value="{{ isset($bikation_address->Name) ? $bikation_address->Name : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">LandLine Number</label>
							<div class="col-md-9">
		        				<input type="text" name="LandLine_Number"  value="{{ isset($bikation_address->LandLine_Number) ? $bikation_address->LandLine_Number : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Phone Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Phone_Number" value="{{ isset($bikation_address->Phone_Number) ? $bikation_address->Phone_Number : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Alternate Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Alternate_Number" value="{{ isset($bikation_address->Alternate_Number) ? $bikation_address->Alternate_Number : '' }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email</label>
							<div class="col-md-9">
		        				<input type="text" name="Email" value="{{ Session::get('email') }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Website</label>
							<div class="col-md-9">
		        				<input type="text" name="Website" value="{{ isset($bikation_address->Website) ? $bikation_address->Website : '' }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">About me</label>
							<div class="col-md-9">
							<textarea class="form-control input-lg" name="about_me" id="about_me">{{ isset($bikation_address->about_me) ? $bikation_address->about_me : '' }}</textarea>

		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<!--button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button-->
		      		</div>
		    	</form>
	</div>

	

@stop		

