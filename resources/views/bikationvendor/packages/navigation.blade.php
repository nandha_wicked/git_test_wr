<div id="fixedtop1">
<div id="center250a">
<header>
	@if (isset(Auth::user()->first_name))                        
	       <div>
                <a class="btn-floating btn-large waves-effect waves-light orange" href="#" data-toggle="collapse" data-target="#userActions"><i class="large fa-bars"></i></a>
					<ul id="userActions" class="collapse">
						<li><a href="/edituser" title="Profile"><i class="i-profile"></i>Profile</a></li>
						<li><a href="/logout" title="Logout"><i class="i-logout"></i>Logout</a></li>
					</ul>
            </div>
	            <?php 
	            	$user_id = Auth::user()->id;
	            	Session::put('user_id', $user_id);
	            ?>
	        	                        
	    @else                       
    
             <a class="btn-floating btn-large waves-effect waves-light orange" href="#" data-toggle="modal" data-target="#addBikeModel-modal"><i class="large fa-user"></i></a>
	    @endif
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '199527113897528'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=199527113897528&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Analytics New Starts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68504157-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics New Ends -->

</header>		
</div>
</div>
@if(!isset(Auth::user()->id))
 <div class="modal fade userModel packgepopup" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                        <div id="content" style="width:100%; margin:0 auto;">
                            <ul id="tabs" class="nav nav-tabs col-xs-12 no-padding" data-tabs="tabs">
                                <li class="active col-xs-12 col-lg-6 no-padding text-center"><a href="#login" data-toggle="tab"><span class="one" data-toggle="span">1</span>Login</a></li>
                                <li class=" col-xs-12 col-lg-6 no-padding text-center"><a href="#register" data-toggle="tab"><span class="two" data-toggle="span">2</span>Register</a></li>
                            </ul>
                            <div id="my-tab-content" class="tab-content">
                                <div class="tab-pane active col-xs-12 no-padding" id="login">
                                    <form method="post" action="" class="form-horizontal" accept-charset="UTF-8" id="myform-login" >
                                     <div class="col-xs-12 bg-grey-100">
                                        <div id="login-msg"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">Email Address <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address">
                                            </div>
                                         </div> <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="password" name="password" placeholder="Password">
                                                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                                                <input type="hidden" name="Trip_url" value="">
                                            </div>
											
                                           </div>
										   
											<div class="col-xs-12 col-sm-12 col-md-6">
                                               <p class = "list-group-item-text text-right"><a href="#" id="forgot-btn" class="forgot-btn"><span class="indicator">Forgot your password?</span></a></p>
                                            </div>
											 <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-2">
                                        </div>
                                        <div class="col-xs-12 col-lg-10">
                                            <button class="btn bg-grey-600 white btnUpdate" id="login-btn">Login</button>
                                        </div>
                                    </div>
									
											
                                    </div>
                                   </form>
								   
								   <form method="post" action="/password/email" class="form-horizontal" accept-charset="UTF-8" id="myform-forgot_pass" style="display:none;">
                                     <div class="col-xs-12 bg-grey-100">
                                        <div id="login-msg"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">ENTER LOGIN EMAIL <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address">
                                            </div>
											
											
                                         </div> 
										 <div class="col-xs-12 col-sm-12 col-md-6">
                                               <p class = "list-group-item-text text-right"><a href="#" id="return-login" class="return-btn"><span class="indicator">Return to login</span></a></p>
                                            </div>
										    <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-2">
                                        </div>
                                        <div class="col-xs-12 col-lg-10">
											 <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                                            <button class="btn bg-grey-600 white btnUpdate" id="login-btn">Send Password Reset Link</button>
                                        </div>
                                    </div>
									
											
                                    </div>
                                   </form>
                                </div>
                                <div class="tab-pane col-xs-12 no-padding" id="register">
                                    <form id="myform-register" name="frmTasks" method="post" action="{{ url('trip/adduser') }}" class="form-horizontal">
                                     <div class="col-xs-12 bg-grey-100 no-padding">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div id="register-msg"></div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="first_name" class="control-label">First Name <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First name" value="" required>
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="Last_name" class="control-label">Last Name <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="Last_name" name="last_name" placeholder="Last name" value="" required>
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="gender" class="control-label">Gender <span class="starIcon">*</span></label>
                                                <select name="gender" id="gender" class="form-control">
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="email_add" class="control-label">Email Address <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address" required>
                                            
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="email_add" class="control-label">Mobile Number <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="mobile_num" maxlength="10" name="mobile_num" placeholder="Mobile Num" required>
                                                <input type="hidden" name="Trip_url" value="">
                                                <meta name="_token" content="{!! csrf_token() !!}"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="password" class="control-label">Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="password1" name="password" placeholder="Password" required>
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="confirmPassword" class="control-label">Confirm Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" required>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-4">
                                           
                                        </div>
                                        <div class="col-xs-12 col-lg-8">
                                            <button class="btn bg-grey-600 white btnUpdate" id="send-btn">Register</button>
                                        </div>
                                    </div>
                            </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </div>
	@endif
