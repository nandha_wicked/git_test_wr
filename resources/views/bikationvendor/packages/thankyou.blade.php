<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Thank you">
    <meta name="author" content="">
    <title>Bikaton| Trip</title>
    <link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/css/site.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/vendor/slidepanel/slidePanel.css">
	 <link href="/user/css/style.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/fonts/font-awesome/font-awesome.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/review/expanding.js"></script>
    <script type="text/javascript" src="/review/starrr.js"></script>
    <script src="{{asset('/js/mail.js')}}"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>

    <style type="text/css">
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
          }
          .stars {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
          }
    </style>
    <style type="text/css">
        .{ padding: 0;}
        .carousel-control, .item{
             border-radius: 4px;
         }
        .caption{
            height: 130px;
            overflow: hidden;
        } 
        .caption h4
        {
            white-space: nowrap;
        }
        .thumbnail img{
          width: 100%;
        }
        .ratings 
        {
            color: #d17581;
            padding-left: 10px;
            padding-right: 10px;
        }
        .thumbnail .caption-full {
        padding: 9px;
        color: #333;
        }
        footer{
          margin-top: 50px;
          margin-bottom: 30px;
        }
    </style>
    <style type="text/css">
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
          }
          .stars {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
          }
    </style>
    
   

</head>
<body>
	@include('bikationvendor/packages/navigation')
    <div class="container">
        <section class="header" style="background-image: url(/assets/images/banner.jpg);">
            <div class="row profileDetail text-center">
                <h1 class="userName" style="padding-top: 137px;">BIKATION</h1>
                <div class="userContact">
                    <ul style="margin-top: 0px; margin-bottom: 199px;">
                        
                    </ul>
                </div>
            </div>
            <!--img src="../assets/images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
        </section>
        <div class="breadcrumb">
            <div class="col-xs-12">
                <a>Home</a>
                <label>></label>
                <a href="/trip">Bikation</a>
               
            </div>
        </div>
        <div class="orangeBand">
        </div>
        <div class="container mega-border">
            
			
			 
        <div class="row">
          <div class="col-xm-12 col-md-6 referImg text-center">
            <img src="/user/images/referImg.png" alt="Refer a friend">
            <p>Refer now to start earning your free trip!</p>
          </div>
          
          <div class="col-xm-12 col-md-6 referSec">
            <div class="col-md-11">
              <h1>Refer a Friend</h1>
              <p>Now you can earn WickedRide credits by inviting your friends. <br>
  If your friend becomes a customer, you’ll get 10% of their first ride as credits and your friend gets 10% off.</p>

              <form class="sendMail" method="POST" action="/sendmail" accept-charset="UTF-8" id="myform">
                <div class="form-group">
                    <div class="controls">
                      <label for="exampleInputEmail1">Send an E-mail to your friends</label>
                      <i class="emailIcon"></i>
                    <input id="email_send" class="form-control" placeholder="friends_name@example.com" name="email_send" type="text" value="">
                    </div>
                </div>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                <button class="send-btn btn btn-default subBtn" type="button">send email</button>
                <label id="lodder"></label>
              </form> 
              <div class="social">
                <p>Share via social media</p>
                <ul>
                  <li class="fb"><a href="#">Facebook</a></li>
                  <li class="twitter"><a href="#">Twitter</a></li>
                  <li class="tmlr"><a href="#">Tumblr</a></li>
                </ul>
              </div>
              <a href="#" class="readmore arrowLhs">Read our Terms & Conditions</a>
            </div>
          </div>  

        </div>

        <div class="referShare">
          <p>Or share your unique referral link in your Blog, E-mail or Instant Messenger <a href="#">< http://www.wickedride.com/signup/?ref=REF ></a></p>
        </div>

            </div>
                       
             
       
<div class="modal popBox2" id="myModal_thank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <h1>Trip Booked SuccsessFully!</h1>
                  <p>Check out Wicked Ride to rent Motorcycles.</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div>
	
	

<div class="modal fade popBox2" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <h1>Invitation Sent!</h1>
                  <p>Check out Wicked Ride to rent Motorcycles.</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div>
	
	
    </div>

	<script>
	$('#myModal_thank').modal('show'); 

	</script>
</body>

</html>
