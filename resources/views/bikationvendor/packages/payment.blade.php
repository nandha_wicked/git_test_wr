<script>
 var razorpay_options = {
    key: "<?php echo env('RAZOR_PAY_PUBLIC');?>",
    amount: "<?php echo $_POST['Total_Cost_return']; ?>",
    name: "<?php echo $_POST['Trip_name']; ?>",
    description: "",
    netbanking: true,
    currency: "INR",
    prefill: {
      name:"<?php echo $_POST['first_name']; ?> ",
      email: "<?php echo $_POST['email']; ?>",
      contact: "<?php echo $_POST['mobile_num']; ?>"
    },
    image:"/images/wicked-ride-logo.png",
    notes: {
     opencart_order_id: "<?php echo $_POST['Booking_ID']; ?>",
	   trip_id: "<?php echo $_POST['Trip_ID']; ?>"
    },
    handler: function (transaction) {
    /*alert(transaction.razorpay_payment_id);*/
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    },
    modal: {
        ondismiss: function(){

           $.ajax({
                  type: "post",
                    url: "/cancel_payment",
                    data : { 
                                        'Booking_ID':"<?php echo $_POST['Booking_ID']; ?>",
                                        'Trip_ID':"<?php echo $_POST['Trip_ID']; ?>",
                                        'amount':"<?php echo $_POST['Total_Cost_return']; ?>",
                                        '_token': $('#frmTasks-addTrip input[name=_token]').val()
                                   },
                    success: function(data){
                                        if(data.success)
                                        {
                                            
                                         }else{
                                          
                                        }    

                                        },
                                    });
        }
    }
  };
  var razorpay_submit_btn, razorpay_instance;
  function razorpaySubmit(el){
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "confirm";
        }
      }
      razorpay_instance = new Razorpay(razorpay_options);
      razorpay_instance.open();
    }
  }
</script>
<script>
  razorpaySubmit();
 
 
 </script>
<form name="razorpay-form" id="razorpay-form" action="{{ url('update_payment_demo_post') }}" method="POST">
  <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
  <input type="hidden" name="Booking_ID" id="Booking_ID" value="<?php echo $_POST['Booking_ID']; ?>"/>
    <input type="hidden" name="Trip_url" id="Trip_url" value="<?php echo $_POST['Trip_url']; ?>"/>

  <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<div class="buttons">
  <div class="pull-right">
    <input type="submit" onclick="razorpaySubmit(this);" value="confirm" class="btn btn-primary" />
  </div>
</div>
