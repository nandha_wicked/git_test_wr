<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
     <!-- Facebook OG Code -->
    <meta property="fb:app_id" content="700892750012103" />
    <meta property="og:url" content="trip/{{ $packageDetail->Trip_url }}" />
    <meta property="og:title" content="{{ $packageDetail->Trip_Name }}"/>
    <meta property="og:image" content="http:{{ $trip_phtos[0]->Media_URL_trip2}} "/>
    <meta property="og:site_name" content="WickedRide.com/trip"/>
    <meta property="og:description" content="{{ strip_tags($packageDetail->Trip_Description) }}"/>
    <!-- End Facebook OG Code -->
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="{{ $packageDetail->Trip_Name }}">
    <meta name="author" content="">
    <title>{{ $packageDetail->Trip_Name }}</title>
    <link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/css/site.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/vendor/slidepanel/slidePanel.css">
     <link href="/user/css/style.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/fonts/font-awesome/font-awesome.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/review/expanding.js"></script>
    <script type="text/javascript" src="/review/starrr.js"></script>
    <script src="{{asset('/js/mail.js')}}"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="/css/materialize.css">

  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
     <!-- Compiled and minified JavaScript -->     

    <style type="text/css">
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
          }
          .stars {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
          }
    </style>
    <style type="text/css">
        .{ padding: 0;}
        .carousel-control, .item{
             border-radius: 4px;
         }
        .caption{
            height: 130px;
            overflow: hidden;
        } 
        .caption h4
        {
            white-space: nowrap;
        }
        .thumbnail img{
          width: 100%;
        }
        .ratings 
        {
            color: #d17581;
            padding-left: 10px;
            padding-right: 10px;
        }
        .thumbnail .caption-full {
        padding: 9px;
        color: #333;
        }
        footer{
          margin-top: 50px;
          margin-bottom: 30px;
        }
    </style>
    <style type="text/css">
         .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
          }
          .stars {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
          }


  .custom-radio [type="radio"]:not(:checked), [type="radio"]:checked {
    position: absolute;
    opacity: 1;
    left: auto;
    margin-left: -10px;
}
.custom-radio span{
    margin: 0 10px;
    line-height: 20px;
}
    </style>
    <script type="text/javascript">
    $(function(){


      // initialize the autosize plugin on the review text area
      $('#new-review').autosize({append: "\n"});
      var reviewBox = $('#post-review-box');
      var newReview = $('#new-review');
      var openReviewBtn = $('#open-review-box');
      var closeReviewBtn = $('#close-review-box');
      var ratingsField = $('#ratings-hidden');

      openReviewBtn.click(function(e)
      {
        reviewBox.slideDown(400, function()
          {
            $('#new-review').trigger('autosize.resize');
            newReview.focus();
          });
        openReviewBtn.fadeOut(100);
        closeReviewBtn.show();
      });

      closeReviewBtn.click(function(e)
      {
        e.preventDefault();
        reviewBox.slideUp(300, function()
          {
            newReview.focus();
            openReviewBtn.fadeIn(200);
          });
        closeReviewBtn.hide();
        
      });

    // Bind the change event for the star rating - store the rating value in a hidden field
      $('.starrr').on('starrr:change', function(e, value){
        ratingsField.val(value);
        var star= value;
        $("#star_reting").val(star);
       });
       $('select').on('change',function(){
        price_calculation();
        });
    });
    function price_calculation()
        {
            var rentBike = $('input[name=Vehicle]:checked').val()
            var No_of_Tickets_Cost_1px =  $("#No_of_Tickets_Cost_1px").val();
            var No_of_Tickets_Cost_2px =  $("#No_of_Tickets_Cost_2px").val();
          $.ajax({
                  type: "post",
                    url: "/price_calculation",
                    data : { 
                                        'Trip_ID':$('#frmTasks-addTrip input[name=Trip_ID]').val(),
                                        'No_of_Tickets_Cost_1px':No_of_Tickets_Cost_1px,
                                        'No_of_Tickets_Cost_2px':No_of_Tickets_Cost_2px,
                                        '_token': $('#frmTasks-addTrip input[name=_token]').val(),
                                        'rent_bike': rentBike
                                   },
                    success: function(data){
                                        if(data.success)
                                        {
                                             $("#total_price").html(data.total_cost+'/-');
                                             $("#Total_Cost").val(data.total_cost);
                                         }else{
                                            
                                        }    

                                        },
                                    });
                                
      
            }
   </script>
   
    
   
 
</head>
<body>
@include('bikationvendor/packages/navigation')
    <div class="container">
        <section class="header hidden-xs" style="background-image: url({{$trip_banner->image}});background-repeat: no-repeat;background-size:cover;">
            <div class="row profileDetail text-center">
				<div class="text-shade_b">
                <h1 class="userName">BIKATION</h1>
                <div class="userContact info">
                    <ul style="margin-top: 0px; margin-bottom: 100px;">
                        
                    </ul>
                </div>
				</div>
            </div>
            <!--img src="../assets/images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
        </section>
        @if(count($errors) > 0)
            <div style="margin:20px"></div>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="breadcrumb">
            <div class="col-xs-12">
                <a>Home</a>
                <label>></label>
                <a href="/trip">Bikation</a>
                <label>></label>
                <a class="active">{{ $packageDetail->Start_Location }} to {{ $packageDetail->End_Location }}</a>
            </div>
        </div>
        <div class="orangeBand">
        </div>
        <div class="container mega-border">
            <div class="col-xs-12 col-md-12 no-padding">
                <div class="col-xs-12 col-md-6 no-padding">
                    <!--img src="{{ $packageDetail->Media_URL }}" class="img-responsive" /-->
                    <div class="carousel slide" id="exampleCarouselDefault" data-ride="carousel">
                        <ol class="carousel-indicators carousel-indicators-fall">
                        <?php $i=0; ?>
                        @foreach($trip_phtos as $key => $trip_phto) 
                            @if($i == 0)    
                                <li class="active" data-slide-to="{{$i}}" data-target="#exampleCarouselDefault"></li>
                            @else   
                                <li class="" data-slide-to="{{$i}}" data-target="#exampleCarouselDefault"></li>
                            @endif      
                        <?php $i++; ?>  
                         @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                        <?php $j=0; ?>
                        @foreach($trip_phtos as $trip_phto) 
                            @if($j == 0)    
                                <div class="item active">
                                    <img class="width-full img-responsive" src="{{ $trip_phto->Media_URL_details }}" alt="">
                                </div>
                            @else   
                                <div class="item">
                                    <img class="width-full img-responsive" src="{{ $trip_phto->Media_URL_details }}" alt="" >
                                </div>
                            @endif      
                        <?php $j++; ?>      
                         @endforeach
                            
                            
                            <!--div class="item">
                                <img class="width-full" src="{{ $packageDetail->Media_URL }}" alt="..." class="img-responsive">
                            </div-->
                        </div>
                        <a class="left carousel-control" href="#exampleCarouselDefault" role="button" data-slide="prev">
                            <span class="icon wb-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#exampleCarouselDefault" role="button" data-slide="next">
                            <span class="icon wb-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 padding-horizontal-25">
                   <h3 class="orange-600 padding-horizontal-20">{{ $packageDetail->Trip_Name }}</h3>
                    <h4 class="padding-horizontal-20">by <a style="color: #62a8ea;" href="#" data-toggle="modal"> <!--data-target="#vendor_details"--> {{ ucfirst($packageDetail->first_name) }} {{ ucfirst($packageDetail->last_name) }}</a></h4>
					
                    <div class="rating col-xs-12 m-v">
                       <div class="col-xs-4 pull-left padding-left-0">
                        @for ($i=1; $i <= 5 ; $i++)
                                <!--<span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_Overall) ? '' : '-empty'}}"></span>-->
                                <img src="/assets/images/{{ ($i <= $total_rating->average_Overall) ? 'orangeStar' : 'whiteStar'}}.png" />
                        @endfor
                        <h6 class="pull-right margin-0">{{ $total_no_rating }} Reviews</h6>
                        </div>
                        
                    </div>
                    <div class="trip_desc">
                         @if(!empty($packageDetail->Trip_Description))
                                        <div class="description col-xs-12">
                         <h5 class="orange-600">Trip Description :</h5>
                                          <div class="padding-horizontal-25"> {!! $packageDetail->Trip_Description !!}</div>
                                        </div>
                         @endif
                         @if(!empty($packageDetail->General_Instructions))
                         <div class="description col-xs-12">
                         <h5 class="orange-600">General Instruction :</h5>
                                          <div class="padding-horizontal-25"> {!! $packageDetail->General_Instructions !!}</div>
                                        </div>
                         @endif
                         </div>
                    <div class="col-xs-12">
                        <h2 class="orange-600"><span class="amount">{{ $packageDetail->Cost_1px }}/-</span><span class="amount-text" >(Inclusive of Taxes)</span></h2>
                    </div>
                    <div class="col-xs-12" style="line-height:40px;">
                        @if( $packageDetail->Tickes_Remaining_Cost_1px  > 0 )
                        <img src="/assets/images/friends.png" />
                        <!--img src="/assets/images/friends.png" />
                        <label>{{ $packageDetail->Tickes_Remaining_Cost_2px }}/{{ $packageDetail->No_of_Tickets_Cost_2px }} Seats Available For Cost 2px</label-->
                          @if($packageDetail->Trip_Start_Date < Carbon\Carbon::today())
                              No Tickets Available For This Ride
                          @else
                          <label>{{ $packageDetail->Tickes_Remaining_Cost_1px }}/{{ $packageDetail->No_of_Tickets_Cost_1px }} Seats Available</label>
                          <button class="btn bg-grey-600 white btnUpdate pull-left margin-right-20 margin-bottom-10"  data-toggle="modal" data-target="#addBikeModel-modal">Book Now</button>
                          @endif
                        @else
                        No Tickets Available For This Ride
                        @endif

                    </div>
                     </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 no-padding">
               <div class="col-xs-12 col-md-6 no-padding">
                <div class="col-xs-12 bg-grey-700 padding-10">
                       <div class="col-xs-12 col-lg-6 white">
                            <h5 class="orange-600"><img class="margin-right-15" src="/assets/images/directions.png"/>Destination</h5>
                            <p>{{ $packageDetail->End_Location }}
                                <br/>({{ $packageDetail->Total_Distance }} Kms)</p>
                        </div>
                        <div class="col-xs-12 col-lg-6 white">
                            <h5 class="orange-600"><img class="margin-right-15" src="/assets/images/calendar.png"/>Date &amp; Time</h5>
                            <p>{{ date('d M, Y H:i:s', strtotime($packageDetail->Trip_Start_Date)) }} <br>{{ date('d M, Y H:i:s', strtotime($packageDetail->Trip_End_Date)) }}</p>
                        </div>
                    </div>
                    <div class="col-xs-12 bg-grey-700 padding-10">
                        <div class="col-xs-12 col-lg-6 white">
                            <h5 class="orange-600"><img class="margin-right-15" src="/assets/images/flag.png"/>Meeting Point</h5>
                            <p>{{ $packageDetail->Start_Location }}</p>
                        </div>
                        <div class="col-xs-12 col-lg-6 white">
                            <h5 class="orange-600"><img class="margin-right-15" src="/assets/images/recommend.png"/>Recomended Bikes</h5>
                            <p>{{ $packageDetail->Preffered_Bikes }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 bg-grey-200">
                    <div class="col-xs-12">
                        @if(isset($ride_include_all))
						
                        <div class="col-xs-12 col-lg-6">
						<div class="ride_addon">	
                            <h5 class="orange-600"><img class="margin-right-15"  src="/assets/images/include.png"/>RIDE INCLUDES</h5>
                            <!--p>{{ $ride_include['Activity_ID'] or '' }} </p-->
							@foreach($ride_include_all as $ride_include)
							 {{ $ride_include->Activity_ID }} <br>
							@endforeach
                        </div>
						</div>
                        @endif
                       @if(isset($ride_exclude_all))
							
                        <div class="col-xs-12 col-lg-6">
						<div class="ride_addon">
                            <h5 class="orange-600"><img class="margin-right-15"  src="/assets/images/include.png"/>RIDE EXCLUDES</h5>
                            @foreach($ride_exclude_all as $ride_exclude)
							 {{ $ride_exclude->Activity_ID }} <br>
							@endforeach
                        </div>
						</div>
                        @endif
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-lg-6">
                            <h5 class="orange-600"><img class="margin-right-15"  src="/assets/images/rules.png"/>RIDE RULES</h5>
                            <p>1. Helmet is a must for every rider</p>
                                <p>2. Bike stunts are strictly prohibited</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                @if(isset($bikation_itinerary_all))
               <h5 class="orange-600">Trip Itinerary :</h5>
               @foreach($bikation_itinerary_all as $bikation_itinerary)
                <div class="col-xs-12 col-md-6 equalheight">
          <h5 class="orange-600"><img class="margin-right-15" src="/assets/images/calendar.png">{{ $bikation_itinerary->Day }} </h5> 
                      <div class="padding-horizontal-25"> {!! $bikation_itinerary->Stay_at !!}</div>
                </div>
                @endforeach
                @endif
            <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-6 padding-vertical-20">
                <div class="col-xs-12 col-sm-4 col-lg-6">
                    <div class="bigstar">
                            <img class="margin-right-15" src="/assets/images/bigstar.png" />
                        </div>
                        <center class="bigtext"><!--{{ intval($total_rating->average_Overall) }}-->There are no customer reviews yet.</center>
                        <center><!---Average ratings based
                            <br/>on {{ $total_no_rating }} reviews--></center>
                    </div>
                   <div class="col-xs-12 col-sm-4 col-lg-6">
                        <div class="col-xs-12">
                            <img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" />
                            <br/>
                            <div class="progress progress-xs margin-10">
                            <div class="progress-bar progress-bar-info bg-orange-600" style="width: 70.3%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                                <span class="sr-only">70.3%</span>
                            </div>
                        </div>
                        </div>
                        <div class="col-xs-12">
                            <img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" />
                            <br/>
                           <div class="progress progress-xs margin-10">
                            <div class="progress-bar progress-bar-info bg-orange-600" style="width: 70.3%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                                <span class="sr-only">70.3%</span>
                            </div>
                        </div>
                        </div>
                        <div class="col-xs-12">
                            <img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" />
                            <br/>
                            <div class="progress progress-xs margin-10">
                            <div class="progress-bar progress-bar-info bg-orange-600" style="width: 70.3%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                                <span class="sr-only">70.3%</span>
                            </div>
                        </div>
                        </div>
                        <div class="col-xs-12">
                            <img src="/assets/images/greystar.png" /><img src="/assets/images/greystar.png" />
                            <br/>
                            <div class="progress progress-xs margin-10">
                            <div class="progress-bar progress-bar-info bg-orange-600" style="width: 70.3%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                                <span class="sr-only">50.3%</span>
                            </div>
                        </div>
                        </div>
                        <div class="col-xs-12">
                            <img src="/assets/images/greystar.png" />
                            <br/>
                            <div class="progress progress-xs margin-10">
                            <div class="progress-bar progress-bar-info bg-orange-600" style="width: 70.3%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70.3" role="progressbar">
                                <span class="sr-only">30.3%</span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 padding-horizontal-25">
                  
                    <div class="col-xs-12">
                        <h2>Write a Review</h2>
       @if(isset(Auth::user()->id))
        <p>Please rate and share your experiences and thoughts
                            <br/>with your fellow riders below</p> 
       @else   
                       <button class="btn bg-grey-600 white btnUpdate pull-left margin-right-20 margin-bottom-10" data-toggle="modal" data-target="#addBikeModel-modal">Please login Here...</button>
       @endif 
                    </div>
      @if(isset(Auth::user()->id))
                    <form method="POST" action="{{ url('trip/addreviews') }}" class="form-horizontal" enctype="multipart/form-data" id="bikation-rating">
                    <div class="col-xs-12">
                    <div class="starrr" data-rating="{{ Input::old('rating',0 )}}"></div>
                    </div>
                    <div class="col-xs-12">
                        <textarea placeholder="Write your review here" class="form-control has-error margin-top-15 margin-bottom-15" name="Remarks"></textarea>
                        <input type="hidden" name="Trip_ID" value="{{ $packageDetail->Trip_ID }}">
                        <input type="hidden" name="Overall" id="star_reting">
                        <input type="hidden" name="Trip_Name" value="{{ $packageDetail->Trip_Name }}">
                        <input type="hidden" name="Trip_url" value="{{ $packageDetail->Trip_url }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="col-xs-12">
                         <div class="col-xs-12 col-lg-4 margin-vertical-10">
                           @if(isset(Auth::user()->id))
                                <button class="btn btnUpdate width-150 active">Submit</button>
                               @else
                           <button class="btn btnUpdate width-150 active" data-toggle="modal" data-target="#addBikeModel-modal">Submit</button> 
                           @endif
                        </div>
                         <div class="col-xs-12 col-lg-4 margin-vertical-10">
                            <button class="btn btnUpdate width-150">Reset</button>
                        </div>
                    </div>
                    </form>
                    @endif
                </div>
            </div>
             <div class="col-xs-12 col-md-12 margin-top-15 margin-bottom-15">
                @foreach($reviews as $review)
                <div class="col-xs-12 col-md-4 bg-grey-200">
                    <div class="col-xs-12">
                        <h6>{{ date('d F, Y', strtotime($review->created_at)) }}</p></h6></div>
                    <div class="col-xs-12">
                        <h4>{{ $review['first_name'] }} {{ $review['last_name'] }}</h4></div>
                    <div class="rating col-xs-12 margin-top-10 margin-bottom-10 no-padding">
                       <div class="col-xs-6 pull-left">
                        @for ($i=1; $i <= 5 ; $i++)
                                <!--<span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_Overall) ? '' : '-empty'}}"></span>-->
                                <img src="/assets/images/{{ ($i <= $review->Overall) ? 'orangeStar' : 'whiteStar'}}.png" />
                        @endfor
                        </div>
                        <h6 class="pull-left margin-0"></label>
                    </div>
                    <div class="col-xs-12">
                        <p>{{ $review->Remarks }}<!--a href="#"> Read More</a--></p>
                    </div>
                    
                </div>
                @endforeach
            </div>
       
    </div>
@include('bikationvendor/packages/footer')
    @if(isset(Auth::user()->id))
     <div class="modal fade userModel packgepopup" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form id="frmTasks-addTrip" name="frmTasks-addTrip" method="post" action="" class="form-horizontal" novalidate="">
                        <div id="content" style="width:100%; margin:0 auto;">
                            
                            <ul id="tabs" class="nav nav-tabs col-xs-12 no-padding" data-tabs="tabs">
                                <li class=" col-xs-12 col-lg-12 no-padding text-center active"><a href="#checkout" data-toggle="tab">Checkout</a></li>

                            </ul>
                            <div id="my-tab-content" class="tab-content">
                                <div class="tab-pane col-xs-12 no-padding active" id="checkout">
                                    <div class="col-xs-12 no-padding bg-grey-100">
                                        <div class="col-xs-12 col-lg-6 no-padding">
                                            <img src="{{ $packageDetail->Media_URL_trip1 }}" class="img-responsive" />
                                        </div>
                                        <div class="col-xs-12 col-lg-6 no-padding padding-vertical-40 bg-grey-200"><div id="addTrip-msg"></div>
                                            <div class="col-xs-12">
                                                <label class="padding-vertical-10">{{ date('d F, Y H:i:s', strtotime($packageDetail->Trip_Start_Date)) }}</label>
                                            </div>
                                            <div class="col-xs-12">
                                                <h2 class="orange-600 margin-vertical-5">{{ $packageDetail->Trip_Name }}</h2></div>
                                            <!--div class="col-xs-2"><img src="../assets/images/trash.png" /></div-->
                                            <div class="col-xs-12">
                                                <label class="padding-vertical-10 margin-0">by {{ $packageDetail->first_name }} {{ $packageDetail->last_name }}</label>
                                            </div>
                                            <div class="col-xs-12 rating">
                                                <div class="col-xs-5 pull-left padding-left-0">
                                                    @for ($i=1; $i <= 5 ; $i++)
                                                    <img src="/assets/images/{{ ($i <= $total_rating->average_Overall) ? 'orangeStar' : 'whiteStar'}}.png" />
                                                    @endfor
                                                     <h6 class="pull-right margin-0">{{ $total_no_rating }} Reviews</h6>
                                                </div>
                                           <div class="col-xs-12 padding-left-0">
											<div class="col-xs-12 col-lg-7 padding-left-0  padding-right-0">
												<div class="form-group col-xs-12 padding-left-0  padding-right-0 margin-bottom-0">
													<div class="form-group col-xs-8 padding-left-0   padding-right-0">
													<label for="No_of_Tickets_Cost_1px" class="control-label">1x No of Rider<span class="starIcon">*</span></label>
													<select name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control input-small send" style="width:80%;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
														<option value="">Select</option>	
														@for ($i=1; $i <= $packageDetail->Tickes_Remaining_Cost_1px; $i++)
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
														@endfor
													</select>
													</div>
													<div class="form-group col-xs-4 padding-left-0 padding-top-40">
													<label for="No_of_Tickets_Cost_2px" class="control-label">{{$packageDetail->Cost_1px}}/- <h6 class="margin-vertical-10  margin-bottom-0  margin-top-0"><small>(1px Price)</small></h6></label>
													</div>
												</div>
												<div class="form-group col-xs-12 padding-left-0  padding-right-0 margin-top-0">
													 <div class="form-group col-xs-8 padding-left-0   padding-right-0">
													 <label for="No_of_Tickets_Cost_2px" class="control-label">1x No of Couples<span class="starIcon">*</span></label>
													<select name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control send" style="width:80%;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
														<option value="">Select</option>
														@for ($i=1; $i <= $packageDetail->Tickes_Remaining_Cost_2px; $i++)
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
														@endfor
													</select>
													</div>
													<div class="form-group col-xs-4 padding-left-0 padding-top-40">
													<label for="No_of_Tickets_Cost_2px" class="control-label">{{$packageDetail->Cost_2px}}/- <h6 class="margin-vertical-10  margin-bottom-0  margin-top-0"><small>(2px Price)</small></h6></label>
													</div>
												
												</div>
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-5">
                                                <label>PRICE</label>
                                                <label class="text-lg h2 orange-600 margin-vertical-10" id="total_price">{{ $packageDetail->Cost_1px }}/-</label>
                                                <h6 class="margin-vertical-10">(inclusive of all taxes)</h6>
                                            </div>
                                            <div class="col-xs-12 no-padding margin-vertical-10">
                                                        <div class="col-xs-12 col-lg-6">
                                                            <i class="icon fa-exchange margin-right-5 padding-right-5 orange-600" aria-hidden="true"><span class="margin-left-10 bookridetext grey-600">{{ $packageDetail->Total_Distance }} KMS</span></i>
                                                        </div>
                                                        <div class="col-xs-12 col-lg-6">
                                                            <i class="icon fa-calendar margin-right-5 orange-600" aria-hidden="true"><span class="margin-left-10 bookridetext grey-600"><?php 
															   $datetime1 = date_create($packageDetail->Trip_Start_Date);
															   $datetime2 = date_create($packageDetail->Trip_End_Date);
															   $interval = date_diff($datetime1, $datetime2);
															   echo $interval->format('%a Days');?></span></i>
                                                        </div>
                                                        
                                             </div>
                                            <div class="col-xs-12">
											<div class="form-group col-xs-12 col-lg-12 no-padding margin-vertical-10">
                                                            <i class="icon fa-users margin-right-5 orange-600" aria-hidden="true"><span class="margin-left-10 bookridetext grey-600">{{ $packageDetail->Tickes_Remaining_Cost_1px }}/{{ $packageDetail->No_of_Tickets_Cost_1px }}  Remaining Seat Cost 1px</span></i>
											</div>	
													
											</div>
											 <div class="col-xs-12">
											 <div class="form-group col-xs-12 col-lg-12 no-padding margin-vertical-10">
                                                            <i class="icon fa-users margin-right-5 orange-600" aria-hidden="true"><span class="margin-left-10 bookridetext grey-600">{{ $packageDetail->Tickes_Remaining_Cost_2px }}/{{ $packageDetail->No_of_Tickets_Cost_2px }} Remaining Seat Cost 2px  
															
															</span></i>
                                                        </div>
											 </div>
											 <div class="col-xs-12">
											 <div class="form-group col-xs-12 col-lg-12 no-padding margin-vertical-10 custom-radio">
                                                  <label for="Vehicle" class="control-label margin-left-10 bookridetext grey-600">Do you need a rental bike ?</label>
												  <br>
												   <label  class="control-label margin-left-10 bookridetext grey-600">


                            <input type="radio" name="Vehicle" value="Yes" class="margin-right-5 orange-600" checked />
                            <span> Yes</span>
                           </label>
												   <label  class="control-label margin-left-10 bookridetext grey-600">
                            <input type="radio" name="Vehicle" value="No" class="margin-right-5 orange-600"  /> 
                             <span> No</span>
                          </label>
											 </div>
											 </div>


                                        </div>

                                    </div>
                                            <div class="col-md-12 col-xs-12">
                                                <input type="checkbox" name="terms" id="terms" style="    position: relative; left:0; opacity: 1;">  I Agree<a href="/termsandconditions"> Terms & Coditions</a>
                                                <br><br>
                                            </div>
                                    </div>
                                    <div class="col-xs-12 bg-grey-100">
                                         <div class="col-xs-12 col-lg-6">
                                                    <div class="form-group col-xs-12 col-lg-6 error m-v no-padding hide">
                                                        <input type="text" class="form-control has-error" id="Enter Ptomo code" name="Enter Ptomo code" placeholder="Mobile Num" value="" required="">
                                                        <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-6 m-v hide">
                                                        <p>* Note that the code
                                                            <br/> entered should be valid
                                                            <br/> and not expired</p>
                                                    </div>
                                                </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="Trip_ID" value="{{ $packageDetail->Trip_ID }}">
											<input type="hidden" name="Total_Cost" id="Total_Cost" value="{{ $packageDetail->Cost_1px }}">
											<input type="hidden" name="Addon_cost" id="Addon_cost" value="0">
                                            <input type="hidden" name="Package_Cost" id="Package_Cost" value="{{ $packageDetail->Cost_1px }}">
                                            <input type="hidden" name="Booked_Type" value="User" class="form-control"/>
                                            <input type="hidden" name="Trip_url" value="{{ $packageDetail->Trip_url }}">
                                            <button class="btn bg-grey-600 white btnUpdate btn-payment-go" disabled>Proceed to Payment</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div id="razorpayform-block" hidden></div>
    </div>
    </div>
     
   @else
    <!--div class="modal fade userModel packgepopup" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                        <div id="content" style="width:100%; margin:0 auto;">
                            <ul id="tabs" class="nav nav-tabs col-xs-12 no-padding" data-tabs="tabs">
                                <li class="active col-xs-12 col-lg-6 no-padding text-center"><a href="#login" data-toggle="tab"><span class="one" data-toggle="span">1</span>Login</a></li>
                                <li class=" col-xs-12 col-lg-6 no-padding text-center"><a href="#register" data-toggle="tab"><span class="two" data-toggle="span">2</span>Register</a></li>
                            </ul>
                            <div id="my-tab-content" class="tab-content">
                                <div class="tab-pane active col-xs-12 no-padding" id="login">
                                    <form method="post" action="" class="form-horizontal" accept-charset="UTF-8" id="myform-login" >
                                     <div class="col-xs-12 bg-grey-100">
                                        <div id="login-msg"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">Email Address <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address">
                                            </div>
                                         </div> <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="password" name="password" placeholder="Password">
                                                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                                                <input type="hidden" name="Trip_url" value="{{ $packageDetail->Trip_url }}">
                                            </div>
											
                                           </div>
										   
											<div class="col-xs-12 col-sm-12 col-md-6">
                                               <p class = "list-group-item-text text-right"><a href="#" id="forgot-btn" class="forgot-btn"><span class="indicator">Forgot your password?</span></a></p>
                                            </div>
											 <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-2">
                                        </div>
                                        <div class="col-xs-12 col-lg-10">
                                            <button class="btn bg-grey-600 white btnUpdate" id="login-btn">Login</button>
                                        </div>
                                    </div>
									
											
                                    </div>
                                   </form>
								   
								   <form method="post" action="/password/email" class="form-horizontal" accept-charset="UTF-8" id="myform-forgot_pass" style="display:none;">
                                     <div class="col-xs-12 bg-grey-100">
                                        <div id="login-msg"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group col-lg-6">
                                                <label for="email_add" class="control-label">ENTER LOGIN EMAIL <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address">
                                            </div>
											
											
                                         </div> 
										 <div class="col-xs-12 col-sm-12 col-md-6">
                                               <p class = "list-group-item-text text-right"><a href="#" id="return-login" class="return-btn"><span class="indicator">Return to login</span></a></p>
                                            </div>
										    <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-2">
                                        </div>
                                        <div class="col-xs-12 col-lg-10">
											 <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                                            <button class="btn bg-grey-600 white btnUpdate" id="login-btn">Send Password Reset Link</button>
                                        </div>
                                    </div>
									
											
                                    </div>
                                   </form>
                                </div>
                                <div class="tab-pane col-xs-12 no-padding" id="register">
                                    <form id="myform-register" name="frmTasks" method="post" action="{{ url('trip/adduser') }}" class="form-horizontal">
                                     <div class="col-xs-12 bg-grey-100 no-padding">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div id="register-msg"></div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="first_name" class="control-label">First Name <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First name" value="" required>
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="Last_name" class="control-label">Last Name <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="Last_name" name="last_name" placeholder="Last name" value="" required>
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="gender" class="control-label">Gender <span class="starIcon">*</span></label>
                                                <select name="gender" id="gender" class="form-control">
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="email_add" class="control-label">Email Address <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="email_add" name="email" placeholder="Email Address" required>
                                            
                                            </div>
                                            <div class="form-group col-xs-12 col-lg-4 error">
                                                <label for="email_add" class="control-label">Mobile Number <span class="starIcon">*</span></label>
                                                <input type="text" class="form-control has-error" id="mobile_num" maxlength="10" name="mobile_num" placeholder="Mobile Num" required>
                                                <input type="hidden" name="Trip_url" value="{{ $packageDetail->Trip_url }}">
                                                <meta name="_token" content="{!! csrf_token() !!}"/>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="password" class="control-label">Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="password1" name="password" placeholder="Password" required>
                                            </div>
                                            <div class="form-group col-xs-8 error">
                                                <label for="confirmPassword" class="control-label">Conform Password <span class="starIcon">*</span></label>
                                                <input type="password" class="form-control has-error" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" required>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 no-padding m-v bg-grey-100">
                                        <div class="col-xs-4">
                                            
                                        </div>
                                        <div class="col-xs-12 col-lg-8">
                                            <button class="btn bg-grey-600 white btnUpdate" id="send-btn">Register</button>
                                        </div>
                                    </div>
                            </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </div-->
    @endif
   
   	<div class="modal fade" id="vendor_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="">
                  <div class="row">
				  <div class="col-md-12"> 
					<div class="row"> 
						<div class="form-group col-xs-12"> 
						<label class="col-md-4"></label>
							<div class="col-md-8">
								<h4 class="padding-horizontal-20">{{ ucfirst($packageDetail->first_name) }} {{ ucfirst($packageDetail->last_name) }}</h4>
							</div>
						</div>		
					</div>
				 </div>	
				  
				  
				 <div class="col-md-12"> 
					<div class="row"> 
						<div class="form-group col-xs-12"> 
						<label class="col-md-4"><strong>About Vendor:</strong></label>
							<div class="col-md-8">
								 {{$packageDetail->about_me}} 
							</div>
						</div>		
					</div>
				 </div>	 	
				
				 <div class="col-md-12"> 
				  <div class="form-group col-xs-12"> 
				 <label class="col-md-4"><strong>Average Rating:</strong></label>
					<div class="col-md-8">
						<div class="rating">
									   @for ($i=1; $i <= 5 ; $i++)
												<!--<span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_Overall) ? '' : '-empty'}}"></span>-->
												<img src="/assets/images/{{ ($i <= $total_vendor_rating->average_Overall) ? 'orangeStar' : 'whiteStar'}}.png" />
										@endfor
										<h6 class="pull-right margin-0 padding-left-10">{{ $total_no_vendor_rating }} Reviews</h6>
										
										
									</div>
					</div>							
					</div>		
					</div>	
				    </div>
                </div>
              </div>
          </div>
      </div>
    </div> 	
	
   <div class="modal fade popBox2" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  	
				
									 
                </div>
              </div>
          </div>
      </div>
    </div> 	
           
		   

   
    <script src="/assets/vendor/animsition/jquery.animsition.js"></script>
    <script src="/assets/vendor/slidepanel/jquery-slidePanel.js"></script>
    <!-- Scripts -->
    <!--script type="text/javascript" src="/js/jquery.validate.min.js"></script-->
	<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
	 <script type="text/javascript" src="/js/additional-methods.js"></script>
    <script type="text/javascript" src="/js/custom.js?v=1"></script>
    <script src="/assets/js/core.js"></script>
    <script src="/assets/js/site.js"></script>
    <script src="/assets/js/sections/menu.js"></script>
    <script src="/assets/js/sections/menubar.js"></script>
    <script src="/assets/js/sections/sidebar.js"></script>
    <script src="/assets/js/configs/config-colors.js"></script>
    <script src="/assets/js/components/animsition.js"></script>
    <script src="/assets/js/components/slidepanel.js"></script>
    <script src="//masonry.desandro.com/masonry.pkgd.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.0.4/jquery.imagesloaded.js"></script>
    <script type="text/javascript">
	
	
 jQuery.extend(jQuery.validator.messages, {
        require_from_group: jQuery.format("'Please enter either username/ email address to recover password'/Please fill out at least {0} of these fields.")
    });
	
    jQuery(document).ready(function($) {
        $('#tabs').tab();
	
$("#return-login").click(function () {
	$('#myform-forgot_pass').hide();
	$('#myform-login').show();
});	
$("#forgot-btn").click(function () {
	$('#myform-login').hide();
	$('#myform-forgot_pass').show();
});	
//return-login  myform-forgot_pass
//forgot-btn myform-login

    });


         $("#terms").click(function () {
              if ($("#terms").is(':checked')) {
                 $(".btn-payment-go").removeAttr("disabled");
             }

             if (!$("#terms").is(':checked')) {
                 $(".btn-payment-go").attr("disabled", "disabled");
             }
         });
         $('input:radio[name=Vehicle]').change(function() {
             price_calculation();
         });
</script>
    
</body>

</html>
