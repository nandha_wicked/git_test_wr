<!DOCTYPE html>
<html class="no-js before-run" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Packages | Bikation">
    <meta name="author" content="">
    <title>Bikaton| Package</title>
    <link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/css/site.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/vendor/slidepanel/slidePanel.css">
	 <link href="/user/css/style.css" rel="stylesheet">
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/fonts/font-awesome/font-awesome.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
    <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="/css/materialize.css">

  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
     <!-- Compiled and minified JavaScript -->  
    
    <script type="text/javascript">
	$(function(){
	$("#search_text").keyup(function() {
		filter_data('filer-price');
		});
	});
	
	 function showDays(firstDate,secondDate){
                
                  

                  var startDay = new Date(firstDate);
                  var endDay = new Date(secondDate);
                  var millisecondsPerDay = 1000 * 60 * 60 * 24;

                  var millisBetween = endDay.getTime() - startDay.getTime();
                  var days = millisBetween / millisecondsPerDay;

                  // Round down.
                  return days;

              }
 function filter_data(id)
    {
        
        var searchid = $('#search_text').val();
        var search_value = '';
		var url_call = '';
		var url_action = '';
		var data_pass = '';
        if(searchid.length >= 3)
        { 
            search_value = searchid; 
			url_call = 'search_trip';
			url_action = 'POST';
			data_pass = { value: id, '_token': $('#_token').val(),search_text:searchid};
        }
        else
        {
            search_value = '';
			url_call = 'filter_price';
			url_action = 'get';
			data_pass = { value: id, '_token': $('#_token').val()};
        }   
			//console.log(url_call);
        $.ajax({
        url: url_call,
        dataType : 'json',
        cache: false,
        data :  data_pass,
        type : url_action,

        success: function(data){
            var price = 1;
            var trHTML="";
            // A response to say if it's updated or not
		//alert(data.length);
			if(data.length > 0) {
            $.each(data, function (i, item) {
			
                if(id != "list_div")        
                {   
                if(price%2 == 0) { 
                    if(price%4 == 0 || price%5 == 0)  { 
                           trHTML += ' <div class="item-item2"><img src="' + item.Media_URL_trip3 + '" alt=""> ';
                            } else {
                          trHTML += '  <div class="item"><img src="' + item.Media_URL_trip2 + '" alt=""> ';
                          }} else {  
                        if(price%4 == 0 || price%5 == 0) {
                           trHTML += ' <div class="item-item2"><img src="' + item.Media_URL_trip3 + '" alt="">';
                            } else {
                           trHTML += ' <div class="item-item1"><img src="' + item.Media_URL_trip1 + '" alt="">';
                            }   }
                }
                else            
                {
                     trHTML += ' <div class="item-item2"><img src="' + item.Media_URL_trip3 + '" alt=""> ';
                }   
                trHTML += '<div class="details"><div class="text-shade">' + item.Trip_Start_Date_new + '<h4 class="white padding-vertical-5">' + item.Trip_Name + '</h4> by '+item.first_name +' '+item.last_name +'</div><div class="bottom-details">';
									if(item.Total_reviews != 0)
									{
                                    <?php for ($i=1; $i <= 5 ; $i++){ ?>
                                         trHTML += '<i class="icon fa-star margin-right-5" aria-hidden="true"></i> ';
                                    <?php  } ?>
										 trHTML += '<span class=" bookridetext margin-left-10">' + item.Total_reviews + 'Reviews</span>';
									}
									var days = showDays(item.Trip_Start_Date,item.Trip_End_Date);
									
                               trHTML += '</i><br><div class="col-xs-12  padding-left-5 padding-right-5"><div class="col-xs-4  padding-left-5 padding-right-5"><i class="icon fa-exchange margin-right-5 padding-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext">' + item.Total_Distance + ' KMS</span></i></div><div class="col-xs-4  padding-left-5 padding-right-5"><i class="icon fa-calendar margin-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext">' + days + ' Days</span></i></div><div class="col-xs-4  padding-left-5 padding-right-5"><i class="icon fa-users margin-right-2" aria-hidden="true"><span class="margin-left-10 bookridetext">'+ item.No_of_Tickets_Cost_1px +' Seats</span></i></div></div></div><a class="details-block btn btn-warning btn-xs btn-detail open-modal settings" href="trip/'+item.Trip_url +'"></a></div></div>';
                    price++;
                    if(price > 5)
                    {
                        price = 1;
                    }
                    
                });
					
					  trHTML += '<div class="col-xs-12 padding-bottom-10 "><!--button class="btn bg-grey-600 white btnUpdate p-v-lg" style="width:100%">Load More</button--></div>';
					}
                    
					  else {
						  
						trHTML += '<div class="col-xs-12 no-padding no-trip ">No Trip found please search or flter again....!!</div>';
					  }
     $('#result').html(trHTML);

        }

    }); 
    }
</script>
 
</head>

<body class="bikation-body">


<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner carousel-bikation">
        <div class="item active">
            <img src="/assets/images/bikations1.jpg" alt="Los Angeles">
        </div>

        <div class="item">
            <img src="/assets/images/bikations2.jpg" alt="Los Angeles">
        </div>

        <div class="item">
            <img src="/assets/images/bikations3.jpg" alt="Los Angeles">
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>



</div>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none;">Open Modal</button>


@include('bikationvendor/packages/navigation')
    <div class="container">
	
        <section class="header hidden-xs" style=" display:none;background-image: url({{$trip_banner->image}});background-repeat: no-repeat;background-size:cover;">
        
            
            <div class="row profileDetail text-center" style="display: none">
			<div class="text-shade_b">
                <h1 class="userName">BIKATION</h1>
                <div class="userContact info">
                    <ul style="margin-top: 0px;">
                        <li class="title">
						{!!$trip_banner->banner_title!!}
						</li>
                    </ul>
                </div>
				</div>
            </div>
            <!--img src="../assets/images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
        </section>
        
        <section class="header visible-xs" style="display:none!important;background-image: url({{$trip_banner->image}});background-repeat: no-repeat;background-size:cover; height:190px;">
            <div class="row profileDetail text-center">
                <div class="text-shade_b">
                    <h1 class="userName">BIKATION</h1>
                    <div class="userContact info">
                        <ul style="margin-top: 0px;">
                            <li class="title">
                            {!!$trip_banner->banner_title!!}
                            </li>
                        </ul>
                    </div>
				</div>
            </div>
            <!--img src="../assets/images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
        </section>
        
        
        @if(count($errors) > 0)
            <div style="margin:20px"></div>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-xs-12 no-padding">
            <section class="searchbox clearfix">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding">
                    <div class="">
                        <input placeholder="SEARCH" class="search_text form-control" id="search_text">
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 no-padding" style="display:none">
                    <div class="">
                        <div class="btn-group">
                            <button type="button" class="btn btn-outline btn-default dropdown-toggle btn-sm" id="exampleSizingDropdown3" data-toggle="dropdown" aria-expanded="false"><img src="/assets/images/Funnel.png"> Filter By
                                <!-- <i class="caret icon" aria-hidden="true"></i> -->
								 <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="exampleSizingDropdown3" role="menu">
                                <!--li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon fa-user" aria-hidden="true"></i>Conductor</a></li-->
                                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="filer-price" onClick="filter_data(this.id);"><i class="icon fa-tag" aria-hidden="true" ></i> Price</a></li>
                                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="filer-popularity" onClick="filter_data(this.id);"><i class="icon fa-star" aria-hidden="true" ></i>Popularity</a></li>
                                <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="filer-locations" onClick="filter_data(this.id);"><i class="icon fa-map-marker" aria-hidden="true"></i>Locations</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
               <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 no-padding" style="display:none">
                    <div class="viewby-main">
                        <h5 class="viewby pull-left">VIEW BY</h5>
                       <i class="fa fa-th-large fa-3x list active" id="Grid_div" aria-hidden="true" data-toggle="true"></i>
                       <i class="fa fa-bars fa-3x list" id="list_div" aria-hidden="true" data-toggle="true"></i>
                    </div>
                </div>
            </section>
        </div>
        <section class="bookRide" style="padding:0">
            <div id="container">
                <div id="result">
				<?php $a=1; ?>
					 @foreach($packagesMakeList as $key => $packages)
						@if($a%2 == 0)	
							@if($a%4 == 0 || $a%5 == 0)	 	
							<div class="item-item2">
                            <img src="{{ $packages->Media_URL_trip3 }}" alt="" class="img-responsive">

							@else
							<div class="item">
                            <img src="{{ $packages->Media_URL_trip2 }}" alt="img-responsive">
							@endif
						@else 	
							@if($a%4 == 0 || $a%5 == 0)	 	
							<div class="item-item2">
                             <img src="{{ $packages->Media_URL_trip3 }}" alt="img-responsive">
							@else
							<div class="item-item1">
                            <img src="{{ $packages->Media_URL_trip1 }}" alt="">
							@endif
						@endif	
						
                        <div class="details">
							
							<div class="text-shade">
							{{ date('d M g:ia', strtotime($packages->Trip_Start_Date)) }}
                            <h4 class="white padding-vertical-5"></h4></div>
                            <div class="bottom-details text-shade margin-right-0 margin-left-0">
								@if($packages->Total_reviews  != 0)	
                                <!--div class="pull-left"-->
                                    @for ($i=1; $i <= 5 ; $i++)
                                            <!--img src="/assets/images/{{ ($i <= $packages->Overall) ? 'orangeStar' : 'whiteStar'}}.png" /-->
										<i class="icon fa-star margin-right-5" aria-hidden="true"></i>
                                    @endfor
							    <!--/div-->
							
								<span class=" bookridetext margin-left-10">{{ $packages->Total_reviews }}  Reviews</span></i>
								@endif	
                                <br>
                                <div class="col-xs-12 padding-left-5 padding-right-5">
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-exchange margin-right-5 padding-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $packages->Total_Distance }} KMS</span></i>
                                    </div>
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-calendar margin-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext"><?php $datetime1 = date_create($packages->Trip_Start_Date);
                                           $datetime2 = date_create($packages->Trip_End_Date);
                                           $interval = date_diff($datetime1, $datetime2);
                                           $days=$interval->format('%a');
                                           $night=$interval->format('%a');
                                           $hour=$interval->format('%h');
                                           if($hour>5) {
                                            $days=$days+1;
                                           }
                                           echo $days.' Days - <br>&nbsp;&nbsp;'.$night.' Night '; ?> </span></i>
                                    </div>
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-users margin-right-2" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $packages->No_of_Tickets_Cost_1px }} Seats</span></i>
                                    </div>
                                </div>
                            </div>
                            <a class="details-block btn btn-warning btn-xs btn-detail open-modal settings" href="trip/{{ $packages->Trip_url }}"></a>
                        </div>
                    </div>
					<?php $a++;
						if($a > 5)
							{$a = 1;}			
						
						?>
					@endforeach
                    <div class="col-xs-12 padding-bottom-10 ">
                        <!--button class="btn bg-grey-600 white btnUpdate p-v-lg" style="width:100%">Load More</button-->
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix clear-fix"></div>
        <hr>
        <h1 class="text-center" style="color: #ffffff">Past Trips</h1>
        <?php $a=1; ?>
        @foreach($pastpackages as $key => $pastpackage)
        @if($a%2 == 0)  
        @if($a%4 == 0 || $a%5 == 0)     
        <div class="item-item2">
            <img src="{{ $pastpackage->Media_URL_trip3 }}" alt="" class="img-responsive">

            @else
            <div class="item">
                <img src="{{ $pastpackage->Media_URL_trip2 }}" alt="img-responsive">
                @endif
                @else   
                @if($a%4 == 0 || $a%5 == 0)     
                <div class="item-item2">
                   <img src="{{ $pastpackage->Media_URL_trip3 }}" alt="img-responsive">
                   @else
                   <div class="item-item1">
                    <img src="{{ $pastpackage->Media_URL_trip1 }}" alt="">
                    @endif
                    @endif  

            <div class="details">

                <div class="text-shade text-center">
                            {{ date('d M g:ia', strtotime($pastpackage->Trip_Start_Date)) }}
                    <h4 class="white padding-vertical-5"></h4></div>
                <div class="bottom-details text-shade margin-right-0 margin-left-0">

                    <br>
                    <div class="col-xs-12 padding-left-5 padding-right-5">
                        <div class="col-xs-4 padding-left-5 padding-right-5">
                            <i class="icon fa-exchange margin-right-5 padding-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $pastpackage->Total_Distance }} </span></i>
                        </div>
                        <div class="col-xs-4 padding-left-5 padding-right-5">
                            <i class="icon fa-calendar margin-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext"><?php $datetime1 = date_create($pastpackage->Trip_Start_Date);
                                           $datetime2 = date_create($pastpackage->Trip_End_Date);
                                           $interval = date_diff($datetime1, $datetime2);
                                           $days=$interval->format('%a');
                                           $night=$interval->format('%a');
                                           $hour=$interval->format('%h');
                                           if($hour>5) {
                                            $days=$days+1;
                                           }
                                           echo $days.' Days - <br>&nbsp;&nbsp;'.$night.' Night '; ?> </span></i>
                        </div>
                        <div class="col-xs-4 padding-left-5 padding-right-5">
                            <i class="icon fa-users margin-right-2" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $pastpackage->No_of_Tickets_Cost_1px }} Seats</span></i>
                        </div>
                    </div>
                </div>
                <a class="details-block btn btn-warning btn-xs btn-detail open-modal settings" href="trip/{{ $pastpackage->Trip_url }}"></a>
            </div>
        </div>
        @endforeach


		@include('bikationvendor/packages/footer')
    </div>
    <!--script src="/assets/vendor/jquery/jquery.js"></script-->
    <script src="/assets/vendor/bootstrap/bootstrap.js"></script>
    <script src="/assets/vendor/animsition/jquery.animsition.js"></script>
    <script src="/assets/vendor/slidepanel/jquery-slidePanel.js"></script>
	<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
	 <script type="text/javascript" src="/js/additional-methods.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
    <!-- Scripts -->
    <script src="/assets/js/core.js"></script>
    <script src="/assets/js/site.js"></script>
    <script src="/assets/js/sections/menu.js"></script>
    <script src="/assets/js/sections/menubar.js"></script>
    <script src="/assets/js/sections/sidebar.js"></script>
    <script src="/assets/js/configs/config-colors.js"></script>
    <script src="/assets/js/components/animsition.js"></script>
    <script src="/assets/js/components/slidepanel.js"></script>
    <script src="/assets/js/masonry.pkgd.min.js"></script>
    <!--script src="/assets/vendor/modernizr/modernizr.js"></script-->
  <script>
 /*   $('#Grid_div').click(function() {
        $('.item').css({
            'width': '390px'
        });
    });
    $('#Grid_div').click(function() {
        $('.item-item1').css({
            'width': '435px'
        });
    });
    $('#list_div').click(function() {
        $('.item, .item-item1').css({
            'width': '50%'
        });
    });*/
    
     $('.viewby-main i').click(function() {
         filter_data(this.id);
        //alert(this.id);
    });
    </script>
      <script>
            $('.list').click(function(e) {
                $('.list').css("color", "#969696");
                $(this).css("color", "#eb952e");
            });
            </script>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-form">

        <!-- Modal content-->
        <div class="modal-content modal-content-form">
            <div class="modal-header modal-header-form">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <forms>
                    <div class="form-group">
                        <label for="text">Email:</label>
                        <input type="text" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Contact number:</label>
                        <input type="number" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                    </div>

                    <div class="form-group">
                        <label for="pwd">Email Id:</label>
                        <input type="email" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                    </div>
                    <div class="form-group">
                        <label for="pwd">qu</label>
                      <select class="form-control">
                          <option>asdad</option>
                      </select>
                    </div>


                    <div class="form-group">
                        <label for="pwd">Your text:</label>
                        <textarea class="form-control">

                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </forms>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

</body>

</html>

