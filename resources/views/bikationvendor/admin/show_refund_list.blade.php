@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Refund</h2>
			
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Booking ID</th>
					<th>TripName</th>
					<th>Razorpay Refund Id</th>
					<th>Refund Amount</th>
					<th>Razorpay Id</th>
					<th>Refund Status</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_RefundList as $bikation_Refund)
						<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_Refund->Booking_ID }}</td>
						<td>{{ $bikation_Refund->TripName }}</td>
						<td>{{ $bikation_Refund->reference_ID }}</td>
						<td>{{ $bikation_Refund->amount/100 }}</td> 
						<td>{{ $bikation_Refund->transaction_ID_from_PG }}</td>
						<td>@if( $bikation_Refund->status == 1 )
								<button class="btn btn-success"><i class="fa fa-check"></i> Success</button>
							@else
								<button class="btn btn-danger"><i class="fa fa-close"></i> Failed</button>
							@endif
						</td>
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	

@stop		
