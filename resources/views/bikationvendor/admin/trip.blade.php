@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">TRIP Homepage</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Trip</button> -->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<!-- <th>Vendor</th> -->
					<th>Trip Type</th>
					<th>Trip Name</th>
					<!-- <th>Created On</th>
					<th>Created by</th>
					<th>Approve Requested on</th>
					<th>Approved By</th>
					<th>Approved  on</th>
					<th>Cancelled Request on</th>
					<th>Cancel approved by</th>
					<th>Cancelled on</th> -->
					<!--th>Total No of Tickets</th>
					<th>No Of Tickets Available</th-->
					<!-- <th>Trip Start Date</th>
					<th>Trip End Date</th> -->
					<th>Start Location</th>
					<th>End Location</th>
					<th>Total Distance</th>
					<th>Total riding hours</th>
					<th>No of stops</th>
				<!-- 	<th>ticket denomination</th> -->
					<!-- <th>Experience Level required</th> -->
					<th>Preffered Bikes</th>
					<!-- <th>General Instructions</th> -->
					<!-- <th>Trip Description</th> -->
					
					<th>Status</th>
					<th>Approve By Admin</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				 <?php  $i=1; 
				// echo '<pre>';
				// print_r($bikation_tripList);exit;
				 ?>
				@foreach($bikation_tripList as $bikation_trip) 
				
					<tr>
					
						<td><?php echo $i; ?></td>
						
						<td>{{ $bikation_trip->Trip_type }}</td>
						<td>{{ $bikation_trip->Trip_Name }}</td>
						<td>{{ $bikation_trip->Start_Location }}</td>
						<td>{{ $bikation_trip->End_Location }}</td>
						<td>{{ $bikation_trip->Total_Distance }}</td>
						<td>{{ $bikation_trip->Total_riding_hours }}</td>
						<td>{{ $bikation_trip->No_of_stops }}</td>
						<td>{{ $bikation_trip->Preffered_Bikes }}</td>
						
						<td>
							@if( $bikation_trip->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>@if( $bikation_trip->Approve_By_Admin == 2 )
								Vendor Submitted trip for approval
							@elseif( $bikation_trip->Approve_By_Admin == 1 )
							<button class="btn btn-success">Approved</button>
							@else
							<button class="btn btn-danger">Cancel</button>
							@endif
						</td>
						<td>

							<a href="{{ url('admin/bikation-vendor-trip/change-status/'.$bikation_trip->Trip_ID) }}"><button class="btn btn-primary">Change Status</button></a>
							<br><br>
						 <button class="btn btn-info bikeModel_edit_btn"  data-id="{{ $bikation_trip->Trip_ID }}" data-vendorid="" data-triptype="{{ $bikation_trip->Trip_type }}" data-tripname="{{ $bikation_trip->Trip_Name }}" data-status="{{ $bikation_trip->Status }}"   data-totalnooftickets="{{ $bikation_trip->Total_No_of_Tickets }}" data-noofticketsavailable="{{ $bikation_trip->No_of_tickets_available }}" data-tripstartdate="{{ $bikation_trip->Trip_Start_Date }}" data-tripenddate="{{ $bikation_trip->Trip_End_Date }}" data-startlocation="{{ $bikation_trip->Start_Location }}" data-endlocation="{{ $bikation_trip->End_Location }}" data-totaldistance="{{ $bikation_trip->Total_Distance }}" data-totalridinghours="{{ $bikation_trip->Total_riding_hours }}" data-noofstops="{{ $bikation_trip->No_of_stops }}" data-ticketdenomination="{{ $bikation_trip->ticket_denomination }}" data-experiencelevelrequired="{{ $bikation_trip->Experience_Level_required }}" data-prefferedbikes="{{ $bikation_trip->Preffered_Bikes }}" data-generalinstructions="{{ $bikation_trip->General_Instructions }}" data-tripdescription="{{ $bikation_trip->Trip_Description }}" data-packagename="{{ $bikation_trip->name }}" data-packagepescription="{{ $bikation_trip->Description }}" data-ticket_1x_details="{{ $bikation_trip->Tickes_Remaining_Cost_1px }}/{{ $bikation_trip->No_of_Tickets_Cost_1px }}" 
						 data-ticket_1x_price="{{ $bikation_trip->Cost_1px }}" data-ticket_2x_details="{{ $bikation_trip->Tickes_Remaining_Cost_2px }}/{{ $bikation_trip->No_of_Tickets_Cost_2px }}" data-ticket_2x_price="{{ $bikation_trip->Cost_2px }}" data-tripphoto="{{ $bikation_trip->Media_URL_trip1 }}"  data-first_name="{{ $bikation_trip->first_name }}" data-last_name="{{ $bikation_trip->last_name }}" data-email="{{ $bikation_trip->email }}" data-mobile_num="{{ $bikation_trip->mobile_num }}"
						 ><i class="fa fa-pencil-square-o"></i> View Trip</button>
						<!--button class="btn btn-danger wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_trip->Trip_ID }}"><i class="fa fa-close"></i> Delete</button-->

						</td>
					</tr>
					 <?php  $i++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		$(function(){
	      Trip_admin.BikationTrip.init();
	    });
	</script>
@stop		
@section('model')
	
	

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" class="form-horizontal" enctype="multipart/form-data" id="bikation-trip-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h2 class="modal-title" id="myModalLabel">View Trip</h2>
		      		</div>
		      		<div class="modal-body">
					<div class="well">
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Trip Details: </h4></p>
		        		<div class="form-group"> 
							<label class="col-md-2"><strong>Trip Type :</strong></label>
							<div class="col-md-4">
								<div id="Trip_type">
								</div>
		        			</div> 
							<label class="col-md-2"><strong>Trip Name :</strong></label>
							<div class="col-md-4">
		        				<div id="Trip_Name">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>Trip Start Date :</strong></label>
							<div class="col-md-4">
								<div id="TripStartDate">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Trip End Date :</strong></label>
							<div class="col-md-4">
								<div id="TripEndDate">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Start Location :</strong></label>
							<div class="col-md-4">
								<div id="Start_Location">
								</div>
		        			</div>
							<label class="col-md-2"><strong>End Location :</strong></label>
							<div class="col-md-4">
								<div id="End_Location">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Total Distance :</strong></label>
							<div class="col-md-4">
								<div id="Total_Distance">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Total Riding Hours :</strong></label>
							<div class="col-md-4">
								<div id="Total_riding_hours">
								</div>
		        			</div>
		        		</div>
						
		        		<div class="form-group">
							<label class="col-md-2"><strong>No Of Stops :</strong></label>
							<div class="col-md-4">
								<div id="No_of_stops">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Ticket Denomi. :</strong></label>
							<div class="col-md-4">
								<div id="ticket_denomination">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>Exp. Level :</strong></label>
							<div class="col-md-4">
								<div id="Experience_Level_required">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Preffered Bikes :</strong></label>
							<div class="col-md-4">
								<div id="Preffered_Bikes">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>General Inst. :</strong></label>
							<div class="col-md-4">
								<div id="General_Instructions">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Trip Desc. :</strong></label>
							<div class="col-md-4">
								<div id="Trip_Description">
								</div>
		        			
		        			</div>
		        		</div>
		        		
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Package Details: </h4></p>
						<div class="form-group">
							<label class="col-md-2"><strong>Package Name :</strong></label>
							<div class="col-md-4">
								<div id="Package_Name">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Package Desc. :</strong></label>
							<div class="col-md-4">
								<div id="Package_Description">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Remaining Tickes /<br>No of Tickets (1PX) :</strong></label>
							<div class="col-md-3">
								<div id="ticket_1x_details">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Cost of 1px ticket:</strong></label>
							<div class="col-md-3">
								<div id="ticket_1x_price">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Remaining Tickes /<br>No of Tickets (2PX) :</strong></label>
							<div class="col-md-3">
								<div id="ticket_2x_details">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Cost of 2px ticket :</strong></label>
							<div class="col-md-3">
								<div id="ticket_2x_price">
								</div>
		        			</div>
		        		</div>
						
						
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Trip Photo: </h4></p> 
						<div class="form-group">
							<label class="col-md-2"><strong>Photo :</strong></label>
							<div class="col-md-4">
								<div id="Media_URL_trip1">
								
								</div>
		        			</div>
							
		        		</div>	
						
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Vendor Details: </h4></p> 
						<div class="form-group">
							<label class="col-md-3"><strong>Name :</strong></label>
							<div class="col-md-9">
								<div id="first_name">
								</div>
		        			</div>
							
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Email :</strong></label>
							<div class="col-md-9">
								<div id="email">
								</div>
		        			</div>
							
		        		</div>
						<div class="form-group">
							
							<label class="col-md-3"><strong>Mobile :</strong></label>
							<div class="col-md-9">
								<div id="mobile_num">
								</div>
		        			</div>
		        		</div>
						
		        		</div>
		        		
		      		</div>
		      		<div class="modal-footer">
		      			<!--button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button-->
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
