@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Packages Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Valid_From</th>
					<th>Valid_To</th>
					<th>Created_By</th>
					<th>Updated_By</th>
					<th>Created_On</th>
					<th>Door_No</th>
					<th>Street_1</th>
					<th>Street_2</th>
					<th>Area</th>
					<th>City</th>
					<th>State</th>
					<th>Country</th>
					<th>ZipCode</th>
					<th>Name</th>
					<th>LandLine_Number</th>
					<th>Phone_Number</th>
					<th>Alternate_Number</th>
					<th>Email</th>
					<th>Website</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikation_addressList as $bikation_address)
					<tr>
						<td>{{ $bikation_address->Address_ID }}</td>
						<td>{{ $bikation_address->Valid_From }}</td>
						<td>{{ $bikation_address->Valid_To }}</td>
						<td>{{ $bikation_address->Created_By }}</td>
						<td>{{ $bikation_address->Updated_By }}</td>
						<td>{{ $bikation_address->Created_On }}</td>
						<td>{{ $bikation_address->Updated_On }}</td>
						<td>{{ $bikation_address->Door_No }}</td>
						<td>{{ $bikation_address->Street_1 }}</td>
						<td>{{ $bikation_address->Street_2 }}</td>
						<td>{{ $bikation_address->Area }}</td>
						<td>{{ $bikation_address->City }}</td>
						<td>{{ $bikation_address->State }}</td>
						<td>{{ $bikation_address->Country }}</td>
						<td>{{ $bikation_address->ZipCode }}</td>
						<td>{{ $bikation_address->Name }}</td>
						<td>{{ $bikation_address->LandLine_Number }}</td>
						<td>{{ $bikation_address->Phone_Number }}</td>
						<td>{{ $bikation_address->Alternate_Number }}</td>
						<td>{{ $bikation_address->Email }}</td>
						<td>{{ $bikation_address->Website }}</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_address->Address_ID }}" data-ValidFrom="{{ $bikation_address->Valid_From }}" data-ValidTo="{{ $bikation_address->Valid_To }}" data-CreatedBy="{{ $bikation_address->Created_By }}" data-UpdatedBy="{{ $bikation_address->Updated_By }}" data-CreatedOn="{{ $bikation_address->Created_On }}" data-UpdatedOn="{{ $bikation_address->Updated_On }}" data-DoorNo="{{ $bikation_address->Door_No }}" data-Street1="{{ $bikation_address->Street_1 }}" data-Street2="{{ $bikation_address->Street_2 }}" data-Area="{{ $bikation_address->Area }}" data-City="{{ $bikation_address->City }}" data-State="{{ $bikation_address->State }}" data-Country="{{ $bikation_address->Country }}" data-ZipCode="{{ $bikation_address->ZipCode }}" data-Name="{{ $bikation_address->Name }}"data-LandLineNumber="{{ $bikation_address->LandLine_Number }}" data-PhoneNumber="{{ $bikation_address->Phone_Number }}" data-AlternateNumber="{{ $bikation_address->Alternate_Number }}" data-Email="{{ $bikation_address->Email }}" data-Website="{{ $bikation_address->Website }}"><i class="fa fa-pencil-square-o"></i> Edit</button><br/>
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_address->Address_ID }}"><i class="fa fa-pencil-square-o"></i> Delete</button>

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Admin.Bikation_Address.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/packages/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_From" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Created_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_By" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Updated_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Created On:</label>
							<div class="col-md-3">
		        				<input type="text" name="Created_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Updated_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Door_No</label>
							<div class="col-md-9">
		        				<input type="text" name="Door_No" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Street_1</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_1" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Street_2</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_2" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area :</label>
							<div class="col-md-9">
		        				<input type="text" name="Area" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">City</label>
							<div class="col-md-9">
		        				<input type="text" name="City" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">State</label>
							<div class="col-md-9">
		        				<input type="text" name="State" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Country</label>
							<div class="col-md-9">
		        				<input type="text" name="Country" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">ZipCode:</label>
							<div class="col-md-3">
		        				<input type="text" name="ZipCode" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">LandLine_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="LandLine_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Phone_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Phone_Number" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Alternate_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Alternate_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email</label>
							<div class="col-md-9">
		        				<input type="text" name="Email" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Website</label>
							<div class="col-md-9">
		        				<input type="text" name="Website" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Address</h4>
		      		</div>
		      			<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_From" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Created_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Created_By" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Updated_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Created On:</label>
							<div class="col-md-3">
		        				<input type="text" name="Created_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Updated_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Updated_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Door_No</label>
							<div class="col-md-9">
		        				<input type="text" name="Door_No" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Street_1</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_1" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Street_2</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_2" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area :</label>
							<div class="col-md-9">
		        				<input type="text" name="Area" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">City</label>
							<div class="col-md-9">
		        				<input type="text" name="City" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">State</label>
							<div class="col-md-9">
		        				<input type="text" name="State" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Country</label>
							<div class="col-md-9">
		        				<input type="text" name="Country" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">ZipCode:</label>
							<div class="col-md-3">
		        				<input type="text" name="ZipCode" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Name" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">LandLine_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="LandLine_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Phone_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Phone_Number" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Alternate_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Alternate_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email</label>
							<div class="col-md-9">
		        				<input type="text" name="Email" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Website</label>
							<div class="col-md-9">
		        				<input type="text" name="Website" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	