@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">People Trip </h2>
		
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add People Trip</button> 
		</div>
		
		<form method="post" action="/admin/bikation-people_on_trip" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Trip Name</th>
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<!-- <th>Sex</th> -->
					<th>DoB</th>
					<th>Rider</th>
					<th>DL</th>
					<th>Booked Type</th>
					<!-- <th>Cancelled On</th>
					<th>Cancelled By</th> -->
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_People_On_TripList as $bikation_People_On_Trip)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_People_On_Trip->TripName }}</td> 
						<td>{{ $bikation_People_On_Trip->Name_of_Co_Px }}</td>
						<td>{{ $bikation_People_On_Trip->Contact_No }}</td>
						<td>{{ $bikation_People_On_Trip->Email_ID }}</td>
						<!-- <td>{{ $bikation_People_On_Trip->Sex }}</td> -->
						<td>{{ $bikation_People_On_Trip->DoB }}</td>
						<td>{{ $bikation_People_On_Trip->Rider }}</td>
						<td>{{ $bikation_People_On_Trip->DL }}</td>
						<td>{{ $bikation_People_On_Trip->Booked_Type }}</td>
						<!-- <td>{{ $bikation_People_On_Trip->Cancelled_On }}</td>
						<td>{{ $bikation_People_On_Trip->Cancelled_By }}</td> -->
						<td>
							@if( $bikation_People_On_Trip->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_People_On_Trip->id }}" data-bookid="{{ $bikation_People_On_Trip->Book_ID }}" data-tripid="{{ $bikation_People_On_Trip->Trip_ID }}"
							data-nameofcopx="{{ $bikation_People_On_Trip->Name_of_Co_Px }}" data-contactno="{{ $bikation_People_On_Trip->Contact_No }}" data-emailid="{{ $bikation_People_On_Trip->Email_ID }}" data-sex="{{ $bikation_People_On_Trip->Sex }}" data-dob="{{ $bikation_People_On_Trip->DoB }}" data-rider="{{ $bikation_People_On_Trip->Rider }}" data-dl="{{ $bikation_People_On_Trip->DL }}" data-cancelledon="{{ $bikation_People_On_Trip->Cancelled_On }}" data-cancelledby="{{ $bikation_People_On_Trip->Cancelled_By }}" data-status="{{ $bikation_People_On_Trip->Status }}"><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-danger wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_People_On_Trip->id }}"><i class="fa fa-close"></i> Delete</button>
							
							

						</td>
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/bikation-people_on_trip/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-people_on_trip-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD People Trip</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group" hidden>
							<label class="col-md-3">Book ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Book_ID" id="Book_ID" class="form-control"/>
		        				<input type="hidden" name="Booked_Type" value="Admin" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Name</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Name of Co Px</label>
							<div class="col-md-9">
		        				<input type="text" name="Name_of_Co_Px" id="Name_of_Co_Px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Contact No</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_No" id="Contact_No" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Email_ID" id="Email_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Sex :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Sex" id="Sex1" value="1"> Male
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Sex" id="Sex2" value="0"> Female
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		<div class="form-group">
							<label class="col-md-3">DoB</label>
							<div class="col-md-9">
		        				<input type="text" name="DoB" id="DoB" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Rider</label>
							<div class="col-md-9">
		        				<input type="text" name="Rider" id="Rider" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">DL</label>
							<div class="col-md-9">
		        				<input type="text" name="DL" id="DL" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		<input type="hidden" name="Booked_Type" value="Admin" class="form-control"/>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data"
				id="bikation-people_on_trip-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT People On trip</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group" hidden>
							<label class="col-md-3">Book ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Book_ID" id="Book_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Trip Name</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Name of Co Px</label>
							<div class="col-md-9">
		        				<input type="text" name="Name_of_Co_Px" id="Name_of_Co_Px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Contact No</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_No" id="Contact_No" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Email ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Email_ID" id="Email_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Sex :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Sex" id="Sex1" value="1"> Male
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Sex" id="Sex2" value="0"> Female
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		<div class="form-group">
							<label class="col-md-3">DoB</label>
							<div class="col-md-9">
		        				<input type="text" name="DoB" id="DoB" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Rider</label>
							<div class="col-md-9">
		        				<input type="text" name="Rider" id="Rider" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">DL</label>
							<div class="col-md-9">
		        				<input type="text" name="DL" id="DL" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden> 
							<label class="col-md-3">Cancelled By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	
