@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Payment Refund</h2>
		<!-- <div class="row">
		<div class="col-xs-7 text-left"><!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Booking</button> 
		</div>
		<!-- 
		<form method="post" action="/admin/bikation-bookings" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form> 
		</div>-->
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Book ID</th>
					<th>Trip Name</th>
				 	<th>User</th>
					<th>Total Cost</th>
					<th>Razorpay Id</th>
					<th>Razorpay status</th>
					<th>No of People</th>
				<!-- 	<th>Package Cost</th>
					<th>Addon cost</th>
					<th>Total Cost</th> -->
				<!-- 	<th>Cancelled On</th>
					<th>Cancelled By</th> -->
				<!-- 	<th>Vehicle</th> -->
				<!-- 	<th>Additional Remarks</th> -->
					<th>Booked Type</th>
					<!--th>Payment status</th-->
					<!-- <th>Status</th> -->
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_bookingsList as $bikation_bookings)
						<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_bookings->Booking_ID }}</td>
						<td>{{ $bikation_bookings['TripName'] }}</td>
					 	<td>{{ $bikation_bookings['first_name'] }} {{ $bikation_bookings['last_name'] }} <br>{{ $bikation_bookings['email'] }}<br>{{ $bikation_bookings['mobile_num'] }}</td>
						<td>{{ $bikation_bookings->Total_Cost }}</td>
					<td>{{ $bikation_bookings->Razorpay_id }}</td>
						<td>{{ $bikation_bookings->Razorpay_status }}</td> 
						<td>{{ $bikation_bookings->No_of_People }}</td>
					<!-- 	<td>{{ $bikation_bookings->Package_Cost }}</td>
						<td>{{ $bikation_bookings->Addon_cost }}</td>
						<td>{{ $bikation_bookings->Total_Cost }}</td> -->
					<!-- 	<td>{{ $bikation_bookings->Cancelled_On }}</td>
						<td>{{ $bikation_bookings->Cancelled_By }}</td> -->
						<!-- <td>{{ $bikation_bookings->Vehicle }}</td> -->
						<!-- <td>{{ $bikation_bookings->Additional_Remarks }}</td> -->
						<td>{{ $bikation_bookings->Booked_Type }}</td>
						<!--td>@if( $bikation_bookings->Payment_status == 1 )
								<button class="btn btn-success">Success</button>
							@else
								<button class="btn btn-danger">Failed</button>
							@endif
						</td-->
						<!-- <td>
							@if( $bikation_bookings->Status == 1 )
								Active
							@else
								Failed
							@endif
						</td> -->
						<td>@if( $bikation_bookings->Payment_status == 1 )
								@if($bikation_bookings->Razorpay_refund_status == 0)
									<button class="btn btn-info bikeModel_refund_btn"  data-id="{{ $bikation_bookings->Booking_ID }}"><i class="fa fa-pencil-square-o"></i> Refund </button>
								@else
									<button class="btn btn-success"><i class="fa fa-check"></i> Refund Success</button>
								@endif
							@else
								<button class="btn btn-danger"><i class="fa fa-close"></i> Failed</button>
							@endif
								
						</td> 
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$(function(){
	      Refund_Admin.BikationRefund_Admin.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure You Want To Refund Amount</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	
