@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Packages Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Booking_ID</th>
					<th>Trip_ID</th>
					<th>Customer_ID</th>
					<th>Package_ID</th>
					<th>Booked_On</th>
					<th>Booked_By</th>
					<th>No_of_People</th>
					<th>Status</th>
					<th>Package_Cost</th>
					<th>Addon_cost</th>
					<th>Total_Cost</th>
					<th>Cancelled_On</th>
					<th>Cancelled_By</th>
					<th>Vehicle</th>
					<th>Additional_Remarks</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikation_bookingsList as $bikation_bookings)
					<tr>
						<td>{{ $bikation_bookings->Booking_ID }}</td>
						<td>{{ $bikation_bookings->Trip_ID }}</td>
						<td>{{ $bikation_bookings->Customer_ID }}</td>
						<td>{{ $bikation_bookings->Package_ID }}</td>
						<td>{{ $bikation_bookings->Booked_On }}</td>
						<td>{{ $bikation_bookings->Booked_By }}</td>
						<td>{{ $bikation_bookings->No_of_People }}</td>
						<td>{{ $bikation_bookings->Status }}</td>
						<td>{{ $bikation_bookings->Package_Cost }}</td>
						<td>{{ $bikation_bookings->Addon_cost }}</td>
						<td>{{ $bikation_bookings->Total_Cost }}</td>
						<td>{{ $bikation_bookings->Cancelled_On }}</td>
						<td>{{ $bikation_bookings->Cancelled_By }}</td>
						<td>{{ $bikation_bookings->Vehicle }}</td>
						<td>{{ $bikation_bookings->Additional_Remarks }}</td>
						<td>
							<!-- <button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_bookings->Booking_ID }}" data-TripID="{{ $bikation_bookings->Trip_ID }}" data-CustomerID="{{ $bikation_bookings->Customer_ID }}" data-PackageID="{{ $bikation_bookings->Package_ID }}" data-BookedOn="{{ $bikation_bookings->Booked_On }}" data-BookedBy="{{ $bikation_bookings->Booked_By }}" data-NoofPeople="{{ $bikation_bookings->No_of_People }}" data-Status="{{ $bikation_bookings->Status }}" data-PackageCost="{{ $bikation_bookings->Package_Cost }}" data-Addoncost="{{ $bikation_bookings->Addon_cost }}" data-TotalCost="{{ $bikation_bookings->Total_Cost }}" data-CancelledOn="{{ $bikation_bookings->Cancelled_On }}" data-CancelledBy="{{ $bikation_bookings->Cancelled_By }}" data-Vehicle="{{ $bikation_bookings->Vehicle }}" data-AdditionalRemarks="{{ $bikation_bookings->Additional_Remarks }}"><i class="fa fa-pencil-square-o"></i> Edit</button> -->
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_bookings->Address_ID }}"><i class="fa fa-pencil-square-o"></i> Delete</button>

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Admin.bikation_bookings.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/packages/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" id="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Package_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Booked_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Booked_By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No_of_People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-9">
		        				<input type="text" name="Status" id="Status" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon_cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total_Cost :</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Additional_Remarks:</label>
							<div class="col-md-3">
		        				<input type="text" name="Additional_Remarks" id="Additional_Remarks" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Address</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" id="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Package_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Booked_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Booked_By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No_of_People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-9">
		        				<input type="text" name="Status" id="Status" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon_cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total_Cost :</label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cancelled_By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Additional_Remarks:</label>
							<div class="col-md-3">
		        				<input type="text" name="Additional_Remarks" id="Additional_Remarks" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	