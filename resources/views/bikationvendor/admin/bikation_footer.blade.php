@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Bikation Footer</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Trip</button> -->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		
		
	</div>
	
	<form method="post" class="form-horizontal" action="/admin/bikation-add-footer" enctype="multipart/form-data" id="bikation-banner-add">
		      		
		      		<div class="modal-body">
					<div class="well">
					<div class="col-md-8"> 
						
						<div class="form-group"> 
							<label class="col-md-3"><strong>Footer Text :</strong></label>
							<div class="col-md-9">
								<textarea type="text" name="footer_text" id="footer_text" rows="10" style="width:100%" />{{$bikation_bannerList->desc}}</textarea>
							</div> 
						</div>
					</div>	
					<div class="col-md-4"> 
						
					</div>	
						<div class="form-group"> 
							<label class="col-md-2"></label>
							<div class="col-md-6">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
							</div> 
						</div>
						
		        	
						
		        		</div>
		        		
		      		</div>
		      		
		    	</form>
	
@stop		
