@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">User Homepage</h2>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>mobile_num</th>
					<th>status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($users as $user)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->mobile_num }}</td>
						<td>
							@if( $user->status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td>
							@if( $user->status == 1 )
							<a href="{{ url('admin/bikation-vendor/'.$user->id) }}"><button class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> Change Status</button></a>
							@else
								<a href="{{ url('admin/bikation-vendor/'.$user->id) }}"><button class="btn btn-danger"><i class="fa fa-close" aria-hidden="true"></i> Change Status</button></a>
							@endif
						<br /><br />
							<button class="btn btn-info bikeModel_edit_btn"  data-id="{{ $user->id }}" data-firstname="{{ $user->first_name }}" data-lastname="{{ $user->last_name }}" data-email="{{ $user->email }}" data-mobilenum="{{ $user->mobile_num }}" data-bank="{{ $user->Bank }}" data-bankaccountnumber="{{ $user->Bank_Account_Number }}" data-ifsc="{{ $user->IFSC }}" data-pan="{{ $user->PAN }}" data-tan="{{ $user->TAN }}" data-srn="{{ $user->SRN }}" data-cin="{{ $user->CIN }}" data-tin="{{ $user->TIN }}" data-overallrating="{{ $user->Overall_Rating }}" data-vendorimage="{{ $user->Vendor_Image }}" data-contactperson="{{ $user->Contact_person }}" data-contactphonenumber="{{ $user->Contact_Phone_Number }}" data-doorno="{{ $user->Door_No }}" data-streetone="{{ $user->Street_1 }}" data-streettwo="{{ $user->Street_2 }}" data-area="{{ $user->Area }}" data-city="{{ $user->City }}" data-state="{{ $user->State }}"  data-country="{{ $user->Country }}" data-zipcode="{{ $user->ZipCode }}" data-landlinenumber="{{ $user->LandLine_Number }}" data-phonenumber="{{ $user->Phone_Number }}" data-alternatenumber="{{ $user->Alternate_Number }}" data-website="{{ $user->Website }}"  data-about_me="{{ $user->about_me }}" data-status="{{ $user->status }}"><i class="fa fa-user"></i> View Profile</button>
						</td>
					</tr>
					<?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		$(function(){
	      Adminvendor.Profile.init();
	    });
	</script>
@stop

@section('model')
	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View Profile</h4>
		      		</div>
					
					<div class="modal-body">
					<div class="well">
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Vendor Details: </h4></p>
		        		<div class="form-group"> 
							<label class="col-md-2"><strong>First Name :</strong></label>
							<div class="col-md-4">
								<div id="first_name">
								</div>
		        			</div> 
							<label class="col-md-2"><strong>Last Name :</strong></label>
							<div class="col-md-4">
		        				<div id="last_name">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>E-mail :</strong></label>
							<div class="col-md-4">
								<div id="email">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Mobile :</strong></label>
							<div class="col-md-4">
								<div id="mobile_num">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Bank :</strong></label>
							<div class="col-md-4">
								<div id="Bank">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Bank Acc Num. :</strong></label>
							<div class="col-md-4">
								<div id="Bank_Account_Number">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>IFSC :</strong></label>
							<div class="col-md-4">
								<div id="IFSC">
								</div>
		        			</div>
							<label class="col-md-2"><strong>PAN :</strong></label>
							<div class="col-md-4">
								<div id="PAN">
								</div>
		        			</div>
		        		</div>
						
		        		<div class="form-group">
							<label class="col-md-2"><strong>TAN :</strong></label>
							<div class="col-md-4">
								<div id="TAN">
								</div>
		        			</div>
							<label class="col-md-2"><strong>SRN :</strong></label>
							<div class="col-md-4">
								<div id="SRN">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>CIN :</strong></label>
							<div class="col-md-4">
								<div id="CIN">
								</div>
		        			</div>
							<label class="col-md-2"><strong>TIN :</strong></label>
							<div class="col-md-4">
								<div id="TIN">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>Overall Rating :</strong></label>
							<div class="col-md-4">
								<div id="Overall_Rating">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Vendor Image :</strong></label>
							<div class="col-md-4">
								<div id="Vendor_Image">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Contact Person :</strong></label>
							<div class="col-md-4">
								<div id="Contact_person">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Contact Phone Number :</strong></label>
							<div class="col-md-4">
								<div id="Contact_Phone_Number">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Door No :</strong></label>
							<div class="col-md-4">
								<div id="Door_No">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Street 1 :</strong></label>
							<div class="col-md-4">
								<div id="Street_1">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Street 2 :</strong></label>
							<div class="col-md-4">
								<div id="Street_2">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Area :</strong></label>
							<div class="col-md-4">
								<div id="Area">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>City :</strong></label>
							<div class="col-md-4">
								<div id="City">
								</div>
		        			</div>
							<label class="col-md-2"><strong>State :</strong></label>
							<div class="col-md-4">
								<div id="State">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Country :</strong></label>
							<div class="col-md-4">
								<div id="Country">
								</div>
		        			</div>
							<label class="col-md-2"><strong>ZipCode :</strong></label>
							<div class="col-md-4">
								<div id="ZipCode">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>LandLine Num :</strong></label>
							<div class="col-md-4">
								<div id="LandLine_Number">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Phone Num :</strong></label>
							<div class="col-md-4">
								<div id="Phone_Number">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Alternate Num :</strong></label>
							<div class="col-md-4">
								<div id="Alternate_Number">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Website :</strong></label>
							<div class="col-md-4">
								<div id="Website">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>About me :</strong></label>
							<div class="col-md-8">
								<div id="about_me">
								</div>
		        			</div>
							
		        		</div>
		        		
						<!---p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Package Details: </h4></p>
						<div class="form-group">
							<label class="col-md-2"><strong>Package Name :</strong></label>
							<div class="col-md-4">
								<div id="Package_Name">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Package Desc. :</strong></label>
							<div class="col-md-4">
								<div id="Package_Description">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Remaining Tickes /<br>No of Tickets (1PX) :</strong></label>
							<div class="col-md-3">
								<div id="ticket_1x_details">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Cost 1px :</strong></label>
							<div class="col-md-3">
								<div id="ticket_1x_price">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Remaining Tickes /<br>No of Tickets (2PX) :</strong></label>
							<div class="col-md-3">
								<div id="ticket_2x_details">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Cost 2px :</strong></label>
							<div class="col-md-3">
								<div id="ticket_2x_price">
								</div>
		        			</div>
		        		</div-->
							
						
		        		</div>
		        		
		      		</div>
		      		<!--div class="modal-body">
		        		<div class="form-group">
							<label class="col-md-3">First Name</label>
							<div class="col-md-9">
		        				<input type="text" name="first_name" class="form-control"/>
		        			</div> 
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Last Name</label>
							<div class="col-md-9">
		        				<input type="text" name="last_name" class="form-control"/>
		        			</div>
		        		</div>
		        		 <div class="form-group">
							<label class="col-md-3">Email</label>
							<div class="col-md-9">
		        				<input type="text" name="email" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Bank</label>
							<div class="col-md-9">
		        				<input type="text" name="Bank" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Bank Account Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Bank_Account_Number" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">IFSC</label>
							<div class="col-md-9">
		        				<input type="text" name="IFSC" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">PAN</label>
							<div class="col-md-9">
		        				<input type="text" name="PAN" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">TAN</label>
							<div class="col-md-9">
		        				<input type="text" name="TAN" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">SRN</label>
							<div class="col-md-9">
		        				<input type="text" name="SRN" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">CIN</label>
							<div class="col-md-9">
		        				<input type="text" name="CIN" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">TIN</label>
							<div class="col-md-9">
		        				<input type="text" name="TIN" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Overall_Rating</label>
							<div class="col-md-9">
		        				<input type="text" name="Overall_Rating" class="form-control "/>
		        			</div>
		        		</div>
		        		
		        		<div class="form-group">
							<label class="col-md-3">Vendor_Image</label>
							<div class="col-md-9">
		        				<div id="old_coverImage_holder"></div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Contact Person</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_person" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Contact Phone Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Contact_Phone_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Door_No</label>
							<div class="col-md-9">
		        				<input type="text" name="Door_No" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Street_1</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_1" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Street_2</label>
							<div class="col-md-9">
		        				<input type="text" name="Street_2" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Area</label>
							<div class="col-md-9">
		        				<input type="text" name="Area" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">City</label>
							<div class="col-md-9">
		        				<input type="text" name="City" class="form-control"/>
		        			</div>
		        		</div><div class="form-group">
							<label class="col-md-3">State</label>
							<div class="col-md-9">
		        				<input type="text" name="State" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Country</label>
							<div class="col-md-9">
		        				<input type="text" name="Country" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">ZipCode</label>
							<div class="col-md-9">
		        				<input type="text" name="ZipCode" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">LandLine_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="LandLine_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Phone_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Phone_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Alternate_Number</label>
							<div class="col-md-9">
		        				<input type="text" name="Alternate_Number" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Website</label>
							<div class="col-md-9">
		        				<input type="text" name="Website" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status :</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div-->
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
