@extends('bikationvendor_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Packages Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Packages</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>id</th>
					<th>Trip ID</th>
					<th>Package ID</th>
					<th>Activity ID</th>
					<th>Exclusion_Inclusion</th>
					<th>Std_Opt</th>
					<th>Cost</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bikation_addonList as $bikation_addon)
					<tr>
						<td>{{ $bikation_addon->id }}</td>
						<td>{{ $bikation_addon->Trip_ID }}</td>
						<td>{{ $bikation_addon->Package_ID }}</td>
						<td>{{ $bikation_addon->Activity_ID }}</td>
						<td>{{ $bikation_addon->Exclusion_Inclusion }}</td>
						<td>{{ $bikation_addon->Std_Opt }}</td>
						<td>{{ $bikation_addon->Cost }}</td>
						<td>
							<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_addon->id }}" data-TripID="{{ $bikation_addon->Trip_ID }}" data-CustomerID="{{ $bikation_addon->Customer_ID }}" data-PackageID="{{ $bikation_addon->Package_ID }}" data-ActivityID="{{ $bikation_addon->Activity_ID }}" data-StdOpt="{{ $bikation_addon->Std_Opt }}" data-Cost="{{ $bikation_addon->Cost }}" ><i class="fa fa-pencil-square-o"></i> Edit</button>
							<button class="btn btn-info wk-btn bikeModel_delete_btn"  data-id="{{ $bikation_addon->id }}"><i class="fa fa-pencil-square-o"></i> Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Admin.Bikation_Addon.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/bikation-addon/add" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD PACKAGES</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Activity_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Activity_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Exclusion_Inclusion:</label>
							<div class="col-md-3">
		        				<input type="text" name="Exclusion_Inclusion" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Std_Opt</label>
							<div class="col-md-9">
		        				<input type="text" name="Std_Opt" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT Address</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Trip_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Valid_To :</label>
							<div class="col-md-9">
		        				<input type="text" name="Valid_To" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_ID" id="Created_By" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Activity_ID</label>
							<div class="col-md-9">
		        				<input type="text" name="Activity_ID" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-4">Exclusion_Inclusion:</label>
							<div class="col-md-3">
		        				<input type="text" name="Exclusion_Inclusion" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Std_Opt</label>
							<div class="col-md-9">
		        				<input type="text" name="Std_Opt" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Cost" class="form-control"/>
		        			</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	
@stop	