@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Booking</h2>
		<div class="col-xs-7 text-left"><button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Booking</button>
		</div>
		
		<form method="post" action="/admin/bikation-bookings" class="form-horizontal" enctype="multipart/form-data">
		
			
			
			<div class="col-xs-3 text-right text-left-xs">
					<select name="Trip_ID" class="form-control">
												<option value="">Select Trip</option>
												<?php $seldata =''; ?>
												@foreach($bikation_AlltripList as $bikation_trip)
												@if($bikation_trip->Trip_ID  == $selected)
													<?php $seldata= 'selected';?>
												@else
													<?php $seldata= ''; ?>
												@endif
													<option <?php echo $seldata; ?> value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
												@endforeach

					</select>
			</div>
			<div class="col-xs-2 text-right text-left-xs">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" name="btn_submit" class="btn btn-default"><i class="fa fa-search"></i>Select</button>	
			<button type="submit" name="btn_reset" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</button>	
			</div>
		
		</form>
		
		
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Booking NO</th>
					<th>Trip Name</th>
				<!-- 	<th>Customer ID</th> -->
					<th>User</th>
				<!-- 	<th>Booked On</th>
					<th>Booked By</th> -->
					<th>No of Tickets 1px</th>
					<th>No of Tickets 2px</th>
					<th>Package Cost</th>
					<!--th>Addon cost</th-->
					<th>Total Cost</th>
				<!-- 	<th>Cancelled On</th>
					<th>Cancelled By</th> -->
					<th>Vehicle</th>
				<!-- 	<th>Additional Remarks</th> -->
					<th>Booked Type</th>
					<th>Payment status</th>
					<!-- <th>Status</th> -->
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_bookingsList as $bikation_bookings)
						<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_bookings['Trip_Name'] }}</td>
					<!-- 	<td>{{ $bikation_bookings->Customer_ID }}</td> -->
						<td>{{ $bikation_bookings['first_name'] }} {{ $bikation_bookings['last_name'] }} <br>{{ $bikation_bookings['email'] }}<br>{{ $bikation_bookings['mobile_num'] }}</td>
					<!-- 	<td>{{ $bikation_bookings->Booked_On }}</td>
						<td>{{ $bikation_bookings->Booked_By }}</td> -->
						<td>{{ $bikation_bookings->No_of_Tickets_Cost_1px }}</td>
						<td>{{ $bikation_bookings->No_of_Tickets_Cost_2px }}</td>
						<td>{{ $bikation_bookings->Package_Cost }}</td>
						<!--td>{{ $bikation_bookings->Addon_cost }}</td-->
						<td>{{ $bikation_bookings->Total_Cost }}</td>
					<!-- 	<td>{{ $bikation_bookings->Cancelled_On }}</td>
						<td>{{ $bikation_bookings->Cancelled_By }}</td> -->
						<td>{{ $bikation_bookings->Vehicle }}</td>
						<!-- <td>{{ $bikation_bookings->Additional_Remarks }}</td> -->
						<td>{{ $bikation_bookings->Booked_Type }}</td>
						<td>@if( $bikation_bookings->Payment_status == 1 )
								<button class="btn btn-success">Success</button>
							@else
								<button class="btn btn-danger">Failed</button>
							@endif
						</td>
						<!-- <td>
							@if( $bikation_bookings->Status == 1 )
								Active
							@else
								Failed
							@endif
						</td> -->
						<td>
								<button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_bookings->Booking_ID }}" data-tripid="{{ $bikation_bookings->Trip_ID }}" data-customerid="{{ $bikation_bookings->Customer_ID }}" data-packageid="{{ $bikation_bookings->Package_ID }}" data-bookedon="{{ $bikation_bookings->Booked_On }}" data-bookedby="{{ $bikation_bookings->Booked_By }}" data-noofpeople="{{ $bikation_bookings->No_of_People }}" data-noofticketscost1px="{{ $bikation_bookings->No_of_Tickets_Cost_1px }}" data-noofticketscost2px="{{ $bikation_bookings->No_of_Tickets_Cost_2px }}" data-status="{{ $bikation_bookings->Status }}" data-packagecost="{{ $bikation_bookings->Package_Cost }}" data-addoncost="{{ $bikation_bookings->Addon_cost }}" data-totalcost="{{ $bikation_bookings->Total_Cost }}" data-cancelledon="{{ $bikation_bookings->Cancelled_On }}" data-cancelledby="{{ $bikation_bookings->Cancelled_By }}" data-vehicle="{{ $bikation_bookings->Vehicle }}" data-additionalremarks="{{ $bikation_bookings->Additional_Remarks }}"  data-razorpay_id="{{ $bikation_bookings->Razorpay_id }}" data-razorpay_method="{{ $bikation_bookings->Razorpay_method }}" data-razorpay_card_id="{{ $bikation_bookings->Razorpay_card_id }}" data-razorpay_bank="{{ $bikation_bookings->Razorpay_bank }}" data-razorpay_wallet="{{ $bikation_bookings->Razorpay_wallet }}" data-trip_name="{{ $bikation_bookings->Trip_Name }}" data-trip_start_date="{{ $bikation_bookings->Trip_Start_Date }}" data-trip_end_date="{{ $bikation_bookings->Trip_End_Date }}" data-start_location="{{ $bikation_bookings->Start_Location }}" data-end_location="{{ $bikation_bookings->End_Location }}" data-total_distance="{{ $bikation_bookings->Total_Distance }}" data-total_riding_hours="{{ $bikation_bookings->Total_riding_hours }}" data-no_of_stops="{{ $bikation_bookings->No_of_stops }}" data-first_name="{{ $bikation_bookings->first_name }}" data-last_name="{{ $bikation_bookings->last_name }}" data-email="{{ $bikation_bookings->email }}" data-mobile_num="{{ $bikation_bookings->mobile_num }}" data-location="{{ $bikation_bookings->location }}"><i class="fa fa-pencil-square-o"></i> View</button><br/>
								<button class="btn btn-danger wk-btn bikeModel_delete_btn" style="    margin-bottom: 5px;"  data-id="{{ $bikation_bookings->Booking_ID }}" data-tripid="{{ $bikation_bookings->Trip_ID }}"><i class="fa fa-close"></i> Delete</button><br/>
								<button class="btn btn-danger bikeModel_cancel_btn"  data-id="{{ $bikation_bookings->Booking_ID }}" data-tripid="{{ $bikation_bookings->Trip_ID }}"><i class="fa fa-close"></i> Cancel Booking</button>
						</td> 
					</tr>
					 <?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>

	

	<script type="text/javascript">
		$(function(){
	      Bookings_Admin.BikationBookings_Admin.init();
	    });
	</script>

@stop		

@section('model')
	
	<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/admin/bikation-bookings/add" class="form-horizontal" enctype="multipart/form-data" id="bikation-bookings-add">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD BOOKING</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" id="Trip_ID" class="form-control">
		        					<option value="">Select</option>
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name</label>
							<div class="col-md-9">
								<select name="Package_ID" id="Package_ID" class="form-control">
									<!--option value="0">Select</option>
		        					@foreach($bikation_packageList as $bikation_package)
								  			<option value="{{ $bikation_package->Package_ID }}">{{ $bikation_package->name }}</option>
								  	@endforeach-->
								</select>
							</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Booked On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-4">Booked By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Customer Name</label>
							<div class="col-md-9">
		        				<input type="text" name="Cust_Name" id="Cust_Name" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Customer Email</label>
							<div class="col-md-9">
		        				<input type="text" name="Cust_email" id="Cust_email" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Customer Mobile</label>
							<div class="col-md-9">
		        				<input type="text" name="Cust_mob" id="Cust_mob" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">No of People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets 1px</label>
							<div class="col-md-9">
		        				<!--input type="text" name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control"/-->
								<select name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control">
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets 2px</label>
							<div class="col-md-9">
		        				<!--input type="text" name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control"/-->
								<select name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control">
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Cost </label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Additional Remarks:</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="Additional_Remarks" id="Additional_Remarks"></textarea>
		        			</div>
		        		</div>
		        		<!--div class="form-group">
			        		<label class="col-md-3">Status</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div-->
						<input type="hidden" name="Booked_Type" value="Admin" class="form-control"/>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="editBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" enctype="multipart/form-data" id="bikation-bookings-edit">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View BOOKING</h4>
		      		</div>
					
					<div class="modal-body">
					<div class="well">
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Booking Details: </h4></p>
		        		<div class="form-group"> 
							<label class="col-md-2"><strong>Booking ID :</strong></label>
							<div class="col-md-4">
								<div id="Booking_ID">
								</div>
		        			</div> 
							<label class="col-md-2"><strong>Booked Date :</strong></label>
							<div class="col-md-4">
		        				<div id="Booked_On">
								</div>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-2"><strong>No of Tickets Cost 1px :</strong></label>
							<div class="col-md-4">
								<div id="No_of_Tickets_Cost_1px">
								</div>
		        			</div>
							<label class="col-md-2"><strong>No of Tickets Cost 2px :</strong></label>
							<div class="col-md-4">
								<div id="No_of_Tickets_Cost_2px">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-2"><strong>Total Cost :</strong></label>
							<div class="col-md-4">
								<div id="Total_Cost">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Vehicle :</strong></label>
							<div class="col-md-4">
								<div id="Vehicle">
								</div>
		        			</div>
		        		</div>
						
		        		
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Payment Details: </h4></p>
						<div class="form-group">
							<label class="col-md-2"><strong>Razorpay id :</strong></label>
							<div class="col-md-4">
								<div id="Razorpay_id">
								</div>
		        			</div>
							<label class="col-md-2"><strong>Razorpay method :</strong></label>
							<div class="col-md-4">
								<div id="Razorpay_method">
								</div>
		        			
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Razorpay card id :</strong></label>
							<div class="col-md-3">
								<div id="Razorpay_card_id">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Razorpay bank :</strong></label>
							<div class="col-md-3">
								<div id="Razorpay_bank">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Razorpay wallet :</strong></label>
							<div class="col-md-3">
								<div id="Razorpay_wallet">
								</div>
		        			</div>
						</div>
						
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Trip Details: </h4></p> 
						<div class="form-group">
							<label class="col-md-3"><strong>Trip Name :</strong></label>
							<div class="col-md-3">
								<div id="Trip_Name">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Trip Start Date :</strong></label>
							<div class="col-md-3">
								<div id="Trip_Start_Date">
								</div>
		        			</div>
		        		</div>	
						<div class="form-group">
							<label class="col-md-3"><strong>Trip End Date :</strong></label>
							<div class="col-md-3">
								<div id="Trip_End_Date">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Start Location :</strong></label>
							<div class="col-md-3">
								<div id="Start_Location">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>End Location :</strong></label>
							<div class="col-md-3">
								<div id="End_Location">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Total Distance :</strong></label>
							<div class="col-md-3">
								<div id="Total_Distance">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Total riding hours :</strong></label>
							<div class="col-md-3">
								<div id="Total_riding_hours">
								</div>
		        			</div>
							<label class="col-md-3"><strong>No. of stops :</strong></label>
							<div class="col-md-3">
								<div id="No_of_stops">
								</div>
		        			</div>
		        		</div>
						<p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">User Details: </h4></p> 
						<div class="form-group">
							<label class="col-md-3"><strong>Name :</strong></label>
							<div class="col-md-3">
								<div id="first_name">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Email :</strong></label>
							<div class="col-md-3">
								<div id="email">
								</div>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3"><strong>Mobile :</strong></label>
							<div class="col-md-3">
								<div id="mobile_num">
								</div>
		        			</div>
							<label class="col-md-3"><strong>Location :</strong></label>
							<div class="col-md-3">
								<div id="location">
								</div>
		        			</div>
		        		</div>
						
						
						
		        		</div>
		        		
		      		</div>
					
		      		<!--div class="modal-body">
						<div class="form-group"> 
							<label class="col-md-3">Trip Name :</label>
							<div class="col-md-9">
		        				<select name="Trip_ID" class="form-control">
		        					@foreach($bikation_tripList as $bikation_trip)
								  			<option value="{{ $bikation_trip->Trip_ID }}">{{ $bikation_trip->Trip_Name }}</option>
								  	@endforeach
								</select>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Customer_ID :</label>
							<div class="col-md-9">
		        				<input type="text" name="Customer_ID" id="Customer_ID" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Name</label>
							<div class="col-md-9">
								<select name="Package_ID" class="form-control">
									<option value="0">Select</option>
		        					@foreach($bikation_packageList as $bikation_package)
								  			<option value="{{ $bikation_package->Package_ID }}">{{ $bikation_package->name }}</option>
								  	@endforeach
								</select>
							</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Booked On</label>
							<div class="col-md-9">
		        				<input type="text" name="Booked_On" id="Booked_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-4">Booked By</label>
							<div class="col-md-3">
		        				<input type="text" name="Booked_By" id="Booked_By" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">No of People</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_People" id="No_of_People" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets Cost 1px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_1px" id="No_of_Tickets_Cost_1px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">No of Tickets Cost 2px</label>
							<div class="col-md-9">
		        				<input type="text" name="No_of_Tickets_Cost_2px" id="No_of_Tickets_Cost_2px" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Package Cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Package_Cost" id="Package_Cost" class="form-control"/>
		        			</div>
		        		</div>
						<div class="form-group">
							<label class="col-md-3">Addon cost</label>
							<div class="col-md-9">
		        				<input type="text" name="Addon_cost" id="Addon_cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Total Cost </label>
							<div class="col-md-9">
		        				<input type="text" name="Total_Cost" id="Total_Cost" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled On</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_On" id="Cancelled_On" class="form-control date_time"/>
		        			</div>
		        		</div>
		        		<div class="form-group" hidden>
							<label class="col-md-3">Cancelled By</label>
							<div class="col-md-9">
		        				<input type="text" name="Cancelled_By" id="Cancelled_By" value="{{ Session::get('Vendor_ID') }}" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Vehicle</label>
							<div class="col-md-9">
		        				<input type="text" name="Vehicle" id="Vehicle" class="form-control"/>
		        			</div>
		        		</div>
		        		<div class="form-group">
							<label class="col-md-3">Additional Remarks:</label>
							<div class="col-md-9">
								<textarea class="form-control textarea" rows="3" name="Additional_Remarks" id="Additional_Remarks"></textarea>
		        			</div>
		        		</div>
		        		<div class="form-group">
			        		<label class="col-md-3">Status</label>
			        		<div class="col-md-9"> 
				        		<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose1" value="1"> Active
								</label>
								<label class="radio-inline">
								  <input type="radio" name="Status" id="bmose2" value="0"> Inactive
								</label>
								<label id="requestorStatus">
        						</label>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div-->
		      		<div class="modal-footer">
		      			<!--button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button-->
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Delete Record</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
								<input type="hidden" name="Trip_ID" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="CancelspecBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Are You Sure Cancel Booking?</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
								<input type="hidden" name="Trip_ID" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
@stop	
