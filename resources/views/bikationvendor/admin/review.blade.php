@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Reviews Homepage</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Reviews</button> -->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Trip Name</th>
				<!-- 	<th>Customer_ID</th> -->
					<th>Overall</th>
					<th>Remarks</th>
					<!--th>Planning</th>
					<th>On_Schedule</th>
					<th>Food</th>
					<th>Stay</th>
					<th>Activities</th-->
					
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php  $i=1; ?>
				@foreach($bikation_reviewsList as $bikation_review)
					<tr>
						<td><?php echo $i ?></td>
						<td>{{ $bikation_review['TripName'] }}</td>
						<!-- <td>{{ $bikation_review->Customer_ID }}</td> -->
						<td>{{ $bikation_review->Overall }}</td>
						<td>{{ $bikation_review->Remarks }}</td>
						<!--td>{{ $bikation_review->Planning }}</td>
						<td>{{ $bikation_review->On_Schedule }}</td>
						<td>{{ $bikation_review->Food }}</td>
						<td>{{ $bikation_review->Stay }}</td>
						<td>{{ $bikation_review->Activities }}</td-->
						
						<td>
							@if( $bikation_review->Status == 1 )
								Active
							@else
								Inactive
							@endif
						</td>
						<td><a href="{{ url('admin/bikation-reviews/change-status/'.$bikation_review->id) }}"><button class="btn btn-primary">Change Status</button></a>
						<!-- <button class="btn btn-info wk-btn bikeModel_edit_btn"  data-id="{{ $bikation_review->id }}" data-tripid="{{ $bikation_review->Trip_ID }}" data-customerid="{{ $bikation_review->Customer_ID }}" data-overall="{{ $bikation_review->Overall }}" data-planning="{{ $bikation_review->Planning }}" data-onschedule="{{ $bikation_review->On_Schedule }}" data-food="{{ $bikation_review->Food }}" data-stay="{{ $bikation_review->Stay }}" data-activities="{{ $bikation_review->Activities }}" data-remarks="{{ $bikation_review->Remarks }}" data-status="{{ $bikation_review->Status }}"><i class="fa fa-pencil-square-o"></i>View Review</button> --></td>

					</tr>
					<?php  $i++ ?>
				@endforeach
			</tbody>
		</table>
	</div>
	
	

@stop		

