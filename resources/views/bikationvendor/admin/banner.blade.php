@extends('admin_layout')

@section('content')
	
	<div class="bikeModel-pg">
		<h2 class="title">Trip Banner</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i> Add Trip</button> -->
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		
		
	</div>
	
	<form method="post" class="form-horizontal" action="/admin/bikation-add-banner" enctype="multipart/form-data" id="bikation-banner-add">
		      		
		      		<div class="modal-body">
					<div class="well">
					<div class="col-md-8"> 
						<!--p class = "list-group-item-text"><h4 class = "list-group-item-heading bg-info">Trip Details: </h4></p-->
		        		<div class="form-group"> 
							<label class="col-md-3"><strong>Banner Image :</strong></label>
							<div class="col-md-6">
								<input type="file" name="banner_img" id="banner_img" value="" />
							</div>
							
						</div>
						<div class="form-group"> 
						<label class="col-md-3"></label>
		        		<label class="col-md-6"><p class="text-primary small">* Image size 1200*460 resolution</p></label>
						</div>
						<div class="form-group"> 
							<label class="col-md-3"><strong>Banner Text :</strong></label>
							<div class="col-md-6">
							@if(!empty($bikation_bannerList->banner_title))
								<textarea type="text" name="banner_text" id="banner_text" rows="3" style="width:100%" />{{$bikation_bannerList->banner_title}}</textarea>
							@else
								<textarea type="text" name="banner_text" id="banner_text" rows="3" style="width:100%" /></textarea>
							@endif
							</div> 
						</div>
					</div>	
					<div class="col-md-4"> 
					@if(!empty($bikation_bannerList->image))
						<img src="{{$bikation_bannerList->image}}" width="200" height="200"/>			
						<input type="hidden" name="prev_img" value="{{$bikation_bannerList->image}}">
						@endif
						
					</div>	
						<div class="form-group"> 
							<label class="col-md-2"></label>
							<div class="col-md-6">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
							</div> 
						</div>
						
		        	
						
		        		</div>
		        		
		      		</div>
		      		
		    	</form>
	
@stop		
