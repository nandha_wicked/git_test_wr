<!DOCTYPE html>
<html>
	<head>
		<meta name="_token" content="{{ Session::token() }}">
		<link rel="stylesheet" href="/css/admin-style.css?a=1" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
		<link rel="stylesheet" href="/css/jquery.dataTables.min.css" />
		<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	</head>
    
	<body class="admin-body">
        
    <nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><?php echo env('SITENAMECAP');?></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
                    <li><a href="/logout">Logout<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-sign-out"></span></a></li>
                   
					<li><a href="/partner/booking"> Booking<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-book"></span></a></li>
					<li><a href="/partner/bike-filter"> Bike Filter<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-filter"></span></a></li>
					
				
				
			</ul>
		</div>
	</div>
</nav>
        
		<div class="admin-wrapper">
			<div class="header">
				
				
					@if (isset(Auth::user()->first_name))
		                                       
		            @else 
                        <div class="block2">
                            <a href="/" target="_blank">View Site</a>
		                <a href="#" data-toggle="modal" data-target="#loginModal">LOGIN</a>
                            </div>
		            @endif 							
				
                
            
                
			</div>
            <div class="content">
				@yield('content')
				@yield('model')
			</div>
			
			
		</div>	
		<script type="text/javascript" src="/js/moment.js"></script>
		<script type="text/javascript" src="/js/back.js?a=1"></script>
		<script type="text/javascript" src="/js/backend.js?a=1"></script>
		<script type="text/javascript" src="/js/admin.js?a=1"></script>
		<script type="text/javascript" src="/js/promocode.js?a=1"></script>
        <script type="text/javascript" src="/js/partner.js?a=1"></script>
		<link rel="stylesheet" href="/css/spinner.css" />
		
		<div id="loader" class="backdrop">
			<div class="loader-div">
				<div class="gauge-loader">
				  Loading…
				</div>
			</div>
		</div>
	</body>
</html>
