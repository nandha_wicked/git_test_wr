@extends('front/frontMaster')

@section('content')

<!-- form starts here -->
	<div class="form_main happy_customer_form password-reset-mainpg" style="padding-top: 100px;">
		<div class="width_960">

			<div class="cd-tabs bookingPg-formContainer" id="sign-inup-container">
				<nav>
					<ul class="cd-tabs-navigation">
						<li><a data-content="inbox" class="selected" href="#0">Forgot Password</a></li>
					</ul> <!-- cd-tabs-navigation -->
				</nav>

				<ul class="cd-tabs-content">
					<li data-content="inbox" class="selected">
						<form class="form" method="post" action="/password/email" id="forgot-password-form">
							@if(Session::has('status'))
							    <div class="alert alert-success success-msg">
							        {{ Session::get('status') }}
							    </div>
							@endif
							@if (count($errors) > 0)
							<div class="alert alert-danger error-msg">
								<ul>
									@foreach ($errors->all() as $error)
										<li>* {{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<ul>
								<div class="login_for_booking only_for_booking_login">
									<h4>Enter login email</h4>
									<div id="login-msg"></div>
									<div class="clear_both"></div>
									<li class="pwd-li"><input type="text" name="email" value="" placeholder="email"></li>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div><a href="/login" class="return-btn"><span class="indicator">Return to login</span></a></div>
									<div class="clear_both"></div>
									<div class="common_btn_big"><span></span><input class="submit" type="submit" value="Send Password Reset Link"></div>
									<div class="connect_with">
										<div class="indicator">or connect with</div>
										<div><a href=""><img src="images/google-icon.png"></a></div>
									</div>
								</div>
							</ul>
						</form> 
					</li>
				</ul> <!-- cd-tabs-content -->
			</div> <!-- cd-tabs -->

		</div>
	</div>
	<!-- form ends here-->

@stop