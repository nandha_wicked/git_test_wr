@extends('front/frontMaster')

@section('content')
	
	<!-- form starts here -->
<div class="form_main happy_customer_form password-reset-pg">
	<div class="width_960">
		<h4 class="hd">Reset your password here.</h4>
		@if (count($errors) > 0)
			<div class="alert alert-danger error-msg">
				<ul>
					@foreach ($errors->all() as $error)
						<li>* {{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form class="form" method="post" action="/password/reset">
			<ul>
				<li><input type="text" name="email" value="{{ Input::old('email') }}" placeholder="email"></li>
				<li><input type="password" name="password" value="" placeholder="password"></li>
				<li><input type="password" name="password_confirmation" value="" placeholder="confirm password"></li>
				<div class="clear_both"></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="token" value="{{ $token }}">
				<div class="common_btn_big"><span></span><input class="submit" type="submit" value="Reset Password"></div>
			</ul>
		</form>
	</div>
</div>
<!-- form ends here-->

@stop