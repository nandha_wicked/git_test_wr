<!DOCTYPE html>
<html>
	<head>
		<meta name="_token" content="{{ Session::token() }}">
		<link rel="stylesheet" href="/css/admin-style.css?a=1" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
		<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>		
	</head>
	<body class="admin-body">
		<div class="admin-wrapper">
			<div class="header">
				<div class="block1">
					<div><i class="fa fa-tachometer"></i> Dashboard</div>
				</div>
				<div class="block2">
					<a href="/" target="_blank">View Site</a>
					@if (isset(Auth::user()->first_name))
		                <span class="name">Hello, {!! Auth::user()->first_name !!}</span>                        
		                <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>		                        
		            @else                       
		                <a href="#" data-toggle="modal" data-target="#loginModal">LOGIN</a>
		            @endif 							
				</div>
			</div>
			<div class="sidebar">
				<div class="logo">
					<a href="/vendor"><img src="/img/logo.png"></a>
				</div>
				<div class="title">MAIN NAVIGATION</div>
				<ul class="main-nav">


					<li><a href="/vendor/price"><i class="fa fa-wrench"></i> Price</a></li>
					<li><a href="/vendor/bike"><i class="fa fa-motorcycle"></i> Bike</a></li>
					<li><a href="/vendor/booking"><i class="fa fa-book"></i> Booking</a></li>
					<li><a href="/vendor/bike-filter"><i class="fa fa-filter"></i> Bike Filter</a></li>

				</ul>
			</div>
			<div class="content">
				@yield('content')
				@yield('model')
			</div>
		</div>	
		<script type="text/javascript" src="/js/moment.js"></script>
		<script type="text/javascript" src="/js/back.js?a=1"></script>
		<script type="text/javascript" src="/js/backend.js?a=1"></script>
		<script type="text/javascript" src="/js/admin.js?a=1"></script>
		<script type="text/javascript" src="/js/promocode.js?a=1"></script>
		<link rel="stylesheet" href="/css/spinner.css" />
		
		<div id="loader" class="backdrop">
			<div class="loader-div">
				<div class="gauge-loader">
				  Loading…
				</div>
			</div>
		</div>
	</body>
</html>