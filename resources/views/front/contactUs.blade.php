@inject('areaObj','App\Models\Area')
@extends('front/frontMaster')

@section('title')
	<title>Contact us for Ride Reservation. Book Your Luxury Bikes</title>
@stop


@section('meta')
	<meta name="description" content="Contact us to rent a ride or book luxurious bike &amp; make your day memorable. For more details you can email us at customer-support@bounceshare.com.">
@stop

@section('content')
<div class="clear_both"></div>

<!-- contact main starts here-->
<div class="contact_main">
	<!--reservation starts here-->
	<div class="reservation_main">
		<h1 class="item h1-clear">Contact - Wicked Ride</h1>
		<div class="width_960">
			<div class="reserve_sec">
				<span class="write-us">Please write to us:</span>
				<!-- <h5 class="rervation_big"><a href="mailto:reservations@metrobikes.in">reservations@metrobikes.in</a></h5> -->
                <p>For customer inquiries or complaints </p>
				<h5 class="rervation_big" style="text-transform:lowercase;"><a href="mailto:customer-support@bounceshare.com">customer-support@bounceshare.com</a></h5>
				<!-- <h5 class="rervation_big"><a href="mailto:reservations@metrobikes.in">reservations@metrobikes.in</a></h5> -->


                <br>
                <p> For any other (non-customer related) inquiries</p>
				<h5 class="rervation_big" style="text-transform:lowercase;"><a href="mailto:customer-support@bounceshare.com">reservations@wickedride.com </a></h5>
			</div>
			<div class="reserve_sec">
				<span>You can call us at</span>
				<h5 class="rervation_big">080 4680 1054</h5>
			</div>
		</div>
	</div>

	<!-- maps starts here-->
<div class="map_main">
	<div class="map_bg">
		<div class="width_960">
			<h4>find us near you</h4>
			<div class="celect_city_main">
				<form>
					<div class="celect_city">
			 			<label id="city_dropdown_container">						   
							<option>Select</option>
						</label>
			 		</div>
			 		<div class="celect_city">
			 			<label id="area_dropdown_container">
			 				<select>
			 					<option>Please select city</option>
			 				</select>						    
						</label>
			 		</div>
			 	</form>		 	
	 		</div>
 		</div>
	</div>
	
	<div id="googleMap" class="map">
	</div>
</div>
<!-- maps ends here-->


	<!--reservation ends here-->
	<div class="clear_both"></div>
  	<!-- form starts here -->
	<div class="form_main">
		<div class="width_960">
			@if (count($errors->contact) > 0)
				<div class="alert alert-danger contact-err-msg">
					<ul>
						@foreach ($errors->contact->all() as $error)
							<li>* {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@if (session('contactSuccessMsg'))
    			<div class="alert alert-success contact-success-msg contact-pop">
                    {{ session('contactSuccessMsg') }}
                    <span class="close-btn">&times;</span>
    			</div>
			@endif
            <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
			<form class="form" method="post" action="/contact-enquiry/add" name="contact-form">
				
				<ul>
					<li><input type="text" name="firstName" value="" placeholder="first name"></li>
					<li><input type="text" name="lastName" value="" placeholder="last name"></li>
					<li><input type="text" name="email" value="" placeholder="email id"></li>
					<li><input type="text" name="mobileNumber" value="" placeholder="mobile number"></li>
					<!-- <li class="full_width"><textarea rows="2" cols="10" type="textfield" name="address" value="" placeholder="address"></textarea></li> -->					
					<li class="full_width"><textarea rows="6" cols="10" type="textfield" name="comment" value="" placeholder="comments if any"></textarea></li>
                    <li> <div id="captcha_container"></div> </li>
<!--                  <li> <div class="g-recaptcha" 
                    data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                </div></li>
 -->
					<div class="clear_both"></div>
					<!-- <div class="text_align_center"><input class="check" type="checkbox" name="vehicle" value="Bike"><label>sign me up for the monthly news letter</label></div> -->
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="common_btn_big"><span></span><input class="submit" type="submit" value="Submit"></div>
					<!--<div class="register">
						<div class="border_top">
							<div class="common_btn_big"><span></span><a href="">Register</a></div>
							<div class="common_btn_big"><span></span><a href="">Login</a></div>
						</div>
					</div>-->
				</ul>
			</form>
		
                <div>
                    <br /><br /><br />
            
    <?php $areas = $areaObj->getAllActiveAreas()->sortBy('city_id'); ?>
	 			 					@foreach($areas as $area)
                    <h5 align="center">{{$area->getCityName($area->city_id)}} - {{$area->area}} </h5>
	 			 						<p align="center"> {{$area->address}}</p>
                    <br />
	 			 					@endforeach

    </div>
            
            
        </div>
	</div>
	<!-- form ends here-->

</div>
<!-- contact main ends here-->


<div class="clear_both"></div>
	
@stop	
