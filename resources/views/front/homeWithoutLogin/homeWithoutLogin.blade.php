@extends('front/frontMasterHome')
@inject('newsObj','App\Models\News')
@inject('bikeMakeObj','App\Models\BikeMake')
@inject('bikeModelObj','App\Models\BikeModel')
@inject('cityObj','App\Models\City')
@inject('areaObj','App\Models\Area')
@inject('frontEndText','App\Models\FrontEndText')



		<?php 
			$city_id = Session::get('current_cityId');
        	if(!$city_id)
            	$city_id = "1";

            $city = $cityObj->getCityById($city_id); 

		?>


@section('title')
	<title>Rent a Bike in {{ $city->city }} | Wicked Ride</title>
@stop


@section('meta')
	<meta name="description" content="Rent bikes such as Enfields, Harley-Davidson, Triumph, Ducati, Kawasaki, KTM, Avenger, in {{ $city->city }} from Wicked Ride, India’s first premium motorcycle rental company and the most trusted motorcycle rental company. No security deposit for bike rentals and comes with Insurance coverage.">
@stop

@section('content')




<!-- START MAIN SLDER SECTION -->
<div class="extra-slider hidden-xs">
<div id="" class="home_slider wrapper">
    <ul class="homeslide">
	    <li class="current" style="background-image: url('https://wickedride.com/img/slider/1485608537.jpeg');">
	      <div class="container">
		    	<div id="blurb" class="banner_content" >
		    		
                    <h1 class="item h1-clear motor_bg common_btn_big_white hidden-xs"><span></span><a href="/booking/select-date">book now</a></h1>
		    	</div>
	       </div>
	    </li>
	    @foreach($sliderImages as $sliderImage)
            @if(!$sliderImage['description'])
                <li class="current" >

                    <a href="{{$sliderImage->url}}"><img class="homeslide home_slider" src="{{$sliderImage->image}}" style="height: 500px;"></a> 	

                </li>
            @else
                <li class="current" style="background-image: url('{{$sliderImage->image}}');">
                  <div class="container">
                        <div id="blurb" class="banner_content" >
                            
                            <h1 class="item h1-clear motor_bg">&nbsp;{{$sliderImage['description']}}</h1>
                            <br/>
                            <br/>
                            <br/>
                            <h1 class="item h1-clear motor_bg common_btn_big_white hidden-xs"><span></span><a href="{{$sliderImage->url}}">{{$sliderImage['button_description']}}</a></h1>
                        </div>
                   </div>
                </li>
            @endif
        @endforeach
    </ul>                
    <div class="banner_nav navigation" style="display: block;">
	        <a class="prev" href=""></a>
	        <a class="next" href=""></a>
	    </div>

	    <div class="latest_news">
	    	<div class="container">
	    		<span>Latest News :</span>
	    			@foreach($newsObj->getActiveNews() as $news)
	    			    <p class="quotes">{{ $news->news }}</p>
                    @endforeach
	    	</div>
	    </div>
</div> 
</div>





<div id="main_slider" class="visible-xs">

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="http://wickedride.com/img/slider/mobile1485608537.jpeg" alt="..." class="img-responsive" width="100%">
      <div class="carousel-caption">
        	
		    		
      </div>
    </div>

        @foreach($sliderImages as $sliderImage)


            <div class="item">
                <a href="{{$sliderImage->url}}"><img src="{{$sliderImage->mobile_image}}" alt="..." class="img-responsive" width="100%">
                <div class="carousel-caption">
                    
                   

                </div></a>
            </div>
        @endforeach

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="fa fa-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="fa fa-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</div>


<!-- END MAIN SLIDER SECTION -->



<!-- START OUR FEET SECTION -->

		<section class="our_fleet_sec" id="feet">
			<div class="container-fluid">
				<div class="row">
					<h2>our fleet</h2>
				</div>
			</div>

			<ul class="titles">
                @foreach($bikeMakeObj->getUniqueBikeMakes($city_id) as $bikeMake)
				    <li><a href="#" data-filter="{{ $bikeMake->bike_make_id }}">{{ $bikeMake->bikeMake_name }}</a></li>
				@endforeach
			</ul>

			<ul id="bikes" class="fleet_bikes">
                @foreach($bikeModelObj->getUniqueBikeModels($city_id) as $bikeModel)

						<li class="{{ $bikeModel->bike_make_id }} animated">
									<!-- <a href="bike/"> -->
                            <?php $image = App\Models\Image::find($bikeModel->thumbnail_img_id); ?>
							<a href="/rent-a-<?php if($bikeModel->bikeMake_name == "Harley-Davidson") echo "Harley Davidson"; else echo $bikeModel->bikeMake_name; ?>-{{$bikeModel->bike_model}}">
								<div class="item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><img class="lazy" data-src="{{ $image['full'] }}" src="{{ $image['full'] }}"></div>
								<div class="clear_both"></div>
								<div class="itemh common_btn_small" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><span></span>{{ $bikeModel->bike_model }}</div>
							</a>
						</li>
                @endforeach
			</ul> 
	
		</section>

<!-- END OUR FEET SECTION -->



<!-- ride section starts here-->
<div class="wicked_ride_main">
	<h2>Bikation</h2>
	<style type="text/css">
		@font-face {font-family: 'CoffeeService';src: url('/fonts/webfonts/2EF6DB_0_0.eot');src: url('/fonts/webfonts/2EF6DB_0_0.eot?#iefix') format('embedded-opentype'),url('/fonts/webfonts/2EF6DB_0_0.woff2') format('woff2'),url('/fonts/webfonts/2EF6DB_0_0.woff') format('woff'),url('/fonts/webfonts/2EF6DB_0_0.ttf') format('truetype');}
		.coming_soon{font-family: CoffeeService; font-weight: normal; font-style: normal;  text-align: center; font-size: 76px; margin-top: 88px;}
	</style>
	 <section class="bookRide" style="padding:0">
            <div id="container" style="width: 100%;margin: 10px;text-align: center;">
                <div id="result" style="display: inline-block;">
				<?php $a=1; $c=0; ?>
                    
                
					 @foreach($packagesMakeList as $key => $packages)
                    
                     @if($c == 2||$c==1||$c==0)
						@if($a%2 == 0)	
							@if($a%4 == 0 || $a%5 == 0)	 	
							<div class="itemBikation-itemBikation2">
                            <img src="{{ $packages->Media_URL_trip3 }}" alt="">

							@else
							<div class="itemBikation">
                            <img src="{{ $packages->Media_URL_trip2 }}" alt="">
							@endif
						@else 	
							@if($a%4 == 0 || $a%5 == 0)	 	
							<div class="itemBikation-itemBikation2">
                             <img src="{{ $packages->Media_URL_trip3 }}" alt="">
							@else
							<div class="itemBikation-itemBikation1">
                            <img src="{{ $packages->Media_URL_trip1 }}" alt="">
							@endif
						@endif	
						
                        <div class="details">
							
							<div class="text-shade">
							{{ date('d F, Y H:i:s', strtotime($packages->Trip_Start_Date)) }}
                            <h3 class="white padding-vertical-5">{{ $packages->Trip_Name }}</h3> by {{ $packages->first_name }} {{ $packages->last_name }}</div>
                            <div class="bottom-details text-shade margin-right-0 margin-left-0">
								
                                <br>
                                <div class="col-xs-12 padding-left-5 padding-right-5">
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-exchange margin-right-5 padding-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $packages->Total_Distance }} KMS</span></i>
                                    </div>
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-calendar margin-right-5" aria-hidden="true"><span class="margin-left-10 bookridetext"><?php $datetime1 = date_create($packages->Trip_Start_Date);
                                           $datetime2 = date_create($packages->Trip_End_Date);
                                           $interval = date_diff($datetime1, $datetime2);
                                           echo $interval->format('%a Days');?> </span></i>
                                    </div>
                                    <div class="col-xs-4 padding-left-5 padding-right-5">
                                        <i class="icon fa-users margin-right-2" aria-hidden="true"><span class="margin-left-10 bookridetext">{{ $packages->No_of_Tickets_Cost_1px }} Seats</span></i>
                                    </div>
                                </div>
                            </div>
                            <a class="details-block btn btn-warning btn-xs btn-detail open-modal settings" href="trip/{{$packageDetail->Trip_url}}"></a>
                        </div>
                    </div>
					<?php $a++;
						if($a > 5)
							{$a = 1;}			
						
				    ?>
                                
                    <?php $c++;?>
                    @endif
					@endforeach
                   
                    <div class="col-xs-12 padding-bottom-10 ">
                    </div>
                </div>
            </div>
        </section>
	    <a href="/trip" class="btn btn-outline-warning col-sm-offset-2 col-sm-5 col-md-offset-3 col-md-6">MORE</a>
</div>
<!-- ride section ends here-->
                

<!-- START TESTIMONIAL SECTION -->

		<div class="testimonials_main">
		  <div class="container testimon_sec">
		  	<div class="row" id="review">
		  		<div class="col-md-6">
		  			<h4>happy customers</h4>
		  			
		  			<div class="clear_both"></div>

		  			<div class="left" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
						<div class="profile"><img class="lazy br100" data-src="images/testimonial-profile-01.jpg" src="images/testimonial-profile-01.jpg"></div>
						
						<div class="testimonial_copy">
							<p>
								I have been a Harley Davidson fan for a time more than I have known. All my friends have experienced my obsession with Harley and they will vouch for it. But for most of us, owning a Harley comes at a cost. And in situations like this Wicked Ride comes to your rescue. Thanks to these people, now riding a Harley or Truimph motorcycles is not a distant dream for us. 
								And if you are planning to buy one of these mean machines, what better way to take a test ride than to book a rental bike with Wicked Ride and go out on a day long trip (at least one day). 
							</p>
							<span class="profile_name">Mihir Kulkarni</span>
						</div>
					</div>
		  		</div>

		  		<div class="col-md-6">
		  			<div class="common_btn_big hidden-xs"><span></span><a href="/reviews">view all testimonials</a></div>

		  			<div class="right space30" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
						<div class="profile"><img class="lazy br100" data-src="images/testimonial-profile-02.jpg" src="images/testimonial-profile-02.jpg"></div>
						<div class="testimonial_copy">
							<p>Amazing riding experience in Bangalore city first with the Thunderbird 500 Royal Enfield and then the Harley Low rider with great customer service and very very friendly team. " I'LL BE BACK"</p>					
							<span class="profile_name">Gavin Morlini</span>									
						</div>
					</div>

					<div class="common_btn_big visible-xs"><span></span><a href="/reviews">view all testimonials</a></div>
		  		</div>
		  	</div>
		  </div>
		</div>

	<!-- END TESTIMONIAL SECTION -->

<div class="space50"></div>

<!-- START OF NEWS SECTION -->

<div class="featured_article_main">
     <div class="container">
        <h4>featured articles</h4>
        <div class="common_btn_big"><span></span><a href="">view all articles</a></div>
        <div class="clear_both"></div>
        <ul class="articles_sec row">
            <li class="col-md-6">
                    <div class="profile"><img class="lazy" data-src="images/bm.png" src="images/bm.png"></div>
                    <div class="article_copy">
                        <h5>Now, a Harley-Davidson on rent</h5>
                        <p>Featured on Aug 26, 2014</p>
                        <div class="common_btn_small mleft115"><a href="http://www.bangaloremirror.com/columns/work/Now-a-Harley-Davidson-on-rent/articleshow/40936863.cms" target="_blank">read this article</a></div>
                    </div>
                </li>
                <li class="col-md-6">
                    <div class="profile"><img class="lazy" data-src="images/dh.png" src="images/dh.png"></div>
                    <div class="article_copy">
                        <h5>Riding the dream machines</h5>
                        <p>Featured on Sep 01, 2014</p>
                        <div class="common_btn_small mleft115"><a href="http://www.deccanherald.com/content/428653/riding-dream-machines.html" target="_blank">read this article</a></div>
                    </div>
                </li>
        </ul>
     </div>
</div>

<!-- END OF NEWS SECTION -->

<!-- START FAQ SECTION -->

<section class="faq_main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="">
                    <div class="faq_main">
                     <div class="">
                        <h4>rental faqs</h4>
                        <div class="accordian_main">
                            <ul>
                                <li>
                                        <div class="accordian_title"><h6>How do I reserve a bike?</h6><span></span></div>
                                        <div class="open_para" style="display: none;">
                                            <p>
                                                Click <a href="/booking/select-date" target="_blank">here</a> and select the dates you are interested in. Select the bike you like from the list of available bikes and also select the pickup location of your choice. You will be asked to login or sign up. Sign up is easy with few essential details like email address and phone number. After that, reconfirm your selected bike, dates and pickup location and you will be taken to the payment gateway. Your reservation is confirmed after you make the payment.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordian_title"><h6>From where do I pick up the bike?</h6><span></span></div>
                                        <div class="open_para" style="">
                                            <p>
                                                You can pick up the rental bike on the day of your reservation from your selected location. The address will be mentioned in the confirmation email. Click <a href="/contact-us" target="_blank">here</a> for the addresses of all the Wicked Ride locations.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordian_title"><h6>What are the documents that I need to submit to take the bike?</h6><span></span></div>
                                        <div class="open_para" style="">
                                            <p>
                                                The following would be the documents that need to be submitted for renting the bike </p>
                                            <p><?php echo $frontEndText->getText('documents_required'); ?></p>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordian_title"><h6>When does my order get confirmed?</h6><span></span></div>
                                        <div class="open_para" style="">
                                            <p>
                                                Your booking will be confirmed on receipt of payment. You will receive an email from Wicked Ride confirming your booking and the address of the pick up location.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordian_title"><h6>Do you have late return penalties?</h6><span></span></div>
                                        <div class="open_para" style="">
                                            <p>
                                                Yes, we charge 3 days of bike rental as late return penalty. A 10 minute buffer may be provided at Wicked Ride's discretion. If you suspect that you will be late, please call ahead and extend your reservation to avoid the penalty. We will not be able to extend if there’s another confirmed reservation for the period.
                                            </p>
                                        </div>
                                    </li>

                            </ul>
                        </div>
                        <div class="faq_btns">
                            <div class="common_btn_big"><span></span><a href="/faqs">view all faqs</a></div>
                            <div class="common_btn_big"><span></span><a href="/contact-us">get in touch with us</a></div>
                        </div>
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END FAQ SECTION -->

<!-- START OF MAPS SECTION -->
                
                
<div class="clear_both"></div> 
<div class="map_main1">
	<div class="map_bg">
		<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4>find us near you</h4>
						</div>
					</div>

					<div class="row celect_city_main">
						<div class="col-md-6">
							<form>
								<div class="celect_city">
						 			<label id="city_dropdown_container"><select id="city_dropdown">
                                        <?php 
                                            //Session::put('current_cityId', 1);
                                            $session_cityId = Session::get('current_cityId'); 
                                            /*if(isset($session_cityId))
                                                $session_cityId = Session::get('current_cityId');
                                            else
                                                $session_cityId = "1";*/
                                        ?>
                                        <?php //var_dump($session_cityId);?>  
                                        <?php $cities = $cityObj->getActiveCities(); ?>
                                            @foreach($cities as $city)
                                                <option value="{{ $city->city }}" 
                                                    <?php 

                                                            if($city->id == $session_cityId) echo "selected"; 

                                                    ?> 
                                                >
                                                    {{ $city->city }}
                                                </option>
                                            @endforeach
                                        
                                        </select></label>
						 		</div>
						 	</form>
						</div>
						
						<div class="col-md-6">
							<form>
						 		<div class="celect_city">
						 			<label id="area_dropdown_container"><select id="area_dropdown"></select></label>
						 		</div>
						 	</form>		 	
				 		</div>
					</div>		
				</div>
	</div>
	
	<div id="googleMap" class="map">
	</div>
</div>
                
<script type="text/javascript">
    
    if(typeof jQuery!=='undefined'){
    console.log('jQuery Loaded');
    }
    else{
        console.log('not loaded yet');
    }
     var json = {
    <?php 
    $cities =$cityObj->getActiveCities();
    foreach($cities as $city)
    {
        echo "\"".$city->city."\":[";
    
        $areas = $areaObj->getAllActiveAreasForCity($city->id);
        foreach($areas as $area)
        {
            
            echo "{\"area\":\"".$area->area."\",\"gmapLink\":\"".$area->gmapLink."\"},";
        
        }
        
            echo "],";
    }
    
    ?>
    }
    
    

	// Create city dropdown
	var city_dropdown = "<select id='city_dropdown'>";
	for (var key in json) {			
		city_dropdown += '<option value="'+key+'" >';
		city_dropdown += key;
		city_dropdown += '</option>';						
	}	
	city_dropdown += "</select>";

	// Update city dropdown
	$('#city_dropdown_container').html(city_dropdown);
    
    function registerCityAreaDropdown(city)
	{
		var area_dropdown = "<select id='area_dropdown'>";
		for (var k in json[city]) {		        
	        area_dropdown += '<option value="'+key+'" data-gmaplink="'+json[city][k]['gmapLink']+'">';
			area_dropdown += json[city][k]['area'];
			area_dropdown += '</option>';	
	    }
		area_dropdown += "</select>";
		// update Area dropdown
		$('#area_dropdown_container').html(area_dropdown);	
		InitAreaDropdown();		
		var gmapLink = $( "#area_dropdown option:selected" ).data('gmaplink');
     	$('#gmapIframeSrc').attr('src',gmapLink);
	}	
	
	

	// On chnage of city dropdown create Area dropdown
	$("#city_dropdown").change(function () {
		registerCityAreaDropdown(this.value);
	});

    // On chnage of area dropdown update google map
    function InitAreaDropdown(){
		$("#area_dropdown").change(function () {
	     	var gmapLink = $(this).find(':selected').data('gmaplink');
			$('#gmapIframeSrc').attr('src',gmapLink);
	    });
    }

    // Default dropdown update
    if($('#city_dropdown option:selected').length)    
		registerCityAreaDropdown($( "#city_dropdown option:selected" ).val());

</script>

<section class="map">
			<iframe id="gmapIframeSrc" src="https://www.google.com/maps/embed/v1/place?q=Wicked+Ride+Bike+Rental,+Marenahalli+Road,+5th+Block,+Jayanagara+Jaya+Nagar,+Bengaluru,+Karnataka,+India&key=AIzaSyDWDQ9kPc2wjwc--c4RVm9XJHa9AbYzYcE" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<!-- END OF MAPS SECTION -->

<!-- START FORM SECTION -->

    <section>
        <div class="form_main">
            
            @if (count($errors->faqhome) > 0)
			<div class="alert alert-danger faq-home-err-msg">
				<ul>
					@foreach ($errors->faqhome->all() as $error)
						<li>* {{ $error }}</li>
					@endforeach
				</ul>
			</div>
            @endif
            @if (session('faqHomeSuccessMsg'))
                <div class="alert alert-success faq-home-success-msg">
                    {{ session('faqHomeSuccessMsg') }}
                </div>
            @endif
            <form class="form" method="post" action="/faq-home-enquiry/add">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        
                          <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                          </div>

                          <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
                          </div>
                        
                    </div>

                    <div class="col-md-6">
                       
                          <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                          </div>

                          <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Mobile Number">
                          </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                       
                          <div class="form-group">
                            <textarea type="email" rows="8" class="form-control" id="exampleInputEmail1" placeholder="Your enquiry"> </textarea>
                          </div>
                        
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit enquiry"></div>
                        
                                  
                        @if (!Auth::user()) 

                        <div class="register">
                           
                            <div class="border_top">
                                <div class="common_btn_big"><span></span><a href="/login">Register</a></div>
                                <div class="common_btn_big"><span></span><a href="/login">Login</a></div>
                            </div>
                        </div>
                        
                        @endif
                    </div>
                </div>
            </div>
         </form>
        </div>
    </section>

<!-- END FORM SECTION -->

<!-- SECTION RESERVATION SECTION -->

		<section>
			<div class="reservation_main">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="reserve_sec">
								<!-- <span>to make a reservation you can mail us at</span> -->
								<h5 class="rervation_big" style="text-transform:lowercase;"><a href="mailto:customer-support@bounceshare.com">customer-support@bounceshare.com</a></h5>
							</div>
						</div>
					
						<div class="col-md-6">
							<div class="reserve_sec1">
								<!-- <span>to make a reservation you can call us at</span> -->
								<h5 class="rervation_big"><a href="tel:080 4680 1054">080 4680 1054</a></h5>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</section>

<!-- END RESERVATION SECTION -->

	
@stop	
