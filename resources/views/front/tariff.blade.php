@inject('priceObj','App\Models\Bike')
@inject('bikeMakeObj','App\Models\BikeMake')
@inject('bikeModelObj','App\Models\BikeModel')
@inject('cityObj','App\Models\City')

@extends('front/frontMaster')
@section('title')
	<title>Tariff</title>
@stop

@section('content')
  <div class="clear_both"></div>
  <!-- booking main starts here-->
  <div class="book_bike_main">
    <div class="bike_booking_bg">
      <!-- width 960 starts here-->
      <div class="width_960">
        
      </div>

      <div id="steps">
        <div id="step2" style="display: block;">
        
          <div class="available_bikes">
            <!--  <div class="tariff_lines">
                  <h4>Check&nbsp;</h4><a href="/offers"><div style="text-decoration: underline;"><h4>offers</h4></div></a><h4>&nbsp;for discounts on Weekdays and Long rentals</h4>
              </div> -->

              <div class="booking_bike_white">
                <p style="color: black; margin-left:20px">* Prices may vary during the holidays</p>
                  <div class="width_960">
                <div id="show-bikes">
                @foreach($bikeModels as $bikeModel) 
                  
                  <ul class="book_bike_fleet happy_customers item" >
                    <li>
                      <h6 class="">{{$bikeModel['bike_make_name'] }} {{ $bikeModel['bike_model'] }}</h6>
                      <?php  $image = \App\Models\Image::find($bikeModel->thumbnail_img_id);
                        if (empty($image)) {?>
                          <div class="">
                            <img src="/images/metro-bikes-logo.png">
                          </div>
                      <?php } else { ?>
                        <div class="">
                          <img src="{{ $image['full'] }}">
                        </div>
                      <?php } ?>
                      <div class="clear_both"></div>

                    </li>
                      
                   
                    <li class="price-section">
                      <div class="tariff_box">
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per hour - Weekday</div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['hour_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per hour - Weekend </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['hour_weekend'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per day - Weekday </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['day_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per day - Weekend </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['day_weekend'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> For 5 Weekdays (Mon - Fri) </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['five_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per Week </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['week'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per Month </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['month'] }}</div>
                        </div>
                          
                        <div class="bike_price tariff_lines">
                            <div class="small_caps side-by-side"> Minimum billing: </div>
                          <div class="tariff_price total-price side-by-side">&#8377  {{ $bikeModel['minimum_price'] }}</div> 
                           <div class="small_caps side-by-side"> ({{ $bikeModel['minimum_hours'] }} hours)</div>
                        </div>
                        <div class="choose-bike-cta">
                          <div class="booking-step2-continue common_btn_small book-btn show">
                            <a href="/rent-a-<?php if($bikeModel->bike_make_name == "Harley-Davidson") echo "Harley Davidson"; else echo $bikeModel->bike_make_name; ?>-{{$bikeModel->bike_model}}">Rent It</a>
                          </div>  
                        </div>

                         
                      </div>
                    </li>
                    
                  </ul>
                
                @endforeach

                </div>
                </div>
            </div>  
            
          </div>
        </div>
      </div>
    <p style="color:black;">&nbsp;&nbsp;&nbsp;*The prices may vary from location to location. The prices shown are for the location with minimum price in a given city.</p>
    </div>
      
  </div>
  <!-- booking main ends here-->
  
  <!--CACHE-->
  <script src="/js/jquery.js"></script>
  <!--<script src="/js/jquery.datetimepicker.js"></script>-->



  <div class="clear_both"></div>
@stop
