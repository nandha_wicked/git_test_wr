@extends('front/frontMasterAppLinks')


@section('content')
<div class="clear_both"></div>

<!-- about main starts here-->
<div class="about_us_main">
  <div id="blurb" class="width_960">
	<div class="app-header"> 
    <img src="/images/plainlogo.png" alt="app-logo">
    
    
	<div class="clear_both"></div>
    <h5>Download Wicked Ride app and start renting your wicked ride and receive Rs 150 credit in wallet.</h5>
    <div class="app-header-web"> 
    
    <a href="/return-to-webstie">
            <h5>Or Continue to website</h5></a>
        
    </div>     
    </div>
      
	
    <div class="clear_both"></div>
  </div>
  
    <div class="app-img app-header">
        <img src="/images/appbanner.png" alt="app-banner">
    </div>
    
    <div class="side-by-side-app">
        <div class="app-img-store">
            <a href="https://play.google.com/store/apps/details?id=com.wickedride.app&referrer=utm_source%3DWeb%26utm_medium%3DMobile%26utm_campaign%3DInitial">
                <img border="0" alt="Wicked Ride App Android" src="/images/google-play-badge.png"></a>
        </div>
        <div class="app-img-store">
            <a href="https://itunes.apple.com/us/app/wicked-ride/id1121704128?ls=1&mt=8">
                <img border="0" alt="Wicked Ride App iOS" src="/images/apple_store2.png"></a>
        </div>
    </div>
   
  	<!-- <div class="white_patch"></div> -->
  	<!--reservation starts here-->
	
    
    
	<!--reservation ends here-->
	<div class="clear_both"></div>
</div>
<!-- about main ends here-->

<div class="clear_both"></div>


<!-- Google Analytics New Starts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68504157-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics New Ends -->


	
@stop	
