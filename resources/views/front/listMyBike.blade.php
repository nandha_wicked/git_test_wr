@extends('front/frontMaster')

@section('content')
<div class="clear_both"></div>
<!-- our fleet starts here-->
<div id="blurb" class="our_fleet_sec listing">
	<h2 class="item">rent from others</h2>
	<p class="item">Renting and listing concept will be explained here</p>
	<div class="item common_btn_big"><span></span><a  href="">click to view listed bikes</a></div>
</div>
<!-- our fleet ends here-->
<div class="clear_both"></div>
<!-- width_960 starts here-->
<div class="width_960">
	<!-- review starts here-->
	<div class="cutomers_review happy_customer_review">
		<h4>have more queries? get in touch with us.</h4>
		
		<!--booking date main starts here-->
		 	<div class="booking_date_main">
				<div class="celect_city">
					<form>
						<label>
						    <select>
						        <option selected> Brands</option>
						        <option>Karnataka</option>
						        <option>TamilNadu</option>
						        <option>maharashtra</option>
						    </select>
						</label>
						<label>
						    <select>
						        <option selected> Listers Name </option>
						        <option>Karnataka</option>
						        <option>TamilNadu</option>
						        <option>maharashtra</option>
						    </select>
						</label>
						<label>
						    <select>
						        <option selected> City </option>
						        <option>Karnataka</option>
						        <option>TamilNadu</option>
						        <option>maharashtra</option>
						    </select>
						</label>
					</form>
				</div>
			</div>
			<!--booking date main starts here-->

			<div class="rent_details">
				<ul>
					<li>
						<div class="thumbnail"><img src="images/listing-thumb.jpg"></div>
						<div class="article_copy">
							<div class="min_height">
								<h5>Harley-Davidson superlow</h5>
								<p>Daily charges <span class="price">xxxx</span></p>
								<p>Weekly charges <span class="price">xxxx</span></p>
								<p>Conditions</p>
							</div>
							<div class="common_btn_small"><span></span><a href="">contact this lister</a></div>
						</div>
							<div class="circle_rating">
								<div class="center_align">
									<span>3/5</span>
									<span class="rating_text">rating</span>
								</div>
							</div>
					</li>
					<li>
						<div class="thumbnail"><img src="images/listing-thumb.jpg"></div>
						<div class="article_copy">
							<div class="min_height">
								<h5>Harley-Davidson superlow</h5>
								<p>Daily charges <span class="price">xxxx</span></p>
								<p>Weekly charges <span class="price">xxxx</span></p>
								<p>Conditions</p>
							</div>
							<div class="common_btn_small"><span></span><a href="">contact this lister</a></div>
						</div>
							<div class="circle_rating">
								<div class="center_align">
									<span>3/5</span>
									<span class="rating_text">rating</span>
								</div>
							</div>
					</li>
					<div class="clear_both"></div>
				</ul>
			</div>
	</div>
	<!-- review ends here-->
</div>
<!-- width_960 ends here-->
<div class="clear_both"></div>

<!-- form starts here -->
<div class="form_main happy_customer_form">
	<div class="width_960">
		<form class="form">
			<h4>have more queries? get in touch with us.</h4>
			<ul>
				<li><input type="text" value="" placeholder="first name"></li>
				<li><input type="text" value="" placeholder="last name"></li>
				<li><input type="text" value="" placeholder="email id"></li>
				<li><input type="text" value="" placeholder="mobile number"></li>
				<li class="full_width"><textarea rows="6" cols="10" type="textfield" value="" placeholder="your enquiry"></textarea></li>
				<div class="clear_both"></div>
				<div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit enquiry"></div>
			</ul>
		</form>
	</div>
</div>
<!-- form ends here-->

<div class="clear_both"></div> 
	
@stop	
