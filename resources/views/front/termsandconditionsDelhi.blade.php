@extends('front/frontMaster')

@section('content')
<div style="color: #000; text-align: left; margin: 70px 10% 20px 10%; font-size: 23px">

<p>MOTORBIKES RENTAL RATES</p>
<p>(2015-16) Bullet 350 CC Electra 5S (with kick start &amp; drum/disk brake) Rental will be 700 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Royal Enfield Bullet Electra 350 CC Twinspark (with electric start &amp; disk brake) Rental will be 1000 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>RE 350 CC Thunderbird Twinspark (with electric start &amp; disk brake) Rental will be 1000 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>RE 350 CC Classic UCE (with electric start &amp; disk brake) Rental will be 1000 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>RE 350 CC Classic/Thunderbird/Electra UCE (2016) (with electric start &amp; disk brake) Rental will be 1450 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>RE 500 CC Machismo (with electric start &amp; disk brake) Rental will be 1250 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>RE 500 CC Classic (with electric start &amp; disk brake) Rental will be 1300 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>RE 500 CC Desert Strom (with electric start &amp; disk brake) Rental will be 1850 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>RE 500 CC Standard (with electric start &amp; disk brake) Rental will be 1850 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>RE 500 CC Thunderbird (with electric start &amp; disk brakes) Rental will be 1850 INR/day alongwith a refundable security deposit of 10000 INR/bike.</p>
<p>Bajaj Pulsar 220 CC Dtsi (with electric start &amp; disk brakes) Rental will be 800 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Hero Impulse (with electric start &amp; disk brake) Rental will be 650 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Bajaj Avenger 220 CC Dtsi (with electric start &amp; disk brakes) Rental will be 900 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>KTM Duke 200 CC (with electric start &amp; disk brakes) Rental will be 800 INR/day alongwith a refundable security deposit of 15000 INR/bike.</p>
<p>Honda Activa/Aviator (with electric start &amp; drum brakes). Rental will be 400 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Yamaha Fazer (with electric start &amp; disk brake) Rental will be 650 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Bajaj Pulsar 150 CC (with electric start &amp; disk brake) Rental will be 650 INR/day alongwith a refundable security deposit of 5000 INR/bike.</p>
<p>Mahindra Thar 4 wheel drive diesel Rental will be 3350 INR/day alongwith a refundable security deposit of 40000 INR/bike.</p>
<p>&nbsp;</p>
<p>Things you should know</p>
<p>1) Now you can pick and drop the bike anywhere in India providing you have to pay the transportation cost to and fro Delhi plus the idle day&rsquo;s rental spent during the transit.</p>
<p>2) We will provide you the spare parts along with the motorbike and that is only chargeable if you use them.</p>
<p>3) The bike will be completely serviced and inspected before handed over to you.</p>
<p>4) Terms &amp; Conditions with guidelines is attached for your reference.</p>
<p>5) Depending upon your itinerary, you can choose the Riding gears/accessories from the attached "SHB_Travel accessories". We have almost all the riding gears &amp; accessories available with us on hire for the short &amp; long term rental as well.</p>
<p>6) If traveling to Leh or Nepal, we'll be providing you an additional Legal Buy Back Agreement along with the bike's original paper to avoid any hassle while routine police checks. Cost for the same will be 600 INR/bike and that will be shared mutually. You just have to pay 300 INR/bike.</p>
<p>7) For the small groups or individual riders who go without the mechanic we will provide a list of mechanics en-route and 24x7 online mechanical support.</p>
<p>8) Documents required for an Indian National; - Address proof Xerox - Driving License Xerox - 2 Recent photos - 1 Refundable Post dated cheque</p>
<p>9) Documents required for an Foreign National; - Passport &amp; Visa Xerox - International Driving Permit Xerox - 2 Recent photos - Return flight ticket Xerox Let us know if you need any further details to proceed further.</p>
<p>10) What is Included in the Rental Rate Unlimited miles and no hourly charges Motorbike accessories including rear view mirrors, Leg guard, Spare parts &amp; Tool kit Original motorbike registration certificate, Third party insurance &amp; Pollution under control certificate</p>
<p>11) Customer Orientation Every client will receive orientation to the motorcycle. The client orientation will cover all aspects of safe motorcycle operation, the clients' maintenance responsibilities, local and state laws.</p>
<p>12) Storage Stoneheadbikes.com will provide storage for client luggage, airline tickets, and other valuables free of charge, however at clients' risk.</p>
<p>13) Fuel Fuel costs are client's responsibility. Stoneheadbikes.com claims no responsibility for motorcycle fuel consumption and mileage.</p>
<p>14) Helmets (Included in Daily Rental Rate) Helmets are provided with all motorcycle rentals and all tours. Clients are encouraged to bring their own helmets for comfort and style. In addition, helmets may be purchased or hired at Stoneheadbikes rental station. Helmets for Rider and as well as for pillion are required by law in most locations.</p>
<p>15) Additional Drivers 1 additional driver is permitted at no extra charge. The minimum age for an additional driver is 21 and a valid motorcycle license is required.</p>
<p>16) Maintenance Responsibility The customer is responsible for checking engine oil levels at each refueling and reporting mechanical failures immediately. Instructions and procedures for maintenance expense reimbursement are provided at pick-up. Customers may be held responsible for mechanical damage due to negligence in motorcycle operation or failure to provide normal maintenance.</p>
<p>17) Motorcycle Substitution Stoneheadbikes reserves the right to substitute the motorcycle confirmed to rental client. Should Stoneheadbikes not be able to provide confirmed motorcycle a similar or better model will be provided to the client. This constitutes the maximum liability of Stoneheadbikes.</p>
<p>18) Security Deposit A security deposit of 5000 INR - 60000 INR will be taken at pick-up. Security deposit must be in cash but can be deposited in USD or Euro's as well.</p>
<p>19) Restrictions that the hired vehicle shall not be used outside India and Nepal; not to use the vehicle for any race or competition; not to use the vehicle for any illegal purpose; not to operate the vehicle in a negligent manner; not to permit the vehicle to be operated by any other person without the written permission of Stoneheadbikes 20) Return Policy Bikes can be collected / returned to our mechanic at any time during working hours (10am to 7pm). However, it is important that you notify us beforehand so that we can prepare the mechanic.</p>
    
</div>

@stop	
