@extends('front/frontMasterHome')


@section('title')
	<title>Offers and Discounts | Wicked Ride</title>
@stop

@section('meta')
	<meta name="description" content="Offers and discounts on bikes such as Enfields, Harley-Davidson, Triumph, Ducati, Kawasaki, KTM, Avenger, in Karnataka - Bangalore, Mysore, Hampi, Belgaum, Rajasthan- Jaipur, Udaipur, Jaisalmer, Gujarat – Bhuj and Ahmedabad from Wicked Ride, India’s first premium motorcycle rental company and the most trusted motorcycle rental company. No security deposit and comes with Insurance coverage.">
@stop

@section('content')



    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Discounts and Offers
                </h1>
            </div>
        </div>
       
        
           
            <div class="row">
              
                <div class="col-md-12">
                    <h3>APP10</h3>

                    <p>Download Wicked Ride app on <a href="https://play.google.com/store/apps/details?id=com.wickedride.app">Google Play Store</a> and book your next ride with 10% off on all days</p>
                    
                </div>
                
                <div class="col-md-12">
                    <h3>WEEKDAY15</h3>

                    <p>Get 15% off on all bikes in Bangalore on weekdays</p>
                    
                </div>
                <div class="col-md-12">
                    <h3>LD10</h3>

                    <p>Get 10% off on all bikes in for rentals longer than 3 days</p>
                    
                </div>
                <div class="col-md-12">
                    <h3>LD20</h3>

                    <p>Get 20% off on all bikes in for rentals longer than 6 days</p>
                    
                </div>
                <div class="col-md-12">
                    <h3>Rentals longer than 15 days or Group rentals<sup>*</sup></h3>

                    <p>Contact us at 08046801054 or reservations@wickedride.com to bigger deals</p>
                    
                </div>
                
                <br/><br/><br/><br/>
                
                <div class="col-md-12">
                     <p>* - Terms &amp; Condtions apply, Offers valid only on selected days. Offer not valid from Nov 1st to Jan 10th</p>
                    <p> Offers valid only in Bangalore</p>
                    
                </div>
                
                
            </div>
         

       


       

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	
@stop	
