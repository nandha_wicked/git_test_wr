@extends('front/frontMasterHome')

@section('content')

<!-- Google Code for leads till checkout Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 932065471;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "QxpKCJHHjWQQv-G4vAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/932065471/?label=QxpKCJHHjWQQv-G4vAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- End of Google Code for leads till checkout Conversion Page -->

	<div class="clear_both"></div>

	<div style="color: #000; text-align: center; margin: 168px 10px; font-size: 23px">
		Please wait while we redirect you to the payment gateway.		
	</div>
	


	<form action="{{$payuInfo['payu_base_url']}}" method="post" name="payuForm" id="payuForm">
		<input type="hidden" name="key" value="{{$payuInfo['merchant_key']}}" />
		<input type="hidden" name="hash" value="{{$payuInfo['hash']}}" />
		<input type="hidden" name="txnid" value="{{$payuInfo['txnid']}}" />
		<input type="hidden" name="amount" value="{{$payuInfo['amount']}}" />
		<input type="hidden" name="firstname" value="{{$payuInfo['firstname']}}" />
		<input type="hidden" name="email" value="{{$payuInfo['email']}}" />
		<input type="hidden" name="phone" value="{{$payuInfo['phone']}}" /> <!-- Change this to original number -->
		<input type="hidden" name="productinfo" value="{{$payuInfo['productinfo']}}" />
		<input type="hidden" name="surl" value="{{$payuInfo['surl']}}" />
		<input type="hidden" name="furl" value="{{$payuInfo['furl']}}" />
		<input type="hidden" name="_token" value="{{ Session::token() }}" />		
		<!-- O type="hidden"ptional Fields -->
		<input type="hidden" name="curl" value="{{$payuInfo['curl']}}" />
		<input type="hidden" name="lastname" value="{{$payuInfo['lastname']}}" />
		<?php if(env('PAYU_MODE', 'TEST') == 'LIVE'):?>
		<input type="hidden" name="service_provider" value="payu_paisa" />   <!-- comment for testing -->
		<?php endif;?>
		<!-- <input type="hidden" name="address1" value="address one" />
		<input type="hidden" name="address2" value="address two" />
		<input type="hidden" name="city" value="Bangalore" />
		<input type="hidden" name="state" value="Karnataka" />
		<input type="hidden" name="country" value="India" />
		<input type="hidden" name="zipcode" value="{{$payuInfo['zipcode']}}" />
		
		<input type="hidden" name="udf2" value="" />
		<input type="hidden" name="udf3" value="" />
		<input type="hidden" name="udf4" value="" />
		<input type="hidden" name="udf5" value="" /> -->
		<input type="hidden" name="pg" value="{{$payuInfo['pg']}}" />
    </form>

    <script type="text/javascript">		
    	document.addEventListener('DOMContentLoaded', function() {    		
    		$('#payuForm').submit();
		});	
	</script>
	
@stop	
