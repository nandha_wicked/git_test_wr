@extends('front/frontMasterHome')


@section('title')
	<title>Biker's Plan | Wicked Ride</title>
@stop

@section('meta')
	<meta name="description" content="Rent bikes such as Enfields, Harley-Davidson, Triumph, Ducati, Kawasaki, KTM, Avenger, in Karnataka - Bangalore, Mysore, Hampi, Belgaum, Rajasthan- Jaipur, Udaipur, Jaisalmer, Gujarat – Bhuj and Ahmedabad from Wicked Ride, India’s first premium motorcycle rental company and the most trusted motorcycle rental company. No security deposit and comes with Insurance coverage.">
@stop

@section('content')



    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Biker Plans
                    <small>Take your Pick</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        @foreach($bikerPlans as $bikerPlan)
            <!-- Project One -->
            <div class="row">
                <div class="col-md-4">
                    <a href="/biker-plan/{{ $bikerPlan->name }}">
                        <img class="img-responsive" src="{{ $bikerPlan->image }}" alt="">
                    </a>
                </div>
                <div class="col-md-8">
                    <h3>{{ $bikerPlan->name }}</h3>

                    <p><?php echo($bikerPlan->description); ?></p>
                    <a class="btn btn-primary" href="/biker-plan/{{ $bikerPlan->name }}">View Plan</a>
                </div>
            </div>
            <!-- /.row -->

            <hr>
        @endforeach


       

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	
@stop	
