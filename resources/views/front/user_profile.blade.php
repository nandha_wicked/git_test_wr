<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Account - Wicked Ride</title>
    <!-- Bootstrap -->
    <link href="/user/css/bootstrap.min.css" rel="stylesheet">
    <link href="/user/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="{{asset('/js/jquery-1.10.2.min.js')}}"></script>
    <script src="{{asset('/js/ajax-crud.js')}}"></script>
    <script src="{{asset('/js/mail.js')}}"></script>
    <script src="{{asset('/js/image.js')}}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
    
    $(function() {
      $( "#dob" ).datepicker();
    });
    </script>
    </head>
  <body>

    <div class="container">
      <section class="profile" style="background-image: url(/user/images/bg_profile.jpg);" >
        <div class="row profileDetail text-center">
          <div id="targetLayer">
            <button class="btn btn-warning btn-xs btn-detail open-modal settings" data-toggle="modal" data-target="#myModal" value="{!! Auth::user()->id !!}">Edit</button>
            <form id="uploadForm" action="/user-upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data">
              <img src="{!! $user->image !!}" alt="" value="" style="max-width:200px;" class="profilePic">
              <input class="hide" id="user_avtar" type="file" multiple=""  name="user_avtar">
              
              <input name="_token" type="hidden" value="{!! csrf_token() !!}">
            </form>
          

          </div>
          <h1 class="userName">{!! $user->first_name !!}<Br/> {!! $user->last_name !!}</h1>
          <div class="userContact">
              <ul>@if(!empty($user->location))
                <li class="address">{!! $user->location !!}</li>
                @endif 
                @if (!empty($user->mobile_num))
                <li class="mobile">{!! $user->mobile_num !!}</li>
                @endif 
                <li class="email"><a href="{!! $user->email !!}" target="_blank">{!! $user->email !!}</a></li>
              </ul>
          </div>
        </div>
        <!--img src="images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
      </section>

        
     <section class="driverID clearfix">
        <div class="col-sm-12 col-md-4 col1">
          <h1>Driver's Identification</h1>
          <p >Please upload picture of your Driving Licence and an Address Proof (Passport/Adhaar Card).</p><p id="dl_id_status"> Status : {{ $user->dl_id_status }}</p>
            
        </div>
        <div class="col-sm-12 col-md-4 col2">
          <div id="targetLayerDL">
            <img id="imageDL" src="<?php
            if(!$user->dl_image)  
            echo "/img/sample_dl.jpg";
            else
            echo $user->dl_image;
                      
            ?>" class="appBg img-responsive center-block" alt="">
          </div>
            <div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin"></i></div>
            <div style=" text-align:center;">
                <a href="javascript:;" class="btn btn-default appBtn btn-uploadDL" role="button">Upload Image</a>
            </div>
            <form id="uploadFormDL" action="/user-upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data">
              <input class="hide" id="uploadDL" type="file" multiple=""  name="uploadDL">
              
              <input name="_token" type="hidden" value="{!! csrf_token() !!}">
            </form>
            
        </div>
         <div class="col-sm-12 col-md-4 col2">
        
          <div id="targetLayerID">
            <img id="imageID" src="<?php
            if(!$user->id_image)  
            echo "/img/sample_id.jpg";
            else
            echo $user->id_image;
                      
            ?>" class="appBg img-responsive center-block" alt="">
          </div>
            <div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin"></i></div>
            <div style=" text-align:center;">
                <a href="javascript:;" class="btn btn-default appBtn btn-uploadID" role="button">Upload Image</a>
            </div>
             
            <form id="uploadFormID" action="/user-upload" accept-charset="UTF-8" method="post" enctype="multipart/form-data">
              <input class="hide" id="uploadID" type="file" multiple=""  name="uploadID">
              
              <input name="_token" type="hidden" value="{!! csrf_token() !!}">
            </form>
            
        </div>
        
      </section>   
        
        
<!--

      <section class="bookRide">
        <h1>Ride Bookings</h1>
        
        <div class="row bookRideWrap">
          <div class="bookRideBoxes clearfix">
            <div class="col-sm-4 col1">
              <h2>12</h2>
              <h3>CURRENT</h3>
              <span>current</span>
            </div>
            <div class="col-sm-4 col2">
              <h2>36</h2>
              <h3>Completed</h3>
              <span>completed</span>
            </div>
            <div class="col-sm-4 col3">
              <h2>04</h2>
              <h3>Cancelled</h3>
              <span>cancelled</span>
            </div>
          </div>
          <div class="bookRideViewBox">
              <span class="curImg"></span>
              <a href="#" class="rideShow"></a>
          </div>
		    <div class="bookingList">
              <ul class="clearfix">
                <li class="bookingBox clearfix current odd firstOne">
                  <span class="bookingPoint"></span>
                  <div class="bookingBoxContent">
                    <a href="#" class="editbook">edit booking</a>
                    <div class="title clearfix">
                      <span class="thumbImg"><img src="/user/images/thumb-harley.png" alt="Harley Davidson IRON 338"></span>
                      <h3>Harley Davidson IRON 338<span>WR6HDIR338</span></h3>
                    </div>
                    <div class="bookingPeriod">
                        <span class="starts">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                        <span class="to">
                          To
                        </span>
                        <span class="ends">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                    </div>
                    <div class="address"><i>Map</i>Indhiranagar, Bengaluru, India</div>
                    
                    <div class="bookRideHover">
                        <ul>
                          <li class="edit"><a href="#">Edit</a></li>
                          <li class="cancel"><a href="#">Cancel</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                  </div>
                </li>
                
                <li class="bookingBox clearfix completed even secondOne">
                  <span class="bookingPoint"></span>
                  <div class="bookingBoxContent">
                    <a href="#" class="editbook">edit booking</a>
                    <div class="title clearfix">
                      <span class="thumbImg"><img src="/user/images/thumb-harley.png" alt="Harley Davidson IRON 338"></span>
                      <h3>Harley Davidson IRON 338<span>WR6HDIR338</span></h3>
                    </div>
                    <div class="bookingPeriod">
                        <span class="starts">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                        <span class="to">
                          To
                        </span>
                        <span class="ends">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                    </div>
                    <div class="address"><i>Map</i>Indhiranagar, Bengaluru, India</div>

                    <div class="bookRideHover">
                        <ul>
                          <li class="edit"><a href="#">Edit</a></li>
                          <li class="cancel"><a href="#">Cancel</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                  </div>
                </li>

                <li class="bookingBox clearfix cancelled odd">
                  <span class="bookingPoint"></span>
                  <div class="bookingBoxContent">
                    <a href="#" class="editbook">edit booking</a>
                    <div class="title clearfix">
                      <span class="thumbImg"><img src="/user/images/thumb-harley.png" alt="Harley Davidson IRON 338"></span>
                      <h3>Harley Davidson IRON 338<span>WR6HDIR338</span></h3>
                    </div>
                    <div class="bookingPeriod">
                        <span class="starts">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                        <span class="to">
                          To
                        </span>
                        <span class="ends">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                    </div>
                    <div class="address"><i>Map</i>Indhiranagar, Bengaluru, India</div>

                    <div class="bookRideHover">
                        <ul>
                          <li class="edit"><a href="#">Edit</a></li>
                          <li class="cancel"><a href="#">Cancel</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                  </div>
                </li>

                <li class="bookingBox clearfix current even">
                  <span class="bookingPoint"></span>
                  <div class="bookingBoxContent">
                    <a href="#" class="editbook">edit booking</a>
                    <div class="title clearfix">
                      <span class="thumbImg"><img src="/user/images/thumb-harley.png" alt="Harley Davidson IRON 338"></span>
                      <h3>Harley Davidson IRON 338<span>WR6HDIR338</span></h3>
                    </div>
                    <div class="bookingPeriod">
                        <span class="starts">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                        <span class="to">
                          To
                        </span>
                        <span class="ends">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                    </div>
                    <div class="address"><i>Map</i>Indhiranagar, Bengaluru, India</div>

                    <div class="bookRideHover">
                        <ul>
                          <li class="edit"><a href="#">Edit</a></li>
                          <li class="cancel"><a href="#">Cancel</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                  </div>
                </li>
                
                <li class="bookingBox clearfix completed odd">
                  <span class="bookingPoint"></span>
                  <div class="bookingBoxContent">
                    <a href="#" class="editbook">edit booking</a>
                    <div class="title clearfix">
                      <span class="thumbImg"><img src="/user/images/thumb-harley.png" alt="Harley Davidson IRON 338"></span>
                      <h3>Harley Davidson IRON 338<span>WR6HDIR338</span></h3>
                    </div>
                    <div class="bookingPeriod">
                        <span class="starts">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                        <span class="to">
                          To
                        </span>
                        <span class="ends">
                          <i>25 Dec 15</i>
                          <em>08:00 PM</em>
                        </span>
                    </div>
                    <div class="address"><i>Map</i>Indhiranagar, Bengaluru, India</div>

                    <div class="bookRideHover">
                        <ul>
                          <li class="edit"><a href="#">Edit</a></li>
                          <li class="cancel"><a href="#">Cancel</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                  </div>
                </li>

              </ul>

              <a href="#" class="btn btn-default loadMore">Load more Bookings</a>

          </div>
		  
		  
        </div>

      </section>
-->
      
<!--
      <section class="referFriend">
        
        <div class="row">
          <div class="col-xm-12 col-md-6 referImg text-center">
            <img src="/user/images/referImg.png" alt="Refer a friend">
            <p>Refer now to start earning your free trip!</p>
          </div>
          
          <div class="col-xm-12 col-md-6 referSec">
            <div class="col-md-11">
              <h1>Refer a Friend</h1>
              <p>Now you can earn WickedRide credits by inviting your friends. <br>
  If your friend becomes a customer, you’ll get 10% of their first ride as credits and your friend gets 10% off.</p>

              <form class="sendMail" method="POST" action="/sendmail" accept-charset="UTF-8" id="myform">
                <div class="form-group">
                    <div class="controls">
                      <label for="exampleInputEmail1">Send an E-mail to your friends</label>
                      <i class="emailIcon"></i>
                    <input id="email_send" class="form-control" placeholder="friends_name@example.com" name="email_send" type="text" value="">
                    </div>
                </div>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                <button class="send-btn btn btn-default subBtn" type="button">send email</button>
                <label id="lodder"></label>
              </form> 
              <div class="social">
                <p>Share via social media</p>
                <ul>
                  <li class="fb"><a href="#">Facebook</a></li>
                  <li class="twitter"><a href="#">Twitter</a></li>
                  <li class="tmlr"><a href="#">Tumblr</a></li>
                </ul>
              </div>
              <a href="#" class="readmore arrowLhs">Read our Terms & Conditions</a>
            </div>
          </div>  

        </div>

        <div class="referShare">
          <p>Or share your unique referral link in your Blog, E-mail or Instant Messenger <a href="#">< http://www.wickedride.com/signup/?ref=REF ></a></p>
        </div>
 </section>


    -->    



      <div class="modal fade userModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                  <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                      <input type="hidden" id="id" name="id" value="">
                      
                      <div class="row row1">
                          <div class="col-xs-12 col-sm-12 col-md-3 proPic">
                            <div id="targetLayer">
                              <img src="{!! $user->image !!}" alt="Profile Pic" value="">
                            </div>
                              <br/>
                            <div style=" text-align:center;">
                                <a href="javascript:;" class="btn btn-primary btn-upload">Upload Image</a>
                            </div>
                              <br/>
                          </div>

                          <div class="col-xs-11 col-sm-11 col-md-9 righCol">
                              <div class="form-group col-xs-6 error">
                                  <label for="first_name" class="control-label">First Name <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First Name" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="first_name" class="control-label">Last Name <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="last_name" name="last_name" placeholder="Last Name" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="gender" class="control-label">Gender <span class="starIcon">*</span></label>
                                    <select name="gender" id="gender" class="form-control">
                                      <option value="1">Male</option> 
                                      <option value="2">Female</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="dob" class="control-label">Date of Birth <span class="starIcon">*</span></label>
                                  <i class="calenderIcon"></i>
                                  <input type="text" class="form-control has-error" id="dob" name="dob" placeholder="Date of birth" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="mobile_num" class="control-label">Mobile Number <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="mobile_num" name="mobile_num" placeholder="Mobile Number" value="" required="">
                                </div>

                          </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-11 divCenter">
                            <div class="form-group col-xs-12 col-sm-12 col-md-8 error">
                              <label for="email_id" class="control-label">Email Address <span class="starIcon">*</span></label>
                              <input type="text" class="form-control has-error" id="email_id" name="email_id" placeholder="Email" value="" required="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="location" class="control-label">Location <span class="starIcon">*</span></label>
                              <select name="location" id="location" class="form-control">
                                <option selected value="Bangalore">Bangalore</option> 
                                <option value="Jaipur">Jaipur</option>
								<option value="Udaipur">Udaipur</option>
								<option value="Mysuru">Mysuru</option>
								<option value="Bhuj">Bhuj</option>
								<option value="Ahmedabad">Ahmedabad</option>
								<option value="Hospet">Hospet (Hampi)</option>
								<option value="Belagavi">Belagavi</option>
								<option value="Jaisalmer">Jaisalmer</option>
								<option value="Delhi">Delhi</option>
                              </select>
                            </div>
                            
                            <div class="hide form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="old_password" class="control-label">Current Password <span class="starIcon">*</span></label>
                              <i class="openEye"></i>
                              <input type="text" class="form-control has-error" id="old_password" name="old_password" placeholder="Old Password" value="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="password" class="control-label">New Password <span class="starIcon">*</span></label>
                              <i class="closedEye" id="showPassword"  onclick="showPassword()"></i>
                              <input type="password" class="form-control has-error" id="password" name="password" placeholder="New Password" value="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="inputTask" class="control-label">Confirm Password <span class="starIcon">*</span></label>
                              <i class="closedEye" id="showCPassword"  onclick="showCPassword()"></i>
                              <input type="password" class="form-control has-error" id="cpassword" name="cpassword" placeholder="Confirm Password" value="">
                            </div>

                            <div class="col-xs12 text-center">
                                <meta name="_token" content="{!! csrf_token() !!}" />
                              <button type="button" class="btn btn-primary btnUpdate" id="btn-save" value="add" data-dismiss="modal">Update</button>
                            </div>

                        </div>
                          
                      </div>
                  </form>

              </div>
          </div>
      </div>
    </div>





<div class="modal fade popBox2" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <h1>Invitation Sent!</h1>
                  <p>Please upload picture of your Driving Licence or an Address Proof to book a WickedRide’s Motorcycle.</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div>


            
                          
                          

    






    </div>
<script>
function showPassword(){
  var showPasswordCheckBox = document.getElementById("showPassword");
  
  if($("#showPassword").attr('class') == 'closedEye')
  {
	$("#showPassword").attr('class','openEye');
	document.getElementById("password").type="TEXT";
  }
  else{
	  $("#showPassword").attr('class','closedEye');
	  document.getElementById("password").type="PASSWORD";
  }
  
}
function showCPassword(){
  var showPasswordCheckBox = document.getElementById("showCPassword");
  
  if($("#showCPassword").attr('class') == 'closedEye')
  {
	$("#showCPassword").attr('class','openEye');
	document.getElementById("cpassword").type="TEXT";
  }
  else{
	  $("#showCPassword").attr('class','closedEye');
	  document.getElementById("cpassword").type="PASSWORD";
  }
  
}
</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/user/js/bootstrap.min.js"></script>
  </body>
</html>
