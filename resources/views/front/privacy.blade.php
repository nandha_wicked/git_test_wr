@extends('front/frontMaster')

@section('content')
<div style="color: #000; text-align: left; margin: 70px 10% 20px 10%; font-size: 23px">
<p>Privacy Policy</p>

<p>https://www.wickedride.com is a website owned and operated by WICKEDRIDE ADVENTURE SERVICES PVT. LTD. a Company duly registered and incorporated under the Companies Act, 1956, whose address is at no. 1705, ii floor, East End A main road, Jayanagar ninth block, Bangalore 560069, Karnataka, India. </p>

<p>WICKEDRIDE ADVENTURE SERVICES PVT. LTD, is strongly committed in protecting your privacy and has taken all necessary and reasonable measures to protect your information and handle the same in a safe and responsible manner in accordance with the terms and conditions of privacy policy set out herein below:</p>

<p>https://www.wickedride.com (the website) is a place where you can take on rent motor bikes.For you to rent the bike you need to signup and login by providing full name, phone number, email, password, emergency contact number, relationship with emegency contact, blood type, allergies( list of information). </p>

<p>In addition to these information, your use patterns, IP addresses through which you accessed this web site are captured when you use this website. For you to avail the services you have to provide your name, address, employment details, age, gender, identify proofs, driving license details, and other personal information and personally identifiable information.</p>

<p>The information contained in this website and the information collected by using/ login and or accessing this website are stored at a secured server managed and owned by Amazon Web Services. It is stated by the server service provider that they have all the best security practices required for the server. The security and privacy practices of the server operator can be known from http://aws.amazon.com/privacy/. https://www.wickedride.com is not giving any warranties regarding the policies or practises of the server operator. Certain of such collected information are stored in Wickedride computers for processing your vehicle hire request, accounting and other operational purposes.</p>

<p>The website is owned by an Indian company and is located in India. Hence, we are duty bound to abide by the Indian privacy laws. This privacy policy is applicable to all users of this website. The user is herein collectively refereed as &lsquo;You&rsquo;. We may not have complied to some privacy laws of other countries. We do not know such legal requirements as well. Hence, if you are a foreign national and requires us to have different privacy practices in relation to your personal data, you are required to notify us before providing your personal data.</p>

<p>Your use of the website services constitute your consent to the terms of this Privacy Policy. </p>

<p>Collection of Information</p>

<p>For log in to the site and by hiring the vehicles, you may have to login by giving following details a)email address b)phone number c)full name d)password . For transacting business through this website you are required to provide the following information a)email address b) phone number, c)full name , d)driving license, e)address ID . While using the vehicle on rent, our system collect the following data: driving license, address ID, work email address, GPS location of the bike, passport, and images of the customer. Some of these information are personally identifiable information and is collected for the purpose of verification of identity of the user and ensure due compliances.</p>

<p>Additionally if you browse through this site, our servers may automatically record information of certain kind. Such information includes information such as the name of the domain and host from which you access the Internet; the Internet Protocol (IP) address of the computer you are using; the date and time your access. We use this information to monitor the usage of our websites and also whatever necessary for our business. </p>

<p>Information collected from third party: We may collect your personal information as well as other information from third parties like business partners; contractors etc and add the same to our account information. </p>

<p>Information placed on your computer: We may store some information such as cookies on your computer. Cookies are pieces of information that an application transfers to the hard drive of a visitor's computer for record-keeping purposes. This information facilitates your use of https://www.wickedride.com and ensures that you do not need to re-enter your details every time you visit it. You can erase or choose to block this information from your computer if you want to. Erasing or blocking such information may limit the range of features available to the visitor on https://www.wickedride.com. We also use such information to provide visitors a personalized experience on https://www.wickedride.com. We may use such information to allow visitors to use https://www.wickedride.com without logging on upon returning. Information to verify that visitors meet the criteria required to process their requests. </p>

<p>Links<br />
Our website contains links to other sites. Such other sites may use information about your visit to this website. Our Privacy Policy does not apply to practices of such sites that we do not own or control or to people we do not employ. Therefore, we are not responsible for the privacy practices or the accuracy or the integrity of the content included on such other sites. We encourage you to read the individual privacy statements of such websites.</p>

<p>Use and Sharing of Information<br />
At no time will we sell your personally-identifiable data without your permission unless set forth in this Privacy Policy. The information we receive about you or from you may be used by us or shared by us with our corporate affiliates, dealers, agents, vendors and other third parties. This we do to help you to get information in the areas were you are interested. This companies may use this information to do target advertisement.</p>

<p>We do not share, sell, trade or rent your personal information to third parties for unknown reasons. We retain complete anonymity while in all analytics and none of the personally sensitive information is used, except in a limited set of circumstances (e.g. if we are asked by a Court of law to produce the same or by a government agency demands that data). We safeguard your email addresses. We don't sell the email addresses provided by you and we use them only as directed by you and in accordance with this policy. </p>

<p>https://www.wickedride.com collects and uses your information to provide you better customer experience and also in the administration of our business. https://www.wickedride.com may use your personal information to administer this service, provide better customer service; personalize the https://www.wickedride.com services for you; enable your access to and use of https://www.wickedride.com services; publish information about you on https://www.wickedride.com; to keep you informed or update you on various products and services available from https://www.wickedride.com and its partner entities; send you statements and invoices; collect payments from you; send you marketing communications; to send you mails or contact you for various customer satisfaction surveys, market research, promotional activities or in connection with certain transactions; make communications necessary to notify you regarding various security, privacy, and administrative issues; to respond to your queries; send you information that may be of interest to you, share this information with products and services made available through https://www.wickedride.com; to respond to any of your legitimate claim, or to address you our reasonable belief, that you are violating the rights of any third party or any of the agreements or policies that govern your use of https://www.wickedride.com product or service; conduct research and perform analysis in order to measure, maintain, protect, develop and improve our services.</p>

<p>https://www.wickedride.com has no operations in other countries and we do not transfer Information to other countries. However, if the server space provider as matter of backup or other general practices may transfer the server content between any of the countries in which server provider operates or servers are located to enable the use of the information in accordance with this privacy policy. You agree to such cross-border transfers of personal information.</p>

<p>Where https://www.wickedride.com discloses your personal information to any third party for any purpose mentioned above, such third parties be obligated to use that personal information in accordance with the terms of this privacy statement. </p>

<p>In addition to the disclosures reasonably necessary for the purposes identified above, https://www.wickedride.com may disclose your personal information to the extent that it is required to do so by law, in connection with any legal proceedings or prospective legal proceedings, and in order to establish, exercise or defend its legal rights.</p>

<p>Information that we shares<br />
We are committed in protecting the privacy and security of your information. We shall not sell or lend your personal information to third parties other than for the purposes mentioned above.. <br />
We also share your personal information as described below:<br />
&#9679; Business Transfers: If we start up subsidiaries or involve in mergers or acquisitions. In such case your personal information may be the matter of transfer. And we will provide notice on any such transfer and become subject to different privacy policy. <br />
&#9679; Requirement under law: We release your personal information to third parties under following circumstances:</p>

<p>&#9679; to comply with the law, legal process or an enforceable governmental request; <br />
&#9679; to enforce or apply our terms of use agreement or other agreements with you;<br />
&#9679; to protect the rights or safety of https://www.wickedride.com or our users or others. However, transfer under this case does not include selling, renting, sharing, or otherwise disclosing your information for commercial purposes in violation of the commitments set forth in this Privacy policy. </p>

<p>Our commitment to data security<br />
We recognize that your privacy is important to you, and therefore we endeavor to keep your personal information confidential. We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information. Our server provider has assured complete security despite so there are possibilities of data breach. At the same time assuring our best effort to protect data, we do not represent, warrant, or guarantee that your personal information will be protected against unauthorized access, loss, misuse, or alterations, and do not accept any liability for the security of the personal information submitted to us nor for your or third parties' use or misuse of personal information.</p>

<p>Information relating to electronic transactions entered into via https://www.wickedride.com will be protected by encryption technology. We have partnered with a secure payment gate way PayuMoney, Paytm, Payzapp, RazorPay and Freecharge . https://www.wickedride.com does not have the ability to interfere and do not interfere with the payment gateway mechanism. https://www.wickedride.com has no access to the data that you may enter for making the payment through the payment gateway. By creating a link to a payment gateway, we do not endorse it, nor we liable for any failure of products or services offered by such payment gateway. Such payment gateway may have a privacy policy different from that of ours. All failures/ errors/ omissions of the payment gateway shall be solely on you. You hereby consent that you shall not sue https://www.wickedride.com for any disputes that you may have the payment gateway for any wrong doing of the payment gateway. </p>

<p>Security<br />
We safeguard your privacy using known security standards and procedures and comply with applicable privacy laws. Our websites combine industry-approved physical, electronic and procedural safeguards to ensure that your information is well protected through it's life cycle in our infrastructure. Sensitive data is hashed or encrypted when it is stored in our infrastructure. Sensitive data is decrypted, processed and immediately re-encrypted or discarded when no longer necessary. </p>

<p>Opt-Out Policy<br />
Please email if you no longer wish to receive any information from us.</p>

<p>Changes to the privacy policy <br />
We reserve the right to modify this Policy. If we decide to change our Privacy Policy, such changes will be posted on https://www.wickedride.com so that you are always aware of the latest amendments in this Privacy Policy. </p>

<p>About this privacy policy<br />
If you choose to visit our site, your visit and any dispute over privacy is subject to this privacy policy and Terms of Use. The terms of this privacy policy do not govern personal information furnished through any means other than https://www.wickedride.com</p>

<p>If you have any questions about this privacy policy or https://www.wickedride.com treatment of your personal information, please write to:</p>

<p>&#9679; by email to [Vivekananda@wickedride.com, Anil.g@wickedride.com, varunagni@wickedride.com ]; or <br />
&#9679; by post to 1705, ii floor, East End A main road, Jayanagar ninth block, Bangalore 560069, Karnataka, India.</p>

<p>Grievances<br />
https://www.wickedride.com has appointed its senior executive Mr. Anil, Director as the grievance officer to redress complaints relating to privacy. If you have any grievances against https://www.wickedride.com privacy practices, or you apprehend that your privacy is compromised at https://www.wickedride.com, then please address your complaint/ concerns to Mr.Anil. We assure, we shall pro-actively address your concerns. </p>

<p>All disputes between you and us in this regard are subject to exclusive jurisdiction of Bangalore courts.</p>
    
</div>

@stop	
