


<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
<meta name="_token" content="{{ Session::token() }}">

<!-- Facebook OG Code -->
<meta property="fb:app_id" content="700892750012103" />
<meta property="og:url" content="https://www.wickedride.com" />
<meta property="og:title" content="Premium Bike Rental - Wicked Ride"/>
<meta property="og:image" content="https://wickedride.com/images/fbthumbnail.png"/>
<meta property="og:site_name" content="WickedRide.com"/>
<meta property="og:description" content="Rent Harley-Davidson, Kawasaki, Triumph, Ducati, Royal Enfield, and many more in Bangalore, Jaipur, Bhuj, Mysore, Ahmedabad, and Udaipur"/>
<!-- End Facebook OG Code -->

<link href='/css/style.css?v=7' rel='stylesheet' type='text/css'>
<link href='/css/rj.css' rel='stylesheet' type='text/css'>
<link href='/css/media.css' rel='stylesheet' type='text/css'>
<link href='/css/animate.css' rel='stylesheet' type='text/css'>
<link href="/css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="/css/multiple-select.css" rel="stylesheet" type="text/css">
{{--<link href="https://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css">--}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">




  

	<!-- tabs-->
	<link rel="stylesheet" href="/css/style-tab.css"> <!-- Resource style -->
	<script src="/js/modernizr.js"></script> <!-- Modernizr -->
	<!-- tabs-->


<!-- date selection-->
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
<!-- date selectio-->

<!-- <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '199527113897528');
fbq('track', "PageView");</script>

<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=199527113897528&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
