@inject('frontEndText','App\Models\FrontEndText')

@if($result == "success")
<h3>Congrats!! Booking Successful&nbsp;</h3>
<p><span class="il">Documents</span> Required :&nbsp;</p>
<p><?php echo $frontEndText->getText('documents_required'); ?></p>
<div>Additional Info :</div>
<div>i. We provide one complimentary helmet</div>
<div>ii. The tariff is Rs 5 per KM and Rs 0.50 per minute.</div>
@else
    @if($id=="notfound")
        <h5>Sorry!! The requested one way trip does not exist. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</h5>
    @elseif($id=="notavailable")
        <h5>Sorry!! The requested one way trip was booked by somebody before we could confirm your booking. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</h5>
    @elseif($id=="pricemismatch")
        <h5>Sorry!! There was a price mismatch and your booking could not be confirmed. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</h5>
    @endif

@endif
