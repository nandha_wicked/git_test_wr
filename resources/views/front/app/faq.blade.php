@extends('front/frontMasterAppLinks')



@section('content')
<div class="clear_both"></div>
<!-- our fleet starts here-->
<div class="our_fleet_sec">
	<h2>how does it work?</h2>

	<div class="width_960">
		<div class="accordian_main">
			<ul>
                @foreach($faqs as $faq)
				<li>
					<div class="accordian_title"><h6>{!! strip_tags($faq->question) !!}</h6><span></span></div>
					<div class="open_para">
						<p>
							{!! $faq->answer !!}
						</p>
					</div>
				</li>
                @endforeach
				
			</ul>
		</div>	
	</div>

<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollmagic.min.js"></script>
<script type="text/javascript" src="/js/t.js"></script>
<!-- <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="js/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js"></script> -->
<script type="text/javascript" src="/js/app.js?a=1"></script>
<script type="text/javascript" src="/js/front.js?a=1"></script>
	
@stop	
