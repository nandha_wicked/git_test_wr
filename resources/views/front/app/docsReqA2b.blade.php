<p><span class="il">Documents</span> Required :&nbsp;</p>
<div>Valid Driving License</div>
<div>Valid Visa and IDP for International customers.</div>
<div>&nbsp;</div>
<div>Any of 2 :</div>
<div>i. Test email to&nbsp;<a href="mailto:reservations@wickedride.com" target="_blank">reservations@wickedride.com</a>&nbsp;<wbr />from your work email ID<br />ii. Passport. Original to be brought, copy will be retained for record<br />iii. Swipe your credit card on our EDC machine for Rs.1.</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>Additional Info :</div>
<div>i. We provide one complimentary helmet</div>
<div>ii. The tariff is Rs 5 per KM and Rs 0.50 per minute.</div>
<div>iii. If pick up and drop locations are same, a flat fee of 100 will be charged and upon return the charges will be calculated based on usage and remaining balance will be deposited into wallet or balance amount will be collected if it exceed initially collected amount.</div>
