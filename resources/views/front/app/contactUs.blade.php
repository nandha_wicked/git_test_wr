@extends('front/frontMasterAppLinks')

@section('content')
<div class="clear_both"></div>

<!-- contact main starts here-->
<div class="contact_main">
	<!--reservation starts here-->
	<div class="reservation_main">
		<h1 class="item h1-clear">Contact - Wicked Ride</h1>
		<div class="width_960">
			<div class="reserve_sec">
				<span>You can mail us at</span>
				<!-- <h5 class="rervation_big"><a href="mailto:reservations@wickedride.in">reservations@wickedride.in</a></h5> -->
				<h5 class="rervation_big" style="text-transform:lowercase;"><a href="mailto:customer-support@bounceshare.com">customer-support@bounceshare.com</a></h5>
			</div>
			<div class="reserve_sec">
				<span>You can call us at</span>
				<h5 class="rervation_big">080 4680 1054</h5>
			</div>
		</div>
	</div>

	<!-- maps starts here-->
<div class="map_main">
	<div class="map_bg">
		<div class="width_960">
			<h4>find us near you</h4>
			<div class="celect_city_main">
				<form>
					<div class="celect_city">
			 			<label id="city_dropdown_container">						   
							<option>Select</option>
						</label>
			 		</div>
			 		<div class="celect_city">
			 			<label id="area_dropdown_container">
			 				<select>
			 					<option>Please select city</option>
			 				</select>						    
						</label>
			 		</div>
			 	</form>		 	
	 		</div>
 		</div>
	</div>
	
	<div id="googleMap" class="map">
	</div>
</div>
<!-- maps ends here-->


	<!--reservation ends here-->
	<div class="clear_both"></div>
  	<!-- form starts here -->
	<div class="form_main">
		<div class="width_960">
			@if (count($errors->contact) > 0)
				<div class="alert alert-danger contact-err-msg">
					<ul>
						@foreach ($errors->contact->all() as $error)
							<li>* {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@if (session('contactSuccessMsg'))
    			<div class="alert alert-success contact-success-msg">
        			{{ session('contactSuccessMsg') }}
    			</div>
			@endif
			<form class="form" method="post" action="/contact-enquiry/add" name="contact-form">
				
				<ul>
					<li><input type="text" name="firstName" value="" placeholder="first name"></li>
					<li><input type="text" name="lastName" value="" placeholder="last name"></li>
					<li><input type="text" name="email" value="" placeholder="email id"></li>
					<li><input type="text" name="mobileNumber" value="" placeholder="mobile number"></li>
					<!-- <li class="full_width"><textarea rows="2" cols="10" type="textfield" name="address" value="" placeholder="address"></textarea></li> -->					
					<li class="full_width"><textarea rows="6" cols="10" type="textfield" name="comment" value="" placeholder="comments if any"></textarea></li>
					<div class="clear_both"></div>
					<!-- <div class="text_align_center"><input class="check" type="checkbox" name="vehicle" value="Bike"><label>sign me up for the monthly news letter</label></div> -->
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="common_btn_big"><span></span><input class="submit" type="submit" value="Submit"></div>
					<!--<div class="register">
						<div class="border_top">
							<div class="common_btn_big"><span></span><a href="">Register</a></div>
							<div class="common_btn_big"><span></span><a href="">Login</a></div>
						</div>
					</div>-->
				</ul>
			</form>
             <div>
    
                 <p><br/><br/><br/><br/><br/><br/></p>
        
            </div>
		</div>
	</div>
    
	<!-- form ends here-->

</div>
<!-- contact main ends here-->


<div class="clear_both"></div>

<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollmagic.min.js"></script>
<script type="text/javascript" src="/js/t.js"></script>
<!-- <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="js/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js"></script> -->
<script type="text/javascript" src="/js/app.js?a=1"></script>
<script type="text/javascript" src="/js/front.js?a=1"></script>
	
	
@stop	
