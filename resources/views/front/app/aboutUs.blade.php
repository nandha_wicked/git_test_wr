@extends('front/frontMasterAppLinks')


@section('content')
<div class="clear_both"></div>

<!-- about main starts here-->
<div class="about_us_main">
  <div id="blurb" class="width_960">
	<h1 class="item h1-clear">About - Wicked Ride</h1>
	<div class="clear_both"></div>
	<p class="item about_para">
		A ride can’t get more wicked than now. Yes, it is indeed time to start getting ideas if you’ve imagined yourself riding luxury bikes on the vast 
		plains and snaking through the curvy mountain paths. Love the biker gangs and their rides on rough terrains; their adventure into the cities, 
		countrysides and no-man’s-lands? Want to chronicle your own stories with wind in your hair, the engine roar in the air, and a song to hum, and 
		to become Wordsworth on wheels while riding? And what if you can do it all without even having to worry about the hassles of owning a premium bike 
		and maintaining it? With Wicked Ride, it is now possible for anyone with a valid driving license to dream big.
	</p>
	<p class="item about_para">
		A Bangalore-based luxury bike rental company, Wicked Ride was founded in 2014 by three friends who did not find options to rent motorcycles in 
		Bangalore at the time. Realising that there is a rather new audience to cater to, they set up a company and started renting bikes starting from
		Royal Enfield to Harley-Davidson. They believe in making your dream come true as a biker – be it riding the premium bikes or experiencing the 
		inexplicable joy of riding in the best biking destinations through biking expeditions called “Bikations®“. The first of its kind, Wicked Ride 
		also conducts free biking lessons for women and promote biking culture. Let your inhibitions waft away and rent a luxury bike in any season of 
		the year and ride on. Bon Voyage!
	</p>
    <div class="clear_both"></div>
  </div>
  	<!-- <div class="white_patch"></div> -->
  	<!--reservation starts here-->
	<div class="reservation_main">
		<div class="width_960">
			<div class="reserve_sec">
				<span>You can mail us at</span>
				<h5 class="rervation_big"><a href="mailto:customer-support@bounceshare.com">customer-support@bounceshare.com</a></h5>
			</div>
			<div class="reserve_sec">
				<span>You can call us at</span>
				<h5 class="rervation_big"><a href="tel:080 4680 1054">080 4680 1054</a></h5>
			</div>
		</div>
	</div>
	<!--reservation ends here-->
	<div class="clear_both"></div>
</div>
<!-- about main ends here-->

<div class="clear_both"></div>


	
@stop	
