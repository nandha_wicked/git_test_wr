@inject('priceObj','App\Models\Bike')
@inject('bikeMakeObj','App\Models\BikeMake')
@inject('bikeModelObj','App\Models\BikeModel')
@inject('cityObj','App\Models\City')

@extends('front/frontMasterAppLinks')
@section('title')
	<title>Tariff</title>
@stop

@section('content')

<div id="nav" class="navigation_main">

     <div class="second_nav">
 		<div class="container pad-left-right">
 			
 			
 			<div class="city_number">
	 			<div class="celect_city">
	 				<form method="get" action="/app/tariff">
			 			<label>
						    <select name="city_id" onchange="this.form.submit()">
						    	
						        <?php $cities = $cityObj->getActiveCities(); ?>
	 			 					@foreach($cities as $city)
	 			 						<option value="{{ $city->id }}" 
	 			 							<?php 
	 			 								
	 			 									if($city->id == $cityId) echo "selected"; 
	 			 		
	 			 							?> 
	 			 						>
	 			 							{{ $city->city }}
	 			 						</option>
	 			 					@endforeach		
	 		                </select>
						</label>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		 			</form>
		 		</div>
	 			
 			</div>
 		</div>
 	</div>
</div>

  <div class="clear_both"></div>
  <!-- booking main starts here-->
  <div class="book_bike_main">
    <div class="bike_booking_bg">
      <!-- width 960 starts here-->
      <div class="width_960">
        
      </div>

      <div id="steps">
        <div id="step2" style="display: block;">
        
          <div class="available_bikes">
            
              <div class="booking_bike_white">
                <p style="color: black; margin-left:20px">* Prices may vary during the holidays</p>
                  <div class="width_960">
                <div id="show-bikes">
                @foreach($bikeModels as $bikeModel) 
                  
                  <ul class="book_bike_fleet happy_customers item" >
                    <li>
                      <h6 class="">{{$bikeModel['bike_make_name'] }} {{ $bikeModel['bike_model'] }}</h6>
                      <?php  $image = \App\Models\Image::find($bikeModel->thumbnail_img_id);
                        if (empty($image)) {?>
                          <div class="">
                            <img src="/images/metro-bikes-logo.png">
                          </div>
                      <?php } else { ?>
                        <div class="">
                          <img src="{{ $image['full'] }}">
                        </div>
                      <?php } ?>
                      <div class="clear_both"></div>

                    </li>
                      
                   
                    <li class="price-section">
                      <div class="tariff_box">
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per hour - Weekday</div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['hour_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per hour - Weekend </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['hour_weekend'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per day - Weekday </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['day_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per day - Weekend </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['day_weekend'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> For 5 Weekdays (Mon - Fri) </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['five_weekday'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per Week </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['week'] }}</div>
                        </div>
                        <div class="bike_price tariff_lines">
                          <div class="small_caps side-by-side"> Price per Month </div>
                          <div class="tariff_price total-price">&#8377  {{ $bikeModel['month'] }}</div>
                        </div>
                          
                        <div class="bike_price tariff_lines">
                            <div class="small_caps side-by-side"> Minimum billing: </div>
                          <div class="tariff_price total-price side-by-side">&#8377  {{ $bikeModel['minimum_price'] }}</div> 
                           <div class="small_caps side-by-side"> ({{ $bikeModel['minimum_hours'] }} hours)</div>
                        </div> 
                      </div>
                    </li>
                    
                  </ul>
                
                @endforeach

                </div>
                </div>
            </div>  
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
  <!-- booking main ends here-->
  
  <!--CACHE-->
  <script src="/js/jquery.js"></script>



  <div class="clear_both"></div>
@stop
