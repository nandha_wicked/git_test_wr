@extends('front/frontMasterAppLinks')
@inject('reviewsObj','App\Models\Reviews')

@section('content')
<div class="clear_both"></div>


<!--reservation ends here-->
<div class="clear_both"></div>
<!-- width_960 starts here-->
<div class="width_960">
	<!-- review starts here-->
	<div class="cutomers_review happy_customer_review">
		<h4>what our customers had to say</h4>
		<ul id="review" class="testimon_sec">
            <?php $count = 0; ?>
            @foreach($reviewsObj->getAllActiveReviews() as $reviews)
            
            <li>
                <?php $image = App\Models\Image::find($reviews->image_id); ?>
				<div class="profile"><img src="{{ $image['full'] }}"></div>
				<div class="testimonial_copy">
					
					<div class="clear_both"></div>
					<p>
						{{ $reviews->review }}
					</p>
					<span class="profile_name">{{ $reviews->name }}</span>
					<div class="clear_both"></div>
					<div class="dated"><span>Reviewed On : {{ $reviews->reviewed_on }}</span></div>
					<!-- <div class="common_btn_small"><span></span><a href="">share this</a></div> -->
				</div>
			</li> 
			@endforeach
			<div class="clear_both"></div>
		</ul>
		<!-- <div class="load_more"><div class="common_btn_big"><span></span><a href="">load more</a></div></div> -->
	</div>
	<!-- review ends here-->

	<!-- media says starts-->
	<div class="media_says">
		<h4>what the media had to say</h4>
		<div class="clear_both"></div>
		<ul class="articles_sec">
			<li>
				<div class="profile"><img src="https://www.wickedride.com/images/bm.png"></div>
				<div class="article_copy">
					<h5>Now, a Harley-Davidson on rent</h5>
					<p>Featured on Aug 26, 2014</p>
					<div class="common_btn_small"><span></span><a href="http://www.bangaloremirror.com/columns/work/Now-a-Harley-Davidson-on-rent/articleshow/40936863.cms" target="_blank">read this article</a></div>
				</div>
			</li>
			<li>
				<div class="profile"><img src="https://www.wickedride.com/images/dh.png"></div>
				<div class="article_copy">
					<h5>Riding the dream machines</h5>
					<p>Featured on Sep 01, 2014</p>
					<div class="common_btn_small"><span></span><a href="http://www.deccanherald.com/content/428653/riding-dream-machines.html" target="_blank">read this article</a></div>
				</div>
			</li>
		</ul>
		
	</div>
	<!-- media says ends here-->
</div>
<!-- width_960 ends here-->

<div class="clear_both"></div> 
@stop	
