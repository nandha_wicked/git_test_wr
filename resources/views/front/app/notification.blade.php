@extends('front/frontMasterAppLinksNoStyle')


@section('content')

<!-- about main starts here-->
<div id="blurb" class="width_960" style="text-align:center;">
    <h5 class="item h1-clear">{{$appNotification->title}}</h5>
    <img class="notification_img" src="{{ $appNotification->image_url }}">
    <div class="clear_both"></div>
    <p class="item about_para" style="color:black;">
        {!! $appNotification->description !!}
    </p>
    

    @if($appNotification->button_text != "")
        <a href="{{ $appNotification->button_link }}" class="myButton">{{$appNotification->button_text}}</a>
        <p class="item about_para"></p>
    @endif
</div>


	
@stop	
