@extends('front/frontMasterHome')


@section('title')
	<title>Biker's Plan | Wicked Ride</title>
@stop

@section('meta')
	<meta name="description" content="Rent bikes such as Enfields, Harley-Davidson, Triumph, Ducati, Kawasaki, KTM, Avenger, in Karnataka - Bangalore, Mysore, Hampi, Belgaum, Rajasthan- Jaipur, Udaipur, Jaisalmer, Gujarat – Bhuj and Ahmedabad from Wicked Ride, India’s first premium motorcycle rental company and the most trusted motorcycle rental company. No security deposit and comes with Insurance coverage.">
@stop

@section('content')



    <!-- Page Content -->
    <div class="container">

        <div class="row top-buffer"></div>
        <div class="row top-buffer"></div>
            
        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $bikerPlan->name }}
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-4">
                    <a href="/biker-plan/{{ $bikerPlan->name }}">
                        <img class="img-responsive" src="{{ $bikerPlan->image }}" alt="{{ $bikerPlan->name }}">
                    </a>
            </div>
            

            <div class="col-md-8">
                
                <?php echo($bikerPlan->description); ?>
                <?php echo($bikerPlan->longDescription); ?>
                <div class="row top-buffer"></div>
                <p> Cost: &#8377 {{ $bikerPlan->cost }}</p>
                <div class="row top-buffer"></div>
                <a class="btn btn-primary" href="/buy-biker-plan">Buy Plan </a>
            
                   
            </div>
            
            

            

        </div>
        <!-- /.row -->
        <div class="row top-buffer"></div>
        <div class="row top-buffer"></div>
            
            
        <div class="row">

        <div class="col-lg-12">
            <h3 class="page-header">Other Plans</h3>
        </div>
        @foreach($otherPlans as $otherPlan)
            <!-- Related Projects Row -->


                <div class="col-sm-3 col-xs-6">
                    <a href="/biker-plan/{{ $otherPlan->name }}">
                        <div class="fixOverlayDiv">
                         <img class="img-responsive" src="{{ $otherPlan->image }}" alt="{{ $otherPlan->name }}">
                         <div class="OverlayText">{{ $otherPlan->name }}</div>
                        </div>
                    </a>
                </div>

        @endforeach


        <hr>

       

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>

	
@stop	
