@inject('frontEndText','App\Models\FrontEndText')

@extends('front/frontMaster')

@section('content')
<div style="color: #000; text-align: left; margin: 70px 10% 20px 10%; font-family: 'Gotham-Book'; font-size: 12px; line-height: 22px;">
<?php $tandc = $frontEndText->getText('general_terms_and_conditions'); ?>
{!! $tandc !!}
</div>

@stop	
