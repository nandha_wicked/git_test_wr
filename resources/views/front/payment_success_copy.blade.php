@inject('frontEndText','App\Models\FrontEndText')


@extends('front/frontMaster')

@section('content')
<div class="clear_both"></div>
	
	<!-- <div style="color: #000; text-align: center; margin: 168px 10px; font-size: 23px">
		Your payment has been made successfully.		
	</div> -->
	<table class="booking-success-tb" cellpadding="0" cellspacing="0">
		<tr class="slot3">
			<td>
				<div class="cname">Dear {{ $userFname }}</div>
				<div class="content">Thank you for booking with Wicked Ride. We wish you an awesome experience on your trip and looking forward to serve again in the future.</div>
				<div class="booking-title">Here are your booking details :</div>
				<div class="details">Booking ID: {{ $refId }}</div>
				<div class="details">Pickup Date: {{ $custFromDate }}</div>
				<div class="details">Pickup Time: {{ $custFromTime }}</div>
				<div class="details">Return Date: {{ $custToDate }}</div>
				<div class="details">Return Time: {{ $custToTime }}</div>
				<div class="details">Pickup Location: {{ $areaName }}  {{ $areaAddress }}</div>
				<div class="details">Return Location: {{ $areaName }}  {{ $areaAddress }}</div>
				<div class="details">Make Name: {{ $makeName }}</div>
				<div class="details">Model Name: {{ $modelName }}</div>
				<!-- <div class="details">Bike Img : <img src="img/bikes_upload/thumbnail/{{ $modelThumbnail }}" /></div> -->
				<div class="details">Total Amount: Rs {{ $amount }}</div>
				<div class="details">Documents Required:</div>
				<p><?php echo $frontEndText->getText('documents_required'); ?></p>
				<div class="continue-button">
					<a href="/booking/select-date" class="btn btn-default">Continue</a>
				</div>
			</td>
		</tr>
		<tr>
		</tr>
	</table>
		
<div class="clear_both"></div>
	
@stop	

