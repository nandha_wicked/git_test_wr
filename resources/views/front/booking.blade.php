@inject('priceObj','App\Models\Bike')

@extends('front/frontMaster')

@section('content')
<div class="clear_both"></div>

<!-- booking main starts here-->
<div class="book_bike_main">
	<div class="bike_booking_bg">
		<!-- width 960 starts here-->
		<div class="width_960">
		<?php /* echo $msg;*/ ?>
			<div class="booking_steps">
		  		<ul>
		  			<li class="active" id="step1-li">
		  				<a href="#">
		  					<span>1</span><div> choose date and time</div>
		  				</a>
		  			</li>
		  			<li id="step2-li">
		  				<a href="#">
		  					<span>2</span><div>choose motorcycle</div>
		  				</a>
		  			</li>
		  			<li id="step3-li">
		  				<a href="#">
		  					<span>3</span><div>login or register</div>
		  				</a>
		  			</li>
		  			<li id="step4-li">
		  				<a href="#">
		  					<span>4</span><div>confirm booking & checkout</div>
		  				</a>
		  			</li>
		  		</ul>
		  	</div>
		</div>

		<div id="steps">
			<!-- Step One-->
			<div id="step1" class="width_960">
				<h4>what are the days you looking at<br/> renting your wicked ride?</h4>
				<div class="clear_both"></div>
				<!--booking date main starts here-->
				<div class="booking_date_main book_bike">
					<div class="">
						<form class="booking_date">
							<input type="text" name="from" class="date_picker some_class" value="date & time" id="from" readonly/>
							<span class="inbetween">to</span>
							<input type="text" name="to" class="date_picker some_class" value="date & time" id="to" readonly/>
						</form>
					</div>
				</div>
				<div id="step1-error"></div>
				<div class="next-btn-container">
					<div class="common_btn_small" id="booking-step1-next-btn"><span></span><a href="#">continue</a></div>
				</div>				
				<div class="clear_both"></div>
				<div class="news-step1">
				    <div class="content">
					    <h3>Can't select your time?</h3>
					    <p>We currently operate on 24 hour rental basis, where you pick up bike at 8 PM and return bike at 8 PM. 
					    If you need the bike at a different time, say in the morning, kindly select the previous day 8PM. Do mention your time preference in the notes 
					    before checkout. Our team will accomodate you in that regard. Please note the drop time will remain 8PM. If you need to drop it of the next morning,
					     an extra day's rental will apply. For example, if you want the bike Saturday morning to Sunday morning, 2 days rental will apply.</p>
	          		</div>
				</div>
			</div>

			<div id="step2">
			<!-- accessories sec starts here -->
		  	<!-- <div class="accessories_booking_main">
			  	<h4>choose your ride</h4>
				<div class="accessories_sec">
					<div class="width_960">
						<h5>would you like to rent our accessories?</h5>
						<ul id="bikes" class="item"> 

							<form>
								<li>
									<div class="checkbox">
									    <input id="jacket" type="checkbox" name="jacket" value="jacket">
									    <label for="jacket">jacket</label>
									</div>
									<div class=""><img src="images/ride-accessories-jacket.jpg"></div>
									<div class="small_caps">per day tariff</div>
									<div class="price">RS 1000</div>
								</li>
								<li>
									<div class="checkbox">
									    <input id="glove" type="checkbox" name="glove" value="glove">
									    <label for="glove">gloves</label>
									</div>
									<div class=""><img src="images/ride-accessories-gloves.jpg"></div>
									<div class="small_caps">per day tariff</div>
									<div class="price">RS 1000</div>
								</li>
								<li>
									<div class="checkbox">
									    <input id="bags" type="checkbox" name="bags" value="bags">
									    <label for="bags">bags</label>
									</div>
									<div class=""><img src="images/ride-accessories-bag.jpg"></div>
									<div class="small_caps">per day tariff</div>
									<div class="price">RS 1000</div>
								</li>
								<li>
									<div class="checkbox">
									    <input id="perfomance" type="checkbox" name="perfomance" value="perfomance">
									    <label for="perfomance">perfomace</label>
									</div>
									<div class=""><img src="images/ride-accessories-parts.jpg"></div>
									<div class="small_caps">per day tariff</div>
									<div class="price">RS 1000</div>
								</li>
							</form>
						</ul>
					</div>
				</div>
				<div class="align_center"><div class="common_btn_small"><span></span><a href="">skip</a></div></div>
				<div class="align_center"><div class="common_btn_small"><span></span><a href="">continue</a></div></div>
			</div> -->
			<!-- accessories sec ends here -->
				<div class="available_bikes">
				  	<h4>choose your ride</h4>
				  	<div class="booking_bike_white">
				  	  	<div class="width_960">
				  	  		<?php
				  	  			/*echo "<pre>"; 
				  	  			var_dump($get_all_models_with_detail);
				  	  			echo "<pre>";
				  	  			die();*/
				  	  		?>
				  	  					  		
							<div id="show-bikes"></div>
					  	</div>
					</div>	
								
				</div>
			</div>
			<div id="step3">
				<div class="form_main happy_customer_form">
					<div class="width_960">
						<!-- <div class="radio_select">
							<form class="width_960">
								
								 <div class="left_radio">
									 <label>Enter your coupon code</label>
									 <div class="clear_both"></div>
									 <div class="radio">
									    <input id="coupon_yes" type="radio" name="gender" value="yes">
									    <input class="input_field" type="text" value="" placeholder="coupon code">
									</div>
								 </div>
							</form>
						</div> -->
				
						<div class="cd-tabs bookingPg-formContainer">
							<nav>
								<ul class="cd-tabs-navigation">
									<li><a data-content="inbox" class="selected" href="#0">login</a></li>
									<li><a data-content="new" href="#0">sign up</a></li>
									
								</ul> <!-- cd-tabs-navigation -->
							</nav>

							<ul class="cd-tabs-content">
								<li data-content="inbox" class="selected">
									<form class="form" method="post" action="/front-login" id="login-form">
										<ul>
											<div class="login_for_booking only_for_booking_login">
												<h4>login to confirm booking</h4>
												<div id="login-msg"></div>
												<!-- <div id="login-failed-msg"></div> -->
												<div class="clear_both"></div>
												<li><input type="text" name="email" value="" placeholder="email"></li>
												<li><input type="password" name="password" value="" placeholder="password"></li>
												<input type="hidden" name="booking" value="1">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<!-- <div class="text_align_center"><input class="check" type="checkbox" name="vehicle" value="Bike"><label>remember me</label></div>
												<div class="float_right"><a href=""><span class="indicator">forgot your password?</span></a></div> -->
												<div class="float_right"><a href="password/email" class="forgot-btn"><span class="indicator">forgot your password?</span></a></div>
												<div class="clear_both"></div>
												<div class="common_btn_big"><span></span><input class="submit" type="submit" value="login and book request"></div>
												<div class="connect_with">
													<div class="indicator">or connect with</div>
													<div><a href=""><img src="images/google-icon.png"></a></div>
												</div>
											</div>
											
											<!--<div class="register">
												<div class="border_top">
													<div class="common_btn_big"><span></span><a href="">Register</a></div>
													<div class="common_btn_big"><span></span><a href="">Login</a></div>
												</div>
											</div>-->
										</ul>
									</form>
								</li>

								<li data-content="new">
									<form class="form" method="post" action="/front-signup" id="signup-form">
										<ul>
											<div class="login_for_booking">
												<h4>signup to confirm booking</h4>
												<div id="signup-msg"></div>
												<div class="clear_both"></div>
												<li><input type="text" name="firstName" value="" placeholder="first name"></li>
												<li><input type="text" name="lastName" value="" placeholder="last name"></li>
												<li><input type="text" name="email" value="" placeholder="email id"></li>
												<li><input type="text" name="mobileNumber" value="" placeholder="mobile number"></li>
												<li><input type="password" name="password" value="" placeholder="password"></li>
												<li><input type="password" name="confirmPassword" value="" placeholder="confirm password"></li>
												<input type="hidden" name="booking" value="1">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="common_btn_big"><span></span><input class="submit" type="submit" value="create an account"></div>
											</div>
										</ul>
									</form>
								</li>				
							</ul> <!-- cd-tabs-content -->
							<!-- <a href="#" id="booking-step3-next-btn">Continue</a> -->
						</div> <!-- cd-tabs -->

					</div>
				</div>

			</div>
			<div id="step4">
				<div id="booking-details">
					
				</div>
				<!-- <form method="post" action="paymentRedirect">
					<button type="submit">Make Payment</button>
					<input type="hidden" name="userId"/>
					<input type="hidden" name="modelId"/>
					<input type="hidden" name="areaId"/>
					<input type="hidden" name="fromDate"/>
					<input type="hidden" name="toDate"/>
					<input type="hidden" name="totalPrice"/>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</form>	 -->
			</div>
			<!-- Step 2 after update Starts here -->
			<!--  <div>
				<div class="login_register_main">
					<div class="booking_bike_white">
				  	  <div class="width_960">
				  		<ul id="bikes" class="fleet_bikes happy_customers item">
							<li class="animated fadeInUp">
								<h6>iron 883</h6>
								<div class=""><img src="images/bikes-harley-iron-883.png"></div>
								<div class="clear_both"></div>
								<div class="edit_option"><a href=""></a></div>
							</li>
							<li class="width_more">
								<div class="date_sec_main">
									<div class="date_time_main">
										<div class="date_time">
											<div class="date">16th July 2020</div>
											<div class="time">10am</div>
										</div>
										<div class="break_line"></div>
										<div class="date_time">
											<div class="date">16th July 2020</div>
											<div class="time">10am</div>
										</div>
								    </div>
									<div class="edit_option"><a href=""></a></div>
							    </div>
							    <div class="clear_both"></div>
							    <div class="duration">
							    	<div class="left">
							    		<div class="small_caps">duration</div>
							    		<div class="time">48 hrs</div>
							    	</div>
							    	<div class="left">
							    		<div class="small_caps">accessories added</div>
							    		<div class="time"><span>Jacket</span><span>Jacket</span><span>Jacket</span></div>
							    	</div>
							    </div>
							     <div class="duration">
							     	<div class="float_left">
							    		<div class="small_caps">location</div>
							    		<div class="time">Jayanagar 10th block, Bangalore</div>
							    	</div>
							    	<div class="edit_option"><a href=""></a></div>
							    </div>
							    <div class="duration">
							    		<div class="small_caps">deposit to be paid</div>
							    		<div class="time">Rs 5000</div>
							    </div>
							</li>
							<li class="">
								<div class="book_now">
									<div class="bike_price">
								    	<div class="small_caps">total tariff</div>
										<div class="price">RS 5000</div>
								    </div>
								    <div class="common_btn_small"><span></span><a href="">Book now</a></div>
								</div>
							</li>
						</ul>
					  </div>
					</div>

				</div>
			</div> -->
			<!-- Step 2 after update ends here -->
			
		</div>	  	 
	</div>



</div>
<!-- booking main ends here-->
<!--CACHE-->
<script src="js/jquery.js"></script>
<script src="js/jquery.datetimepicker.js"></script>
<script>

/*$(function(){
	$('.news .content h3').click(function(){
		$('.news .content p').slideToggle();
	});
});*/


/*$('.news').each(function(){
  var t,
      $self = $(this),
      $contentBox = $self.find('.content'),
      newHeight = $contentBox.innerHeight()*10
  
  $self.hover(function(){
      clearTimeout(t);
      $contentBox.stop().animate({
      'height' : newHeight
    }, {
      step: function() {
        $contentBox.css("overflow-y", "auto");
      }
    });
  }, function(){
    clearTimeout(t);
    t=setTimeout(function(){
      $contentBox.stop().animate({
        'height' : 15
      }, {
        step: function() {
          $contentBox.css("overflow-y", "auto");
        }
      });
    }, 300)
  });
 });*/



var dateToday = new Date();
var fromDate = $('#from').datetimepicker({
	//timepicker:false,
	minDate: dateToday,
	format:'d-M-Y 20:00',
	allowTimes:['20:00'],
	scrollInput : false
	//format:'DD.MM.YYYY h:mm a',
	//formatTime:'h:mm a',
	//formatDate:'DD.MM.YYYY'
	/*onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
	}*/
});
/*RJ c*/
$('#to').datetimepicker({
	//timepicker:false,	
	onShow:function( ct ){
	   this.setOptions({
	    minDate:formattedDate($("#from").val())
	   })
	  },
	format:'d-M-Y 20:00',
	allowTimes:['20:00'],
	scrollInput : false
});

function formattedDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate() + 1),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}

/*RJ E*/
/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/
$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

/*$('.some_class').datetimepicker();*/

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
});

$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#datetimepicker3').datetimepicker({
	inline:true
});
$('#datetimepicker4').datetimepicker();
$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
});
$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
});
$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
});
$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
});
$('#datetimepicker6').datetimepicker();
$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
});
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});
$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
});
$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
});
var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
});
$('#datetimepicker_dark').datetimepicker({theme:'dark'})


</script>

<div class="clear_both"></div>
	
@stop	
