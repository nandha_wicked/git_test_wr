@inject('cityObj','App\Models\City')
@inject('bmObj','App\Models\BikeModel')


<!DOCTYPE html>
<html>
    <head> 
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
<meta name="_token" content="{{ Session::token() }}">
<link href='/css/style.css' rel='stylesheet' type='text/css'>
<link href='/css/rj.css' rel='stylesheet' type='text/css'>
<link href='/css/media.css' rel='stylesheet' type='text/css'>
<link href='/css/animate.css' rel='stylesheet' type='text/css'>

	<!-- tabs-->
	<link rel="stylesheet" href="/css/style-tab.css"> <!-- Resource style -->
	<script src="/js/modernizr.js"></script> <!-- Modernizr -->
	<!-- tabs-->


<!-- date selection-->
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
<!-- date selectio-->

<!-- <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '199527113897528');
fbq('track', "PageView");
fbq('track', 'Purchase', {value: '{{ $amount }}', currency: 'INR'});
</script>

<noscript>
    <img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=199527113897528&ev=PageView&noscript=1"
/>
</noscript>
       
    </head>
    <body>
        
 
        
        
        <!-- Google Code for checkout Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 932065471;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "w7XtCIT7-2MQv-G4vAM";
var google_conversion_value = {{ $amount }};
var google_conversion_currency = "INR";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/932065471/?value=1.00&amp;currency_code=INR&amp;label=w7XtCIT7-2MQv-G4vAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

        
        

        <!-- <div id="preloader"></div> -->

        <div class="main">            
            <?php 
	/*$mod = $bmObj->getUniqueBikeModels(1);
	echo "<pre>";
	print_r($mod);
	echo "</pre>";*/
?>	
<!-- navigation starts here-->
 <div id="nav" class="navigation_main">
 	<div class="width_960"><div class="logo"><a href="/"><img src="/images/wicked-ride-logo.png" width="100%"></a></div></div>
 	<div class="first_nav">
 	 <div class="width_960">
 		<ul>
 			<li><a href="http://store.wickedride.in/" target="_blank">accessories store</a></li>
 			<!-- <li><a href="/list-my-bike">list your bike</a></li> -->
 			<li><a href="/about-us">about us</a></li>
 			<li><a href="/reviews">testimonials</a></li>
 			<li><a href="/contact-us">contact us</a></li>
 			@if (isset(Auth::user()->first_name))                        
		        <li id="logout-li">
		           	<a href="/front-logout"> Logout</a>
		            <span class="name" style="text-transform:capitalize;font-size:12px;">Hello, {!! Auth::user()->first_name !!}</span>
		            <?php 
		            	$user_id = Auth::user()->id;
		            	Session::put('user_id', $user_id);
		            ?>
		        </li>		                        
		    @else                       
		        <li><a href="/login">login</a></li>
		    @endif 
 			 
 		</ul>
 	 </div>
 	</div>
 	<div class="second_nav">
 		<div class="width_960">
 			
 			<ul>
	 			<li><span><a class="nav_open" href="">our fleet</a></span><div class="plus_icon"></div></li>
	 			<!-- <li><a href="/booking">book a bike</a></li> -->
	 			<li><a href="/booking/select-date">book a bike</a></li>
	 			<li><a href="/tariff">tariff</a></li>
	 			<li><a href="/faqs">faqs</a></li>
	 			<li><a href="http://blog.wickedride.com/" target="_blank">blog</a></li>
	 			<!-- <li><a href="/booking/select-date">test booking</a></li> -->  <!-- test booking link -->	
 			</ul>
 			<div class="city_number">
	 			<div class="celect_city">
	 				<form method="post" action="/fetch-city">
			 			<label>
						    <select name="select_activeCity" onchange="this.form.submit()">
						    	<!-- Getting value stored in session variable "current_cityId" -->
						    	<?php 
						    		//Session::put('current_cityId', 1);
						    		$session_cityId = Session::get('current_cityId'); 
						    		/*if(isset($session_cityId))
						    			$session_cityId = Session::get('current_cityId');
						    		else
						    			$session_cityId = "1";*/
						    	?>
						    	<?php //var_dump($session_cityId);?>  
						        <?php $cities = $cityObj->getActiveCities(); ?>
	 			 					@foreach($cities as $city)
	 			 						<option value="{{ $city->id }}" 
	 			 							<?php 
	 			 								
	 			 									if($city->id == $session_cityId) echo "selected"; 
	 			 		
	 			 							?> 
	 			 						>
	 			 							{{ $city->city }}
	 			 						</option>
	 			 					@endforeach
						    </select>
						</label>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		 			</form>
		 		</div>
	 			<div class="booking_number">
	 				<span>Any questions? Call.</span>
	 				<h5>08046801054</h5>
	 				<div class="call_icon"></div>
	 			</div>
 			</div>
 		</div>
 	</div>


		 	<!-- our fleet starts here-->
		<div id="subnav" class="our_fleet_sec second_nav_open" style="display:none">
				<h4>our fleet</h4> 
				<ul id="navbikes" class="fleet_bikes">
				<?php 
					$city_id = Session::get('current_cityId');
        			if(!$city_id)
            			$city_id = "1";
				?>
				<?php  $bikeModels = $bmObj->getUniqueBikeModels($city_id); ?>
					@foreach($bikeModels as $bikeModel)
					<li class="animated">
					<?php $image = App\Models\Image::find($bikeModel->thumbnail_img_id); ?>
						<!-- <a href="/bike/{{-- $bikeModel->bike_model --}}"> -->
						<a href="/booking/select-date?model_id={{$bikeModel->model_id}}">
							<div class="item">
								<img src="{{ $image['full'] }}">
							</div>
							<div class="clear_both"></div>
							<div class="itemh common_btn_small"><span></span>{{ $bikeModel->bike_model }}</div>
						</a>
					</li>
					@endforeach
					<!-- <li>
						<a href="motorcycle.php">
							<div class="item"><img src="/images/bikes-harley-street-750.png"></div>
							<div class="clear_both"></div>
							<div class="itemh common_btn_small"><span></span>street 750</div>
						</a>
					</li>
					<li>
						<a href="motorcycle.php">
							<div class="item"><img src="/images/bikes-triumph-bonneville.png"></div>
							<div class="clear_both"></div>
							<div class="itemh common_btn_small"><span></span>bonneville</div>
						</a>
					</li>
					<li>
						<a href="motorcycle.php">
							<div class="item"><img src="/images/bikes-re-desert-storm.png"></div>
							<div class="clear_both"></div>
							<div class="itemh common_btn_small"><span></span>desert storm</div>
						</a>
					</li>
					<li>
						<a href="motorcycle.php">
							<div class="item"><img src="/images/bikes-re-gt.png"></div>
							<div class="clear_both"></div>
							<div class="itemh common_btn_small"><span></span>continental gt</div>
						</a>
					</li> -->
				</ul>
		</div>
		<!-- our fleet ends here-->


		<!-- wraper starts her-->
 <div id="wrapper">

  <input type="checkbox" id="menu" name="menu" class="menu-checkbox">
  <div class="menu">
    <label class="menu-toggle" for="menu"><span>Toggle</span></label>
    <ul>
      <li><a href="http://store.wickedride.in/">accessories store</a></li>
   	  <!-- <li><a href="/listing.php">list your bike</a></li> -->
   	  <li><a href="/about-us">about us</a></li>
   	  <li><a href="/contact-us">contact us</a></li>
		<li>
			<label for="menu-4">Motorcycle</label>
			<input type="checkbox" id="menu-4" name="menu-4" class="menu-checkbox">
			<div class="menu">
				<label class="menu-toggle" for="menu-4"><span>Toggle</span></label>
				<ul class="bike_margin">
					<?php  $bikeModels = $bmObj->getUniqueBikeModels($city_id); ?>
					@foreach($bikeModels as $bikeModel)
						<li>
						<?php $image = App\Models\Image::find($bikeModel->thumbnail_img_id); ?>
							<!-- <a href="/bike/{{-- $bikeModel->bike_model --}}"> -->
							<a href="/booking/select-date?model_id={{$bikeModel->model_id}}">
								<div>
									<img src="{{ $image['full'] }}">
								</div>
								<div class="clear_both"></div>							
								<div class="common_btn_small"><span></span>{{ $bikeModel->bike_model }}</div>
							</a>
						</li>
					@endforeach           
				</ul>
			</div>
		</li>
		<li><a href="/booking/select-date">book a bike</a></li>
		<li><a href="/tariff">tariff</a></li>
        <li><a href="/reviews">testimonials</a></li>
        <li><a href="/faqs">faqs</a></li> 
        <li><a href="http://blog.wickedride.com/">blog</a></li>      

        @if (isset(Auth::user()->first_name))                        
	        <li>	        	
	           	<a href="/front-logout"> Logout</a>
	            <span class="name">Hello, {!! Auth::user()->first_name !!}</span>
	            <?php 
	            	$user_id = Auth::user()->id;
	            	Session::put('user_id', $user_id);
	            ?>
	        </li>		                        
	    @else                       
	        <li><a href="/login">login</a></li>
	    @endif 
    </ul>
  </div>
</div>

<!-- #wrapper -->

 </div>
 <!-- navigation ends here-->

            <div class="clear_both"></div>
	
	<!-- <div style="color: #000; text-align: center; margin: 168px 10px; font-size: 23px">
		Your payment has been made successfully.		
	</div> -->
	<table class="booking-success-tb" cellpadding="0" cellspacing="0">
		<tr class="slot3">
			<td>
				<div class="cname">Dear {{ $userFname }}</div>
				<div class="content">Thank you for signing up with Wicked Ride Biker's Plan. We wish you an awesome experience on your trips and looking forward to serve you.</div>
				
				<div class="continue-button">
					<a href="/booking/select-date" class="btn btn-default">Continue</a>
				</div>
			</td>
		</tr>
		<tr>
		</tr>
	</table>
		
<div class="clear_both"></div>

<!-- footer starts here -->
<div class="footer_main">
	<div class="width_960">
		@if (count($errors->subscribe) > 0)
			<div class="alert alert-danger subscribe-err-msg">
				<ul>
					@foreach ($errors->subscribe->all() as $error)
						<li>* {{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if (session('subscribeSuccessMsg'))
    		<div class="alert alert-success subscribe-success-msg">
        		{{ session('subscribeSuccessMsg') }}
    		</div>
		@endif
		<form method="post" action="/email-subscribe/add" name="subscription">
			<div class="subscribe">
				<input type="text" value="" name="email" placeholder="enter email id to subscribe">
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit"></div>
		</form>

		<div class="footer_nav">
 			<ul>
	 			<li><a href="/booking/select-date">motorcycles</a></li>
	 			<!-- <li><a href="#">bikation</a></li> -->
	 			<li><a href="/reviews">testimonials</a></li>
	 			<li><a href="/faqs">faqs</a></li>
                <li><a href="/termsandconditions">terms and conditions</a></li>
	 			@if (isset(Auth::user()->first_name))                        
			        <li id="logout-li">
			           	<a href="/front-logout"> Logout</a>
			            <?php 
			            	$user_id = Auth::user()->id;
			            	Session::put('user_id', $user_id);
			            ?>
			        </li>		                        
			    @else                       
			        <li><a href="/login">login</a></li>
			    @endif 
 			</ul> 
	 	</div>
	 	<div class="clear_both"></div>
	 	<div class="copy_rights">Copyright 2015 WickedRide adventure services pvt. ltd.</div>
	</div>
</div>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollmagic.min.js"></script>
<script type="text/javascript" src="/js/extra.slider.js"></script>
<script type="text/javascript" src="/js/t.js"></script>
<!-- <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="js/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js"></script> -->
<script type="text/javascript" src="/js/app.js?a=1"></script>
<script type="text/javascript" src="/js/front.js?a=1"></script>
<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>

<!-- tab js-->
<script src="/js/main-old.js"></script> <!-- Resource jQuery -->
<script src="/js/main.js"></script> <!-- Resource jQuery -->
<script src="/js/ChooseDate.js"></script>
<script src="/js/ChooseBike.js"></script>
<!-- tab js-->
<script type="text/javascript">
	Main.init();
</script>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<!--footer ends here-->

<!--  Analytics  Old-->
<script type="text/javascript">
    /* var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-51735917-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/
</script>
<!--  End Analytics Old-->  

<!-- Google Analytics New Starts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68504157-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics New Ends -->


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"07e1m1a8FRh27i", domain:"wickedride.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=07e1m1a8FRh27i" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5641b134e014ea2c3cae03c8/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->        </div>

        <script type="text/javascript">
            // $(".extra-slider").extraSlider({
            //       paginate:false,
            //       auto:false,
            //       type:'fade'
            //   });
            (function() {

                var quotes = $(".quotes");
                var quoteIndex = -1;
                
                function showNextQuote() {
                    ++quoteIndex;
                    quotes.eq(quoteIndex % quotes.length)
                        .fadeIn(2000)
                        .delay(5000)
                        .fadeOut(2000, showNextQuote);
                }
                
                showNextQuote();
                
            })();
        </script>
    </body>
</html>







