@inject('priceObj','App\Models\Bike')

@extends('front/frontMaster')

@section('title')
	<title><?php if($modelObj->model_name == "none") echo "Book Your Amazing Ride Today in Bangalore, Udaipur, Jaipur"; else echo "Rent a ".$modelObj->make_name." ".$modelObj->model_name." in ".$modelObj->city_name;?></title>
@stop

@section('meta')
	<meta name="description" content="Hire a <?php if($modelObj->model_name == "none") echo "premium bike in Bangalore, Udaipur, Jaipur"; else echo $modelObj->make_name." ".$modelObj->model_name." in ".$modelObj->city_name;?> at cheap price. If you would like to book a luxury bike fill out the form or contact us at: 080 4680 1054">
@stop

@section('content')
<div class="clear_both"></div>

<!-- booking main starts here-->
<div class="book_bike_main">
	<div class="bike_booking_bg">
		<h1 class="h1-clear">Book Your <?php if($modelObj->model_name == "none") echo "Ride"; else echo $modelObj->make_name." ".$modelObj->model_name;?></h1>
		<!-- width 960 starts here-->
		<div class="width_960">
			<div class="booking_steps">
		  		<ul>
		  			<li class="active" id="step1-li">
		  				<a href="/booking/select-date">
		  					<span>1</span><div>Choose date and time</div>
		  				</a>
		  			</li>
		  			<li id="step2-li">
		  				<a href="#">
		  					<span>2</span><div>Choose bike</div>
		  				</a>
		  			</li>
		  			<li id="step3-li">
		  				<a href="#">
		  					<span>3</span><div>Login or Register</div>
		  				</a>
		  			</li>
		  			<li id="step4-li">
		  				<a href="#">
		  					<span>4</span><div>Confirm booking & checkout</div>
		  				</a>
		  			</li>
		  		</ul>
		  	</div>
		</div>
		<div id="steps">
			<!-- Step One-->
			<div id="step1" class="width_960">
				<h4>Choose the dates for renting your wicked ride</h4>
				<div class="clear_both"></div>
				<!--booking date main starts here-->
				<div class="booking_date_main book_bike">
					<form class="booking_date" id="selectDateForm" method="get" action="/booking/choose-models">
						<input id="fromdatepicker" name="start_date" type="text" class="date_picker" placeholder="Start Date" readonly autocomplete="off">
						<input id="fromtimepicker" name="start_time" type="text" class="time_picker" placeholder="Time" readonly autocomplete="off">
						
						<span class="inbetween">to</span>

						<input id="todatepicker" name="end_date" type="text" class="date_picker" placeholder="End Date" readonly autocomplete="off">
						<input id="totimepicker" name="end_time" type="text"  class="time_picker" placeholder="Time" readonly autocomplete="off">
						<input type="hidden" name="city_id" value="{{ $city_id }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>
				</div>
				<div id="step1-error"></div>
				<div class="next-btn-container">
					<div class="common_btn_small" id="booking-step1-continue"><span></span><a href="#">continue</a></div>
				</div><br/>
				<div class="clear_both"></div>
				
			</div>			
		</div>	  	 
	</div>
</div>
<!-- booking main ends here-->


<!--CACHE-->
<script src="/js/jquery.js"></script>
<!-- <script src="/js/jquery.datetimepicker.js"></script> -->
<script type="text/javascript">
$(function(){
	Booking.ChooseDate.init();
});
</script>

<div class="clear_both"></div>

@stop
