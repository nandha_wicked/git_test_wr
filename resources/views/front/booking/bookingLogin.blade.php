@inject('priceObj','App\Models\Bike')

@extends('front/frontMaster')

@section('content')
<div class="clear_both"></div>

<!-- booking main starts here-->
<div class="book_bike_main">
	<div class="bike_booking_bg">
		<!-- width 960 starts here-->
		<div class="width_960">
		<?php /* echo $msg;*/ ?>
			<div class="booking_steps">
		  		<ul>
		  			<li id="step1-li">
		  				<a href="/booking/select-date">
		  					<span>1</span><div> choose date and time</div>
		  				</a>
		  			</li>
		  			<li id="step2-li">
		  				<a href="#">
		  					<span>2</span><div>choose bike</div>
		  				</a>
		  			</li>
		  			<li class="active" id="step3-li">
		  				<a href="#">
		  					<span>3</span><div>login or register</div>
		  				</a>
		  			</li>
		  			<li id="step4-li">
		  				<a href="#">
		  					<span>4</span><div>confirm booking & checkout</div>
		  				</a>
		  			</li>
		  		</ul>
		  	</div>
		</div>

		<div id="steps">
			
			
			<div id="step3" style="display: block;">
				<div class="form_main happy_customer_form">
					<div class="width_960">
					
						<div class="cd-tabs bookingPg-formContainer">
							<nav>
								<ul class="cd-tabs-navigation">
									<li><a data-content="inbox" class="selected" href="#0">login</a></li>
									<li><a data-content="new" href="#0">sign up</a></li>
									
								</ul> <!-- cd-tabs-navigation -->
							</nav>

							<ul class="cd-tabs-content">
								<li data-content="inbox" class="selected">
									<form class="form" method="post" action="/front-login" id="login-form">
										<ul>
											<div class="login_for_booking only_for_booking_login">
												<h4>login to confirm booking</h4>
												<div id="login-msg"></div>
												<!-- <div id="login-failed-msg"></div> -->
												<div class="clear_both"></div>
												<li><input type="text" name="email" value="" placeholder="email"></li>
												<li><input type="password" name="password" value="" placeholder="password"></li>
												<input type="hidden" name="booking" value="1">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<!-- <div class="text_align_center"><input class="check" type="checkbox" name="vehicle" value="Bike"><label>remember me</label></div>
												<div class="float_right"><a href=""><span class="indicator">forgot your password?</span></a></div> -->
												<div class="float_right"><a href="/password/email" class="forgot-btn"><span class="indicator">forgot your password?</span></a></div>
												<div class="clear_both"></div>
												<div class="common_btn_big"><span></span><input class="submit" type="submit" value="login and book request"></div>
												<div class="connect_with">
													<div class="indicator">or connect with</div>
													<div><a href=""><img src="images/google-icon.png"></a></div>
												</div>
											</div>
											
											<!--<div class="register">
												<div class="border_top">
													<div class="common_btn_big"><span></span><a href="">Register</a></div>
													<div class="common_btn_big"><span></span><a href="">Login</a></div>
												</div>
											</div>-->
										</ul>
									</form>
								</li>

								<li data-content="new">
									<form class="form" method="post" action="/front-signup" id="signup-form">
										<ul>
											<div class="login_for_booking">
												<h4>signup to confirm booking</h4>
												<div id="signup-msg"></div>
												<div class="clear_both"></div>
												<li><input type="text" name="firstName" value="" placeholder="first name"></li>
												<li><input type="text" name="lastName" value="" placeholder="last name"></li>
												<li><input type="text" name="email" value="" placeholder="email id"></li>
												<li><input type="text" name="mobileNumber" value="" placeholder="mobile number"></li>
												<li><input type="password" name="password" value="" placeholder="password"></li>
												<li><input type="password" name="confirmPassword" value="" placeholder="confirm password"></li>
												<input type="hidden" name="booking" value="1">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="common_btn_big"><span></span><input class="submit" type="submit" value="create an account"></div>
											</div>
										</ul>
									</form>
								</li>				
							</ul> <!-- cd-tabs-content -->
							<!-- <a href="#" id="booking-step3-next-btn">Continue</a> -->
						</div> <!-- cd-tabs -->

						<div class="my-loader">
							<img src="/img/loading2.gif" />
						</div>

					</div>
				</div>

			</div>
		
			
		</div>	  	 
	</div>



</div>
<!-- booking main ends here-->
<!--CACHE-->

<div class="clear_both"></div>
	
@stop	
