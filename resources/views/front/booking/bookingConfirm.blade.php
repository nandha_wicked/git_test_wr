@inject('priceObj','App\Models\Bike')
@inject('frontEndText','App\Models\FrontEndText')

@extends('front/frontMasterCheckout')


@section('content')
<div class="clear_both"></div>

<!-- booking main starts here-->
<div class="book_bike_main">
	<div class="bike_booking_bg">
		<!-- width 960 starts here-->
		<div class="width_960">
		<?php /* echo $msg;*/ ?>
			<div class="booking_steps">
		  		<ul>
		  			<li id="step1-li">
		  				<a href="/booking/select-date">
		  					<span>1</span><div> choose date and time</div>
		  				</a>
		  			</li>
		  			<li id="step2-li">
		  				<a href="#">
		  					<span>2</span><div>choose bike</div>
		  				</a>
		  			</li>
		  			<li id="step3-li">
		  				<a href="#">
		  					<span>3</span><div>login or register</div>
		  				</a>
		  			</li>
		  			<li id="step4-li" class="active">
		  				<a href="#">
		  					<span>4</span><div>confirm booking & checkout</div>
		  				</a>
		  			</li>
		  		</ul>
		  	</div>
		</div>

		<div id="steps">		
			<div id="step4" style="display: block;">
				<div id="booking-details">
					
					<div class="login_register_main">
						<div class="booking_bike_white">
					  	  	<div class="width_960">
					  	  		<!--<form method="post" action="/paymentRedirect">-->
					  			<ul id="bikes" class="book_bike_fleet happy_customers item">
					  			<?php 
					  				$params = '?model_id='.session('booking_model_priority').'&start_date='.session('bkStartDate').'&start_time='.session('bkStartTime').'&end_date='.session('bkEndDate').'&end_time='.session('bkEndTime').'&_token='.csrf_token();
					  			?>
									<li class="animated fadeInUp">
										<h6 class="model-name">{{ session('bkmodelName') }}</h6>
										<?php  $image = Session::get('bkmodelImg');
					                        if (empty($image)) {?>
					                          <div class="">
					                            <img src="/images/wicked-ride-logo.png">
					                          </div>
					                      <?php } else { ?>
					                        <div class="">
					                          <img src="{{ session('bkmodelImg') }}">
					                        </div>
					                    <?php } ?>
										<div class="clear_both"></div>
										<div class="edit_option">
											<!-- <a href="#" class="step4-bike-edit-btn"></a> -->
											<a href="/booking/choose-models<?= $params?>"></a>
											<!-- <a href="#"></a> -->
										</div>
									</li>
									<li class="width_more">
										<div class="date_sec_main">
											<div class="date_time_main">
												<div class="date_time">
													<div class="date from">{{ \Carbon\Carbon::parse(session('bkStartDate'))->format('d M y') }}</div>
													<div class="time">{{ \Carbon\Carbon::parse(session('bkStartTime'))->format('h:i') }}</div>
												</div>
												<div class="break_line"></div>
												<div class="date_time">
													<div class="date to">{{ \Carbon\Carbon::parse(session('bkEndDate'))->format('d M y') }}</div>
													<div class="time">{{ \Carbon\Carbon::parse(session('bkEndTime'))->format('h:i') }}</div>
												</div>
									    	</div>
											<div class="edit_option">
												<!-- <a href="#" class="step4-date-edit-btn"></a> -->
												<a href="/booking/choose-models<?= $params?>"></a>
											</div>
								   		</div>
								    	<div class="clear_both"></div>
								     	<div class="duration">
								     		<div class="float_left">
								    			<div class="small_caps">location</div>
								    			<div class="time loc">{{ session('bkareaName') }}</div>
								    		</div>
								    		<div class="edit_option">
								    			<!-- <a href="#" class="step4-loc-edit-btn"></a> -->
								    			 <a href="/booking/choose-models<?= $params?>"></a>
								    			<!-- <a href="#"></a> -->
								    		</div>
								    	</div>
                                        
								    	<div class="apply-promocode" data-model-id="{{ session('booking_model_priority') }}" data-area-id="{{ session('bkareaId') }}" data-city-id="{{ session('bkcityId') }}" data-start-date="{{ session('bkStartDate') }}" data-start-time="{{ session('bkStartTime') }}" data-end-date="{{ session('bkEndDate') }}" data-end-time="{{ session('bkEndTime') }}">
								    		<div class="promocode-div">
												<input type="text" value="" name="promocode" placeholder="Add Promocode" autocomplete="off">
											</div>
								    		<div class="common_btn_big">
								    			<span></span>
								    			<div class="submit" id="apply-promocode-btn">Apply</div>
								    		</div>
								    	</div>
								    	<div id="promocode-msg"></div>
								    	<a href="#" id="note-add">Add Special Instructions</a>
								    	<div class="note-container"><textarea rows="5" name="note"></textarea></div>
								    <!-- <div class="duration">
								    		<div class="small_caps">deposit to be paid</div>
								    		<div class="time amt"></div>
								    </div> -->
									</li>
									<li class="">
										<div class="book_now">
											<div class="bike_price">
									    		<div class="small_caps">Amount Payable</div>
									    		<span class="small_caps basic_price">Rs {{ session('noTaxAmt') }} </span>
									    		<span class="small_caps">+</span>
									    		<span class="small_caps tax">{{ session('taxAmt') }} ({{$taxDetails->tax_name}} {{$taxDetails->amount}}%)</span>
												<div class="price amt">Rs {{ session('noTaxAmt')*(1+($taxDetails->amount/100)) }}</div>
                                                @if(session('fullSlabAmt') > session('noTaxAmt'))
                                                    <div class="full_slab_price">RS {{ session('fullSlabAmt')*(1+($taxDetails->amount/100)) }}</div>
                                                @endif
									    	</div>
                                            <div class="small_caps message_line">{{ session('message') }}
                                        </div>
									    	<div class="common_btn_small">
									    		<!-- <form method="post" action="/paymentRedirect"> -->
									    		<div class="text_align_center step4-check"><input class="check" type="checkbox" name="age" value="ageyes"><label> I am over {{ $minAge }}</label></div>
                                                <div class="idModal">
                                                <div class="text_align_center step4-check"><input class="check" type="checkbox" name="licence" value="licenceyes"><label> I have valid Driving Licence and the <a onclick="return theFunction();">required documents</a></label></div></div>
									    		<div class="text_align_center step4-check step4-check2"><input class="check" type="checkbox" name="tandc" value="tandcyes"><label> I agree to <a style="float:none; font-size:10px;" href= "/<?php
                                               if (session('bkcityId')==10)
                                               {
                                                  echo "termsandconditionsDelhi"; 
                                               }
                                               if (session('bkcityId')==12)
                                               {
                                                  echo "termsandconditionsNZ"; 
                                               }
                                               else
                                               {
                                                   echo "termsandconditions"; 
                                               }
                                               
                                               ?>" target="_blank">Terms & Conditions</a>.</label></div>
                                                <div class="text_align_center step4-check"><input class="check" type="checkbox" name="donate" value="yes" onclick="return donateMoneyFunction();"><label> I would like to donate Rs 1 to CRY</label></div>
									    		<div class="payment-btn-container">
										    		<span></span>
										    	<!-- <a href="">Book now</a>
										    	<form method="post" action="paymentRedirect"> -->
														<button type="submit" id="validate-step4-check">Continue To Payment</button>
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<!-- </form> -->
												</div>
												<div class="clear_both"></div>
											<!-- <div class="code-apply-container"><input type="text" name="promo_code" placeholder="Promo code"/><button>Apply</button></di -->	
									    		<!-- </form> -->
									   		</div>
										</div>
									</li>
								</ul>
				<!--				</form> -->
						 	</div>
						</div>
					</div>

				</div>
			</div>
		</div>	  	 
	</div>

</div>


<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">×</span>
    <div class="open_paragraph">
                        <p>
                            The following would be the documents that need to be submitted for renting the bike </p>
                        <p><?php echo $frontEndText->getText('documents_required'); ?></p>
                        
					</div>
  </div>

</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("idModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function theFunction() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

function donateMoneyFunction()
{
    var test1=$('#step4 input[name=donate]:checked').val();
    
    if(test1 == "yes")
    {
        var totalPrice = parseFloat($('.book_bike_fleet').find('.book_now .amt').text().substr(3,10)) + 1;
        $('.book_bike_fleet').find('.book_now .amt').text('RS '+totalPrice);
    }
    else
    {
        var totalPrice = parseFloat($('.book_bike_fleet').find('.book_now .amt').text().substr(3,10)) - 1;
        $('.book_bike_fleet').find('.book_now .amt').text('RS '+totalPrice);
    }
    
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<!-- booking main ends here-->
<!--CACHE-->

<script src="/js/jquery.js"></script>
<script src="/js/jquery.datetimepicker.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

        function razorpay() {
        $.ajax({
                        url : '/api/cancelledEnquiries/add',
                        type : 'GET',
                        data : { 
                            'name': "{{ $userName }}",
                            'email': "{{ $userEmail }}",
                            'phone': "{{ $userMob }}",
                            'enquiry': "{{ $enquiry }}",
                            'amount': Math.round($('.book_bike_fleet').find('.book_now .amt').text().substr(3,10)),
                            'notes': $('#step4 textarea').val(),
                            'coupon':$('input[name=promocode]').val()

                        },
                        success: function(data){
                            var cancelledId = data.cancelledId;
                            var test1=$('#step4 input[name=donate]:checked').val();
                            var options = {
                                            "key": "<?php echo env('RAZOR_PAY_PUBLIC');?>",
                                            "amount": Math.round($('.book_bike_fleet').find('.book_now .amt').text().substr(3,10)*100), // 2000 paise = INR 20
                                            "name": "Wicked Ride",
                                            "description": "Motorcycle Rentals",
                                            "image": "/images/wicked-ride-logo.png",
                                            "handler": function (response){
                                                window.location.assign("/payment/success?txnid="+response.razorpay_payment_id+"&note="+$('#step4 textarea').val()+"&donated="+test1);
                                            },
                                            "prefill": {
                                                "name": "{{ $userName }}",
                                                "email": "{{ $userEmail }}",
                                                "contact": "{{ $userMob }}"
                                            },
                                            "notes": {
                                                "enquiry": "{{ $enquiry }}",
                                                "enquiry_id":cancelledId
                                            },
                                            "theme": {
                                                "color": "#eb952e"
                                            }
                                        };
                                var rzp1 = new Razorpay(options);
                                rzp1.open();
                            }
                });
            
        
    }
    
	$('#validate-step4-check').click(function(e){

		//var note=$('textarea[name=booking_note]').val();
		//$('.book_bike_main #step4 input[name=note]').val(note);

		var test1=$('#step4 input[name=age]:checked').val();
		var test2=$('#step4 input[name=licence]:checked').val();
		var test3=$('#step4 input[name=tandc]:checked').val();
					//alert(test1 + test2);

		if(test1 == "ageyes" && test2 == "licenceyes" && test3 == "tandcyes"){
            
            if("{{ session('bkcityId') }}" =="10")
            {
               alert("There is a requirement for security deposit of ₹ {{ $security_deposit }} payable at the location during pick up.\n\nDocuments required for an Indian National:\n- Address proof Xerox\n- Driving License Xerox\n- 2 Recent photos\n- 1 Refundable Post dated cheque\n\nDocuments required for an Foreign National:\n- Passport & Visa Xerox\n- International Driving Permit Xerox\n- 2 Recent photos\n- Return flight ticket Xerox");
			e.preventDefault();
                razorpay();
            }
        
            
			e.preventDefault();
                razorpay();
		}
		else{
			alert("Please tick the checkboxes to continue.");
			return false;
		}
        
        
	});

	// APPLY PROMOCODE
	$('#apply-promocode-btn').click(function(){
		WickedRide.applyPromocode();
	});

</script>

<div class="clear_both"></div>
	
@stop	
