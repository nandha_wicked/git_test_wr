@inject('priceObj','App\Models\Bike')

@extends('front/frontMaster')
@section('title')
	<title>Choose the bike you want to rent</title>
@stop

<script>
var bikeJson = JSON.parse('{!! $json !!}');
var originalBikeJson = JSON.parse('{!! $json !!}');
var cityId = '{{ $cityId }}';
var csrf_token = '{{ csrf_token() }}';
</script>

@section('content')
  <div class="clear_both"></div>
  <!-- booking main starts here-->
  <div class="book_bike_main">
    <div class="bike_booking_bg">
      <!-- width 960 starts here-->
      <div class="width_960">
        <div class="booking_steps">
            <ul>
              <li id="step1-li">
                <a href="/booking/select-date">
                  <span>1</span><div> choose date and time</div>
                </a>
              </li>
              <li id="step2-li" class="active">
                <a href="#">
                  <span>2</span><div>choose bike</div>
                </a>
              </li>
              <li id="step3-li">
                <a href="#">
                  <span>3</span><div>login or register</div>
                </a>
              </li>
              <li id="step4-li">
                <a href="#">
                  <span>4</span><div>confirm booking & checkout</div>
                </a>
              </li>
            </ul>
          </div>
      </div>

      <div id="steps">
        <div id="step2" style="display: block;">
        
          <div class="available_bikes">
              <h4>choose your accessories</h4>
              <div class="booking_bike_white">
                
                <div class="clear_both"></div>
                <div class="width_960">
                <div id="show-bikes" data-month="{{ $month }}" data-year="{{ $year }}" 
                    data-start-date="{{ $sd }}" data-end-date="{{ $ed }}" 
                    data-start-time="{{ $startTime }}" data-end-time="{{ $endTime }}" data-city-id="{{ $cityId }}">
                @foreach($models as $model) 
                  <form method="POST" action="/booking/checkUserLoggedin" data-model-id="{{ $model['id'] }}">
                  <ul class="book_bike_fleet happy_customers item" >
                    <li>
                      <h6 class="">{{ $model['name'] }}</h6>
                      <?php  $image = \App\Models\Image::find($model['image_id']);
                        if (empty($image)) {?>
                          <div class="">
                            <img src="/images/wicked-ride-logo.png">
                          </div>
                      <?php } else { ?>
                        <div class="">
                          <img src="{{ $image['full'] }}">
                        </div>
                      <?php } ?>
                      <div class="clear_both"></div>
                      <div class="bike_price">
                        <div class="small_caps">HOURLY RATE</div>
                        <div class="price effective-price">RS {{ $model['price_per_hour'] }}</div>
                      </div>
                        
                    </li>
                    <li class="width_more">
                      <div class="date-time-calendar">
                        <div class="choose-bike-datetime-picker">
                          <div class="no-border">
                            <div class="choose-bike-date">
                              <input readonly autocomplete="off" placeholder="Date" class="startDatePicker" name="start_date" type="text" value="{{ $model['startDate'] }}" 
                              autocomplete="off">
                            </div>
                            <div class="choose-bike-time">
                              <input readonly autocomplete="off" placeholder="Time" class="startTimePicker" name="start_time" type="text" value="{{ $model['startTime'] }}" 
                              autocomplete="off">
                            </div>
                          </div>
                          <div class="no-border to-bar">
                            <span class="inbetween">to</span>
                          </div>
                          <div class="no-border">
                            <div class="choose-bike-date">
                              <input readonly autocomplete="off" placeholder="Date" class="endDatePicker" name="end_date" type="text" class="date_picker" value="{{ $model['endDate'] }}" 
                              autocomplete="off">
                            </div>
                            <div class="choose-bike-time">
                              <input readonly autocomplete="off" placeholder="Time" class="endTimePicker" name="end_time" type="text"  class="time_picker" value="{{ $model['endTime'] }}" 
                              autocomplete="off">
                            </div>
                          </div>
                          <div class="no-border edit-btn">
                            <span class="edit-button"></span>
                          </div>
                          <div class="clear_both"></div>
                        </div>
                      </div>
                      <div class="clear_both"></div>
                      <div class="step2-error"></div>
                      <div class="booking_date_main">
                        <div class="celect_city">
                        <div class="celect_area">
                          <!-- <form> -->
                            <label>
                              <select name="areaName" placeholder="Select Area" 
                                class="js-states form-control" data-model-id="{{ $model['id'] }}">
                                <optgroup label="Available at">
                                @foreach($model['available_locations'] as $area) 
                                  <option  value="{{ $area['id'] }}" areaName="{{ $area['area'] }}" areaAddress="{{ $area['address'] }}">
                                    {{ $area['area'] }}
                                  </option>
                                @endforeach
                                </optgroup>
                                <optgroup label="Not available at">
                                @foreach($model['not_available_locations'] as $area) 
                                  <option data-target="#dialog" value="{{ $area['id'] }}" areaName="{{ $area['area'] }}" areaAddress="{{ $area['address'] }}" disabled="disabled">
                                    {{ $area['area'] }}
                                  </option>
                                @endforeach
                                </optgroup>
                              </select>
                            </label>
                          <!-- </form> -->
                        </div>
                        </div>
                      </div>
                    <div class="small_caps message_line">
                        {{$model['minimum_billing_message']}}
                    </div>
                    </li>
                    <li class="price-section">
                      <div class="book_now">
                        <div class="bike_price">
                          <div class="small_caps">total tariff</div>
                          <div class="price total-price">RS {{ $model['totalPrice'] }}</div>
                            @if($model['full_slab_price'] > $model['totalPrice'])
                            <div class="full_slab_price">RS {{ $model['full_slab_price'] }}</div>
                            @endif
                        </div>
                        <div class="choose-bike-cta">
                          <div class="booking-step2-continue common_btn_small book-btn <?php echo ($model['bikeAvailability'])? 'show' : '' ?>">
                            <a href="#">Book</a>
                          </div>
                          <div class="sold-out-wrapper <?php echo ($model['bikeAvailability'])? '' : 'show' ?>">
                            <div>Sold Out</div>
                            <div class="avail-btn-wrapper">
                              <span></span>
                              <div class="avail-btn">check availability</div>
                            </div>
                          </div>



                        </div>

                          <div class="common_btn_small book-btn" <?php echo ($model['bikeAvailability'])? 'style="display:none;"':''?>>
                              <a href="#" data-startdate="{{ $model['startDate'] }}" data-starttime="{{ $model['startTime'] }}" data-enddate="{{ $model['endDate'] }}" data-endtime="{{ $model['endTime'] }}"  data-modelname="{{ $model['name'] }}" class="enquire-now">Enquire</a>
                          </div>
                      </div>
                    </li>
                    <input type="hidden" name="modelId" value="{{ $model['id'] }}" />
                    <input type="hidden" name="cityId" value="{{ $cityId }}" />
                    <input type="hidden" name="number_of_hours" value="{{ $model['number_of_hours'] }}" /> 
                    <input type="hidden" name="minimum_hours" value="{{ $model['minimum_hours'] }}" /> 
                    <input type="hidden" name="totalPrice" value="{{ $model['totalPrice'] }}"/>
                    <input type="hidden" name="full_slab_price" value="{{ $model['full_slab_price'] }}"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </ul>
                  </form>
                
                @endforeach

                </div>
                </div>
            </div>  
            
          </div>
        </div>
      </div>
    
    </div>
  </div>
  <!-- booking main ends here-->
  
  <!--CACHE-->
  <script src="/js/jquery.js"></script>
  <!--<script src="/js/jquery.datetimepicker.js"></script>-->

  <script>
    $(function(){
      $('#makeFilter').multipleSelect({
        width: '100%',
        placeholder: "Select Brand"
      });
      Booking.Filter.init();
      Booking.ChooseBike.init();
    });
  </script>

  <div class="clear_both"></div>
@stop
