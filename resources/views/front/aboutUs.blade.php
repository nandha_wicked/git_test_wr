@extends('front/frontMaster')

@section('title')
	<title>About Wicked Ride Services in India, Luxury bike rental company</title>
@stop


@section('meta')
	<meta name="description" content="Wicked Ride is a luxury bike rental company founded in Banglore and now 
	also serving in Jaipur and Udaipur. We complete all biker's dream.">
@stop

@section('content')
<div class="clear_both"></div>

<!-- about main starts here-->
<div class="about_us_main">
  <div id="blurb" class="width_960">
	<h1 class="item h1-clear">About - Wicked Ride</h1>
	<div class="clear_both"></div>
	<p class="item about_para">
		<p><strong>Our journey so far:</strong></p>
<p>Wicked Ride started with a fleet of 8 motorcycles. Our first motorcycle was a Continental GT from Royal Enfield then a Harley-Davidson Street 750.<br />Vivek&rsquo;s excitement woke him up early for his first visit to the Harley-Davidson dealership in Bangalore to ensure that we are one of the first ones to get the newly launched Street 750.</p>
<p>The greatest experience of riding a Harley-Davidson for the first time in life, is what we were happy to offer to a lot of people, for many of whom it was a childhood fantasy.<br />It was something which we had never even dared to dream ourselves, but, once the idea took shape, we were telling to ourselves that &ldquo;we don&rsquo;t have to sell the concept of renting motorcycles; we just have to tell&rdquo;</p>
<p>After the Street 750 from Harley-Davidson, we set our eyes on bigger and more Harley-Davidson motorcycles. We saw a post for a used Super low and out motorcycle lust got better of us and we managed to get it at the same time as our first Street 750. So we had two Harley- Davidsons now&nbsp;J. These motorcycles were parked very safely at Anil&rsquo;s place which had the most secure parking.</p>
<p>Every time we rode these motorcycles, the attention they demanded was massive. It was an awesome head turner. The most frequently asked questions were &ldquo;Esthu Guru?&rdquo; and &ldquo;Esth Koduthe?&rdquo; Which means what does the bike cost and what is the mileage?<br />We used to tell them it&rsquo;s not ours and that we have taken it for rent from Wicked Ride and started our first step in marketing.</p>
<p>Every minute of our journey has been exciting and adventurous.&nbsp; We remember those days when everybody laughed at us for renting premium motorcycles in India and most importantly without any deposit. (Wicked Ride even now doesn&rsquo;t take any deposit). We cherish the days when we had to work with various Government departments in getting the license. We remember when we travelled to Goa to meet the Transport Commissioner to get a set of standard documents so that we can share it with rest of the states. We are thankful for every officer of Government in believing in the potential in our model to promote tourism and provide an alternate way of touring.<br />Special thanks to the Rajasthan, Gujarat and Karnataka Transport Departments for believing in us and granting our first licenses in most of the places we had applied.&nbsp; We got good inputs and support from Rajasthan Transport Department on how to go about in Rajasthan. For the first time in our life we were so happy working and interacting with Government agencies. They were so helpful and worked with us closely in implementing the scheme.</p>
<p>We now are shocked and happy to see the number of motorcycle and bike rental companies which have come up. We are happy that we did the ground work and paved way for a whole new segment of the travel and tourism industry. We definitely want more and more companies to come in as the segment itself is very young and new. A lot of time, effort and money has to be spent on bringing about the awareness. Now this can be shared by all the players in the segment.</p>
<p>We remember those days when we started Wicked Ride- Premium Bike rentals, search results on google for &ldquo;motorcycle&rdquo; or &ldquo;Bikes on rent&rdquo; threw up very few results of little companies with few utility motorcycles, as against now where we have to work on paying for Google AdWordsJ&nbsp;to push us as the top result.</p>
<p>Now Wicked Ride is focusing on bringing the best of motorcycling on one platform and make it easily accessible.<br />We have started an utility arm called &ldquo;ApnaRide&rdquo;, a platform where you can rent scooters and motorbikes such as Honda Activa, Bajaj Avenger, Electric Bikes and Bicycles.<br />Wicked Ride also has been working closely with various agencies in solving the last mile connectivity problems seen in metros and Tier II cities.<br />We also conduct Guided Adventure Motorcycle Tours to Many Places across India and Beyond. Giving one an exhilarating experience of exploring our beautiful country with the thrill and convenience that only a motorcycle can offer.
      </p>
      </p>
 	<div class="clear_both"></div>
	 	<!-- <div class="meet_founders">
	 		<h4 class="item">meet the founders</h4>
	 		<ul id="f" class="item founders">
	 			<li>
	 			  <div class="founder_inside">
	 				<div class="profile_detail vivek"></div>
	 				<div class="name">Vivekananda H.R</div>
	 				<p>Vivekananda H.R. is a Chartered Accountant by qualification and takes care of business development, marketing and sales for wicked ride. He currently loves the Continental GT. He is trying his luck in agriculture  and feels at home when he is at his farm.</p>	 				
	 			  </div>
	 			</li>
	 			<li>
	 			  <div class="founder_inside">
	 				<div class="profile_detail anil"></div>
	 				<div class="name">Anil G.</div>
	 				<p>Gold medalist in Costing.  Is a qualified Company Secretary and cost work accountant. He rides a Classic Chrome 500. He loves riding and he would love to run away exploring new places every chance he gets. He is also an avid foodie.</p>	 				
	 			  </div>
	 			</li>
	 			<li>
	 			  <div class="founder_inside">
	 				<div class="profile_detail varun"></div>
	 				<div class="name">Varun Agni</div>
	 				<p>Bachelor of engineering in electronics and also Master in telecommunications from Maryland. Varun is a licensed sky diver  and an avid foodie. He is extensively updated about everything and we call him the walking encyclopaedia.  Varun also can cook brilliant pizzas and Italian food. </p>
	 			  </div>
	 			</li>
	 		</ul>
	 	</div> -->
	 	<div class="clear_both"></div>
  </div>
  	<!-- <div class="white_patch"></div> -->
  	<!--reservation starts here-->
	<div class="reservation_main">
		<div class="width_960">
			<div class="reserve_sec">
				<span>You can mail us at</span>
				<h5 class="rervation_big"><a href="mailto:customer-support@bounceshare.com">customer-support@bounceshare.com</a></h5>
			</div>
			<div class="reserve_sec">
				<span>You can call us at</span>
				<h5 class="rervation_big"><a href="tel:080 4680 1054">080 4680 1054</a></h5>
			</div>
		</div>
	</div>
	<!--reservation ends here-->
	<div class="clear_both"></div>
</div>
<!-- about main ends here-->

<div class="clear_both"></div>

<!-- form starts here -->
<!-- <div class="form_main happy_customer_form">
	<div class="width_960">
		<form class="form">
			<h4>interested in doing business with us?</h4>
			<ul>
				<li><input type="text" value="" placeholder="Your name"></li>
				<li><input type="text" value="" placeholder="company you represent"></li>
				<li><input type="text" value="" placeholder="email id"></li>
				<li><input type="text" value="" placeholder="mobile number"></li>
				<li class="full_width"><textarea rows="6" cols="10" type="textfield" value="" placeholder="enquiry"></textarea></li>
				<div class="clear_both"></div>
				<div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit enquiry"></div>
			</ul>
		</form>
	</div>
</div> -->
<!-- form ends here-->
<div class="clear_both"></div>
	
@stop	
