<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">WPP Registration</h4>
            </div>
            <div class="modal-body">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p>PERSONAL</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p>WPP</p>
                        </div>
                    </div> 
                </div>
                	
                <form role="form" method="post" action="/wpp-add" id="registerForm" name="mainForm">
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <!-- <h3> Step 1</h3> -->
                                <h2>Personal Detail</h2>
                                <div class="form-group">
                                    <label1 class="control-label1">First Name</label1>
                                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" name="fullName" />
                                </div>
                                <div class="form-group">
                                    <label1 class="control-label1">Email</label1>
                                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Email" name="email" />
                                </div>
                                <div class="form-group">
                                    <label1 class="control-label1">Mobile Number (10 digits only)</label1>
                                    <div class="mob-no">
                                        <span class="input-group-add">+91</span>
                                        <input type="text" type="tel" pattern="^\d{10}$" required class="form-control1 onError" placeholder="Mobile Number" name="mobileNumber" />
                                    </div>
                                </div>
                                <div class="button1">
                                    <button class="btn btn-primary nextBtn btn-lg btn1" type="button">NEXT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2">
                        <!-- <div class="col-xs-12"> -->
                            <div class="col-md-12">
                                <h2>WPP details</h2>
                                <div class="form-group">
                                    <label1 class="control-label">No of bikes to share/add</label1>
                                    <select name="bikes_count" class="input1">
                                        <option value="1">1</option>
                                        <option value="1">2</option>
                                        <option value="1">3</option>

                                        <option value="More than 3">More than 3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label1 class="control-label1">Bikes interested in</label1>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="bikeinterested[]" value="Harley-Davidson" class="box1">Harley-Davidson&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label>
                                            <input type="checkbox" name="bikeinterested[]" value="Triumph" class="box1">Triumph&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label>
                                            <input type="checkbox" name="bikeinterested[]" value="Royal_Enfield" class="box1">Royal Enfield&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label>
                                            <input type="checkbox" name="bikeinterested[]" value="Kawasaki" class="box1">Kawasaki&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label>
                                            <input type="checkbox" name="bikeinterested[]" value="Others" class="box1">Others Bikes&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label1 class="control-label1">Need a loan</label1>

                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="loan" value="true" class="box1"> Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                        <label>
                                            <input type="radio" name="loan" value="false" class="box1" checked> No&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="modal-footer footend11">
                            <button class="btn btn-success btn-lg btn1" type="submit" id="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
