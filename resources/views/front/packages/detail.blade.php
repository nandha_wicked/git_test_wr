<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="/packages/bootstrap.min.css" />  
<link rel="stylesheet" href="/packages/style.css" />
<script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/packages/tab.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="/review/expanding.js"></script>
<script type="text/javascript" src="/review/starrr.js"></script>
<style type="text/css">

     /* Enhance the look of the textarea expanding animation */
     .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
      }

      .stars {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
      }
  </style>
  <style type="text/css">
    .thumbnail{ padding: 0;}

    .carousel-control, .item{
         border-radius: 4px;
     }

    .caption{
        height: 130px;
        overflow: hidden;
    } 

    .caption h4
    {
        white-space: nowrap;
    }

    .thumbnail img{
      width: 100%;
    }

    .ratings 
    {
        color: #d17581;
        padding-left: 10px;
        padding-right: 10px;
    }

    .thumbnail .caption-full {
    padding: 9px;
    color: #333;
    }

    footer{
      margin-top: 50px;
      margin-bottom: 30px;
    }
  </style>
  <style type="text/css">

     /* Enhance the look of the textarea expanding animation */
     .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
      }

      .stars {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
      }
  </style>
   <script type="text/javascript">
    $(function(){

      // initialize the autosize plugin on the review text area
      $('#new-review').autosize({append: "\n"});

      var reviewBox = $('#post-review-box');
      var newReview = $('#new-review');
      var openReviewBtn = $('#open-review-box');
      var closeReviewBtn = $('#close-review-box');
      var ratingsField = $('#ratings-hidden');

      openReviewBtn.click(function(e)
      {
        reviewBox.slideDown(400, function()
          {
            $('#new-review').trigger('autosize.resize');
            newReview.focus();
          });
        openReviewBtn.fadeOut(100);
        closeReviewBtn.show();
      });

      closeReviewBtn.click(function(e)
      {
        e.preventDefault();
        reviewBox.slideUp(300, function()
          {
            newReview.focus();
            openReviewBtn.fadeIn(200);
          });
        closeReviewBtn.hide();
        
      });

      // If there were validation errors we need to open the comment form programmatically 
      @if($errors->first('comment') || $errors->first('rating'))
        openReviewBtn.click();
      @endif

      // Bind the change event for the star rating - store the rating value in a hidden field
      $('.starrr').on('starrr:change', function(e, value){
        ratingsField.val(value);
        var star= value;
        $("#star_reting").val(star);
       });
    });
  </script>
</head>
<body>
    <div class="container">
    <div class="col-xs-12">
        
        <center>
            <img src="/packages/images/banner.jpg" class="img-responsive">
        <label>BIKATION</label><br/>
        </center>
    </div>
    <div class="breadcrumb">
        <div class="col-xs-12">
            <a>Home</a>
            <label>></label>
            <a>Bikation</a>
            <label>></label>
            <a>{{ $packageDetail->name }}</a>
        </div>
    </div>
    <div class="orangeBand">
    </div>
    
    <div class="container">
        <div class="col-xs-12 col-md-12">
        <div class="col-xs-12 col-md-6">
            <img src="{{ $packageDetail['cover_img'] }}" class="img-responsive" width ="300px" hieght="200px"/>
        </div>
        <div class="col-xs-12 col-md-6">
        <h2>{{ $packageDetail->name }}</h2>
        <h4>by Motorcycle club banglore</h4>
        <div class="rating col-xs-12">
                        @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_score) ? '' : '-empty'}}"></span>
                        @endfor
                        <label><a>{{ $total_rows }} Reviews</a></label>
        </div>
        <div class="description col-xs-12">
            {{ $packageDetail->desc }}
        </div>
        <div class="col-xs-12">
            <h2>${{ $packageDetail->price }}/-<span>(Inclusive of Taxes)</span></h2>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-6">
        <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeModel-modal"><i class="fa fa-plus"></i>Book Now</button>

            </div>
            <div class="col-xs-6">
            <img src="/packages/images/friends.png"/>
            <label> {{ $packageDetail->available_seat }}/{{ $packageDetail->total_seats }} Seats Available</label>
            </div>
        </div>
        </div>
        </div>
        <div class="col-xs-12 col-md-12">
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12">
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/directions.png"/>Destination</h5>
                    <p>{{ $packageDetail->name }}<br/>{{ $packageDetail->total_km }}Kms</p>
                </div>
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/calendar.png"/>Date &amp; Time</h5>
                    <p>{{ date('d F, Y H:i:s', strtotime($packageDetail->date_time)) }}<br/></p>
                </div>
                </div>
                <div class="col-xs-12">
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/flag.png"/>Meeting Point</h5>
                    <p>{{ $packageDetail->meeting_point }}</p>
                </div>
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/recommend.png"/>Recomended Bikes</h5>
                    <p>{{ $packageDetail->recommanded_bike }}</p>
                </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12">
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/include.png"/>RIDE INCLUDES</h5>
                    <p>{{ $packageDetail->ride_include }}</p>
                </div>
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/include.png"/>RIDE EXCLUDES</h5>
                    <p>{{ $packageDetail->ride_exclude }}</p>
                </div>
                </div>
                <div class="col-xs-12">
                <div class="col-xs-6">
                    <h5><img  src="/packages/images/rules.png"/>RIDE RULES</h5>
                    <p>{{ $packageDetail->ride_rule }}</p>
                </div>
            
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-12">
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-6">
                    <div>
                        <img src="/packages/images/bigstar.png"/>
                    </div>
                    <center>{{ $total_rating->average_score }}</center>
                    <center>Average ratings based<br/>on {{ $total_rows }} reviews</center>
                </div>
                <div class="col-xs-6">
                    @for ($i=1; $i <= 5 ; $i++)
                            <span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_score) ? '' : '-empty'}}"></span>
                    @endfor
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <form method="POST" action="{{ url('packages/addreviews') }}" class="form-horizontal" enctype="multipart/form-data">
                <div class="col-xs-12">
                <h2>Write a Review</h2>
                <p>Please rate and share your experiences and thoughts<br/>with your fellow riders below</p>
                </div>
                <div class="col-xs-12">
                    <div class="stars starrr" data-rating="{{ Input::old('rating',0 )}}"></div>
                </div>
                <div class="col-xs-12">
                <textarea  placeholder="Write your review here" name="comment"></textarea>
                </div>
                <input type="hidden" name="product_id" value="{{ $packageDetail->id }}">
                <input type="hidden" name="rating" id="star_reting">
                <input type="hidden" name="product_name" value="{{ $packageDetail->name }}">
                <input type="hidden" name="url" value="{{ $packageDetail->url }}">


                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-xs-12">
                <button>Submit</button>
               
                </div>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-md-12">
            @foreach($reviews as $review)
            <div class="col-xs-12 col-md-4">
                <div class="col-xs-12"><h6>{{ date('d F, Y', strtotime($review->created_at)) }}</p></h6></div>
                <div class="col-xs-12"><h4>{{ $review->product_name }}</h4></div>
                <div class="rating col-xs-12">
                    @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                    @endfor
                    <label>{{ date('d F, Y H:i:s', strtotime($review->created_at)) }}</label>
                </div>
                <div class="col-xs-12"><p>{{ $review->comment }}</p></div>
                <div class="col-xs-12"></div>
            </div>
            @endforeach
           
        </div>
    </div>
    </div>






<div class="modal fade" id="addBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="border-style: solid; background-color: #fff;">
       <div class="container">
<div id="content">
    <ul id="tabs" class="nav nav-tabs col-xs-12" data-tabs="tabs">
        <li class="active col-xs-6"><a href="#reg" data-toggle="tab">Register</a></li>
        <li class=" col-xs-6"><a href="#checkout" data-toggle="tab">Checkout</a></li>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active col-xs-12" id="reg">
            <form method="post" action="/packages/addpackage" class="form-horizontal" enctype="multipart/form-data">
            <div class="col-xs-12">
                <div class="col-xs-4">
                    <div class="col-xs-12">
                    <div>
                    <label>First Name</label>
                    <label>*</label>
                    </div>
                    <div><input type="text" name="firstname" class="form-control"/></div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-12">
                    <div>
                    <label>Last Name</label>
                    <label>*</label>
                    </div>
                    <div><input type="text" name="lastname" class="form-control"/></div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-12">
                    <div>
                    <label>Gender</label>
                    <label>*</label>
                    </div>
                    <div><select name="gender">
                              <option value="0">Female</option>
                              <option value="1">Male</option>
                        </select>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-8">
                    <div class="col-xs-12">
                    <div>
                    <label>Email Address</label>
                    <label>*</label>
                    </div>
                    <div><input type="text" name="email" class="form-control"/></div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-12">
                    <div>
                    <label>Mobile Number</label>
                    <label>*</label>
                    </div>
                    <div><input type="text" name="mobileNo" class="form-control"/></div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-12">
                    <div>
                    <label>No of Seats</label>
                    <label>*</label>
                    </div>
                    <div><input type="text" name="no_seats" class="form-control"/></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-8">
                    <div class="col-xs-12">
                    <label>WHICH BIKE ARE YOU RIDING CURRENTLY?</label>
                    <label>{{ $packageDetail->recommanded_bike }}</label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <label>*  Note that the bike your riding<br/>currently should</label>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-8">
                    <div><label>{{ $packageDetail->name }} ({{ $packageDetail->total_days }} Days)</label></div>
                    <div><label>$1250/-</label><label>(inclusive of all taxes)</label></div>
                </div>
                <input type="hidden" name="package_id" value="{{ $packageDetail->id }}">
                <input type="hidden" name="package_name" value="{{ $packageDetail->name }}">
                <input type="hidden" name="url" value="{{ $packageDetail->url }}">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-xs-4">
                     <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Confirm &amp; Proceed</button>
                </div>
            </div>
            </form>
        </div>
        <div class="tab-pane col-xs-12" id="checkout">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <img src="{{ $packageDetail['cover_img'] }}" class="img-responsive"/>
                </div>
                <div class="col-xs-6">
                    <div class="col-xs-12"><label>{{ date('d F, Y H:i:s', strtotime($packageDetail->date_time)) }}</label></div>
                    <div class="col-xs-10"><h2>{{ $packageDetail->name }}</h2></div><div class="col-xs-2"><img src="/packages/images/trash.png"/></div>
                    <div class="col-xs-12"><label>by Motorcycle Club Bangalore</label></div>
                    <div class="rating col-xs-12">
                        @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $total_rating->average_score) ? '' : '-empty'}}"></span>
                        @endfor
                        <label><a>{{ $total_rows }} Reviews</a></label>
                    </div>
                    <div class="col-xs-5">
                        <label> {{ $packageDetail->available_seat }}/{{ $packageDetail->total_seats }} Seats Available</label>
                    </div>
                    <div class="col-xs-5"><label>PRICE</label><label>${{ $packageDetail->price }}/-</label><label>(inclusive of all taxes)</label></div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-8">
                    <div class="col-xs-6"><p>*  Note that the code<br/>
                    entered should be valid<br/>
                    and not expired</p></div>
                </div>
                <div class="col-xs-4">
                    <button>Proceed to Payment</button>
                </div>
            </div>
        </div>
    </div>
</div>

</div> 
    </div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script> 
</body>
</html>