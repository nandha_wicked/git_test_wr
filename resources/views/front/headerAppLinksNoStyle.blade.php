


<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
<meta name="_token" content="{{ Session::token() }}">

<!-- Facebook OG Code -->
<meta property="fb:app_id" content="700892750012103" />
<meta property="og:url" content="https://www.wickedride.com" />
<meta property="og:title" content="Premium Bike Rental - Wicked Ride"/>
<meta property="og:image" content="https://wickedride.com/images/fbthumbnail.png"/>
<meta property="og:site_name" content="WickedRide.com"/>
<meta property="og:description" content="Rent Harley-Davidson, Kawasaki, Triumph, Ducati, Royal Enfield, and many more in Bangalore, Jaipur, Bhuj, Mysore, Ahmedabad, and Udaipur"/>
<!-- End Facebook OG Code -->

<link href='/css/style-app-links.css?v=2' rel='stylesheet' type='text/css'>


