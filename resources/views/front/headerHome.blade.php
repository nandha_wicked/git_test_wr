<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="icon" href="img/favicon.ico" type="img/gif" sizes="16x16">
<meta name="_token" content="{{ Session::token() }}">

<?php
  //or, if you DO want a file to cache, use:
  header("Cache-Control: max-age=864000"); //30days (60sec * 60min * 24hours * 30days)

?>

<!-- Facebook OG Code -->
<meta property="fb:app_id" content="700892750012103" />
<meta property="og:url" content="https://www.wickedride.com" />
<meta property="og:title" content="Premium Bike Rental - Wicked Ride"/>
<meta property="og:image" content="https://wickedride.com/images/fbthumbnail.png"/>
<meta property="og:site_name" content="WickedRide.com"/>
<meta property="og:description" content="Rent Harley-Davidson, Kawasaki, Triumph, Ducati, Royal Enfield, and many more in Bangalore, Jaipur, Bhuj, Mysore, Ahmedabad, and Udaipur"/>
<!-- End Facebook OG Code -->


<!-- Minify and serve 1 CSS File -->

{!! Minify::stylesheet(array('/home/css/bootstrap.min.css', '/home/css/style.css', '/home/font-awesome-4.3.0/css/font-awesome.min.css', 'https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700', '/home/css/style-1.css', '/home/css/rj.css', '/home/css/media.css', '/home/css/animate.css', '/home/css/jquery-ui.css', 'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css', '/home/bikation/fonts/font-awesome/font-awesome.min.css', '/assets/css/site.min.css', '/home/bikation/css/style.css', '/home/css/style-tab.css', '/home/css/jquery.datetimepicker.css')) !!}




	<script src="home/js/modernizr.js"></script> <!-- Modernizr -->
	<!-- tabs-->



 

<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '199527113897528');
fbq('track', "PageView");</script>

<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=199527113897528&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

        
