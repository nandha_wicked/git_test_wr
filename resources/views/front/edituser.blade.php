 @inject('Booking','App\Models\Booking')
 @inject('bike_img','App\Models\Bikation_footer')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Account - WickedRide</title>
    <!-- Bootstrap -->
    <link href="/user/css/bootstrap.min.css" rel="stylesheet">
    <link href="/user/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css" />
    <script src="{{asset('/js/jquery-1.10.2.min.js')}}"></script>
    <script src="{{asset('/js/ajax-crud.js')}}"></script>
    <script src="{{asset('/js/mail.js')}}"></script>
    <script src="{{asset('/js/image.js')}}"></script>
    
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
    
    $(function() {
      $( "#dob" ).datepicker({
        changeMonth: true,
        changeYear: true,
		yearRange: "-60:+0"
    });
	
	$('.bikeModel_delete').click(function(){
		var id = $(this).data('docid');
		$('#specBikeModel-modal-data form').attr("action","/user/document_delete/"+id);
		$('#specBikeModel-modal-data').modal('show');
	});
    });
	
    </script>
    </head>
  <body>

  <div id="fixedtop1">
<div id="center250a">
<header>
	@if (isset(Auth::user()->first_name))                        
	       	   <div class="link no-collapse collapsed" data-toggle="collapse" data-target="#userActions"><div class="text-shade_e">Hello, <a href="#"> {!! Auth::user()->first_name !!}<i class="caret-icon"></i></a></div>
					<ul id="userActions" class="collapse">
						<li><a href="/" title="Profile"><i class="i-profile"></i>Home</a></li>
						<li><a href="/trip" title="Profile"><i class="i-profile"></i>Bikation</a></li>
						<li><a href="/logout" title="Logout"><i class="i-logout"></i>Logout</a></li>
					</ul>
				</div>
				
	            <?php 
	            	$user_id = Auth::user()->id;
	            	Session::put('user_id', $user_id);
	            ?>
	        	                        
	    @else                       
	      
			<ul id="userActions-log" ><li>
			<!--a href="/login"><i class="i-profile"></i>login</a-->
			<a href="#" data-toggle="modal" data-target="#addBikeModel-modal"><i class="i-profile"></i>login</a>
			 
			</li></ul>
	    @endif
</header>		
</div>
</div>
    <div class="container">
			 @if(!empty($blur_image))
      <section class="profile" id="profile_blur" style="background-image: url({!! $blur_image !!});" >
		 @else
	  <section class="profile" id="profile_blur" style="background-image: url(/user/images/bg_profile.jpg);" >		 
	     @endif
        <div class="row profileDetail text-center">
          <div id="targetLayer">
            <button class="btn btn-warning btn-xs btn-detail open-modal settings" data-toggle="modal" data-target="#myModal" value="{!! Auth::user()->id !!}">Edit</button>
           <form id="uploadForm" action="/upload" accept-charset="UTF-8" class="" method="post" enctype="multipart/form-data">
			  @if(!empty($small_image))
              <img src="{!! $small_image !!}" alt="Profile Pic" value="" style="max-width:210px;" class="profilePic">
			  @else
			  <img src="/user/images/user-icon-helmet.png" alt="Profile Pic" value="" style="max-width:210px;" class="profilePic">
		      @endif
              <input id="user_avtar" type="file" style="display:none;" class="file_user_avtar" name="user_avtar">
			  <img src="/user/images/cameraicon.png" alt="Profile Image Upload" title="Profile Image Upload" class="profilePic_upload" id="profilePic_upload" >
              <input name="_token" type="hidden" value="{!! csrf_token() !!}">
								<input type='hidden' name='maxx_avtar' id='maxx_avtar' value='200'/>
										<input type='hidden' name='maxy_avtar' id='maxy_avtar' value='200'/>
										<img src="" alt="Image preview" id="preview_avtar"  style="display:none;">
										<textarea name="base64_avtar" id="base64_avtar" rows='10' cols='90'  style="display:none;"></textarea>
            </form>
          <button class="btn bulbBtn hide">Bulb <i class="notiIcon"></i> </button>

          </div>
          <h1 class="userName">{!! $first_name !!}<Br/> {!! $last_name !!}</h1>
          <div class="userContact">
              <ul>@if(!empty($location))
                <li class="address">{!! $location !!}</li>
                @endif 
                @if (!empty($mobile_num))
                <li class="mobile">{!! $mobile_num !!}</li>
                @endif 
                <li class="email"><a href="{!! $email !!}" target="_blank">{!! $email !!}</a></li>
              </ul>
          </div>
        </div>
        <!--img src="images/bg_profile.jpg" alt="Profile Backgourd" class="profileBg" /-->
      </section>

      <!--section class="utilityBoxes clearfix">
        <div class="utibox wickedRide">
          <a href="#" class="iconQuery"><img src="/user/images/icon-query-orange.png" alt=""></a>
          <p>05 December 2015</p>
          <h2 class="iconWallet">5682</h2>
          <p>WickedRide Credits in your Wallet</p>
        </div>
        
        <div class="utibox utiHistory"><a href="#"><img src="/user/images/historyImg.jpg" alt="View History"></a></div>
        
        <div class="utibox remeningRides">
          <a href="#" class="iconQuery"><img src="/user/images/icon-query-white.png" alt=""></a>
          <span>15</span>
          <p>RIDES REMAINING</p>
        </div>
        
        <div class="utibox annuMemberShip">
          <h2>Annual Membership</h2>
          <p>Please ensure all the details are clearly visible in uploaded image.</p>
          <a href="#" class="readmore arrowLhs">More Details</a>
        </div>
      </section-->
	   <!--div class="breadcrumb">
            <div class="col-xs-12">
                <a href="/">Home</a>
                <label>></label>
                <a href="/trip">Bikation</a>
            </div>
        </div-->
	   <section class="driverID clearfix">
        <div class="col-sm-12 col-md-4 col1">
          <h1>Driver's Identification</h1>
          <p>Please upload picture of your Driving Licence or an Address Proof to book a WickedRide’s Motorcycle.</p>
        </div>
        <div class="col-sm-12 col-md-4 col2">
          <!--a class="btn btn-default appBtn" href="#" role="button">Approved</a-->
		  <!--a class="btn btn-default appBtn" href="#" role="button">Please Upload your Document.</a-->
		  @if ($user_document_status == 'Approve')
		  <a class="btn btn-default appBtn" href="#" data-toggle="modal" data-target="#doc_upload_form" role="button">Approved</a>
		  @else
		  <a class="btn btn-default appBtn" id="" data-toggle="modal" data-target="#doc_upload_form" href="#" role="button">Upload Document</a>
		  @endif	  
		  <!--form id="upload_document_Form" class="" action="/upload_document" accept-charset="UTF-8" class="" method="post" enctype="multipart/form-data">
			@if (!empty($user_document_status))
                <a class="btn btn-default appBtn" id="upload_doc" href="javascript:void(0);" role="button">{!! $user_document_status !!}</a>
			@else
				<a class="btn btn-default appBtn" id="upload_doc" href="javascript:void(0);" role="button">Upload Document</a>
				<input id="user_document" type="file" style="display:none;" class="btn btn-default appBtn" name="user_document">
              <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                @endif
		   
            </form-->
		  
          <img src="/user/images/app_bgImg.jpg" class="appBg" alt="">
        </div>
        <div class="col-sm-12 col-md-4 col3">
          <p>We manually check each document. Please ensure all the details are clearly visible in the uploaded image.</p>
          <p>In case we find improper or faulty details in the document, we will discard them without notice.</p>
        </div>
      </section>

      <section class="bookRide">
        <h1>Bookings</h1>
        <div class="row bookRideWrap">
          <div class="bookRideBoxes clearfix">
            <div class="col-sm-4 col1">
              <h2><?php echo count($bikation_user_bookings_current_List); ?></h2>
              <h3>CURRENT</h3>
              <span>current</span>
            </div>
            <div class="col-sm-4 col2">
              <h2><?php echo count($bikation_user_bookings_complete_List); ?></h2>
              <h3>Completed</h3>
              <span>completed</span>
            </div>
            <div class="col-sm-4 col3">
              <h2><?php echo count($bikation_user_bookings_cancel_List); ?></h2>
              <h3>Upcoming</h3>
              <span>upcoming</span>
            </div>
          </div>
          <div class="bookRideViewBox">
              <span class="curImg"></span>
              <!--a href="#" class="rideShow"></a-->
          </div>
		    <div class="bookingList">
              <ul class="clearfix">
			  <?php $a=1; ?>
			  
			  @foreach($bikation_user_bookingsList as $bikation_user_bookings)
				 @if($bikation_user_bookings->booking == 'bikation_book')	
					<form id="cancel_booking_request_form" class="element-row" action="/upload_document" accept-charset="UTF-8" class="" method="post" enctype="multipart/form-data">
				
					@if($a%2 == 0)	
						@if($bikation_user_bookings->book_status == 'Booking')	
						<li class="bookingBox clearfix completed even">
						@else
						<li class="bookingBox clearfix cancelled even">	
						@endif	 		
					@else
						@if($bikation_user_bookings->book_status == 'Booking')	
						<li class="bookingBox clearfix completed odd">
						@else
						<li class="bookingBox clearfix cancelled odd">	
						@endif	
					@endif	
					
					@if($bikation_user_bookings->Trip_Start_Date <=  date('Y-m-d H:i:s') && $bikation_user_bookings->Trip_End_Date >=  date('Y-m-d H:i:s') )
							<span class="bookingPoint current"></span>
						   @elseif($bikation_user_bookings->Trip_Start_Date >  date('Y-m-d H:i:s'))
						    <span class="bookingPoint upcoming"></span>
							@else
							<span class="bookingPoint"></span>
							@endif	
					  
					  <div class="bookingBoxContent">
						<!--a href="#" class="editbook">edit booking</a-->
						<div class="title clearfix">
						  <span class="thumbImg"><img src="{{ $bikation_user_bookings->Media_URL_trip1 }}" class="circular-pic" alt="{{ $bikation_user_bookings->Trip_Name }}"></span>
						  <h3>{{ $bikation_user_bookings->Trip_Name }}<!--span>WR6HDIR338</span--></h3>
						</div>
						<div class="bookingPeriod">
							<span class="starts">
							
							  <i><?php echo date('d M y',strtotime($bikation_user_bookings->Trip_Start_Date)); ?></i>
							  <em><?php echo date('h:i A',strtotime($bikation_user_bookings->Trip_Start_Date)); ?></em>
							</span>
							@if($bikation_user_bookings->Trip_Start_Date <=  date('Y-m-d H:i:s') && $bikation_user_bookings->Trip_End_Date >=  date('Y-m-d H:i:s') )
							<span class="to current">
						   @elseif($bikation_user_bookings->Trip_Start_Date >  date('Y-m-d'))
						    <span class="to upcoming">
							@else
							<span class="to">	
							@endif	
							To
							</span>
							<span class="ends">
							  <i><?php echo date('d M y',strtotime($bikation_user_bookings->Trip_End_Date)); ?></i>
							  <em><?php echo date('h:i A',strtotime($bikation_user_bookings->Trip_End_Date)); ?></em>
							</span>
						</div>
						<div class="address"><i>Map</i>{{ $bikation_user_bookings->Start_Location }} </div>
						@if($bikation_user_bookings->book_status == 'Booking')
						<div class="bookRideHover">
							<ul>
							<!--li class="cancel"><a href="javascript:void(0);">Cancel</a></li>
							  <li class="edit"><a href="#">Edit</a></li>
							  <li class="cancel"><a href="#">Cancel</a></li>
							  <li class="delete"><a href="#">Delete</a></li-->
							</ul>
						</div>
						@endif	
					  </div>
					</li>
				
				<?php $a++;
						?>
					<input id="Booking_ID" type="hidden" class="deleteid" name="Booking_ID" value="{{ $bikation_user_bookings->Booking_ID }}">
					<input id="Razorpay_id" type="hidden" class="deleteid" name="Razorpay_id" value="{{ $bikation_user_bookings->Razorpay_id }}">
              <input name="_token" id="tokenid" type="hidden" value="{!! csrf_token() !!}">
            </form>	
			@else
			<form id="cancel_booking_request_form" class="element-row" action="/upload_document" accept-charset="UTF-8" class="" method="post" enctype="multipart/form-data">
				
					@if($a%2 == 0)	
						@if($bikation_user_bookings->status == 'COMPLETE')	
						<li class="bookingBox clearfix completed even">
						@else
						<li class="bookingBox clearfix cancelled even">	
						@endif	 		
					@else
						@if($bikation_user_bookings->status == 'COMPLETE')	
						<li class="bookingBox clearfix completed odd">
						@else
						<li class="bookingBox clearfix cancelled odd">	
						@endif	
					@endif	
					
					  <span class="bookingPoint"></span>
					  <div class="bookingBoxContent">
						<!--a href="#" class="editbook">edit booking</a-->
						<div class="title clearfix">
						<?php 
						$thumb_id = $bike_img->getBikeModel_thumb($bikation_user_bookings->model_id);
						$image = App\Models\Image::find($thumb_id);
						?>
						  <span class="thumbImg"><img src="{{ $image['full'] }}" class="circular-pic" alt="{{ $Booking->getBikeMakeName($bikation_user_bookings->model_id) }}" style="width:80px;"></span>
						  <h3>{{ $Booking->getBikeMakeName($bikation_user_bookings->model_id) }} - {{  $Booking->getBikeModelName($bikation_user_bookings->model_id) }}
						  </h3>
						</div>
						<div class="bookingPeriod">
							<span class="starts">
							
							  <i><?php echo date('d M y',strtotime($bikation_user_bookings->start_datetime)); ?></i>
							  <em><?php echo date('h:i A',strtotime($bikation_user_bookings->start_datetime)); ?></em>
							</span>
							<span class="to">
							  To
							</span>
							<span class="ends">
							  <i><?php echo date('d M y',strtotime($bikation_user_bookings->end_datetime)); ?></i>
							  <em><?php echo date('h:i A',strtotime($bikation_user_bookings->end_datetime)); ?></em>
							</span>
						</div>
						<div class="address"><i>Map</i><td>{{ $Booking->getCityName($bikation_user_bookings->area_id) }} - {{ $Booking->getAreaName($bikation_user_bookings->area_id) }}</td> </div>
						@if($bikation_user_bookings->pg_status == 'success')
						<div class="bookRideHover">
							<ul>
							<!--li class="cancel"><a href="javascript:void(0);">Cancel</a></li>
							  <li class="edit"><a href="#">Edit</a></li>
							  <li class="cancel"><a href="#">Cancel</a></li>
							  <li class="delete"><a href="#">Delete</a></li-->
							</ul>
						</div>
						@endif	
					  </div>
					</li>
				
				<?php $a++;
						?>
					<input id="Booking_ID" type="hidden" class="deleteid" name="Booking_ID" value="{{ $bikation_user_bookings->id }}">
					<input id="Razorpay_id" type="hidden" class="deleteid" name="Razorpay_id" value="{{ $bikation_user_bookings->pg_txn_id	 }}">
              <input name="_token" id="tokenid" type="hidden" value="{!! csrf_token() !!}">
            </form>
				@endif		
				

			  @endforeach
			  
			  
              

              </ul>

              <!--a href="#" class="btn btn-default loadMore">Load more Bookings</a-->

          </div>
		  
		  
        </div>

      </section>


     

      <section class="referFriend hide">
        
        <div class="row">
          <div class="col-xm-12 col-md-6 referImg text-center">
            <img src="/user/images/referImg.png" alt="Refer a friend">
            <!--p>Refer now to start earning your free trip!</p-->
          </div>
          
          <div class="col-xm-12 col-md-6 referSec">
            <div class="col-md-11">
              <h1>Refer a Friend</h1>
              <p>Now you can earn WickedRide credits by inviting your friends. <br>
  If your friend becomes a customer, you’ll get 10% of their first ride as credits and your friend gets 10% off.</p>

              <form class="sendMail" method="POST" action="/sendmail" accept-charset="UTF-8" id="myform">
                <div class="form-group">
                    <div class="controls">
                      <label for="exampleInputEmail1">Send an E-mail to your friends</label>
                      <i class="emailIcon"></i>
                    <input id="email_send" class="form-control" placeholder="friends_name@example.com" name="email_send" type="text" value="">
                    </div>
                </div>
                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                <button class="send-btn btn btn-default subBtn" type="button">send email</button>
                <label id="lodder"></label>
              </form> 
              <!--div class="social">
                <p>Share via social media</p>
                <ul>
                  <li class="fb"><a href="#">Facebook</a></li>
                  <li class="twitter"><a href="#">Twitter</a></li>
                  <li class="tmlr"><a href="#">Tumblr</a></li>
                </ul>
              </div>
              <a href="#" class="readmore arrowLhs">Read our Terms & Conditions</a--->
            </div>
          </div>  

        </div>

        <!--div class="referShare">
          <p>Or share your unique referral link in your Blog, E-mail or Instant Messenger <a href="#">< http://www.wickedride.com/signup/?ref=REF ></a></p>
        </div-->


        
</section>


      <div class="modal fade userModel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                  <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                      <input type="hidden" id="id" name="id" value="">
                      
                      <div class="row row1">
                          <div class="col-xs-12 col-sm-12 col-md-3 proPic">
                            <div id="targetLayer">
                              @if(!empty($small_image))
							  <img src="{!! $small_image !!}" alt="Profile Pic" value="" style="max-width:210px;" id="pop_profilePic">
							  @else
							  <img src="/user/images/user-icon.png" alt="Profile Pic" value="" style="max-width:210px;" id="pop_profilePic">
							  @endif
                            </div>
                          </div>

                          <div class="col-xs-11 col-sm-11 col-md-9 righCol">
                              <div class="form-group col-xs-6 error">
                                  <label for="first_name" class="control-label">First Name <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First Name" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="first_name" class="control-label">Last Name <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="last_name" name="last_name" placeholder="Last Name" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="gender" class="control-label">Gender <span class="starIcon">*</span></label>
                                    <select name="gender" id="gender" class="form-control">
                                      <option value="1">Male</option> 
                                      <option value="2">Female</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="dob" class="control-label">Date of Birth <span class="starIcon">*</span></label>
                                  <i class="calenderIcon"></i>
                                  <input type="text" class="form-control has-error" id="dob" name="dob" placeholder="Date of birth" value="" required="">
                                </div>
                                <div class="form-group col-xs-6 error">
                                  <label for="mobile_num" class="control-label">Mobile Number <span class="starIcon">*</span></label>
                                  <input type="text" class="form-control has-error" id="mobile_num" name="mobile_num" placeholder="Mobile Number" disabled>
                                </div>
								 
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-11 divCenter">
                            <div class="form-group col-xs-12 col-sm-12 col-md-8 error">
                              <label for="email_id" class="control-label">Email Address <span class="starIcon">*</span></label>
                              <input type="text" class="form-control has-error" id="email_id" name="email_id" placeholder="Email" value="" required="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="location" class="control-label">Location <span class="starIcon">*</span></label>
                              <select name="location" id="location" class="form-control">
                                <option selected="" value="Bangalore">Bangalore</option> 
                                <option value="Jaipur">Jaipur</option>
								<option value="Udaipur">Udaipur</option>
								<option value="Mysuru">Mysuru</option>
								<option value="Bhuj">Bhuj</option>
								<option value="Ahmedabad">Ahmedabad</option>
								<option value="Hospet">Hospet (Hampi)</option>
								<option value="Belagavi">Belagavi</option>
								<option value="Jaisalmer">Jaisalmer</option>
								<option value="Delhi">Delhi</option>
                              </select>
                            </div>
                            
                            <div class="hide form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="old_password" class="control-label">Current Password <span class="starIcon">*</span></label>
                              <i class="openEye"></i>
                              <input type="text" class="form-control has-error" id="old_password" name="old_password" placeholder="Old Password" value="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="password" class="control-label">New Password <span class="starIcon">*</span></label>
                              <i class="closedEye" id="new_pass"></i>
                              <input type="password" class="form-control has-error" id="password" name="password" placeholder="New Password" value="">
                            </div>
                            <div class="form-group col-xs-6 col-sm-6 col-md-4 error">
                              <label for="inputTask" class="control-label">Confirm Password <span class="starIcon">*</span></label>
                              <i class="closedEye" id="new_re_pass"></i>
                              <input type="password" class="form-control has-error" id="cpassword" name="cpassword" placeholder="Confirm Password" value="">
                            </div>

                            <div class="col-xs12 text-center">
                                <meta name="_token" content="{!! csrf_token() !!}" />
                              <button type="button" class="btn btn-primary btnUpdate" id="btn-save" value="add" data-dismiss="modal">Update</button>
                            </div>

                        </div>
                          
                      </div>
                  </form>

              </div>
          </div>
      </div>
    </div>





<div class="modal fade popBox2" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <h1>Invitation Sent!</h1>
                  <p>Please upload picture of your Driving Licence or an Address Proof to book a WickedRide’s Motorcycle.</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div>

    
		<div class="modal fade popBox2" id="myModal_doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <p>Thank you For uploading your document....</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div> 
	
	<div class="modal fade popBox2" id="cancel_process" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/loading.gif" alt="Invitation Sent">
                </div>
              </div>
          </div>
      </div>
    </div> 
            
		<div class="modal fade popBox2" id="cancel_responce_msg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                  <img src="/user/images/icon-invitationSent.png" alt="Invitation Sent">
                  <p>We would like to inform you that we are processing your cancellation request.</p>
                  <h2>THANK YOU!</h2>
                </div>
              </div>
          </div>
      </div>
    </div> 	
           
	
	
<div class="modal fade popBox2" id="doc_upload_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content" style="border:solid 4px #eb952e">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <div class="popBoxWrapper">
                 <form id="upload_document_Form" action="/upload_document" accept-charset="UTF-8" class="" method="post" enctype="multipart/form-data">
				 <div class="row">
				 <div class="col-md-12"> 
					<div class="row"> 
						<div class="form-group col-xs-12"> 
							<label class="col-md-3"><strong>Doc Type :</strong></label>
							<div class="col-md-6">
								 <select name="user_doc" id="user_doc" class="form-control">
                                                    <option value="DL">Driving Licence</option>
                                                    <option value="Passport">Passport</option>
													<option value="Address">Address proof</option>
                                                </select>
							</div>
						</div>
					</div>
				 </div>
				 <div class="col-md-12"> 
				  <div class="form-group col-xs-12"> 
				 <label class="col-md-3"><strong>Doc :</strong></label>
					<div class="col-md-6">
						<input id="user_document" required="" type="file" class="btn btn-default appBtn user_document" name="user_document">	
					</div>							
					</div>		
					</div>	
				
			@if (!empty($user_document_status))
                <!--a class="btn btn-default appBtn" id="upload_doc" href="javascript:void(0);" role="button">{!! $user_document_status !!}</a-->
			@else
				<!--a class="btn btn-default appBtn" id="upload_doc" href="javascript:void(0);" role="button">Upload Document</a-->
			@endif
				<div class="form-group col-xs-12"> 
								<label class="col-md-2"></label>
								<div class="col-md-6">
								 
										<input type='hidden' name='maxx' id='maxx' value='200'/>
										<input type='hidden' name='maxy' id='maxy' value='200'/>
										<img src="" alt="Image preview" id="preview"  style="display:none;">
										<textarea name="base64" id="base64" rows='10' cols='90'  style="display:none;"></textarea>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button type="submit" onClick="return resize();" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
								</div> 
				</div>
			 </div>
            </form>
			
						<table class="wk-table" border="1" width="100%">
			<thead>
				<tr>
					<th style="text-align: center;padding: 5px;">Document</th>
					<!--th style="text-align: center;padding: 5px;">Reason</th-->
					<th style="text-align: center;padding: 5px;">View</th>
					<th style="text-align: center;padding: 5px;">Status</th>
					<th style="text-align: center;padding: 5px;">Action</th>
					
				</tr>
			</thead>
			<tbody>
			@if(!empty($bikation_user_document_List))
			  @foreach($bikation_user_document_List as $bikation_user_document)
				
				
													<tr>
						<td>{{ $bikation_user_document->doc_type }}</td>
						<!--td>{{ $bikation_user_document->doc_reason }} {{ $bikation_user_document->doc }} </td-->
						<td><a  href="#" id="view_doc" onclick="view_doc('{{ $bikation_user_document->doc }}');" ><!---i class="fa fa-search"></i--> <img src="{{ $bikation_user_document->doc }}" width="50"></a>
						
						</td>
						<td>@if($bikation_user_document->status  == 0) 
							<button class="btn btn-primary" style="margin: 4px 0px;"><i class="fa fa-clock-o"></i> <span>Pending</span></button>
							@elseif($bikation_user_document->status  == 1)
							<button class="btn btn-success" style="margin: 4px 0px;"><i class="fa fa-check-square"></i> <span>Approved</span></button>
                            @else
							<button class="btn btn-danger" style="margin: 4px 0px;"><i class=""></i> <span>Rejected</span></button>
							@endif
							</td>
						
						<td>
						@if($bikation_user_document->status  == 0) 
				<button class="btn bikeModel_delete btn-danger" data-docid="{{ $bikation_user_document->id }}"  ><i class="fa fa-close" aria-hidden="true"></i> <span>Delete</span></button>
						@endif
						</td>
					</tr>
						 @endforeach				
				@else	 
					<tr>
				<td colspan="4">Not any document upload yet!!!</td>
				</tr>	 
				@endif	
				
				
							</tbody>
		</table>
			
                </div>
              </div>
          </div>
      </div>
    </div> 			   
                  
<div class="modal fade" id="specBikeModel-modal-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		
		      		</div>
		      		<div class="modal-body">
						<h4 class="modal-title" id="myModalLabel">Are You Sure Delete this File</h4>
						<div class="form-group">
							<div class="col-md-4">
								<input type="hidden" name="id" class="form-control"/>
							</div>
		        		</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Yes</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> No</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
		
		
	</div>
	
		                 
<div class="modal fade" id="view_document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width:800px;">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		
		      		</div>
		      		<div class="modal-body">
						
						<div class="form-group">
							<iframe id="doc_id" src="" height="500" width="800"></iframe>
		        		</div>
					</div>
		      		
		    	</form>
	    	</div>
	  	</div>
		
		
	</div>
				  
 				  @include('bikationvendor/packages/footer')




    </div>

	<script>
	function view_doc(id)
	{
		$("iframe#doc_id").attr('src',id);
		$("#view_document").modal('show');
	}
	</script>
	<?php
	if(isset($_GET['doc']))
	{
		?>
		<script>
 $(window).load(function(){
	$("#doc_upload_form").modal('show');
	 });
	 
	
		</script>
		<?php 
		
	}
	?>
	
	<script type="text/javascript">
$(document).ready(function (e) {
var _URL = window.URL || window.webkitURL;

$("#user_document").change(function(e) {
    var file, img;


    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
             if(this.width > 1024)
			 {	document.getElementById('maxx').value = 1024;			 }
			 else
			 {document.getElementById('maxx').value = this.width;	 }
		  if(this.height > 1024)
			 {	document.getElementById('maxy').value = 1024;			 }
			 else
			 {document.getElementById('maxy').value = this.height;	 }
			
        };
        
        img.src = _URL.createObjectURL(file);


    }

});
});


function _resize(img, maxWidth, maxHeight) 
{
        var ratio = 1;

	var canvas = document.createElement("canvas");
	canvas.style.display="none";
	document.body.appendChild(canvas);

	var canvasCopy = document.createElement("canvas");
	canvasCopy.style.display="none";
	document.body.appendChild(canvasCopy);

	var ctx = canvas.getContext("2d");
	var copyContext = canvasCopy.getContext("2d");

        if(img.width > maxWidth)
                ratio = maxWidth / img.width;
        else if(img.height > maxHeight)
                ratio = maxHeight / img.height;

        canvasCopy.width = img.width;
        canvasCopy.height = img.height;
try {
        copyContext.drawImage(img, 0, 0);
} catch (e) { 
	//document.getElementById('loader').style.display="none";
//	alert("There was a problem - please reupload your image");
	return false;
}

        canvas.width = img.width * ratio;
        canvas.height = img.height * ratio;
        // the line to change
        //ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
        // the method signature you are using is for slicing
        ctx.drawImage(canvasCopy, 0, 0, canvas.width, canvas.height);

    	var dataURL = canvas.toDataURL("image/png");

	document.body.removeChild(canvas);
	document.body.removeChild(canvasCopy);
    	return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");


};

function resize() { 

	var photo = document.getElementById("user_document");


	if(photo.files!=undefined){ 

		//var loader = document.getElementById("loader");
		//loader.style.display = "inline";

		var file  = photo.files[0];
		
		var preview = document.getElementById("preview");
		var r = new FileReader();
		r.onload = (function(previewImage) { 
			return function(e) { 
				var maxx = document.getElementById('maxx').value;
				var maxy = document.getElementById('maxy').value;
				previewImage.src = e.target.result; 
				var k = _resize(previewImage, maxx, maxy);
				if(k!=false) { 
				document.getElementById('base64').value= k;
				document.getElementById('upload_document_Form').submit();
				} else {
alert('problem - please attempt to upload again');
				}
			}; 
		})(preview);
		r.readAsDataURL(file);
	} else {
		alert("Seems your browser doesn't support resizing");
	}
	return false;
}

//document.getElementById('photo').addEventListener('change', handleFileSelect, false);


</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/user/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
  </body>
</html>
