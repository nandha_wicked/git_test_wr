@extends('front/frontMaster')

@section('content')
<div class="clear_both"></div>

<!-- login register main ends here-->

	
  	<!-- form starts here -->
	<!-- <div class="form_main happy_customer_form" style="padding-top: 100px;"> -->
	<div class="form_main happy_customer_form">
		<h1 class="item h1-clear">Login - Wicked Ride</h1>
		<div class="width_960">


			<!-- <div class="radio_select">
				<form class="width_960">
					
					 <div class="left_radio">
						 <label>Enter your coupon code</label>
						 <div class="clear_both"></div>
						 <div class="radio">
						    <input id="coupon_yes" type="radio" name="gender" value="yes">
						    <input class="input_field" type="text" value="" placeholder="coupon code">
						</div>
					 </div>
				</form>
			</div> -->

			

			<div class="cd-tabs bookingPg-formContainer" id="sign-inup-container">
				<nav>
					<ul class="cd-tabs-navigation">
						<li><a data-content="inbox" class="selected" href="#0">login</a></li>
						<li><a data-content="new" href="#0">sign up</a></li>
						
					</ul> <!-- cd-tabs-navigation -->
				</nav>

				<ul class="cd-tabs-content">
					<li data-content="inbox" class="selected">
						<form class="form" method="post" action="/front-login" id="login-form">
							<ul>
								<div class="login_for_booking only_for_booking_login">
									<h4>login to confirm booking</h4>
									<div id="login-msg"></div>
									<!-- <div id="login-failed-msg"></div> -->
									<div class="clear_both"></div>
									<li><input type="text" name="email" value="" placeholder="email" required=""></li>
									<li><input type="password" name="password" value="" placeholder="password" required=""></li>
                                    <input type="hidden" name="booking" value="0">
                                    <input type="hidden" name="plan" value="1">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<!-- <div class="text_align_center"><input class="check" type="checkbox" name="vehicle" value="Bike"><label>remember me</label></div> -->
									<div class="float_right"><a href="password/email" class="forgot-btn"><span class="indicator">forgot your password?</span></a></div>
									<div class="clear_both"></div>
									<div class="common_btn_big"><span></span><input class="submit" type="submit" value="login and book request"></div>
									<div class="connect_with">
										<div class="indicator">or connect with</div>
										<div><a href=""><img src="images/google-icon.png"></a></div>
									</div>
								</div>
								
								<!--<div class="register">
									<div class="border_top">
										<div class="common_btn_big"><span></span><a href="">Register</a></div>
										<div class="common_btn_big"><span></span><a href="">Login</a></div>
									</div>
								</div>-->
							</ul>
						</form>

						<!-- <form class="form" method="post" action="password/email" id="forgot-password-form">
							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<ul>
								<div class="login_for_booking only_for_booking_login">
									<h4>Enter login email</h4>
									<div id="login-msg"></div>
									<div class="clear_both"></div>
									<li class="pwd-li"><input type="text" name="email" value="" placeholder="email"></li>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div><a href="#" class="return-btn"><span class="indicator">Return to login</span></a></div>
									<div class="clear_both"></div>
									<div class="common_btn_big"><span></span><input class="submit" type="submit" value="Send Password Reset Link"></div>
									<div class="connect_with">
										<div class="indicator">or connect with</div>
										<div><a href=""><img src="images/google-icon.png"></a></div>
									</div>
								</div>
							</ul>
						</form> -->
					</li>

					<li data-content="new">
						<form class="form" method="post" action="/front-plan-signup" id="signup-form">
							<ul>
								<div class="login_for_booking">
									<h4>signup to confirm booking</h4>
									<div id="signup-msg"></div>
									<div class="clear_both"></div>
									<li><input type="text" name="firstName" value="" placeholder="first name" required=""></li>
									<li><input type="text" name="lastName" value="" placeholder="last name" required=""></li>
									<li><input type="text" name="email" value="" placeholder="email id" required=""></li>
									<li><input type="text" name="mobileNumber" value="" placeholder="mobile number" required=""></li>
									<li><input type="password" name="password" value="" placeholder="password" required=""></li>
									<li><input type="password" name="confirmPassword" value="" placeholder="confirm password" required=""></li>
                                    <input type="hidden" name="booking" value="0">
                                    <input type="hidden" name="plan" value="1">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="common_btn_big"><span></span><input class="submit" type="submit" value="create an account"></div>
                                    
								</div>
							</ul>
						</form>
					</li>

					
				</ul> <!-- cd-tabs-content -->
			</div> <!-- cd-tabs -->

			<div class="my-loader">
				<img src="img/loading2.gif" />
			</div>

		</div>
	</div>
	<!-- form ends here-->
<div class="clear_both"></div>
@stop