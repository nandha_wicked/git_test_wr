<!DOCTYPE html>
<html>
    <head>        
        @include('front/header')
        @yield('title')
        @yield('meta')
        
        
    </head>
    <body>
        <!-- <div id="preloader"></div> -->

        <div class="main">
            <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha2&render=explicit" async defer></script>
            <div id="dialog" title="Enquiry Form" style="display: none;">
                <form class="form-horizontal col-sm-12" id="enquire-form" action="/enquiry-form/add" method="post">
                    <fieldset>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                    <input type="hidden" name="bikemodel" value="" id="bikemodel" />
                    <input type="hidden" name="startDate" value="" id="startdate" />
                    <input type="hidden" name="startTime" value="" id="starttime" />
                    <input type="hidden" name="endDate" value="" id="enddate" />
                    <input type="hidden" name="endTime" value="" id="endtime" />

                        <label> First Name</label>
                        <input type="text" name="first_name" required="true" class="required ui-widget-content ui-corner-all">
                        <label> Last Name</label>
                        <input  type="text" name="last_name" required="true" class="required ui-widget-content ui-corner-all">

                        <label>E-Mail</label>
                        <input type="email" name="email" id="email" required="true" class="required  ui-widget-content ui-corner-all">
                        <label>Phone</label>
                        <input  type="phone" name="phone" required="true" class="required  ui-widget-content ui-corner-all">
                         <label>Message</label>
                        <textarea name="message" required="true" id="enquiremessage" class="required  ui-widget  ui-corner-all" ></textarea>
                                                <div id="footercaptcha_container"></div>

                    <input type="submit" value="Send It" id="enquirysubmit">
                        {{--<button type="submit" class="btn btn-success pull-right">Send It!</button>--}}
                        <p class="help-block pull-left text-danger hide" style="color: #a94442;background-color: #f2dede;border-color: #ebccd1;" id="form-error">&nbsp; The form is not valid. </p>
                        <p class="help-block pull-left alert-success hide" style="color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;" id="form-success">&nbsp; Enquiry form submitted.We will contact you shortly </p>

                        </fieldset>
                </form>
            </div>
            @include('front/navigation')

            @yield('content')

            @include('front/footer')            
        </div>

        <script type="text/javascript">
            // $(".extra-slider").extraSlider({
            //       paginate:false,
            //       auto:false,
            //       type:'fade'
            //   });
            (function() {

                var quotes = $(".quotes");
                var quoteIndex = -1;
                
                function showNextQuote() {
                    ++quoteIndex;
                    quotes.eq(quoteIndex % quotes.length)
                        .fadeIn(2000)
                        .delay(5000)
                        .fadeOut(2000, showNextQuote);
                }
                
                showNextQuote();
                
            })();
        </script>
    </body>
</html>
