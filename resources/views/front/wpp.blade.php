<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Partner Program</title>
<link rel="icon" href="/images/favicon.ico" type="image/gif" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_wpp.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css"> 
 -->
</head>

<body>
    <div class="container-fluid clearfix header">
       <div class="button4">
            <button type = "button" onclick="window.location.href='/'" class = "btn-home btn btn-default">Home</button>
      </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="image">
                        <img src="images/wpp/logo.png" alt="logo">
                    </div>
                    <div class="text11">
                        <h4>Own your dream motorcycle,</h4>
                    </div>        
                    <div class="text21">
                        <h1> And make it earn for you. </h1>
                    </div>      
                    <div class="register1">
                        <a data-toggle="modal" data-target="#myModal" class="largeredbtn">REGISTER</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid clearfix textsection">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   
                    <div class="subtext">
                        <p>Well.. We all dreamt of owning a Pulsar when we first saw the Pulsar ad or a Bullet when we saw a guy riding a Bullet or a Harley-Davidson after watching Terminator.</p>
                    </div>
                    <div class="descriptiontext">
                        <p>With the Wicked Ride &#39;Earn from your bike program &#39; you can ride your own dream bike when needed and earn from it by renting it out to fellow riders.You can now buy a bike and get it associated with Wicked Ride and we shall take it ahead from there..Get riders to ride them and manage bookings, revenue and bikes with timely service from the authorized dealers.All you have to do is sit back, relax and spend the money your bike earns or buy newer bikes :) .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>            
    <div class="container-fluid clearfix registersection">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <div class="punchlinetext">
                        <p>Register today to become a proud bike owner and a member of India's first bike-sharing community.</p>
                    </div>
                    <div class="">
                        <form action="#" class="form1">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Register</button>
                          <button id="know-more" type="button" class="btn btn-info btn-lg">Know More</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="container-fluid clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="modal-title">WPP Video</h4>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/poJT9oX_Doo"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid clearfix end-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper">
                        <div class="copyright clearfix">
                            USE OF THIS SITE CONSTITUTES ACCEPTANCE OF OUR <a href="https://www.wickedride.com/privacy">PRIVACY POLICY</a>. COPYRIGHT 2015 WICKEDRIDE ADVENTURE SERVICES PVT. LTD.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   @extends('front/wpp-modal')

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script_wpp.js"></script>
</body>
</html>