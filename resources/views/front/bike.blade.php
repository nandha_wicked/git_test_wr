@extends('front/frontMaster')

@section('content')

<style type="text/css">
	.home_slider li{background-image: url(/img/bikes_upload/cover/{{ $bikeModel->cover_img }});}
</style>
<!--banner starts here-->
<div id="" class="motorcycle_banner">
    <ul class="home_slider">
	    <li class="current">
	    	<div id="blurb" class="motorcycle_banner_content">
	    		<h1><small class="item">{{ $bikeModel->getBikeMakeName($bikeModel->id) }}</small><big class="item">{{ $bikeModel->bike_model }}</big></h1>
	    		<div class="common_btn item"><span></span><a href="/booking/select-date">book now</a></div>
	    		<div class="bike_rating"></div>
	    	</div>
	    </li>
    </ul>
    <!--             
    <div class="banner_nav" style="display: block;">
	    <a class="prev" href=""></a>
	    <a class="next" href=""></a>
	</div>
	-->
</div>          
<!--banner ends here-->

<!-- gallery & technical spec starts here -->
<div class="gallery_tech_main">
 <div class="width_960">
  <div class="giving_space">
	<!-- gallery_sec left starts here-->
	<div class="gallery_sec">
		<h4>gallery</h4>
		<ul class="gallery">
			<li >
				<a class="left" ><img src="/img/bikes_upload/gallery/0gallery-thumb-iron-01.jpg"></a>
			</li>
			<li>
				<a ><img src="/img/bikes_upload/gallery/1gallery-thumb-iron-02.jpg"></a>
			</li>
			<li>
				<a ><img src="/img/bikes_upload/gallery/2gallery-thumb-iron-03.jpg"></a>
			</li>
			<li class="width_100">
				<a ><img src="/img/bikes_upload/gallery/3gallery-thumb-iron-04.jpg"></a>
			</li>
			<li>
				<a ><img src="/img/bikes_upload/gallery/4gallery-thumb-iron-05.jpg"></a>
			</li>
			<li>
				<a ><img src="/img/bikes_upload/gallery/5gallery-thumb-iron-06.jpg"></a>
			</li>
			
		</ul>
	</div>
	<!-- gallery_sec left ends here-->

	<!-- tech spec right starts here-->
	<div class="tech_sec">
		<h4>technical specs</h4>
		<div class="accordian_main">
			<ul>
				<?php if($bikeModel->getBikeModelSpecKey($bikeModel->spec1) != "" ||  $bikeModel->getBikeModelSpecValue($bikeModel->spec1) !=""){ ?>
				<li>
					<div class="accordian_title"><h6>{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec1) }}</h6><span></span></div>
					<div class="open_para">
						<p>{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec1) }}</p>
					</div>
				</li>
				<?php } ?>
				<?php if($bikeModel->getBikeModelSpecKey($bikeModel->spec2) != "" ||  $bikeModel->getBikeModelSpecValue($bikeModel->spec2) != ""){ ?>
				<li>
					<div class="accordian_title"><h6>{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec2) }}</h6><span></span></div>
					<div class="open_para">
						<p>{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec2) }}</p>
					</div>
				</li>
				<?php } ?>
				<?php if($bikeModel->getBikeModelSpecKey($bikeModel->spec3) != "" ||  $bikeModel->getBikeModelSpecValue($bikeModel->spec3) !="" ){ ?>
				<li>		
					<div class="accordian_title"><h6>{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec3) }}</h6><span></span></div>
					<div class="open_para">
						<p>{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec3) }}</p>
					</div>
				</li>
				<?php } ?>
				<?php if($bikeModel->getBikeModelSpecKey($bikeModel->spec4) != "" || $bikeModel->getBikeModelSpecValue($bikeModel->spec4) !="" ){ ?>
				<li>
					<div class="accordian_title"><h6>{{ $bikeModel->getBikeModelSpecKey($bikeModel->spec4) }}</h6><span></span></div>
					<div class="open_para">
						<p>{{ $bikeModel->getBikeModelSpecValue($bikeModel->spec4) }}</p>
					</div>
				</li>
				<?php } ?>
			</ul>
		</div>
		<!-- review starts here-->
		<div class="cutomers_review">
			<h4>what our customers felt about this motorcycle</h4>
			<ul class="testimon_sec">
				<li>
					<div class="profile"><img src="/images/testimonial-profile-01.jpg"></div>
					<div class="testimonial_copy">
						<!-- <fieldset class="rating">
						    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
						    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
						    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
						    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
						    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
						    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
						    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
						    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
						    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
						    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
						</fieldset> -->
						<div class="clear_both"></div>
						<p>Incredible experience.One of the best surprises I have given to my Hubby on his B'day **Harley Davidson Superlow**.The moment u start it u hav all eyes on u,we had an awesome ride..the sound it makes...woooo....real good.</p>
						<span class="profile_name">Sushant</span>
						<div class="clear_both"></div>
						<div class="dated"><span>Rated: 12th May, 2015</span></div>
					</div>
				</li>
			</ul>
		</div>
		<!-- review ends here-->
	</div>
	<!-- tech spec right ends here-->
	<div class="clear_both"></div>
	</div>
 </div>
</div>
<!-- gallery & technical spec ends here -->
<br/>
<br/>
<br/>
<!-- accessories sec starts here -->
<!-- <div class="accessories_sec">
	<div class="width_960">
		<h4>checkout our store</h4>
		<ul id="bikes"> 
			<li>
				<h6 class="item">jackets</h6>
				<div class="item"><img src="/images/ride-accessories-jacket.jpg"></div>
				<div class="item common_btn_small"><span></span><a href="">view all jackets</a></div>
			</li>
			<li>
				<h6 class="item">gloves</h6>
				<div class="item"><img src="/images/ride-accessories-gloves.jpg"></div>
				<div class="item common_btn_small"><span></span><a href="">view all gloves</a></div>
			</li>
			<li>
				<h6 class="item">bags</h6>
				<div class="item"><img src="/images/ride-accessories-bag.jpg"></div>
				<div class="item common_btn_small"><span></span><a href="">view all bags</a></div>
			</li>
			<li>
				<h6 class="item">perfomance parts</h6>
				<div class="item"><img src="/images/ride-accessories-parts.jpg"></div>
				<div class="item common_btn_small"><span></span><a href="">view all parts</a></div>
			</li>
		</ul>
	</div>
</div> -->
<!-- accessories sec ends here -->

	
@stop	
