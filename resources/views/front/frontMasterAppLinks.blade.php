<!DOCTYPE html>
<html>
    <head>        
        @include('front/header')

    </head>
    <body>
        <!-- <div id="preloader"></div> -->

        <div class="main">            

            @yield('content')

        </div>

      
    </body>
</html>
