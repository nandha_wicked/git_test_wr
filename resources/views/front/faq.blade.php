@extends('front/frontMaster')

@section('title')
	<title>Faqs - Booking, Advice, Wicked Ride India</title>
@stop


@section('meta')
	<meta name="description" content="Check your booking advice or query regarding booking a
	 ride in FAQ. If your query isn't answered here, please don't hesitate to contact us.">
@stop

@section('content')
<div class="clear_both"></div>
<!-- our fleet starts here-->
<div class="our_fleet_sec">
	<h2>how does it work?</h2>

	<div class="width_960">
		<div class="accordian_main">
			<ul>
				@foreach($faqs as $faq)
				<li>
					<div class="accordian_title"><h6>{!! strip_tags($faq->question) !!}</h6><span></span></div>
					<div class="open_para">
						<p>
							{!! $faq->answer !!}
						</p>
					</div>
				</li>
                @endforeach
			</ul>
		</div>	
	</div>

	


<!-- form starts here -->
<div class="form_main happy_customer_form">
	<div class="width_960">
		<h4 class="hd">have more queries? get in touch with us.</h4>
		@if (count($errors->faq) > 0)
			<div class="alert alert-danger faq-err-msg">
				<ul>
					@foreach ($errors->faq->all() as $error)
						<li>* {{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if (session('faqSuccessMsg'))
    		<div class="alert alert-success faq-success-msg  contact-pop">
				{{ session('faqSuccessMsg') }}
				<span class="close-btn">&times;</span>
    		</div>
		@endif
        <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha1&render=explicit" async defer></script>
		<form class="form" method="post" action="/faq-enquiry/add" name="faq-form">
			<ul>
				<li><input type="text" name="firstName" value="" placeholder="First Name"></li>
				<li><input type="text" name="lastName" value="" placeholder="Last Name"></li>
				<li><input type="text" name="email" value="" placeholder="Email"></li>
				<li><input type="text" name="mobileNumber" value="" placeholder="Mobile Number"></li>
				<li class="full_width"><textarea rows="6" cols="10" type="textfield" name="enquiry" value="" placeholder="Your enquiry"></textarea></li>
				<li> <div id="captcha_containerfaq"></div> </li>
				<!-- <li> <div class="g-recaptcha" 
                    data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                </div></li> -->
				<div class="clear_both"></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit enquiry"></div>
			</ul>
		</form>
	</div>
</div>
<!-- form ends here-->


	
@stop	
