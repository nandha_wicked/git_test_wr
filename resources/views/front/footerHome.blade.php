<div class="space20"></div>

	<!-- START FOOTER SECTION -->

		<section>
			<div class="footer_main">
                
                @if (count($errors->subscribe) > 0)
                    <div class="alert alert-danger subscribe-err-msg">
                        <ul>
                            @foreach ($errors->subscribe->all() as $error)
                                <li>* {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('subscribeSuccessMsg'))
                    <div class="alert alert-success subscribe-success-msg">
                        {{ session('subscribeSuccessMsg') }}
                    </div>
                @endif
				<div class="container">
					<div class="row">
						
							<div class="foot-form">
								<form method="post" action="/email-subscribe/add" name="subscription">
								  <div class="col-sm-3">
								        <input type="email" class="footerSub" id="exampleInputEmail2" placeholder="ENTER EMAIL ID TO SUBSCRIBE">
                                     
								  </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="common_btn_big col-sm-offset-0 col-sm-2"><span></span><input class="submit" type="submit" value="submit"> SUBMIT</div>
								</form>
							</div>	
                        

						<div class="col-sm-offset-0 col-sm-6">
							<div class="linkin">
								<ul>
									<li><a href="/booking/select-date">motorcycles</a></li>
                                    <!-- <li><a href="#">bikation</a></li> -->
                                    <li><a href="/reviews">testimonials</a></li>
                                    <li><a href="/faqs">faqs</a></li>
                                    <li><a href="/<?php
                                        $city_id = Session::get('current_cityId');
                                        if(!$city_id)
                                           $city_id = "1";
                                                                   if ($city_id==10)
                                                                   {
                                                                      echo "termsandconditionsDelhi"; 
                                                                   }
                                                                   if ($city_id==12)
                                                                   {
                                                                      echo "termsandconditionsNZ"; 
                                                                   }
                                                                   else
                                                                   {
                                                                       echo "termsandconditions"; 
                                                                   }

                                                                   ?>">terms and conditions</a></li>
                                    @if (isset(Auth::user()->first_name))                        
                                        <li id="logout-li">
                                            <a href="/front-logout"> Logout</a>
                                            <?php 
                                                $user_id = Auth::user()->id;
                                                Session::put('user_id', $user_id);
                                            ?>
                                        </li>		                        
                                    @else                       
                                        <li><a href="/login">login</a></li>
                                    @endif 
								</ul>
							</div>
						</div>
					</div>

						<div class="row space20">
							<div class="col-md-12">
								<div class="footer_nav">
									<div style="font-size:10px; color:black; display:none">
			                
					                    <p> Bikes for rent in Bangalore to explore city. Rent a bike for daily commute. Hire bike for tours and expeditions. Rental bike for office commute. Motorcycle rentals for long weekends. Rent a motorcycle for special occasions and celebrate. Rental motorcycle for anniversary celebrations. Hire a motorcycle to celebrate a promotion. Rental bikes for grand entrance of groom. Bikes on rent for few hours to ride to a exotic dinner. Bike rentals for road trips, weekend leisure and many more.  Rent Royal enfield's Bullet, Classic 350, Thunderbird 350, Standard 500, Thunderbird 500, Desert Storm, Himalayan, Continental GT, Classic 500 and many more.
					                    Rent KTM Duke 200, 390, RC 200, RC 390, Avenger, Benelli, Ducati, Harley Davidson, Ninja, Kawasaki, Triumph and many more. 
					                    We have served riders from from USA, Europe nations like France, Germany, Italy, Spain, Norway, Sweden, Australia, New Zealand, China, Japan, Singapore, Israel and many more. They rent motorcycles to travel across India, Nepal, Bhutan and many other Southeast Asian countries. We provide backup vehicles with support staff for guided tours. We conduct expeditions to Leh, Ladakh, Spiti Valley, Arunachal Pradesh, Goa and many exotic destinations across India. The motorcycle rentals stores are spread across India to provide service and support to travellers. The rental motorcycles come with Insurance and additional spares to support long tours. The rental motorcycles come with zero deposit and easy booking, pick up and return process. The motorcycle rentals has helped travellers to eliminate middle men and explore country as per individual tastes. Our offer great bikes at cheapest rates with good road side assistance across India. The cheap rates will not affect the quality of service but helps to serve more customers through rental services.
					                    Royal Enfield for rent in Bangalore.. Royal Enfield rentals in Bangalore. KTM Duke for rent in Bangalore. KTM Duke rentals in Bangalore. Best and cheapest bike rentals in Bangalore. Avenger for rent in Bangalore
					                
					                    Bike rentals in Bangalore now at cheapest rates at various locations across Bangalore. Bike rentals in Bangalore from Wicked Ride is most preferred and highly rated by users. Bike rentals in Bangalore was started by Wicked Ride and now serves at 8 locations across Bangalore.

					                    Wicked Ride provides riding gears for rent. The rental gears can be availed during pick up or during booking with help of sales team. You can rent Rynox Jackets, DSG Jackets for rent. You can also rent Rynox gloves and DSG Gloves from Wicked Ride. You can also rent many other riding gears from Wicked Ride. The rental riding gears are available across all stores. The riding gears can be bought at <a href="http://store.wickedride.in">store.wickedride.com</a>

					                    Wicked Ride advice use of riding gears for safety and comfort during rides. The customers often rent riding gears from Wicked Ride for Leh Ladakh tours, Nepal tours, Bhutan tours and various other tours.</p>
						                
						            </div>

						            <ul class="down-links">
						            	<li><a href="/bangalore">rent a bike in Bangalore</a></li>
						                <li><a href="/jaipur">rent a bike in Jaipur</a></li>
						                <li><a href="/udaipur">rent a bike in Udaipur</a></li>
						                <li><a href="/mysore">rent a bike in Mysore</a></li>
						                <li><a href="/bhuj">rent a bike in Bhuj</a></li>
						                <li><a href="/ahmedabad">rent a bike in Ahmedabad</a></li>
						                <li><a href="/hampi">rent a bike in Hampi</a></li>
						                <li><a href="/belagavi">rent a bike in Belagvi</a></li>
						                <li><a href="/jaisalmer">rent a bike in Jaisalmer</a></li>
						                <li><a href="/delhi">rent a bike in Delhi</a></li>
						                <li><a href="http://bangalore.wickedride.com">Bike Rentals in Bangalore</a></li>
						                <li><a href="http://delhi.wickedride.com">Bike Rentals in Delhi</a></li>
						                <li><a href="http://jaipur.wickedride.com">Bike Rentals in Jaipur</a></li>
						            </ul>

						            <div class="clear_both"></div>
			        				<div class="copy_rights"> 
			        					<h5> Use of this site constitutes acceptance of our <a href="/privacy">Privacy Policy</a>. Copyright 2015 WickedRide adventure services pvt. ltd. </h5>
			        				</div>
								</div>	
							</div>
						</div>
				 	
				</div>
			</div>
            
            <ul class="social-icons icon-circle icon-zoom list-unstyled list-inline"> 
              <li><a href="https://www.facebook.com/wickedrideindia"><i class="fa fa-facebook"></i></a></li> 
              <li><a href="https://www.instagram.com/wickedride/"><i class="fa fa-instagram"></i></a> </li>   
              <li> <a href="https://twitter.com/WickedRideIndia"><i class="fa fa-twitter"></i></a> </li>
              <li> <a href="https://itunes.apple.com/us/app/wicked-ride/id1121704128?ls=1&mt=8"><i class="fa fa-apple"></i></a> </li> 
              <li> <a href="https://play.google.com/store/apps/details?id=com.wickedride.app"><i class="fa fa-android"></i></a> </li> 
            </ul>
		</section>

	<!-- END FOOTER SECTION -->




{!! Minify::javascript(array('/home/js/jquery-1.10.2.min.js', '/home/js/bootstrap.js', '/home/js/TweenMax.min.js', '/home/js/jquery.scrollmagic.min.js', '/home/js/extra.slider.js', '/home/js/t.js', '/home/js/app.js', '/home/js/front.js', '/home/js/jquery.datetimepicker.full.js', '/home/js/main-old.js', '/home/js/main.js')) !!}


<!--

<script type="text/javascript" src="home/js/jquery-1.10.2.min.js"></script> 
 <script src="home/js/bootstrap.js"></script>

<script type="text/javascript" src="home/js/TweenMax.min.js"></script>
<script type="text/javascript" src="home/js/jquery.scrollmagic.min.js"></script>
<script type="text/javascript" src="home/js/extra.slider.js"></script>
<script type="text/javascript" src="home/js/t.js"></script>
<script type="text/javascript" src="home/js/app.js"></script>
<script type="text/javascript" src="home/js/front.js?a=1"></script>
<script type="text/javascript" src="home/js/jquery.datetimepicker.full.js"></script>


<script src="home/js/main-old.js"></script> 
<script src="home/js/main.js?3"></script> 
-->


<script type="text/javascript">
	Main.init();
</script>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<!--footer ends here-->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>


<!-- Google Analytics New Starts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68504157-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics New Ends -->


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"07e1m1a8FRh27i", domain:"wickedride.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=07e1m1a8FRh27i" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5641b134e014ea2c3cae03c8/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->

<script type="text/javascript">
	
	$(document).ready(function() {
    $('.map').click(function () {
        $('.map iframe').css("pointer-events", "auto");
    });
    
    $( ".map" ).mouseleave(function() {
      $('.map iframe').css("pointer-events", "none"); 
    });
 });                  
</script>


            

<script type="text/javascript">

(function() {

    var quotes = $(".quotes");
    var quoteIndex = -1;

    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(5000)
            .fadeOut(2000, showNextQuote);
    }

    showNextQuote();

})();
</script>
