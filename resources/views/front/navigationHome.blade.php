@inject('cityObj','App\Models\City')
@inject('bmObj','App\Models\BikeModel')

<?php 
	/*$mod = $bmObj->getUniqueBikeModels(1);
	echo "<pre>";
	print_r($mod);
	echo "</pre>";*/
?>

<style>
    .blinking{
        animation:blinkingText 0.8s infinite;
    }
    @keyframes blinkingText{
        0%{		color: #000;	}
        49%{	color: #000000;	}
        50%{	color: #efc08c;	}
        99%{	color: #ebb172;	}
        100%{	color: #f2a654;	}
    }
</style>
<div style="display:none">
    
<!-- Google Code for leads Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 932065471;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "qMizCJu_hmQQv-G4vAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/932065471/?label=qMizCJu_hmQQv-G4vAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


</div>

<!-- navigation starts here-->
 <div id="nav" class="navigation_main">
 	<div class="container"><div class="logo hidden-xs"><a href="/"><img src="/img/logo.png" width="<?php echo env('LOGOWIDTH'); ?>"></a></div></div>
 	<div class="first_nav hidden-xs">
 	 <div class="container">
 		<ul>
            <li><a href="/biker-plan">Biker Plans</a></li>
 			<!-- <li><a href="http://store.wickedride.in/" target="_blank">accessories store</a></li>
 			<li><a href="/list-my-bike">list your bike</a></li> -->
            <!-- <li><a href="https://blog.wickedride.com/" target="_blank">blog</a></li> -->
            <li><a href="/contact-us">contact us</a></li>
            <li><a href="/about-us">about us</a></li>
            <!-- <li><a href="http://services.wickedride.com/" target="_blank">Services</a></li> -->
 			                       
            @if (isset(Auth::user()->first_name))                        
            <li id="logout-li">
                <a href="/front-logout"> Logout</a>
                <span class="name" style="text-transform:capitalize;font-size:12px;">Hello, <a href="/edituser">{!! Auth::user()->first_name !!}</a></span>
                <?php 
                    $user_id = Auth::user()->id;
                    Session::put('user_id', $user_id);
                ?>
            </li>		                        
		    @else                       
		        <li><a href="/login">login</a></li>
		    @endif 
		     
 			 
 		</ul>
 	 </div>
 	</div>
 	<div class="second_nav">
 		<div class="container pad-left-right">
 			
 			<ul>
	 			<li><span><a class="nav_open" href="">our fleet</a></span><div class="plus_icon"></div></li>
                <li><a href="/trip"class="blinking">Bikation</a></li>
	 		    <li><a href="/wpp">Earn with us</a></li>
	 			<li><a href="/tariff">tariff</a></li>
	 			<li><a href="/faqs">faqs</a></li>
	 			<li><img width="25px" style="margin-left:10px;" src="https://bounceshare.com/images/bounce-logo-green.png"><a href="https://bounceshare.com" target="_blank">bounce - rent scooters</a></li>
                
	 			<!-- <li><a href="/booking/select-date">test booking</a></li> -->  <!-- test booking link -->	
 			</ul>
 			<div class="city_number">
	 			<div class="celect_city">
	 				<form method="post" action="/fetch-city">
			 			<label>
						    <select name="select_activeCity" onchange="this.form.submit()">
						    	<!-- Getting value stored in session variable "current_cityId" -->
						    							    	  
						         <?php 
						    		//Session::put('current_cityId', 1);
						    		$session_cityId = Session::get('current_cityId'); 
						    		/*if(isset($session_cityId))
						    			$session_cityId = Session::get('current_cityId');
						    		else
						    			$session_cityId = "1";*/
						    	?>
						    	<?php //var_dump($session_cityId);?>  
						        <?php $cities = $cityObj->getActiveCities(); ?>
	 			 					@foreach($cities as $city)
	 			 						<option value="{{ $city->id }}" 
	 			 							<?php 
	 			 								
	 			 									if($city->id == $session_cityId) echo "selected"; 
	 			 		
	 			 							?> 
	 			 						>
	 			 							{{ $city->city }}
	 			 						</option>
	 			 					@endforeach		
	 		                </select>
						</label>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		 			</form>
		 		</div>
	 			<div class="booking_number">
                    
	 				 <a href="/booking/select-date"> <h1 class="item h1-clear-book">Book A Bike</h1> </a>
                    
	 			</div>
 			</div>
 		</div>
 	</div>


		 	<!-- our fleet starts here-->
		<div id="subnav" class="our_fleet_sec second_nav_open" style="display:none">
				<h4>our fleet</h4> 
				<ul id="navbikes" class="fleet_bikes">
                    <?php 
					$city_id = Session::get('current_cityId');
        			if(!$city_id)
            			$city_id = "1";
				    ?>
                    <?php  $bikeModels = $bmObj->getUniqueBikeModels($city_id); ?>
					@foreach($bikeModels as $bikeModel)
                    
                        <li class="animated">
                        <?php $image = App\Models\Image::find($bikeModel->thumbnail_img_id); ?>                        <!-- <a href="/bike/"> -->
                            <a href="/booking/select-date?model_id={{$bikeModel->model_id}}">
                                <div class="item">
                                    <img src="{{ $image['full'] }}">
                                </div>
                                <div class="clear_both"></div>
                                <div class="itemh common_btn_small"><span></span>{{ $bikeModel->bike_model }}</div>
                            </a>
                        </li>
					@endforeach					
						
										
				</ul>
		</div>
		<!-- our fleet ends here-->


		<!-- wraper starts her-->
 <div id="wrapper">

  <input type="checkbox" id="menu" name="menu" class="menu-checkbox">
  <div class="menu">
    <label class="menu-toggle" for="menu"><span>Toggle</span></label>
    <ul>
    <li><a href="/trip" target="_blank">Tours</a></li>
	
   	  	<li>
			<label for="menu-4">Motorcycle <i class="fa fa-chevron-right orange" aria-hidden="true"></i></label>
			<input type="checkbox" id="menu-4" name="menu-4" class="menu-checkbox">
			<div class="menu">
				<label class="menu-toggle" for="menu-4"><span>Toggle</span></label>
				<ul class="bike_margin fleet_bikes">
					<?php  $bikeModels = $bmObj->getUniqueBikeModels($city_id); ?>
					@foreach($bikeModels as $bikeModel)
                        <li>
                                <?php $image = App\Models\Image::find($bikeModel->thumbnail_img_id); ?>
                                <!-- <a href="/bike/{{-- $bikeModel->bike_model --}}"> -->
                                <a href="/booking/select-date?model_id={{$bikeModel->model_id}}">
                                    <div>
                                        <img src="{{ $image['full'] }}">
                                    </div>
                                    <div class="clear_both"></div>							
                                    <div class="common_btn_small"><span></span>{{ $bikeModel->bike_model }}</div>
                                </a>
                        </li>
					 @endforeach          
				</ul>
			</div>
		</li>
        <li><a href="/wpp">Earn with us</a></li>
        <!--<li><a href="http://store.wickedride.in/">accessories store</a></li>
   	     <li><a href="/listing.php">list your bike</a></li> -->
   	  	<li><a href="/about-us">about us</a></li>
   	  	<li><a href="/contact-us">contact us</a></li>
		<li><a href="/booking/select-date">book a bike</a></li>
		<li><a href="/tariff">tariff</a></li>
        <li><a href="/reviews">testimonials</a></li>
        <li><a href="/faqs">faqs</a></li> 
        <li><a href="https://blog.wickedride.com/">blog</a></li>
        <li><img width="25px" style="margin-left:10px;" src="https://bounceshare.com/images/bounce-logo-green.png"><a href="https://bounceshare.com/" target="_blank">bounce share - rent scooters</a></li>
             

        @if (isset(Auth::user()->first_name))                        
	        <li>	        	
	           	<a href="/front-logout"> Logout</a>
                <a href="/edituser">My Account</a>
	            <?php 
	            	$user_id = Auth::user()->id;
	            	Session::put('user_id', $user_id);
	            ?>
	        </li>		                        
	    @else                       
	        <li><a href="/login">login</a></li>
	    @endif 
	     
    </ul>
  </div>
</div>

<!-- #wrapper -->

 </div>
 <!-- navigation ends here-->
