@extends('front/frontMaster')

@section('title')
	<title>Error Page</title>
@stop

@section('content')
	<div class="clear_both"></div>
	<div class="our_fleet_sec" style="height:200px;">
		<h1 class="h1-clear">{{ $message	}}</h1>
	</div>
	<div class="clear_both"></div>
@stop	