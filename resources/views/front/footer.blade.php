<!-- footer starts here -->
<div class="footer_main">
	<div class="width_960">
		@if (count($errors->subscribe) > 0)
			<div class="alert alert-danger subscribe-err-msg">
				<ul>
					@foreach ($errors->subscribe->all() as $error)
						<li>* {{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if (session('subscribeSuccessMsg'))
    		<div class="alert alert-success subscribe-success-msg">
        		{{ session('subscribeSuccessMsg') }}
    		</div>
		@endif
		<form method="post" action="/email-subscribe/add" name="subscription">
			<div class="subscribe">
				<input type="text" value="" name="email" placeholder="Enter Email to subscribe">
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="common_btn_big"><span></span><input class="submit" type="submit" value="submit"></div>
		</form>

		<div class="footer_nav">
 			<ul>
	 			<li><a href="/booking/select-date">motorcycles</a></li>
	 			<!-- <li><a href="#">bikation</a></li> -->
	 			<li><a href="/reviews">testimonials</a></li>
	 			<li><a href="/faqs">faqs</a></li>
                 <li><a href="/about-us">about us</a></li>
                <li><a href="/<?php
                    $city_id = Session::get('current_cityId');
                    if(!$city_id)
            	       $city_id = "1";
                                               if ($city_id==10)
                                               {
                                                  echo "termsandconditionsDelhi"; 
                                               }
                                               if ($city_id==12)
                                               {
                                                  echo "termsandconditionsNZ"; 
                                               }
                                               else
                                               {
                                                   echo "termsandconditions"; 
                                               }
                                               
                                               ?>">terms and conditions</a></li>
	 			@if (isset(Auth::user()->first_name))                        
			        <li id="logout-li">
			           	<a href="/front-logout"> Logout</a>
			            <?php 
			            	$user_id = Auth::user()->id;
			            	Session::put('user_id', $user_id);
			            ?>
			        </li>		                        
			    @else                       
			        <li><a href="/login">login</a></li>
			    @endif 
 			</ul> 
	 	</div>
        <div class="footer_nav">
            
 			<ul>
	 			<li><a href="/bangalore">rent a bike in Bangalore</a></li>
                <li><a href="/jaipur">rent a bike in Jaipur</a></li>
                <li><a href="/udaipur">rent a bike in Udaipur</a></li>
                <li><a href="/mysore">rent a bike in Mysore</a></li>
                <li><a href="/bhuj">rent a bike in Bhuj</a></li>
                <li><a href="/ahmedabad">rent a bike in Ahmedabad</a></li>
                <li><a href="/hampi">rent a bike in Hampi</a></li>
                <li><a href="/belagavi">rent a bike in Belagvi</a></li>
                <li><a href="/jaisalmer">rent a bike in Jaisalmer</a></li>
                <li><a href="/delhi">rent a bike in Delhi</a></li>
                <li><a href="http://bangalore.wickedride.com">Bike Rentals in Bangalore</a></li>
                <li><a href="http://delhi.wickedride.com">Bike Rentals in Delhi</a></li>
                <li><a href="http://jaipur.wickedride.com">Bike Rentals in Jaipur</a></li>

 			</ul> 
	 	</div>
	 	<div class="clear_both"></div>
        <div class="copy_rights">Use of this site constitutes acceptance of our <a href="/privacy">Privacy Policy</a>. Copyright 2015 WickedRide adventure services pvt. ltd.</div>
	</div>
</div>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollmagic.min.js"></script>
<script type="text/javascript" src="/js/extra.slider.js"></script>
<script type="text/javascript" src="/js/t.js"></script>
<!-- <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="js/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js"></script> -->
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/front.js?a=3"></script>
<script type="text/javascript" src="/js/jquery.datetimepicker.full.js"></script>
<script type="text/javascript" src="/js/multiple-select.js"></script>

<!-- tab js-->
<script src="/js/main-old.js"></script> <!-- Resource jQuery -->
<script src="/js/main.js?v=3"></script> <!-- Resource jQuery -->
<script src="/js/ChooseDate.js?v=9"></script>
<script src="/js/ChooseBike.js?v=10"></script>
<script src="/js/filter.js?v=4"></script>
<!-- tab js-->
<script type="text/javascript">
	Main.init();
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo env('GOOGLEMAPSAPI');?>"></script>
<!--footer ends here-->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!--  Analytics  Old-->
<script type="text/javascript">
    /* var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-51735917-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/
</script>
<!--  End Analytics Old-->  

<!-- Google Analytics New Starts -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68504157-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics New Ends -->


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"07e1m1a8FRh27i", domain:"wickedride.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=07e1m1a8FRh27i" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5641b134e014ea2c3cae03c8/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript">
    $(function() {
        $('.enquire-now').click(function(e)
        {
            $('#form-error').hide();
            $('#form-success').hide();
            $('#enquirysubmit').show();
            $('#enquire-form #enquiremessage').val('');
            $('#enquirysubmit').prop('disabled', false);
            $('#form-error').html('&nbsp; The form is not valid. ');
            //console.log(window.test=e);
            bikeModel= e.target.attributes['data-modelname'].value;
            startDate= e.target.attributes['data-startdate'].value;
            endDate= e.target.attributes['data-enddate'].value;
            startTime= e.target.attributes['data-starttime'].value;
            endTime= e.target.attributes['data-endtime'].value;
            $('#enquire-form #bikemodel').attr('value',bikeModel);
            $('#enquire-form #startdate').attr('value',startDate);
            $('#enquire-form #enddate').attr('value',endDate);
            $('#enquire-form #starttime').attr('value',startTime);
            $('#enquire-form #endtime').attr('value',endTime);

            e.preventDefault();
            $("#dialog").data('bikemodel','testmodel').dialog({
                modal: true,
                //height: 500,
                //width: 600
                width: 'auto',
                autoResize:true,
                resizable: true,
                position: {
                    my: "center",
                    at: "center",
                    of: window
                }

            });
        });


    });

    /* form validation plugin */
    $.fn.goValidate = function() {
        var $form = this,
                $inputs = $form.find('input:text');

        $inputs.on('shown.bs.popover', function () {
            $(this).data("shown",true);
        });

        $inputs.on('hidden.bs.popover', function () {
            $(this).data("shown",false);
        });
        var valid = false;
        var message = '';

        $form.submit(function(e) {
            var valid = false;
            var message = '';
            e.preventDefault();
            $('#form-error').html('&nbsp; The form is not valid. ');

            var formData = {
                'first_name'        : $('input[name=first_name]').val(),
                'last_name'         : $('input[name=last_name]').val(),
                'email'             : $('input[id=email]').val(),
                'phone'             : $('input[name=phone]').val(),
                'message'           : $('textarea[name=message]').val(),
                'bikemodel'         : $('input[name=bikemodel]').val(),
                'startdate'         : $('input[name=startDate]').val(),
                'enddate'           : $('input[name=endDate]').val(),
                'starttime'         : $('input[name=startTime]').val(),
                'endtime'           : $('input[name=endTime]').val()
            };
//            console.log($('input[id=email]').val());
//            console.log($('#email').val());
            console.log(formData);
            //if(formData['phone'].match(/\d/g).length!==10)
            //if(formData['phone'].match('^(\+[\d]{1,3}|0)?[7-9]\d{9}$'))
            if((formData['phone'].match('^([0]|\\+)?\\d{10}'))==null)
            {
                message = 'Phone Number Error';
            }
            else{
                valid=true;
            }

            if (valid==false) { /* form is not valid */

                e.preventDefault();
                $('#form-error').append('<br />&nbsp;Enter Correct Phone Number').show();
            }
            else{
                console.log('form is valid');
                e.preventDefault();
             $.ajax({
                    url: "/enquiry-form/add",
                    method:"POST",
                    data:formData,
                    success: function(result){

                        if(result=='success')
                        {
                            $('#form-error').css('display','none');
                            $('#form-success').css('display','block');
                            $('#enquirysubmit').prop('disabled', true);
                            $('#enquirysubmit').hide();
                            setTimeout(function()
                            {

                                // code to close the modal
                                $('#dialog').dialog('close');

                            }, 3000);

                        }
                        else{
                            $('#form-error').css('display','block');
                        }

                    }});
            }





        });
        return this;
    };

    $('#enquire-form').goValidate();



</script>



