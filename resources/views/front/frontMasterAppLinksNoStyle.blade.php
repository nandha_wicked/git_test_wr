<!DOCTYPE html>
<html>
    <head>        
        @include('front/headerAppLinksNoStyle')

    </head>
    <body>
        <!-- <div id="preloader"></div> -->

        <div class="main">            

            @yield('content')

        </div>

      
    </body>
</html>
