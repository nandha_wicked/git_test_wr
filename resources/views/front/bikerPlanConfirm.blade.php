@inject('priceObj','App\Models\Bike')

@extends('front/frontMasterHome')

@section('content')
<div class="clear_both"></div>

<div class="row top-buffer"></div>

<!-- booking main starts here-->
<div class="book_bike_main">
	
		<!-- width 960 starts here-->

        <div id="steps">		
                <div id="step4" style="display: block;">

                    <div id="booking-details">

                        <div class="login_register_main">
                           
                                <div class="width_960">
                                    <form method="post" action="/paymentBikerPlanRedirect">

                                        <div class="happy_customers row vertical-align">
                                            <div class="col-lg-3"> 
                                              <img class="img-responsive" src="{{ session ('planImage') }}" alt="{{ session ('planName') }}">  
                                            </div>
                                            <div class="col-lg-4"> 
                                                <div>
                                                <h4>{{ session ('planName') }}</h4>
                                                <?php echo(session('planLongDescription'));?>
                                                </div>
                                            </div>

                                            <div class="col-lg-5">
                                                <div class="book_now book_bike_fleet item">
                                                    <div class="bike_price">
                                                        <div class="small_caps">Amount Payable</div>

                                                        <div class="price amt">Rs {{ session ('planCost') }}</div>
                                                    </div>
                                                    <div class="common_btn_small">

                                                        <div class="step4-check "><input class="check" type="checkbox" name="age" value="ageyes"><label> I am over 21</label></div>
                                                        <div class="step4-check "><input class="check" type="checkbox" name="licence" value="licenceyes"><label> I have valid Driving Licence</label></div>
                                                        <div class="step4-check  step4-check2"><input class="check" type="checkbox" name="tandc" value="tandcyes"><label> I agree to <a style="float:none; font-size:10px;" href= "/termsandconditions" target="_blank">Terms & Conditions</a>.</label></div>
                                                        
                                                        <div class="payment-btn-container">

                                                            <button type="submit" id="validate-step4-check">Continue To Payment</button>
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                        </div>

                                                        <div class="clear_both"></div>

                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                
                                </div>
                        </div>

                    </div>
                </div>
        </div>
	
	  	 

</div>
<!-- booking main ends here-->
<!--CACHE-->

<script src="/js/jquery.js"></script>
<script src="/js/jquery.datetimepicker.js"></script>
<script>

	$('#validate-step4-check').click(function(){

		//var note=$('textarea[name=booking_note]').val();
		//$('.book_bike_main #step4 input[name=note]').val(note);

		var test1=$('#step4 form input[name=age]:checked').val();
		var test2=$('#step4 form input[name=licence]:checked').val();
		var test3=$('#step4 form input[name=tandc]:checked').val();
					//alert(test1 + test2);

		if(test1 == "ageyes" && test2 == "licenceyes" && test3 == "tandcyes"){
			return true;
		}
		else{
			alert("Please tick the checkboxes to continue.");
			return false;
		}
	});

	// APPLY PROMOCODE
	$('#apply-promocode-btn').click(function(){
		WickedRide.applyPromocode();
	});

</script>

<div class="clear_both"></div>
	
@stop	
