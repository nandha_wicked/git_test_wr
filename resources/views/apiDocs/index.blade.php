@extends('api_docs_layout')

@section('body')
<body class="index" data-languages="[{{$data_languages_string}}]">

    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="images/navbar.png" />
      </span>
    </a>
    <div class="tocify-wrapper">
      <img src="images/logo.png" />
        <div class="lang-selector">
            @foreach($languages as $language)
              <a href="#" data-language-name="{{$language->language}}">{{$language->language}}</a>
            @endforeach
        </div>
        <div class="search">
          <input type="text" class="search" id="input-search" placeholder="Search">
        </div>
        <ul class="search-results"></ul>
      <div id="toc">
      </div>
        <ul class="toc-footer">
            
        </ul>
    </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
       
          
    @foreach($apiCategories as $apiCategory)    
          

        <!--Left side part -->
        <h1 id="{{ $apiCategory['category_id'] }}">{{ $apiCategory['category'] }}</h1>

        @foreach($apiCategory['apis'] as $api)
          
          
          
            <h2 id="{{ $api['call_id'] }}">{{ $api['call_title'] }}</h2>
          
            <!--Right side part -->
            
            @foreach($api['responses'] as $response)

                <blockquote>{!! $response->description !!}</blockquote>
          <blockquote><div class="response">{!! $response->response !!}</div></blockquote>
            
            @endforeach
          
            

            <!-- Middle Part -->
            <p><code class="prettyprint">{{ $api['call_method'] }} {{ $api['call_url'] }}</code></p>
          
            <p>{!! $api['call_description'] !!}</p>
          
            @foreach($api['messages'] as $message)
                @if($message['type'] == "blue")
                    <aside class="notice">{!! $message['message'] !!}</aside>
                @elseif($message['type'] == "red")
                    <aside class="warning">{!! $message['message'] !!}</aside>
                @else
                    <aside class="success">{!! $message['message'] !!}</aside>
                @endif
            @endforeach
          
            @foreach($api['tables'] as $table)
                <h3 id="{{ $table['title_id'] }}">{{ $table->title }}</h3>
                <table>
                    <thead>
                        <tr>
                            <th>{{ $table->left_title }}</th>
                            <th>{{ $table->right_title }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($table['table_cells'] as $tablecell)
                            <tr>
                                <td>{{ $tablecell->title }}</td>
                                <td>{!! $tablecell->description !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach
          
            @if(count($api['arguments'])>0)
          
                <h3 id="{{ $api['call_id'] }}-argument">Arguments for the API call</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($api['arguments'] as $argument)
                        <tr>
                            @if($argument['child_argument'])
                                <td style="padding-left:{{ $argument['child_argument_offset'] }}px;"> &rdsh;
                            @else
                                <td>
                            @endif
                                    {{ $argument['argument'] }}
                                <br/>
                                @if($argument['required_or_optional'] == "Required")
                                    <p style="color:orange;">Required</p>
                                @else
                                    <p style="color:grey;">Optional</p>
                                @endif
                            </td>
                            <td>
                                 ({{ $argument['type'] }}) - {!! $argument['description'] !!}
                            </td>
                        </tr>
                        

                        @endforeach
                        
                        
                    </tbody>
                </table>
            @else
                <h3 id="{{ $api['call_id'] }}-argument">No Arguments required for the API call</h3>
            @endif

        @endforeach

    @endforeach
    
          
    
          
             
     
          
       

      </div>
      <div class="dark-box">
          <div class="lang-selector">
                @foreach($languages as $language)
                    <a href="#" data-language-name="{{$language->language}}">{{$language->language}}</a>
                @endforeach
                
          </div>
      </div>
    </div>
  </body>
@stop


