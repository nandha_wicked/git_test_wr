@extends('admin_layout')
@section('content')


<form method="get" action="/test327e5gbach5FFD" class="form-horizontal" name="add-call" id="add-call"> 
        <div class="form-group">
            <fieldset>
                <div id= "argument-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_argument_btn" type = "button">Add Argument</button>
                </div>
            </fieldset>
        </div>
        <div class="form-group">
            <fieldset>
                <div id= "box-field-set">
                </div>
                <br/>
                <div class="form-group">
                    <button id="add_blue_box_btn" type = "button">Add Info Box</button>
                </div>
                <div class="form-group">
                    <button id="add_red_box_btn" type = "button">Add Alert Box</button>
                </div>
                <div class="form-group">
                    <button id="add_green_box_btn" type = "button">Add Success Box</button>
                </div>

                
            </fieldset>
        </div>
        
        <input type="submit" class="btn btn-primary" value="Add Call" />
    
</form>
       



    
    
<script>
    var i = 0;
    
    $(document).ready(function(){

                

        $("#add_argument_btn").click(function(){

            
            $("#argument-field-set").append(
                "<br/>"+
                "<fieldset id=\"argument"+window.i+"\">"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Name : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Description : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_description["+window.i+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Type : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<input type=\"text\" name=\"argument_type["+window.i+"]\" class=\"form-control\" />"+
                        "</div>"+
                    "</div>"+
                    "<div class=\"form-group\">"+
                        "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                        "<div class=\"col-md-7\">"+
                            "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" class=\"form-control\">"+
                                "<option value=\"Required\">Required</option>"+
                                "<option value=\"Optional\">Optional</option>"+
                            "</select>"+
                        "</div>"+
                    "</div>"+
                    "<br/>"+
                    "<div class=\"form-group\">"+
                    "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                        "<br/>"+ 
                    "</div>"+
                    "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+window.i+"\">"+
                    "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
                "</fieldset>"
            
            );
            
            window.i++;
            
            
            
            
            
        });
        

    });

    function addChildArgument(argumentId){

        console.log(window.i);
                
        $("#child-argument-fieldset-"+argumentId).append(
            "<br/>"+
            "<fieldset id=\"argument"+window.i+"\">"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Name : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_name["+window.i+"]\" id = \"argument-input-"+window.i+"\" onkeyup= 'changeBtnText(\""+window.i+"\")' class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Description : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_description["+window.i+"]\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Type : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<input type=\"text\" name=\"argument_type["+window.i+"]\" class=\"form-control\" />"+
                    "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                    "<label class=\"col-md-5\">Argument Required/Optional : </label>"+
                    "<div class=\"col-md-7\">"+
                        "<select type=\"text\" name=\"argument_required_or_optional["+window.i+"]\" class=\"form-control\">"+
                            "<option value=\"Required\">Required</option>"+
                            "<option value=\"Optional\">Optional</option>"+
                        "</select>"+
                    "</div>"+
                "</div>"+
                "<br/>"+
                "<div class=\"form-group\">"+
                "<div id = \"child-argument-fieldset-"+window.i+"\"></div>"+
                    "<br/>"+ 
                "</div>"+
                "<input type=\"hidden\" name=\"parent_argument["+window.i+"]\" value=\""+argumentId+"\">"+
                "<button type=\"button\" id=\"button-"+window.i+"\" onClick='addChildArgument(\""+window.i+"\")'>Add Child Argument in this argument</button>"+
            "</fieldset>"

        );

        window.i++;
        
        
    }
    
    function changeBtnText(buttonId){
        
        var btn_text = "Add Child Argument for ";
        btn_text += $("#argument-input-"+buttonId).val();
        
        $("#button-"+buttonId).text(btn_text);
        
    }
    
    
</script>





@stop