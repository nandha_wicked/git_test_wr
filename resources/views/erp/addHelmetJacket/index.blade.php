@extends('erp_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Helmet/Jacket Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addHelmetJacketModal"><i class="fa fa-plus"></i> Add Helmet/Jacket</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					
					<th>Helmets</th>
                    <th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				@foreach($helmetList as $helmet)
					<tr>
						
						<td>{{ $helmet->item_id }}</td>
						
						
						<td>
							<button class="btn btn-danger wk-btn helmetJacket_delete_btn" data-id="{{ $helmet->id }}" data-item="helmet"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
        
        <table class="wk-table">
			<thead>
				<tr>
					
					<th>Jackets</th>
                    <th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				@foreach($jacketList as $jacket)
					<tr>
						
						<td>{{ $jacket->item_id }}</td>
						
						
						<td>
							<button class="btn btn-danger wk-btn helmetJacket_delete_btn" data-id="{{ $jacket->id }}" data-item="jacket"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.helmetJacket_delete_btn').click(function(){
			var id = $(this).data('id');
            var item = $(this).data('item');
			$('#deleteHelmetJacket-modal form').attr("action","/erp/addHelmetJacket/delete/"+id);
            $('#deleteHelmetJacket-modal form input[name=item]').val(item);
			$('#deleteHelmetJacket-modal').modal('show');
		});
	</script>

	<script type="text/javascript">
		$(function(){
            helmetJacket.init();
	    });
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="deleteHelmetJacket-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Helmet/Jacket </h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Helmet/Jacket?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="item" value="">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addHelmetJacketModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/addHelmetJacket/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add Helmet Jacket</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						<div class="form-group">
							<label class="col-md-3">Helmet/Jacket:</label>
							<div class="col-md-9">
		        				<select name="item" class="form-control">
		        					
		        		            <option value="helmet">Helmet</option>
                                    <option value="jacket">Jacket</option>
		      
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Item ID:</label>
							<div class="col-md-9">
		        				<input type="text" name="item_id" class="form-control"/>
		        			</div>
		        		</div>
                        
                        
                        

		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



@stop
