@extends('erp_layout')

@section('content')
	
	<div class="esubscribe-pg">
		<h2 class="title">Bike Numbers Homepage</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addBikeNumberModal"><i class="fa fa-plus"></i> Add Bike Number</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					
					<th>Bike</th>
                    <th>Model</th>
                    <th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				@foreach($bikeList as $bike)
					<tr>
						
						<td>{{ $bike->number }}</td>
                        <td>{{ $bike->getModelName() }}</td>
						
						
						<td>
							<button class="btn btn-danger wk-btn bikeNumber_delete_btn" data-id="{{ $bike->id }}"><i class="fa fa-trash-o"></i> Delete </button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script type="text/javascript">
		$('.bikeNumber_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBikeNumber-modal form').attr("action","/erp/addBikeNumbers/delete/"+id);
			$('#deleteBikeNumber-modal').modal('show');
		});
	</script>

	<script type="text/javascript">
		$(function(){
            bikeNumbers.init();
	    });
	</script>

@stop

@section('model')
	
	<div class="modal fade" id="deleteBikeNumber-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Bike Number</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this bike number?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



	<div class="modal fade" id="addBikeNumberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/addBikeNumbers/add" class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Add BikeNumber</h4>
		      		</div>
		      		<div class="modal-body">
		      			
						<div class="form-group">
							<label class="col-md-3">State:</label>
							<div class="col-md-9">
		        				<select name="stateCode" class="form-control">
		        					
		        		            <option value="KA">KA</option>
                                    <option value="GJ">GJ</option>
                                    <option value="RJ">RJ</option>
                                    <option value="DL">DL</option>
		        					
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Area Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="areaCode" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Series Letters:</label>
							<div class="col-md-9">
		        				<input type="text" name="seriesCode" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Number:</label>
							<div class="col-md-9">
		        				<input type="text" name="number" class="form-control"/>
		        			</div>
		        		</div>
		        		
						
		        		<div class="form-group">
							<label class="col-md-3">Model:</label>
							<div class="col-md-9">
		        				<select name="model_id" class="form-control">
		        					@foreach($bikeModels as $bikeModel)
		        						<option value="{{ $bikeModel->id }}">{{ $bikeModel->bike_model }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>	
                        
                        

		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>



@stop
