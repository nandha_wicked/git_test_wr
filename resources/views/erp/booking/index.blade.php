@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Booking Homepage</h2>
		
        
        
             <p>&nbsp;</p>

     
                    <form id="custom-search-form" method="get" action="/erp/booking/search" class="form-search form-horizontal pull-left">
                        <div class="input-append span12">
                            <input type="text" name = "id" class="search-query mac-style" placeholder="Search">
                            <button type="submit" class="btn" style="background-image: url(../editer/lib/img/glyphicons-halflings.png); width: 14px; height: 14px; background-position: -48px 0;"></button>
                        </div>
                    </form>
           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        @if($count>0)
		<table class="fixedCol wk-table" style="table-layout: fixed; width:1980px">
			<thead>
				<tr>
					<th style="width: 50px !important;">Id</th>
					<th style="width: 80px !important;">Start Date</th>
					<th style="width: 60px !important;">Start Time</th>
					<th style="width: 80px !important;">End Date</th>
					<th style="width: 60px !important;">End Time</th>
					<th style="width: 140px !important;">Customer</th>
					<th style="width: 110px !important;">Bike</th>
					<th style="width: 100px !important;">Area</th>
					<th style="width: 300px !important;">Customer Instruction</th>
					<th style="width: 80px !important;">Amount Paid</th>
                    <th style="width: 100px !important;">CreatedBy</th>
                    <th style="width: 80px !important;">Payment ID</th>
                    <th style="width: 100px !important;">Promocode</th>
                    <th style="width: 300px !important;">Notes for Operations</th>
                    <th style="width: 120px !important;">Notes Acknowledged By</th>
                    <th style="width: 120px !important;">Notes Acknowledged At</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['id'] }}</td>
						<td>{{ $booking['start_date'] }}</td>
						<td>{{ $booking['start_time'] }}</td>
						<td>{{ $booking['end_date'] }}</td>
						<td>{{ $booking['end_time'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserEmail($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
                        <td>{{ $booking->order_id }}</td>
                        <td>{{ $booking->promocode }}</td>
                        <td>{{$booking->backend_notes}}</td>
                        <td>{{$booking->getBackEndNotesAckByEmail()}}</td>
                        <td>{{$booking->getBackEndNotesAckAt()}}</td>
                        
                </tr>
				@endforeach
			</tbody>
		</table>
        @endif
        
        
        <form method="post" action="/erp/booking/add" onsubmit="Backend.Booking.validateBookingForm();" class="form-horizontal" name="add-booking-inPage" id="add-booking">
		      		<div class="">
		        		
		        		<h4 class="modal-title" id="myModalLabel">ADD Booking</h4>
		      		</div>
		      	<div>	
						<div class="inPageForm">
							<label class="col-md-3">Start Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="startDate" class="form-control datepicker" id="startDate" placeholder="Start Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="startTime" class="form-control datepicker" id="startTime" placeholder="Start Time"/>
		        			</div>
		        		</div>
		        		<div class="inPageForm">
							<label class="col-md-3">End Date & Time:</label>
							<div class="col-md-5">
		        				<input type="text" name="endDate" class="form-control datepicker" id="endDate" placeholder="End Date"/>
		        			</div>
							<div class="col-md-3">
		        				<input type="text" name="endTime" class="form-control datepicker" id="endTime" placeholder="End Time"/>
		        			</div>
		        		</div>
		        		<div id="date-error"></div>
                    <div class = "postDateForm hidden" > 
                        <div class="inPageForm">
							<label class="col-md-3">Area:</label>
		        			<div class="col-md-9">
		        				<select name="area" class="form-control area_select">
                                    <option disabled selected value> -- Select an Area -- </option>
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="inPageForm">
							<label class="col-md-3">Delivery Area:</label>
							<div class="col-md-9">
		        				<select name="delivery_area" class="form-control">
                                    <option value="same" selected> --- Same as above ---</option>
		        					@foreach($areas as $area)
		        						<option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
		        		<div class="inPageForm">
							<label class="col-md-3">Model:</label>
							<div class="col-md-9">
                                <select name="model" class="form-control model_select">
                                    <option disabled selected value> -- Select a Model -- </option>
		        				    @foreach($models as $model)
		        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
		        		            @endforeach
                                </select>
		        			</div>
		        			
		        		</div>
                        
		        		<div class="inPageForm hidden">
							<label class="col-md-3">Customer:</label>
							<div class="col-md-9">
		        				<select name="customer" id ="customer" class="form-control">
		        						<option value="<?php echo env('RESERVATIONSID');?>" selected>reservations@wickedride.com</option>
		        				</select>
		        			</div>
                        </div>
                        <div class="inPageForm" name="customer_email_div" >
							<label class="col-md-3">Customer Email:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_email" class="form-control" />
		        			</div>
		        		</div>
                        <div class="inPageForm" name="customer_mobile_div" >
							<label class="col-md-3">Customer Mobile:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_mobile" class="form-control" />
		        			</div>
		        		</div>
                        <div class="inPageForm" name="customer_fname_div" >
							<label class="col-md-3">Customer First Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_fname" class="form-control" />
		        			</div>
		        		</div>
                        <div class="inPageForm" name="customer_lname_div" >
							<label class="col-md-3">Customer Last Name:</label>
							<div class="col-md-9">
		        				<input type="text" name="customer_lname" class="form-control" />
		        			</div>
		        		</div>
                        
		        		<div class="inPageForm">
							<label class="col-md-3">Customer Instruction:</label>
							<div class="col-md-9">
		        				<textarea name="customer_note" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
		        		<div class="inPageForm">
							<label class="col-md-3">Promo Code:</label>
							<div class="col-md-9">
		        				<input type="text" name="coupon" class="form-control"/>
		        			</div>
		        		</div>
                        <div class="inPageForm">
							<label class="col-md-3">Price With Tax:</label>
							<div class="col-md-9">
		        				<input type="text" name="amount" class="form-control" disabled/>
		        			</div>
		        		</div>
                        <div id="payment_box">
                        </div>
                        <button type="button" id="add_payment_box">Add Payment Info</button>	
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="source" value="ERP">
                        <input type="hidden" name="selected" value="">
		      		
		      		<div id="date-error"></div>
            
            </div>
            <div class="clear_both"></div>
		      		<div align="center">
		      			<input style="width:200px" type="submit" class="btn btn-primary" value="Add" />
		        		
		      		</div>
                </div>
		    	</form>
        
        
        
        
	</div>

	<script type="text/javascript">
		$('.booking_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteBooking-modal form').attr("action","/admin/booking/delete/"+id);
			$('#deleteBooking-modal').modal('show');
		});
        
	</script>
    
    <script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/admin/updateNotes/bookings/backend_notes/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>


    <script type="text/javascript">
        $('.area_select').change(function(){
            
            
            var selected = $('input[name="selected"]').val();
            if(selected == "model")
            {
            }
            else
            {
            
                var startDate = $('input[name="startDate"]').val();
                var startTime = $('input[name="startTime"]').val();
                var endDate = $('input[name="endDate"]').val();
                var endTime = $('input[name="endTime"]').val();
                var areaId = $( "select[name='area'] option:selected").val();
                var options = $('.model_select');
                options.empty().append("<option disabled selected value> -- Select a Model -- </option>");


                 $.ajax({
                                type: "GET",
                                url: "/api/getModelPricesForDates",
                                data: { 
                                'start_date': startDate,
                                'start_time': startTime,
                                'end_date': endDate,
                                'end_time': endTime,
                                'area_id': areaId
                                },
                                cache: false,
                                success: function(data) {
                                    var models = data['result']['data'];

                                    var options = $('.model_select');
                                    $.each(models, function() {
                                        options.append($("<option />").val(this.model_id).text(this.model_name+" ---  ₹"+this.model_price));
                                    });
                                }
                });
                
                $('input[name="selected"]').val("area");
            }
        });
        
        
        $('.model_select').change(function(){
            
            
            var selected = $('input[name="selected"]').val();
            if(selected == "area")
            {
            }
            else
            {
            
                var startDate = $('input[name="startDate"]').val();
                var startTime = $('input[name="startTime"]').val();
                var endDate = $('input[name="endDate"]').val();
                var endTime = $('input[name="endTime"]').val();
                var modelId = $( "select[name='model'] option:selected").val();
                var options = $('.area_select');
                options.empty().append("<option disabled selected value> -- Select an Area -- </option>");


                 $.ajax({
                                type: "GET",
                                url: "/api/getAreaPricesForDates",
                                data: { 
                                'start_date': startDate,
                                'start_time': startTime,
                                'end_date': endDate,
                                'end_time': endTime,
                                'model_id': modelId
                                },
                                cache: false,
                                success: function(data) {
                                    var models = data['result']['data'];

                                    var options = $('.area_select');
                                    $.each(models, function() {
                                        options.append($("<option />").val(this.area_id).text(this.area_name+" ---  ₹"+this.area_price));
                                    });
                                }
                });
                
                $('input[name="selected"]').val("model");
            }
        });
            
            
        $('.datepicker').change(function(){  
            
            var startDate = $('input[name="startDate"]').val();
            var startTime = $('input[name="startTime"]').val();
            var endDate = $('input[name="endDate"]').val();
            var endTime = $('input[name="endTime"]').val();
            if(startDate&&startTime&&endDate&&endTime)
            {
                $('.postDateForm').removeClass("hidden");
            }
        });
        
    </script>

    <script type="text/javascript">
        $('#add_payment_box').click(function(){
            $('#payment_box').append("<fieldset style=\"padding: 15px;margin: 10px;border: 2px solid lightgrey;\"><div class=\"inPageForm\"><label class=\"col-md-3\">Amount Paid:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"amount[]\" class=\"form-control\" /> </div></div> <div class=\"inPageForm\"><label class=\"col-md-3\">Payment Mode:</label><div class=\"col-md-9\">	<select name=\"payment_method[]\" id =\"payment_method\" class=\"form-control\"> @foreach($paymentMethods as $paymentMethod)<option value=\"{{$paymentMethod->id}}\">{{$paymentMethod->method}}</option>@endforeach </select>	  </div>	</div> <div class=\"inPageForm\"><label class=\"col-md-3\">Payment ID:</label><div class=\"col-md-9\">	<input type=\"text\" name=\"payment_id[]\" class=\"form-control\" /></div> </div></fieldset>");
        });
        
    </script>
    
	
				
	    	
@stop

@section('model')


	<div class="modal fade" id="deleteBooking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action name="booking_delete_form" id="booking_delete_form">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Booking</h4>
		      		</div>
                    
		      		<div class="modal-body">
		        		Are you sure, you want to delete this booking?<br>
		        		If "Yes" press "Delete" else "Close". Also select reason for deleting.
                        
                        <select name="reason" class="form-control" id="booking_delete_form_select">
                            <option value="not_selected" disabled selected>-- Select Reason --</option>
                            <option value="error">Error in Booking</option>
                            <option value="user_cancelled">User Cancelled</option>
                            <option value="wicked_ride_cancelled">Wicked Ride Cancelled</option>
                        </select>
                        <p id="booking_delete_form_error" style="color:red;"></p>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="button" class="btn btn-primary booking_delete_form_btn"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<script type="text/javascript">
		$(function(){
			Backend.Booking.init();
			
		});
	</script>
		
    <script type="text/javascript">
     $('.booking_delete_form_btn').click(function(){

                e = document.getElementById("booking_delete_form_select");
                var reason = e.options[e.selectedIndex].text;
            
                if(reason == "-- Select Reason --")
                {
                    $("#booking_delete_form_error").html("Select a reason");        
                }
                else
                {
                    $('#booking_delete_form').submit();
                    $('#cancel-delivery').submit();
                }

        });

    </script>
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


@stop	
