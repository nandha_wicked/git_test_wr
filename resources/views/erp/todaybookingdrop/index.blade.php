@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Today Drop Off</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/erp/todaybookingdrop">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
                    @foreach($activeAreas as $area)
                        <option value="{{ $area->id }}" <?php if($area->id == $selected) echo "selected"?>>{{ $area->area }}</option>
                    @endforeach
                    @if($allowAllAreas)
                            <option value="0" <?php if($selected==0) echo "selected"?>>All Areas</option>
                    @endif
		  		</select>
		  	</div>
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
		  	<input type="hidden" name="areaName"/>
            
            <!-- Rounded switch -->
            <div style="padding-top:10px;">
            <label class="switch">
              <input type="checkbox" name="pending">
              <div class="slider round"></div>
            </label>
            <div style="font-size:15px; font-weight: 700; margin-top:-10px;">Show Pending Returns</div>
            
            </div>
		</form>

        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:1610px">
			<thead>
				<tr>
                    <th style="width: 100px !important;">Action</th>
					<th style="width: 90px !important;">Id</th>
                    <th style="width: 140px !important;">Customer</th>
					<th style="width: 110px !important;">Bike</th>
                    <th style="width: 120px !important;">Bike Assigned</th>
					<th style="width: 80px !important;">Duration</th>
					<th style="width: 80px !important;">End Date</th>
                    <th style="width: 80px !important;">Start Date</th>
                    <th style="width: 120px !important;">Area</th>
					<th style="width: 100px !important;">KM Limit</th>
					<th style="width: 300px !important;">Customer Instruction</th>
					<th style="width: 80px !important;">Amount Paid</th>
                    <th style="width: 110px !important;">CreatedBy</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
                        <td>
                     
                            
                            
                            @if($booking->is_returned==0)
                                <button class="btn btn-primary booking_return_btn"  data-id="{{$booking->id}}" bookingid="{{$booking->id}}" data-bookingref="{{$booking->reference_id}}" data-startdate="{{$booking->start_date}}" data-starttime="{{$booking->start_time}}" data-enddate="{{$booking->end_date}}" data-endtime="{{$booking->end_time}}" data-area="{{$booking->getDeliveredAt()}}" data-priceperhour="{{$booking['price_per_hour']}}" data-extrakmprice="{{$booking['extra_km_price']}}" data-startodo="{{$booking->start_odo}}" data-accessorieslabel="{{$booking['accessories_label']}}" data-deliverynotes ="{{$booking->delivery_notes}}" data-username="{{$booking->getUserName($booking->user_id)}}" data-modelname="{{$booking->getBikeModelNameThis()}}" data-bikenumber="{{$booking->getBikeNumber()}}" data-kmlimit="{{$booking->KMlimit}}">Return</button>
                            @else
                            
                                <button class="btn btn-success booking_return_edit_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}"  data-booking_customer_edit = "{{ $booking->getUserName($booking->user_id) }}"><i class="fa fa-edit"></i> Edit </button>
                            
                            @endif
                           
                        </td>
						<td>{{ $booking['reference_id'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserMobileNum($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                        <td>{{ $booking->getBikeNumber() }}</td>
						<td>{{ $booking->getDuration() }}</td>
                        <td>{{ $booking->getReadableDateTime($booking->end_datetime) }}</td>
                        <td>{{ $booking->getReadableDateTime($booking->start_datetime) }}</td>
                        <td>{{ $booking->getAreaNameThis() }}</td>
						<td>{{ $booking->KMlimit }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
						
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	 <script type="text/javascript">
		$(function(){
			Return.init();
	    });
	</script>

@stop

@section('model')
	<div class="modal fade" id="editReturnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="edit-return" id="edit-return">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><div id="editModalHeading"></div></h4>
		      		</div>
		      		<div class="modal-body">
                        
                        
                        <div class="form-group">
                            <div id="return_detail_label_edit"></div>
                        </div>
                        
                        <div class="form-group">
							<label class="col-md-3">Start ODO:</label>
							<div class="col-md-9">
		        				<div id="return_start_odo_label_edit"></div>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">End ODO:</label>
							<div class="col-md-9">
		        				<input type="number" name="end_odo_edit" class="form-control end_odo_edit" value ="0"/>
		        			</div>
		        		</div>
                        <div class="extra-km-section-edit hidden" style="margin: 20px; padding: 10px; border-width: thin; border-style: solid;">
                            <div class="form-group">
                                <label class="col-md-3">Extra KM Charges:</label>
                                <div class="col-md-9">
                                    <label class="extra_km_charges" name="extra_km_charges" id="extra_km_charges_edit"></label>
                                </div>
                            </div>
                        </div>
                        
                        
                       <div class="form-group">
							<label class="col-md-3">Accessories Provided:</label>
							<div class="col-md-9">
		        				<div id="return_accessories_label_edit"></div>
		        			</div>
		        		</div> 
                        
                        <div class="form-group">
							<label class="col-md-3">Bike Recieved in Good Condition:</label>
							<div class="col-md-9">
                                
                                <div style="padding-top:10px;">
                                    
                                    <label class="switch">
                                      <input type="checkbox" name="bike_received_edit" id="bike_received_edit" class ="bike_received_edit">
                                      <div class="slider round"></div>
                                    </label>
                                    <label style="position: absolute; margin-top: 10px; margin-left: 5px;">YES
                                    </label>
                                </div>
                            </div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Accesories Recieved in Good Condition:</label>
							<div class="col-md-9">
                                
                                <div style="padding-top:10px;">
                                    
                                    <label class="switch">
                                      <input type="checkbox" name="accessories_received_edit" id="accessories_received_edit" class="accessories_received_edit">
                                      <div class="slider round"></div>
                                    </label>
                                    <label style="position: absolute; margin-top: 10px; margin-left: 5px;">YES
                                    </label>
                                </div>
                            </div>
		        		</div>
                        
                        <div class="form-group problem_notes_section_edit">
							<label class="col-md-3">Problem Notes:</label>
							<div class="col-md-9">
		        				<textarea type="text" name="return_problem_notes_edit" class="form-control" value ="" placeholder="Enter details of the problem."></textarea>
		        			</div>
		        		</div>
                       
                        <div class="extra-money-section-edit" style="margin: 20px; padding: 10px; border-width: thin; border-style: solid;">
                        
                            <div class="form-group">
                                <label class="col-md-3">Extra Money:</label>
                                <div class="col-md-9">
                                    <input type="number" name="extra_money_edit" class="form-control" value ="0"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3">Collection Mode:</label>
                                <div class="col-md-9">
                                    <select name="collection_mode_edit" id="collection_mode_edit" class="form-control">
                                        <option value = "Cash" selected> Cash </option>
                                        <option value="Card">Card</option>

                                    </select>
                                </div>
                            </div>
                            
                        </div>

                            <div class="form-group">
                                <label class="col-md-3">Notes:</label>
                                <div class="col-md-9">
                                    <textarea type="text" name="return_notes_edit" class="form-control" value =""></textarea>
                                </div>
                            </div>
                        
                        
                        
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="booking_ref_edit">
                        <input type="hidden" name="booking_customer_edit">
                        <input type="hidden" name="price_per_hour_edit">
                        <input type="hidden" name="extra_km_price_edit">
                        <input type="hidden" name="km_limit_edit">
                        
		      		</div>
		      		<div class="modal-footer">
                        
		      			<button type="button" class="btn btn-primary booking_confirm_return_edit_btn" ><i class="fa fa-plus"></i> Return</button>
                        
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
</div>
                        
                        
<div class="modal fade" id="addReturnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="add-return" id="add-return">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><div id="addModalHeading"></div></h4>
		      		</div>
		      		<div class="modal-body">
                        
                        <div class="form-group">
                            <div id="return_detail_label_add"></div>
                        </div>
                        
                        <div class="form-group">
							<label class="col-md-3">Start ODO:</label>
							<div class="col-md-9">
		        				<div id="return_start_odo_label_add"></div>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">End ODO:</label>
							<div class="col-md-9">
		        				<input type="number" name="end_odo_add" class="form-control end_odo_add" value ="0"/>
		        			</div>
		        		</div>
                        <div class="extra-km-section-add hidden" style="margin: 20px; padding: 10px; border-width: thin; border-style: solid;">
                            <div class="form-group">
                                <label class="col-md-3">Extra KM Charges:</label>
                                <div class="col-md-9">
                                    <label class="extra_km_charges" name="extra_km_charges" id="extra_km_charges_add"></label>
                                </div>
                            </div>
                        </div>
                        
                        
                       <div class="form-group">
							<label class="col-md-3">Accessories Provided:</label>
							<div class="col-md-9">
		        				<div id="return_accessories_label_add"></div>
		        			</div>
		        		</div> 
                        
                        <div class="form-group">
							<label class="col-md-3">Bike Recieved in Good Condition:</label>
							<div class="col-md-9">
                                
                                <div style="padding-top:10px;">
                                    
                                    <label class="switch">
                                      <input type="checkbox" name="bike_received_add" id="bike_received_add" class ="bike_received_add">
                                      <div class="slider round"></div>
                                    </label>
                                    <label style="position: absolute; margin-top: 10px; margin-left: 5px;">YES
                                    </label>
                                </div>
                            </div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Accesories Recieved in Good Condition:</label>
							<div class="col-md-9">
                                
                                <div style="padding-top:10px;">
                                    
                                    <label class="switch">
                                      <input type="checkbox" name="accessories_received_add" id="accessories_received_add" class="accessories_received_add">
                                      <div class="slider round"></div>
                                    </label>
                                    <label style="position: absolute; margin-top: 10px; margin-left: 5px;">YES
                                    </label>
                                </div>
                            </div>
		        		</div>
                        
                        <div class="form-group problem_notes_section_add">
							<label class="col-md-3">Problem Notes:</label>
							<div class="col-md-9">
		        				<textarea type="text" name="return_problem_notes_add" class="form-control" value ="" placeholder="Enter details of the problem."></textarea>
		        			</div>
		        		</div>
                       
                        <div class="extra-money-section-add" style="margin: 20px; padding: 10px; border-width: thin; border-style: solid;">
                        
                            <div class="form-group">
                                <label class="col-md-3">Extra Money:</label>
                                <div class="col-md-9">
                                    <input type="number" name="extra_money_add" class="form-control" value ="0"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3">Collection Mode:</label>
                                <div class="col-md-9">
                                    <select name="collection_mode_add" id="collection_mode_add" class="form-control">
                                        <option value = "Cash" selected> Cash </option>
                                        <option value="Card">Card</option>

                                    </select>
                                </div>
                            </div>
                            
                        </div>

                            <div class="form-group">
                                <label class="col-md-3">Notes:</label>
                                <div class="col-md-9">
                                    <textarea type="text" name="return_notes_add" class="form-control" value =""></textarea>
                                </div>
                            </div>
                        
                        
                        
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="booking_ref_add">
                        <input type="hidden" name="booking_customer_add">
                        <input type="hidden" name="price_per_hour_add">
                        <input type="hidden" name="extra_km_price_add">
                        <input type="hidden" name="km_limit_add">
                        
		      		</div>
		      		<div class="modal-footer">
                        
		      			<button type="button" class="btn btn-primary booking_confirm_return_btn" ><i class="fa fa-plus"></i> Return</button>
                        
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
                    
                    
		    	</form>
	    	</div>
	  	</div>
</div>

<div class="modal fade" id="confirmReturnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><div id="confirmHeading"></div></h4>
        </div>
         <div id="confirmModalContent"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_return_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_return_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>


<div class="modal fade" id="confirmReturnEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><div id="confirmEditHeading"></div></h4>
        </div>
         <div id="confirmEditModalContent"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_return_edit_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_return_edit_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>


<div class="modal fade" id="deliveryfailureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Problem with Delivery</h4>
                </div>
                <div class="form-group">
                    <label><p> &nbsp;  The Delivery details are not entered correctly. Please do so and return.</p></label>
                </div>
            </div>
    </div>
</div>

<script>
    //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>   

@stop


