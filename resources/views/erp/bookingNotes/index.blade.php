@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Acknowledge Request</h2>
        
        
             <p>&nbsp;</p>

           
            <p>&nbsp;</p>
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Start Date</th>
					<th>Start Time</th>
					<th>End Date</th>
					<th>End Time</th>
					<th>Customer</th>
					<th>Bike</th>
					<th>Area</th>
                    <th>Notes for Operations</th>
                	<th>Action</th>
                    <th>Acknowledge</th>
                    <th>Notes Acknowledged By</th>
                    <th>Notes Acknowledged At</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking['id'] }}</td>
						<td>{{ $booking['start_date'] }}</td>
						<td>{{ $booking['start_time'] }}</td>
						<td>{{ $booking['end_date'] }}</td>
						<td>{{ $booking['end_time'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
                        <td>{{$booking->backend_notes}}</td>
                        <td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $booking->id }}"
							data-note="{{ $booking->backend_notes }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
                        @if(!$booking->backend_notes_ack_by)
                        <td>
							<button class="btn btn-primary ack_btn" data-id="{{ $booking->id }}"> Acknowledge</button>
						</td>
                        @else
                        <td></td>
                        @endif
                        
                        <td>{{$booking->getBackEndNotesAckByEmail()}}</td>
                        <td>{{$booking->getBackEndNotesAckAt()}}</td>
                        
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.ack_btn').click(function(){
			var id = $(this).data('id');
            $('#ackNotes-modal form').attr("action","/erp/bookingNotes/ack/"+id);
			$('#ackNotes-modal').modal('show');
            
			
		});
	</script>
    
    <script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/erp/updateNotes/bookings/backend_notes/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>

@stop

@section('model')
	

	<div class="modal fade" id="ackNotes-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Acknowledge Customer Request</h4>
		      		</div>
		      		<div class="modal-body">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Acknowldge</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


		
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


@stop	
