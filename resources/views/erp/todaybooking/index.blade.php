@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Today Pick Up</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/erp/todaybooking">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
                    @foreach($activeAreas as $area)
                        <option value="{{ $area->id }}" <?php if($area->id == $selected) echo "selected"?>>{{ $area->area }}</option>
                    @endforeach
                    @if($allowAllAreas)
                        <option value="0" <?php if($selected==0) echo "selected"?>>All Areas</option>
                    @endif
		  		</select>
		  	</div>
            

            
            
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
            
            
            <!-- Rounded switch -->
            <div style="padding-top:10px;">
            <label class="switch">
              <input type="checkbox" name="pending">
              <div class="slider round"></div>
            </label><div style="font-size:15px; font-weight: 700; margin-top:-10px;">Show Pending Deliveries</div>
            
            </div>
		</form>
        
       
        
        

        
		@if(count($errors) > 0)
        
        <script type="text/javascript"> 
            alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
        </script>
			
		@endif
        
        
        <div class="hidden-xs">
            <table class="fixedCol wk-table" style="table-layout: fixed; width:2050px;" >
                <thead>
                    <tr>
                        <th style="width: 100px !important;">Action</th>
                        <th style="width: 90px !important;">Id</th>
                        <th style="width: 140px !important;">Customer</th>
                        <th style="width: 110px !important;">Bike</th>
                        <th style="width: 150px !important;">Bike Assigned</th>
                        <th style="width: 80px !important;">Duration</th>
                        <th style="width: 80px !important;">Start Date</th>
                        <th style="width: 80px !important;">End Date</th>
                        <th style="width: 120px !important;">Area</th>
                        <th style="width: 100px !important;">KM Limit</th>
                        <th style="width: 300px !important;">Customer Instruction</th>
                        <th style="width: 300px !important;">Notes for Operations</th>
                        <th style="width: 300px !important;">Acknowledged By</th>
                        <th style="width: 80px !important;">Amount Paid</th>
                        <th style="width: 110px !important;">CreatedBy</th>
                        <th style="width: 100px !important;">Delivered By</th>
                        <th style="width: 100px !important;">Extra Money</th>
                        <th style="width: 100px !important;">Collection Mode</th>
                        <th style="width: 180px !important;">User Document</th>
                        <th style="width: 100px !important;">Work Email Verified</th>
                        <th style="width: 200px !important;">Document Notes For Operations</th>

                   </tr>
                </thead>
                <tbody>
                    @foreach($bookingsByArea as $booking)
                        <tr>
                            <td>
                                @if($booking->delivery_cancelled)
                                    <p style="color: red; font:bold 15px arial, sans-serif;">Delivery Cancelled</p>
                                    <button class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_add = "{{ $booking->getUserName($booking->user_id)  }}" data-delivered_at_add="{{ $booking->delivered_at_area }}" data-model_id_add="{{ $booking->model_id }}" data-delivered_at_name_add="{{ $booking->getDeliveredAt() }}" data-model_id_name_add="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_add="{{ $booking->getPayLater() }}"><i class="fa fa-plus"></i> Deliver</button>

                                @elseif($booking->is_delivered==0)

                                    <button class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_add = "{{ $booking->getUserName($booking->user_id) }}" data-delivered_at_add="{{ $booking->area_id }}" data-model_id_add="{{ $booking->model_id }}" data-delivered_at_name_add="{{ $booking->getDeliveredAt() }}" data-model_id_name_add="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_add="{{ $booking->getPayLater() }}"><i class="fa fa-plus"></i> Deliver</button>

                                @else

                                    <button class="btn btn-success booking_deliver_edit_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_edit = "{{ $booking->getUserName($booking->user_id) }}" data-delivered_at_edit="{{ $booking->delivered_at_area }}" data-model_id_edit="{{ $booking->model_id }}" data-delivered_at_name_edit="{{ $booking->getAreaName($booking->area_id) }}" data-model_id_name_edit="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_edit="{{ $booking->getPayLater() }}"><i class="fa fa-edit"></i> Edit </button>

                                @endif
                            </td>
                            <td>{{ $booking['reference_id'] }}</td>
                            <td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserMobileNum($booking->user_id) }}</td>
                            <td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                            <td><div id="bikeNumberDiv{{$booking->id}}">
                                @if($booking->bike_number_id != 0)
                                <select class="form-control" data-id="{{$booking->id}}" onClick="onClickBikeNumber({{$booking->id}})"><option value="{{ $booking->bike_number_id }}">{{ $booking->getBikeNumber() }}</option></select>
                                @else
                                <button type="button" class="form-control" onClick="onClickBikeNumber({{$booking->id}})" data-id="{{$booking->id}}">Assign a bike</button>
                                @endif
                                </div></td>
                            <td>{{ $booking->getDuration() }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->start_datetime) }}</td>
                            <td>{{ $booking->getReadableDateTime($booking->end_datetime) }}</td>
                            <td>{{ $booking->getAreaNameThis() }}</td>
                            <td>{{ $booking->KMlimit }}</td>
                            <td>{{ $booking->note }}</td>
                            <td>{{ $booking->backend_notes }}</td>
                            <td>{{ $booking->backend_notes_ack_by }}</td>
                            <td>{{ $booking->amountPaid() }}</td>
                            <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
                            <td>{{ $booking->getDeliveredByEmail($booking->createdBy) }}</td>
                            <td>{{ $booking->extra_money }}</td>
                            <td>{{ $booking->collectedModeText() }}</td>
                            <td>
                                @if(count($booking['user_document'])==0)
                                -- Not Availabe --
                                @else

                                    @foreach($booking['user_document'] as $user_doc)
                                    @if($user_doc->status == 0)
                                    <button class="btn btn-primary user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>

                                    @elseif($user_doc->status == 1)
                                    <button class="btn btn-success user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                    @else
                                    <button class="btn btn-danger user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>


                                    @endif	
                                    @endforeach
                                @endif
                            </td>
                            <td> 
                                @if($booking['work_email_verified'] == "No")
                                    <button class="btn btn-danger " >NO</button>
                                @else
                                    <button class="btn btn-success ">YES</button>
                                @endif
                            </td>
                            <td> {{$booking['user_doc_notes']}} </td>


                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="visible-xs">
            @foreach($bookingsByArea as $booking)
                <div class="card">
                  <div class="card-block">
                        <h4 class="card-title">{{ $booking['reference_id'] }}</h4>

                        <?php $user = $booking->getUser(); ?>  
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $user->first_name }} - {{ $user->mobile_num }}</li>
                            <li class="list-group-item">{{ $booking->getAreaNameThis() }} - {{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</li>
                            <li class="list-group-item">Bike: <div id="bikeNumberDivMobile{{$booking->id}}">
                                @if($booking->bike_number_id != 0)
                                <select class="form-control" data-id="{{$booking->id}}" onClick="onClickBikeNumberMobile({{$booking->id}})"><option value="{{ $booking->bike_number_id }}">{{ $booking->getBikeNumber() }}</option></select>
                                @else
                                <button type="button" class="form-control" onClick="onClickBikeNumberMobile({{$booking->id}})" data-id="{{$booking->id}}">Assign a bike</button>
                                @endif
                                </div></li>
                            <li class="list-group-item">Amount Paid: {{ $booking->total_price }} KM Limit: {{ $booking->KMlimit }}</li>
                            <li class="list-group-item">{{ $booking->getReadableDateTime($booking->start_datetime) }} to {{ $booking->getReadableDateTime($booking->end_datetime) }} Duration: {{ $booking->getDuration() }}</li>
                            <li class="list-group-item">Customer Instruction: {{ $booking->note }}</li>
                            <li class="list-group-item">Notes for Operations: {{ $booking->backend_notes }} Acknowledged By: {{ $booking->backend_notes_ack_by }}</li>
                            <li class="list-group-item">Delivered By: {{ $booking->getDeliveredByEmail() }}</li>
                            <li class="list-group-item"> User Documents:<div>
                                @if(count($booking['user_document'])==0)
                                    -- None Uploaded --
                                @endif
                                    
                                @if(count($booking['user_document'])>0)
                                    @foreach($booking['user_document'] as $user_doc)
                                        @if($user_doc->status == 0)
                                            <button type="button" class="btn btn-primary user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @elseif($user_doc->status == 1)
                                            <button type="button" class="btn btn-success user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}" ><i class="fa fa-check" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @else
                                            <button type="button" class="btn btn-danger user_doc_show_btn" data-userdocument="{{ $user_doc->doc }}" data-doctype="{{ $user_doc->doc_type }}" data-reason="{{ $user_doc->doc_reason }}"   data-docid="{{ $user_doc->id }}"  data-userid="{{ $user_doc->user_id }}" data-status="{{ $user_doc->status }}"><i class="fa fa-close" aria-hidden="true"></i> {{$user_doc->doc_type}}</button><br>
                                        @endif	
                                    @endforeach
                                @endif
                                
                                <form id="saveIDForm{{ $booking->id }}" method="post" enctype="multipart/form-data" action="upload_user_docs"><div id="addIDImage{{ $booking->id }}"></div><input type="hidden" name="id" value="{{ $booking->id }}"><input type="hidden" name="_token" value="{{ csrf_token() }}"></form><div style="text-align:center; margin:5px;"><button type="button" class="btn btn-primary user_doc_add_btn" data-id="{{ $booking->id }}" data-count="0" id="addImageBtn{{ $booking->id }}"><i class="fa fa-plus"></i> Add Image</button>&nbsp;<button type="button" class="btn btn-success user_doc_save_btn" data-id="{{ $booking->id }}" id="saveImageBtn{{ $booking->id }}">Save Images</button></div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Work Email verified : @if($booking['work_email_verified'] == "No")
                                    NO
                                @else
                                    YES
                                @endif
                            </li>
                            <li class="list-group-item">Extra Money: {{ $booking->extra_money }} Collection Mode: {{ $booking->collectedModeText() }}</li>
                        </ul>
                        <input type="hidden" name="id" value="{{ $booking->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if($booking->delivery_cancelled)
                            <p style="color: red; font:bold 15px arial, sans-serif;">Delivery Cancelled</p>
                            <button type="button" class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_add = "{{ $booking->getUserName($booking->user_id)  }}" data-delivered_at_add="{{ $booking->delivered_at_area }}" data-model_id_add="{{ $booking->model_id }}" data-delivered_at_name_add="{{ $booking->getDeliveredAt() }}" data-model_id_name_add="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_add="{{ $booking->getPayLater() }}"><i class="fa fa-plus"></i> Deliver</button>

                        @elseif($booking->is_delivered==0)

                            <button type="button" class="btn btn-primary booking_deliver_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_add = "{{ $booking->getUserName($booking->user_id) }}" data-delivered_at_add="{{ $booking->area_id }}" data-model_id_add="{{ $booking->model_id }}" data-delivered_at_name_add="{{ $booking->getDeliveredAt() }}" data-model_id_name_add="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_add="{{ $booking->getPayLater() }}"><i class="fa fa-plus"></i> Deliver</button>

                        @else

                            <button type="button" class="btn btn-success booking_deliver_edit_btn" data-id="{{ $booking->id }}" data-booking_id="{{ $booking->reference_id }}" data-booking_customer_edit = "{{ $booking->getUserName($booking->user_id) }}" data-delivered_at_edit="{{ $booking->delivered_at_area }}" data-model_id_edit="{{ $booking->model_id }}" data-delivered_at_name_edit="{{ $booking->getAreaName($booking->area_id) }}" data-model_id_name_edit="{{ $booking->getBikeModelName($booking->model_id) }}" data-paylater_edit="{{ $booking->getPayLater() }}"><i class="fa fa-edit"></i> Edit </button>

                        @endif
                  </div>
                </div>
            @endforeach
        </div>
        
        
	</div>

    
    <img hidden id="imgID">

    <script type="text/javascript">
		$(function(){
			Delivery.init();
	    });
	</script>

    <script type="text/javascript">


        $('.user_doc_show_btn').click(function(){
            var  user_document	= $(this).data('userdocument');
            var  status	= $(this).data('status');
            var  doctype	= $(this).data('doctype');
            var  reason	= $(this).data('reason');
            var  docid	= $(this).data('docid');
            var  userid	= $(this).data('userid');

            var  search_user_status	=  $("#seach-modal input[name='user_status']:checked").val();
            var  search_seach_by	= $('#seach-modal select[name=seach_by]').val();
            var  search_txtname	= $('#seach-modal input[name=tx_search]').val();


            //user_document = user_document.replace("//", "");

            //var coverImgHolder = '<img src="http:'+user_document+'" />';
            var coverImgHolder = '<img src="'+user_document+'" width=100%""></img>';
            $('#old_coverImage_holder').html(coverImgHolder);
            $('#user_document_reason').html(reason);
            $('#doc_id').val(docid);
            $('#user_id').val(userid);
            $('#user_doc').val(doctype);

            $('#search_user_status').val(search_user_status);
            $('#search_seach_by').val(search_seach_by);
            $('#search_txtname').val(search_txtname);
            if(status == '1')
                $('#bmose1').prop("checked",true);	
            else
                $('#bmose2').prop("checked",true);
            
            $('#doc_id_delete').val(docid);


            $('#specBikeModel-modal').modal('show');
        });
    </script>

@stop

@section('model')
	<div class="modal fade" id="editDeliveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="edit-delivery" id="edit-delivery">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><div id="editModalHeading"></div></h4>
		      		</div>
		      		<div class="modal-body">
                        
                        <div class="form-group">
							<label class="col-md-3">Delivered At:</label>
							<div class="col-md-9">
		        				<div class="cascade" id="delivered_at_edit_div"></div>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Model & Bike Number:</label>
							<div class="col-md-4">
		        				<div class="cascade" id="delivered_model_id_edit_div"></div>
		        			</div>
                            <div class="col-md-9">
                                <div class="cascade" id="bikeNumbersEdit"></div>
                                <div></div>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Starting Odo:</label>
							<div class="col-md-9">
		        				<input type="number" name="starting_odo_edit" class="form-control"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Route Plan:</label>
							<div class="col-md-9">
		        				<input type="text" name="route_plan_edit" class="form-control"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Helmet 1:</label>
							<div class="col-md-9">
		        				<select name="helmet_1_edit" id="helmet_1_edit" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($helmetList as $helmet)
		        						<option value="{{ $helmet->id }}">{{ $helmet->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Helmet 2:</label>
							<div class="col-md-9">
		        				<select name="helmet_2_edit" id="helmet_2_edit" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($helmetList as $helmet)
		        						<option value="{{ $helmet->id }}">{{ $helmet->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        
                        
                        <div class="form-group">
							<label class="col-md-3">Jacket 1:</label>
							<div class="col-md-9">
		        				<select name="jacket_1_edit" id="jacket_1_edit" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($jacketList as $jacket)
		        						<option value="{{ $jacket->id }}">{{ $jacket->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Jacket 2:</label>
							<div class="col-md-9">
		        				<select name="jacket_2_edit" id="jacket_2_edit" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($jacketList as $jacket)
		        						<option value="{{ $jacket->id }}">{{ $jacket->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        
                        Gloves
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[5]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[5]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[5]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                            
                        
                            Go-Pro
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[2]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[2]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[2]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                            
                                
                                
                            Knee-Guard
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[3]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[3]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[3]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                                    
                                
                            
                                Saddle Bags
                                
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[4]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[4]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[4]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                                
                                
                                Mobile-Holder
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[7]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[7]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[7]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                        </div>                           
                                Tank Bag
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_edit[8]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_edit[8]" class="form-control input-number" value="0" min="0" max="5">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_edit[8]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                        
                        <br/>
                        <br/>
                        
                        
                        <div class="form-group">
							<label class="col-md-3">Extra Money:</label>
							<div class="col-md-9">
		        				<input type="number" name="extra_money_edit" class="form-control" value ="0"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Collection Mode:</label>
							<div class="col-md-9">
		        				<select name="collection_mode_edit" id="collection_mode_edit" class="form-control">
                                    <option value = "Cash" selected> Cash </option>
		        		            <option value="Card">Card</option>
		        					
		        				</select>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Notes:</label>
							<div class="col-md-9">
		        				<textarea type="text" name="delivery_notes_edit" class="form-control" value =""></textarea>
		        			</div>
		        		</div>
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="booking_ref_edit">
                        <input type="hidden" name="booking_customer_edit">
                        
		      		</div>
		      		<div class="modal-footer">
                        <button type="button" class="btn btn-primary booking_cancel_deliver_edit_btn" ><i class="fa fa-times-circle"></i> Did Not Deliver</button>
                        
		      			<button type="button" class="btn btn-primary booking_confirm_deliver_edit_btn" ><i class="fa fa-plus"></i> Deliver</button>
                        
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
</div>
                        
                        
<div class="modal fade" id="addDeliveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="" class="form-horizontal" name="add-delivery" id="add-delivery">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><div id="addModalHeading"></div></h4>
                        <br/>
                        <h5 class="modal-title" id="myModalLabel"><div id="addModalPayLater"></div></h5>
		      		</div>
		      		<div class="modal-body">
                        
                        <div class="form-group">
							<label class="col-md-3">Delivered At:</label>
							<div class="col-md-9">
                                <div id="delivered_at_add_div"></div>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Model & Bike Number:</label>
							<div class="col-md-4">
		        				<div class="cascade" id="delivered_model_id_add_div"></div>
		        			</div>
                            <div class="col-md-9">
                                <div id="bikeNumbersAdd"></div>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Starting Odo:</label>
							<div class="col-md-9">
		        				<input type="number" name="starting_odo_add" class="form-control"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Route Plan:</label>
							<div class="col-md-9">
		        				<input type="text" name="route_plan_add" class="form-control"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Helmet 1:</label>
							<div class="col-md-9">
		        				<select name="helmet_1_add" id="helmet_1_add" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($helmetList as $helmet)
		        						<option value="{{ $helmet->id }}">{{ $helmet->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Helmet 2:</label>
							<div class="col-md-9">
		        				<select name="helmet_2_add" id="helmet_2_add" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($helmetList as $helmet)
		        						<option value="{{ $helmet->id }}">{{ $helmet->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        
                        
                        <div class="form-group">
							<label class="col-md-3">Jacket 1:</label>
							<div class="col-md-9">
		        				<select name="jacket_1_add" id="jacket_1_add" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($jacketList as $jacket)
		        						<option value="{{ $jacket->id }}">{{ $jacket->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        <div class="form-group">
							<label class="col-md-3">Jacket 2:</label>
							<div class="col-md-9">
		        				<select name="jacket_2_add" id="jacket_2_add" class="form-control">
                                    <option value = "0" selected> None </option>
		        					@foreach($jacketList as $jacket)
		        						<option value="{{ $jacket->id }}">{{ $jacket->item_id }}</option>
		        					@endforeach
		        				</select>
		        			</div>
		        		</div>
                        
                        Gloves
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[5]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[5]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[5]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                            
                        
                            Go-Pro
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[2]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[2]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[2]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                            
                                
                                
                            Knee-Guard
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[3]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[3]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[3]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                                    
                                
                            
                                Saddle Bags
                                
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[4]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[4]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[4]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                                
                                
                                Mobile-Holder
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[7]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[7]" class="form-control input-number" value="0" min="0" max="100">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[7]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                        </div>                           
                                Tank Bag
                            
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number"  data-type="minus" data-field="quant_add[8]">
                                    <span class="glyphicon glyphicon-minus"></span>
                                  </button>
                              </span>
                              <input type="text" name="quant_add[8]" class="form-control input-number" value="0" min="0" max="5">
                              <span class="input-group-btn">
                                  <button type="button" class="btn  btn-number" data-type="plus" data-field="quant_add[8]">
                                      <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                              </span>
                          </div>
                        
                        <br/>
                        <br/>
                        
                        
                        <div class="form-group">
							<label class="col-md-3">Extra Money:</label>
							<div class="col-md-9">
		        				<input type="number" name="extra_money_add" class="form-control" value ="0"/>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Collection Mode:</label>
							<div class="col-md-9">
		        				<select name="collection_mode_add" id="collection_mode_add" class="form-control">
                                    <option value = "Cash" selected> Cash </option>
		        		            <option value="Card">Card</option>
		        					
		        				</select>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Notes:</label>
							<div class="col-md-9">
		        				<textarea type="text" name="delivery_notes_add" class="form-control" value =""></textarea>
		        			</div>
		        		</div>
                        
                        <div id="addDeliveryImage"></div>
                        <div style="text-align:center; margin:5px;">
                            <button type="button" class="btn btn-primary delivery_img_add_btn" data-countDeliveryAdd="0" id="addDeliveryImageBtn"><i class="fa fa-plus"></i> Add Bike Image</button>
                        </div>
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="booking_ref_add">
                        <input type="hidden" name="booking_customer_add">
		      		</div>
		      		<div class="modal-footer">
                        <button type="button" class="btn btn-primary booking_cancel_deliver_btn" ><i class="fa fa-times-circle"></i> Did Not Deliver</button>
                        
		      			<button type="button" class="btn btn-primary booking_confirm_deliver_btn" ><i class="fa fa-plus"></i> Deliver</button>
                        
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
                    
                    
		    	</form>
	    	</div>
	  	</div>
</div>

<div class="modal fade" id="confirmDeliveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><div id="confirmHeading"></div></h4>
        </div>
         <div id="confirmModalContent"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_deliver_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_deliver_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>

<div class="modal fade" id="cancelDeliveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Confirm Cancel Delivery</h4>
        </div>
            <form method="post" action="" class="form-horizontal" name="cancel-delivery" id="cancel-delivery">
            <div class="form-group">
                <label class="col-md-1"></label>
                <label class="col-md-3">Cancellation Notes:</label>
                <div class="col-md-7">
                    <textarea name="cancelled_notes" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_cancel_deliver_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_cancel_deliver_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>

<div class="modal fade" id="confirmDeliveryEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><div id="confirmEditHeading"></div></h4>
        </div>
         <div id="confirmEditModalContent"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_deliver_edit_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_deliver_edit_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>

<div class="modal fade" id="cancelDeliveryEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Confirm Cancel Delivery</h4>
        </div>
            <form method="post" action="" class="form-horizontal" name="cancel-edit-delivery" id="cancel-edit-delivery">
            <div class="form-group">
                <label class="col-md-1"></label>
                <label class="col-md-3">Cancellation Notes:</label>
                <div class="col-md-7">
                    <textarea name="cancelled_notes_edit" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
         <div class="modal-footer">
             <button type="button" class="btn btn-primary booking_submit_cancel_deliver_btn"><i class="fa fa-floppy-o"></i> Confirm</button>
             <button type="button" class="btn btn-default booking_close_cancel_deliver_btn" ><i class="fa fa-times-circle"></i> Close</button>
         </div>
      </div>
    </div>
</div>

<div class="modal fade" id="specBikeModel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/users_with_doc/update" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">View Document</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="col-md-12">
						<div class="form-group">
		        			<div class="row">
									<div class="col-md-12">
									<div class="col-md-1"></div>
		        				
				        			<div id="old_coverImage_holder"></div>
				        			</div>
		        			</div>
		        		</div>
						
						<div class="form-group">
							<div class="col-md-12">
		        			<div class="row">
		        			Reason: 
							<textarea type="text" name="user_document_reason" id="user_document_reason" rows="3" style="width:40%"></textarea>
							
		        			</div>
							</div>
		        		</div>
						<div class="form-group">
		        			<div class="row">
							<div class="col-md-12">
		        			Status: 
                                <label class="radio-inline">
								  <input type="radio" name="status" id="bmose1" value="1"> Approve
								</label>
								<label class="radio-inline">
								  <input type="radio" name="status" id="bmose2" value="2"> Not Approve
								</label>
								<label id="requestorStatus">
        						</label>
								</div>
		        			</div>
		        		</div>
		        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="doc_id" id="doc_id" value="">
						<input type="hidden" name="user_id" id="user_id" value="">
						<input type="hidden" name="user_doc" id="user_doc" value="">
						<input type="hidden" name="source" value="erp">
						<input type="hidden" name="search_user_status" id="search_user_status" value="">
						<input type="hidden" name="search_seach_by" id="search_seach_by" value="">
						<input type="hidden" name="search_txtname" id="search_txtname" value="">
						</div>
		      		</div>
		      		<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Update</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
                <form method="post" action="/erp/users_with_doc/delete" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="doc_id" id="doc_id_delete" value="">
						<input type="hidden" name="source" value="erp">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Delete</button>
                    </div>
                </form>
	    	</div>
	  	</div>
	</div>



<script>
    
    var countObj = [];
    
    var countDeliveryAddObj = [];
    
    //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
    

$('.user_doc_add_btn').click(function(){
    var  id	= $(this).data('id');
    
    if(countObj[id] == null)
    {
        countObj[id] = {"count":0};
        var  count	= 0;
    }
    else
    {
        countObj[id].count++;
        var count = countObj[id].count;
    }
    
    
    $('#addIDImage'+id).append("<fieldset style=\"border:1px solid silver\"><label class=\"col-md-3\">Document Type: </label><select name=\"idImageType[]\" style=\"border:1px solid silver; margin:1px;\" class=\"col-md-4\"><option value=\"Passport\">Passport</option><option value=\"DL\">DL</option><option value=\"ID Proof\">ID Proof</option></select><div class=\"col-md-9\"><input name=\"idImageFile"+count+"\" type=\"file\" data-idsuffix=\""+count+"\" data-idbooking=\""+id+"\" id=\"fileUpload"+count+"\"><input name=\"imageBase64"+count+"\" type=\"hidden\" id=\"inputID"+count+"\"/></div></fieldset>");
    
    el('fileUpload'+count).addEventListener("change", readImage, false);
    el('fileUpload'+count).addEventListener("click", function(){
        $('#saveImageBtn'+id).hide(); 
    }, false);
    count++;
    $(this).attr('data-count', count);
    
    $(this).hide();
    
    
    resized = 0;
    
    
});
    
    
    
$('.delivery_img_add_btn').click(function(){
    var  id	= $(this).data('id');
    
    if(countDeliveryAddObj[id] == null)
    {
        countDeliveryAddObj[id] = {"count":0};
        var  countDeliveryAdd	= 0;
    }
    else
    {
        countDeliveryAddObj[id].count++;
        var countDeliveryAdd = countDeliveryAddObj[id].count;
    }
    
    
    $('#addDeliveryImage').append("<fieldset style=\"border:1px solid silver\"><div class=\"col-md-9\"><input name=\"idImageFile"+countDeliveryAdd+"\" type=\"file\" data-idsuffixdeliveryadd=\""+countDeliveryAdd+"\"  id=\"fileUploadDeliveryAdd"+countDeliveryAdd+"\"><input name=\"imageBase64"+countDeliveryAdd+"\" type=\"hidden\" id=\"inputAddDeliveryID"+countDeliveryAdd+"\"/><input name=\"image[]\" type=\"hidden\" value=\""+countDeliveryAdd+"\"/></div></fieldset>");
    
    el('fileUploadDeliveryAdd'+countDeliveryAdd).addEventListener("change", readImageDeliveryAdd, false);
    countDeliveryAdd++;
    $(this).attr('data-countDeliveryAdd', countDeliveryAdd);
    $(this).hide();
    
    
    resizedDeliveryAdd = 0;
    
    
});
    
$('.user_doc_save_btn').click(function(){
        var  id	= $(this).data('id');    
        $('#saveIDForm'+id).submit();
});

$('.input-number').focusin(function(){
   $(this).data('oldvalue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldvalue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldvalue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>   

<script>

    
    function onClickBikeNumber(id){
		                    
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-for-booking",
            data: { 

                'booking_id':id,
                'device':""
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(html) {                
                    $('#bikeNumberDiv'+id).html( html );
                    $('#bikeNumber'+id).focus();
                }
        }); 
    }
	
    
    
    function applyBikeNumberChange(id){

        var bikeNumber = $('#bikeNumber'+id).val();
        
        
        $.ajax({
            type: "POST",
            url: "/api/apply-bike-numbers-for-booking",
            data: { 

                'booking_id':id,
                'bike_number_id':bikeNumber,
                'device':""
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) { 
                
                    
                    if(result.result.data.success)
                    {
                        $('#bikeNumberDiv'+id).html( result.result.data.html );
                    }
                    else
                    {
                        alert(result.result.data.message);
                    }
                    
                }
        });        
        
	};
    
    function focusOutBikeNumber(id){

                
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-html-for-booking",
            data: { 

                'booking_id':id,
                'device':""
                
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) {
                    
                    $('#bikeNumberDiv'+id).html( result );
                    
                }
        });        
        
	};
    
</script>


<script>

    
    function onClickBikeNumberMobile(id){
		                    
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-for-booking",
            data: { 

                'booking_id':id,
                'device':"Mobile"
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDivMobile'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(html) {                
                    $('#bikeNumberDivMobile'+id).html( html );
                    $('#bikeNumberMobile'+id).focus();
                }
        }); 
    }
	
    
    
    function applyBikeNumberChangeMobile(id){

        var bikeNumber = $('#bikeNumberMobile'+id).val();
        
        
        $.ajax({
            type: "POST",
            url: "/api/apply-bike-numbers-for-booking",
            data: { 

                'booking_id':id,
                'bike_number_id':bikeNumber,
                'device':"Mobile"
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDivMobile'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) { 
                
                    
                    if(result.result.data.success)
                    {
                        $('#bikeNumberDivMobile'+id).html( result.result.data.html );
                    }
                    else
                    {
                        alert(result.result.data.message);
                    }
                    
                }
        });        
        
	};
    
    function focusOutBikeNumberMobile(id){

                
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-html-for-booking",
            data: { 

                'booking_id':id,
                'device':"Mobile"
                
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDivMobile'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) {
                    
                    $('#bikeNumberDivMobile'+id).html( result );
                    
                }
        });        
        
	};
    
</script>


<script>
    
    function el(id) {
      return document.getElementById(id);
    } // Get elem by ID

    var imageObj = el("imgID");
    var idsuffix;
    var idbooking;
    var resized = 0;
    
    var imageObjDeliveryAdd = el("imgID");
    var idsuffixDeliveryAdd;
    var resizedDeliveryAdd = 0;

    function readImage() {
        
      if (this.files && this.files[0]) {
        idsuffix = $(this).data('idsuffix');
        idbooking = $(this).data('idbooking');    
        var FR = new FileReader();
        FR.onload = function(e) {
            imageObj.src = e.target.result;
            imageObj.onload = function()
            {
                if(resized == 0)
                {
                    resized = 1;
                    
                    if(imageObjDeliveryAdd.width > 300)
                    {
                        var HERMITE = new Hermite_class();
                        //default resize
                        HERMITE.resize_image('imgID', 300, 300*imageObj.height/imageObj.width, 100, true, function(){
                            $('#inputID'+idsuffix).val($("#imgID").attr('src'));
                            $('#addImageBtn'+idbooking).show();
                            $('#saveImageBtn'+idbooking).show();
                        });
                    }
                    else
                    {
                        $('#inputID'+idsuffix).val($("#imgID").attr('src'));
                        $('#addImageBtn'+idbooking).show();
                        $('#saveImageBtn'+idbooking).show();
                    }
                    
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    
    function readImageDeliveryAdd() {
      if (this.files && this.files[0]) {
        idsuffixDeliveryAdd = $(this).data('idsuffixdeliveryadd');
        var FR = new FileReader();
        FR.onload = function(e) {
            imageObjDeliveryAdd.src = e.target.result;  
            imageObjDeliveryAdd.onload = function()
            {
                if(resizedDeliveryAdd == 0  && imageObjDeliveryAdd.width > 300)
                {
                    resizedDeliveryAdd = 1;
                    
                    if(imageObjDeliveryAdd.width > 300)
                    {
                        var HERMITE = new Hermite_class();
                        //default resize
                        HERMITE.resize_image('imgID', 300, 300*imageObjDeliveryAdd.height/imageObjDeliveryAdd.width, 100, true, function(){
                            $('#inputAddDeliveryID'+idsuffixDeliveryAdd).val($("#imgID").attr('src'));                        
                            $('#addDeliveryImageBtn').show();
                        });
                    }
                    else
                    {
                        $('#addDeliveryImageBtn').show();
                    }
                    
                }
            };
            
        };
        FR.readAsDataURL(this.files[0]);
      }
    }
    
    


</script>

@stop
