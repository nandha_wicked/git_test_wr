@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
	
	
        
             <p>&nbsp;</p>

     
                    
        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
        <form method="post" action="/erp/moveBike/add" class="form-vertical" name="add-booking" id="add-booking">
            
        <h4 class="modal-title" >MOVE BIKE</h4>
            
            
            <p>&nbsp;</p>
            <p>&nbsp;</p>
                <div class="form-group">
                    <label class="col-md-3">Start Date & Time:</label>
                    <div class="col-md-5">
                        <input type="text" name="from_date" class="form-control" id="startDate" placeholder="Start Date"/>
                    </div>
                    
                    <div class="col-md-3">
                        <input type="text" name="from_time" class="form-control" id="startTime" placeholder="Start Time"/>
                    </div>
                    
                </div>
                <p>&nbsp;</p>
                <div class="form-group">
                    <label class="col-md-3">End Date & Time:</label>
                    <div class="col-md-5">
                        <input type="text" name="to_date" class="form-control" id="endDate" placeholder="End Date"/>
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="to_time" class="form-control" id="endTime" placeholder="End Time"/>
                    </div>
                </div>
                <div id="date-error"></div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="form-group">
                    <label class="col-md-3">From Location:</label>
                    <div class="col-md-4">
                        <select name="from_area_id" class="form-control">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="form-group">
                    <label class="col-md-3">To Location:</label>
                    <div class="col-md-4">
                        <select name="to_area_id" class="form-control">
                            @foreach($areaList as $area)
                                <option value="{{ $area['id'] }}" data-city="{{ $area['city_id']}}">{{ $area['area'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="form-group">
                    <label class="col-md-3">Model & Bike Number:</label>
                    <div class="col-md-4">
                        <select name="model_id" class="form-control" id="modelDrop">
                            <option selected disabled> Select Model</option>
                            @foreach($modelList as $model)
                                <option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-9">
                        <div class="cascade" id="bikeNumbers"></div>
                    </div>

                </div>
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            

                <input type="submit" class="btn btn-primary" value="Add" />
            
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
           
        </form>
    </div>
	    	

    <script>
        $(document).ready(function(){
            $("select#modelDrop").change(function(){

                var model_id = $("select#modelDrop option:selected").attr('value');

                    $.ajax({
                        type: "GET",
                        url: "/api/getBikeNumbersForModel",
                        data: { 
                        'model_id': model_id
                           },
                        cache: false,
                        beforeSend: function () {
                        $('#bikeNumbers').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                        },
                        success: function(html) {                
                            $("#bikeNumbers").html( html );
                        }
                    });

            });
        });
    </script>

	<script type="text/javascript">
		$(function(){
			Backend.Booking.init();
			
		});
	</script>


@stop	
