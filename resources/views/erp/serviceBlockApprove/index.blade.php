@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Service Block Add </h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addServiceBlock-modal"><i class="fa fa-plus"></i> Add Service Block</button>
        
        
             <p>&nbsp;</p>
 
		
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
                    <th>Bike Number</th>
                    <th>Area</th>
                    <th>Model</th>
					<th>Start Date Time</th>
					<th>End Date Time</th>
					<th>Notes</th>
                    <th>CreatedBy</th>
                    <th>Approval Status</th>
                	<th>Action</th>
               </tr>
			</thead>
			<tbody>
				@foreach($serviceBlocks as $serviceBlock)
					<tr>
						<td>{{ $serviceBlock['id'] }}</td>
                        <td>{{ $serviceBlock->getBikeNumber() }}</td>
                        <td>{{ $serviceBlock->getAreaName() }}</td>
                        <td>{{ $serviceBlock->getModelName() }}</td>
						<td><?php 
                            $date =  new \DateTime($serviceBlock->start_datetime);
                            $date=$date->format('d-m-Y');
                            echo $date;
                            echo " - ";
                            $time = new \DateTime($serviceBlock->start_datetime);
                            $time = $time->format('ha');
                            echo $time;
                            ?></td>
                        <td><?php 
                            $date =  new \DateTime($serviceBlock->end_datetime);
                            $date=$date->format('d-m-Y');
                            echo $date;
                            echo " - ";
                            $time = new \DateTime($serviceBlock->end_datetime);
                            $time = $time->format('ha');
                            echo $time;
                            ?></td>
                        <td>{{ $serviceBlock['note'] }}</td>
						<td>{{ $serviceBlock->getCreatedByEmail() }}</td>
						<td>{{ $serviceBlock['approval_status'] }}</td>
						<td>
                            <?php
                            if($serviceBlock->approval_status == 'Pending')
                            {
                                echo "<button class=\"btn btn-success wk-btn serviceBlock_approve_btn\" data-id=\"".$serviceBlock->id."\">Approve</button>";
                                
                                echo "<p></p>";

                                echo "<button class=\"btn btn-danger wk-btn serviceBlock_reject_btn\" data-id=\"".$serviceBlock->id."\">Reject</button>";
                            }
                            
                            ?>
						</td>
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.serviceBlock_approve_btn').click(function(){
			var id = $(this).data('id');
			$('#approveServiceBlock-modal form').attr("action","/erp/serviceBlockApprove/approve/"+id);
			$('#approveServiceBlock-modal').modal('show');
		});
	</script>
    
    <script type="text/javascript">
		$('.serviceBlock_reject_btn').click(function(){
			var id = $(this).data('id');
			$('#rejectServiceBlock-modal form').attr("action","/erp/serviceBlockApprove/reject/"+id);
			$('#rejectServiceBlock-modal').modal('show');
		});
	</script>

@stop

@section('model')
	

	<div class="modal fade" id="approveServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Approve Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to approve this Service Block?<br>
		        		If "Yes" press "Approve" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary">Approve</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

    <div class="modal fade" id="rejectServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Reject Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to reject this Service Block?<br>
		        		If "Yes" press "Reject" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary">Reject</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>
    
    
	
@stop	
