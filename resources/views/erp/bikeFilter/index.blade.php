@inject('bikeObj','App\Models\Bike')
@inject('bookingObj','App\Models\Booking')

@extends('erp_layout')

@section('content')

	<div class="bikeFilter-pg">
		<h2 class="title">Bike Availability Filter Homepage</h2>
		<!-- <button class="btn btn-primary" data-toggle="modal" data-target="#addBikeMake-modal"><i class="fa fa-plus"></i> Add BikeMake</button> -->

		<h3>Select:</h3>
		<form class="form-inline">
			<div class="form-group">
		    	<input type="text" name="startDateFilter" class="form-control" id="startDateFilter" placeholder="Start Date" autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="startTimeFilter" class="form-control" id="startTimeFilter" placeholder="Start Time" autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="endDateFilter" class="form-control" id="endDateFilter" placeholder="End Date" autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<input type="text" name="endTimeFilter" class="form-control" id="endTimeFilter" placeholder="End Time" autocomplete="off">
		  	</div>
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
		  		@foreach($activeAreas as $area)
		  			<option value="{{ $area->id }}">{{ $area->area }}</option>
		  		@endforeach
		  		</select>
		  	</div>
		  	<div class="form-group">
		  		<a class="btn btn-success">Filter</a>
		  	</div>
		  	<input type="hidden" name="areaName"/>
		</form>

		<div id="user-details">
			<h3>Showing results for:</h3>
			<ul>
				<li>From date : <span id="bike-filter-from"></span></li>
				<li>To date : <span id="bike-filter-to"></span></li>
			</ul>
		</div>

		<table class="wk-table wk-table1">
			<thead>
				<tr>
					<th>Bike Model</th>
					<th>TotalNo. of Bikes</th>
					<th>No. of Available bikes</th>
					<th>No. of Booked bikes</th>
                    <th>Price with GST</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>

	
	</div>	

<script type="text/javascript">
	$(function() {
		Backend.BikeFilterErp.init();
	});
</script>

<script>
	// $('#filter-form button').click(function(){
	// 	var areaName = $('#filter-form select[name=areaId] option:selected').text();
	// 	$('#filter-form input[name=areaName]').val(areaName);
	// 	console.log($('#filter-form input[name=areaName]').val());
	// });
</script>	

@stop	
