@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Service Blocks</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/erp/service-block">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
		  		@foreach($activeAreas as $area)
		  			<option value="{{ $area->id }}" <?php if($area->id == $selected) echo "selected"?>>{{ $area->area }}</option>
		  		@endforeach
                    <option value="0" <?php if($selected==0) echo "selected"?>>All Areas</option>
		  		</select>
		  	</div>
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
		  	<input type="hidden" name="areaName"/>
		</form>

        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:1290px">
			<thead>
				<tr>
					<th style="width: 110px !important;">Bike Number</th>
					<th style="width: 80px !important;">Start Date</th>
					<th style="width: 80px !important;">End Date</th>
					<th style="width: 80px !important;">Duration</th>
					<th style="width: 100px !important;">Bike</th>
					<th style="width: 100px !important;">Area</th>
					<th style="width: 250px !important;">Customer Instruction</th>
                    <th style="width: 80px !important;">CreatedBy</th>
                    <th style="width: 200px !important;">Notes</th>
                    <th style="width: 80px !important;">Updated By</th>
                    <th style="width: 80px !important;">Booking ID</th>
                    <th style="width: 80px !important;">Edit Note</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookingsByArea as $booking)
					<tr>
						<td>{{ $booking['bike_number'] }}</td>
						<td>{{ $booking->getReadableDateTime($booking['start_datetime']) }}</td>
						<td>{{ $booking->getReadableDateTime($booking['end_datetime']) }}</td>
                        <td>{{ $booking->getDuration() }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking['created_by_email'] }}</td>
                        <td>{{ $booking->backend_notes }}</td>
                        <td>{{ $booking->updated_by }}</td>
                        <td>{{ $booking['reference_id'] }}</td>
						<td>
							<button class="btn btn-info wk-btn note_edit_btn" 
							data-id="{{ $booking->id }}"
							data-note="{{ $booking->backend_notes }}"
							><i class="fa fa-pencil-square-o"></i> Edit</button>
						</td>
						
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

    <script type="text/javascript">
		$('.note_edit_btn').click(function(){
			var id = $(this).data('id');
			var note = $(this).data('note');

			$('#editNote-modal form').attr("action","/erp/update-notes-for-service-block/"+id);
			$('#editNote-modal form input[name=id]').val(id);
			$('#editNote-modal form textarea').val(note);
			
	
			$('#editNote-modal').modal('show');
		});
	</script>

@stop	


@section('model')
	
	
	<div class="modal fade" id="editNote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">EDIT NOTE</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
							<label class="col-md-3">Note:</label>
							<div class="col-md-9">
		        				<textarea class="form-control" name="note" rows="6"></textarea>
		        			</div>
		        		</div>
		        		
		        		<input type="hidden" name="id" class="form-control" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop	
