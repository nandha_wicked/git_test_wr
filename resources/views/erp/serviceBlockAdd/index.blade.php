@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Service Block Add </h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addServiceBlock-modal"><i class="fa fa-plus"></i> Add Service Block</button>
        
        
             <p>&nbsp;</p>
 
		
		<table class="fixedCol wk-table" style="table-layout: fixed; width:1860px">
			<thead>
				<tr>
					<th style="width: 40px !important;">Id</th>
                    <th style="width: 100px !important;">Bike Number</th>
                    <th style="width: 100px !important;">Area</th>
                    <th style="width: 100px !important;">Model</th>
					<th style="width: 85px !important;">Start Date Time</th>
					<th style="width: 85px !important;">End Date Time</th>
					<th style="width: 350px !important;">Notes</th>
                    <th style="width: 150px !important;">CreatedBy</th>
                    <th style="width: 70px !important;">Approval Status</th>
                    <th style="width: 150px !important;">Approved By</th>
                    <th style="width: 70px !important;">Booking Number</th>
                	<th style="width: 90px !important;">Action</th>
                    <th style="width: 150px !important;">Blocked By</th>
                    <th style="width: 85px !important;">Blocked At</th> 
                    <th style="width: 150px !important;">Unblocked By</th>
                    <th style="width: 85px !important;">Unblocked At</th>
               </tr>
			</thead>
			<tbody>
				@foreach($serviceBlocks as $serviceBlock)
					<tr>
						<td>{{ $serviceBlock['id'] }}</td>
                        <td>{{ $serviceBlock->getBikeNumber() }}</td>
                        <td>{{ $serviceBlock->getAreaName() }}</td>
                        <td>{{ $serviceBlock->getModelName() }}</td>
						<td><?php 
                            $date =  new \DateTime($serviceBlock->start_datetime);
                            $date=$date->format('d-m-Y');
                            echo $date;
                            echo " - ";
                            $time = new \DateTime($serviceBlock->start_datetime);
                            $time = $time->format('ha');
                            echo $time;
                            ?></td>
                        <td><?php 
                            $date =  new \DateTime($serviceBlock->end_datetime);
                            $date=$date->format('d-m-Y');
                            echo $date;
                            echo " - ";
                            $time = new \DateTime($serviceBlock->end_datetime);
                            $time = $time->format('ha');
                            echo $time;
                            ?></td>
                        <td>{{ $serviceBlock['note'] }}</td>
						<td>{{ $serviceBlock->getCreatedByEmail() }}</td>
						<td>{{ $serviceBlock['approval_status'] }}</td>
                        <td>{{ $serviceBlock->getApprovedByEmail() }}</td>
						<td>{{ $serviceBlock['booking_id'] }}</td>
						<td>
                            <?php
                            
                            
                            if($serviceBlock->booking_status==1&&$serviceBlock->status==1 && $serviceBlock['isCurrent'] == "yes")
                            {
                                echo "<button class=\"btn btn-danger wk-btn serviceBlock_unblock_btn\" data-id=\"".$serviceBlock->id."\">Unblock</button>";
                            }
                            if($serviceBlock->booking_status==0 && $serviceBlock['isCurrent'] == "yes" && $serviceBlock->approval_status!='Rejected')
                            {
                                echo "<button class=\"btn btn-danger wk-btn serviceBlock_delete_btn\" data-id=\"".$serviceBlock->id."\"><i class=\"fa fa-trash-o\"></i> Delete </button>";
                            }
                            ?>
							
						</td>
                        <td>{{ $serviceBlock->getBookedByEmail() }}</td>
                        <td>{{ $serviceBlock->getBookedAt() }}</td>
                        <td>{{ $serviceBlock->getUnblockedByEmail() }}</td>
                        <td>{{ $serviceBlock->getUnblockedAt() }}</td>
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$('.serviceBlock_delete_btn').click(function(){
			var id = $(this).data('id');
			$('#deleteServiceBlock-modal form').attr("action","/erp/serviceBlock/delete/"+id);
			$('#deleteServiceBlock-modal').modal('show');
		});
	</script>

    <script type="text/javascript">
		$('.serviceBlock_unblock_btn').click(function(){
			var id = $(this).data('id');
			$('#unblockServiceBlock-modal form').attr("action","/erp/serviceBlock/unblock/"+id);
			$('#unblockServiceBlock-modal').modal('show');
		});
	</script>
    <?php
    if(isset($errorStr))
    {
        echo "<script type=\"text/javascript\"> alert(\"".$errorStr."\"); </script>";
    }
    ?>
        

@stop

@section('model')
	
	<div class="modal fade" id="addServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/erp/serviceBlock/add" onsubmit="Backend.ServiceBlock.validateServiceBlockForm();" class="form-horizontal" name="add-serviceBlock" id="add-serviceBlock">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">ADD ServiceBlock</h4>
		      		</div>
		      		<div class="modal-body">
						<div class="form-group">
                            <label class="col-md-5">Start Date & Time:</label>
                            <div class="col-md-7">
                                <input type="text" id="startDateTime" name="start_datetime" class="form-control datepicker" placeholder="Start Date Time"/>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-5">End Date & Time:</label>
                            <div class="col-md-7">
                                <input type="text" id="endDateTime" name="end_datetime" class="form-control datepicker" placeholder="End Date Time"/>
                            </div>

                        </div>
		        		<div id="date-error"></div>
		        		
		        		<div class="form-group">
							<label class="col-md-3">Model & Bike Number:</label>
							<div class="col-md-4">
		        				<select name="model" class="form-control" id="modelDrop">
                                    <option selected disabled> Select Model</option>
		        					@foreach($models as $model)
		        						<option value="{{ $model['id'] }}">{{ $model['bike_model'] }}</option>
		        					@endforeach
		        				</select>
		        			</div>		        			
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Area:</label>
                            <div class="col-md-9">
                                <div class="cascade" id="areasWithModel"></div>
		        			</div>
		        		</div>
                        
                        <div class="form-group">
							<label class="col-md-3">Bike Numbers:</label>
                            <div class="col-md-9">
                                <div class="cascade" id="bikeNumbers"></div>
		        			</div>
		        		</div>
                        
		        		<div class="form-group">
							<label class="col-md-3">Notes:</label>
							<div class="col-md-9">
		        				<textarea name="note" class="form-control" rows="4"></textarea>
		        			</div>
		        		</div>
		        		
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div id="date-error"></div>
		      		<div class="modal-footer">
		      			<input type="submit" class="btn btn-primary" value="Add" />
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>


	<div class="modal fade" id="deleteServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Delete Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to delete this Service Block?<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

    <div class="modal fade" id="unblockServiceBlock-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">Unblock Service Block</h4>
		      		</div>
		      		<div class="modal-body">
		        		Are you sure, you want to unblock this Service Block?<br>
		        		If "Yes" press "Unblock" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Unblock</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	

    <script>
        $(document).ready(function(){
            $("select#modelDrop").change(function(){

                var model_id = $("select#modelDrop option:selected").attr('value');
                      $.ajax({
                        type: "GET",
                        url: "/api/getBikeNumbersForModel",
                        data: { 
						'model_id': model_id
					       },
                        cache: false,
                        beforeSend: function () {
                        $('#bikeNumbers').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                        },
                        success: function(html) {                
                            $("#bikeNumbers").html( html );
                        }
                    });
                
            });
        });
        
        
        $(document).ready(function(){
            $("select#modelDrop").change(function(){

                var model_id = $("select#modelDrop option:selected").attr('value');
                var start_datetime = $("#startDateTime").val();
                var end_datetime = $("#endDateTime").val();
             
                    $.ajax({
                        type: "GET",
                        url: "/api/get-areas-with-models",
                        data: { 
						'model_id': model_id,
                        'start_datetime':start_datetime,
                        'end_datetime':end_datetime    
					       },
                        cache: false,
                        beforeSend: function () {
                        $('#areasWithModel').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                        },
                        success: function(html) {                
                            $("#areasWithModel").html( html );
                        }
                    });
                
            });
        });
    </script>

    <script type="text/javascript">
                $(function () {
                    $('#startDateTime').datetimepicker({
                      format:'Y-m-d H:00',
                      minDate:'2015/09/01'
                    });
                        $('#endDateTime').datetimepicker({
                      format:'Y-m-d H:00',
                      minDate:'2015/09/01'
                    });
                });
    </script>

@stop	
