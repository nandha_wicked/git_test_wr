@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Tomorrow Pick Up</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/erp/tomorrowbooking">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
                    @foreach($activeAreas as $area)
                        <option value="{{ $area->id }}" <?php if($area->id == $selected) echo "selected"?>>{{ $area->area }}</option>
                    @endforeach
                    @if($allowAllAreas)
                            <option value="0" <?php if($selected==0) echo "selected"?>>All Areas</option>
                    @endif
                </select>
		  	</div>
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
		  	<input type="hidden" name="areaName"/>
		</form>

        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="fixedCol wk-table" style="table-layout: fixed; width:2050px;" >
			<thead>
				<tr>
					<th style="width: 90px !important;">Id</th>
					<th style="width: 140px !important;">Customer</th>
					<th style="width: 110px !important;">Bike</th>
                    <th style="width: 150px !important;">Bike Assigned</th>
					<th style="width: 80px !important;">Duration</th>
					<th style="width: 80px !important;">Start Date</th>
                    <th style="width: 80px !important;">End Date</th>
                    <th style="width: 120px !important;">Area</th>
					<th style="width: 300px !important;">Customer Instruction</th>
					<th style="width: 80px !important;">Amount Paid</th>
                    <th style="width: 110px !important;">CreatedBy</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookingsByArea as $booking)
					<tr>
						<td>{{ $booking['reference_id'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserMobileNum($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
                        <td>
                            <div id="bikeNumberDiv{{$booking->id}}">
                                @if($booking->bike_number_id != 0)
                                <select class="form-control" data-id="{{$booking->id}}" onClick="onClickBikeNumber({{$booking->id}})"><option value="{{ $booking->bike_number_id }}">{{ $booking->getBikeNumber() }}</option></select>
                                @else
                                <button type="button" class="form-control" onClick="onClickBikeNumber({{$booking->id}})" data-id="{{$booking->id}}">Assign a bike</button>
                                @endif
                            </div>
                        </td>
						<td>{{ $booking->getDuration() }}</td>
						<td>{{ $booking->getReadableDateTime($booking->start_datetime) }}</td>
                        <td>{{ $booking->getReadableDateTime($booking->end_datetime) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
						
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

<script>

    
    function onClickBikeNumber(id){
		                    
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-for-booking",
            data: { 

                'booking_id':id
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(html) {                
                    $('#bikeNumberDiv'+id).html( html );
                    $('#bikeNumber'+id).focus();
                }
        }); 
    }
	
    
    
    function applyBikeNumberChange(id){

        var bikeNumber = $('#bikeNumber'+id).val();
        
        console.log(bikeNumber);
        
        $.ajax({
            type: "POST",
            url: "/api/apply-bike-numbers-for-booking",
            data: { 

                'booking_id':id,
                'bike_number_id':bikeNumber
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) { 
                
                    
                    if(result.result.data.success)
                    {
                        $('#bikeNumberDiv'+id).html( result.result.data.html );
                    }
                    else
                    {
                        alert(result.result.data.message);
                    }
                    
                }
        });        
        
	};
    
    function focusOutBikeNumber(id){

                
        $.ajax({
            type: "GET",
            url: "/api/get-bike-numbers-html-for-booking",
            data: { 

                'booking_id':id,
                
            },
            cache: false,
            beforeSend: function () {
                $('#bikeNumberDiv'+id).html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
                },
                success: function(result) {
                    
                    $('#bikeNumberDiv'+id).html( result );
                    
                }
        });        
        
	};
    
</script>	

@stop

