@extends('erp_layout')

@section('content')
	
	<div class="booking-pg">
		<h2 class="title">Tomorrow Drop Off</h2>
	
        
        
                <p>&nbsp;</p>

        <form class="form-inline" method="get" action="/erp/tomorrowbookingdrop">
			
		  	<div class="form-group">
		    	<label class="" for="exampleInputPassword3">Area: </label>
		  		<select name="areaId" class="form-control">
                    @foreach($activeAreas as $area)
                        <option value="{{ $area->id }}" <?php if($area->id == $selected) echo "selected"?>>{{ $area->area }}</option>
                    @endforeach
                    @if($allowAllAreas)
                            <option value="0" <?php if($selected==0) echo "selected"?>>All Areas</option>
                    @endif
                </select>
		  	</div>
		  	<div class="form-group">
		  		<button type="submit" class="btn btn-primary"> Filter</button>
		  	</div>
		  	<input type="hidden" name="areaName"/>
		</form>

        
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<table class="wk-table">
			<thead>
				<tr>
					<th>Id</th>
                    <th>End Date</th>
					<th>End Time</th>
					<th>Start Date</th>
					<th>Start Time</th>
					<th>Customer</th>
					<th>Bike</th>
					<th>Area</th>
					<th>Customer Instruction</th>
					<th>Amount Paid</th>
                    <th>CreatedBy</th>
               </tr>
			</thead>
			<tbody>
				@foreach($bookingsByArea as $booking)
					<tr>
						<td>{{ $booking['reference_id'] }}</td>
                        <td>{{ $booking['end_date'] }}</td>
						<td>{{ $booking['end_time'] }}</td>
						<td>{{ $booking['start_date'] }}</td>
						<td>{{ $booking['start_time'] }}</td>
						<td>{{ $booking->getUserName($booking->user_id) }} - {{ $booking->getUserMobileNum($booking->user_id) }}</td>
						<td>{{ $booking->getBikeMakeName($booking->model_id) }} - {{  $booking->getBikeModelName($booking->model_id) }}</td>
						<td>{{ $booking->getCityName($booking->area_id) }} - {{ $booking->getAreaName($booking->area_id) }}</td>
						<td>{{ $booking->note }}</td>
                        <td>{{ $booking->total_price }}</td>
                        <td>{{ $booking->getCreatedByEmail($booking->createdBy) }}</td>
						
                </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	

@stop

