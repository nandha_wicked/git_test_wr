@extends('randm_layout')

@section('content')
	<div class="dashboard-pg">
		<h1><i class="fa fa-user-secret"></i> Welcome Back, <span>{{Auth::user()->first_name}}</span></h1>
	</div>
@stop