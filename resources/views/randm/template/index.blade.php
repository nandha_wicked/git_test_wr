<?php if(!isset($layoutOverride)) $layout = "randm_layout"; else $layout = $layoutOverride; ?>
@extends($layout)
@section('content')
	
	<div class="news-pg">
		<h2 class="title">{{$pageTitle}}</h2>
		<button class="btn btn-primary" data-toggle="modal" data-target="#{{$addObject['modal_name']}}"><i class="fa fa-plus"></i>{{$addObject['add_button_text']}}</button>
		@if(count($errors) > 0)
			<div style="margin:20px"></div>
			@if(count($errors) > 0)
                <script type="text/javascript"> 
                    alert("@foreach ($errors->all() as $error){{ $error }} @endforeach"); 
                </script>
            @endif
		@endif
        
        @if(isset($filterOptions))
        <p>&nbsp;</p>        
            <form class="form-inline" method="get" action="{{ $filterOptions['url'] }}">
                <div class="form-group">
                    <label class="" for="exampleInputPassword3">{{ $filterOptions['label'] }}: </label>
                    <select name="{{ $filterOptions['name'] }}" class="form-control">
                        @foreach($filterOptions['options'] as $option)
                            @if($filterOptions['selected']==$option['value'])
                                <option value="{{ $option['value'] }}" selected>{{ $option['text'] }}</option>
                            @else
                                <option value="{{ $option['value'] }}">{{ $option['text'] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"> Filter</button>
                </div>
            </form>
        @endif
        <div id="no-more-tables">
            <table class="fixedCol wk-table" style="table-layout: fixed; width:{{$objectTableWidth}}px">
                <thead>
                    <tr>
                        @foreach($objectTableArray['headers'] as $header)
                           <th style="width: {{$header['width']}}px !important;">{{$header['title']}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($objectTableArray['body'] as $body)

                        <tr>
                            @foreach($body as $key=>$bodyEl)    
                                @if($bodyEl['type']=="text")
                                    @if($bodyEl['value']['pre']=="true")
                                        <td data-title="{{ $objectTableArray['headers'][$key]['title'] }}"><pre>{!! $bodyEl['value']['text'] !!}</pre></td>
                                    @else
                                        <td data-title="{{ $objectTableArray['headers'][$key]['title'] }}">{{ $bodyEl['value']['text'] }}</td>
                                    @endif
                                @elseif($bodyEl['type']=="image")
                                    <td data-title="{{ $objectTableArray['headers'][$key]['title'] }}"><img src="{{ $bodyEl['value']['src'] }}" width ="{{ $bodyEl['value']['width'] }}px" /></td>
                                @elseif($bodyEl['type']=="button")
                                <td data-title="{{ $objectTableArray['headers'][$key]['title'] }}">
                                    @foreach($bodyEl['value'] as $button)
                                    <button class="{{$button['style_class']}} {{$button['js_class']}}" id="{{$button['id']}}" data-id="{{ $button['data_id'] }}"><i class="{{$button['icon']}}"></i> {{$button['text']}}</button>
                                    @endforeach
                                </td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
	</div>
    

	<script type="text/javascript">
		$('.{{$editObject['edit_btn_class']}}').click(function(){
            
			var id = $(this).data('id');
            
            $('#{{$editObject['modal_name']}} form').attr("action","{{$editObject['edit_url']}}/"+id);
            
            $.ajax(
            {
                type: "GET",
                url: "{{$editObject['ajax_url']}}",
                data: { 
                    'id': id,
                    '_token':"{{ csrf_token() }}"
                },
                cache: false,
                success: function(data) 
                {
                    
                    var return_object = data['result']['data'];
                    
                    return_object.forEach(populateEditAfterAjax);
                    
                    function populateEditAfterAjax(item, index){
                        if(item.type == "input")
                        {
                            $('#{{$editObject['modal_name']}} form input[name="'+item.name+'"]').val(item.value);
                        }
                        else if(item.type == "textarea")
                        {
                            $('#{{$editObject['modal_name']}} form textarea[name="'+item.name+'"]').data("wysihtml5").editor.setValue(item.value);
                        }
                        else if(item.type == "image")
                        {
                            $('#{{$editObject['modal_name']}} form #'+item.imgId).html(item.html);
                        }
                        else if(item.type == "select")
                        {
                            $('#{{$editObject['modal_name']}} form select[name="'+item.name+'"]').val(item.value);
                        }
                        else if(item.type == "checkbox")
                        {
                            if(item.status == '1')
                                $('#'+item.ckbxId1).prop("checked",true);	
                            else
                                $('#'+item.ckbxId2).prop("checked",true);	 
                        }
                        else if(item.type == "date")
                        {
                            $('#{{$editObject['modal_name']}} form input[name="'+item.name+'"]').val(item.value);
                        }
                        else if(item.type == "time")
                        {
                            $('#{{$editObject['modal_name']}} form input[name="'+item.name+'"]').val(item.value);
                        }
                        else if(item.type == "multiselect")
                        {
                            $('#'+item.id).multiSelect('deselect_all');
                            var valueArray = item.value.split(",");
                            $('#'+item.id).multiSelect('select', valueArray);
                            
                        }
                        
                    }

                }
            });
            
            $('#{{$editObject['modal_name']}}').modal('show');
        });

		$('.{{$deleteObject['delete_btn_class']}}').click(function(){
			var id = $(this).data('id');
			$('#{{$deleteObject['modal_name']}} form').attr("action","{{$deleteObject['delete_url']}}/"+id);
			$('#{{$deleteObject['modal_name']}}').modal('show');
		});
	</script>

@stop


@section('model')
	

    <div class="modal fade" id="{{$addObject['modal_name']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="{{$addObject['add_url']}}" class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">{{$addObject['modal_title']}}</h4>
		      		</div>
                        <div class="modal-body">
                            
                            @foreach($addObject['add_modal_form_items'] as $formItem)                            
                                @if($formItem['type'] == "input")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="{{$formItem['input_type']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "image")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <input type="file" name="{{$formItem['name']}}" />
                                        </div>
                                    </div>

                                @elseif($formItem['type'] == "textarea")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control textarea" rows="3" name="{{$formItem['name']}}"></textarea>
                                        </div>
                                    </div>

                                @elseif($formItem['type'] == "checkbox")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9"> 
                                            <label class="radio-inline">
                                              <input type="radio" name="{{$formItem['name']}}" id="{{$formItem['ckbxId1']}}" value="1"> {{$formItem['ckbxLabel1']}}
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="{{$formItem['name']}}" id="{{$formItem['ckbxId2']}}" value="0"> {{$formItem['ckbxLabel2']}}
                                            </label>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "select")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select name="{{$formItem['name']}}" id="{{$formItem['id']}}" class="form-control">
                                                @if($formItem['default_value'] != "not_set")
                                                    <option value = "{{$formItem['default_value']}}" selected> {{$formItem['default_text']}} </option>
                                                @endif
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "combobox")
                                    <div class="form-group form-inline">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select name="{{$formItem['name']}}" id="{{$formItem['id']}}">
                                                <option disabled selected></option>
                                                @if($formItem['default_value'] != "not_set")
                                                    <option value = "{{$formItem['default_value']}}" selected> {{$formItem['default_text']}} </option>
                                                @endif
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                        <script>
                                            $("#{{$formItem['id']}}").combobox();
                                        </script>
                                    </div>
                            
                                @elseif($formItem['type'] == "fieldset")
                                    <div class="form-group">
                                        
                                    </div>
                            
                                @elseif($formItem['type'] == "date")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="text" id="{{$formItem['id']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery('#{{$formItem["id"]}}').datetimepicker({
                                          format:'Y-m-d H:00',
                                          minDate:'2015/09/01'
                                        });
                                    </script>
                                @elseif($formItem['type'] == "time")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="text" id="{{$formItem['id']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('#{{$formItem["id"]}}').datetimepicker({
                                          datepicker: false,
                                          format: 'H:i'
                                        });
                                    </script>
                                @elseif($formItem['type'] == "multiSelect")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select multiple="multiple" id="{{$formItem['id']}}" name="{{$formItem['name']}}[]">
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    @if((isset($selectItem['selected']))&&($selectItem['selected'] == true))
                                                        <option value="{{ $selectItem['value'] }}" selected>{{ $selectItem['text'] }}</option>
                                                    @else
                                                        <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            
                                    <script>
                                        $('#{{$formItem['id']}}').multiSelect({
                                            selectableHeader: "<div class='custom-header'>{{ $formItem['selection_header'] }}</div>",
                                            selectionHeader: "<div class='custom-header'>{{ $formItem['selected_header'] }}</div>",
                                        })
                                    </script>
                                @endif
                            
                            @endforeach
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
                        
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Add</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="{{$editObject['modal_name']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action class="form-horizontal" enctype="multipart/form-data">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">{{$editObject['modal_title']}}</h4>
		      		</div>
                        <div class="modal-body">
                            
                            @foreach($editObject['edit_modal_form_items'] as $formItem)
                            
                                @if($formItem['type'] == "input")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="{{$formItem['input_type']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "image")
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label class="col-md-6">{{$formItem['label']}} :</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="{{$formItem['name']}}" />
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-6">
                                                <label class="col-md-3">Old Image :</label>
                                                <div class="col-md-6">
                                                    <div id="{{$formItem['old_image_id']}}"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @elseif($formItem['type'] == "textarea")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control textarea" rows="3" name="{{$formItem['name']}}"></textarea>
                                        </div>
                                    </div>

                                @elseif($formItem['type'] == "checkbox")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9"> 
                                            <label class="radio-inline">
                                              <input type="radio" name="{{$formItem['name']}}" id="{{$formItem['ckbxId1']}}" value="1"> {{$formItem['ckbxLabel1']}}
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="{{$formItem['name']}}" id="{{$formItem['ckbxId2']}}" value="0"> {{$formItem['ckbxLabel2']}}
                                            </label>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "select")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select name="{{$formItem['name']}}" id="{{$formItem['id']}}" class="form-control">
                                                @if($formItem['default_value'] != "not_set")
                                                    <option value = "{{$formItem['default_value']}}" selected> {{$formItem['default_text']}} </option>
                                                @endif
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            
                                @elseif($formItem['type'] == "combobox")
                                    <div class="form-group form-inline">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select name="{{$formItem['name']}}" id="{{$formItem['id']}}" class="form-control">
                                                <option disabled selected></option>
                                                @if($formItem['default_value'] != "not_set")
                                                    <option value = "{{$formItem['default_value']}}" selected> {{$formItem['default_text']}} </option>
                                                @endif
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                        <script>
                                            $("#{{$formItem['id']}}").combobox();
                                            $( "#toggle" ).on( "click", function() {
                                                $( "#{{$formItem['id']}}" ).toggle();
                                            });
                                        </script>
                                    </div>
                            
                                @elseif($formItem['type'] == "date")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="text" id="{{$formItem['id']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery('#{{$formItem["id"]}}').datetimepicker({
                                          format:'Y-m-d H:00',
                                          minDate:'2015/09/01'
                                        });
                                    </script>
                                @elseif($formItem['type'] == "time")
                                    <div class="form-group">
                                     <label class="col-md-3">{{$formItem['label']}} :</label>
                                     <div class="col-md-9">
                                          <input type="text" id="{{$formItem['id']}}" name="{{$formItem['name']}}" class="form-control"/>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('#{{$formItem["id"]}}').datetimepicker({
                                          datepicker: false,
                                          format: 'H:i'
                                        });
                                    </script>
                                @elseif($formItem['type'] == "multiSelect")
                                    <div class="form-group">
                                        <label class="col-md-3">{{$formItem['label']}} :</label>
                                        <div class="col-md-9">
                                            <select multiple="multiple" id="{{$formItem['id']}}" name="{{$formItem['name']}}[]">
                                                @foreach($formItem['select_items'] as $selectItem)
                                                    @if((isset($selectItem['selected']))&&($selectItem['selected'] == true))
                                                        <option value="{{ $selectItem['value'] }}" selected>{{ $selectItem['text'] }}</option>
                                                    @else
                                                        <option value="{{ $selectItem['value'] }}">{{ $selectItem['text'] }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            
                                    <script>
                                        $('#{{$formItem['id']}}').multiSelect({
                                            selectableHeader: "<div class='custom-header'>{{ $formItem['selection_header'] }}</div>",
                                            selectionHeader: "<div class='custom-header'>{{ $formItem['selected_header'] }}</div>",
                                        })
                                    </script>
                                @endif
                            
                            
                            @endforeach
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
                        
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

	

    <div class="modal fade" id="{{$deleteObject['modal_name']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action>
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel">{{$deleteObject['modal_title']}}</h4>
		      		</div>
		      		<div class="modal-body">
		        		{{$deleteObject['delete_description']}}<br>
		        		If "Yes" press "Delete" else "Close".
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		</div>
		      		<div class="modal-footer">
		      			<button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Delete</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
		      		</div>
		    	</form>
	    	</div>
	  	</div>
	</div>

@stop
