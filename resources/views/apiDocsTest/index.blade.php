@extends('api_docs_layout')

@section('body')
<body class="index" data-languages="[{{$data_languages_string}}]">

    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="images/navbar.png" />
      </span>
    </a>
    <div class="tocify-wrapper">
      <img src="images/logo.png" />
        <div class="lang-selector">
            @foreach($languages as $language)
              <a href="#" data-language-name="{{$language->language}}">{{$language->language}}</a>
            @endforeach
        </div>
        <div class="search">
          <input type="text" class="search" id="input-search" placeholder="Search">
        </div>
        <ul class="search-results"></ul>
      <div id="toc">
      </div>
        <ul class="toc-footer">
            
        </ul>
    </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
       
          
    @foreach($apiCategories as $apiCategory)    
          

        <!--Left side part -->
        <h1 id="category">Category</h1>

        @foreach($apiCategory['apis'] as $api)
          
          
          
            <h2 id="api-call">Api Call Title</h2>
          
            <!--Right side part -->
            
            @foreach($api['right_side_elements'] as $right_side_element)

                <blockquote>Your request sucks  Welcome to the Kittn API! You can use our API to access Kittn API endpoints, which can get information on various cats, kittens, and breeds in our database.</blockquote>
                <blockquote><p>Body of right side part - code samples</p></blockquote>
                <blockquote><p>Body of right side part - response examples</p></blockquote>
            
            @endforeach
          
            

            <!-- Middle Part -->
            <p><code class="prettyprint">GET http://example.com/api/&lt;ID&gt;</code></p>
            <h3 id="middle-part">Middle part headings</h3>
          
            <p>Middle part description - description of api docs</p>
            <aside class="notice">Blue Box</aside>
            <aside class="warning">Red Box.</aside>
            <aside class="success">Green Box.</aside>
          
            <h3 id="middle-part">Middle part Table Heading</h3>
            <table>
                <thead>
                    <tr>
                        <th>Error Code</th>
                        <th>Meaning</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>400</td>
                        <td>
                            Bad Request &ndash; Your request sucks  Welcome to the Kittn API! You can use our API to access Kittn API endpoints, which can get information on various cats, kittens, and breeds in our database.
                        </td>
                    </tr>
                </tbody>
            </table>
          
            <h3 id="middle-part">Middle part Arguments headings</h3>
            <table>
                <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>parameter_name <br/><p style="color:grey;">Optional</p></td>
                        <td>
                             (string) - You can use our API to access Kittn API endpoints, which can get information on various cats, kittens, and breeds in our database.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:20px;">&rdsh; parameter_name <br/><p style="color:orange;">Required</p></td>
                        <td>
                             (string) - You can use our API to access Kittn API endpoints, which can get information on various cats, kittens, and breeds in our database.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:40px;">&rdsh; parameter_name_is_really_long <br/><p style="color:orange;">Required</p></td>
                        <td>
                             (string) - You can use our API to access Kittn API endpoints, which can get information on various cats, kittens, and breeds in our database.
                        </td>
                    </tr>
                </tbody>
            </table>

        @endforeach

    @endforeach
    
          
    
          
             
     
          
       

      </div>
      <div class="dark-box">
          <div class="lang-selector">
                @foreach($languages as $language)
                    <a href="#" data-language-name="{{$language->language}}">{{$language->language}}</a>
                @endforeach
                
          </div>
      </div>
    </div>
  </body>
@stop


