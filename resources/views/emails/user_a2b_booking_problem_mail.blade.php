<div>Hi {{ $fname }},</div>
<br>
@if($id=="notfound")
    <p>Sorry!! The requested one way trip does not exist. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</p>
@elseif($id=="notavailable")
    <p>Sorry!! The requested one way trip was booked by somebody before we could confirm your booking. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</p>
@elseif($id=="pricemismatch")
    <p>Sorry!! There was a price mismatch and your booking could not be confirmed. Please contact the reservation desk at 08046801054 or reservations@metrobikes.in for assistance with refund or rescheduling. &nbsp;</p>
@endif
<br><br>
<div>Thank You</div>

