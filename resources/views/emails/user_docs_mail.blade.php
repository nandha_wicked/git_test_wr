@inject('frontEndText','App\Models\FrontEndText')

<div>Hi {{ $fname }},</div>
<br>
<p>Your Booking - {{ $reference_id }}  is due soon.</p>
<p>The following are the documents that are needed for renting the bike: </p>
<p>{!! $frontEndText->getText('documents_required') !!}</p>

<br>
KYC: Could you please answer the below questions and help us serve you better?
<br>
{{ $surveyLink }}

<br><br>
<div><?php echo env('SITEMOTTO');?><br>Team@<?php echo env('SITENAMECAP');?></div>


