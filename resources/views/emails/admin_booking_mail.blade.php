<div>Hi Admin,</div>
<br>
<div><?php echo env('SITENAMECAP');?> Booking Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userFname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Booking ID:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $bookingId }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Email:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userEmail }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userMob }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Model Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $modelName }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Pickup Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $readable_start_datetime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Dropoff Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $readable_end_datetime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Location :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $areaName }}  {{ $areaAddress }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Instruction:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $note }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Amount :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $amount }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">KM Limit :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $kmLimitMessage }}</td>
	</tr>
</table>
<br><br>
<div>Thank You</div>

