
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta content="text/html;charset=UTF-8" http-equiv="Content-Type"><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>User Signup Details</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] { 
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px!important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td[class="rnb-force-nav"] {
        display: block;
        }
        }

        @media only screen and (max-width : 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td[class~="rnb-force-col"] {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        }

        table[class~="rnb-container"] {
         width: 100% !important;
        }

        table[class="rnb-btn-col-content"] {
        width: 100% !important;
        }
        table[class="rnb-col-3"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table[class="rnb-last-col-3"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table[class="rnb-col-2-noborder-onright"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table[class="rnb-col-2-noborder-onleft"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table[class="rnb-last-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class="rnb-col-1"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img[class="rnb-col-3-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img-side-xs"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img-side-xl"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-1-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-header-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img[class="rnb-logo-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td[class="rnb-mbl-float-none"] {
        float:inherit !important;
        }

        .img-block-center{text-align:center!important;}

        .logo-img-center
        {
            float:inherit!important;
        }

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if mso 15]><style type="text/css">.mso-single-img-block {padding:0px 10px 0px 10px !important;}.msib-left-img {padding:0px 10px 0px 0px !important;}.mso-single-img-block2 {padding:0px 10px 0px 10px !important;}.msib-right-img {padding:0px 0px 0px 5px !important;}.mso-double-img-block {padding:0px 0px 0px 10px !important;}.mdib-first-left-img {padding:0px 0px 0px 0px !important;}.mso-three-img-block {padding:0px 0px 0px 10px !important;}.mtib-first-left-img {padding:0px 0px 0px 0px !important;}.mtib-second-left-img {padding:0px 0px 0px 0px !important;}.mso-img-content-block {padding:0px 13px 0px 13px !important;}.mso-button-block {padding:0px 10px 0px 10px !important;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--></head><body>

<table border="0" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">

	
	<tbody>

	<tr>

		
		<td align="center" valign="top" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">
	<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#d6d6d6" style="min-width:590px; background-color:#d6d6d6;" name="Layout_1" id="Layout_1">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#d6d6d6" style="min-width:590px; background-color: #d6d6d6;">
						
						<table width="590" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#d6d6d6" style="border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate; background-color: rgb(214, 214, 214);">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td valign="top" class="rnb-container-padding" bgcolor="#d6d6d6" style="background-color: #d6d6d6;" align="left">
									<table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
										<tbody><tr>
											<td valign="top" align="center">
												<table cellpadding="0" border="0" align="center" cellspacing="0" ng-class="{'logo-img-center':col.td.align}" class="logo-img-center">
													<tbody><tr>
														<td valign="middle" align="center">
                                                            
                                                            
                                                            
                                                                <img width="280" vspace="0" hspace="0" border="0" alt="[&quot;<?php echo env('SITENAMESMALLNOCOM');?>&quot;]" style="max-width:280px;" class="rnb-logo-img" src="http://wx1m.r.bh.d.sendibt3.com/nfcdbkuabe.png">
														</td>
													</tr>
												</tbody></table>
												
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>

		
		<td align="center" valign="top" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#d6d6d6" style="min-width:590px; background-color:#d6d6d6;" name="Layout_2" id="Layout_2">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#d6d6d6" style="min-width:590px; background-color: #d6d6d6;">
						
						<table width="590" border="0" cellpadding="0" cellspacing="0" class="mso-img-content-block rnb-container" bgcolor="#ffffff" style="border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px; background-color: rgb(255, 255, 255);">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
										<tbody><tr>
											
											
											
											<td class=" rnb-force-col" valign="top" style="padding-right: 0px;">

												
												
												
												
												<table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="left" class="rnb-col-1">

													
													<tbody>
													
													

													
													<tr>
														
														<td style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#999; font-weight:bold; text-align:left;">
															
															
                                                            
															<span style="color:#999; font-weight:bold;"><span style="color:#000000">Hi {{ ucfirst($fname) }},</span></span>
														</td>
													</tr>
													<tr>
														<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
													</tr>
													

													
													<tr>
														
														<td style="font-size:13px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#555;">
															
															<div><div>Thank you for signing up for Bikation.</div>

<div>&nbsp;</div>

<div>Your Login Details -</div>

<div>Email :{{ $email }}</div>

<div>Password :{{ $password }}</div>
</div>
														</td>
													</tr>
													

													
													
													
													

													
													
													
													

												</tbody></table>

												
											</td>
										</tr>
									</tbody></table>

								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>

					</td>
				</tr>
			</tbody></table>
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>

		
		<td align="center" valign="top" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">

			
			
			

			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px; background-color:#d6d6d6;" name="Layout_" id="Layout_">
				<tbody><tr>
					
					<td class="rnb-del-min-width" valign="top" align="center" bgcolor="#d6d6d6" style="min-width:590px; background-color:#d6d6d6;">
						
						<table width="100%" cellpadding="0" border="0" height="30" cellspacing="0" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">
							<tbody><tr>
								
								<td valign="top" height="30">
									<img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://wx1m.r.bh.d.sendibt3.com/nfd5rkuabe.gif">
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

            
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>

		
		<td align="center" valign="top" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">

			
			
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#d6d6d6" style="min-width:590px; background-color:#d6d6d6;" name="Layout_3" id="Layout_3">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#555" style="min-width:590px; background-color: #555;">
						<table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0" style="padding-right:20px; padding-left:20px;">
							<tbody><tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td>
									<div style="font-size:13px; color:#919191; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;"><div>You received this email because you are registered with <?php echo env('SITENAMESMALLNOCOM');?></div><div>&nbsp;</div></div>
									<!--div style="font-size:13px; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
										
										<a style="text-decoration:none; color:#ccc;" target="_blank" href="fc67kuabg.html">Unsubscribe here</a>
									</div-->
                                </td>
							</tr>
							<tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
                            <!--tr>
								<td>
									
                                    <div style="text-align:center;">
                                        <div style="font-family:Arial, Helvetica, sans-serif;color:#aaaaaa;opacity:0.8">Sent by</div>
                                        <a href="http://wx1m.r.bh.d.sendibt3.com/1pcodbkuabk.html" target="_blank"><img border="0" hspace="0" vspace="0" alt="SendinBlue" style="margin:auto;" src="http://wx1m.r.bh.d.sendibt3.com/nfdy7kuabe.png"></a>
                                    </div>
								</td>
							</tr-->
							<tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

			
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>

		
		<td align="center" valign="top" bgcolor="#d6d6d6" style="background-color:#d6d6d6;">

			
			
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#d6d6d6" style="min-width:590px; background-color:#d6d6d6;" name="Layout_4" id="Layout_4">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#333" style="min-width:590px; background-color: #333;">
						<table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" style="padding-right:20px; padding-left:20px;">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td style="font-size:13px; color:#919191; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
									
									<div>© 2016 <?php echo env('SITENAMESMALLNOCOM');?></div>
								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

            
            
            


            
            
            

		</td>
	</tr>
</tbody></table>

<div style="color: #727272; font-size: 10px;"><center></center></div></body></html>
