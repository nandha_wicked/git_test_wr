<div>Hi Admin,</div>
<br>
<div><?php echo env('SITENAMECAP');?> ContactEnquiry Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">First Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Last Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $lname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Email :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $email }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $mob }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Bike :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $bike }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;"> Start Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $startdate }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Start Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $starttime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">End Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $enddate }}</td>
	</tr>
		<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">End Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $endtime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Message :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $comment }}</td>
	</tr>
</table>
<br><br>
<div>Thank You</div>

