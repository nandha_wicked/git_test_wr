<div>Hi Admin,</div>
<br>
<div><?php echo env('SITENAMECAP');?> Booking Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
    
    
    <thead>
        <tr>
            <th style="border:1px solid #333;width:30%;padding:3px;">Id</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">Start Date Time</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">End Date Time</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">Bike</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">Area</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">Notes</th>
            <th style="border:1px solid #333;width:30%;padding:3px;">CreatedBy</th>
       </tr>
    </thead>
    
    <tbody>
    @foreach($bookings as $booking)
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['id'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['start_datetime'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['end_datetime'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['make-model'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['areaName'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['note'] }}</td>
            <td style="border:1px solid #333;width:30%;padding:3px;">{{ $booking['block-email'] }}</td>
        
	   </tr>
    
    @endforeach
    </tbody>
</table>
<br><br>
<div>Thank You</div>

