@inject('emailPromoLinkObj','App\Models\EmailPromoLink')

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>Welcome to <?php echo env('SITENAMECAP');?></title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

		@media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
		.rnb-yahoo-width{ width:360px!important;}				
		}	

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td[class="rnb-force-nav"] {
        display: block;
        }
        }

        @media only screen and (max-width : 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td[class~="rnb-force-col"] {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        }

        table[class~="rnb-container"] {
         width: 100% !important;
        }

        table[class="rnb-btn-col-content"] {
        width: 100% !important;
        }
        table[class="rnb-col-3"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table[class="rnb-last-col-3"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table[class="rnb-col-2-noborder-onright"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table[class="rnb-col-2-noborder-onleft"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table[class="rnb-last-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class="rnb-col-1"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img[class="rnb-col-3-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img-side-xs"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-2-img-side-xl"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-col-1-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img[class="rnb-header-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img[class="rnb-logo-img"] {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td[class="rnb-mbl-float-none"] {
        float:inherit !important;
        }

		.img-block-center{text-align:center!important;}

		.logo-img-center
		{
			float:inherit!important;
		}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if mso 15]><style type="text/css">.mso-single-img-block {padding:0px 10px 0px 10px !important;}.msib-left-img {padding:0px 10px 0px 0px !important;}.mso-single-img-block2 {padding:0px 10px 0px 10px !important;}.msib-right-img {padding:0px 0px 0px 5px !important;}.mso-double-img-block {padding:0px 0px 0px 10px !important;}.mdib-first-left-img {padding:0px 0px 0px 0px !important;}.mso-three-img-block {padding:0px 0px 0px 10px !important;}.mtib-first-left-img {padding:0px 0px 0px 0px !important;}.mtib-second-left-img {padding:0px 0px 0px 0px !important;}.mso-img-content-block {padding:0px 13px 0px 13px !important;}.mso-button-block {padding:0px 10px 0px 10px !important;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--></head><body>

<table border="0" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#eb952e" style="background-color:#eb952e;">

	
	<tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
	
	<tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			
			

			
			
			

			
			
			

            
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e;" name="Layout_1" id="Layout_1">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#eb952e" style="min-width:590px; background-color: #eb952e;">
						
						<table width="590" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#c6c6c6" style="border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate; background-color: rgb(214, 214, 214);">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td valign="top" class="rnb-container-padding" bgcolor="#c6c6c6" style="background-color: #c6c6c6;" align="left">
									<table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
										<tbody><tr>
											<td valign="top" align="center">
												<table cellpadding="0" border="0" align="center" cellspacing="0" ng-class="{'logo-img-center':col.td.align}" class="logo-img-center">
													<tbody><tr>
														<td valign="middle" align="center">
                                                            
                                                            <a style="text-decoration:none;" target="_blank" href="https://www.<?php echo env('SITENAMESMALL');?>">
																<img width="300" vspace="0" hspace="0" border="0" alt="<?php echo env('SITENAMECAP');?>" style="max-width:300px;" class="rnb-logo-img" src="http://img.mailinblue.com/1513435/images/rnb/original/5773b4758bc34edd3f8b4ab0.png">
															</a>
                                                            
                                                                
														</td>
													</tr>
												</tbody></table>
												
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

			
			
			
			
			
			
			

			
			
			

			
			
			

			            
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e;" name="Layout_2" id="Layout_2">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#eb952e" style="min-width:590px; background-color: #eb952e;">
						
						<table width="590" border="0" cellpadding="0" cellspacing="0" class="mso-img-content-block rnb-container" bgcolor="#ffffff" style="border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px; background-color: rgb(255, 255, 255);">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
										<tbody><tr>
											
											
											
											<td class=" rnb-force-col" valign="top" style="padding-right: 0px;">
											
												
												
												
												
												<table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="left" class="rnb-col-1">
												
													
													<tbody>
													
													
													
													
													<tr>
														
														<td style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#999; font-weight:bold; text-align:left;">
															
															
                                                            
															<span style="color:#999; font-weight:bold;"><div style="line-height:150%;"><span style="color:#000000">Welcome to </span><span style="color:#FF8C00"><?php echo env('SITENAMECAP');?></span><br>
Your one stop shop to explore an exciting world on two wheels</div>
</span>
														</td>
													</tr>
													<tr>
														<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
													</tr>
													
													
													
													<tr>
														
														<td style="font-size:13px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#555;">
															
															<div><div><?php echo env('SITENAMECAP');?> is a Premium Motorcycle Rental and Touring Company offering Motorcycle Rentals and Motorcycle Tours from over 12 locations across India.</div>

<div>&nbsp;</div>

<div>Experience a wide range of motorcycles from Harley Davidson, Triumph, Royal Enfield, Ducati, Benelli and More.</div>

<div><br>
Try the <?php echo env('SITENAMECAP');?> App on Android and AppStore where our experiences are just a click away.</div>

<div>&nbsp;</div>

<div>Just shoot us an email! We’re always here to help.</div>

<div>&nbsp;</div>

<div>&nbsp;</div>

<div>Revs in our hearts,</div>

<div>&nbsp;</div>

<div><?php echo env('SITENAMECAP');?> Team</div>

<div>&nbsp;</div>

<div>&nbsp;</div>
<?php $emailPromoLinks = $emailPromoLinkObj->getActiveLinks(); ?>
@if(!$emailPromoLinks)
                                                                
@else
@foreach($emailPromoLinks as $emailPromoLink)
<div>{!! $emailPromoLink->description !!}</div>
<div style="position: relative;"><a href="{{ $emailPromoLink->url }}" style="text-decoration: none; color: rgb(153, 153, 153);"><img alt="" class="rnb-col-3-img element-has-options ui-droppable" data-options-id="element-img-2-0" image-droppable="" imageonload="" src="{{ $emailPromoLink->image }}" style="vertical-align: top; display:block;  border-width: 0px; border-style: solid; margin: auto; width: 550px; background-color: transparent;"></a></div><br/><br/>
                                        
@endforeach                                                                
@endif
</div>
														</td>
													</tr>
													
													
													
													
													
													
													
													
													
													
													
													
												</tbody></table>
												
												
											</td>
										</tr>
									</tbody></table>
								
								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
				
					</td>
				</tr>
			</tbody></table>
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			
			
			
			
			

			
			
			

			
			
			

			            
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			
			

			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px; background-color:#eb952e;" name="Layout_" id="Layout_">
				<tbody><tr>
					
					<td class="rnb-del-min-width" valign="top" align="center" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e;">
						
						<table width="100%" cellpadding="0" border="0" height="30" cellspacing="0" bgcolor="#eb952e" style="background-color:#eb952e;">
							<tbody><tr>
								
								<td valign="top" height="30">
									<img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

            
			
			

			
			
			

			
			
			
			
			
			
			

			
			
			

			
			
			

			            
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			
			
			
			
			

			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e;" name="Layout_3" id="Layout_3">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#555" style="min-width:590px; background-color: #555;">
						<table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0" style="padding-right:20px; padding-left:20px;">
							<tbody><tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td>
									<div style="font-size:13px; color:#919191; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">This email was sent to {{$email}}<div>You received this email because you are registered with <?php echo env('SITENAMECAP');?></div><div>&nbsp;</div></div>
									<div style="font-size:13px; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
										
										<a style="text-decoration:none; color:#ccc;" target="_blank" href="[UNSUBSCRIBE]">Unsubscribe here</a>
									</div>
                                </td>
							</tr>
							<tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
                            
							
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

			            
			
			

            
            
            


            
            
            

		</td>
	</tr><tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			
			

			
			
			

			
			
			

            
			
			

			
			
			

			
			
			
			
			
			
			

			
			
			

			
			
			

			            
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e;" name="Layout_4" id="Layout_4">
				<tbody><tr>
					
					<td class="rnb-del-min-width" align="center" valign="top" bgcolor="#333" style="min-width:590px; background-color: #333;">
						<table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" style="padding-right:20px; padding-left:20px;">
							<tbody><tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td style="font-size:13px; color:#919191; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
									
									<div>© 2016 <?php echo env('SITENAMECAP');?></div>
								</td>
							</tr>
							<tr>
								<td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

            
            
            


            
            
            

		</td>
	</tr><tr>
	
		
		<td align="center" valign="top" bgcolor="#eb952e" style="background-color:#eb952e;">
		
			
			
			

			
			<table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eb952e" style="min-width:590px; background-color:#eb952e" name="Layout_0" id="Layout_0">
				<tbody><tr>
					
					<td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px; background-color: rgb(85, 85, 85);">
						<table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-yahoo-width">
							<tbody><tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
							<tr>
								
								<td align="center" height="20" style="font-size: 13px; color: rgb(255, 255, 255); font-weight: normal; text-align: center; font-family: Arial, Helvetica, sans-serif;">
									<span style="color: rgb(255, 255, 255); text-decoration: none;">
										
										<a target="_blank" href="[MIRROR]" style="text-decoration: none; color: rgb(255, 255, 255);">View in browser</a>
									</span>
								</td>
							</tr>
							<tr>
								<td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
			

			
			
			

            
			
			

			
			
			

			
			
			
			
			
			
			

			
			
			

			
			
			

			            
			
			

            
            
            


            
            
            

		</td>
	</tr>
</tbody></table>

</body></html>
