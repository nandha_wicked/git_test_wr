<div>Hi Admin,</div>
<br>
<div><?php echo env('SITENAMECAP');?> Biker's Plan Subscription Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userFname }}</td>
	</tr>

	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Email:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userEmail }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userMob }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Plan Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $plan }}</td>
	</tr>

	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Amount :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $amount }}</td>
	</tr>
</table>
<br><br>
<div>Thank You</div>

