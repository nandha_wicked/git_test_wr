<div>Hi {{ ucfirst($fname) }},</div>
<br>
<div>Your new login password: {{ $password }}</div>
<br>
<div>Please make sure to change your password once you login.</div>
<br><br>
<div><?php echo env('SITEMOTTO');?><br>Team@<?php echo env('SITENAMECAP');?></div>

