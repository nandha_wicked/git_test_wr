<br>
<div><?php echo env('SITENAMECAP');?> Booking Details.</div>
<br>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<thead>
		<tr>
			<th>Id</th>
			<th>Booking ID</th>
			<th>Area</th>
			<th>Model</th>
			<th>Bike ID</th>
			<th>Pickup Date-Time</th>
			<th>Dropoff Date-Time</th>
			<th>Value</th>
			<th>User</th>
		</tr>
	</thead>
	<tbody>
		@foreach($bookingsMatrix as $bm) 
			@if ($bookingId == $bm['referenceId'])
				<tr style="background-color:#C6FD91">
					<td style="border:1px solid #333;width:5%;padding:3px;">{{ $bm['id'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['referenceId'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['area'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['model'] }}</td>
					<td style="border:1px solid #333;width:5%;padding:3px;">{{ $bm['bikeId'] }}</td>
					<td style="border:1px solid #333;width:20%;padding:3px;">{{ $bm['fromDateTime'] }}</td>
					<td style="border:1px solid #333;width:20%;padding:3px;">{{ $bm['toDateTime'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['value'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['userName'] }}</td>
				</tr>
			@else
				<tr>
					<td style="border:1px solid #333;width:5%;padding:3px;">{{ $bm['id'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['referenceId'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['area'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['model'] }}</td>
					<td style="border:1px solid #333;width:5%;padding:3px;">{{ $bm['bikeId'] }}</td>
					<td style="border:1px solid #333;width:20%;padding:3px;">{{ $bm['fromDateTime'] }}</td>
					<td style="border:1px solid #333;width:20%;padding:3px;">{{ $bm['toDateTime'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['value'] }}</td>
					<td style="border:1px solid #333;width:10%;padding:3px;">{{ $bm['userName'] }}</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>
<br><br>
<div>Thank You</div>
