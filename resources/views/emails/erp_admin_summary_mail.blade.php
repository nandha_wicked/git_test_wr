
<div><h3>Operations Summary</h3></div>

<br/>
<div><h4>Following are the bookings with no delivery information entered</h4></div>
<br/>
@foreach($undeliveredBookingsByArea as $undeliveredBookingsByAreaEl)
<div>{{$undeliveredBookingsByAreaEl['area']}}</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Booking IDs with no info :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $undeliveredBookingsByAreaEl['bookingReferenceIds'] }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Deliveries without info:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $undeliveredBookingsByAreaEl['count'] }} of {{ $undeliveredBookingsByAreaEl['total_count'] }}</td>
	</tr>
    
</table>
<br/>
@endforeach

<br/>
<br/>
<div><h4>Following are the bookings with no return information entered</h4></div>
<br/>
@foreach($unreturnedBookingsByArea as $unreturnedBookingsByAreaEl)
<div>{{$unreturnedBookingsByAreaEl['area']}}</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Booking IDs with no info :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $unreturnedBookingsByAreaEl['bookingReferenceIds'] }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Returns without info:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $unreturnedBookingsByAreaEl['count'] }} of {{ $unreturnedBookingsByAreaEl['total_count'] }}</td>
	</tr>
    
</table>
<br/>
@endforeach

<br/>
<br/>
<div><h4>Following are the Walk-In Bookings</h4></div>
<br/>
@foreach($walkins as $walkinsByArea)
<div><h4>{{$walkinsByArea['area']}}</h4></div>

<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Count :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $walkinsByArea['count'] }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Collection :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">&#8377; {{ $walkinsByArea['total_collection'] }}</td>
	</tr>
    
</table>
<br/>

@if(count($walkinsByArea['walkin_bookings'])>0)
    <div>The following Walk-In bookings are from {{$walkinsByArea['area']}}</div>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 80px !important;">Booking ID</th>
            <th style="width: 80px !important;">Model</th>
            <th style="width: 60px !important;">Duration</th>
            <th style="width: 50px !important;">Value</th>
            <th style="width: 60px !important;">Discount</th>
            <th style="width: 160px !important;">Created By</th>
            <th style="width: 290px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($walkinsByArea['walkin_bookings'] as $walkinBooking)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['reference_id'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['model'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['duration'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['total_price'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['discount'] }}%</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['created_by'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $walkinBooking['notes'] }}</td>
            </tr>
        @endforeach
    </tbody>

</table>

@endif
<br/>
@endforeach



<br/>
<br/>
<div><h4>Following are the In-Store money Collections at Delivery(accessories)</h4></div>
<br/>
@foreach($storeCollectionDelivery as $storeCollectionDeliveryByArea)
<div><h4>{{$storeCollectionDeliveryByArea['area']}}</h4></div>

<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Collection :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">&#8377; {{ $storeCollectionDeliveryByArea['total_collection'] }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Cash Collection :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">&#8377; {{ $storeCollectionDeliveryByArea['cash_collection'] }}</td>
	</tr>
    
</table>
<br/>

<div>Details of Collections from {{$storeCollectionDeliveryByArea['area']}} at Delivery</div>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 80px !important;">Booking ID</th>
            <th style="width: 80px !important;">Amount</th>
            <th style="width: 80px !important;">Mode</th>
            <th style="width: 540px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($storeCollectionDeliveryByArea['collections'] as $collection)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['bookingReferenceId'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['money_collected'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['collection_mode'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['notes'] }}</td>
                
            </tr>
        @endforeach
    </tbody>

</table>
<br/>
@endforeach



<br/>
<br/>
<div><h4>Following are the In-Store money Collections at Return(extra KM, speed fine)</h4></div>
<br/>
@foreach($storeCollectionReturn as $storeCollectionReturnByArea)
<div><h4>{{$storeCollectionReturnByArea['area']}}</h4></div>

<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Collection :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">&#8377; {{ $storeCollectionReturnByArea['total_collection'] }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Cash Collection :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">&#8377; {{ $storeCollectionReturnByArea['cash_collection'] }}</td>
	</tr>
    
</table>
<br/>

<div>Details of Collections from {{$storeCollectionReturnByArea['area']}} at Return</div>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 80px !important;">Booking ID</th>
            <th style="width: 80px !important;">Amount</th>
            <th style="width: 80px !important;">Mode</th>
            <th style="width: 540px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($storeCollectionReturnByArea['collections'] as $collection)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['bookingReferenceId'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['money_collected'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['collection_mode'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $collection['notes'] }}</td>
                
            </tr>
        @endforeach
    </tbody>

</table>
<br/>
@endforeach



<br/>
<br/>
<div><h4>Bookings that were not delivered due to issues</h4></div>
<br/>
@foreach($cancelledDeliveries as $cancelledDeliveriesByArea)
<div>{{$cancelledDeliveriesByArea['area']}}</div>
<br/>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 80px !important;">Booking ID</th>
            <th style="width: 110px !important;">Model</th>
            <th style="width: 540px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($cancelledDeliveriesByArea['cancelled_bookings'] as $cancelled_bookings)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $cancelled_bookings['bookingReferenceId'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $cancelled_bookings['model'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $cancelled_bookings['notes'] }}</td>
                
            </tr>
        @endforeach
    </tbody>

</table>
<br/>
@endforeach



<br/>
<br/>
<div><h4>Approved Service Blocks</h4></div>
<br/>
@foreach($approvedServiceBlocks as $approvedServiceBlocksByArea)
<div>{{$approvedServiceBlocksByArea['area']}}</div>
<br/>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 110px !important;">Model</th>
            <th style="width: 110px !important;">Duration</th>
            <th style="width: 540px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($approvedServiceBlocksByArea['approved_service_blocks'] as $approvedServiceBlocksByAreaDetail)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $approvedServiceBlocksByAreaDetail['model_id'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $approvedServiceBlocksByAreaDetail['duration'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $approvedServiceBlocksByAreaDetail['notes'] }}</td>
                
            </tr>
        @endforeach
    </tbody>

</table>
<br/>
@endforeach



<br/>
<br/>
<div><h4>Expiring Service Blocks</h4></div>
<br/>
@foreach($expiringServiceBlocks as $expiringServiceBlocksByArea)
<div>{{$expiringServiceBlocksByArea['area']}}</div>
<br/>

<table style="table-layout: fixed; width:780px;border:1px solid #333;border-collapse:collapse;">
    
    <thead>
        <tr>
            <th style="width: 110px !important;">Model</th>
            <th style="width: 110px !important;">End Time</th>
            <th style="width: 540px !important;">Notes</th>
       </tr>
    </thead>
    <tbody>
        @foreach($expiringServiceBlocksByArea['expiring_service_blocks'] as $expiringServiceBlocksByAreaDetail)
            <tr>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $expiringServiceBlocksByAreaDetail['model_id'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $expiringServiceBlocksByAreaDetail['end_datetime'] }}</td>
                <td style="border:1px solid #333;width:30%;padding:3px;">{{ $expiringServiceBlocksByAreaDetail['notes'] }}</td>
                
            </tr>
        @endforeach
    </tbody>

</table>
<br/>
@endforeach


<br><br>
<div>Thank You</div>

