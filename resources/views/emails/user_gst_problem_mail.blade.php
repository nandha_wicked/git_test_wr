@inject('frontEndText','App\Models\FrontEndText')

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>       
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
</head>           	
	
<body style="font-family: 'arial'; font-size: 13px; color: #fff; background: #efefef; width: 100%; height: 100%; max-width: 600px; margin: 0 auto;">

   	<style type="text/css">	
		/*.full_row_table{
			width: 100%;
			color: #fff; padding: 15px;
		}
		.width100{ width: 100%; }
		.text_center{text-align: center;}
		.horizontal_line{ width: 100%; height: 1px; background-color: #555; padding: 0px;}*/
	</style>


	<table style="background-color: #F5F5F5; width: 100%;">
		<tr>
			<td>
				<img src="https://www.<?php echo env('SITENAMESMALL');?>/images/<?php echo env('SITELOGO');?>">
			</td>
			<td style="color: #333; font-size: 18px;">
				Sorry! An error occured during your recent booking.
			</td>
		</tr>
	</table>
	<!--  Main Row -->
	<table class="full_row_table" style="background-color: #333; width: 100%; color: #fff; padding: 15px;">
		<tr>
			<td style="font-size: 15px;">Dear {{ $fname }}</td>
		</tr>
		<tr>
			<td>
				<div style="margin-top: 10px;">
				    {{ $email_body }}
				</div>				
			</td>
		</tr>
	</table>

	
    
	<!-- Third Main Row -->
	<table class="full_row_table text_center" style="text-align: center; width: 100%; color: #fff; padding: 15px; padding-top: 24px; background-color: #F5F5F5; padding-bottom: 0px;">
		<tr>
			<td style="text-transform: uppercase; color: #333; font-size: 12px;"> 
				For any queries regarding your booking, Call
			</td>			
		</tr>
		<tr>
			<td style="color: #<?php echo env('WEBCOLOR'); ?>; font-weight: bold; font-size: 17px;">
				080 4680 1054
			</td>
		</tr>
	</table>
	<!-- Fourth Main Row -->
	<table class="full_row_table text_center" style=" text-align: center; width: 100%; color: #fff; padding: 15px;background-color: #F5F5F5; padding: 10px 0px;">
		<tr>
			<td style="width: 20%;"></td>
			<td style="width: 60%; border-top: 1px solid #ddd"></td>
			<td style="width: 20%;"></td>
		</tr>
	</table>
	<!-- Fifth Main Row -->
	<table class="full_row_table text_center" style="text-align: center; width: 100%; color: #fff; padding: 15px; background-color: #F5F5F5;">
		<tr>
			<td>
				<a href="#" style="padding: 0px 5px; font-size: 0px;">
					<img src="https://gallery.mailchimp.com/49ac554afdd434eac1749e9ad/images/6a562c74-3f68-46f6-8268-dc62848e8b40.jpg">
				</a>
				<a href="#" style="padding: 0px 5px; font-size: 0px;">
					<img src="https://gallery.mailchimp.com/49ac554afdd434eac1749e9ad/images/98c8510c-6a81-4f07-9cb3-375ab23c95d3.jpg">
				</a>
				<a href="#" style="padding: 0px 5px; font-size: 0px;">
					<img src="https://gallery.mailchimp.com/49ac554afdd434eac1749e9ad/images/36036ccc-8de0-4a9e-a4eb-df7f42db14c4.jpg">
				</a>
				<a href="#" style="padding: 0px 5px; font-size: 0px;">
					<img src="https://gallery.mailchimp.com/49ac554afdd434eac1749e9ad/images/2a7eb51d-ded8-47f4-9f7f-f9644b7ea3dd.jpg">
				</a>				
			</td>
		</tr>
		<tr>
			<td style="text-transform: uppercase; color: #333; font-size: 13px; padding-top: 13px;">
				Copyright 2015 <?php echo env('SITENAMECAP');?> Adventure Services. All Rights Reserved.
			</td>
		</tr>
	</table>
</body>
</html>
