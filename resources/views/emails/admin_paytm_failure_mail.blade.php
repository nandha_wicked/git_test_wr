<div>Hi Admin,</div>
<br>
<div>This customer has booked on App but did not receive confirmation. Please contact the customer.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userFname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Email:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userEmail }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $userMob }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Model Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $modelName }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Pickup Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fromDate }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Pickup Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fromTime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Dropoff Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $toDate }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Dropoff Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $toTime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Location :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $areaName }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Amount :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $amount }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Reason for Problem :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $reason }}</td>
	</tr>
    
</table>
<br><br>
<div>Thank You</div>

