<div>Dear {{ $userFullName }},</div>
<br>
<div>There have been a few changes made to your <?php echo env('SITENAMECAP');?> Booking - {{ $bookingId }}.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Model Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $modelName }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Pickup Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fromDate }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Pickup Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fromTime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Dropoff Date :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $toDate }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Dropoff Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $toTime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Location :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $areaName }}  {{ $areaAddress }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Additional Amount Paid :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $amount }}</td>
	</tr>
    
</table>
<br><br>
<div>Thank You</div>

