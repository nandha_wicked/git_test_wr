<div>Hi Admin,</div>
<br>
<div>Service Block Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
    
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Bike Number :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $bike_number }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Area:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $area_name }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Model:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $model_name }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Start Date Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $start_datetime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">End Date Time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $end_datetime }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Notes :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $note }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Created By :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $created_by }}</td>
	</tr>
	
</table>
<br><br>
<div>Thank You</div>

