<div>Hi Admin,</div>
<br>
<div><?php echo env('SITENAMECAP');?> ContactEnquiry Details.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">First Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $fname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Last Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $lname }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Email :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $email }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $mob }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Comments :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $comment }}</td>
	</tr>
</table>
<br><br>
<div>Thank You</div>

