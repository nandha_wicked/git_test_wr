<div>Hi Admin,</div>
<br>
@if($problem)
    <div><?php echo env('SITENAMECAP');?> Problem with One Way Rental Booking.</div>
@else
    <div><?php echo env('SITENAMECAP');?> One Way Rental Booking Details.</div>
@endif
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $name }}</td>
	</tr>
@if($problem == false)
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Booking ID:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $reference_id }}</td>
	</tr>
@endif
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Email:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $email }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Mobile No :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $phone }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Model Name :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $model }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">From Location :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $from }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">To Location :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $to }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Customer Instruction:</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $customer_instructions }}</td>
	</tr>
	<tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Total Amount with TAX :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $price }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Wallet Balance Applied :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $wallet_amount_applied }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Amount paid from payment gateway :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $pgTxnAmount }}</td>
	</tr>
@if($problem)
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Payment ID :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $pgTxnId }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Reason for Booking Failure :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $reason }}</td>
	</tr>
@else
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Estimated KM :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $estimated_distance }}</td>
	</tr>
    <tr>
		<td style="border:1px solid #333;width:30%;padding:3px;">Estimated time :</td>
		<td style="border:1px solid #333;width:70%;padding:3px;">{{ $estimated_duration }}</td>
	</tr>
@endif
</table>
<br><br>
<div>Thank You</div>

