<div>Hi Admin,</div>
<br>
<div>This customer has booked a bike but there is a problem with the payment. Please contact the customer.</div>
<table style="width:100%;border:1px solid #333;border-collapse:collapse;">
	@if(isset($bookingId))
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Booking ID :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">{{ $bookingId }}</td>
        </tr>
    @endif
    @if(isset($txnAmount))
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Payment Made :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">{{ $txnAmount }}</td>
        </tr>
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Payment that should have been made :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">{{ $totalPrice }}</td>
        </tr>
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Problem :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">User has not paid full amount due.</td>
        </tr>
    @else
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Problem :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">{{ $reason }}</td>
        </tr>
    @endif
    
    @if(isset($details))
        <tr>
            <td style="border:1px solid #333;width:30%;padding:3px;">Details :</td>
            <td style="border:1px solid #333;width:70%;padding:3px;">{{ $details }}</td>
        </tr>
    @endif
    
</table>
<br><br>
<div>Thank You</div>

