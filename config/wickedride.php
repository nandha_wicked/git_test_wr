<?php
use Illuminate\Support\Facades\Config;

return [

    'buffer' => [
    	'0_24' => 1,
    	'25_48' => 2,
    	'49_00' => 3
    ],

];