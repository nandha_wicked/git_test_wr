var WickedRide = {
	Map : {}
};

$(function(){
	WickedRide.Init();
});

WickedRide.Init = function(){
	WickedRide.Login();
	WickedRide.Signup();
	WickedRide.Map.Init();
}

WickedRide.Map.createMap = function(latitude,longitude){	
	var mapProp = {
		center:new google.maps.LatLng(latitude,longitude),
		scrollwheel: false,
		zoom:13,
		mapTypeId:google.maps.MapTypeId.ROADMAP,
		title: 'Hello World!'
	};
	var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);	
	//var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);	
	    var myLatLng = {lat: latitude, lng: longitude};
	    var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    title: 'Wicked Ride'
	  });
}


WickedRide.Map.Init = function()
{	
	// initilize bangalore location
	/*if($('#googleMap').length)
		WickedRide.Map.createMap(12.920843,77.586568);*/

/* ANTON KOMKOV START */
	if ($(window).width() <= 480) {
		return;
	}
	/* ANTON KOMKOV END */


}	

WickedRide.Login = function(){
	$('#login-form').submit(function(event){
	   event.preventDefault();
	   
	   $.ajax({
	       url: '/booking/front-login',
	       type: "post",
	       data: {'email':$('#login-form input[name=email]').val(), 'password':$('#login-form input[name=password]').val(), 'booking':$('#login-form input[name=booking]').val(),'plan':$('#login-form input[name=plan]').val(), '_token': $('#login-form input[name=_token]').val()},
	       success: function(data){ 
	       		// Build errors to show
				var errorString = '<div class="login-ms">';
					$.each( data.errors, function( key, value) {	        
					errorString += '<div>' + '* ' + value + '</div>';
				});
				errorString += '</div>';

	       		if(data.success)
	       		{
	       			if(data.booking == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						window.location = '/booking/confirm';
	       			}
                    else if(data.plan == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						window.location = '/confirm-plan';
	       			}
	       			else{
	       				//$('#login-form #login-msg').html("Login success.....");
	       				window.location = '/';
	       			}
	       		}else{
	       			var html1 = errorString;
	       			$('#login-form #login-msg').html(html1);
	       		}          
	       }
	   });
	});
}

WickedRide.Signup = function(){
	$('#signup-form').submit(function(event){
		event.preventDefault();

		$.ajax({
			url : '/booking/front-signup',
			type : 'post',
			data : { 
						'firstName':$('#signup-form input[name=firstName]').val(),
						'lastName':$('#signup-form input[name=lastName]').val(),
						'email':$('#signup-form input[name=email]').val(),
						'mobileNumber':$('#signup-form input[name=mobileNumber]').val(),			 
						'password':$('#signup-form input[name=password]').val(),
						'confirmPassword':$('#signup-form input[name=confirmPassword]').val(),
						'booking':$('#signup-form input[name=booking]').val(),
                        'plan':$('#signup-form input[name=plan]').val(),
						'_token': $('#signup-form input[name=_token]').val()
				   },
			success: function(data){

				var errorString = '';
				errorString += '<div class="signup-ms">';
				$.each(data.errors, function(key, value){
					errorString += '<div>'+'* '+ value + '</div>'; 
				});
				errorString += '</div>';

				if(data.success){
                    
                    console.log(data.plan);
                    
					if(data.booking == 1){
						/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						$('#signup-form #signup-msg').html("You have been signed up successfully. We are redirecting ....");
						window.location = '/booking/confirm';
						//window.location = '/booking/confirm';
					}
                    else if(data.plan == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
                        $('#signup-form #signup-msg').html("You have been signed up successfully. We are redirecting ....");
						window.location = '/confirm-plan';
	       			}
					else
						//$('#signup-form #signup-msg').html("You have been signed up successfully.....");
						window.location = '/';		
				}
				else{
					$('#signup-form #signup-msg').html(errorString);	
				}
			},
			beforeSend : function(){
				$('.my-loader').show();
			},
			complete : function(){
				$('.my-loader').hide();
			}	   
		});
	});
}

WickedRide.applyPromocode = function(){
	var ele = $('.apply-promocode');
	var modelId = ele.data('model-id');
	var areaId = ele.data('area-id');
	var cityId = ele.data('city-id');
	var startDate = ele.data('start-date');
	var startTime = ele.data('start-time');
	var endDate = ele.data('end-date');
	var endTime = ele.data('end-time');
	var coupon = $('input[name=promocode]').val();
	$.ajax({
		url : '/api/bookings/total-price',
		type : 'GET',
		headers: { },
		data : { 
		'model_id': modelId,
		'area_id': areaId,
		'city_id': cityId,
		'start_date': startDate,
		'start_time': startTime,
		'end_date': endDate,
		'end_time': endTime,
		'coupon': coupon,
        'device': 'WEB'
		},
		success: function(response){
			var result = response['result']['data'];
			if (result['promocode']['status'] == false){
				$('#promocode-msg').removeClass('success-msg').addClass('error-msg').text(result['promocode']['message']);
				var totalPrice = WickedRide.addVat(result['total_price']);
				$('.book_bike_fleet').find('.book_now .basic_price').text('RS '+result['final_price']);
				$('.book_bike_fleet').find('.book_now .tax').text( Math.round(result['final_price'] * 5.5)/100 +' (VAT 5.5%)');
				$('.book_bike_fleet').find('.book_now .amt').text('RS '+totalPrice);
				return false;
			}
            
            if(result['final_price']==0)
            {
                
                window.location = "/booking/confirmWithPromo?model_id="+modelId+"&area_id="+areaId+"&city_id="+cityId+"&start_date="+startDate+"&start_time="+startTime+"&end_date="+endDate+"&end_time="+endTime+"&coupon="+coupon;
            }

			var finalPrice = WickedRide.addVat(result['final_price']);
			var vatPrice =  Math.round(result['final_price'] * 5.5)/100;
			
			$('#promocode-msg').removeClass('error-msg').addClass('success-msg').text(result['promocode']['message']);
			$('.book_bike_fleet').find('.book_now .basic_price').text('RS '+result['final_price']);
			$('.book_bike_fleet').find('.book_now .tax').text(vatPrice+' (VAT 5.5%)');
			$('.book_bike_fleet').find('.book_now .amt').text('RS '+finalPrice);
		}
	});
}

WickedRide.addVat = function(totalPrice){
	//5.5% VAT
	totalPrice = totalPrice + (totalPrice * 0.055);
	// 2 decimal points round up
	totalPrice = Math.round(totalPrice * 100) / 100;
	return totalPrice;
}
