Booking.ChooseBike = {
  fromDateTimes: [],
  toDateTime: []
};

Booking.ChooseBike.init = function(){
  Booking.ChooseBike.initEventHandlers();
  
}.bind(Booking.ChooseBike);

Booking.ChooseBike.initEventHandlers = function(){
  // ON SUBMIT
  $('.booking-step2-continue').click(function(){
    if($(this).find('div').length)
      return false;
    var ulEl = $(this).closest('.book_bike_fleet ');
    var fromDate = ulEl.find('input[name="start_date"]').val();
    var fromTime = ulEl.find('input[name="start_time"]').val();
    var toDate = ulEl.find('input[name="end_date"]').val();
    var toTime = ulEl.find('input[name="end_time"]').val();
    var modelId = ulEl.find('input[name="modelId"]').val();

    var errEl = $('.step2-error', ulEl);

    if(!fromDate || !toDate || !fromTime || !toTime){
      errEl.text("ERROR : Please Enter Both Date & Time");
      return false;
    }

    var fromDateTime = fromDate + ' ' + fromTime;
    var toDateTime = toDate + ' ' + toTime;
    fromDateTime = new Date(fromDateTime).getTime();
    toDateTime = new Date(toDateTime).getTime();

    if(fromDateTime >= toDateTime){
      errEl.text("ERROR : Start Date cannot be greater than End Date");
      return false;
    }
    var now = new Date().getTime();
    if(fromDateTime <= now || toDateTime <= now){
      errEl.text("ERROR : Selected time has already passed!");
      return false;
    }
    

      

    errEl.text("");
    $('#show-bikes form[data-model-id='+ modelId +']').submit();
    return false;

  });
  $(".no-border .edit-button").click(function(){
    Booking.ChooseBike.makeEditable(this);
  });

  $(".book_now .avail-btn").click(function(){
    var checkButton = $(this).closest('ul.book_bike_fleet').find('.no-border .edit-button');
    Booking.ChooseBike.makeEditable(checkButton);
  }); 

  $('select[name="areaName"]').change(function(){
    var el = $(this).closest('.book_bike_fleet');
    var fromDateEl = el.find('input[name="start_date"]');
    var fromTimeEl = el.find('input[name="start_time"]');
    var toDateEl = el.find('input[name="end_date"]');
    var toTimeEl = el.find('input[name="end_time"]');
    
    Booking.ChooseBike.removePrice(el);
    Booking.ChooseBike.getPriceDetails(this);
    Booking.ChooseBike.makeEditable(this);
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.makeEditable = function(el){
  Booking.ChooseBike.removeEditable();
  Booking.ChooseBike.initDateTimePickers(el);
  var start = $(el).closest('.date-time-calendar').find('.startDatePicker').addClass("border");
  $(el).closest('.date-time-calendar').find('.startTimePicker').addClass("border");
  $(el).closest('.date-time-calendar').find('.endDatePicker').addClass("border");
  $(el).closest('.date-time-calendar').find('.endTimePicker').addClass("border");
}

Booking.ChooseBike.removeEditable = function(){
  $('.startDatePicker, .startTimePicker, .endDatePicker, .endTimePicker').removeClass("border");
}

Booking.ChooseBike.initDateTimePickers = function(el){
 
    var that = $(el).closest('.book_bike_fleet');
    var start_date = $('input[name=start_date]', that).val();
    var start_time = $('input[name=start_time]', that).val();
    ct = new Date(start_date+' '+start_time);
    Booking.ChooseBike.getFromDates(ct,that);
    Booking.ChooseBike.getToDate(that);
}.bind(Booking.ChooseBike);

Booking.ChooseBike.initFromDatePicker = function(ct, el){
  var fromDateEl = el.find('input[name="start_date"]');
  var modelId = el.find('input[name="modelId"]').val();
  var areaId = el.find('select[name="areaName"]').val();

  var datetimes = Booking.ChooseBike.fromDateTimes[ct.dateFormat('Y-m')+"-"+modelId+"-"+areaId];
  var highlightedDates = Booking.ChooseBike.formatHighlightedDates(datetimes);
  var disabledDates = Booking.ChooseBike.getNotAvailableDates(datetimes);
  var lastDate = new Date(datetimes[datetimes.length-1].date).dateFormat('Y/m/d');
  var cityId = $('#show-bikes').data('city-id');
    
    
  $.ajax({
				url : '/api/day-offs',
				type : 'GET',
				data : { 
                    'city_id': cityId
				},
				success: function(data1){
                    
                    var data1arr = data1['result']['data'];
                    for (var i=0; i<data1arr.length; i++)
                    {
                        data1arr[i] = parseInt(data1arr[i], 10);
                    }

                  $(fromDateEl).datetimepicker({
                    timepicker: false,
                    scrollInput: false,
                    format:'d M Y',
                    defaultDate: ct.dateFormat('d M Y'),
                    minDate: 0,
                    maxDate: lastDate,
                    highlightedDates: highlightedDates,
                    disabledWeekDays: data1arr,
                    beforeShowDay: function(date){
                        if($.inArray(date.dateFormat('Y-m-d'), disabledDates) !== -1){
                          return [false, ""];
                        }
                        return [true, ""];
                    },
                    onChangeMonth: function(selectedDateTime, ele){
                      var el = $(ele).closest('.book_bike_fleet');
                      Booking.ChooseBike.getFromDates(selectedDateTime,el);
                    },
                    onSelectDate: function(selectedDateTime, ele){
                      var el = $(ele).closest('.book_bike_fleet');
                    
                      var fromTimeEl = el.find('input[name="start_time"]');
                      var toDateEl = el.find('input[name="end_date"]');
                      var toTimeEl = el.find('input[name="end_time"]');

                      // CLEAR OTHER DATE-TIME SELECTIONS
                      fromTimeEl.val('');
                      toDateEl.val('');
                      toDateEl.datetimepicker('destroy');
                      toTimeEl.val('');
                      toTimeEl.datetimepicker('destroy');
                      Booking.ChooseBike.removePrice(el);
                      Booking.ChooseBike.initFromTimePicker(el);
                      $(fromTimeEl).datetimepicker('show').focus();
                    }
                  });
                  if($(fromDateEl).hasClass('border'))
                   $(fromDateEl).datetimepicker('show').focus();

                  Booking.ChooseBike.initFromTimePicker(el);
                }
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.getSlotsByDate = function(date, modelId, areaId){
  var fromDateObj = new Date(date);
  var datetimes = Booking.ChooseBike.fromDateTimes[fromDateObj.dateFormat('Y-m')+"-"+modelId+"-"+areaId];
  $.each(datetimes, function(i,datetime){
    if(datetime.date == date){
      return slots = datetime.slots;
    }
  });
  return slots;
}

Booking.ChooseBike.formatAvailableSlots = function(slots){
  var formattedAvailableSlots = [];
  $.each(slots, function(i, slot){
    if(slot.status){
      var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
      formattedAvailableSlots.push(slotStartStr);
    }
  });
  return formattedAvailableSlots;
}

Booking.ChooseBike.getNotAvailableDates = function(dates){
  var notAvailableDates = [];
  $.each(dates, function(i, datetime){
    if(datetime.status == "N"){
      notAvailableDates.push(datetime.date);
    }
  });
  return notAvailableDates;
}

Booking.ChooseBike.initFromTimePicker = function(el){
  var fromDateEl = el.find('input[name="start_date"]');
  var modelId = el.find('input[name="modelId"]').val();
  var areaId = el.find('select[name="areaName"]').val();
  var now = new Date();
  var fromDate = fromDateEl.val();
  var fromDateObj = new Date(fromDate);

  var slots = Booking.ChooseBike.getSlotsByDate(fromDateObj.dateFormat('Y-m-d'), modelId, areaId);
  var allowedTimes = Booking.ChooseBike.formatAvailableSlots(slots);
  var fromTimeEl = el.find('input[name="start_time"]');
  var fromTime = fromTimeEl.val();
  var minTime = slots[0].start_time.substring( 0, slots[0]['start_time'].lastIndexOf( ":00" ) );
  var maxTime = slots[slots.length-1].end_time.substring( 0, slots[slots.length-1]['start_time'].lastIndexOf( ":00" ) );

  if(fromDate == now.dateFormat('d M Y')){
    minTime = now.dateFormat('H:i');
  }

  $(fromTimeEl).datetimepicker({
    datepicker: false,
    scrollInput: false,
    format: 'H:i',
    minTime: minTime,
    maxTime: maxTime,
    defaultTime: fromTime,
    allowTimes: allowedTimes,
    onSelectTime: function( dt, ele ){
      var el = $(ele).closest('.book_bike_fleet');
     
      var toDateEl = el.find('input[name="end_date"]');
      var toTimeEl = el.find('input[name="end_time"]');

      toDateEl.val('');
      toTimeEl.val('');
     
      Booking.ChooseBike.removePrice(el);
      Booking.ChooseBike.getToDate(el,true);
     
    }
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.initToDatePicker = function(el, endDate, open){
  var fromDateEl = $(el).find('input[name="start_date"]');
  var fromDate = fromDateEl.val();
  var fromDateObj = new Date(fromDate);
  var fromTime = $(el).find('input[name="start_time"]').val();
  var toTime = $(el).find('input[name="end_time"]').val();

  var toDateEl = $(el).find('input[name="end_date"]');
  var toDate = toDateEl.val();
  var toDateObj = new Date(toDate);
  var modelId = $(el).find('input[name="modelId"]').val();
  var areaId = $(el).find('select[name="areaName"]').val();

  var minDate = fromDateObj.dateFormat('Y/m/d');
  var defaultDate = toDateObj.dateFormat('Y/m/d');
 
  var maxDate = new Date(endDate);
  var maxDateObj = new Date(maxDate);
  var maxDateStr = maxDateObj.dateFormat('Y/m/d');
  var cityId = $('#show-bikes').data('city-id');
  $.ajax({
				url : '/api/day-offs',
				type : 'GET',
				data : { 
                    'city_id': cityId
				},
				success: function(data1){
                    
                    var data1arr = data1['result']['data'];
                    for (var i=0; i<data1arr.length; i++)
                    {
                        data1arr[i] = parseInt(data1arr[i], 10);
                    }

                      $(toDateEl).datetimepicker({
                        timepicker: false,
                        scrollInput: false,
                       
                        format:'d M Y',
                        minDate: minDate,
                        maxDate: maxDateStr,
                        disabledWeekDays: data1arr,
                        onSelectDate: function(selectedDateTime, ele){
                          var el = $(ele).closest('.book_bike_fleet');
                         
                          var toTimeEl = el.find('input[name="end_time"]');
                          toTimeEl.val('');
                          
                          Booking.ChooseBike.removePrice(el);
                          Booking.ChooseBike.initToTimePicker(el, true);
                        }
                      });
                     if(open == true)
                         {
                             $(toDateEl).datetimepicker('show').focus();
                         }
                      if(toTime != "")
                        Booking.ChooseBike.initToTimePicker(el);
                }
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.initToTimePicker = function(el,open){
  var fromDateEl = el.find('input[name="start_date"]');
  var fromDate = fromDateEl.val();
  var fromDateObj = new Date(fromDate);
  var fromTime = $(el).find('input[name="start_time"]').val();
  var toDateEl = el.find('input[name="end_date"]');
  var toDate = toDateEl.val();
  var toDateObj = new Date(toDate);
  var toDateStr = toDateObj.dateFormat('Y/m/d');

  var modelId = $(el).find('input[name="modelId"]').val();
  var areaId = $(el).find('select[name="areaName"]').val();
  var maxDate = Booking.ChooseBike.toDateTime[fromDateObj.dateFormat('Y-m')+"-"+modelId+"-"+areaId+'-'+fromTime].end_date;
  var maxDateObj = new Date(maxDate);
  var maxDateStr = maxDateObj.dateFormat('Y/m/d');
  var lastSlot = Booking.ChooseBike.toDateTime[fromDateObj.dateFormat('Y-m')+"-"+modelId+"-"+areaId+'-'+fromTime].last_slot;
  lastSlot = lastSlot['start_time'].substring( 0, lastSlot['start_time'].lastIndexOf( ":00" ) );
  var cityId = $('#show-bikes').data('city-id');

  $.ajax({
        url : '/api/to-working-hours',
        type : 'get',
        data : { 
          'date': toDate,
          'city_id': cityId
        },
        ele: el,
        open:open,
        toDateObj: toDateObj,
        maxDateObj: maxDateObj,
        lastSlot: lastSlot,
        success: function(data){
          var el = $(this.ele).closest('.book_bike_fleet');
          var fromTimeEl = el.find('input[name="start_time"]');
          var fromTime = fromTimeEl.val();
          var fromDateTime = fromDate + ' ' + fromTime;
          var fromDateTimeObj = new Date(fromDateTime);
          var toTimeEl = el.find('input[name="end_time"]');

          var slots = data['result']['data']['slots'];
          var allowedTimes = Booking.ChooseDate.formatSlots(slots);
          
          if(!slots){
            return false;
          }

          if(this.toDateObj.dateFormat('Y/m/d') == this.maxDateObj.dateFormat('Y/m/d')){
            var newAllowedTimes = [];
            $.each(allowedTimes,function(i,allowedTime){
              if(allowedTime == lastSlot){
                newAllowedTimes.push(allowedTime);
                return false;
              }
              newAllowedTimes.push(allowedTime);
            });
            allowedTimes = newAllowedTimes;
          }
          var startTime = allowedTimes[0];
          
          // CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
          if(toDateObj.dateFormat('d M Y') == fromDateTimeObj.dateFormat('d M Y')){
            var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
            nextHour = new Date(nextHour);
            startTime = nextHour.dateFormat('H:i');
          }
            
          $(toTimeEl).datetimepicker({
            datepicker: false,
            scrollInput: false,
            format: 'H:i',
            defaultTime: startTime,
            minTime: startTime,
            allowTimes: allowedTimes,
            onSelectTime: function( dt, ele ){
              var el = $(ele).closest('.book_bike_fleet');
            
              Booking.ChooseBike.removeEditable();
              Booking.ChooseBike.getPriceDetails(el);
            }
          });
          if(this.open === true)
            $(toTimeEl).datetimepicker('show').focus();
        },
      });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.getPriceDetails = function(el){
  var ulEl = $(el).closest('.book_bike_fleet');
  var modelId = ulEl.find('input[name="modelId"]').val();
  var areaId = ulEl.find('select[name="areaName"]').val();
  var cityId = $('#show-bikes').data('city-id');
  var fromDateEl = ulEl.find('input[name="start_date"]');
  var fromDate = fromDateEl.val();
  var fromTimeEl = ulEl.find('input[name="start_time"]');
  var fromTime = fromTimeEl.val();
  var toDateEl = ulEl.find('input[name="end_date"]');
  var toDate = toDateEl.val();
  var toTimeEl = ulEl.find('input[name="end_time"]');
  var toTime = toTimeEl.val();

  if(toDate == '' || toTime == '' || fromDate == '' || fromTime == '' || areaId == '' || modelId == '' || cityId == ''){
    if(toDate == '')
      toDateEl.focus();
    if(toTime == '')
      toTimeEl.focus();
    if(fromDate == '')
      fromDateEl.focus();
    if(fromTime == '')
      fromTimeEl.focus();
    return;
  }

  $.ajax({
    url : '/api/bookings/price-and-check-availability',
    type : 'GET',
    data : { 
      'model_id': modelId,
      'area_id': areaId,
      'city_id': cityId,
      'start_date': fromDate,
      'start_time': fromTime,
      'end_date': toDate,
      'end_time': toTime
    },
    ele: ulEl,
    success: function(response){
      var result = response['result']['data'];
      if(!result){
        return false;
      }
      this.ele.find('input[name=totalPrice]').attr('value', result['total_price']);
      this.ele.find('.total-price').text('RS '+result['total_price']);
      this.ele.find('.effective-price').text('RS '+result['effective_price']);
      this.ele.find('.message_line').text(result['minimum_billing_message']);
      this.ele.find('input[name=number_of_hours]').attr('value', result['number_of_hours']);
      this.ele.find('input[name=minimum_hours]').attr('value', result['minimum_hours']);

      

      if(result['bike_id'] == "none"){
        this.ele.find('.choose-bike-cta .booking-step2-continue').removeClass('show');
        this.ele.find('.choose-bike-cta .sold-out-wrapper').addClass('show');
      }
      else{
        this.ele.find('.choose-bike-cta .booking-step2-continue').addClass('show');
        this.ele.find('.choose-bike-cta .sold-out-wrapper').removeClass('show');
      }
    }
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.getFromDates = function(ct, el){

  var modelId = $(el).find('input[name="modelId"]').val();
  var areaId = $(el).find('select[name="areaName"]').val();
  var cityId = $('#show-bikes').data('city-id');
  var fromDateObj = new Date(ct);
  var now = new Date();
 
  $.ajax({
    url : '/api/from-availability-calendar-web',
    type : 'GET',
    data : { 
      'model_id': modelId,
      'area_id': areaId,
      'city_id': cityId,
      'month': fromDateObj.dateFormat('m'),
      'year': fromDateObj.dateFormat('Y')
    },
    ct: ct,
    el: el,
    modelId: modelId,
    areaId: areaId,
    success: function(response){
      var result = response['result']['data'];
      if(!result){
        return false;
      }
      Booking.ChooseBike.fromDateTimes[this.ct.dateFormat('Y-m')+'-'+this.modelId+'-'+this.areaId] = result;
      Booking.ChooseBike.initFromDatePicker(this.ct, this.el);
      
    }
  });
}.bind(Booking.ChooseBike);


Booking.ChooseBike.getToDate = function(el,open){
  var modelId = $(el).find('input[name="modelId"]').val();
  var areaId = $(el).find('select[name="areaName"]').val();
  var fromDate = $(el).find('input[name="start_date"]').val();
  var fromTime = $(el).find('input[name="start_time"]').val();
  var fromDateObj = new Date(fromDate);
  var cityId = $('#show-bikes').data('city-id');

  $.ajax({
    url : '/api/to-availability-calendar',
    type : 'GET',
    data : { 
      'model_id': modelId,
      'area_id': areaId,
      'start_date': fromDate,
      'start_time': fromTime,
      'city_id':cityId
    },
    el: el,
    open:open,
    areaId:areaId,
    modelId:modelId,
    fromDateObj:fromDateObj,
    fromTime:fromTime,
    success: function(response){
      var toDateEl = $(this.el).find('input[name="end_date"]');
      var result = response['result']['data'];
     
      if(!result){
        return false;
      }

      var endDateObj = new Date(result['end_date']);
      var highlightedPeriods = new Array();
      Booking.ChooseBike.toDateTime[this.fromDateObj.dateFormat('Y-m')+'-'+this.modelId+'-'+this.areaId+'-'+fromTime] = result;
      highlightedPeriods.push(fromDateObj.dateFormat('Y/m/d') + ','+ endDateObj.dateFormat('Y/m/d') + ',Available,xdsoft_highlighted_mint');
      if(this.open==true)
          {
              Booking.ChooseBike.initToDatePicker(this.el, result['end_date'], true);
              $(toDateEl).datetimepicker('show').focus();
          }
        
      
    }
  });
}.bind(Booking.ChooseBike);

Booking.ChooseBike.formatHighlightedDates = function(data){
  var highlightedDates = new Array();
  var states = {
    "A": "Available,xdsoft_highlighted_mint",
    "P": "Partially Available,xdsoft_highlighted_default",
    "N": "Sold Out,xdsoft_highlighted_red",
    "G": "Holiday, xdsoft_highlighted_grey"
  };
  $.each(data, function(i, day){
    highlightedDates.push(new Date(day['date']).dateFormat('Y/m/d') + ','+states[day['status']]);
  });
  return highlightedDates;
}.bind(Booking.ChooseBike);

Booking.ChooseBike.removePrice =  function(el){
  ele = $(el);
  ele.find('.total-price').text('RS -');
  ele.find('.effective-price').text('RS -');
}.bind(Booking.ChooseBike);
