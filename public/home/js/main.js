var Main = {
	HighlightedDates:[],
};
var	Helpers = {};
var Booking = {};

Main.init = function(){
	Helpers.init();
	Main.emailSubscriptionCheck();
	Main.contactFormValidation();
}.bind(Main);

/*HELPER FUNCTIONS*/

Helpers.init = function(){
	//Date addHours helper
	Date.prototype.addHours= function(h){
		this.setHours(this.getHours()+h);
		return this;
	}

}.bind(Helpers);

Helpers.isDateValid = function(date){
	// FIREFOX DOESNT SUPPORT DD-MMM-YYY format
	date = date.replace(/-/g, " ");
	var ts = new Date(date).getTime();
	return isNaN(ts)?false:true;
}

/*MAIN FUNCTIONS*/

Main.ValidDate = function(date){
	var ts = new Date(date).getTime();
	if(isNaN(ts))
		return false;
	else
		return true;
}.bind(Main);

Main.formattedDate = function(date){
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate()),
        year = d.getFullYear();
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}.bind(Main);

Main.formattedTime = function(date){
	var d = new Date(date || Date.now()),
        hour = '' + (d.getHours()),
        min = '' + (d.getMinutes());

    if (hour.length < 2) month = '0' + hour;
    if (min.length < 2) day = '0' + min;

    return [hour, min].join(':0');
}.bind(Main);

Main.dbFormattedDate = function(date){
	var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate()),
        year = d.getFullYear();
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}.bind(Main);

Main.dbFormattedTime = function(time){
	var arr = time.split(":", 2);
	var fTime = arr.join(':');
    return fTime;
}.bind(Main);

Main.getMonth = function(date){
	var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate()),
        year = d.getFullYear();
    
    if (month.length < 2) month = '0' + month;
    return month;
}.bind(Main);

Main.getYear = function(date){
	var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate()),
        year = d.getFullYear();
    
    return year;
}.bind(Main);

Main.testFormat = function(date){
	data = date.replace(/-/g, ".");
	return data;
}.bind(Main);

Main.getFromCalendar = function(){
	$( ".book_bike_fleet" ).each(function() {
		var that = $(this);
		Main.getFromCalendarOneModel(that);
		Main.getToCalendarOneModel(that);
	});
}.bind(Main);

Main.getFromCalendarOneModel = function(element){
	var model_id = $(element).attr('data-model-id');
	var area_id = $(element).attr('data-area-id');
	var month = $(element).attr('data-month');
	var year = $(element).attr('data-year');
	var highlightedDates = new Array();
    var cityId = $(element).attr("data-city-id");
    $.ajax({
				url : '/api/day-offs',
				type : 'GET',
				data : { 
                    'city_id': city_id
				},
				success: function(data1){
                    
                    var data1arr = data1['result']['data'];
                    for (var i=0; i<data1arr.length; i++)
                    {
                        data1arr[i] = parseInt(data1arr[i], 10);
                    }
                $.ajax({
                    url : '/api/from-availability-calendar-web',
                    type : 'get',
                    data : { 
                        'model_id': model_id,
                        'area_id': area_id,
                        'month': month,
                        'year': year,
                        'city_id':cityId
                    },
                    success: function(res){
                        var range = res['result']['data'];
                        for (var i = 0; i < (range.length); i++) {
                            if(range[i]['status'] == 'A'){
                                highlightedDates.push(Main.dbFormattedDate(range[i]['date']) + ',available,' + 'xdsoft_highlighted_mint');
                            }else if(range[i]['status'] == 'P'){
                                highlightedDates.push(Main.dbFormattedDate(range[i]['date']) + ',partially available,' + 'xdsoft_highlighted_default');
                            }else{
                                highlightedDates.push(Main.dbFormattedDate(range[i]['date']) + ',sold out,' + 'xdsoft_highlighted_red');
                            }
                        }
                        Main.HighlightedDates = highlightedDates;
                        Main.ChooseBike.fromDatePicker(element, highlightedDates, range, model_id, area_id);
                        Main.ChooseBike.fromTimePicker(element, highlightedDates, range, model_id, area_id);
                    }
                });
        
                }
    });
}.bind(Main);

Main.getToCalendarOneModel = function(element){
	var modelId = $(element).attr('data-model-id');
	var areaId = $(element).attr('data-area-id');
	var startDate = $(element).attr('data-start-date');
	var startTime = $(element).attr('data-start-time');
    var cityId = $(element).attr("data-city-id");
	var highlightedDates = new Array();

	$.ajax({
		url : '/api/to-availability-calendar',
		type : 'get',
		data : { 
			'model_id': modelId,
			'area_id': areaId,
			'start_date': startDate,
			'start_time': startTime,
            'city_id':cityId
		},
		success: function(data){
			var endDateData = data['result']['data'];
			var highlightedPeriod = new Array();
			var lastSlot = Main.dbFormattedTime(endDateData['last_slot']['end_time']);
			
			highlightedPeriod.push(startDate + ','+ endDateData['end_date'] + ',Available,xdsoft_highlighted_mint');
				Main.ChooseBike.toDatePicker(element, highlightedPeriod, endDateData['end_date']);
				Main.ChooseBike.toTimePicker(element, endDateData['end_date'], lastSlot, startTime);
		}
	});
}.bind(Main);

Main.callTotalPriceApi = function(el){
	var modelId = $(el).attr("data-model-id");
	var cityId = $(el).attr("data-city-id");
	var areaId = $(el).attr("data-area-id");
	var startDate = $(el).attr("data-start-date");
	var endDate = $(el).attr("data-end-date");
	var startTime = $(el).attr("data-start-time");
	var endTime = $(el).attr("data-end-time");
	
	$.ajax({
		url : '/api/bookings/price-and-check-availability',
		type : 'get',
		data : { 
			'model_id': modelId,
			'city_id': cityId,
			'area_id': areaId,
			'start_date': startDate,
			'start_time': startTime,
			'end_date': endDate,
			'end_time': endTime
		},
		success: function(data){
			var totalPrice = data['result']['data']['total_price'];
			var effectivelPrice = data['result']['data']['effective_price'];
			var availableBike = data['result']['data']['bike_availability'];
			var bike = data['result']['data']['bike_id'];
			if(bike == "none"){
				$(el).closest('.book_bike_fleet').find('.common_btn_small').removeClass('book-btn').addClass('booked-btn');
				$(el).closest('.book_bike_fleet').find('.common_btn_small a').text('Sold Out');
			}else{
				$(el).closest('.book_bike_fleet').find('.common_btn_small').removeClass('booked-btn').addClass('book-btn');
				$(el).closest('.book_bike_fleet').find('.common_btn_small a').text('Book');
			}

			$(el).closest('.book_bike_fleet').attr('data-total-price', totalPrice);
			$(el).closest('.book_bike_fleet').find('input[name=totalPrice]').attr('value', totalPrice);
			$(el).closest('.book_bike_fleet').find('.price[data-model-id="' + modelId + '"]').text('RS '+totalPrice);
			$(el).closest('.book_bike_fleet').find('.effective-price').text('RS '+effectivelPrice);
		}
	});
}.bind(Main);

Main.emailSubscriptionCheck = function(){
	$('form[name=subscription]').submit(function(){
		var email = $('input[name=email]', this).val();
		if($.trim(email) == ""){
			alert("Please enter the email to continue.");
			return false;
		}
		else{
			return true;
		}
	});
}.bind(Main);

Main.contactFormValidation = function(){
	$('form[name=contact-form]').submit(function(){
		var firstName = $('input[name=firstName]', this).val();
		var email = $('input[name=email]', this).val();
		var mobile = $('input[name=mobileNumber]', this).val();
		var comment = $('textarea[name=comment]', this).val();
		if($.trim(firstName) == ""){
			alert("Please enter the first name to continue.");
			return false;
		}
		if($.trim(email) == ""){
			alert("Please enter the email to continue.");
			return false;
		}
		if($.trim(mobile) == ""){
			alert("Please enter the mobile number to continue.");
			return false;
		}
		if($.trim(comment) == ""){
			alert("Please enter the comment to continue.");
			return false;
		}

		return true;
	});
}.bind(Main);
