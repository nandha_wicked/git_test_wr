var WickedRide = {

};

$(function(){
	WickedRide.Init();
	$('#loader').click(function(){
		if($('#loader').hasClass('show')){
			$('#loader').removeClass('show');
		}
	});
});

WickedRide.Init = function(){

	WickedRide.CheckAdminBikeAvailability();
}


function ValidDate(date){
	var ts=new Date(date).getTime();
	if(isNaN(ts))
		return false;
	else
		return true;
}

WickedRide.CheckAdminBikeAvailability = function(){

	$('#addBooking-modal form button[type=submit]').click(function(event){
		event.preventDefault();


		var fromDt = $('#addBooking-modal #toValidateFromDate').val();
		var toDt = $('#addBooking-modal #toValidateToDate').val();

		var fromTs = new Date(fromDt).getTime()/1000;
		var toTs = new Date(toDt).getTime()/1000;	

		var fmObj = new Date(fromDt);
		var toObj = new Date(toDt);

		var fmSec = fmObj.getSeconds();
		var toSec = toObj.getSeconds();

		var newFmTs = fromTs - fmSec;
		var newToTs = toTs - toSec;	

		if(newFmTs > newToTs)
			$('.adminBikeAvailabilityError').text("ERROR : From Date greater than To Date");
		else if(newFmTs == newToTs)
			$('.adminBikeAvailabilityError').text("ERROR : Please choose some other date");
		else if(ValidDate(fromDt) == false || ValidDate(toDt) == false)
			$('.adminBikeAvailabilityError').text("ERROR : Please Enter Date");
		else{
			$.ajax({
				 url : 'booking/bikeAvailability',
				 type : 'post',
				 data : {
				 			'fromDate' : $('#addBooking-modal form input[name=fromDate]').val(), 
				 			'toDate' : $('#addBooking-modal form input[name=toDate]').val(),
				 			'modelId' : $('#addBooking-modal form select[name=bike] option:selected').data('modelid'),
				 			'areaId' : $('#addBooking-modal form select[name=bike] option:selected').data('areaid'),
				 			'_token' : $('meta[name=_token]').attr('content')
				 		},
				 success : function(data){
				 	if(data == 1){
				 		$('#addBooking-modal form #model_id').val($('#addBooking-modal form select[name=bike] option:selected').data('modelid'));
				 		$('#addBooking-modal form #area_id').val($('#addBooking-modal form select[name=bike] option:selected').data('areaid'));
				 		$('#addBooking-modal form').submit();
				 	}
				 	else if(data == 0){
				 		$('#addBooking-modal').show('modal');
				 		$('.adminBikeAvailabilityError').html('Sold Out');
				 	}
				 } 			 	
			});
		}

	});
}