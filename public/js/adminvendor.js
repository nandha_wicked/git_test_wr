var Adminvendor = {
	Profile:{},
	PromoCode: {}
};

Adminvendor.Profile.init = function(){
	Adminvendor.Profile.editBikeModel();
}.bind(Adminvendor.Profile);

Adminvendor.Profile.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var first_name = $(this).data('firstname');
		var last_name = $(this).data('lastname');
		var email = $(this).data('email');
		var mobile_num = $(this).data('mobilenum');
		var Bank = $(this).data('bank');
		var Bank_Account_Number = $(this).data('bankaccountnumber');
		var IFSC = $(this).data('ifsc');
		var PAN = $(this).data('pan');
		var TAN = $(this).data('tan');
		var SRN = $(this).data('srn');
		var CIN = $(this).data('cin');
		var TIN = $(this).data('tin');
		var Overall_Rating = $(this).data('overallrating');
		var Vendor_Image = $(this).data('vendorimage');
		var Contact_person = $(this).data('contactperson');
		var Contact_Phone_Number = $(this).data('contactphonenumber');
		var Door_No = $(this).data('doorno');
		var Street_1 = $(this).data('streetone');
		var Street_2 = $(this).data('streettwo');
		var Area = $(this).data('area');
		var City = $(this).data('city');
		var State = $(this).data('state');
		var Country = $(this).data('country');
		var ZipCode = $(this).data('zipcode');
		var LandLine_Number = $(this).data('landlinenumber');
		var Phone_Number = $(this).data('phonenumber');
		var Alternate_Number = $(this).data('alternatenumber');
		var Website = $(this).data('website');
		var about_me = $(this).data('about_me');
		var Status = $(this).data('status');
		var coverImgHolder = '<img src="'+Vendor_Image+'" width="50px" />';

		
		$('#editBikeModel-modal form #first_name').html(first_name);
		$('#editBikeModel-modal form #last_name').html(last_name);
		$('#editBikeModel-modal form #email').html(email);
		$('#editBikeModel-modal form #mobile_num').html(mobile_num);
		$('#editBikeModel-modal form #Bank').html(Bank);
		$('#editBikeModel-modal form #Bank_Account_Number').html(Bank_Account_Number);
		$('#editBikeModel-modal form #IFSC').html(IFSC);
		$('#editBikeModel-modal form #PAN').html(PAN);
		$('#editBikeModel-modal form #TAN').html(TAN);
		$('#editBikeModel-modal form #SRN').html(SRN);
		$('#editBikeModel-modal form #CIN').html(CIN);
		$('#editBikeModel-modal form #TIN').html(TIN);	
		$('#editBikeModel-modal form #Contact_person').html(Contact_person);		
		$('#editBikeModel-modal form #Contact_Phone_Number').html(Contact_Phone_Number);		
		$('#editBikeModel-modal form #Door_No').html(Door_No);		
		$('#editBikeModel-modal form #Street_1').html(Street_1);		
		$('#editBikeModel-modal form #Street_2').html(Street_2);		
		$('#editBikeModel-modal form #Area').html(Area);		
		$('#editBikeModel-modal form #City').html(City);		
		$('#editBikeModel-modal form #State').html(State);		
		$('#editBikeModel-modal form #Country').html(Country);		
		$('#editBikeModel-modal form #ZipCode').html(ZipCode);
		$('#editBikeModel-modal form #LandLine_Number').html(LandLine_Number);		
		$('#editBikeModel-modal form #Phone_Number').html(Phone_Number);		
		$('#editBikeModel-modal form #Alternate_Number').html(Alternate_Number);	
		$('#editBikeModel-modal form #Website').html(Website);		
		$('#editBikeModel-modal form #about_me').html(about_me);		
		
		$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);		

		$('#editBikeModel-modal').modal('show');
	});
}.bind(Adminvendor.Profile);

