var Partner = {
	Add:{},
	Edit:{}
};

var options = [];

Partner.init = function(){
	Partner.initHandlers();
    Partner.editPartnerModel();
	
}.bind(Partner);

Partner.initHandlers = function(){
	// SELECT ALL MODELS CHECKBOX
	$('.modal').on('hidden.bs.modal', function(e){ 
		$("form", this).get(0).reset();
    }) ;
    $('#addPartnerModal').on('shown.bs.modal', function(e){ 
		Partner.chooseRangeOrSpecificDates.perform();
    }) ;
	$( '.dropdown-menu a' ).on( 'click', function( event ) {
		var $target = $( event.currentTarget ),
			val = $target.attr( 'data-value' ),
			$inp = $target.find( 'input' ),
			idx;


		if ( ( idx = options.indexOf( val ) ) > -1 ) {
			options.splice( idx, 1 );
			setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
		} else {
			options.push( val );
			setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
		}
		
		if($(this).hasClass('make-name')){
			var status = $(this).find('input[type=checkbox][name=make-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.model-ids').prop("checked", status);
		}
		if($(this).hasClass('city-name')){
			var status = $(this).find('input[type=checkbox][name=city-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.area-ids').prop("checked", status);
		}
		
		$( event.target ).blur();

		return false;
	});
    
    
    $('#startDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
			minDate: new Date(2014,01,01),
			
		});

}.bind(Partner);

Partner.editPartnerModel = function(){
	$('.partner_edit_btn').click(function(){
		// EMPTY MODEL AND AREA CHECKBOX, VALUES WERE GETTING RETAINED FROM PREVIOUS OPENED MODAL
		var id = $(this).data('id');
		
		var modelIds = $(this).data('model-ids');
		var areaIds = $(this).data('area-ids');
        
        var fname = $(this).data('fname');
        
		

		var formEl = $('form[name="edit-partner"]');

		formEl.attr("action","/admin/partner/edit/"+id);
        
        formEl.find('input[name=fname]').val(fname);
		

		if (modelIds !== null) {
			modelIds = String(modelIds);
			var modelIdsArr = modelIds.split(',');
		}
		
		if (areaIds !== null) {
			areaIds = String(areaIds);
			var areaIdsArr = areaIds.split(',');
		}

		$.each(modelIdsArr, function( index, value ) {
			var modelEl = formEl.find('input[name="model_ids[]"][value="'+value+'"]');
			modelEl.prop("checked", true);
			if (modelEl.is(':checked')){
				modelEl.closest('ul').siblings().find('input[name=make-name]').prop('checked', true);
			}
		});
		
		$.each(areaIdsArr, function( index, value ) {
			var areaEl = formEl.find('input[name="area_ids[]"][value="'+value+'"]');
			areaEl.prop("checked", true);
			if (areaEl.is(':checked')) {
				areaEl.closest('ul').siblings().find('input[name=city-name]').prop('checked', true);
			}
		});

		
		$('#editPartnerModal').modal('show');
	});
}.bind(Partner);






