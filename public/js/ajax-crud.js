$(document).ready(function(){

    var url = "/tasks";

    //display modal form for task editing
    $('.open-modal').click(function(){
        var id = $(this).val();

        $.get(url + '/' + id, function (data) {
            //success data
            console.log(data);
            $('#id').val(data.id);
            $('#first_name').val(data.first_name);
            $('#last_name').val(data.last_name);
            $('#email_id').val(data.email);
            $('#dob').val(data.dob);
            $('#old_password').val(data.old_password);
            $('#password').val(data.password);
            $('#mobile_num').val(data.mobile_num);
            $('#gender').val(data.gender);
            $('#location').val(data.location);
            $('#profile_img_id').val(data.profile_img_id);
			 $('#work_email').val(data.work_email);
            $('#btn-save').val("update");

         //   $('#myModal').modal('show');
        }) 
    });

    //display modal form for creating new task
    $('#btn-add').click(function(){
        $('#btn-save').val("add");
        $('#frmTasks').trigger("reset");
        $('#myModal').modal('show');
    });

    //delete task and remove it from list
    $('.delete-task').click(function(){
        var id = $(this).val();

        $.ajax({

            type: "DELETE",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);

                $("#task" + id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    //create new task / update existing task
    $("#btn-save").click(function (e) {

        var password =$('#password').val();
        var confirmPassword = $('#cpassword').val();
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault(); 

        var formData = {
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            email: $('#email_id').val(),
            mobile_num: $('#mobile_num').val(),
            old_password: $('#old_password').val(),
            password: $('#password').val(),
            cpassword: $('#cpassword').val(),
            dob: $('#dob').val(),
            gender: $('#gender').val(),
            location: $('#location').val(),
			work_email: $('#work_email').val(),
            profile_img_id: $('#profile_img_id').val(),
        }

        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();

        var type = "POST"; //for creating new resource
        var id = $('#id').val();;
        var my_url = url;

        if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/' + id;
        }

        console.log(formData);

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);

                var task = '<tr id="task' + data.id + '"><td>' + data.id + '</td><td>' + data.first_name + '</td><td>' + data.last_name + '</td><td>' + data.email + '</td><td>' + data.mobile_num + '</td><td>' + data.dob + '</td><td>' + data.gender + '</td><td>' + data.location + '</td><td>' + data.old_password + '</td><td>' + data.password + '</td><td>' + data.cpassword + '</td><td>' + data.profile_img_id + '</td><td>' + data.created_at + '</td>';
                task += '<td><button class="btn btn-warning btn-xs btn-detail open-modal" value="' + data.id + '">Edit</button>';

                if (state == "add"){ //if user added a new record
                    $('#tasks-list').append(task);
                }else{ //if user updated an existing record

                    $("#task" + id).replaceWith( task );
                    window.location='edituser/';
                }

                $('#frmTasks').trigger("reset");

                $('#myModal').modal('hide');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
	 $("#new_pass").click(function (e) { 
		var passwordField = document.getElementById('password');
		var value = passwordField.value;
		if(passwordField.type == 'password' ) {
			passwordField.type = 'text';
			 $( this ).addClass( "openEye" );
			 $( this ).removeClass( "closedEye" );
		}
		else {
			passwordField.type = 'password';
			  $( this ).addClass( "closedEye" );
			 $( this ).removeClass( "openEye" );
		}
		
		passwordField.value = value;
		});
	 $("#new_re_pass").click(function (e) { 
		var passwordField = document.getElementById('cpassword');
		var value = passwordField.value;
		if(passwordField.type == 'password' ) {
			passwordField.type = 'text';
			 $( this ).addClass( "openEye" );
			 $( this ).removeClass( "closedEye" );
		}
		else {
			passwordField.type = 'password';
			  $( this ).addClass( "closedEye" );
			 $( this ).removeClass( "openEye" );
		}
		
		passwordField.value = value;
		});	
		
});
