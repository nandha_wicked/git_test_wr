var Refund_Admin = {
	BikationRefund_Admin:{},
	PromoCode: {}
};

Refund_Admin.BikationRefund_Admin.init = function(){
	Refund_Admin.BikationRefund_Admin.editSpecBikeModel();
}.bind(Refund_Admin.BikationRefund_Admin);



Refund_Admin.BikationRefund_Admin.editSpecBikeModel = function(){
	$('.bikeModel_refund_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/admin/bikation-vendor-refund/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Refund_Admin.BikationRefund_Admin);

