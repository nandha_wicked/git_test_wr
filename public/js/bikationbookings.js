var Bookings = {
	BikationBookings:{},
	PromoCode: {}
};

Bookings.BikationBookings.init = function(){
	Bookings.BikationBookings.editBikeModel();
	Bookings.BikationBookings.editSpecBikeModel();
}.bind(Bookings.BikationBookings);

Bookings.BikationBookings.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Customer_ID = $(this).data('customerid');
		var Package_ID = $(this).data('packageid');
		var Booked_On = $(this).data('bookedon');
		var Booked_By = $(this).data('bookedby');
		var No_of_Tickets_Cost_1px = $(this).data('noofticketscost1px');
		var No_of_Tickets_Cost_2px = $(this).data('noofticketscost2px');
		var Status = $(this).data('status');
		var Package_Cost = $(this).data('packagecost');
		var Addon_cost = $(this).data('addoncost');
		var Total_Cost = $(this).data('totalcost');
		var Cancelled_On = $(this).data('cancelledon');
		var Cancelled_By = $(this).data('cancelledby');
		var Vehicle = $(this).data('vehicle');
		var Additional_Remarks = $(this).data('additionalremarks');
		var Razorpay_id = $(this).data('razorpay_id');
		var Razorpay_method = $(this).data('razorpay_method');
		var Razorpay_card_id = $(this).data('razorpay_card_id');
		var Razorpay_bank = $(this).data('razorpay_bank');
		var Razorpay_wallet = $(this).data('razorpay_wallet');
		var Trip_Name = $(this).data('trip_name');
		var Trip_Start_Date = $(this).data('trip_start_date');
		var Trip_End_Date = $(this).data('trip_end_date');
		var Start_Location = $(this).data('start_location');
		var End_Location = $(this).data('end_location');
		var Total_Distance = $(this).data('total_distance');
		var Total_riding_hours = $(this).data('total_riding_hours');
		var No_of_stops = $(this).data('no_of_stops');
		var first_name = $(this).data('first_name');
		var last_name = $(this).data('last_name');
		var email = $(this).data('email');
		var mobile_num = $(this).data('mobile_num');
		var location = $(this).data('location');

		$('#editBikeModel-modal form').attr("action","/bikation-bookings/edit/"+id);
		//$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		//$('#editBikeModel-modal form select option[value='+Package_ID+']').prop('selected',true);
		$('#editBikeModel-modal form #Booking_ID').html(id);
		$('#editBikeModel-modal form #Customer_ID').html(Customer_ID);
		$('#editBikeModel-modal form #Booked_On').html(Booked_On);
		$('#editBikeModel-modal form #Booked_By').html(Booked_By);
		$('#editBikeModel-modal form #No_of_Tickets_Cost_1px').html(No_of_Tickets_Cost_1px);
		$('#editBikeModel-modal form #No_of_Tickets_Cost_2px').html(No_of_Tickets_Cost_2px);
		$('#editBikeModel-modal form #Package_Cost').html(Package_Cost);
		$('#editBikeModel-modal form #Addon_cost').html(Addon_cost);
		$('#editBikeModel-modal form #Total_Cost').html(Total_Cost);
		$('#editBikeModel-modal form #Cancelled_On').html(Cancelled_On);
		$('#editBikeModel-modal form #Cancelled_By').html(Cancelled_By);
		$('#editBikeModel-modal form #Vehicle').html(Vehicle);
		$('#editBikeModel-modal form #Additional_Remarks').html(Additional_Remarks);
		$('#editBikeModel-modal form #Razorpay_id').html(Razorpay_id);
		$('#editBikeModel-modal form #Razorpay_method').html(Razorpay_method);
		$('#editBikeModel-modal form #Razorpay_card_id').html(Razorpay_card_id);
		$('#editBikeModel-modal form #Razorpay_bank').html(Razorpay_bank);
		$('#editBikeModel-modal form #Razorpay_wallet').html(Razorpay_wallet);
		$('#editBikeModel-modal form #Trip_Name').html(Trip_Name);
		$('#editBikeModel-modal form #Trip_Start_Date').html(Trip_Start_Date);
		$('#editBikeModel-modal form #Trip_End_Date').html(Trip_End_Date);
		$('#editBikeModel-modal form #Start_Location').html(Start_Location);
		$('#editBikeModel-modal form #End_Location').html(End_Location);
		$('#editBikeModel-modal form #Total_Distance').html(Total_Distance);
		$('#editBikeModel-modal form #Total_riding_hours').html(Total_riding_hours);
		$('#editBikeModel-modal form #No_of_stops').html(No_of_stops);
		$('#editBikeModel-modal form #first_name').html(first_name +' '+last_name);
		//$('#editBikeModel-modal form #last_name').html(last_name);
		$('#editBikeModel-modal form #email').html(email);
		$('#editBikeModel-modal form #mobile_num').html(mobile_num);
		$('#editBikeModel-modal form #location').html(location);
		
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);
		$('#editBikeModel-modal').modal('show');		
	});
}.bind(Bookings.BikationBookings);

Bookings.BikationBookings.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		$('#specBikeModel-modal form').attr("action","/bikation-bookings/delete/"+id);
		$('#specBikeModel-modal form input[name=Trip_ID]').val(Trip_ID);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Bookings.BikationBookings);

