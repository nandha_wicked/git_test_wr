var Admin = {
	Bikation_Addon:{}

};

Admin.Bikation_Addon.init = function(){
	Admin.Bikation_Addon.editBikeModel();
}.bind(Admin.Bikation_Addon);

Admin.Bikation_Addon.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('TripID');
		var Package_ID = $(this).data('PackageID');
		var Activity_ID = $(this).data('ActivityID');
		var Exclusion_Inclusion = $(this).data('ExclusionInclusion');
		var Std_Opt = $(this).data('StdOpt');
		var Cost = $(this).data('Cost');
		
		$('#editBikeModel-modal form').attr("action","/admin/bikation-addon/edit/"+id);
		$('#editBikeModel-modal form input[type=text]').val(Trip_ID);
		$('#editBikeModel-modal form input[type=text]').val(Package_ID);
		$('#editBikeModel-modal form input[type=text]').val(Activity_ID);
		$('#editBikeModel-modal form input[type=text]').val(Exclusion_Inclusion);
		$('#editBikeModel-modal form input[type=text]').val(Std_Opt);
		$('#editBikeModel-modal form input[type=text]').val(Cost);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Admin.Bikation_Addon);
