var Trip = {
	BikationTrip:{},
	PromoCode: {}
};

Trip.BikationTrip.init = function(){
	Trip.BikationTrip.editBikeModel();
	Trip.BikationTrip.editSpecBikeModel();
}.bind(Trip.BikationTrip);

Trip.BikationTrip.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Vendor_ID = $(this).data('vendorid');
		var Trip_type = $(this).data('triptype');
		var Trip_Name = $(this).data('tripname');
		var Created_On = $(this).data('createdon');
		var Created_by = $(this).data('createdby');
		var Status = $(this).data('status');
		var Approve_requested_on = $(this).data('approverequestedon');
		var Approved_By = $(this).data('approvedby');
		var Approved_on = $(this).data('approvedon');
		var Cancelled_Request_on = $(this).data('cancelledrequeston');
		var Cancel_approved_by = $(this).data('cancelapprovedby');
		var Cancelled_on = $(this).data('cancelledon');
		var Total_No_of_Tickets = $(this).data('totalnooftickets');
		var No_of_tickets_available = $(this).data('noofticketsavailable');
		var Trip_Start_Date = $(this).data('tripstartdate');
		var Trip_End_Date = $(this).data('tripenddate');
		var Start_Location = $(this).data('startlocation');
		var End_Location = $(this).data('endlocation');
		var Total_Distance = $(this).data('totaldistance');
		var Total_riding_hours = $(this).data('totalridinghours');
		var No_of_stops = $(this).data('noofstops');
		var ticket_denomination = $(this).data('ticketdenomination');
		var Experience_Level_required = $(this).data('experiencelevelrequired');
		var Preffered_Bikes = $(this).data('prefferedbikes');
		var General_Instructions = $(this).data('generalinstructions');
		var Trip_Description = $(this).data('tripdescription');

		$('#editBikeModel-modal form').attr("action","/bikation-trip/edit/"+id);
		$('#editBikeModel-modal form input[name=Vendor_ID]').val(Vendor_ID);
		$('#editBikeModel-modal form input[name=Trip_type]').val(Trip_type);
		$('#editBikeModel-modal form input[name=Trip_Name]').val(Trip_Name);
		$('#editBikeModel-modal form input[name=Created_On]').val(Created_On);
		$('#editBikeModel-modal form input[name=Created_by]').val(Created_by);
		$('#editBikeModel-modal form input[name=Approve_requested_on]').val(Approve_requested_on);
		$('#editBikeModel-modal form input[name=Approved_By]').val(Approved_By);
		$('#editBikeModel-modal form input[name=Approved_on]').val(Approved_on);
		$('#editBikeModel-modal form input[name=Cancelled_Request_on]').val(Cancelled_Request_on);
		$('#editBikeModel-modal form input[name=Cancel_approved_by]').val(Cancel_approved_by);
		$('#editBikeModel-modal form input[name=Cancelled_on]').val(Cancelled_on);
		$('#editBikeModel-modal form input[name=Total_No_of_Tickets]').val(Total_No_of_Tickets);
		$('#editBikeModel-modal form input[name=No_of_tickets_available]').val(No_of_tickets_available);	
		$('#editBikeModel-modal form input[name=TripStartDate]').val(Trip_Start_Date);		
		$('#editBikeModel-modal form input[name=TripEndDate]').val(Trip_End_Date);		
		$('#editBikeModel-modal form input[name=Start_Location]').val(Start_Location);		
		$('#editBikeModel-modal form input[name=End_Location]').val(End_Location);		
		$('#editBikeModel-modal form input[name=Total_Distance]').val(Total_Distance);		
		$('#editBikeModel-modal form input[name=Total_riding_hours]').val(Total_riding_hours);		
		$('#editBikeModel-modal form input[name=No_of_stops]').val(No_of_stops);		
		$('#editBikeModel-modal form input[name=ticket_denomination]').val(ticket_denomination);		
		$('#editBikeModel-modal form input[name=Experience_Level_required]').val(Experience_Level_required);		
		$('#editBikeModel-modal form input[name=Preffered_Bikes]').val(Preffered_Bikes);
		$('#editBikeModel-modal form textarea[name=General_Instructions]').data("wysihtml5").editor.setValue(General_Instructions);
		$('#editBikeModel-modal form textarea[name=Trip_Description]').data("wysihtml5").editor.setValue(Trip_Description);
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);		

		$('#editBikeModel-modal').modal('show');
	});
}.bind(Trip.BikationTrip);

Trip.BikationTrip.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-trip/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Trip.BikationTrip);

