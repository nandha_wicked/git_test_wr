var WickedRide = {
	Map : {}
};

$(function(){
	WickedRide.Init();
});

WickedRide.Init = function(){
	WickedRide.Login();
	WickedRide.Signup();
	WickedRide.Map.Init();
}

WickedRide.Map.createMap = function(latitude,longitude){	
	var mapProp = {
		center:new google.maps.LatLng(latitude,longitude),
		scrollwheel: false,
		zoom:13,
		mapTypeId:google.maps.MapTypeId.ROADMAP,
		title: 'Hello World!'
	};
	var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);	
	//var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);	
	    var myLatLng = {lat: latitude, lng: longitude};
	    var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    title: 'Wicked Ride'
	  });
}


WickedRide.Map.Init = function()
{	
	// initilize bangalore location
	/*if($('#googleMap').length)
		WickedRide.Map.createMap(12.920843,77.586568);*/

/* ANTON KOMKOV START */
	if ($(window).width() <= 480) {
		return;
	}
	/* ANTON KOMKOV END */


	var json = {
		"Bangalore":[
		    {"area":"Jayanagar 5th Block", "lat":"12.916944", "long":"77.582626"}, 
		    {"area":"Indiranagar", "lat":"12.978102", "long":"77.635596"},
            {"area":"Electronic City", "lat":"12.841082", "long":"77.644708"}
		    		    
		],
        "Mysore":[
		    {"area":"Nazarbad", "lat":"12.30948", "long":"76.663934"}		    
		],
		"Jaipur":[
		    {"area":"JLN Marg", "lat":"26.850015", "long":"75.800522"}		    
		],
		"Udaipur":[
		    {"area":"Udaipur", "lat":"24.575338", "long":"73.698580"}		    
		],
        "Bhuj":[
		    {"area":"Times Square Building", "lat":"23.232387", "long":"69.643836"}		    
		]
	}

	// Create city dropdown
	var city_dropdown = "<select id='city_dropdown'>";
	for (var key in json) {			
		city_dropdown += '<option value="'+key+'" >';
		city_dropdown += key;
		city_dropdown += '</option>';						
	}	
	city_dropdown += "</select>";

	// Update city dropdown
	$('#city_dropdown_container').html(city_dropdown);

	function registerCityAreaDropdown(city)
	{
		var area_dropdown = "<select id='area_dropdown'>";
		for (var k in json[city]) {		        
	        area_dropdown += '<option value="'+key+'" data-lat="'+json[city][k]['lat']+'" data-long="'+json[city][k]['long']+'">';
			area_dropdown += json[city][k]['area'];
			area_dropdown += '</option>';	
	    }
		area_dropdown += "</select>";
		// update Area dropdown
		$('#area_dropdown_container').html(area_dropdown);	
		InitAreaDropdown();		
		var latitide = $( "#area_dropdown option:selected" ).data('lat');
     	var longitude = $( "#area_dropdown option:selected" ).data('long');						
		google.maps.event.addDomListener(window, 'load', WickedRide.Map.createMap(latitide,longitude));	
	}	
	
	

	// On chnage of city dropdown create Area dropdown
	$("#city_dropdown").change(function () {
		registerCityAreaDropdown(this.value);
	});

    // On chnage of area dropdown update google map
    function InitAreaDropdown(){
		$("#area_dropdown").change(function () {
	     	var latitide = $(this).find(':selected').data('lat');
	     	var longitude = $(this).find(':selected').data('long');						
			google.maps.event.addDomListener(window, 'load', WickedRide.Map.createMap(latitide,longitude));	
	    });
    }

    // Default dropdown update
    if($('#city_dropdown option:selected').length)    
		registerCityAreaDropdown($( "#city_dropdown option:selected" ).val());
    
}	

WickedRide.Login = function(){
	$('#login-form').submit(function(event){
	   event.preventDefault();
	   
	   $.ajax({
	       url: '/booking/front-login',
	       type: "post",
	       data: {'email':$('#login-form input[name=email]').val(), 'password':$('#login-form input[name=password]').val(), 'booking':$('#login-form input[name=booking]').val(),'plan':$('#login-form input[name=plan]').val(), '_token': $('#login-form input[name=_token]').val()},
	       success: function(data){ 
	       		// Build errors to show
				var errorString = '<div class="login-ms">';
					$.each( data.errors, function( key, value) {	        
					errorString += '<div>' + '* ' + value + '</div>';
				});
				errorString += '</div>';

	       		if(data.success)
	       		{
	       			if(data.booking == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						window.location = '/booking/confirm';
	       			}
                    else if(data.plan == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						window.location = '/confirm-plan';
	       			}
	       			else{
	       				//$('#login-form #login-msg').html("Login success.....");
	       				window.location = '/';
	       			}
	       		}else{
	       			var html1 = errorString;
	       			$('#login-form #login-msg').html(html1);
	       		}          
	       }
	   });
	});
}

WickedRide.Signup = function(){
	$('#signup-form').submit(function(event){
		event.preventDefault();

		$.ajax({
			url : '/booking/front-signup',
			type : 'post',
			data : { 
						'firstName':$('#signup-form input[name=firstName]').val(),
						'lastName':$('#signup-form input[name=lastName]').val(),
						'email':$('#signup-form input[name=email]').val(),
						'mobileNumber':$('#signup-form input[name=mobileNumber]').val(),			 
						'password':$('#signup-form input[name=password]').val(),
						'confirmPassword':$('#signup-form input[name=confirmPassword]').val(),
						'booking':$('#signup-form input[name=booking]').val(),
                        'plan':$('#signup-form input[name=plan]').val(),
						'_token': $('#signup-form input[name=_token]').val()
				   },
			success: function(data){

				var errorString = '';
				errorString += '<div class="signup-ms">';
				$.each(data.errors, function(key, value){
					errorString += '<div>'+'* '+ value + '</div>'; 
				});
				errorString += '</div>';

				if(data.success){
                    
                    console.log(data.plan);
                    
					if(data.booking == 1){
						/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
						$('#signup-form #signup-msg').html("You have been signed up successfully. We are redirecting ....");
						window.location = '/booking/confirm';
						//window.location = '/booking/confirm';
					}
                    else if(data.plan == 1){
	       				/*$('.book_bike_main #step3').fadeOut("slow");
						$('.book_bike_main #step4').fadeIn("slow");
						$('.booking_steps ul > li').each(function(){
							$(this).removeClass("active");
						});
						$('.booking_steps #step4-li').addClass("active");*/
                        $('#signup-form #signup-msg').html("You have been signed up successfully. We are redirecting ....");
						window.location = '/confirm-plan';
	       			}
					else
						//$('#signup-form #signup-msg').html("You have been signed up successfully.....");
						window.location = '/';		
				}
				else{
					$('#signup-form #signup-msg').html(errorString);	
				}
			},
			beforeSend : function(){
				$('.my-loader').show();
			},
			complete : function(){
				$('.my-loader').hide();
			}	   
		});
	});
}

WickedRide.applyPromocode = function(){
	var ele = $('.apply-promocode');
	var modelId = ele.data('model-id');
	var areaId = ele.data('area-id');
	var cityId = ele.data('city-id');
	var startDate = ele.data('start-date');
	var startTime = ele.data('start-time');
	var endDate = ele.data('end-date');
	var endTime = ele.data('end-time');
	var coupon = $('input[name=promocode]').val();
	$.ajax({
		url : '/api/bookings/total-price',
		type : 'GET',
		headers: { },
		data : { 
		'model_id': modelId,
		'area_id': areaId,
		'city_id': cityId,
		'start_date': startDate,
		'start_time': startTime,
		'end_date': endDate,
		'end_time': endTime,
		'coupon': coupon,
        'device': 'WEB'
		},
		success: function(response){
			var result = response['result']['data'];
			if (result['promocode']['status'] == false){
				$('#promocode-msg').removeClass('success-msg').addClass('error-msg').text(result['promocode']['message']);
				var totalPrice = WickedRide.addVat(result['total_price'],result['tax_amount']);
				$('.book_bike_fleet').find('.book_now .basic_price').text('RS '+result['final_price']);
				$('.book_bike_fleet').find('.book_now .tax').text( Math.round(result['final_price'] * result['tax_amount'])/100 +' ('+result['tax_name']+' '+result['tax_amount']+'%)');
				$('.book_bike_fleet').find('.book_now .amt').text('RS '+totalPrice);
				return false;
			}
            
            if(result['final_price']==0)
            {
                
                window.location = "/booking/confirmWithPromo?model_id="+modelId+"&area_id="+areaId+"&city_id="+cityId+"&start_date="+startDate+"&start_time="+startTime+"&end_date="+endDate+"&end_time="+endTime+"&coupon="+coupon;
            }

			var finalPrice = WickedRide.addVat(result['final_price'],result['tax_amount']);
			var vatPrice =  Math.round(result['final_price'] *result['tax_amount'])/100;
			
			$('#promocode-msg').removeClass('error-msg').addClass('success-msg').text(result['promocode']['message']);
			$('.book_bike_fleet').find('.book_now .basic_price').text('RS '+result['final_price']);
			$('.book_bike_fleet').find('.book_now .tax').text(vatPrice+'('+result['tax_name']+' '+result['tax_amount']+'%)');
			$('.book_bike_fleet').find('.book_now .amt').text('RS '+finalPrice);
		}
	});
}

WickedRide.addVat = function(totalPrice,taxAmount){
	//5.5% VAT
	totalPrice = totalPrice*(1 + taxAmount/100);
	// 2 decimal points round up
	totalPrice = Math.round(totalPrice * 100) / 100;
	return totalPrice;
}
