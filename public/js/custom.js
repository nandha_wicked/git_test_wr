var total_price='';
// validate addon form on keyup and submit
$("#bikation-addon-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Exclusion_Inclusion: {
		required: true
		},
		Cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		Cost: {
			required: "Please Provide a Cost"
		},
		Exclusion_Inclusion: {
			required: "Please Exclusion Inclusion"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-addon-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Exclusion_Inclusion: {
		required: true
		},
		Cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		Cost: {
			required: "Please Provide a Cost"
		},
		Exclusion_Inclusion: {
			required: "Please Exclusion Inclusion"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
			
// validate address add form on keyup and submit
$("#bikation-address-add").validate({
	rules: {
		
		Area: {
		required: true
		},
		City: {
		required: true
		},
		State: {
		required: true
		},
		Country: {
			required: true
		},
		ZipCode: {
			required: true
		},
		LandLine_Number: {
			required: true
		},
		Website: {
			required: true
		},
	},
	messages: {
		Area: {
			required: "Please Provide a Area"
		},
		City: {
			required: "Please Provide a City"
		},
		State: {
			required: "Please Provide a State"
		},
		Country: {
			required: "Please Provide a Exclusion Country"
		},
		ZipCode: {
			required: "Please Provide a ZipCode"
		},
		LandLine_Number: {
			required: "Please Provide a LandLine_Number"
		},
		Website: {
			required: "Please Provide a Website"
		},
	},
	
});

// validate package form on keyup and submit
$("#bikation-package-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		name: {
		required: true
		},
		Description: {
		required: true
		},
		No_of_Tickets_Cost_1px: {
			required: true,
			number: true
		},
		Tickes_Remaining_Cost_1px: {
			required: true,
			number: true,
     			greaterThan: '#No_of_Tickets_Cost_1px'
		},
		No_of_Tickets_Cost_2px: {
			required: true,
			number: true
		},
		Tickes_Remaining_Cost_2px: {
			required: true,
			number: true,
			greaterThan: '#No_of_Tickets_Cost_2px'
		},
		Cost_1px: {
			required: true,
			number: true
		},
		Cost_2px: {
			required: true,
			number: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		name: {
			required: "Please Provide a Package Name"
		},
		Description: {
			required: "Please Provide a Description"
		},
		No_of_Tickets_Cost_1px: {
			required: "Please Provide a No of Tickets 1px"
		},
		Tickes_Remaining_Cost_1px: {
			required: "Please Provide a Tickes Remaining 1px",
			greaterThan: "Should be less than total tickets"
			
		},
		No_of_Tickets_Cost_2px: {
			required: "Please Provide a No of Tickets 2px"
			
		},
		Tickes_Remaining_Cost_2px: {
			required: "Please Provide a Tickes Remaining 2px",
			greaterThan: "Should be less than total tickets"
		},
		Cost_1px: {
			required: "Please Provide a Cost 1px"
		},
		Cost_2px: {
			required: "Please Provide a Cost 2px"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate package edit form on keyup and submit
$("#bikation-package-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		name: {
		required: true
		},
		Description: {
		required: true
		},
		No_of_Tickets_Cost_1px: {
			required: true,
			number: true
		},
		Tickes_Remaining_Cost_1px: {
			required: true,
			number: true,
     			greaterThan: '#No_of_Tickets_Cost_1px'
		},
		No_of_Tickets_Cost_2px: {
			required: true,
			number: true
		},
		Tickes_Remaining_Cost_2px: {
			required: true,
			number: true
		},
		Cost_1px: {
			required: true,
			number: true
		},
		Cost_2px: {
			required: true,
			number: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		name: {
			required: "Please Provide a Package Name"
		},
		Description: {
			required: "Please Provide a Description"
		},
		No_of_Tickets_Cost_1px: {
			required: "Please Provide a No of Tickets Cost 1px"
		},
		Tickes_Remaining_Cost_1px: {
			required: "Please Provide a Tickes Remaining Cost 1px",
			greaterThan:"Please Enter Value Less Then Number Of Ticket"
		},
		No_of_Tickets_Cost_2px: {
			required: "Please Provide a No of Tickets Cost 2px"
		},
		Tickes_Remaining_Cost_2px: {
			required: "Please Provide a Tickes Remaining Cost 2px"
		},
		Cost_1px: {
			required: "Please Provide a Cost_1px"
		},
		Cost_2px: {
			required: "Please Provide a Cost_2px"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});

// validate itinerary form on keyup and submit
$("#bikation-itinerary-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Day: {
		required: true
		},
		/*Image_ID: {
		required: true
		},*/
		// Video_ID: {
		// 	required: true
		// },
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Day: {
			required: "Please Provide a Day"
		},
		/*Image_ID: {
			required: "Please Provide a Image"
		},*/
		// Video_ID: {
		// 	required: "Please Provide a Video"
		// },
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate itinerary edit form on keyup and submit
$("#bikation-itinerary-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Day: {
		required: true
		},
		/*Image_ID: {
		required: true
		},*/
		// Video_ID: {
		// 	required: true
		// },
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Day: {
			required: "Please Provide a Day"
		},
		/*Image_ID: {
			required: "Please Provide a Image"
		},*/
		// Video_ID: {
		// 	required: "Please Provide a Video"
		// },
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});



// validate addon form on keyup and submit
$("#bikation-trip-add").validate({
	rules: {
		
		Trip_type: {
		required: true
		},
		Trip_Name: {
		required: true
		},
		Trip_Start_Date: {
		required: true
		},
		Trip_End_Date: {
			required: true
		},
		Start_Location: {
		required: true
		},
		End_Location: {
			required: true
		},
		No_of_stops: {
		required: true
		},
		Preffered_Bikes: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_type: {
			required: "Please Provide a Trip Type"
		},
		Trip_Name: {
			required: "Please Provide a Trip Name"
		},
		Trip_Start_Date: {
			required: "Please Provide a Trip Start Date"
		},
		Trip_End_Date: {
			required: "Please Provide a Trip End Date"
		},
		Start_Location: {
			required: "Please Provide a Start Location"
		},
		End_Location: {
			required: "Please Provide a End Location"
		},
		No_of_stops: {
			required: "Please Provide a No of stops"
		},
		Preffered_Bikes: {
			required: "Please Provide Preffered Bikes"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-trip-edit").validate({
	rules: {
		
		Trip_type: {
		required: true
		},
		Trip_Name: {
		required: true
		},
		TripStartDate: {
		required: true
		},
		TripEndDate: {
			required: true
		},
		Start_Location: {
		required: true
		},
		End_Location: {
			required: true
		},
		No_of_stops: {
		required: true
		},
		Preffered_Bikes: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_type: {
			required: "Please Provide a Trip Type"
		},
		Trip_Name: {
			required: "Please Provide a Trip Name"
		},
		TripStartDate: {
			required: "Please Provide a Trip Start Date"
		},
		TripEndDate: {
			required: "Please Provide a Trip End Date"
		},
		Start_Location: {
			required: "Please Provide a Start Location"
		},
		End_Location: {
			required: "Please Provide a End Location"
		},
		No_of_stops: {
			required: "Please Provide a No of stops"
		},
		Preffered_Bikes: {
			required: "Please Provide Preffered Bikes"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


$("input:radio[name=Media_Select]").on("change", function() { 

  if(this.value == 'Video')
  {
   $(this).closest(".modal-body").find("#media_video").removeClass("hide"); 
   $(this).closest(".modal-body").find("#media_video").addClass("show");
   $(this).closest(".modal-body").find("#media_image").removeClass("show"); 
   $(this).closest(".modal-body").find("#media_image").addClass("hide"); 
  }
  else
  {
   $(this).closest(".modal-body").find("#media_image").removeClass("hide"); 
   $(this).closest(".modal-body").find("#media_image").addClass("show");
   $(this).closest(".modal-body").find("#media_video").removeClass("show"); 
   $(this).closest(".modal-body").find("#media_video").addClass("hide");  
  }
});


// validate addon form on keyup and submit
$("#bikation-trip-photo-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Media_URL: {
		required: true
		},
		
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Media_URL: {
			required: "Please Provide a Media URL"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-trip-photo-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});

// validate addon form on keyup and submit
$("#bikation-people_on_trip-add").validate({
	rules: {
		
		Book_ID: {
		required: true
		},
		Trip_ID: {
		required: true
		},
		Contact_No: {
		required: true
		},
		Email_ID: {
			required: true
		},
		Rider: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Book_ID: {
			required: "Please Select a Booking Name"
		},
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Contact_No: {
			required: "Please Provide a Contact No"
		},
		Email_ID: {
			required: "Please Provide a Email"
		},
		Rider: {
			required: "Please Provide a Rider"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-people_on_trip-edit").validate({
	rules: {
		
		Book_ID: {
		required: true
		},
		Trip_ID: {
		required: true
		},
		Contact_No: {
		required: true
		},
		Email_ID: {
			required: true
		},
		Rider: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Book_ID: {
			required: "Please Select a Book Name"
		},
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Contact_No: {
			required: "Please Provide a Contact No"
		},
		Email_ID: {
			required: "Please Provide a Email"
		},
		Rider: {
			required: "Please Provide a Rider"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


// validate addon form on keyup and submit
$("#bikation-bookings-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Cust_Name: {
		required: true
		},
		Cust_email: {
		required: true
		},
		Package_Cost: {
			required: true
		},
		Total_Cost: {
		required: true
		},
		Vehicle: {
		required: true
		},
		Additional_Remarks: {
		required: true
		},
		No_of_Tickets_Cost_1px: {
			required: true
		},
		No_of_Tickets_Cost_2px: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		Package_Cost: {
			required: "Please Provide a Package Cost"
		},
		Cust_Name: {
			required: "Please Provide a Customer Name"
		},
		Cust_email: {
			required: "Please Provide a Customer Email"
		},
		Total_Cost: {
			required: "Please Provide a Total Cost"
		},
		Vehicle: {
			required: "Please Provide a Vehicle Name"
		},
		Additional_Remarks: {
			required: "Please Provide a Additional Remarks"
		},
		No_of_Tickets_Cost_1px: {
			required: "Please add a No of Tickets for 1px"
		},
		No_of_Tickets_Cost_2px: {
			required: "Please Provide a No of Tickets for 2px"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-bookings-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Package_Cost: {
			required: true
		},
		Total_Cost: {
		required: true
		},
		Vehicle: {
		required: true
		},
		Additional_Remarks: {
		required: true
		},
		Addon_cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		
		Package_Cost: {
			required: "Please Provide a Package Cost"
		},
		Total_Cost: {
			required: "Please Provide a Total Cost"
		},
		Vehicle: {
			required: "Please Provide a Vehicle Name"
		},
		Additional_Remarks: {
			required: "Please Provide a Additional Remarks"
		},
		Addon_cost: {
			required: "Please Provide a Addon cost"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


// validate address add form on keyup and submit
$("#bikation-rating").validate({
	rules: {
		
		Remarks: {
		required: true
		},
		Overall: {
		required: true
		},
	},
	messages: {
		Remarks: {
			required: "Write your review here"
		},
		Overall: {
			required: "Please Select Rating"
		},
	},
	
});


// validate addon form on keyup and submit
$("#myform-register").validate({
	rules: {
		
		first_name: {
		required: true
		},
		Last_name: {
		required: true
		},
		email: {
		required: true,
		email: true
		},
		mobile_num: {
			required: true,
			number:true,
			maxlength: 10,
			minlength: 10
		},
		password1: {
			required: true,
			minlength: 6
		},
		confirmPassword: {
			required: true,
			equalTo: "#password1"
		},
		
	},
	messages: {
		first_name: {
			required: "Please Provide a First Name"
		},
		last_name: {
			required: "Please Provide a Last Name"
		},
		email: {
			required: "Please Provide a Email"
		},
		mobile_num: {
			required: "Please Provide Mobile Number",
			number : "Please enter only Number",
			maxlength: "Your Mobile must be at least 10 Digit",
			minlength: "Your password must be at least 10 Digit"
			
		},
		password1: {
			required: "Please Provide Password",
			minlength: "Your password must be at least 6 characters long"
		},
		confirmPassword: {
			required: "Please Provide Password",
			equalTo: "Please Provide confirm Password"
		},
	},
 submitHandler: function (form) {
 	$.ajax({
			url : '/trip-signup',
			type : 'post',
			data : { 
						'first_name':$('#myform-register input[name=first_name]').val(),
						'last_name':$('#myform-register input[name=last_name]').val(),
						'email':$('#myform-register input[name=email]').val(),
						'mobile_num':$('#myform-register input[name=mobile_num]').val(),			 
						'password':$('#myform-register input[name=password]').val(),
						'confirmPassword':$('#myform-register input[name=confirmPassword]').val(),
						'_token': $('#myform-register input[name=_token]').val()
				   },
				   beforeSend: function() {
				   	var waitString = '<div class="signup-ms error"><div>please wait</div></div>';
					$('#myform-register #register-msg').html(waitString);	
					$('#send-btn').prop('disabled',true);
				   },
			success: function(data){
				$("#loaderdiv").hide();

				var errorString = '';
				errorString += '<div class="signup-ms error">';
				$.each(data.errors, function(key, value){
					errorString += '<div>'+'* '+ value + '</div>'; 
				});
				errorString += '</div>';

				if(data.success){
					$('#addBikeModel-modal').modal({
						dismissible: true
					});
					window.location = '';
				
				}else{
					$('#myform-register #register-msg').html(errorString);	
				}
			},
		});
    return false;
},
});

$("#vertical-menu h3").click(function () {
    //slide up all the link lists
    $("#vertical-menu ul ul").slideUp();
    $('.plus',this).html('+');
    //slide down the link list below the h3 clicked - only if its closed
    if (!$(this).next().is(":visible")) {
        $(this).next().slideDown();
        //$(this).remove("span").append('<span class="minus">-</span>');
        $('.plus').html('+');
        $('.plus',this).html('-');
    }
})


// validate address add form on keyup and submit
$("#myform-login").validate({
 rules: {
  
  email: {
  required: true
  },
  
  password: {
   required: true
  },
 },
 messages: {
  email: {
   required: "Please Provide a Email"
  },
  password: {
   required: "Please Provide Password"
  },
  
 },
 submitHandler: function (form) {
 	$.ajax({
    type: "post",
    url: "/triplogin",
    data: {'email':$('input[name=email]').val(),'password':$('input[name=password]').val(),'Trip_url':$('input[name=Trip_url]').val(), '_token': $('input[name=_token]').val()},
    success: function(data){ 
	       		// Build errors to show
				var errorString = '<div class="login-msg error">';
					$.each( data.errors, function( key, value) {	        
					errorString += '<div>' + '* ' + value + '</div>';
				});
				errorString += '</div>';

	       		if(data.success)
	       		{	
	       			window.location = data.Trip_url;
	       		 }else{
	       			var html1 = errorString;
	       			$('#myform-login #login-msg').html(html1);
	       		}          
	       }
    	});
    return false;
},
});



// validate address add form on keyup and submit
$("#myform-forgot_pass").validate({
 rules: {
  
  email: {
  required: true,
  email:true
  },
     
 },
 
});


// validate address add form on keyup and submit
$("#frmTasks-addTrip").validate({
	 groups: {
            names: "No_of_Tickets_Cost_1px No_of_Tickets_Cost_2px"
        },
	
 rules: {
  
   No_of_Tickets_Cost_1px: {
         require_from_group: [1, ".send"]
    },
    No_of_Tickets_Cost_2px: {
         require_from_group: [1, ".send"]
    },
  /*No_of_Tickets_Cost_1px: {
  required: true
  },*/
 Vehicle:{ required: true}
 },
 /*messages: {
  No_of_Tickets_Cost_1px: {
   required: "Please Provide a No of Seats"
  },
  },*/
 errorPlacement: function(error, element) { }, 
 submitHandler: function (form) {
	 var href = window.location.href;	
	 var href_spl_removal = href.split('?')[0];
  	$.ajax({
    type: "post",
    url: "/add_no_seats",
    data : { 
						'Trip_ID':$('#frmTasks-addTrip input[name=Trip_ID]').val(),
						'Trip_url_get':href_spl_removal.substr(href_spl_removal.lastIndexOf('/') + 1),
						'Package_Cost':$('#frmTasks-addTrip input[name=Package_Cost]').val(),
						'Addon_cost':$('#frmTasks-addTrip input[name=Addon_cost]').val(),
						'Total_Cost':$('#frmTasks-addTrip input[name=Total_Cost]').val(),
						'Booked_Type':$('#frmTasks-addTrip input[name=Booked_Type]').val(),
						'No_of_Tickets_Cost_1px':$('#frmTasks-addTrip select[name=No_of_Tickets_Cost_1px]').val(),
						'No_of_Tickets_Cost_2px':$('#frmTasks-addTrip select[name=No_of_Tickets_Cost_2px]').val(),
						'Trip_url':$('#frmTasks-addTrip input[name=Trip_url]').val(),
						'Vehicle': $('#frmTasks-addTrip input[name=Vehicle]:checked').val(),
						'_token': $('#frmTasks-addTrip input[name=_token]').val()
				   },
    success: function(data){ 
	       		// Build errors to show
				var errorString = '<div class="addTrip-msg">';
					$.each( data.errors, function( key, value) {	        
					errorString += '<div>' + '*' + value + '</div>';
				});
				errorString += '</div>';
				
	       		if(data.success == 'true')
	       		{	
			
	       			$.ajax({
				      type: "post",
				      url: "/update_payment_trip",
				      data : {
				        'Booking_ID':data.Booking_ID,
				        'Total_Cost_return':data.Total_Cost*100, 
				        'Trip_ID':data.Trip_ID, 
				        'email':data.email,
				        'first_name':data.first_name, 
				        'last_name':data.last_name,
				        'mobile_num':data.mobile_num,
				         'Trip_name':data.Trip_name,  
				         'Trip_url':data.Trip_url,
				        '_token': $('#frmTasks-addTrip input[name=_token]').val()   
				        },
				       success: function(data){
				       $("#razorpayform-block").html(data);
				       //document.getElementById("razorpay-form1").submit();
				       //$(".razorpay-payment-button").trigger('click');
				       } 
				      });
	       		 }else{
	       			alert('Something Wrong Parameters.Please wait or change Tickets.');
	       		}          
	       }
    	});
    return false;
},
});

$.validator.addMethod("greaterThan",

function (value, element, param) {
  var $min = $(param);
  if (this.settings.onfocusout) {
    $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
      $(element).valid();
    });
  }
  return parseInt(value) <= parseInt($min.val());
}, "Max must be greater than min");


//vendor-register-form  
$("#vendor_signup_form").validate({

 rules: {
  
  first_name: {
  required: true
  },
  last_name: {
  required: true
  },
  email: {
  required: true,
  email:true
  },
  mobile_num: {
   required: true
  },
  password: {
   required: true
  },
  password_confirmation: {
   required: true,
   equalTo: "#password"
  },
  Street_1: {
   required: true
  },
  Area: {
   required: true
  },
  City: {
   required: true
  },
  State: {
   required: true
  },
  Country: {
   required: true
  },
  ZipCode: {
   required: true
  },
  Name: {
   required: true
  },
  Alternate_Number: {
   required: true
  },
  
 },
 messages: {
  first_name: {
   required: "Please add first_name"
  },
  last_name: {
   required: "Please add lastName"
  },
  email: {
   required: "Please Provide email"
  },
  mobile_num: {
   required: "Please add mobileNumber"
  },
  password: {
   required: "Please add password"
  },
  password_confirmation: {
   required: "Please add password_confirmation"
  },
  Street_1: {
   required: "Please add Street_1"
  },
  Area: {
   required: "Please add  Area"
  },
  City: {
   required: "Please add  City"
  },
  State: {
   required: "Please add State"
  },
  Country: {
   required: "Please add Country"
  },
  ZipCode: {
   required: "Please add ZipCode"
  },
  Name: {
   required: "Please add Name"
  },
  Alternate_Number: {
   required: "Please add Alternate_Number"
  },
 },
 errorPlacement: function(error, element) { }
 
});

$('#bikation-addon-add  #Trip_ID').on('change',function(){
	    get_packages_add();
        });
$('#bikation-addon-edit  #Trip_ID').on('change',function(){
	    get_packages_edit();
        });		
$('#bikation-bookings-add  #Trip_ID').on('change',function(){
	    get_packages_booking();
        });	
$('#bikation-bookings-add  #Package_ID').on('change',function(){
	    get_ticket_booking();
        });	

function get_ticket_booking()
        {
			var Tickets_1px_count = $('#bikation-bookings-add  #Package_ID').children('option:selected').data('remaining1px');//$('#bikation-bookings-add  #Package_ID').data('remaining1px');
			var Tickets_2px_count = $('#bikation-bookings-add  #Package_ID').children('option:selected').data('remaining2px');
			//alert(Tickets_1px_count);return false;
			var Tickets_1px_list = '';
			var Tickets_2px_list = '';
			Tickets_1px_list += '<option value="">Select</option>';
			Tickets_2px_list += '<option value="">Select</option>';
			
			for (i = 1; i < Tickets_1px_count; i++) {
			Tickets_1px_list += '<option value='+i+'>'+i+'</option>';
			}
			for (i = 1; i < Tickets_2px_count; i++) {
			Tickets_2px_list += '<option value='+i+'>'+i+'</option>';
			}
			$("#bikation-bookings-add  #No_of_Tickets_Cost_1px").html(Tickets_1px_list);
			$("#bikation-bookings-add  #No_of_Tickets_Cost_2px").html(Tickets_2px_list);
		
		}			
function get_packages_booking()
        {
        var Trip_ID =  $("#bikation-bookings-add #Trip_ID").val();
          $.ajax({
                    type: "post",
                    url: "bikation-vendor/get_packages",
                    data : { 
                              'Trip_ID':Trip_ID,
                              '_token': $('#bikation-bookings-add input[name=_token]').val()
                           },
                    success: function(data){
                                        if(data.success)
                                        {
											var pack_list = '';
											 pack_list += '<option value="">Select</option>';
											$.each(data.bikation_packageList, function(idx, obj) {
												
												 pack_list += '<option data-remaining1px='+obj.Tickes_Remaining_Cost_1px+'  data-remaining2px="'+obj.Tickes_Remaining_Cost_2px+'" value='+obj.Package_ID+'>'+obj.name+'</option>';
											});
											$("#bikation-bookings-add  #Package_ID").html(pack_list);
                                           
                                         }  

                                        },
                                    });
                                
      
            }		
function get_packages_add()
        {
        var Trip_ID =  $("#bikation-addon-add #Trip_ID").val();
          $.ajax({
                    type: "post",
                    url: "bikation-vendor/get_packages",
                    data : { 
                              'Trip_ID':Trip_ID,
                              '_token': $('#bikation-addon-add input[name=_token]').val()
                           },
                    success: function(data){
                                        if(data.success)
                                        {
											var pack_list = '';
											 pack_list += '<option value="">Select</option>';
											$.each(data.bikation_packageList, function(idx, obj) {
												
												 pack_list += '<option value='+obj.Package_ID+'>'+obj.name+'</option>';
											});
											$("#bikation-addon-add  #Package_ID").html(pack_list);
                                           
                                         }  

                                        },
                                    });
                                
      
            }
			
function get_packages_edit(Trip_ID,Package_ID)
        {
    	
		var Trip_ID =  Trip_ID;//alert(Package_ID);
		   $.ajax({
                    type: "post",
                    url: "bikation-vendor/get_packages",
                    data : { 
                              'Trip_ID':Trip_ID,
                              '_token': $('#bikation-addon-edit input[name=_token]').val()
                           },
                    success: function(data){
                                        if(data.success)
                                        {
											var pack_list = '';
											 pack_list += '<option value="">Select</option>';
											$.each(data.bikation_packageList, function(idx, obj) {
												if(Package_ID == obj.Package_ID){pack_list += '<option selected value='+obj.Package_ID+'>'+obj.name+'</option>';}
												else { pack_list += '<option value='+obj.Package_ID+'>'+obj.name+'</option>'; }
												 
											});
											$("#bikation-addon-edit  #Package_ID").html(pack_list);
                                           
                                         }   

                                        },
                                    });
                                
      
            }


equalheight = function(container) {
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}
$(window).load(function() {

    equalheight('.equalheight');

});
$(window).resize(function() {

    equalheight('.equalheight');

});			
