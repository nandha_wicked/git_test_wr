var Itinerary = {
	BikationItinerary:{},
	PromoCode: {}
};

Itinerary.BikationItinerary.init = function(){
	Itinerary.BikationItinerary.editBikeModel();
	Itinerary.BikationItinerary.editSpecBikeModel();
}.bind(Itinerary.BikationItinerary);
Itinerary.BikationItinerary.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Day = $(this).data('day');
		var Text_ID = $(this).data('textid');
		var Stay_at = $(this).data('stayat');
		var Image_ID = $(this).data('image');
		var Video_ID = $(this).data('videoid');
		var status = $(this).data('status');
		var coverImgHolder =  '';
		if(Image_ID != '')
		{
		coverImgHolder = '<img src="'+Image_ID+'" width="50px" />';
		}
		else
		{ coverImgHolder = '';}	

		$('#editBikeModel-modal form').attr("action","/bikation-itinerary/edit/"+id);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);		$('#editBikeModel-modal form input[type=text]').val(Day);
		$('#editBikeModel-modal form input[name=Day]').val(Day);
		$('#editBikeModel-modal form input[name=Text_ID]').val(Text_ID);
		$('#editBikeModel-modal form textarea[name=Stay_at]').data("wysihtml5").editor.setValue(Stay_at);

		$('#editBikeModel-modal form input[name=text]').val(Video_ID);
		$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);
		if(status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);

			
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Itinerary.BikationItinerary);


Itinerary.BikationItinerary.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-itinerary/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Itinerary.BikationItinerary);
