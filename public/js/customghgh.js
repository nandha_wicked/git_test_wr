// validate addon form on keyup and submit
$("#bikation-addon-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Exclusion_Inclusion: {
		required: true
		},
		Cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		Cost: {
			required: "Please Provide a Cost"
		},
		Exclusion_Inclusion: {
			required: "Please Exclusion Inclusion"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-addon-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		Exclusion_Inclusion: {
		required: true
		},
		Cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		Cost: {
			required: "Please Provide a Cost"
		},
		Exclusion_Inclusion: {
			required: "Please Exclusion Inclusion"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
			
// validate address add form on keyup and submit
$("#bikation-address-add").validate({
	rules: {
		
		Area: {
		required: true
		},
		City: {
		required: true
		},
		State: {
		required: true
		},
		Country: {
			required: true
		},
		ZipCode: {
			required: true
		},
		LandLine_Number: {
			required: true
		},
		Website: {
			required: true
		},
	},
	messages: {
		Area: {
			required: "Please Provide a Area"
		},
		City: {
			required: "Please Provide a City"
		},
		State: {
			required: "Please Provide a State"
		},
		Country: {
			required: "Please Provide a Exclusion Country"
		},
		ZipCode: {
			required: "Please Provide a ZipCode"
		},
		LandLine_Number: {
			required: "Please Provide a LandLine_Number"
		},
		Website: {
			required: "Please Provide a Website"
		},
	},
	
});

// validate package form on keyup and submit
$("#bikation-package-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		name: {
		required: true
		},
		Description: {
		required: true
		},
		No_of_Tickets: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		name: {
			required: "Please Provide a Package Name"
		},
		Description: {
			required: "Please Provide a Description"
		},
		No_of_Tickets: {
			required: "Please Provide a No of Tickets"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate package edit form on keyup and submit
$("#bikation-package-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		name: {
		required: true
		},
		Description: {
		required: true
		},
		No_of_Tickets: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		name: {
			required: "Please Provide a Package Name"
		},
		Description: {
			required: "Please Provide a Description"
		},
		No_of_Tickets: {
			required: "Please Provide a No of Tickets"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});

// validate itinerary form on keyup and submit
$("#bikation-itinerary-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Day: {
		required: true
		},
		Image_ID: {
		required: true
		},
		// Video_ID: {
		// 	required: true
		// },
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Day: {
			required: "Please Provide a Day"
		},
		Image_ID: {
			required: "Please Provide a Image"
		},
		// Video_ID: {
		// 	required: "Please Provide a Video"
		// },
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate itinerary edit form on keyup and submit
$("#bikation-itinerary-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Day: {
		required: true
		},
		/*Image_ID: {
		required: true
		},*/
		// Video_ID: {
		// 	required: true
		// },
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Day: {
			required: "Please Provide a Day"
		},
		/*Image_ID: {
			required: "Please Provide a Image"
		},*/
		// Video_ID: {
		// 	required: "Please Provide a Video"
		// },
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});



// validate addon form on keyup and submit
$("#bikation-trip-add").validate({
	rules: {
		
		Trip_type: {
		required: true
		},
		Trip_Name: {
		required: true
		},
		Trip_Start_Date: {
		required: true
		},
		Trip_End_Date: {
			required: true
		},
		Start_Location: {
		required: true
		},
		End_Location: {
			required: true
		},
		No_of_stops: {
		required: true
		},
		Preffered_Bikes: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_type: {
			required: "Please Provide a Trip Type"
		},
		Trip_Name: {
			required: "Please Provide a Trip Name"
		},
		Trip_Start_Date: {
			required: "Please Provide a Trip Start Date"
		},
		Trip_End_Date: {
			required: "Please Provide a Trip End Date"
		},
		Start_Location: {
			required: "Please Provide a Start Location"
		},
		End_Location: {
			required: "Please Provide a End Location"
		},
		No_of_stops: {
			required: "Please Provide a No of stops"
		},
		Preffered_Bikes: {
			required: "Please Provide a Preffered Bikes"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-trip-edit").validate({
	rules: {
		
		Trip_type: {
		required: true
		},
		Trip_Name: {
		required: true
		},
		Trip_Start_Date: {
		required: true
		},
		Trip_End_Date: {
			required: true
		},
		Start_Location: {
		required: true
		},
		End_Location: {
			required: true
		},
		No_of_stops: {
		required: true
		},
		Preffered_Bikes: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_type: {
			required: "Please Provide a Trip Type"
		},
		Trip_Name: {
			required: "Please Provide a Trip Name"
		},
		Trip_Start_Date: {
			required: "Please Provide a Trip Start Date"
		},
		Trip_End_Date: {
			required: "Please Provide a Trip End Date"
		},
		Start_Location: {
			required: "Please Provide a Start Location"
		},
		End_Location: {
			required: "Please Provide a End Location"
		},
		No_of_stops: {
			required: "Please Provide a No of stops"
		},
		Preffered_Bikes: {
			required: "Please Provide a Preffered Bikes"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


$("input:radio[name=Media_Select]").on("change", function() { 

  if(this.value == 'Video')
  {
   $(this).closest(".modal-body").find("#media_video").removeClass("hide"); 
   $(this).closest(".modal-body").find("#media_video").addClass("show");
   $(this).closest(".modal-body").find("#media_image").removeClass("show"); 
   $(this).closest(".modal-body").find("#media_image").addClass("hide"); 
  }
  else
  {
   $(this).closest(".modal-body").find("#media_image").removeClass("hide"); 
   $(this).closest(".modal-body").find("#media_image").addClass("show");
   $(this).closest(".modal-body").find("#media_video").removeClass("show"); 
   $(this).closest(".modal-body").find("#media_video").addClass("hide");  
  }
});


// validate addon form on keyup and submit
$("#bikation-trip-photo-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Media_URL: {
		required: true
		},
		
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Media_URL: {
			required: "Please Provide a Media URL"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-trip-photo-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Media_URL: {
		required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},Media_Select: {
			required: "Please Select a Media"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});

// validate addon form on keyup and submit
$("#bikation-people_on_trip-add").validate({
	rules: {
		
		Book_ID: {
		required: true
		},
		Trip_ID: {
		required: true
		},
		Contact_No: {
		required: true
		},
		Email_ID: {
			required: true
		},
		Rider: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Book_ID: {
			required: "Please Select a Booking Name"
		},
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Contact_No: {
			required: "Please Provide a Contact No"
		},
		Email_ID: {
			required: "Please Provide a Email"
		},
		Rider: {
			required: "Please Provide a Rider"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-people_on_trip-edit").validate({
	rules: {
		
		Book_ID: {
		required: true
		},
		Trip_ID: {
		required: true
		},
		Contact_No: {
		required: true
		},
		Email_ID: {
			required: true
		},
		Rider: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Book_ID: {
			required: "Please Select a Book Name"
		},
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Contact_No: {
			required: "Please Provide a Contact No"
		},
		Email_ID: {
			required: "Please Provide a Email"
		},
		Rider: {
			required: "Please Provide a Rider"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


// validate addon form on keyup and submit
$("#bikation-bookings-add").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		No_of_People: {
		required: true
		},
		Package_Cost: {
			required: true
		},
		Total_Cost: {
		required: true
		},
		Vehicle: {
		required: true
		},
		Additional_Remarks: {
		required: true
		},
		Addon_cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		No_of_People: {
			required: "Please Provide a No of People"
		},
		Package_Cost: {
			required: "Please Provide a Package Cost"
		},
		Total_Cost: {
			required: "Please Provide a Total Cost"
		},
		Vehicle: {
			required: "Please Provide a Vehicle Name"
		},
		Additional_Remarks: {
			required: "Please Provide a Additional Remarks"
		},
		Addon_cost: {
			required: "Please Provide a Addon cost"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});
		
// validate addon edit form on keyup and submit
$("#bikation-bookings-edit").validate({
	rules: {
		
		Trip_ID: {
		required: true
		},
		Package_ID: {
		required: true
		},
		No_of_People: {
		required: true
		},
		Package_Cost: {
			required: true
		},
		Total_Cost: {
		required: true
		},
		Vehicle: {
		required: true
		},
		Additional_Remarks: {
		required: true
		},
		Addon_cost: {
			required: true
		},
		Status: {
			required: true
		},
		
	},
	messages: {
		Trip_ID: {
			required: "Please Select a Trip Name"
		},
		Package_ID: {
			required: "Please Select a Package Name"
		},
		No_of_People: {
			required: "Please Provide a No of People"
		},
		Package_Cost: {
			required: "Please Provide a Package Cost"
		},
		Total_Cost: {
			required: "Please Provide a Total Cost"
		},
		Vehicle: {
			required: "Please Provide a Vehicle Name"
		},
		Additional_Remarks: {
			required: "Please Provide a Additional Remarks"
		},
		Addon_cost: {
			required: "Please Provide a Addon cost"
		},
		Status: {
			required: "Please Select a Status"
		},
	},
	errorPlacement: function(error, element) {
    if (element.attr("name") == "Status") {
    error.insertAfter("#requestorStatus");
    } else {
    error.insertAfter(element);
    }
  },
	
});


// validate address add form on keyup and submit
$("#bikation-rating").validate({
	rules: {
		
		desc: {
		required: true
		},
	},
	messages: {
		desc: {
			required: "Write your review here"
		},
	},
	
});


// validate addon form on keyup and submit
$("#frmTasks").validate({
	rules: {
		
		first_name: {
		required: true
		},
		Last_name: {
		required: true
		},
		email: {
		required: true
		},
		mobile_num: {
			required: true
		},
		password: {
			required: true
		},
		
	},
	messages: {
		first_name: {
			required: "Please Provide a First Name"
		},
		last_name: {
			required: "Please Provide a Last Name"
		},
		email: {
			required: "Please Provide a Email"
		},
		mobile_num: {
			required: "Please Provide Mobile Number"
		},
		password: {
			required: "Please Provide Password"
		},
	},
});


$("#vertical-menu h3").click(function () {
    //slide up all the link lists
    $("#vertical-menu ul ul").slideUp();
    $('.plus',this).html('+');
    //slide down the link list below the h3 clicked - only if its closed
    if (!$(this).next().is(":visible")) {
        $(this).next().slideDown();
        //$(this).remove("span").append('<span class="minus">-</span>');
        $('.plus').html('+');
        $('.plus',this).html('-');
    }
})


