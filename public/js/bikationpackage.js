var Package = {
	BikationPackage:{},
	PromoCode: {}
};
Package.BikationPackage.init = function(){
	Package.BikationPackage.editBikeModel();
	Package.BikationPackage.editSpecBikeModel();
}.bind(Package.BikationPackage);
Package.BikationPackage.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Description = $(this).data('description');
		var name = $(this).data('name');
		var No_of_Tickets_Cost_1px = $(this).data('noofticketscost1px');
		var Tickes_Remaining_Cost_1px = $(this).data('tickesremainingcost1px');
		var Cost_1px = $(this).data('cost1px');
		var No_of_Tickets_Cost_2px = $(this).data('noofticketscost2px');
		var Tickes_Remaining_Cost_2px = $(this).data('tickesremainingcost2px');
		var Cost_2px = $(this).data('cost2px');
		var Status = $(this).data('status');
		var rental_bike_cost = $(this).data('rental_bike_cost');

		$('#editBikeModel-modal form').attr("action","/bikation-package/edit/"+id);
		$('#editBikeModel-modal form textarea[name=Description]').data("wysihtml5").editor.setValue(Description);
		$('#editBikeModel-modal form input[name=name]').val(name);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		$('#editBikeModel-modal form input[name=No_of_Tickets_Cost_1px]').val( 	No_of_Tickets_Cost_1px);
		$('#editBikeModel-modal form input[name=Tickes_Remaining_Cost_1px]').val(Tickes_Remaining_Cost_1px);
		$('#editBikeModel-modal form input[name=Cost_1px]').val(Cost_1px);
		$('#editBikeModel-modal form input[name=No_of_Tickets_Cost_2px]').val(No_of_Tickets_Cost_2px);
		$('#editBikeModel-modal form input[name=Tickes_Remaining_Cost_2px]').val(Tickes_Remaining_Cost_2px);
		$('#editBikeModel-modal form input[name=Cost_2px]').val(Cost_2px);
		$('#editBikeModel-modal form input[name=rental_bike_cost]').val(rental_bike_cost);
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);

		$('#editBikeModel-modal').modal('show');
	});
}.bind(Package.BikationPackage);

Package.BikationPackage.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-package/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Package.BikationPackage);
