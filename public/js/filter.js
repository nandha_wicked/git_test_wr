Booking.Filter = {
	showAvailable: false,
	makeId: ''
};

Booking.Filter.init = function(){
  Booking.Filter.initEventHandlers();
  Booking.Filter.checkForFilters();
}.bind(Booking.Filter);

Booking.Filter.initEventHandlers = function(){

	$('select[name="sortOption"]').change(function(){
		var model = Booking.Filter.sortModel();
		var html = Booking.Filter.generateHtml(model);
		if(html != ''){
	    	$('#show-bikes').removeClass('noResult').html(html);
	    	Booking.ChooseBike.initEventHandlers();
	    }else{
	    	$('#show-bikes').addClass('noResult').html('No Results found Please try modifying the filters');
	    }
	});

	$('select[name="makeFilter"]').change(function(){
		var model = Booking.Filter.sortModel();
		Booking.Filter.makeId = $(this).val();
		var html = Booking.Filter.generateHtml(model);
	    if(html != ''){
	    	$('#show-bikes').removeClass('noResult').html(html);
	    	Booking.ChooseBike.initEventHandlers();
	    }else{
	    	$('#show-bikes').addClass('noResult').html('No Results found Please try modifying the filters');
	    }
	});

	$('#showAvailable').on('change', function() {
		var model = Booking.Filter.sortModel();
		if($(this).is(':checked')){
			Booking.Filter.showAvailable = true;
		}else{
			Booking.Filter.showAvailable = false;
		}
		var html = Booking.Filter.generateHtml(model);
	    if(html != ''){
	    	$('#show-bikes').removeClass('noResult').html(html);
	    	Booking.ChooseBike.initEventHandlers();
	    }else{
	    	$('#show-bikes').addClass('noResult').html('No Results found Please try modifying the filters');
	    }
	});
}.bind(Booking.Filter);

Booking.Filter.checkForFilters = function(){
	var opt = $('select[name="sortOption"]').val();
	if($('#showAvailable').is(':checked')){
		Booking.Filter.showAvailable = true;
	}else{
		Booking.Filter.showAvailable = false;
	}
	Booking.Filter.makeId = $('select[name="makeFilter"]').val();
	if(opt != '' || Booking.Filter.showAvailable || Booking.Filter.makeId != ''){
		var model = Booking.Filter.sortModel();
		var html = Booking.Filter.generateHtml(model);
		if(html != ''){
	    	$('#show-bikes').removeClass('noResult').html(html);
	    	Booking.ChooseBike.initEventHandlers();
	    }else{
	    	$('#show-bikes').addClass('noResult').html('No Results found Please try modifying the filters');
	    }
	}
}

Booking.Filter.getModelAsc = function(model){
	model.sort(function(a, b){
        return a.effectivePrice - b.effectivePrice;
    });
    return model;
}

Booking.Filter.getModelDsc = function(model){
	model.sort(function(a, b){
        return b.effectivePrice - a.effectivePrice;
    });
    return model;
}

Booking.Filter.sortModel = function(){
	var opt = $('select[name="sortOption"]').val();
	var model = ''
	if(opt === 'priceAsc'){
		model = Booking.Filter.getModelAsc(bikeJson);
	}else if(opt === 'priceDsc'){
		model = Booking.Filter.getModelDsc(bikeJson);
	}else{
		model = originalBikeJson;
	}
	return model;
}

Booking.Filter.generateHtml = function(model){
	
	html = '';
    for(x in model){
      if( x < model.length){
      	if(Booking.Filter.showAvailable){
      		if(!model[x].bikeAvailability){
      			continue;
      		}
      	}
      	if(Booking.Filter.makeId != '' && Booking.Filter.makeId != null){
      		if($.inArray(''+model[x].make_id , Booking.Filter.makeId) == -1 ){
      			continue;
      		}
      	}
        var imageUrl = '';
        if(model[x].image && model[x].image.full){
          imageUrl = model[x].image.full;
        }else{
          imageUrl = '/images/wicked-ride-logo.png';
        }
        var availableLoc = model[x].available_locations;
        var notAvailableLocations = model[x].not_available_locations;
        var availableArea = '';
        var nonAvailableArea = '';
        availableArea += '';
        for(a in availableLoc){
          availableArea += [
            '<option value="'+availableLoc[a].id+'" areaName="'+availableLoc[a].area+'" areaAddress="'+availableLoc[a].address+'">'+availableLoc[a].area+" "+availableLoc[a].area_warning+'</option>',
          ].join('');
        }
        for(b in notAvailableLocations){
          nonAvailableArea += [
            '<option  value="'+notAvailableLocations[b].id+'" areaName="'+notAvailableLocations[b].area+'" areaAddress="'+notAvailableLocations[b].address+'" disabled="disabled">'+notAvailableLocations[b].area+'</option>',
          ].join('');
        }
        var bikeAvblClass='';
        var bikeNtAvblClass='';
        var inlineStyle='';
        if(model[x].bikeAvailability){
          bikeAvblClass = 'show';
          bikeNtAvblClass = '';
          inlineStyle = 'style="display:none;"';
        }else{
          bikeAvblClass = '';
          bikeNtAvblClass = 'show';
          inlineStyle = '';
        }
        
        if(model[x].full_slab_price > model[x].totalPrice){  
            slashThrough = '<div class="full_slab_price"> RS '+model[x].full_slab_price+'</div>';
        }
        else
        {
            slashThrough  = '';
        }

        html += [ '<form method="POST" action="/booking/checkUserLoggedin" data-model-id="'+model[x].id+'">',
                  '<ul class="book_bike_fleet happy_customers item">',
                  '<li>',
                  '<h6 class="">'+model[x].name+'</h6>',
                  '<div class=""><img src="'+imageUrl+'"></div>',
                  '<div class="clear_both"></div>',
                  '<div class="bike_price"><div class="small_caps"> HOURLY RATE</div><div class="price effective-price">RS '+model[x].effectivePrice+'</div></div>',
                  '</li>',
                  '<li class="width_more">',
                  '<div class="date-time-calendar"><div class="choose-bike-datetime-picker">',
                  '<div class="no-border">',
                  '<div class="choose-bike-date"><input readonly autocomplete="off" placeholder="Date" class="startDatePicker" name="start_date" type="text" value="'+model[x].startDate+'" autocomplete="off"></div>',
                  '<div class="choose-bike-time"><input readonly autocomplete="off" placeholder="Time" class="startTimePicker" name="start_time" type="text" value="'+model[x].startTime+'" autocomplete="off"></div>',
                  '</div>',
                  '<div class="no-border to-bar"><span class="inbetween">to</span></div>',
                  '<div class="no-border">',
                  '<div class="choose-bike-date"><input readonly autocomplete="off" placeholder="Date" class="endDatePicker" name="end_date" type="text" class="date_picker" value="'+model[x].endDate+'" autocomplete="off"></div>',
                  '<div class="choose-bike-time"><input readonly autocomplete="off" placeholder="Time" class="endTimePicker" name="end_time" type="text"  class="time_picker" value="'+model[x].endTime+'" autocomplete="off"></div>',              
                  '</div>',
                  '<div class="no-border edit-btn"><span class="edit-button"></span></div><div class="clear_both"></div></div></div>',
                  '<div class="clear_both"></div><div class="step2-error"></div>',
                  '<div class="booking_date_main"><div class="celect_city"><div class="celect_area"><label>',
                  '<select name="areaName" placeholder="Select Area" class="js-states form-control" data-model-id="'+model[x].id+'">',
                  '<optgroup label="Available at">',
                  availableArea,
                  '</optgroup><optgroup label="Not available at">',
                  nonAvailableArea,
                  '</optgroup></select></label></div></div></div>',
                  '<div class="small_caps message_line">'+model[x].minimum_billing_message+'</div>',
                  '</li>',
                  '<li class="price-section">',
                  '<div class="book_now">',
                  '<div class="bike_price"><div class="small_caps">total tariff</div><div class="price total-price">RS '+model[x].totalPrice+'</div>'+slashThrough+'</div> ',
                  '<div class="choose-bike-cta">',
                  '<div class="booking-step2-continue common_btn_small book-btn '+bikeAvblClass+'"><a href="#">Book</a></div>',
                  '<div class="sold-out-wrapper '+bikeNtAvblClass+'"><div>Sold Out</div><div class="avail-btn-wrapper"><span></span><div class="avail-btn">check availability</div></div></div>',
                  '</div>',
                  '<div class="common_btn_small book-btn" '+inlineStyle+'>',
                  '<a href="#" data-startdate="'+model[x].startDate+'" data-starttime="'+model[x].startTime+'" data-enddate="'+model[x].endDate+'" data-endtime="'+model[x].endTime+'"  data-modelname="'+model[x].name+'" class="enquire-now">Enquire</a>',
                  '</div>',
                  '</div>',
                  '</li>',
                  '<input type="hidden" name="modelId" value="'+model[x].id+'" />',
                  '<input type="hidden" name="cityId" value="'+cityId+'" />',
                  '<input type="hidden" name="number_of_hours" value="'+model[x].number_of_hours+'" />',
                  '<input type="hidden" name="minimum_hours" value="'+model[x].minimum_hours+'" />',
                  '<input type="hidden" name="totalPrice" value="'+model[x].totalPrice+'"/>',
                  '<input type="hidden" name="full_slab_price" value="'+model[x].full_slab_price+'"/>',
                  '<input type="hidden" name="areaId" value="'+model[x].available_locations[0].id+'"/>',
                  '<input type="hidden" name="areaFullName" value="'+model[x].available_locations[0].area+'"/>',
                  '<input type="hidden" name="storeOpen" value="'+model[x].available_locations[0].store_open+'"/>', 
                  '<input type="hidden" name="areaWarning" value="'+model[x].available_locations[0].area_warning+'"/>',  
                  '<input type="hidden" name="_token" value="'+csrf_token+'">',
                  '</ul></form>',
        ].join('');
      }
    }

    return html;
}
