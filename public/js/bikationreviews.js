var Reviews = {
	BikationReviews:{},
	PromoCode: {}
};

Reviews.BikationReviews.init = function(){
	Reviews.BikationReviews.editBikeModel();
}.bind(Reviews.BikationReviews);

Reviews.BikationReviews.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Customer_ID = $(this).data('customerid');
		var Overall = $(this).data('overall');
		var Planning = $(this).data('planning');
		var On_Schedule = $(this).data('onschedule');
		var Food = $(this).data('sood');
		var Stay = $(this).data('stay');
		var Activities = $(this).data('activities');
		var Remarks = $(this).data('remarks');
		var Status = $(this).data('status');
		
		$('#editBikeModel-modal form').attr("action","/bikation-reviews/edit/"+id);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		$('#editBikeModel-modal form input[type=text]').val(Customer_ID);
		$('#editBikeModel-modal form input[name=Overall]').val(Overall);
		$('#editBikeModel-modal form input[name=Planning]').val(Planning);
		$('#editBikeModel-modal form input[name=On_Schedule]').val(On_Schedule);
		$('#editBikeModel-modal form input[name=Food]').val(Food);
		$('#editBikeModel-modal form input[name=Stay]').val(Stay);
		$('#editBikeModel-modal form input[name=Activities]').val(Activities);
		$('#editBikeModel-modal form textarea[name=Remarks]').val(Remarks);
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Reviews.BikationReviews);

