var Admin = {
	Bikation_Bookings:{}
	
};

Admin.Bikation_Bookings.init = function(){
	Admin.Bikation_Bookings.editBikeModel();
}.bind(Admin.Bikation_Bookings);

Admin.Bikation_Bookings.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('TripID');
		var Customer_ID = $(this).data('CustomerID');
		var Package_ID = $(this).data('PackageID');
		var Booked_On = $(this).data('BookedOn');
		var Booked_By = $(this).data('BookedBy');
		var No_of_People = $(this).data('NoofPeople');
		var Status = $(this).data('Status');
		var Package_Cost = $(this).data('PackageCost');
		var Addon_cost = $(this).data('Addoncost');
		var Total_Cost = $(this).data('TotalCost');
		var Cancelled_On = $(this).data('CancelledOn');
		var Cancelled_By = $(this).data('CancelledBy');
		var Vehicle = $(this).data('Vehicle');
		var Additional_Remarks = $(this).data('AdditionalRemarks');

		$('#editBikeModel-modal form').attr("action","/admin/bikeModel/edit/"+id);
		$('#editBikeModel-modal form input[type=text]').val(Trip_ID);
		$('#editBikeModel-modal form input[type=text]').val(Customer_ID);
		$('#editBikeModel-modal form input[type=text]').val(Package_ID);
		$('#editBikeModel-modal form input[type=text]').val(Booked_On);
		$('#editBikeModel-modal form input[type=text]').val(Booked_By);
		$('#editBikeModel-modal form input[type=text]').val(No_of_People);
		$('#editBikeModel-modal form input[type=text]').val(Status);
		$('#editBikeModel-modal form input[type=text]').val(Package_Cost);
		$('#editBikeModel-modal form input[type=text]').val(Addon_cost);
		$('#editBikeModel-modal form input[type=text]').val(Total_Cost);
		$('#editBikeModel-modal form input[type=text]').val(Cancelled_On);
		$('#editBikeModel-modal form input[type=text]').val(Cancelled_By);
		$('#editBikeModel-modal form input[type=text]').val(Package_Cost);
		$('#editBikeModel-modal form input[type=text]').val(Vehicle);
		$('#editBikeModel-modal form input[type=text]').val(Additional_Remarks);		
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Admin.Bikation_Bookings);

