var Backend = {
	Booking: {
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
	BikeFilter:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    BikeFilterVendor:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    BikeFilterPartner:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    BikeFilterErp:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    BikeFilterAMH:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    BikeFilterCollection:{
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
    ServiceBlock: {
		startDatePicker: [],
		startDatePickerEl: [],
		endDatePicker: [],
		endDatePickerEl: [],
		startTimePicker: [],
		startTimePickerEl: [],
		endTimePicker: [],
		endTimePickerEl: [],
	},
};


	Backend.Booking.init = function(){
		Backend.Booking.initHelperFns();
		Backend.Booking.initFromDateTimePicker();
		Backend.Booking.initToDateTimePicker();
		$('form[name=add-booking] select[name=model]').change(function(){
			Backend.Booking.priceSuggest();
		});
		$('form[name=add-booking] select[name=area]').change(function(){
			Backend.Booking.priceSuggest();
		});
		$('form[name=add-booking] input[name=endTime]').change(function(){
			Backend.Booking.priceSuggest();
		});
		$('form[name=add-booking] input[name=coupon]').change(function(){
			Backend.Booking.priceSuggest();
		});
        
        $('form[name=add-booking-inPage] select[name=model]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=add-booking-inPage] select[name=area]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=add-booking-inPage] input[name=endTime]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=add-booking-inPage] input[name=coupon]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
        
   
		$('form[name=booking_edit_form] select[name=model]').change(function(){
			Backend.Booking.priceSuggestEdit();
		});
		$('form[name=booking_edit_form] select[name=area]').change(function(){
			Backend.Booking.priceSuggestEdit();
		});
		$('form[name=booking_edit_form] input[name=endTime]').change(function(){
			Backend.Booking.priceSuggestEdit();
		});
		$('form[name=booking_edit_form] input[name=coupon]').change(function(){
			Backend.Booking.priceSuggestEdit();
		});
        
        $('form[name=booking_edit_form-inPage] select[name=model]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=booking_edit_form-inPage] select[name=area]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=booking_edit_form-inPage] input[name=endTime]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		$('form[name=booking_edit_form-inPage] input[name=coupon]').change(function(){
			Backend.Booking.priceSuggestInPageForm();
		});
		
	}.bind(Backend.Booking);

	Backend.BikeFilter.init = function(){
		Backend.BikeFilter.initFromDateTimePicker();
		Backend.BikeFilter.initToDateTimePicker();
		Backend.BikeFilter.getBikeDetailsOnFilter();
		Backend.BikeFilter.table = $('#all-bookings').DataTable({});
	}.bind(Backend.BikeFilter);


    Backend.BikeFilterVendor.init = function(){
		Backend.BikeFilterVendor.initFromDateTimePicker();
		Backend.BikeFilterVendor.initToDateTimePicker();
		Backend.BikeFilterVendor.getBikeDetailsOnFilter();
		Backend.BikeFilterVendor.table = $('#all-bookings').DataTable({});
	}.bind(Backend.BikeFilterVendor);

    Backend.BikeFilterPartner.init = function(){
		Backend.BikeFilterPartner.initFromDateTimePicker();
		Backend.BikeFilterPartner.initToDateTimePicker();
		Backend.BikeFilterPartner.getBikeDetailsOnFilter();
		Backend.BikeFilterPartner.table = $('#all-bookings').DataTable({});
	}.bind(Backend.BikeFilterPartner);

    Backend.BikeFilterErp.init = function(){
		Backend.BikeFilterErp.initFromDateTimePicker();
		Backend.BikeFilterErp.initToDateTimePicker();
		Backend.BikeFilterErp.getBikeDetailsOnFilter();
		Backend.BikeFilterErp.table = $('#all-bookings').DataTable({});
	}.bind(Backend.BikeFilterErp);

    Backend.BikeFilterAMH.init = function(){
		Backend.BikeFilterAMH.initFromDateTimePicker();
		Backend.BikeFilterAMH.initToDateTimePicker();
		Backend.BikeFilterAMH.getBikeDetailsOnFilter();
		Backend.BikeFilterAMH.table = $('#all-bookings').DataTable({});
	}.bind(Backend.BikeFilterAMH);

    Backend.ServiceBlock.init = function(){
		Backend.ServiceBlock.initHelperFns();
		Backend.ServiceBlock.initFromDateTimePicker();
		Backend.ServiceBlock.initToDateTimePicker();
		
		
	}.bind(Backend.ServiceBlock);

	Backend.Booking.initHelperFns = function(){
		// addHour fn
		Date.prototype.addHours= function(h){
			this.setHours(this.getHours()+h);
			return this;
		}
	}

	Backend.Booking.validateBookingForm = function(){
		el = $('input[type=submit]');
		var isDisabled = el.is(':disabled');
		if (isDisabled) 
	        return false;
	    else {
			el.prop('disabled', true);
	    }
		var startDate = $('input[name="startDate"]').val();
		var startTime = $('input[name="startTime"]').val();
		var endDate = $('input[name="endDate"]').val();
		var endTime = $('input[name="endTime"]').val();

		if(!startDate || !endDate || !startTime || !endTime){
		    $('#date-error').text("ERROR : Please Enter Both Date & Time");
		    return false;
		}
		if( startDate == endDate){
			if(startTime == endTime){
				$('#date-error').text("ERROR : Please Choose Some Other Time");
				return false;
			}
			else if(startTime > endTime){
				$('#date-error').text("ERROR : Start Time Greater Than End Time");
				return false;
			}
			else{
				$('#date-error').text('');
				return false;
			}
		}
		if(startDate > endDate){
			$('#date-error').text("ERROR : Start Date Greater Than End Date");
			return false;
		}
		$('#date-error').text('');
		return true;
	}.bind(Backend.Booking);

	Backend.Booking.initFromDateTimePicker = function(el){
		$('#startDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
			minDate: 0,
			onSelectDate: function(selectedDateTime, $el){
				var fromDateEl = $('#startDate');
				var fromTimeEl = $('#startTime');
				var fromDate = fromDateEl.val();
				var fromTime = fromTimeEl.val();
				var toDateEl = $('#endDate');
				var toTimeEl = $('#endTime');
				
				// CLEAR OTHER DATE-TIME SELECTIONS
				fromTimeEl.val('');
				toDateEl.val('');
				toTimeEl.val('');

				// GET WORKING SLOTS FOR THE SELECTED DATE
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'GET',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var now = new Date();
						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
						if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
							startTime = now.dateFormat('H:i');
						}
						
						$('#startTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes,
							onSelectTime: function( dt, $el ){
								Backend.Booking.initToDateTimePicker();
								$('#endDate').datetimepicker('show').focus();
							}
						}).datetimepicker('show').focus();
					}
				});
			}
		});
        
        $('#editstartDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
			minDate: 0,
			onSelectDate: function(selectedDateTime, $el){
				var fromDateEl = $('#editstartDate');
				var fromTimeEl = $('#editstartTime');
				var fromDate = fromDateEl.val();
				var fromTime = fromTimeEl.val();
				var toDateEl = $('#editendDate');
				var toTimeEl = $('#editendTime');
				
				// CLEAR OTHER DATE-TIME SELECTIONS
				fromTimeEl.val('');
				toDateEl.val('');
				toTimeEl.val('');

				// GET WORKING SLOTS FOR THE SELECTED DATE
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'GET',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var now = new Date();
						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
						if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
							startTime = now.dateFormat('H:i');
						}
						
						$('#editstartTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes,
							onSelectTime: function( dt, $el ){
								Backend.Booking.initToDateTimePicker();
								$('#editendDate').datetimepicker('show').focus();
							}
						}).datetimepicker('show').focus();
					}
				});
			}
		});
	}.bind(Backend.Booking);

	Backend.Booking.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDate');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTime');
				toTimeEl.val('');
				$.ajax({
					url : '/api/to-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDate');
						var fromTimeEl = $('#startTime');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
        
        var editfromDateEl = $('#editstartDate');
		var editfromDate = editfromDateEl.val();
		var editfromDateObj = new Date(editfromDate);
		var editminDate = editfromDateObj.dateFormat('Y/m/d');
        
        $('#editendDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: editminDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#editendTime');
				toTimeEl.val('');
				$.ajax({
					url : '/api/to-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#editstartDate');
						var fromTimeEl = $('#editstartTime');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#editendTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.Booking);

	Backend.addHour = function(time){
		var hourArr = time.split(":");
		var hourNo = parseInt(hourArr);
		var oneHrExtra = hourNo + 1;
		var hourNew = oneHrExtra.toString() + ':00';
		return hourNew;
	}.bind(Backend);

	Backend.Booking.makeBooking = function(){
		// $('#create-booking').click(function(){
		// 	var startDate = $('#toValidateStartDate').val();
		// 	var endDate = $('#toValidateEndDate').val();
		// 	var startTime = $('#toValidateStartTime').val();
		// 	var endTime = $('#toValidateEndTime').val();
		// 	var modelId = $('#adminBookingModelId').val();
		// 	var areaId = $('#adminBookingArealId').val();
			
		// });
	}.bind(Backend.Booking);

	Backend.Booking.allowedTimes = function(data){
		var allowed = data['result']['data']['slots'];
		var dtDefault = data['result']['data']['slots'][0]['start_time'];
		var time = dtDefault.substring( 0, dtDefault.lastIndexOf( ":00" ) );
		var dtallowTimes = [];
		for (var item in allowed) {
			allTimes = allowed[item]['start_time'].substring( 0, allowed[item]['start_time'].lastIndexOf( ":00" ) );
		    dtallowTimes.push(allTimes);
		}
		return dtallowTimes;
	}.bind(Backend.Booking);

	Backend.BikeFilter.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilter.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilter.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilter);

	Backend.BikeFilter.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilter);

	Backend.BikeFilter.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilter.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilter.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilter);

	Backend.BikeFilter.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td><td>'+val['price']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilter);

	Backend.BikeFilter.getAllBooking = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bookings',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
                'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data']['bookings'];
				Backend.BikeFilter.table.destroy();
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
					var mobile = (val['mobile'] === null) ? '' : val['mobile'];
					var note = (val['note'] === null) ? '' : val['note'];
				  	var html = '<tr><td>'+val['reference_id']+'</td><td>'+val['start_datetime']+'</td><td>'+val['end_datetime']+'</td><td>'+val['custName']+'</td><td>'+val['email']+'</td><td>'+mobile+'</td><td>'+val['model']+'</td><td>'+val['area']+'</td><td>'+note+'</td><td>'+val['total_price']+'</td></tr>';
				   htmlArr.push(html);
				});
				$(".wk-table2 tbody").html(htmlArr);
				Backend.BikeFilter.table = $('#all-bookings').DataTable({});
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilter);

	Backend.BikeFilter.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilter);

	Backend.Booking.priceSuggest = function(){
		el = $('#addBooking-modal');
		$(el).find('#data-error').text('');
		var modelId = el.find('select[name="model"]').val();
		var areaId = el.find('select[name="area"]').val();
		var cityId = el.find('select[name=area]').find(":selected").data('city');
		var fromDateEl = el.find('input[name="startDate"]');
		var fromDate = fromDateEl.val();
		var fromTimeEl = el.find('input[name="startTime"]');
		var fromTime = fromTimeEl.val();
		var toDateEl = el.find('input[name="endDate"]');
		var toDate = toDateEl.val();
		var toTimeEl = el.find('input[name="endTime"]');
		var toTime = toTimeEl.val();
		var coupon = el.find('input[name="coupon"]').val();

		if(toDate == '' || toTime == '' || fromDate == '' || fromTime == '' || areaId == '' || modelId == '' || cityId == ''){
		if(toDate == '')
		  toDateEl.focus();
		if(toTime == '')
		  toTimeEl.focus();
		if(fromDate == '')
		  fromDateEl.focus();
		if(fromTime == '')
		  fromTimeEl.focus();
		return;
		}

		$.ajax({
			url : '/api/bookings/price-and-check-availability',
			type : 'GET',
			data : { 
			  'model_id': modelId,
			  'area_id': areaId,
			  'city_id': cityId,
			  'start_date': fromDate,
			  'start_time': fromTime,
			  'end_date': toDate,
			  'end_time': toTime,
			  'with_vat': 'true',
			  'coupon': coupon
            
			},
			ele: el,
			success: function(res){
			 	$(this.ele).find('input[name=amount]').val(res['result']['data']['total_price_with_tax']);
			}

		});
	}.bind(Backend.Booking);

    Backend.Booking.priceSuggestEdit = function(){
		el = $('#editBooking-modal');
		$(el).find('#data-error').text('');
		var modelId = el.find('select[name="model"]').val();
		var areaId = el.find('select[name="area"]').val();
		var cityId = el.find('select[name=area]').find(":selected").data('city');
		var fromDateEl = el.find('input[name="startDate"]');
		var fromDate = fromDateEl.val();
		var fromTimeEl = el.find('input[name="startTime"]');
		var fromTime = fromTimeEl.val();
		var toDateEl = el.find('input[name="endDate"]');
		var toDate = toDateEl.val();
		var toTimeEl = el.find('input[name="endTime"]');
		var toTime = toTimeEl.val();
		var coupon = el.find('input[name="coupon"]').val();

		if(toDate == '' || toTime == '' || fromDate == '' || fromTime == '' || areaId == '' || modelId == '' || cityId == ''){
		if(toDate == '')
		  toDateEl.focus();
		if(toTime == '')
		  toTimeEl.focus();
		if(fromDate == '')
		  fromDateEl.focus();
		if(fromTime == '')
		  fromTimeEl.focus();
		return;
		}

		$.ajax({
			url : '/api/bookings/price-and-check-availability',
			type : 'GET',
			data : { 
			  'model_id': modelId,
			  'area_id': areaId,
			  'city_id': cityId,
			  'start_date': fromDate,
			  'start_time': fromTime,
			  'end_date': toDate,
			  'end_time': toTime,
			  'with_vat': 'true',
			  'coupon': coupon
            
			},
			ele: el,
			success: function(res){
			 	$(this.ele).find('input[name=amount]').val(res['result']['data']['total_price_with_tax']);
			}

		});
	}.bind(Backend.Booking);

    Backend.Booking.priceSuggestInPageForm = function(){
		el = $(window);
		$('#data-error').text('');
		var modelId = $('select[name="model"]').val();
		var areaId = $('select[name="area"]').val();
		var cityId = $('select[name=area]').find(":selected").data('city');
		var fromDateEl = $('input[name="startDate"]');
		var fromDate = fromDateEl.val();
		var fromTimeEl = $('input[name="startTime"]');
		var fromTime = fromTimeEl.val();
		var toDateEl = $('input[name="endDate"]');
		var toDate = toDateEl.val();
		var toTimeEl = $('input[name="endTime"]');
		var toTime = toTimeEl.val();
		var coupon = $('input[name="coupon"]').val();

		if(toDate == '' || toTime == '' || fromDate == '' || fromTime == '' || areaId == '' || modelId == '' || cityId == ''){
		if(toDate == '')
		  toDateEl.focus();
		if(toTime == '')
		  toTimeEl.focus();
		if(fromDate == '')
		  fromDateEl.focus();
		if(fromTime == '')
		  fromTimeEl.focus();
		return;
		}

		$.ajax({
			url : '/api/bookings/price-and-check-availability',
			type : 'GET',
			data : { 
			  'model_id': modelId,
			  'area_id': areaId,
			  'city_id': cityId,
			  'start_date': fromDate,
			  'start_time': fromTime,
			  'end_date': toDate,
			  'end_time': toTime,
			  'with_vat': 'true',
			  'coupon': coupon
            
			},
			ele: el,
			success: function(res){
			 	$('input[name=amount]').val(res['result']['data']['total_price_with_tax']);
			}

		});
	}.bind(Backend.Booking);







	Backend.BikeFilterVendor.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterVendor.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterVendor.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterVendor);

	Backend.BikeFilterVendor.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterVendor.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterVendor);

	Backend.BikeFilterVendor.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterVendor.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterVendor.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterVendor);

	Backend.BikeFilterVendor.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count-vendor',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterVendor);

	Backend.BikeFilterVendor.getAllBooking = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bookings-vendor',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
                'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data']['bookings'];
				Backend.BikeFilterVendor.table.destroy();
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
					var mobile = (val['mobile'] === null) ? '' : val['mobile'];
					var note = (val['note'] === null) ? '' : val['note'];
				  	var html = '<tr><td>'+val['reference_id']+'</td><td>'+val['start_datetime']+'</td><td>'+val['end_datetime']+'</td><td>'+val['custName']+'</td><td>'+val['email']+'</td><td>'+mobile+'</td><td>'+val['model']+'</td><td>'+val['area']+'</td><td>'+note+'</td><td>'+val['total_price']+'</td></tr>';
				   htmlArr.push(html);
				});
				$(".wk-table2 tbody").html(htmlArr);
				Backend.BikeFilterVendor.table = $('#all-bookings').DataTable({});
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterVendor);

	Backend.BikeFilterVendor.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterVendor);











Backend.BikeFilterPartner.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterPartner.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterPartner.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterPartner);

	Backend.BikeFilterPartner.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterPartner.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterPartner);

	Backend.BikeFilterPartner.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterPartner.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterPartner.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterPartner);

	Backend.BikeFilterPartner.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count-partner',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterPartner);

	Backend.BikeFilterPartner.getAllBooking = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bookings-partner',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
                'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data']['bookings'];
				Backend.BikeFilterPartner.table.destroy();
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
					var mobile = (val['mobile'] === null) ? '' : val['mobile'];
					var note = (val['note'] === null) ? '' : val['note'];
				  	var html = '<tr><td>'+val['reference_id']+'</td><td>'+val['start_datetime']+'</td><td>'+val['end_datetime']+'</td><td>'+val['custName']+'</td><td>'+val['model']+'</td><td>'+val['area']+'</td><td>'+val['total_price']+'</td></tr>';
				   htmlArr.push(html);
				});
				$(".wk-table2 tbody").html(htmlArr);
				Backend.BikeFilterPartner.table = $('#all-bookings').DataTable({});
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterPartner);

	Backend.BikeFilterPartner.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterPartner);







//Bike Filter for ERP



	Backend.BikeFilterErp.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterErp.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterErp.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterErp.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterErp.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterErp.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td><td>'+val['price']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterErp);

	

	Backend.BikeFilterErp.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterErp);





// Bike Filter for Collections

Backend.BikeFilterErp.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterErp.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterErp.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterErp.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterErp.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterErp.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterErp);

	Backend.BikeFilterErp.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td><td>'+val['price']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterErp);

	

	Backend.BikeFilterErp.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterErp);



//Bike Filter for AMH



	Backend.BikeFilterAMH.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterAMH.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterAMH.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterAMH.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterAMH.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterAMH.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count-AMH',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterAMH);

	

	Backend.BikeFilterAMH.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterAMH);





// Bike Filter for Collections

Backend.BikeFilterAMH.initFromDateTimePicker = function(el){
		$('#startDateFilter').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDateFilter');
			var fromTimeEl = $('#startTimeFilter');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDateFilter');
			var toTimeEl = $('#endTimeFilter');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/from-working-hours-web',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilterAMH.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTimeFilter').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							Backend.BikeFilterAMH.initToDateTimePicker();
							$('#endDateFilter').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDateFilter');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDateFilter').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTimeFilter');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDateFilter');
						var fromTimeEl = $('#startTimeFilter');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilterAMH.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTimeFilter').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.getBikeDetailsOnFilter = function(){
		$('a.btn').click(function(){
			var areaName = $('#filter-form select[name=areaId] option:selected').text();
			$('#filter-form input[name=areaName]').val(areaName);
			var startDate = $('input[name="startDateFilter"]').val();
			var startTime = $('input[name="startTimeFilter"]').val();
			var endDate = $('input[name="endDateFilter"]').val();
			var endTime = $('input[name="endTimeFilter"]').val();
			var areaId = $( "select[name='areaId'] option:selected").val();
			$('#bike-filter-from').text(startDate);
			$('#bike-filter-to').text(endDate);
			
			Backend.BikeFilterAMH.getBikeDetails(startDate, startTime, endDate, endTime, areaId);
			Backend.BikeFilterAMH.getAllBooking(startDate, startTime, endDate, endTime,areaId);
		});
	}.bind(Backend.BikeFilterAMH);

	Backend.BikeFilterAMH.getBikeDetails = function(startDate, startTime, endDate, endTime, areaId){
		$.ajax({
			url : '/api/filter/bike-count-AMH',
			type : 'get',
			data : { 
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'area_id': areaId
			},
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				$.each(result, function( index, val ) {
				  var html = '<tr><td>'+val['model_name']+'</td><td>'+val['total_bikes']+'</td><td>'+val['available_count']+'</td><td>'+val['not_available_count']+'</td></tr>';
				  htmlArr.push(html);
				});
				$(".wk-table1 tbody").html(htmlArr);
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	}.bind(Backend.BikeFilterAMH);

	

	Backend.BikeFilterAMH.formatSlots = function(slots){
		var allowedTimes = [];
		$.each(slots, function(i,slot){
			var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
			allowedTimes.push(slotStartStr);
		});
		return allowedTimes;
	}.bind(Backend.BikeFilterAMH);




//End of AMH




    Backend.ServiceBlock.initHelperFns = function(){
		// addHour fn
		Date.prototype.addHours= function(h){
			this.setHours(this.getHours()+h);
			return this;
		}
	}

	Backend.ServiceBlock.validateServiceBlockForm = function(){
		el = $('input[type=submit]');
		var isDisabled = el.is(':disabled');
		if (isDisabled) 
	        return false;
	    else {
			el.prop('disabled', true);
	    }
		var startDate = $('input[name="startDate"]').val();
		var startTime = $('input[name="startTime"]').val();
		var endDate = $('input[name="endDate"]').val();
		var endTime = $('input[name="endTime"]').val();

		
		$('#date-error').text('');
		return true;
	}.bind(Backend.ServiceBlock);

	Backend.ServiceBlock.initFromDateTimePicker = function(el){
		$('#startDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
			minDate: 0,
			onSelectDate: function(selectedDateTime, $el){
				var fromDateEl = $('#startDate');
				var fromTimeEl = $('#startTime');
				var fromDate = fromDateEl.val();
				var fromTime = fromTimeEl.val();
				var toDateEl = $('#endDate');
				var toTimeEl = $('#endTime');
				
				// CLEAR OTHER DATE-TIME SELECTIONS
				fromTimeEl.val('');
				toDateEl.val('');
				toTimeEl.val('');

				// GET WORKING SLOTS FOR THE SELECTED DATE
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'GET',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var now = new Date();
						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
						if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
							startTime = now.dateFormat('H:i');
						}
						
						$('#startTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes,
							onSelectTime: function( dt, $el ){
								Backend.ServiceBlock.initToDateTimePicker();
								$('#endDate').datetimepicker('show').focus();
							}
						}).datetimepicker('show').focus();
					}
				});
			}
		});
	}.bind(Backend.ServiceBlock);

	Backend.ServiceBlock.initToDateTimePicker = function(el){
		var fromDateEl = $('#startDate');
		var fromDate = fromDateEl.val();
		var fromDateObj = new Date(fromDate);
		var minDate = fromDateObj.dateFormat('Y/m/d');

		$('#endDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			minDate: minDate,
			format:'Y-m-d',
			onSelectDate: function(selectedDateTime, $el){
				var toTimeEl = $('#endTime');
				toTimeEl.val('');
				$.ajax({
					url : '/api/from-working-hours-web',
					type : 'get',
					data : { 
						'date': selectedDateTime.dateFormat('Y-m-d')
					},
					success: function(data){
						var fromDateEl = $('#startDate');
						var fromTimeEl = $('#startTime');
						var fromDate = fromDateEl.val();
						var fromTime = fromTimeEl.val();
						var fromDateTime = fromDate + ' ' + fromTime;
						var fromDateTimeObj = new Date(fromDateTime);

						var slots = data['result']['data']['slots'];
						var allowedTimes = Backend.BikeFilter.formatSlots(slots);
						var startTime = allowedTimes[0];
						
						// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
						if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
							var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
							nextHour = new Date(nextHour);
							startTime = nextHour.dateFormat('H:i');
						}
						
						$('#endTime').datetimepicker({
							datepicker: false,
							scrollInput: false,
							format: 'H:i',
							defaultTime: startTime,
							minTime: startTime,
							allowTimes: allowedTimes
						}).datetimepicker('show').focus();
					},
				});
			}
		});
	}.bind(Backend.ServiceBlock);


	Backend.ServiceBlock.makeServiceBlock = function(){
		// $('#create-serviceBlock').click(function(){
		// 	var startDate = $('#toValidateStartDate').val();
		// 	var endDate = $('#toValidateEndDate').val();
		// 	var startTime = $('#toValidateStartTime').val();
		// 	var endTime = $('#toValidateEndTime').val();
		// 	var modelId = $('#adminServiceBlockModelId').val();
		// 	var areaId = $('#adminServiceBlockArealId').val();
			
		// });
	}.bind(Backend.ServiceBlock);

	Backend.ServiceBlock.allowedTimes = function(data){
		var allowed = data['result']['data']['slots'];
		var dtDefault = data['result']['data']['slots'][0]['start_time'];
		var time = dtDefault.substring( 0, dtDefault.lastIndexOf( ":00" ) );
		var dtallowTimes = [];
		for (var item in allowed) {
			allTimes = allowed[item]['start_time'].substring( 0, allowed[item]['start_time'].lastIndexOf( ":00" ) );
		    dtallowTimes.push(allTimes);
		}
		return dtallowTimes;
	}.bind(Backend.ServiceBlock);


	
