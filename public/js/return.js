var Return = {
	Add:{},
	Edit:{}
};

var options = [];

Return.init = function(){
    Return.addReturnModel();
	Return.editReturnModel();
    //Return.Add.validationCheck();
    //Return.Edit.validationCheck();
}.bind(Return);




Return.editReturnModel = function(){
    
    //Start open edit form and fill details from existing return
	$('.booking_return_edit_btn').click(function(){
        
        var id = $(this).data('id');
        var booking_id = $(this).data('booking_id');
        var booking_customer = $(this).data('booking_customer_edit');
        
        
        
		var formEl = $('form[name="edit-return"]');

		formEl.attr("action","/erp/return/edit/"+id);
        
        formEl.find('input[name="booking_ref_edit"]').val(booking_id);
        formEl.find('input[name="booking_customer_edit"]').val(booking_customer);
        
        $('#editModalHeading').html("Edit Return - "+booking_id);
		
        
        $.ajax({
                        type: "POST",
                        url: "/api/getBookingDetailsFromId",
                        data: { 
						'booking_id': id
					       },
                        cache: false,
                        success: function(data) {
                            var booking = data['result']['data']['booking'];
                            
                            
                            formEl.find('input[name="booking_ref_edit"]').val(booking.reference_id);
                            formEl.find('input[name="booking_customer_edit"]').val(booking.username);
                            formEl.find('input[name="price_per_hour_edit"]').val(booking.PricePerHour);
                            formEl.find('input[name="extra_km_price_edit"]').val(booking.ExtraKMPrice);
                            formEl.find('input[name="km_limit_edit"]').val(booking.KMlimit);
                            formEl.find('textarea[name="return_notes_edit"]').val(booking.delivery_notes);

                            $("#return_start_odo_label_edit").html(booking.start_odo);                            formEl.find('input[name="end_odo_edit"]').val(booking.end_odo);

                            $("#return_accessories_label_edit").html(booking.accessories_label);

                            $("#return_detail_label_edit").html("<pre><p> User Name: "+booking.username+"</p>"+"<p> Model: "+booking.delivered_model_name+"</p>"+"<p> Bike Number: "+booking.delivered_bike_number+"</p>"+"<p> Start Date and Time: "+booking.start_datetime+"</p>"+"<p> End Date and Time: "+booking.end_datetime+"</p>"+"<p> Area: "+booking.delivered_at_area_name+"</p></pre>");
                            
                            
                            
     
                        },
                        error: function(data) {
            
                            $('#editReturnModal').modal('hide');
                            $('#deliveryfailureModal').modal('show');
                            return 0;
                        }
                    });
        
		
		

		$('#editReturnModal').modal('show');
	});
    
    //End open edit form and fill details from existing return
    
    $('.bike_received_edit').change(function(){
        
       
    
        var bike_received_edit = $('input[name="bike_received_edit"]:checked').length > 0;
        
        if(bike_received_edit)
        {
            var accessories_received_edit = $('input[name="accessories_received_edit"]:checked').length > 0;
            if(accessories_received_edit)
            {
                $('.problem_notes_section_edit').addClass("hidden");
            }
            else
            {
                $('.problem_notes_section_edit').removeClass("hidden");   
                
            }

        }
        else
        {
            $('.problem_notes_section_edit').removeClass("hidden");  
        }
    });
    
    
    $('.accessories_received_edit').change(function(){
        
       
    
        var accessories_received_edit = $('input[name="accessories_received_edit"]:checked').length > 0;
        
        if(accessories_received_edit)
        {
            var bike_received_edit = $('input[name="bike_received_edit"]:checked').length > 0;
            if(bike_received_edit)
            {
                $('.problem_notes_section_edit').addClass("hidden");
            }
            else
            {
                $('.problem_notes_section_edit').removeClass("hidden"); 
            }

        }
        else
        {
            $('.problem_notes_section_edit').removeClass("hidden"); 
        }
    });
    
    
    $('.end_odo_edit').change(function(){
            
        var formEl = $('form[name="edit-return"]');        
        
        end_odo_edit = formEl.find('input[name="end_odo_edit"]').val();
        start_odo_edit = $("#return_start_odo_label_edit").html();
        price_per_hour_edit = formEl.find('input[name="price_per_hour_edit"]').val();
        extra_km_price_edit = parseInt(formEl.find('input[name="extra_km_price_edit"]').val(),10);
        km_limit_edit = parseInt(formEl.find('input[name="km_limit_edit"]').val(),10);
        
        extrakm = parseInt(end_odo_edit, 10) -  parseInt(start_odo_edit, 10);
        if(extrakm>km_limit_edit)
        {
            
            $("#extra_km_charges_edit").html((extrakm-km_limit_edit)*extra_km_price_edit);
            $('.extra-km-section-edit').removeClass("hidden");
            $('.extra-money-section-edit').removeClass("hidden");
        }
        else
        {
            $("#extra_km_charges_edit").html(0);
            $('.extra-km-section-edit').addClass("hidden");
        }
    });
    
    
    
    //Start Confirm Return Edit
    
    $('.booking_confirm_return_edit_btn').click(function(){
		
        var formEl = $('form[name="edit-return"]');
        
        var e = document.getElementById("collection_mode_edit");
        var collection_mode_edit = e.options[e.selectedIndex].text;
        
        var accessories_received_edit = $('input[name="accessories_received_edit"]:checked').length > 0;
        var bike_received_edit = $('input[name="bike_received_edit"]:checked').length > 0;
        
        if(accessories_received_edit)
        {
            accessories_received_edit_str = "YES";
        }
        else
        {
            accessories_received_edit_str = "NO";
        }
        if(bike_received_edit)
        {
            bike_received_edit_str = "YES";
        }
        else
        {
            bike_received_edit_str = "NO";
        }
           
        
        var return_detail_label_edit = $("#return_detail_label_edit").html();
        
        $text = "<p>&nbsp&nbsp"+return_detail_label_edit+"<br/>"+"&nbsp&nbsp"+"Start ODO : "+$("#return_start_odo_label_edit").html()+"<br/>"+"&nbsp&nbsp"+"End ODO : "+formEl.find('input[name="end_odo_edit"]').val()+"<br/>"+"&nbsp&nbsp"+"Bike Returned without Problems : "+bike_received_edit_str+"<br/>"+"&nbsp&nbsp"+"Accessories Returned without Problems : "+accessories_received_edit_str+"<br/>"+"&nbsp&nbsp"+"Problem Notes : "+formEl.find('textarea[name="return_problem_notes_edit"]').val()+"<br/>"+"&nbsp&nbsp"+"Extra Money : "+formEl.find('input[name="extra_money_edit"]').val()+"<br/>"+"&nbsp&nbsp"+"Collection Mode : "+collection_mode_edit+"<br/>"+"&nbsp&nbsp"+"Notes : "+formEl.find('textarea[name="return_notes_edit"]').val()+"</p>";
        
        
        $('#confirmEditModalContent').html($text);
        
        $('#confirmEditHeading').html("Confirm Return - "+formEl.find('input[name="booking_ref_edit"]').val()+" - "+formEl.find('input[name="booking_customer_edit"]').val());
        
                            
        
		$('#confirmReturnEditModal').modal('show');
        
	});
    
    //End Confirm Return Edit
    
    
    
    //Submit Edit form
    $('.booking_submit_return_edit_btn').click(function(){
        
        $('#edit-return').submit();
    });
    
    
    //Dismiss Edit modal
    $('.booking_close_return_edit_btn').click(function(){
        
        $('#editReturnModal').modal('hide');
        $('#confirmReturnEditModal').modal('hide');
    });
    
    
    
    
}.bind(Return);

Return.addReturnModel = function(){
    //Open Add return 
	$('.booking_return_btn').click(function(){
		
        var id = $(this).data('id');
        var username = $(this).data('username');
        var modelname = $(this).data('modelname');
        var bikenumber = $(this).data('bikenumber');
        var bookingref = $(this).data('bookingref');
        var startdate = $(this).data('startdate');
        var starttime = $(this).data('starttime');
        var enddate = $(this).data('enddate');
        var endtime = $(this).data('endtime');
        var area = $(this).data('area');
        var priceperhour = $(this).data('priceperhour');
        var extrakmprice = $(this).data('extrakmprice');
        var startodo = $(this).data('startodo');
        var accessorieslabel = $(this).data('accessorieslabel');
        var deliverynotes = $(this).data('deliverynotes');
        var kmlimit = $(this).data('kmlimit');
        if(!bikenumber)
            {
                $('#deliveryfailureModal').modal('show');
                return 0;
            }

		var formEl = $('form[name="add-return"]');

		formEl.attr("action","/erp/return/"+id);
        
        
        
        formEl.find('input[name="booking_ref_add"]').val(bookingref);
        formEl.find('input[name="booking_customer_add"]').val(username);
        formEl.find('input[name="price_per_hour_add"]').val(priceperhour);
        formEl.find('input[name="extra_km_price_add"]').val(extrakmprice);
        formEl.find('input[name="km_limit_add"]').val(kmlimit);
        formEl.find('input[name="end_odo_add"]').val(0);
        formEl.find('textarea[name="return_notes_add"]').val(deliverynotes);
        
        $("#return_start_odo_label_add").html(startodo);
        $("#return_accessories_label_add").html(accessorieslabel);
        
        $("#return_detail_label_add").html("<pre><p> User Name: "+username+"</p>"+"<p> Model: "+modelname+"</p>"+"<p> Bike Number: "+bikenumber+"</p>"+"<p> Start Date and Time: "+startdate+" "+starttime+"</p>"+"<p> End Date and Time: "+enddate+" "+endtime+"</p>"+"<p> Area: "+area+"</p></pre>");
        
		
        $('#addModalHeading').html("Return - "+bookingref);
        

		$('#addReturnModal').modal('show');
	});
    
    $('.bike_received_add').change(function(){
        
       
    
        var bike_received_add = $('input[name="bike_received_add"]:checked').length > 0;
        
        if(bike_received_add)
        {
            var accessories_received_add = $('input[name="accessories_received_add"]:checked').length > 0;
            if(accessories_received_add)
            {
                $('.problem_notes_section_add').addClass("hidden");
            }
            else
            {
                $('.problem_notes_section_add').removeClass("hidden");   
                
            }

        }
        else
        {
            $('.problem_notes_section_add').removeClass("hidden");  
        }
    });
    
    
    $('.accessories_received_add').change(function(){
        
       
    
        var accessories_received_add = $('input[name="accessories_received_add"]:checked').length > 0;
        
        if(accessories_received_add)
        {
            var bike_received_add = $('input[name="bike_received_add"]:checked').length > 0;
            if(bike_received_add)
            {
                $('.problem_notes_section_add').addClass("hidden");
            }
            else
            {
                $('.problem_notes_section_add').removeClass("hidden"); 
            }

        }
        else
        {
            $('.problem_notes_section_add').removeClass("hidden"); 
        }
    });
    
    
    $('.end_odo_add').change(function(){
            
        var formEl = $('form[name="add-return"]');        
        
        end_odo_add = formEl.find('input[name="end_odo_add"]').val();
        start_odo_add = $("#return_start_odo_label_add").html();
        price_per_hour_add = formEl.find('input[name="price_per_hour_add"]').val();
        extra_km_price_add = parseInt(formEl.find('input[name="extra_km_price_add"]').val(),10);
        km_limit_add = parseInt(formEl.find('input[name="km_limit_add"]').val(),10);
        booking_ref_add = formEl.find('input[name="booking_ref_add"]').val();
        
        $.ajax({
            
            type: "GET",
            url: "/api/get-extra-amount-for-return",
            data: { 
                'end_odo':end_odo_add,
                'start_odo':start_odo_add,
                'booking_ref': booking_ref_add
            },
            cache: false,
            success: function(data) {
                var extra_amount = data['result']['data']['extra_amount'];
                
                if(extra_amount>0)
                {
                    $("#extra_km_charges_add").html(extra_amount);
                    $('.extra-km-section-add').removeClass("hidden");
                    $('.extra-money-section-add').removeClass("hidden");
                }
                else
                {
                    $("#extra_km_charges_add").html(0);
                    $('.extra-km-section-add').addClass("hidden");
                }

            },
            error: function(data) {

                $('#editReturnModal').modal('hide');
                $('#deliveryfailureModal').modal('show');
                return 0;
            }
        });

        
        
    });
    
    
    //Start populate confirm add return modal
    $('.booking_confirm_return_btn').click(function(){
		
        var formEl = $('form[name="add-return"]');
        
        var e = document.getElementById("collection_mode_add");
        var collection_mode_add = e.options[e.selectedIndex].text;
        
        var accessories_received_add = $('input[name="accessories_received_add"]:checked').length > 0;
        var bike_received_add = $('input[name="bike_received_add"]:checked').length > 0;
        
        if(accessories_received_add)
        {
            accessories_received_add_str = "YES";
        }
        else
        {
            accessories_received_add_str = "NO";
        }
        if(bike_received_add)
        {
            bike_received_add_str = "YES";
        }
        else
        {
            bike_received_add_str = "NO";
        }
           
        
        var return_detail_label_add = $("#return_detail_label_add").html();
        
        $text = "<p>&nbsp&nbsp"+return_detail_label_add+"<br/>"+"&nbsp&nbsp"+"Start ODO : "+$("#return_start_odo_label_add").html()+"<br/>"+"&nbsp&nbsp"+"End ODO : "+formEl.find('input[name="end_odo_add"]').val()+"<br/>"+"&nbsp&nbsp"+"Bike Returned without Problems : "+bike_received_add_str+"<br/>"+"&nbsp&nbsp"+"Accessories Returned without Problems : "+accessories_received_add_str+"<br/>"+"&nbsp&nbsp"+"Problem Notes : "+formEl.find('textarea[name="return_problem_notes_add"]').val()+"<br/>"+"&nbsp&nbsp"+"Extra Money : "+formEl.find('input[name="extra_money_add"]').val()+"<br/>"+"&nbsp&nbsp"+"Collection Mode : "+collection_mode_add+"<br/>"+"&nbsp&nbsp"+"Notes : "+formEl.find('textarea[name="return_notes_add"]').val()+"</p>";
        
        
        $('#confirmModalContent').html($text);
        
        $('#confirmHeading').html("Confirm Return - "+formEl.find('input[name="booking_ref_add"]').val()+" - "+formEl.find('input[name="booking_customer_add"]').val());
                            
        
		$('#confirmReturnModal').modal('show');
        
	});
    //End populate confirm add return 

    
    //Submit add return form
    $('.booking_submit_return_btn').click(function(){
        
        $('#add-return').submit();
    });
    
    
    //Close add return
    $('.booking_close_return_btn').click(function(){
        
        $('#addReturnModal').modal('hide');
        $('#confirmReturnModal').modal('hide');
    });
    
    
    
    //Submit cancel return from add return 
    $('.booking_submit_cancel_return_btn').click(function(){
        
        $('#cancel-return').submit();
    });
    
    
    
    
}.bind(Return);








