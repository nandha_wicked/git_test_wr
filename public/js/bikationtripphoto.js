var TripPhoto = {
	BikationTripPhoto:{},
	PromoCode: {}
};

TripPhoto.BikationTripPhoto.init = function(){
	TripPhoto.BikationTripPhoto.editBikeModel();
	TripPhoto.BikationTripPhoto.editSpecBikeModel();
}.bind(TripPhoto.BikationTripPhoto);
TripPhoto.BikationTripPhoto.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Media_Select = $(this).data('mediaselect');
		var Media_URL = $(this).data('mediaurl');
		var Media_Size = $(this).data('mediasize');
		var created_on = $(this).data('createdon');
		var updated_on = $(this).data('updatedon');
		var Created_By = $(this).data('Createdby');
		var Updated_by = $(this).data('updatedby');
		var Status = $(this).data('status');	
		$('#editBikeModel-modal form').attr("action","/bikation-trip-photo/edit/"+id);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		$('#editBikeModel-modal form input[name=Media_Size]').val(Media_Size);
		$('#editBikeModel-modal form input[name=created_on]').val(created_on);
		$('#editBikeModel-modal form input[name=updated_on]').val(updated_on);
		$('#editBikeModel-modal form input[name=Created_By]').val(Created_By);
		$('#editBikeModel-modal form input[name=Updated_by]').val(Updated_by);

		if(Media_Select == "Image"){
			$("#bikation-trip-photo-edit #media_image").removeClass("hide"); 
			$("#bikation-trip-photo-edit #media_image").addClass("show");
			$("#bikation-trip-photo-edit #media_video").removeClass("show"); 
			$("#bikation-trip-photo-edit #media_video").addClass("hide"); 
			$('#editBikeModel-modal form #Media_Select1').prop("checked",true); 
			var coverImgHolder = '<img src="'+Media_URL+'" width="50px" />';
			$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);

		}else{
			$("#bikation-trip-photo-edit #media_video").removeClass("hide"); 
			$("#bikation-trip-photo-edit #media_video").addClass("show");
			$("#bikation-trip-photo-edit #media_image").removeClass("show"); 
			$("#bikation-trip-photo-edit #media_image").addClass("hide"); 
			$('#editBikeModel-modal form #Media_Select2').prop("checked",true);
			$('#editBikeModel-modal form input:text[name=Media_URL]').val(Media_URL);
		}
		if(Status == '1'){
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		}else{
			$('#editBikeModel-modal form #bmose2').prop("checked",true);
		}
		
		$('#editBikeModel-modal').modal('show');
	});
}.bind(TripPhoto.BikationTripPhoto);


TripPhoto.BikationTripPhoto.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-trip-photo/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(TripPhoto.BikationTripPhoto);
