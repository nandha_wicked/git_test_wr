var PeopleOnTrip_Admin = {
	BikationPeopleOnTrip_Admin:{},
	PromoCode: {}
	
};

PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.init = function(){
	PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.editBikeModel();
	PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.editSpecBikeModel();
}.bind(PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin);

PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Book_ID = $(this).data('bookid');
		var Name_of_Co_Px = $(this).data('nameofcopx');
		var Contact_No = $(this).data('contactno');
		var Email_ID = $(this).data('emailid');
		var Sex = $(this).data('sex');
		var DoB = $(this).data('dob');
		var Rider = $(this).data('rider');
		var DL = $(this).data('dl');
		var Cancelled_On = $(this).data('cancelledon');
		var Cancelled_By = $(this).data('cancelledby');
		var Status = $(this).data('status');

		$('#editBikeModel-modal form').attr("action","/admin/bikation-people_on_trip/edit/"+id);
		$('#editBikeModel-modal form input[type=text]').val(Book_ID);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		$('#editBikeModel-modal form input[name=Name_of_Co_Px]').val(Name_of_Co_Px);
		$('#editBikeModel-modal form input[name=Contact_No]').val(Contact_No);
		$('#editBikeModel-modal form input[name=Email_ID]').val(Email_ID);
		$('#editBikeModel-modal form input[name=DoB]').val(DoB);
		$('#editBikeModel-modal form input[name=Rider]').val(Rider);
		$('#editBikeModel-modal form input[name=DL]').val(DL);
		$('#editBikeModel-modal form input[name=Cancelled_On]').val(Cancelled_On);
		$('#editBikeModel-modal form input[name=Cancelled_By]').val(Cancelled_By);
		if(Sex == '1')
			$('#editBikeModel-modal form #Sex1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #Sex2').prop("checked",true);
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin);

PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/admin/bikation-people_on_trip/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(PeopleOnTrip_Admin.BikationPeopleOnTrip_Admin);

