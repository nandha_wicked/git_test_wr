
var BikeModel = {
	
};

BikeModel.init = function(){
	BikeModel.editBikeModel();
	BikeModel.editSpecBikeModel();
	BikeModel.getGalleryImages();
}.bind(BikeModel);

BikeModel.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var bikeMakeId = $(this).data('bikemakeid');
		var bikeModel = $(this).data('bikemodel');
		var modelDesc = $(this).data('description');
		var status = $(this).data('status');
        var priority = $(this).data('priority');
		var coverImg = $(this).data('cover-img');
		var thumbImg = $(this).data('thumbnail-img');
		var coverImgHolder = '<img src="'+coverImg+'" width="50px" />';
		var thumbImgHolder = '<img src="'+thumbImg+'" width="50px" />';
				
		$('#editBikeModel-modal form').attr("action","/admin/bikeModel/edit/"+id);
		$('#editBikeModel-modal form select option[value='+bikeMakeId+']').prop('selected',true);
		$('#editBikeModel-modal form input[type=text]').val(bikeModel);
        $('#editBikeModel-modal form input[name="priority"]').val(priority);
		$('#editBikeModel-modal form textarea[name="modelDesc"]').val(modelDesc);
		if(status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);

		$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);
		$('#editBikeModel-modal form #old_thumbnailImage_holder').html(thumbImgHolder);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(BikeModel);

BikeModel.editSpecBikeModel = function(){
	$('.bikeModel_spec_btn').click(function(){
		var id = $(this).data('id');
		var spec1Key = $(this).data('spec1key');
		var spec2Key = $(this).data('spec2key');
		var spec3Key = $(this).data('spec3key');
		var spec4Key = $(this).data('spec4key');
		var spec1Value = $(this).data('spec1value');
		var spec2Value = $(this).data('spec2value');
		var spec3Value = $(this).data('spec3value');
		var spec4Value = $(this).data('spec4value');
		$('#specBikeModel-modal form').attr("action","/admin/bikeModel/spec/"+id);
		$('#specBikeModel-modal form input[name=spec1Key]').val(spec1Key);
		$('#specBikeModel-modal form input[name=spec2Key]').val(spec2Key);
		$('#specBikeModel-modal form input[name=spec3Key]').val(spec3Key);
		$('#specBikeModel-modal form input[name=spec4Key]').val(spec4Key);
		$('#specBikeModel-modal form textarea[name=spec1Value]').val(spec1Value);
		$('#specBikeModel-modal form textarea[name=spec2Value]').val(spec2Value);
		$('#specBikeModel-modal form textarea[name=spec3Value]').val(spec3Value);
		$('#specBikeModel-modal form textarea[name=spec4Value]').val(spec4Value);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(BikeModel);

BikeModel.getGalleryImages = function(){
	$('.view-model-gallery').click(function(){
		var modelId = $(this).data('model-id');
		$.ajax({
			url : '/api/models/gallery-images/' + modelId,
			type : 'get',
			success: function(res){
				var result = res['result']['data'];
				var htmlArr = new Array();
				if(typeof result == 'undefined'){
					var img = '<div class="item active"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgdmlld0JveD0iMCAwIDkwMCA1MDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzkwMHg1MDAvYXV0by8jNzc3OiM1NTUvdGV4dDpGaXJzdCBzbGlkZQpDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48IVtDREFUQVsjaG9sZGVyXzE1MGIyYWEyNmI4IHRleHQgeyBmaWxsOiM1NTU7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6NDVwdCB9IF1dPjwvc3R5bGU+PC9kZWZzPjxnIGlkPSJob2xkZXJfMTUwYjJhYTI2YjgiPjxyZWN0IHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIiBmaWxsPSIjNzc3Ii8+PGc+PHRleHQgeD0iMzA4LjI4OTA2MjUiIHk9IjI3MC4xIj5GaXJzdCBzbGlkZTwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" alt="..."></div>';
					$('#view-gallery-modal .carousel-inner').html(img);
					$('#view-gallery-modal').modal('show');
					$('#view-gallery-modal').on('hidden.bs.modal', function (e) {
						$('#loader').removeClass('show');
					});
					return false;
				}
					
				var activeImage = '<div class="item active"><img src="http:'+result[0]+'"></div>';
				htmlArr.push(activeImage);

				// to avoid the first element to repeat
				result.shift();

				$.each(result, function( index, val ) {
				  var img = '<div class="item"><img src="http:'+val+'"></div>';
				  htmlArr.push(img);
				});

				$('#view-gallery-modal .carousel-inner').html(htmlArr);
				$('#view-gallery-modal').modal('show');
			},
			beforeSend: function(){
				$('#loader').addClass('show');
			},
			complete: function(){
				$('#loader').removeClass('show');
			}
		});
	});
}.bind(BikeModel);

$('#all-users').DataTable(
    {
        autoWidth: false, //step 1
        "scrollX": true, //step 1
           columnDefs: [
              { width: '100px', targets: 0 }, //step 2, column 1 out of 4
              { width: '300px', targets: 1 }, //step 2, column 2 out of 4
              { width: '300px', targets: 2 }, //step 2, column 2 out of 4
              { width: '100px', targets: 3 }, //step 2, column 2 out of 4
              { width: '30px', targets: 4 }, //step 2, column 2 out of 4
              { width: '30px', targets: 5 }, //step 2, column 2 out of 4
              { width: '300px', targets: 6 }, //step 2, column 2 out of 4
              { width: '100px', targets: 7 }, //step 2, column 2 out of 4
              { width: '300px', targets: 8 }, //step 2, column 2 out of 4
              { width: '80px', targets: 9 }, //step 2, column 2 out of 4
              { width: '30px', targets: 10 }, //step 2, column 2 out of 4
              { width: '30px', targets: 11 }, //step 2, column 2 out of 4
              { width: '300px', targets: 12 }  //step 2, column 3 out of 4
           ]
    });

$('#all-enquiries').DataTable(
    {
        autoWidth: false,
        scrollY:  '70vh',
        "order": [[ 0, "desc" ]],
        scrollCollapse: true,//step 1
        "scrollX": true, //step 1
           columnDefs: [
              { width: '10px', targets: 0 }, //step 2, column 1 out of 4
              { width: '100px', targets: 1 }, //step 2, column 2 out of 4
              { width: '100px', targets: 2 }, //step 2, column 2 out of 4
              { width: '100px', targets: 3 }, //step 2, column 2 out of 4
              { width: '300px', targets: 4 }, //step 2, column 2 out of 4
              { width: '30px', targets: 5 }, //step 2, column 2 out of 4
              { width: '300px', targets: 6 }, //step 2, column 2 out of 4
              { width: '100px', targets: 7 }, //step 2, column 2 out of 4
              { width: '50px', targets: 8 }, //step 2, column 2 out of 4
              { width: '70px', targets: 9 } 
              
           ]
    });


$('#all-promocodes').DataTable(
    {
        autoWidth: false,
        scrollY:  '70vh',
        "order": [[ 0, "desc" ]],
        scrollCollapse: true,//step 1
        "scrollX": true, //step 1
           columnDefs: [
              { width: '80px', targets: 0 }, //step 2, column 1 out of 4
              { width: '80px', targets: 1 }, //step 2, column 2 out of 4
              { width: '100px', targets: 2 }, //step 2, column 2 out of 4
              { width: '200px', targets: 3 }, //step 2, column 2 out of 4
              { width: '60px', targets: 4 }, //step 2, column 2 out of 4
              { width: '60px', targets: 5 }, //step 2, column 2 out of 4
              { width: '40px', targets: 6 }, //step 2, column 2 out of 4
              { width: '80px', targets: 7 }, //step 2, column 2 out of 4
              { width: '80px', targets: 8 }, //step 2, column 2 out of 4
              { width: '80px', targets: 9 },
              { width: '40px', targets: 10 },
               { width: '40px', targets: 11 },
               { width: '350px', targets: 12 },
               { width: '350px', targets: 13 },
               { width: '70px', targets: 14 },
               { width: '70px', targets: 15 },
               { width: '70px', targets: 16 },
               { width: '70px', targets: 17 },
               { width: '70px', targets: 18 },
               { width: '70px', targets: 19 },
               { width: '70px', targets: 20 },
               { width: '70px', targets: 21 },
               
              
           ]
    });

$('#sort-table').DataTable(
    {
        scrollY:  '70vh',
        "order": [[ 0, "desc" ]],
        scrollCollapse: true,//step 1
        "scrollX": true //step 1
    });

$('#all-serviceblocks').DataTable(
    {
        autoWidth: false,
        scrollY:  '70vh',
        "order": [[ 0, "desc" ]],
        scrollCollapse: true,//step 1
        "scrollX": true, //step 1
           columnDefs: [
              { width: '10px', targets: 0 }, //step 2, column 1 out of 4
              { width: '100px', targets: 1 }, //step 2, column 2 out of 4
              { width: '100px', targets: 2 }, //step 2, column 2 out of 4
              { width: '100px', targets: 3 }, //step 2, column 2 out of 4
              { width: '300px', targets: 4 }, //step 2, column 2 out of 4
              { width: '30px', targets: 5 }, //step 2, column 2 out of 4
              { width: '300px', targets: 6 }, //step 2, column 2 out of 4
              { width: '100px', targets: 7 }, //step 2, column 2 out of 4
              { width: '50px', targets: 8 }, //step 2, column 2 out of 4
              { width: '70px', targets: 9 } 
              
           ]
    });


    function htmlbodyHeightUpdate(){
		var height3 = $( window ).height()
		var height1 = $('.nav').height()+50
		height2 = $('.main').height()
		if(height2 > height3){
			$('html').height(Math.max(height1,height3,height2)+10);
			$('body').height(Math.max(height1,height3,height2)+10);
		}
		else
		{
			$('html').height(Math.max(height1,height3,height2));
			$('body').height(Math.max(height1,height3,height2));
		}
		
	}
	$(document).ready(function () {
		htmlbodyHeightUpdate()
		$( window ).resize(function() {
			htmlbodyHeightUpdate()
		});
		$( window ).scroll(function() {
			height2 = $('.main').height()
  			htmlbodyHeightUpdate()
		});
	});
