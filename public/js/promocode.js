var PromoCode = {
	Add:{},
	Edit:{}
};

var options = [];

PromoCode.init = function(){
	PromoCode.initHandlers();
	PromoCode.editPromoCodeModel();
	PromoCode.Add.initFromDateTimePicker();
	PromoCode.Add.initToDateTimePicker();
	PromoCode.Edit.initFromDateTimePicker();
	PromoCode.Edit.initToDateTimePicker();
	PromoCode.chooseRangeOrSpecificDates();
	PromoCode.updateAvailableCount();
}.bind(PromoCode);

PromoCode.initHandlers = function(){
	// SELECT ALL MODELS CHECKBOX
	$('.modal').on('hidden.bs.modal', function(e){ 
		$("form", this).get(0).reset();
    }) ;
    $('#addPromoCodeModal').on('shown.bs.modal', function(e){ 
		PromoCode.chooseRangeOrSpecificDates.perform();
    }) ;
	$( '.dropdown-menu a' ).on( 'click', function( event ) {
		var $target = $( event.currentTarget ),
			val = $target.attr( 'data-value' ),
			$inp = $target.find( 'input' ),
			idx;


		if ( ( idx = options.indexOf( val ) ) > -1 ) {
			options.splice( idx, 1 );
			setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
		} else {
			options.push( val );
			setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
		}
		
		if($(this).hasClass('make-name')){
			var status = $(this).find('input[type=checkbox][name=make-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.model-ids').prop("checked", status);
		}
		if($(this).hasClass('city-name')){
			var status = $(this).find('input[type=checkbox][name=city-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.area-ids').prop("checked", status);
		}
		
		$( event.target ).blur();

		return false;
	});

}.bind(PromoCode);


PromoCode.editPromoCodeModel = function(){
	$('.promocode_edit_btn').click(function(){
		// EMPTY MODEL AND AREA CHECKBOX, VALUES WERE GETTING RETAINED FROM PREVIOUS OPENED MODAL
		PromoCode.chooseRangeOrSpecificDates.perform();
		var id = $(this).data('id');
		var code = $(this).data('code');
		var title = $(this).data('title');
		var description = $(this).data('description');
		var valueType = $(this).data('value-type');
		var value = $(this).data('value');
		var totCount = $(this).data('total-count');
		var avlCount = $(this).data('available-count');
		var minOrder = $(this).data('min-order');
		var newUser = $(this).data('new-user');
		var modelIds = $(this).data('model-ids');
		var areaIds = $(this).data('area-ids');
		var weekday = $(this).data('weekday');
		var weekend = $(this).data('weekend');
		var dates = $(this).data('dates');
		var device = $(this).data('device');
		var status = $(this).data('status');
		var startSet = $(this).data('start');
		var endSet = $(this).data('end');
		var startDateObj = new Date(startSet);
		var endDateObj = new Date(endSet);

		var formEl = $('form[name="edit-promocode"]');

		formEl.attr("action","/admin/promocode/"+id);
		formEl.find('input[name=code]').val(code);
		formEl.find('input[name=title]').val(title);
		formEl.find('textarea[name=description]').val(description);
		
		if(dates == ''){
			formEl.find('input[name="range-or-specific"][value="1"]').prop('checked', true);
            formEl.find('.dates-range', '.modal.in').show();
            formEl.find('.specific-date', '.modal.in').hide();
            formEl.find('input[name=start_date]').val(startDateObj.dateFormat('Y-m-d'));
            formEl.find('input[name=start_time]').val(startDateObj.dateFormat('H:i'));
            formEl.find('input[name=end_date]').val(endDateObj.dateFormat('Y-m-d'));
            formEl.find('input[name=end_time]').val(endDateObj.dateFormat('H:i'));

            if(weekday == '1')
				formEl.find('input[name="weekday"][value="1"]').prop("checked",true);	
			else
				formEl.find('input[name="weekday"][value="0"]').prop("checked",true);	

			if(weekend == '1')
				formEl.find('input[name="weekend"][value="1"]').prop("checked",true);	
			else
				formEl.find('input[name="weekend"][value="0"]').prop("checked",true);	
		}
		else{
			formEl.find('.dates-range', '.modal.in').hide();
			formEl.find('input[name="range-or-specific"][value="0"]').prop('checked', true);
			formEl.find('.specific-date', '.modal.in').show();
			formEl.find('input[name="dates"]').val(dates);
		}

		formEl.find('input[name="total_count"]').val(totCount);
		formEl.find('input[name="available_count"]').val(avlCount);
		formEl.find('select[name="value_type"] option[value='+valueType+']').attr('selected', 'selected');
		formEl.find('input[name="value"]').val(value);
		formEl.find('input[name="min_order"]').val(minOrder);

		if(newUser == '1')
			formEl.find('input[name="new_user"][value="1"]').prop("checked",true);	
		else
			formEl.find('input[name="new_user"][value="0"]').prop("checked",true);	

		if (modelIds !== null) {
			modelIds = String(modelIds);
			var modelIdsArr = modelIds.split(',');
		}
		
		if (areaIds !== null) {
			areaIds = String(areaIds);
			var areaIdsArr = areaIds.split(',');
		}

		$.each(modelIdsArr, function( index, value ) {
			var modelEl = formEl.find('input[name="model_ids[]"][value="'+value+'"]');
			modelEl.prop("checked", true);
			if (modelEl.is(':checked')){
				modelEl.closest('ul').siblings().find('input[name=make-name]').prop('checked', true);
			}
		});
		
		$.each(areaIdsArr, function( index, value ) {
			var areaEl = formEl.find('input[name="area_ids[]"][value="'+value+'"]');
			areaEl.prop("checked", true);
			if (areaEl.is(':checked')) {
				areaEl.closest('ul').siblings().find('input[name=city-name]').prop('checked', true);
			}
		});

		formEl.find('select[name="device"] option[value='+device+']').attr('selected', 'selected');

		if(status == '1')
			formEl.find('input[name="status"][value="1"]').prop("checked",true);	
		else
			formEl.find('input[name="status"][value="0"]').prop("checked",true);	

		$('#editPromoCodeModal').modal('show');
	});
}.bind(PromoCode);

PromoCode.Add.initFromDateTimePicker = function(el){
	$('#addPcStartDate').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#addPcStartDate');
			var fromTimeEl = $('#addPcStartTime');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#addPcEndDate');
			var toTimeEl = $('#addPcEndTime');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/working-hours',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilter.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#addPcStartTime').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							PromoCode.Add.initToDateTimePicker();
							$('#addPcEndDate').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
}.bind(PromoCode.Add);

PromoCode.Add.initToDateTimePicker = function(el){
	var fromDateEl = $('#addPcStartDate');
	var fromDate = fromDateEl.val();
	var fromDateObj = new Date(fromDate);
	var minDate = fromDateObj.dateFormat('Y/m/d');

	$('#addPcEndDate').datetimepicker({
		timepicker: false,
		scrollInput: false,
		minDate: minDate,
		format:'Y-m-d',
		onSelectDate: function(selectedDateTime, $el){
			var toTimeEl = $('#addPcEndTime');
			toTimeEl.val('');
			$.ajax({
				url : '/api/working-hours',
				type : 'get',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var fromDateEl = $('#addPcStartDate');
					var fromTimeEl = $('#addPcStartTime');
					var fromDate = fromDateEl.val();
					var fromTime = fromTimeEl.val();
					var fromDateTime = fromDate + ' ' + fromTime;
					var fromDateTimeObj = new Date(fromDateTime);

					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilter.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
					if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
						var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
						nextHour = new Date(nextHour);
						startTime = nextHour.dateFormat('H:i');
					}
					
					$('#addPcEndTime').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							PromoCode.Add.validationCheck();
						}
					}).datetimepicker('show').focus();
				},
			});
		}
	});
}.bind(PromoCode.Add);

PromoCode.Edit.initFromDateTimePicker = function(el){
	$('#startDate').datetimepicker({
		timepicker: false,
		scrollInput: false,
		format:'Y-m-d',
		minDate: 0,
		onSelectDate: function(selectedDateTime, $el){
			var fromDateEl = $('#startDate');
			var fromTimeEl = $('#startTime');
			var fromDate = fromDateEl.val();
			var fromTime = fromTimeEl.val();
			var toDateEl = $('#endDate');
			var toTimeEl = $('#endTime');
			
			// CLEAR OTHER DATE-TIME SELECTIONS
			fromTimeEl.val('');
			toDateEl.val('');
			toTimeEl.val('');

			// GET WORKING SLOTS FOR THE SELECTED DATE
			$.ajax({
				url : '/api/working-hours',
				type : 'GET',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var now = new Date();
					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilter.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
					if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
						startTime = now.dateFormat('H:i');
					}
					
					$('#startTime').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							PromoCode.Edit.initToDateTimePicker();
							$('#endDate').datetimepicker('show').focus();
						}
					}).datetimepicker('show').focus();
				}
			});
		}
	});
}.bind(PromoCode.Edit);

PromoCode.Edit.initToDateTimePicker = function(el){
	var fromDateEl = $('#startDate');
	var fromDate = fromDateEl.val();
	var fromDateObj = new Date(fromDate);
	var minDate = fromDateObj.dateFormat('Y/m/d');

	$('#endDate').datetimepicker({
		timepicker: false,
		scrollInput: false,
		minDate: minDate,
		format:'Y-m-d',
		onSelectDate: function(selectedDateTime, $el){
			var toTimeEl = $('#endTime');
			toTimeEl.val('');
			$.ajax({
				url : '/api/working-hours',
				type : 'get',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d')
				},
				success: function(data){
					var fromDateEl = $('#startDate');
					var fromTimeEl = $('#startTime');
					var fromDate = fromDateEl.val();
					var fromTime = fromTimeEl.val();
					var fromDateTime = fromDate + ' ' + fromTime;
					var fromDateTimeObj = new Date(fromDateTime);

					var slots = data['result']['data']['slots'];
					var allowedTimes = Backend.BikeFilter.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
					if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
						var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
						nextHour = new Date(nextHour);
						startTime = nextHour.dateFormat('H:i');
					}
					
					$('#endTime').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes,
						onSelectTime: function( dt, $el ){
							PromoCode.Edit.validationCheck();
						}
					}).datetimepicker('show').focus();
				},
			});
		}
	});
}.bind(PromoCode.Edit);

PromoCode.Add.validationCheck = function(){
	var startDate = $('#addPcStartDate').val();
	var startTime = $('#addPcStartTime').val();
	var endDate = $('#addPcEndDate').val();
	var endTime = $('#addPcEndTime').val();
	var dateErrorEl = $('#date-error-add');
	PromoCode.validationCheck(startDate, startTime, endDate, endTime, dateErrorEl);
}.bind(PromoCode.Add);

PromoCode.Edit.validationCheck = function(){
	var startDate = $('input[name="start_date"]').val();
	var startTime = $('input[name="start_time"]').val();
	var endDate = $('input[name="end_date"]').val();
	var endTime = $('input[name="end_time"]').val();
	var dateErrorEl = $('#date-error-edit');
	PromoCode.validationCheck(startDate, startTime, endDate, endTime, dateErrorEl);
}.bind(PromoCode.Edit);

PromoCode.validationCheck = function(startDate, startTime, endDate, endTime, dateErrorEl){
	if(!startDate || !endDate || !startTime || !endTime){
	    $('#date-error').text("ERROR : Please Enter Both Date & Time");
	}
	if( startDate == endDate){
		if(startTime == endTime){
			dateErrorEl.text("ERROR : Please Choose Some Other Time");
		}
		else if(startTime > endTime){
			dateErrorEl.text("ERROR : Start Time Greater Than End Time");
		}
		else
			dateErrorEl.text('');
	}
	if(startDate > endDate){
		dateErrorEl.text("ERROR : Start Date Greater Than End Date");
	}
	dateErrorEl.text('');
}.bind(PromoCode);

PromoCode.chooseRangeOrSpecificDates = function(){
	$('input[type=radio][name=range-or-specific]').change(PromoCode.chooseRangeOrSpecificDates.perform);
}

PromoCode.chooseRangeOrSpecificDates.perform = function(){
	$('.dates-range', '.modal.in').hide();
	$('.specific-date', '.modal.in').hide();
	$('input[name=start_date]', '.modal.in').val('');
	$('input[name=end_date]', '.modal.in').val('');
	$('input[name=start_time]', '.modal.in').val('');
	$('input[name=end_date]', '.modal.in').val('');
	$('input[name=weekend]', '.modal.in').prop('checked', false);
	$('input[name=weekday]', '.modal.in').prop('checked', false);
	
	$('input[name=dates]', '.modal.in').val('');
	if ($("[name=range-or-specific]:checked", '.modal.in').val() == 1) {
       $('.dates-range', '.modal.in').show();
    }
    else {
       $('.specific-date', '.modal.in').show();
    }
};

PromoCode.updateAvailableCount = function(){
	$('#addPromoCodeModal input[name="total_count"]').keyup(function(){
	    $('#addPromoCodeModal input[name="available_count"]').val($('#addPromoCodeModal input[name="total_count"]').val());
	});
}
