Booking.ChooseDate = {};

Booking.ChooseDate.init = function(){
	Booking.ChooseDate.initEventHandlers();
	Booking.ChooseDate.initFromDateTimePicker();
}.bind(Booking.ChooseDate);

Booking.ChooseDate.initEventHandlers = function(){
	// ON SUBMIT
	$('#booking-step1-continue').click(function(){
		var fromDate = $('#fromdatepicker').val();
		var fromTime = $('#fromtimepicker').val();
		var toDate = $('#todatepicker').val();
		var toTime = $('#totimepicker').val();
		var errEl = $('#step1-error');
		
		if(!fromDate || !toDate || !fromTime || !toTime){
			errEl.text("ERROR : Please Enter Both Date & Time");
			return false;
		}
		var fromDateTime = fromDate + ' ' + fromTime;
		var toDateTime = toDate + ' ' + toTime;
		fromDateTime = new Date(fromDateTime).getTime();
		toDateTime = new Date(toDateTime).getTime();

		if(fromDateTime >= toDateTime){
			errEl.text("ERROR : Start Date cannot be greater than End Date");
			return false;
		}
		var now = new Date().getTime();
		if(fromDateTime <= now || toDateTime <= now){
			errEl.text("ERROR : Selected time has already passed!");
			return false;
		}

		errEl.text("");
		$('#selectDateForm').submit();
		return false;
	});
}.bind(Booking.ChooseDate);

Booking.ChooseDate.formatSlots = function(slots){
	var allowedTimes = [];
    if (slots.length>0)
    {
	   $.each(slots, function(i,slot){
		  var slotStartStr = slot['start_time'].substring( 0, slot['start_time'].lastIndexOf( ":00" ) );
		  allowedTimes.push(slotStartStr);
	   });
    }
	return allowedTimes;
}.bind(Booking.ChooseDate);

Booking.ChooseDate.initFromDateTimePicker = function(){
    var city_id = $('input[name="city_id"]').val();
    $.ajax({
				url : '/api/day-offs',
				type : 'GET',
				data : { 
                    'city_id': city_id
				},
				success: function(data1){
                    
                    var data1arr = data1['result']['data'];
                    for (var i=0; i<data1arr.length; i++)
                    {
                        data1arr[i] = parseInt(data1arr[i], 10);
                    }
            
                    var d = new Date();
                    var dateStr = d.toISOString().slice(0,10);
                    $.ajax({
                        url : '/api/from-working-hours',
                        type : 'GET',
                        data : { 
                            'date': dateStr,
                            'city_id': city_id
                        },
                        success: function(data){
                            slots = data['result']['data']['slots'];
                            if(slots.count > 0)
                            {
                                lastSlot = slots.slice(-1).pop();
                                lastHour = lastSlot['id'];
                            }
                            else
                            {
                                lastHour = 22;
                            }
                            
                            
                            var d2 = new Date();
                            var timeStr = d2.getHours();
                            if (lastHour-1<=timeStr)
                                {
                                    minDate =new Date(new Date().getTime()+(24*60*60*1000));
                                }
                            else{
                                minDate =0;
                            }
                            
                            $('#fromdatepicker').datetimepicker({
                                timepicker: false,
                                scrollInput: false,
                                format:'d M Y',
                                minDate: minDate,
                                disabledWeekDays: data1arr,
                                onSelectDate: function(selectedDateTime, $el){
                                    var fromDateEl = $('#fromdatepicker');
                                    var fromTimeEl = $('#fromtimepicker');
                                    var fromDate = fromDateEl.val();
                                    var fromTime = fromTimeEl.val();
                                    var toDateEl = $('#todatepicker');
                                    var toTimeEl = $('#totimepicker');
                                    var city_id = $('input[name="city_id"]').val();

                                    // CLEAR OTHER DATE-TIME SELECTIONS
                                    fromTimeEl.val('');
                                    toDateEl.val('');
                                    toTimeEl.val('');

                                    // GET WORKING SLOTS FOR THE SELECTED DATE
                                    $.ajax({
                                        url : '/api/from-working-hours',
                                        type : 'GET',
                                        data : { 
                                            'date': selectedDateTime.dateFormat('Y-m-d'),
                                            'city_id': city_id
                                        },
                                        success: function(data){
                                            var now = new Date();
                                            var slots = data['result']['data']['slots'];
                                            var allowedTimes = Booking.ChooseDate.formatSlots(slots);
                                            if(allowedTimes.length>0)
                                                {
                                                    var startTime = allowedTimes[0];

                                                    // CHECK IF CHOSEN DATE IS TODAY, SET START-TIME TO CURRENT-TIME 
                                                    if(selectedDateTime.dateFormat('Y-m-d') == now.dateFormat('Y-m-d')){
                                                        startTime = now.dateFormat('H:i');
                                                    }

                                                    $('#fromtimepicker').datetimepicker({
                                                        datepicker: false,
                                                        scrollInput: false,
                                                        format: 'H:i',
                                                        defaultTime: startTime,
                                                        minTime: startTime,
                                                        allowTimes: allowedTimes,
                                                        onSelectTime: function( dt, $el ){
                                                            Booking.ChooseDate.initToDateTimePicker();
                                                            $('#todatepicker').datetimepicker('show').focus();
                                                        }
                                                    }).datetimepicker('show').focus();
                                                }
                                            else
                                                {
                                                 $('#fromtimepicker').datetimepicker({
                                                        datepicker: false,
                                                        scrollInput: false,
                                                        format: 'H:i',
                                                        disableTimeRanges: ['9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00'],
                                                        onSelectTime: function( dt, $el ){
                                                            Booking.ChooseDate.initToDateTimePicker();
                                                            $('#todatepicker').datetimepicker('show').focus();
                                                        }
                                                    }).datetimepicker('show').focus();   
                                                }
                                        }
                                    });
                                  }
                                });
                            
                        }
                        
                    });
                            

      

    
            
    }
    });
}.bind(Booking.ChooseDate);

Booking.ChooseDate.initToDateTimePicker = function(){    
    var city_id = $('input[name="city_id"]').val();
    $.ajax({
				url : '/api/day-offs',
				type : 'GET',
				data : { 
                    'city_id': city_id
				},
				success: function(data1){
                    
                    var data1arr = data1['result']['data'];
                    for (var i=0; i<data1arr.length; i++)
                    {
                        data1arr[i] = parseInt(data1arr[i], 10);
                    }
                
	var fromDateEl = $('#fromdatepicker');
	var fromDate = fromDateEl.val();
	var fromDateObj = new Date(fromDate);
	var minDate = fromDateObj.dateFormat('Y/m/d');
                                        
	$('#todatepicker').datetimepicker({
		timepicker: false,
		scrollInput: false,
		minDate: minDate,
		format:'d M Y',
		startDate: minDate,
        disabledWeekDays: data1arr,
		onSelectDate: function(selectedDateTime, $el){
			var toTimeEl = $('#totimepicker');
			toTimeEl.val('');
			$.ajax({
				url : '/api/to-working-hours',
				type : 'get',
				data : { 
					'date': selectedDateTime.dateFormat('Y-m-d'),
                    'city_id':city_id
				},
				success: function(data){
                                        
					var fromDateEl = $('#fromdatepicker');
					var fromTimeEl = $('#fromtimepicker');
					var fromDate = fromDateEl.val();
					var fromTime = fromTimeEl.val();
					var fromDateTime = fromDate + ' ' + fromTime;
					var fromDateTimeObj = new Date(fromDateTime);

					var slots = data['result']['data']['slots'];
					var allowedTimes = Booking.ChooseDate.formatSlots(slots);
					var startTime = allowedTimes[0];
					
					// CHECK IF CHOSEN DATE IS FROM-DATE -> SET START-TIME TO FROM-TIME + 1HR 
					if(selectedDateTime.dateFormat('Y-m-d') == fromDateTimeObj.dateFormat('Y-m-d')){
						var nextHour = fromDateTimeObj.getTime() + (60*60*1000);
						nextHour = new Date(nextHour);
						startTime = nextHour.dateFormat('H:i');
					}
					
					$('#totimepicker').datetimepicker({
						datepicker: false,
						scrollInput: false,
						format: 'H:i',
						defaultTime: startTime,
						minTime: startTime,
						allowTimes: allowedTimes
					}).datetimepicker('show').focus();
				},
			});
		}
	});
    $('#todatepicker').datetimepicker('show').focus();
    }
    });
}.bind(Booking.ChooseDate);
