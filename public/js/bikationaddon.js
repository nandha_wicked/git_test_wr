var Addon = {
	BikationAddon:{},
	PromoCode: {}
};

Addon.BikationAddon.init = function(){
	Addon.BikationAddon.editBikeModel();
	Addon.BikationAddon.editSpecBikeModel();
}.bind(Addon.BikationAddon);

Addon.BikationAddon.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Trip_ID = $(this).data('tripid');
		var Package_ID = $(this).data('packageid');
		var Activity_ID = $(this).data('activityid');
		var Exclusion_Inclusion = $(this).data('exclusioninclusion');
		var Std_Opt = $(this).data('stdopt');
		var Cost = $(this).data('cost');
		var Status = $(this).data('status');
		//alert(Package_ID);
		get_packages_edit(Trip_ID,Package_ID);
		$('#editBikeModel-modal form').attr("action","/bikation-addon/edit/"+id);
		$('#editBikeModel-modal form select option[value='+Trip_ID+']').prop('selected',true);
		$('#editBikeModel-modal form select option[value='+Package_ID+']').prop('selected',true);
		$('#editBikeModel-modal form input[name=Activity_ID]').val(Activity_ID);
		//$('#editBikeModel-modal form input[name=Std_Opt]').val(Std_Opt);
		$('#editBikeModel-modal form input[name=Cost]').val(Cost);
		if(Exclusion_Inclusion == '1')
			$('#editBikeModel-modal form #Exclusion_Inclusion1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #Exclusion_Inclusion2').prop("checked",true);
		
		if(Std_Opt == 'Standard')
			$('#editBikeModel-modal form #Standard').prop("checked",true);	
		else
			$('#editBikeModel-modal form #Optional').prop("checked",true);
		
		
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Addon.BikationAddon);


Addon.BikationAddon.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-addon/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Addon.BikationAddon);
