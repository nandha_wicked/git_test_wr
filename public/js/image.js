$(document).ready(function (e) {
	
	var _URL = window.URL || window.webkitURL;

$("#user_avtar").change(function(e) {
    

});


    $('.btn-upload').click(function() 
    {
        $('#user_avtar').trigger('click');
    });
    $("#user_avtar").on('change', function(e) 
    {
		var fsize = $('#user_avtar')[0].files[0].size;
		
		
		var file1, img;


    if ((file1 = this.files[0])) {
		
        img = new Image();
        img.onload = function() {
			
             if(this.width >= 1024)
			 {	document.getElementById('maxx_avtar').value = 1024;			 }
			 else
			 {document.getElementById('maxx_avtar').value = this.width;	 }
		  if(this.height >= 1024)
			 {	document.getElementById('maxy_avtar').value = 1024;			 }
			 else
			 {document.getElementById('maxy_avtar').value = this.height;	 }
			
        };
        
        img.src = _URL.createObjectURL(file1);


    }
	
		/* -----  */
		var photo_banner = document.getElementById("user_avtar");
		var file  = photo_banner.files[0];
		
		var preview = document.getElementById("preview_avtar");
		var r = new FileReader();
		r.onload = (function(previewImage) { 
			return function(e) { 
				var maxx_avtar = document.getElementById('maxx_avtar').value;
				var maxy_avtar = document.getElementById('maxy_avtar').value;
				previewImage.src = e.target.result; 
				var k = _resize_avatar(previewImage, maxx_avtar, maxy_avtar);
				if(k!=false) { 
				document.getElementById('base64_avtar').value= k;
				
				  var formData = new FormData($('#uploadForm')[0]);
					  $.ajax({
						url: "upload",
						type: "POST",
						contentType: false,
						cache: false,
						processData: false,
						data: formData,
						beforeSend: function() {
						$('#cancel_process').modal('show');
						  },
						success: function(data)
						  {
							  
							$('#cancel_process').modal('hide');  
							$("#targetLayer img.profilePic").attr('src',data.avatar_thumb);
							$("img#pop_profilePic").attr('src',data.avatar_thumb);
							$("#profile_blur").attr('style',  'background-image:url('+data.blur_image+')');
						  },
						  error: function() 
						  {
						  }           
					   });
				
				} else {
alert('problem - please attempt to upload again');
				}
			}; 
		})(preview);
		r.readAsDataURL(file);
		/*  ----  */
		if(fsize>5242880) 
		{
			alert('Please Upload Max 5MB file');
			return false
		}
    
    });
	
		$("#profilePic_upload").click(function(){
			$('#user_avtar').trigger('click');
		});
		
		
		$("#user_document").on('change', function(e) 
		{
/*			
		var fsize = $('#user_document')[0].files[0].size;
		
		if(fsize>5242880) 
		{
			alert('Please Upload Max 5MB file');
			return false
		}
      var formData = new FormData($('#upload_document_Form')[0]);
	  
      $.ajax({
        url: "upload_document",
        type: "POST",
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
		beforeSend: function() {
		$('#cancel_process').modal('show');
		  },
        success: function(data)
          {
			   $('#cancel_process').modal('hide'); 	
		      $("#upload_doc").html(data);
			  $("#user_document").remove();
			  $("#myModal_doc").modal('show');
				  
					 setTimeout(function() {
						$("#myModal_doc").modal('hide');
					}, 3000);
          },
          error: function() 
          {
          }           
       });*/
    });
	
		$("#upload_doc").click(function(){
			$('#user_document').trigger('click');
		});
		
		
		$(".bookingList li.cancel").click(function(){
			
			var Booking_ID = $(this).parent().parent().parent().parent().parent().find("#Booking_ID").val();
			var Razorpay_id = $(this).parent().parent().parent().parent().parent().find("#Razorpay_id").val();
			var token = $(this).parent().parent().parent().parent().parent().find("#tokenid").val();
			
		 BootstrapDialog.confirm({
				title: 'WARNING',
				message: 'Hi, Are you want to cancel your Booking ?',
				type: '', // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				closable: false, // <-- Default value is false
				draggable: false, // <-- Default value is false
				btnCancelLabel: 'No', // <-- Default value is 'Cancel',
				btnOKLabel: 'Yes', // <-- Default value is 'OK',
				btnOKClass: 'btn-info', // <-- If you didn't specify it, dialog type will be used,
				callback: function(result) {
					if(result) {
							//alert(Booking_ID);		
							//alert(Razorpay_id);
						
						  $.ajax({
							url: "cancel_booking_request",
							type: "post",
							beforeSend: function() {
								$('#cancel_process').modal('show');
							  },
							data : { 
                                        'Booking_ID':Booking_ID,
                                        'Razorpay_id':Razorpay_id,
                                        '_token': token
                                   },
							success: function(data)
							  {
								   $('#cancel_process').modal('hide');
								   $("#cancel_responce_msg").modal('show');
								 // $("#upload_doc").html(data);
								 // $("#user_document").remove();
							  }
							           
						   });
						
						//alert('Yup.');
					}
				}
			});
			
		});
		
		
		
	

function _resize_avatar(img, maxWidth, maxHeight) 
{
        var ratio = 1;

	var canvas_av = document.createElement("canvas");
	canvas_av.style.display="none";
	document.body.appendChild(canvas_av);

	var canvasCopy_av = document.createElement("canvas");
	canvasCopy_av.style.display="none";
	document.body.appendChild(canvasCopy_av);

	var ctx_av = canvas_av.getContext("2d");
	var copyContext_av = canvasCopy_av.getContext("2d");

        if(img.width > maxWidth)
                ratio = maxWidth / img.width;
        else if(img.height > maxHeight)
                ratio = maxHeight / img.height;

        canvasCopy_av.width = img.width;
        canvasCopy_av.height = img.height;
try {
        copyContext_av.drawImage(img, 0, 0);
} catch (e) { 
	//document.getElementById('loader').style.display="none";
//	alert("There was a problem - please reupload your image");
	return false;
}

        canvas_av.width = img.width * ratio;
        canvas_av.height = img.height * ratio;
        // the line to change
        //ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
        // the method signature you are using is for slicing
        ctx_av.drawImage(canvasCopy_av, 0, 0, canvas_av.width, canvas_av.height);

    	var dataURL = canvas_av.toDataURL("image/png");

	document.body.removeChild(canvas_av);
	document.body.removeChild(canvasCopy_av);
    	return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");


};	
		
  });
 
