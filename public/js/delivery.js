var Delivery = {
	Add:{},
	Edit:{}
};

var options = [];

Delivery.init = function(){
    Delivery.addDeliveryModel();
	Delivery.editDeliveryModel();
    //Delivery.Add.validationCheck();
    //Delivery.Edit.validationCheck();
}.bind(Delivery);




Delivery.editDeliveryModel = function(){
    
    //Start open edit form and fill details from existing delivery
	$('.booking_deliver_edit_btn').click(function(){
        
        var id = $(this).data('id');
        var booking_id = $(this).data('booking_id');
        var booking_customer = $(this).data('booking_customer_edit');
        
        
        
		var formEl = $('form[name="edit-delivery"]');

		formEl.attr("action","/erp/deliver/edit/"+id);
        
        var formCancelEl = $('form[name="cancel-delivery"]');
		formCancelEl.attr("action","/erp/cancel-delivery/"+id);
        
        formEl.find('input[name="booking_ref_edit"]').val(booking_id);
        formEl.find('input[name="booking_customer_edit"]').val(booking_customer);
        
        $('#editModalHeading').html("Edit Delivery - "+booking_id);
		
        
        $.ajax({
                        type: "POST",
                        url: "/api/getBookingDetailsFromId",
                        data: { 
						'booking_id': id
					       },
                        cache: false,
                        success: function(data) {
                            var booking = data['result']['data']['booking'];
                            
                            
                            $("#delivered_at_edit_div").html("<select name=\"delivered_at_edit\" id=\"delivered_at_edit\" class=\"form-control\"><option selected disabled value = \""+booking.delivered_at_area+"\""+">"+booking.delivered_at_area_name+"</option></select>");
        
                            $("#delivered_model_id_edit_div").html("<select name=\"delivered_model_id_edit\" id=\"modelDropEdit\" class=\"form-control\"><option selected disabled value = \""+booking.delivered_model_id+"\""+">"+booking.delivered_model_name+"</option></select>");
                            
                            $("#bikeNumbersEdit").html(booking.bike_numbers_options);
                            formEl.find('select[name="bike_number_edit"]').val(booking.bike_number_id);
                            formEl.find('input[name="starting_odo_edit"]').val(booking.start_odo);
                            formEl.find('input[name="route_plan_edit"]').val(booking.route_plan);
                            
                            formEl.find('select[name="helmet_1_edit"]').val(booking.helmet_id_1);
                            formEl.find('select[name="helmet_2_edit"]').val(booking.helmet_id_2);
                            formEl.find('select[name="jacket_1_edit"]').val(booking.jacket_id_1);
                            formEl.find('select[name="jacket_2_edit"]').val(booking.jacket_id_2);
                            formEl.find('input[name="quant_edit[5]"]').val(booking.gloves);
                            formEl.find('input[name="quant_edit[2]"]').val(booking.camera);
                            formEl.find('input[name="quant_edit[3]"]').val(booking.knee_guard);
                            formEl.find('input[name="quant_edit[4]"]').val(booking.saddle_bags);
                            formEl.find('input[name="quant_edit[8]"]').val(booking.tank_bags);
                            formEl.find('input[name="quant_edit[7]"]').val(booking.mobile_holder);
                            
                            formEl.find('input[name="extra_money_edit"]').val(booking.extra_money);
                            formEl.find('select[name="collection_mode_edit"]').val(booking.collection_mode);
                            formEl.find('textarea[name="delivery_notes_edit"]').val(booking.delivery_notes);
                            
                            
                            
     
                        }
                    });
        
		
		

		$('#editDeliveryModal').modal('show');
	});
    
    //End open edit form and fill details from existing delivery
    
    
    
    //Start Confirm Delivery Edit
    
    $('.booking_confirm_deliver_edit_btn').click(function(){
		
        var formEl = $('form[name="edit-delivery"]');
        
        var e = document.getElementById("delivered_at_edit");
        var delivered_at = e.options[e.selectedIndex].text;
         e = document.getElementById("modelDropEdit");
        var deliveredModelId = e.options[e.selectedIndex].text;
         e = document.getElementById("bikeNumberIdEdit");
        var bikeNumberId = e.options[e.selectedIndex].text;
         e = document.getElementById("helmet_1_edit");
        var helmet_1 = e.options[e.selectedIndex].text;
         e = document.getElementById("helmet_2_edit");
        var helmet_2 = e.options[e.selectedIndex].text;
         e = document.getElementById("jacket_1_edit");
        var jacket_1 = e.options[e.selectedIndex].text;
         e = document.getElementById("jacket_2_edit");
        var jacket_2 = e.options[e.selectedIndex].text;
        e = document.getElementById("collection_mode_edit");
        var collection_mode_edit = e.options[e.selectedIndex].text;
        
        
               
        $text = "<p>&nbsp&nbsp"+"Bike Model : "+deliveredModelId+"<br/>"+
        "&nbsp&nbsp"+"Bike Number : "+bikeNumberId+"<br/>"+
        "&nbsp&nbsp"+"Starting Odo : "+formEl.find('input[name="starting_odo_edit"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Route Plan : "+formEl.find('input[name="route_plan_edit"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Delivered At : "+delivered_at+"<br/>"+
        "&nbsp&nbsp"+"Helmet 1 : "+helmet_1+"<br/>"+
        "&nbsp&nbsp"+"Helmet 2 : "+helmet_2+"<br/>"+
        "&nbsp&nbsp"+"Jacket 1 : "+jacket_1+"<br/>"+
        "&nbsp&nbsp"+"Jacket 2 : "+jacket_2+"<br/>"+
        "&nbsp&nbsp"+"Gloves  : "+formEl.find('input[name="quant_edit[5]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Go-Pro  : "+formEl.find('input[name="quant_edit[2]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Knee-Guard  : "+formEl.find('input[name="quant_edit[3]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Saddle Bags  : "+formEl.find('input[name="quant_edit[4]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Mobile-Holder  : "+formEl.find('input[name="quant_edit[7]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Tank Bag  : "+formEl.find('input[name="quant_edit[8]"]').val()+"<br/>"+"&nbsp&nbsp"+"Extra Money : "+formEl.find('input[name="extra_money_edit"]').val()+"<br/>"+"&nbsp&nbsp"+"Collection Mode : "+collection_mode_edit+"<br/>"+"&nbsp&nbsp"+"Notes : "+formEl.find('textarea[name="delivery_notes_edit"]').val()+"</p>";
        $('#confirmEditModalContent').html($text);
        
        
        $('#confirmEditHeading').html("Confirm Delivery - "+formEl.find('input[name="booking_ref_edit"]').val()+" - "+formEl.find('input[name="booking_customer_edit"]').val());
                            
        
		$('#confirmDeliveryEditModal').modal('show');
        
	});
    
    //End Confirm Delivery Edit
    
    
    
    //Submit Edit form
    $('.booking_submit_deliver_edit_btn').click(function(){
        
        $('#edit-delivery').submit();
    });
    
    
    //Dismiss Edit modal
    $('.booking_close_deliver_edit_btn').click(function(){
        
        $('#editDeliveryModal').modal('hide');
        $('#confirmDeliveryEditModal').modal('hide');
    });
    
    
    
    //Open Did Not deliver Edit
    $('.booking_cancel_deliver_edit_btn').click(function(){
        $('#cancelDeliveryEditModal').modal('show');
    });
    
    
    //Submit Did Not deliver Edit
    $('.booking_submit_cancel_deliver_edit_btn').click(function(){
        
        $('#cancel-edit-delivery').submit();
    });
    
    
    //Cancel Did not deliver Edit
    $('.booking_close_cancel_deliver_edit_btn').click(function(){
        
        $('#addDeliveryEditModal').modal('hide');
        $('#cancelDeliveryEditModal').modal('hide');
    });
}.bind(Delivery);

Delivery.addDeliveryModel = function(){
    //Open Add delivery 
	$('.booking_deliver_btn').click(function(){
		
        var id = $(this).data('id');
        var booking_id = $(this).data('booking_id');
        var booking_customer = $(this).data('booking_customer_add');
        var delivered_at_add = $(this).data('delivered_at_add');
        var model_id_add = $(this).data('model_id_add');
        var delivered_at_name_add = $(this).data('delivered_at_name_add');
        var model_id_name_add = $(this).data('model_id_name_add');
        var paylater_add = $(this).data('paylater_add');
        
        
        
		var formEl = $('form[name="add-delivery"]');

		formEl.attr("action","/erp/deliver/"+id);
        
        formEl.find('input[name="booking_ref_add"]').val(booking_id);
        formEl.find('input[name="booking_customer_add"]').val(booking_customer);
        
        $("#delivered_at_add_div").html("<select name=\"delivered_at_add\" id=\"delivered_at_add\" class=\"form-control\"><option selected disabled value = \""+delivered_at_add+"\""+">"+delivered_at_name_add+"</option></select>");
        
        $("#delivered_model_id_add_div").html("<select name=\"delivered_model_id_add\" id=\"modelDropAdd\" class=\"form-control\"><option selected disabled value = \""+model_id_add+"\""+">"+model_id_name_add+"</option></select>");
        
        
       
        
        
        $.ajax({
            type: "GET",
            url: "/api/getBikeNumbersForModelAdd",
            data: { 
                'model_id': model_id_add,
                'booking_id':booking_id
            },
            cache: false,
            beforeSend: function () {
            $('#bikeNumbersAdd').html('<img src="/img/ajax-loader.gif" alt="" width="24" height="24">');
            },
            success: function(html) {                
                $("#bikeNumbersAdd").html( html );
            }
        });
        
        var formCancelEl = $('form[name="cancel-delivery"]');
		formCancelEl.attr("action","/erp/cancel-delivery/"+id);
		
        $('#addModalHeading').html("Delivery - "+booking_id);
        
        if(paylater_add > 0)
        {
            alert("Rs. "+paylater_add+" has to be collected from customer before delivering the bike.");
            $('#addModalPayLater').html("Rs. "+paylater_add+" has to be collected from customer before delivering the bike.");   
        }
        

		$('#addDeliveryModal').modal('show');
	});
    
    
    //Start populate confirm add delivery modal
    $('.booking_confirm_deliver_btn').click(function(){
		
        var formEl = $('form[name="add-delivery"]');
        
        var e = document.getElementById("delivered_at_add");
        var delivered_at = e.options[e.selectedIndex].text;
         e = document.getElementById("modelDropAdd");
        var deliveredModelId = e.options[e.selectedIndex].text;
         e = document.getElementById("bikeNumberIdAdd");
        var bikeNumberId = e.options[e.selectedIndex].text;
         e = document.getElementById("helmet_1_add");
        var helmet_1 = e.options[e.selectedIndex].text;
         e = document.getElementById("helmet_2_add");
        var helmet_2 = e.options[e.selectedIndex].text;
         e = document.getElementById("jacket_1_add");
        var jacket_1 = e.options[e.selectedIndex].text;
         e = document.getElementById("jacket_2_add");
        var jacket_2 = e.options[e.selectedIndex].text;
        
        e = document.getElementById("collection_mode_add");
        var collection_mode_add = e.options[e.selectedIndex].text;
               
        $text = "<p>&nbsp&nbsp"+"Bike Model : "+deliveredModelId+"<br/>"+
        "&nbsp&nbsp"+"Bike Number : "+bikeNumberId+"<br/>"+
        "&nbsp&nbsp"+"Starting Odo : "+formEl.find('input[name="starting_odo_add"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Route Plan : "+formEl.find('input[name="route_plan_add"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Delivered At : "+delivered_at+"<br/>"+
        "&nbsp&nbsp"+"Helmet 1 : "+helmet_1+"<br/>"+
        "&nbsp&nbsp"+"Helmet 2 : "+helmet_2+"<br/>"+
        "&nbsp&nbsp"+"Jacket 1 : "+jacket_1+"<br/>"+
        "&nbsp&nbsp"+"Jacket 2 : "+jacket_2+"<br/>"+
        "&nbsp&nbsp"+"Gloves  : "+formEl.find('input[name="quant_add[5]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Go-Pro  : "+formEl.find('input[name="quant_add[2]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Knee-Guard  : "+formEl.find('input[name="quant_add[3]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Saddle Bags  : "+formEl.find('input[name="quant_add[4]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Mobile-Holder  : "+formEl.find('input[name="quant_add[7]"]').val()+"<br/>"+
        "&nbsp&nbsp"+"Tank Bag  : "+formEl.find('input[name="quant_add[8]"]').val()+"<br/>"+"&nbsp&nbsp"+"Extra Money : "+formEl.find('input[name="extra_money_add"]').val()+"<br/>"+"&nbsp&nbsp"+"Collection Mode : "+collection_mode_add+"<br/>"+"&nbsp&nbsp"+"Notes : "+formEl.find('textarea[name="delivery_notes_add"]').val()+"</p>";
        
        
        $('#confirmModalContent').html($text);
        
        $('#confirmHeading').html("Confirm Delivery - "+formEl.find('input[name="booking_ref_add"]').val()+" - "+formEl.find('input[name="booking_customer_add"]').val());
                            
        
		$('#confirmDeliveryModal').modal('show');
        
	});
    //End populate confirm add delivery 

    
    //Submit add delivery form
    $('.booking_submit_deliver_btn').click(function(){
        
        $('#add-delivery').submit();
    });
    
    
    //Close add delivery
    $('.booking_close_deliver_btn').click(function(){
        
        $('#addDeliveryModal').modal('hide');
        $('#confirmDeliveryModal').modal('hide');
    });
    
    
    //Show cancel modal for add delivery
    $('.booking_cancel_deliver_btn').click(function(){
        $('#cancelDeliveryModal').modal('show');
    });
    
    
    //Submit cancel delivery from add delivery 
    $('.booking_submit_cancel_deliver_btn').click(function(){
        
        $('#cancel-delivery').submit();
    });
    
    
    //Dismiss cancel deliver modal
    $('.booking_close_cancel_deliver_btn').click(function(){
        
        $('#addDeliveryModal').modal('hide');
        $('#cancelDeliveryModal').modal('hide');
    });
    
    
}.bind(Delivery);








