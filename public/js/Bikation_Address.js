var Admin = {
	Bikation_Address:{},
	PromoCode: {}
};

Admin.Bikation_Address.init = function(){
	Admin.Bikation_Address.editBikeModel();
	Admin.Bikation_Address.editSpecBikeModel();
	Admin.Bikation_Address.getGalleryImages();
}.bind(Admin.Bikation_Address);

Admin.Bikation_Address.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var bikeMakeId = $(this).data('bikemakeid');
		var bikeModel = $(this).data('bikemodel');
		var modelDesc = $(this).data('description');
		var status = $(this).data('status');
		var coverImg = $(this).data('cover-img');
		var thumbImg = $(this).data('thumbnail-img');
		var coverImgHolder = '<img src="'+coverImg+'" width="50px" />';
		var thumbImgHolder = '<img src="'+thumbImg+'" width="50px" />';
				
		$('#editBikeModel-modal form').attr("action","/admin/bikeModel/edit/"+id);
		$('#editBikeModel-modal form select option[value='+bikeMakeId+']').prop('selected',true);
		$('#editBikeModel-modal form input[type=text]').val(bikeModel);
		$('#editBikeModel-modal form textarea[name="modelDesc"]').val(modelDesc);
		if(status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);

		$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);
		$('#editBikeModel-modal form #old_thumbnailImage_holder').html(thumbImgHolder);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Admin.Bikation_Address);

Admin.Bikation_Address.editSpecBikeModel = function(){
	$('.bikeModel_spec_btn').click(function(){
		var id = $(this).data('id');
		var spec1Key = $(this).data('spec1key');
		var spec2Key = $(this).data('spec2key');
		var spec3Key = $(this).data('spec3key');
		var spec4Key = $(this).data('spec4key');
		var spec1Value = $(this).data('spec1value');
		var spec2Value = $(this).data('spec2value');
		var spec3Value = $(this).data('spec3value');
		var spec4Value = $(this).data('spec4value');
		$('#specBikeModel-modal form').attr("action","/admin/packages/spec/"+id);
		$('#specBikeModel-modal form input[name=spec1Key]').val(spec1Key);
		$('#specBikeModel-modal form input[name=spec2Key]').val(spec2Key);
		$('#specBikeModel-modal form input[name=spec3Key]').val(spec3Key);
		$('#specBikeModel-modal form input[name=spec4Key]').val(spec4Key);
		$('#specBikeModel-modal form textarea[name=spec1Value]').val(spec1Value);
		$('#specBikeModel-modal form textarea[name=spec2Value]').val(spec2Value);
		$('#specBikeModel-modal form textarea[name=spec3Value]').val(spec3Value);
		$('#specBikeModel-modal form textarea[name=spec4Value]').val(spec4Value);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Admin.Bikation_Address);

