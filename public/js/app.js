//rj
//Navigation Scene

$(window).load(function() {

   

    var controller = new ScrollMagic();

    var isMobile = false;

    var w = $(window).width();

    var hw = $(window).height();

    if(w <= 768){
        isMobile = true;
    }

    var originalWidth = 1600;
    var originalHeight =500;
    var newHeight = Math.round((originalHeight/originalWidth)*w+100);
                  //original height / original width x new width = new height (formula)



    var hslider = newHeight;

     var h = newHeight;

      $(".homeslide li").css("width",w);
    $(".homeslide").css("height",newHeight);

    $(".homeslide li").css("height",newHeight);

   

    /*$("#preloader").fadeOut("slow", function() {*/
       // blurb();
   /* });*/


    $(".extra-slider").extraSlider({
        paginate:false,
        auto:6,
        type:'fade',
        h:newHeight,
        speed:0.5
    });


    

    if(!isMobile){


    function updateBox(e) {
       // console.log("A");
        if (e.type == "start") {
            $("#nav").removeClass("addfix");
        } else {
            $("#nav").addClass("addfix");
            $("#nav").animate({
                "top": "0px"
            });
        }
    }

   /* var fadeTween = TweenMax.staggerTo(".home_slider", 0.5, {
        opacity: 0
    }, 0.5);

    var sliderScene = new ScrollScene({
        duration: 670,
        offset: 200,
        triggerElement: ".home_slider"
    }).setTween(fadeTween).addTo(controller);*/

    var blurbTween = TweenMax.staggerFromTo('#blurb .item', 0.5, {
            y: 50,
            x: 0,
            opacity: 0
        }, {
            y: 0,
            x: 0,
            opacity: 1
        },
        0.2
    );

    function blurb(){
        TweenMax.staggerFromTo('#blurb .item', 0.5, {
            y: 50,
            x: 0,
            opacity: 0
        }, {
            y: 0,
            x: 0,
            opacity: 1
        },
        0.2);
    }

    //Banner text animation

    var blurbScene = new ScrollScene({
            triggerElement: '#blurb .item',
            offset: 0
        })
        .setTween(blurbTween)
        .addTo(controller);

    var scene = new ScrollScene({
            triggerHook: "onLeave",
            offset: 100
        }).setPin("#nav")
        .on("start end", updateBox)
        .on("end", function(e) {
            if (e.target.parent().info("scrollDirection") == "REVERSE") {
                // closeMenu(); 
            }
        }).addTo(controller);

    // var bikeanimation = TweenMax.staggerFromTo("#bikes .item",Math.random(),
    // {
    //  y:50,
    //  opacity:0
    // },{
    //  y:0,
    //  opacity:1,
    //  delay:0.5
    // },0.2);

    function bikeanimation() {
        TweenMax.staggerFromTo("#bikes .item", 0.5, {
            y: 50,
            opacity: 0,
            ease: Linear.easeNone
        }, {
            y: 0,
            opacity: 1,
            ease: Linear.easeNone
        }, 0.2);
        TweenMax.staggerFromTo("#bikes .itemh", 0.5, {
            y: 50,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        }, 0.2);
    }


    var bikeScene = new ScrollScene({
            triggerElement: "#bikes .item",
            triggerHook: "onCenter",
            offset: -300
        })
        .setTween(bikeanimation)
        .addTo(controller);

    var ridealertAnimation = TweenMax.fromTo('.bike_rent', 0.3, {
        right: -320
    }, {
        right: 0
    });


    var ridealertScene = new ScrollScene({
            triggerElement: ".wicked_ride_main",
            triggerHook: "onCenter",
            reverse: false
        })
        .setTween(ridealertAnimation)
        .addTo(controller)

    function rideAnimation() {
        // TweenMax.staggerFromTo("#fx .d2",0.8,
        // {
        //  y:300,
        //  opacity:0,
        //  ease:Linear.easeNone
        // },{
        //  y:0,
        //  opacity:1,
        //  ease:Linear.easeNone
        // },0.6);
        TweenMax.staggerFromTo("#fx .d1", 0.5, {
            y: 150,
            opacity: 0,
            ease: Linear.easeNone
        }, {
            y: 0,
            opacity: 1,
            ease: Linear.easeNone
        }, 0.6);
    }

    var ridesScene = new ScrollScene({
            triggerElement: ".wicked_ride_main",
            triggerHook: "onCenter",
            offset: -50,
            reverse: false
        })
        .setTween(rideAnimation)
        .addTo(controller);




    //Window scroll
    $(window).scroll(function() {
        var st = $(this).scrollTop();
        $(".home_slider").height(h - st);
         $(".home_slider").each(function () {
            var offset = $(this).offset().top;
            var height = $(this).outerHeight();
            offset = offset + height / 2;
            $(this).css({ 'opacity': 1 - (st - offset + h) / h });
            if(st <= 0){
                $(this).css({ 'opacity': 1 });
            }
        });
    });


    //Motorcycle page

   function galleryAnimation(){
        TweenMax.staggerFromTo(".gallery a", 0.5, {
            scale:0,
            ease:Quad.easeInOut
        }, {
            scale:1,
            ease:Quad.easeInOut
        }, 0.4);
   }

    var galleryScene = new ScrollScene({
        triggerElement: ".gallery_tech_main",
        triggerHook: "onCenter",
        offset: -50,
        reverse: false
    })
    .setTween(galleryAnimation)
    .addTo(controller);

    function reviewAnimation(){
        TweenMax.staggerFromTo("#review .left", 0.5, {
            x:-200,
            opacity:0.2
        }, {
            x:0,
            opacity:1
        }, 0.4);

        TweenMax.staggerFromTo("#review .right", 0.5, {
            x:200,
            opacity:0.2
        }, {
            x:0,
            opacity:1
        }, 0.4);
   }

    var reviewScene = new ScrollScene({
        triggerElement: "#review",
        triggerHook: "onCenter",
        offset: -50,
        reverse: false
    })
    .setTween(reviewAnimation)
    .addTo(controller);

}

     /* ANTON KOMKOV START */    
    $('img.lazy').each(function() {
        var $this = $(this);
        $this.attr('src', $this.attr('data-src'));
    });
    /* ANTON KOMKOV END */


});

$(document).on("click", ".accordian_title", function(e) {
    e.preventDefault();

    $(".open_para").slideUp();

    if ($(this).hasClass("active")) {
        //Close
        $(this).next(".open_para").slideUp();
        $(".accordian_title").removeClass("active");
    } else {
        //Open
        $(this).next(".open_para").slideDown();
        $(".accordian_title").removeClass("active");
        $(this).addClass("active");
    }

});

$(document).on("click", ".plus_icon,.nav_open", function(e) {
    e.preventDefault();
    if (!$(this).hasClass("active")) {
        //Open nav
        var d = 0;
        $("#navbikes .item").each(function() {
            TweenMax.to(this, 0.5, {
                y: 0,
                opacity: 1,
                delay: d,
                ease: Linear.easeNone
            });
            var t = $(this).parent().find(".itemh");
            TweenMax.to(t, 0.5, {
                y: 0,
                opacity: 1,
                delay: d
            });
            d = d + 0.2;
        });


    } else {
        TweenMax.to("#navbikes .item", 0.5, {
            y: -50,
            opacity: 0,
            ease: Linear.easeNone
        });
        TweenMax.to("#navbikes .itemh", 0.5, {
            y: 50,
            opacity: 0
        });
    }
    $(this).toggleClass("active");
    $("#subnav").slideToggle();

});
 $("a[href='#top']").click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    });
$(function()
    {
        $('a.show-option').click(function(evt)
        {
            evt.preventDefault();
            $(this).siblings('.options').slideToggle();
        });
    })

$(document).on("click",".titles a",function(e){
    e.preventDefault();
    var filter = $(this).data("filter");
    // console.log(filter);
    filters(filter,'b');
});

function filters(data,items) {
    // console.log(data+items);
    var filter = data;
    var apps = $('.'+items);
    apps.show();  
    if (filter != ''){
        apps.not("."+filter).hide(); 
    } 
    
}

