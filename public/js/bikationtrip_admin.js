var Trip_admin = {
	BikationTrip:{},
	PromoCode: {}
};

Trip_admin.BikationTrip.init = function(){
	Trip_admin.BikationTrip.editBikeModel();
	Trip_admin.BikationTrip.editSpecBikeModel();
}.bind(Trip_admin.BikationTrip);

Trip_admin.BikationTrip.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var Vendor_ID = $(this).data('vendorid');
		var Trip_type = $(this).data('triptype');
		var Trip_Name = $(this).data('tripname');
		var Created_On = $(this).data('createdon');
		var Created_by = $(this).data('createdby');
		var Status = $(this).data('status');
		var Approve_requested_on = $(this).data('approverequestedon');
		var Approved_By = $(this).data('approvedby');
		var Approved_on = $(this).data('approvedon');
		var Cancelled_Request_on = $(this).data('cancelledrequeston');
		var Cancel_approved_by = $(this).data('cancelapprovedby');
		var Cancelled_on = $(this).data('cancelledon');
		var Total_No_of_Tickets = $(this).data('totalnooftickets');
		var No_of_tickets_available = $(this).data('noofticketsavailable');
		var Trip_Start_Date = $(this).data('tripstartdate');
		var Trip_End_Date = $(this).data('tripenddate');
		var Start_Location = $(this).data('startlocation');
		var End_Location = $(this).data('endlocation');
		var Total_Distance = $(this).data('totaldistance');
		var Total_riding_hours = $(this).data('totalridinghours');
		var No_of_stops = $(this).data('noofstops');
		var ticket_denomination = $(this).data('ticketdenomination');
		var Experience_Level_required = $(this).data('experiencelevelrequired');
		var Preffered_Bikes = $(this).data('prefferedbikes');
		var General_Instructions = $(this).data('generalinstructions');
		var Trip_Description = $(this).data('tripdescription');
		var Package_Name = $(this).data('packagename');
		var Package_Description = $(this).data('packagepescription');
		var ticket_1x_details = $(this).data('ticket_1x_details');
		var ticket_1x_price = $(this).data('ticket_1x_price');
		var ticket_2x_details = $(this).data('ticket_2x_details');
		var ticket_2x_price = $(this).data('ticket_2x_price');
		var Media_URL_trip1 = $(this).data('tripphoto');
		var first_name = $(this).data('first_name');
		var last_name = $(this).data('last_name');
		var email = $(this).data('email');
		var mobile_num = $(this).data('mobile_num');

		$('#editBikeModel-modal form').attr("action","/bikation-trip/edit/"+id);
		$('#editBikeModel-modal form input[name=Vendor_ID]').val(Vendor_ID);
		$('#editBikeModel-modal form #Trip_type').html(Trip_type);
		$('#editBikeModel-modal form #Trip_Name').html(Trip_Name);
		$('#editBikeModel-modal form #Created_On').html(Created_On);
		$('#editBikeModel-modal form #Created_by').html(Created_by);
		$('#editBikeModel-modal form #Approve_requested_on').html(Approve_requested_on);
		$('#editBikeModel-modal form #Approved_By').html(Approved_By);
		$('#editBikeModel-modal form #Approved_on').html(Approved_on);
		$('#editBikeModel-modal form #Cancelled_Request_on').html(Cancelled_Request_on);
		$('#editBikeModel-modal form #Cancel_approved_by').html(Cancel_approved_by);
		$('#editBikeModel-modal form #Cancelled_on').html(Cancelled_on);
		$('#editBikeModel-modal form #Total_No_of_Tickets').html(Total_No_of_Tickets);
		$('#editBikeModel-modal form #No_of_tickets_available').html(No_of_tickets_available);	
		$('#editBikeModel-modal form #TripStartDate').html(Trip_Start_Date);		
		$('#editBikeModel-modal form #TripEndDate').html(Trip_End_Date);		
		$('#editBikeModel-modal form #Start_Location').html(Start_Location);		
		$('#editBikeModel-modal form #End_Location').html(End_Location);		
		$('#editBikeModel-modal form #Total_Distance').html(Total_Distance);		
		$('#editBikeModel-modal form #Total_riding_hours').html(Total_riding_hours);		
		$('#editBikeModel-modal form #No_of_stops').html(No_of_stops);		
		$('#editBikeModel-modal form #ticket_denomination').html(ticket_denomination);		
		$('#editBikeModel-modal form #Experience_Level_required').html(Experience_Level_required);		
		$('#editBikeModel-modal form #Preffered_Bikes').html(Preffered_Bikes);
		$('#editBikeModel-modal form #General_Instructions').html(General_Instructions);
		$('#editBikeModel-modal form #Trip_Description').html(Trip_Description);
		$('#editBikeModel-modal form #Package_Name').html(Package_Name);
		$('#editBikeModel-modal form #Package_Description').html(Package_Description);
		$('#editBikeModel-modal form #ticket_1x_details').html(ticket_1x_details);
		$('#editBikeModel-modal form #ticket_1x_price').html(ticket_1x_price);
		$('#editBikeModel-modal form #ticket_2x_details').html(ticket_2x_details);
		$('#editBikeModel-modal form #ticket_2x_price').html(ticket_2x_price);
		$('#editBikeModel-modal form #first_name').html(first_name +' '+last_name);
		$('#editBikeModel-modal form #email').html(email);
		$('#editBikeModel-modal form #mobile_num').html(mobile_num);
		if(Media_URL_trip1 != '')
		{
		$('#editBikeModel-modal form #Media_URL_trip1').html('<img src='+Media_URL_trip1+' width="435" height="350" />'); 
		}
		if(Status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);		

		$('#editBikeModel-modal').modal('show');
	});
}.bind(Trip_admin.BikationTrip);

Trip_admin.BikationTrip.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/bikation-trip/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Trip_admin.BikationTrip);

