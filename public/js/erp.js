var Partner = {
	Add:{},
	Edit:{}
};

var options = [];

Partner.init = function(){
	Partner.initHandlers();
	
}.bind(Partner);

Partner.initHandlers = function(){
	// SELECT ALL MODELS CHECKBOX
	$('.modal').on('hidden.bs.modal', function(e){ 
		$("form", this).get(0).reset();
    }) ;
    $('#addPartnerModal').on('shown.bs.modal', function(e){ 
		Partner.chooseRangeOrSpecificDates.perform();
    }) ;
	$( '.dropdown-menu a' ).on( 'click', function( event ) {
		var $target = $( event.currentTarget ),
			val = $target.attr( 'data-value' ),
			$inp = $target.find( 'input' ),
			idx;


		if ( ( idx = options.indexOf( val ) ) > -1 ) {
			options.splice( idx, 1 );
			setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
		} else {
			options.push( val );
			setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
		}
		
		if($(this).hasClass('make-name')){
			var status = $(this).find('input[type=checkbox][name=make-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.model-ids').prop("checked", status);
		}
		if($(this).hasClass('city-name')){
			var status = $(this).find('input[type=checkbox][name=city-name]').is(":checked");
			if($(event.target)[0].tagName == "A")
				status = !status;
			$(this).next().find('input.area-ids').prop("checked", status);
		}
		
		$( event.target ).blur();

		return false;
	});
    
    
    $('#startDate').datetimepicker({
			timepicker: false,
			scrollInput: false,
			format:'Y-m-d',
			minDate: new Date(2014,01,01),
			
		});

}.bind(Partner);






