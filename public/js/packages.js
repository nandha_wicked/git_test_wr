var Admin = {
	PackagesModel:{},
	PromoCode: {}
};

Admin.PackagesModel.init = function(){
	Admin.PackagesModel.editBikeModel();
	Admin.PackagesModel.editSpecBikeModel();
}.bind(Admin.PackagesModel);

Admin.PackagesModel.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var name = $(this).data('name');
		var desc = $(this).data('desc');
		var destination = $(this).data('destination');
		var date_time = $(this).data('datetime');
		var price = $(this).data('price');
		var coverImg = $(this).data('cover-img');
		var available_seat = $(this).data('availableseat');
		var total_seats = $(this).data('totalseats');
		var ride_include = $(this).data('rideinclude');
		var ride_exclude = $(this).data('rideexclude');
		var ride_rule = $(this).data('riderule');
		var meeting_point = $(this).data('meetingpoint');
		var recommanded_bike = $(this).data('recommandedbike');
		var total_km = $(this).data('totalkm');
		var total_days = $(this).data('totaldays');
		var status = $(this).data('status');
		var coverImgHolder = '<img src="'+coverImg+'" width="50px" />';
		
				
		$('#editBikeModel-modal form').attr("action","/admin/packages/edit/"+id);
		$('#editBikeModel-modal form input[name=name]').val(name);
		$('#editBikeModel-modal form textarea[name="desc"]').val(desc);
		$('#editBikeModel-modal form input[name=destination]').val(destination);
		$('#editBikeModel-modal form input[name=date_time]').val(date_time);
		$('#editBikeModel-modal form input[name=price]').val(price);
		$('#editBikeModel-modal form #old_coverImage_holder').html(coverImgHolder);
		$('#editBikeModel-modal form input[name=available_seat]').val(available_seat);
		$('#editBikeModel-modal form input[name=total_seats]').val(total_seats);

		$('#editBikeModel-modal form textarea[name="ride_include"]').val(ride_include);
		$('#editBikeModel-modal form textarea[name="ride_exclude"]').val(ride_exclude);
		$('#editBikeModel-modal form textarea[name="ride_rule"]').val(ride_rule);
		$('#editBikeModel-modal form textarea[name="meeting_point"]').val(meeting_point);
		$('#editBikeModel-modal form textarea[name="recommanded_bike"]').val(recommanded_bike);
		$('#editBikeModel-modal form input[name=total_km]').val(total_km);
		$('#editBikeModel-modal form input[name=total_days]').val(total_days);
		if(status == '1')
			$('#editBikeModel-modal form #bmose1').prop("checked",true);	
		else
			$('#editBikeModel-modal form #bmose2').prop("checked",true);

		$('#editBikeModel-modal').modal('show');
	});
}.bind(Admin.PackagesModel);

Admin.PackagesModel.editSpecBikeModel = function(){
	$('.bikeModel_delete_btn').click(function(){
		var id = $(this).data('id');
		$('#specBikeModel-modal form').attr("action","/admin/packages/delete/"+id);
		$('#specBikeModel-modal').modal('show');
	});
}.bind(Admin.PackagesModel);


