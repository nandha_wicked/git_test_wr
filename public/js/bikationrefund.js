var Admin = {
	BikationRefund:{},
	PromoCode: {}
};

Admin.BikationRefund.init = function(){
	Admin.BikationRefund.editBikeModel();
}.bind(Admin.BikationRefund);

Admin.BikationRefund.editBikeModel = function(){
	$('.bikeModel_edit_btn').click(function(){
		var id = $(this).data('id');
		var reference_ID = $(this).data('referenceID');
		var bikation_bike_rental = $(this).data('bikationbikerental');
		var user = $(this).data('user');
		var refund_reason = $(this).data('refundeason');
		var status = $(this).data('status');
		var transaction_ID_from_PG = $(this).data('transactionIDfromPG');
		var created_on = $(this).data('createdon');
		var updated_on = $(this).data('updatedon');
		var Created_By = $(this).data('CreatedBy');
		var Updated_by = $(this).data('Updatedby');
		var Approved_By = $(this).data('ApprovedBy');
		var Approved_on = $(this).data('Approvedon');
		

		$('#editBikeModel-modal form').attr("action","/bikation-refund/edit/"+id);
		$('#editBikeModel-modal form input[type=text]').val(reference_ID);
		$('#editBikeModel-modal form input[type=text]').val(bikation_bike_rental);
		$('#editBikeModel-modal form input[type=text]').val(user);
		$('#editBikeModel-modal form input[type=text]').val(amount);
		$('#editBikeModel-modal form input[type=text]').val(refund_reason);
		$('#editBikeModel-modal form input[type=text]').val(status);
		$('#editBikeModel-modal form input[type=text]').val(transaction_ID_from_PG);
		$('#editBikeModel-modal form input[type=text]').val(created_on);
		$('#editBikeModel-modal form input[type=text]').val(updated_on);
		$('#editBikeModel-modal form input[type=text]').val(Created_By);
		$('#editBikeModel-modal form input[type=text]').val(Updated_by);
		$('#editBikeModel-modal form input[type=text]').val(Approved_By);
		$('#editBikeModel-modal form input[type=text]').val(Approved_on);
		$('#editBikeModel-modal').modal('show');
	});
}.bind(Admin.BikationRefund);

